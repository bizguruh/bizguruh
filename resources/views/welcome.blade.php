<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Your social Business school, BizGuruh is an independent provider of business education and strategic market insight for the African environment, borne out of the desire to see more sustainable businesses in Africa.">
        <meta name="theme-color" content="#fff">

        <title>Your Social Business School</title> 
        <link rel=" icon" href="images/logo-min.png" type="image/png" sizes="16x16 32x32 64x64">
        <link rel="manifest" href="manifest.json" />

     
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <link href="https://unpkg.com/cloudinary-video-player/dist/cld-video-player.min.css" rel="stylesheet">
       
        <link rel="prefetch" href="https://fonts.googleapis.com/css?family=Playfair+Display:700|Source+Sans+Pro&display=swap" rel="stylesheet"
      />
      <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Open+Sans&display=swap" rel="stylesheet">
        <link  href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet">
        <script>
          
            window.Engagespot={},q=function(e){return function(){(window.engageq=window.engageq||[]).push({f:e,a:arguments})}},f=["captureEvent","subscribe","init","showPrompt","identifyUser","clearUser"];for(k in f)Engagespot[f[k]]=q(f[k]);var s=document.createElement("script");s.type="text/javascript",s.async=!0,s.src="https://cdn.engagespot.co/EngagespotSDK.2.0.js";var x=document.getElementsByTagName("script")[0];x.parentNode.insertBefore(s,x);
           
            </script>
   

    
    </head>
    <body>
                <div id="app">
                    <router-view></router-view>
                </div>
             
                 <script>
                     
        //              if ('serviceWorker' in navigator) {
        //     window.addEventListener('load', () => {
        //         navigator.serviceWorker.register('/sw.js');
        //     });
        // }
        // Notification.requestPermission( permission => {
        //     if (permission) {
        //         let notification = new Notification('New post alert!', {
        //         body: 'content for the alert',
        //         icon: "https://pusher.com/static_logos/320x320.png" 
        //       });

              // link to page on clicking the notification
            //   notification.onclick = () => {
            //     window.open(window.location.href);
            //   };
            // }
             
            // });
          
        
                 </script>
                <script  src="{{asset('js/manifest.js')}}"  defer></script>
                <script  src="{{asset('js/vendor.js')}}" defer></script>
                <script  src="{{asset('js/app.js')}}" defer> </script>
     
       <script src="https://kit.fontawesome.com/51185f46ed.js" defer></script>

      <script src="https://unpkg.com/cloudinary-core/cloudinary-core-shrinkwrap.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/cloudinary-video-player/dist/cld-video-player.min.js" type="text/javascript"></script>
       <script src="https://js.paystack.co/v1/inline.js" defer></script>

        <link rel="prefetch"
                href="{{asset('js/popper.min.js')}}"
                as="script"
            >
            <link
            rel="prefetch"
            href="{{asset('js/bootstrap.min.js')}}"
            as="script"
            >
          

    </body>
</html>
