<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

    <title>Subscription</title>


</head>
<body>
    <div  style=" background: #f2f5fe;
    padding: 10px;">
     <h2 style="text-align:center;"><span style="color:black">BIZ</span><span style="color:#a3c2dc">GURUH</span></h2>
        <div  style="border-radius:5px color:rgba(0, 0, 0, .74);">
            <table class="table" style="width:600; margin-left:auto;margin-right:auto;  background:#ffffff; padding:20px 20px;border-radius:5px color:rgba(0, 0, 0, .74);">
      
                <tbody>
                <tr>
                    <td>
                    <h4>Hello {{$user->name}},</h4></td></tr>
                   
                    <tr>
                    <td scope="row"><p>You have been invited to <strong>{{$user->account_name}}</strong> Accounting portal</p>
                      
                            <h4>Find your Details below</h4>
                       <p>Role : {{$user->role}}</p>
                        <p>Email : {{$user->email}}</p>
                        <p>Password: {{$user->password}}</p>
                        </td>
                     
                    </tr>
                    <tr>
                        <td>
                        <button type="button" class="btn btn-primary " 
                        style="box-shadow: 0 0 1px 1px #ccc;background:#a4c2db;font-size:14px; color:white;border:1px solid #a4c2db; text-decoration:none;padding:8px 15px ;"> <a  style=" color:white; text-decoration :none;" href="localhost:8000/account?id={{$user->id}}">Click here to begin</a></button>
                        </td>
                    </tr>
                  
                   
                </tbody>
            </table>


        </div>
        <div>
            <table class="table" style=" margin-left:auto;margin-right:auto;   padding:20px 20px;border-radius:5px color:rgba(0, 0, 0, .74);">
                <tbody>
                    <tr>
                        <td  style="font-size:11px; color:rgba(0, 0, 0, .54); text-align:center">
                            <hr>      
                            <p>You can contact us  <a href="http://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
                              <p>  BizGuruh values your privacy; read our  <a href="http://bizguruh.com/policies">privacy policy</a> and our  <a href="http://bizguruh.com/terms">terms of use</a>.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>



    </div>

</body>
</html>
