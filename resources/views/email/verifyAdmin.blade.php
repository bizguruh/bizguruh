<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to the site {{$adminUser['name']}}</h2>
<br/>
Your registered email-id is {{$adminUser['email']}} , Please click on the below link to verify your email account
<br/>
<a href="{{url('adminuser/verify', $adminUser->verifyAdmin->token)}}">Verify Email</a>
</body>

</html>