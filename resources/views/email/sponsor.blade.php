<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

    <title>Thanks For Your Purchase</title>


</head>
<body>
    <div class="main" style=" background: #fafafa;">
    <h2 style="text-align:center;padding:20px "><span style="color:#333333">Biz</span><span style="color:#a3c2dc">Guruh</span></h2>
           
        <div style="width:60%;margin:0 auto;padding:15px;border-radius:3px; background:#fff">
        <p>You have Just Purchased the sponsorship package</p>
          <P>Your token can be found below, please keep it safe</P>

          <p class="mt-4"> Amount Paid : <b>NGN {{$name->price}}</b></p>
       <p>Token : <b> {{$name->token}}</b></p>
       <p>Amount of Users : <b>{{$name->users}}</b></p>
       <p>Validity : <b>1 year</b></p>


        </div>

      
        <div style="font-size:12px; text-align:center;color:rgba(0, 0, 0, .54);padding-top:40px;padding:bottom:20px">
                            <p>If you have trouble accessing your purchase, you can contact support <a href="http://bizguruh.com/contact">here</a></p>
                        <address>
                                <p>Yours Sincerely,
                                    </p>
                             <p>  Your BizTribe @ BizGuruh.</p>

                        </address>

           </div>
    </div>
    




</body>
</html>
