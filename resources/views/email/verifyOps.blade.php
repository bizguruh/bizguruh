<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to the site {{$opsUser['name']}}</h2>
<br/>
Your registered email-id is {{$opsUser['email']}} , Please click on the below link to verify your email account
<br/>
<a href="{{url('opsuser/verify', $opsUser->verifyOps->token)}}">Verify Email</a>
</body>

</html>