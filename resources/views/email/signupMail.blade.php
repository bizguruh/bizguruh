
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

 <title>Welcome to Bizguruh</title>
  <style>
    body{
      line-height: 1.6;
      font-size: 18.5px;
      width: 100%;
      background-color: hsl(207, 43%, 90%);
    }
    .main{
      background-color: #f7f8fa;
      background-image: url('https://bizguruh.com/images/curve2.jpg');
      background-size: cover;
      border-radius: 5px;
      width: 80%;
      padding: 15px;
    }
    a{
      text-decoration: none;
    }
    .welcome-header{
      font-size: 44px;
      text-align: center;
      width: 100%;
      font-family: 'Josefin Sans';
     color: #a4c2db;
    }
    ul{
      width:100%;
    }
    .img-contain{
      width: 100%;
      height: 200px;
    }
    .insights{
      list-style: none;
      padding:  0;
    }
    .insights tr td{
      padding: 20px;
      text-align: center;
    }
    ul li{
      padding: 15px 0;
    }
    .insights li{

      text-align: center;
      border:1px solid #ccc;
    }
    .insights tr td a p{
      font-size: 18px;
      font-weight: bold;
      padding: 10px;
      text-align: center;
    }
    
    .img-contain img{
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    .settle-text{
      font-size: 24px;
      color: hsl(207, 43%, 20%) ;
      text-align: center;
      width: 100%;
    }
    a{
      color: hsl(207, 43%, 20%) ;
    }
    table{
      font-size: 18px;
    }
    .elevated_btn{
      position:relative;
      max-width: 250px;
      margin-top: 16px;
      margin-right: auto;
      margin-left: auto;
      padding: 20px 30px 18px;
      -webkit-align-self: center;
      -ms-flex-item-align: center;
      -ms-grid-row-align: center;
      align-self: center;
      border: 2px none transparent;
      border-radius: 4px;
      color : #fff;
      box-shadow: 0 8px 14px 0 rgba(5, 17, 26, 0.28);
      -webkit-transition: all 400ms ease;
      transition: all 400ms ease;
      font-family: Sailec, sans-serif;
      /* background-color: hsl(207, 43%, 20%); */
      font-size: 16px;
      line-height: 22px;
      font-weight: 700;
      text-align: center;
      letter-spacing: 1px;
      text-decoration: none;
      text-transform: capitalize;
      display:flex;
      justify-content:space-evenly;
    align-items:center;
  }
  .elevated_btn:hover{
    background-color: #fff !important;
    box-shadow: 0 9px 17px 1px rgba(5, 17, 26, 0.39);
    opacity: 0.92;
    -webkit-transform: translate(0px, -3px);
    -ms-transform: translate(0px, -3px);
    transform: translate(0px, -3px);
    color: hsl(207, 43%, 20%) !important;
  }
  .first{
     /* background-color:#f7f8fa; */
    background-image: url('https://bizguruh.com/images/mockups/why.png');
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 40%;
  }
  .second{
    /* background-color:#f7f8fa; */
    background-image: url('https://bizguruh.com/images/account.jpg');
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 40%;
  }
  .intro{
   
  }
  .bg{
    background-color: hsl(207, 43%, 95%);
    padding: 40px 0;
  }
  li, p, ul,ol{
    font-size: 18px;
  }
  li a{
    color: hsl(207, 43%, 20%);
  }
  .welcome-image{
    width: 100%;
    text-align: center;
  }
  .prof{
    background:linear-gradient(90deg, #673ab7 0%, #512da8 100%); 
  }
  .bap{
    background:linear-gradient(90deg, #000000 0%, #434343 100%);
  }
  .basic{
    background:linear-gradient(90deg, #44a08d 0%, #093637 100%);
  }
  .essential{
    background:linear-gradient(90deg, #0575e6 0%, #021b79 100%);
  }
    @media (max-width:575px){
      .first,.second{
        padding:10px 0;
        background-size: cover;
        width: 300px !important;
      }
      .img-contain{
        height: 150px;
      }
      .one{
        display: flex;
        flex-direction: column-reverse;
      }
      .welcome-header{
        font-size: 28px
      }
      .settle-text{
        font-size: 16px;
      }
      .elevated_btn{
      position:relative;
      max-width: 450px;
      margin-top: 16px;
      margin-right: auto;
      margin-left: auto;
      padding: 15px 20px 13px;
      -webkit-align-self: center;
      -ms-flex-item-align: center;
      -ms-grid-row-align: center;
      align-self: center;
      border: 2px none transparent;
      border-radius: 4px;
      color : #fff;
      box-shadow: 0 8px 14px 0 rgba(5, 17, 26, 0.28);
      -webkit-transition: all 400ms ease;
      transition: all 400ms ease;
      font-family: Sailec, sans-serif;
      /* background-color: hsl(207, 43%, 20%); */
      font-size: 16px;
      line-height: 22px;
      font-weight: 700;
      text-align: center;
      letter-spacing: 1px;
      text-decoration: none;
      text-transform: capitalize;
      display:flex;
      justify-content:space-evenly;
    align-items:center;
  }

  .main{
    width: 95%;
  }
    }
  </style>

</head>
<body>

<div class="bg">

  <table class="main" style="margin:0 auto;">
    <tr>
      <td >
        <p> Dear {{$newUser['name']}},</p>   
      </td>
     </tr>
     <tr>
       <td  class="intro" style="padding:20px;">
           <h1 class="welcome-header"> <span style="  color: hsl(207, 43%, 20%);">Welcome to BizGuruh!</span></h1>
           <h4 style="text-align: center"> We’re glad you’re here</h4>
           <div style="text-align:center;:0 auto;width:100%" class="welcome-image">
             <img src="https://bizguruh.com/images/mockups/new2.png" alt="IMAGE" width="100%" height="auto" style="text-align:center;margin:0 auto" srcset="">
           </div>
           <h3 class="settle-text">Let’s get you settled in, shall we?</h3>
       </td>
     </tr>
    <tr>
      <td>
       <table>
         <tr class="one">
            <td style="padding:0px 10px 40px">
               <h3 style="background:linear-gradient(90deg, #44a08d 0%, #093637 100%); padding:10px; text-align:center; color:white">START WITH THE BASICS</h3>
               <p>You’re now part of a community of entrepreneurs who can access honest and practical Africa-focused business insights and resources, on the go.</p>
               <ul>
                 <li>
                   Enjoy select free business insights from some of our experts, to get you started
                 </li>
                 <li>
                   Follow subject matter and industry experts, and get their latest insights delivered to you, fresh!
                 </li>
                 <li>
                   Save valued resources to your BizLibrary, for easy reference anytime
                 </li>
               </ul>
               <div style="text-align: center">
                 <a href="http://bizguruh.com/explore">  <button class="elevated_btn basic">Start Exploring</button>
                 </a>
                 </div>
             </td>
             {{-- <td class="first">
               
             </td>
  --}}
         </tr>
        
       </table>
      </td>
    </tr>
    <tr>
     
   </tr>
   
    <tr>
      <td>
        <table>
          <tr>
            <td>
             <tr class="one">
               
               <td >
                 <h3 style="background:linear-gradient(90deg, #000000 0%, #434343 100%); padding:10px; text-align:center; color:white">NOT ABOUT THAT BASIC LIFE?</h3>
                 <p>Then subscribe to our BizGuruh BAP-6 Program, a 6-week Business Acceleration Program designed for the entrepreneur who is ready to be heard loud and clear through their business.</p>
                 <p>
                   Once you do, you will have access to subject matter and industry experts who will work with you to develop a business roadmap that is structured to be fulfilling and profitable, even whilst serving others.
                 </p>
                 <p>
                   This program will take you on a journey of renewed self-awareness and arm you with sound understanding of your business-operating environment. <br> <br>
                   Finally, you will be able to develop and deploy strategies that best align your resources and capabilities to the requirements of your business operating environment and overall vision.
                 </p>
                 <p>
                   All templates/checklists/guides used throughout the program will be archived on your custom dashboard as a reference point or to see how far you’ve come, anytime; because, while growing a business is awesome, know what’s even better? Being at peace with who you really are while doing so!
                 </p>
                 <div style="text-align: center">
                   <a href="http://bizguruh.com//user-subscription/3">  <button class="elevated_btn bap">Start BAP-6 Now</button> </a>
                 </div>
               </td>
             </tr>
            
            </td>
          </tr>
        </table>
      </td>
    </tr>
  
   <tr>
     <td>
       <table style="padding: 45px 0 40px">
         <tr class="one" >
           <td>
             <h3  style="background:linear-gradient(90deg, #673ab7 0%, #512da8 100%); padding:10px; text-align:center; color:white">WANT TO GET PROFESSIONAL?</h3>
             <p>Just ditch the paper work for our business accounting solution. A powerful, yet approachable accounting system that gets you organized, so that you can track your business profitability and growth in real-time. <br> <br>
               Our accounting system provides an easy way to complete your daily book keeping entries and generate comprehensive accounting statements on the go.
             </p>
             <p>
               As a welcome gift, we’re offering <strong>3 Months free</strong> business diagnostics sessions, which include expert analysis and advisory when you use the system, so take advantage of that now.
             </p>
             <div style="text-align: center">
               <a href="http://bizguruh.com//user-subscription/2">  <button class="elevated_btn prof">Start  Now</button></a>
             </div>
           </td>
          
         </tr>
        
       </table>
     </td>
   </tr>
   <tr>
     <td style="text-align: center">
       <h3  style="background:linear-gradient(90deg, #0575e6 0%, #021b79 100%); padding:10px; text-align:center; color:white">NUMBERS DON'T LIE</h3>
       <p>Eager to know what your numbers really say about your business?</p>
       <a href="http://bizguruh.com/account">  <button class="elevated_btn essential">Begin Here</button>
     </td>
   </tr>
   <tr>
    <td style="padding: 20px 0;">
     <h4>In the meanwhile, here are some insights you might like:</h4>
    

     <table class="insights">
     
       <tr>
         <td>
          <a href="https://bizguruh.com/article-page/238/WHENYOURPROSPECTSTHINKYOUARETOOEXPENSIVE">
            <div class="img-contain">
              <img  src="https://res.cloudinary.com/diuflmhz2/image/upload/v1539121209/coverImage1568240250.jpg" alt="">
             </div>
            <p> When your prospects think you are too expensive</p></a>
         </td>
         <td>
          <a href="https://bizguruh.com/article-page/230/CUSTOMERRETENTIONSTRATEGIES">
            <div class="img-contain">
             <img  src="https://res.cloudinary.com/diuflmhz2/image/upload/v1539121209/coverImage1574172490.jpg" alt="">
            </div>
           <p> Customer retention strategies</p></a>
         </td>
       </tr>
       <tr>
       
        <td>
          <a href="https://bizguruh.com/video/191">
            <div class="img-contain">
              <img  src="https://res.cloudinary.com/diuflmhz2/image/upload/v1539121209/coverImage1568802343.jpg" alt="">
            </div>
          <p>Does your business really need a loan scale?</p></a>
        </td>
        <td>
          <a href="https://bizguruh.com/video/182">
            <div class="img-contain">
              <img  src="https://res.cloudinary.com/diuflmhz2/image/upload/v1539121209/coverImage1568790343.jpg" alt="">
            </div>
         <p> The customer is always right- Yes or No?</p></a>
        </td>
       </tr>

     </table>
   
     
     
    </td>
   </tr>
  

  </table>
  <table class="bg" style=" width:80%;margin:0 auto">
    <tr  style="text-align: center">
      <td  style="font-size:12px; color:rgba(0, 0, 0, .54); text-align:center;">
         
          <p style="text-align: center;font-size:12px">Our customer service is run by real people who are committed to seeing you win, so reach out anytime.</p>
          <p  style="text-align: center;font-size:12px">You can contact us  <a href="http://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
            <p  style="text-align: center;font-size:12px">  BizGuruh values your privacy; read our  <a href="http://bizguruh.com/policies">privacy policy</a> and our  <a href="http://bizguruh.com/terms">terms of use</a>.</p>
      </td>
   </tr>
   </table>
   

</div>

</body>
</html>
