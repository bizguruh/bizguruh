<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

    <title>Content Purchase</title>


</head>
<body>
    <div class="main" style=" background: #f2f5fe;
    padding: 10px;">
     <h2 style="text-align:center;"><span style="color:#333333">Biz</span><span style="color:#a3c2dc">Guruh</span></h2>
        <div class="main" style=" width:80%; margin-left:auto;margin-right:auto; background:#ffffff; padding:10px ;border-radius:5px ">
            <h4>Hi {{$name['name']}},</h4>
            <p>
                    Thank you for buying with us. Below is summary of your recent purchase:
                    </p>
                    <table class="table table-striped table-inverse table-responsive">
                            <tbody>
                                <tr>
                                    <td><b>Order Id :</b></td>
                                    <td>#{{$name['order_id']}}</td>
                                </tr>
                                <tr>
                                    <td><b>Date : </b></td>
                                    <td>{{date("l jS \of F Y")}}</td>
                                </tr>
                                <tr>
                                    <td><b>Title : </b></td>
                                    <td>{{$name['productTitle']}} ({{$name['category']}})</td>
                                </tr>
                                <tr>
                                    <td><b>Industry :</b></td>
                                <td>{{$name['subjectMatter']}} {{$name['industry']}}</td>
                                </tr>
                                <tr>
                                    <td><b>Amount :</b></td>
                                    <td>NGN {{$name['totalAmount']}}.00</td>
                                </tr>

                                <tr>
                                    <td><b>Order No : </b></td>
                                    <td>{{$name['orderNum']}}</td>
                                </tr>
                                <tr>
                                    <td><b>Reference No : </b></td>
                                    <td>{{$name['referenceNo']}}</td>
                                </tr>

                            </tbody>
                        </table>

                        {{-- <a href="http://">Download Now</a> /
                        <a href="http://">Read Now</a> / --}}
                            <div style="text-align:center;padding:10px" >
                                    <button type="button" class="btn btn-primary"  style="background:#a3c2dc; color:white">  <a  style="background:#a3c2dc; color:white; text-decoration:none;" href="https://bizguruh.com/profile/products/subscription">Go to Library</a></button>

                            </div>
                            <hr style="width:70%; text-align:center;margin-left:auto;margin-right:auto;color: rgba(0, 0, 0,0.54);">
                      <div style="font-size:12px; text-align:center;color:rgba(0, 0, 0, .54)">
                            <p>If you have trouble accessing your purchase, you can contact support <a href="http://bizguruh.com/contact">here</a></p>
                        <address>
                                <p>Yours Sincerely,
                                    </p>
                             <p>  Your BizTribe @ BizGuruh.</p>

                        </address>

                      </div>

        </div>



    </div>

</body>
</html>
