<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     

    <title>Welcome Email</title>
    <style>
        body{
            line-height: 1.6;
        }
        .btn-primary{
            padding: 10px 50px;
            background: #a4c2db;
            color: #fff !important;
            text-decoration: none;
            border-radius: 3px;
            border: 1px solid #fff;
            font-size: 18px;
            box-shadow: 0px 0 2px 1px #ccc;
        }
        .btn-primary:hover{
            background: rgba(164, 194, 219, .5);
            cursor: pointer;
        }
        ul, ol ,li{
            list-style: none;
        }
       
    
    
    </style>
</head>

<body style="margin: 0; padding:0 ; background:#f2f5fe">


<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 15px; margin-bottom: 20px" >
    <tr>
        <td> <h2 style="text-align:center;"><span style="color:#333">BIZ</span><span style="color:#a4c2db">GURUH</span></h2>
                <table align="center" border="0" cellpadding="15px" cellspacing="0" width="800"  
                bgcolor="#FFFFFF">
                        <tr>
                                <td>
                                  <table width="100%" height="200">
                                      <tr>
                                          <td>
                                            <h4>Hello {{$vendorUser['storeName']}},</h4>
                                          </td>
                                      </tr>
                                      <tr>
                                         
                                          <td  align="center">
                                             
                                            <strong>WELCOME TO THE BIZGURUH PARTNER FORUM (BPF) </strong>
             
                                      </tr>
                                  </table>
                                </td>
                               </tr>
                        <tr>
                         <td>
                           <table width="100%" height="200" cellpadding="0" cellspacing="0">
                               <tr>
                                   <td width="50%"  style="padding: 15px; background: #e8f3ec"><h3>  A PLACE TO SHARE YOUR AFRICAN BUSINESS INSIGHT</h3>
      
                                    <p>  We’ve created a home for all stakeholders of Africa’s informal sector to converge and enrich each other’s perspective. For us its really about connecting, learning and collaborating to change Africa’s socioeconomic
                                         narrative through her enterprises.</p>
                                    </td>
                                 
                                    <td width="50%">  <img src="{{asset('/images/first.jpg')}}" alt=" A PLACE TO SHARE YOUR AFRICAN BUSINESS INSIGHT"
                                    width="100%" 
                                    ></td>
                               </tr>
                           </table>
                         </td>
                        </tr>
                        <tr>
                                <td>
                                <table width="100%" height="150">
                                    <tr>
                                        <td width="50%" ><img  src="{{asset('/images/tribe.jpg')}}" alt="A PLACE TO SHARE YOUR AFRICAN BUSINESS INSIGHT"   width="100%"></td>
                                        <td width="50%" style="padding: 15px"> <p> You are here because you have demonstrated a keen desire to see the African informal sector thrive; but this does not have to be at the expense of your intellectual property. So we created a safe and efficient environment where you can continue doing what you love— ALL FOR FREE.</p></td>
                                    </tr>
                                </table>
                                </td>
                               </tr>
                               <tr>
                                    <td>
                                   <table width="100%"  height="250" style="background: #fafafa">
                                     
                                      <tr>
                                            <th> <h3 >Being a BPF member guarantees you:</h2></th>
                                      </tr>
                                       <tr>
                                           <td>
                                               <table width="100%" >
                                                   <tr > 
                                                       <ul style=" list-style:none;">
                                                            <td align="center" width="33.333%" style="padding:20px 5px">   <li>A platform for broader impact</li></td>
                                                            <td align="center" width="33.333%"  style="padding:20px 0"> <li>Exposure to a wider network</li></td>
                                                            <td align="center"  width="33.333%"  style="padding:20px 0">  <li>Higher revenue</li></td>
                                                       </ul>
                                                   </tr>
                                                   <tr>
                                                      <ul>
                                                            <td align="center" width="33.333%"  style="padding:20px 0"> <li>Cost savings on market research </li></td>
                                                            <td align="center" width="33.333%"  style="padding:20px 0"> <li>Enhanced marketing</li></td>
                                                            <td align="center" width="33.333%"  style="padding:20px "><li>Front row pass at a forum of new elite business educators</li></td>
                                                      </ul>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                                    </td>
                                   </tr>
                                   <tr>
                                        <td>
                                        <table width="100%"  height="250" >
                                            <tr>
                                                <th>    <h4 style="width:100%">HOW IT WORKS</h4></th>
                                            </tr>
                                            <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center" width="20%"><img style="border-radius:50%" src="{{asset('/images/share.jpg')}}" alt="image" width="60" height="60"></td>
                                                        <td align="center" width="20%"><img style="border-radius:50%"src="{{asset('/images/referral.png')}}" alt="image" width="60" height="60"></td>
                                                        <td align="center" width="20%"> <img style="border-radius:50%" src="{{asset('/images/expand.png')}}" alt="image" width="60" height="60"></td>
                                                        <td align="center" width="20%"><img style="border-radius:50%" src="{{asset('/images/makemoney.jpg')}}" alt="image" width="60" height="60"></td>
                                                    </tr>
                                                    <tr>
                                                            <td align="center" width="20%"><p>
                                                                    -Share something good weekly
                                                            </p></td>
                                                            <td align="center" width="20%">   <p>-Get recommended to subscribers</p></td>
                                                            <td align="center" width="20%">  <p>-Grow your BizTribe</p></td>
                                                            <td align="center" width="20%">  <p>-Earn for your work</p></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                        </td>
                                       </tr>
                                       <tr>
                                            <td>
                                               
                                           <table style="background:#ccc " height="200"> 
                                               <tr>
                                                   <td align="center"> <h3 style="padding:15px;"> Now you can focus on what truly matters— putting your best business insights out there to raise a generation of African enterprise titans.</h3></td>
                                               </tr>
                                           </table>
                                            </td>
                                           </tr>
                                           <tr>
                                                <td>
                                               <table width="100%">
                                                   <tr>
                                                       <td align="center">  <p>  Click link below to verify your account and lets get to work, the world needs a superhero just like you!</p></td>
                                                   </tr>
                                                   <tr>
                                                       <td align="center" style="padding: 20px">
                                                            <a name="" id="" class="btn btn-primary" 
                                                            href="{{url('vendoruser/verify', $vendorUser->verifyUser->token)}}
                                                            "
                                                             role="button">Verify Me</a>
                                                       </td>
                                                   </tr>
                                               </table>
                                                </td>
                                               </tr>
                                               <tr><td>
                                                <hr style="width:70%; text-align:center;margin-left:auto;margin-right:auto;color: rgba(0, 0, 0,0.54);">
                                              </td></tr>
                                              <tr>
                                                <td  style="font-size:11px; color:rgba(0, 0, 0, .54); text-align:center">
                                                    <p>You can contact us  <a href="http://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
                                                      <p>  BizGuruh values your privacy; read our  <a href="http://bizguruh.com/policies">privacy policy</a> and our  <a href="http://bizguruh.com/terms">terms of use</a>.</p>
                                                </td>
                                              </tr>
                                           
                       </table>
                       
        </td>
    </tr>
</table>


</body>

</html>