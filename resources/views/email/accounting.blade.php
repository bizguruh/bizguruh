<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

    <title>Subscription</title>


</head>
<body>
    <div class="main" style=" background: #f2f5fe;
    padding: 10px;">
     <h2 style="text-align:center;"><span style="color:black">BIZ</span><span style="color:#a3c2dc">GURUH</span></h2>
        <div class="main" style="border-radius:5px color:rgba(0, 0, 0, .74);">
            <table class="table" style=" width:600; margin-left:auto;margin-right:auto;  background:#ffffff; padding:20px 20px;border-radius:5px color:rgba(0, 0, 0, .74);">
      
                <tbody>
                    <tr scope="row" class="text-center" style="text-align:center">Welcome to Bizguruh Accounting</tr>
                    <tr>
                        <td scope="row"><p>Use our accounting system to organize your business financial information and monitor your business growth</p>
                      
                            <h4>Accounting Login Details</h4>
                        <p>Email : {{$user->email}}</p>
                        <p>Password: admin</p>
                        </td>
                     
                    </tr>
               
                   
                </tbody>
            </table>


        </div>
        <div>
            <table class="table" style=" margin-left:auto;margin-right:auto;   padding:20px 20px;border-radius:5px color:rgba(0, 0, 0, .74);">
                <tbody>
                    <tr>
                        <td  style="font-size:11px; color:rgba(0, 0, 0, .54); text-align:center">
                            <hr>      
                            <p>You can contact us  <a href="http://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
                              <p>  BizGuruh values your privacy; read our  <a href="http://bizguruh.com/policies">privacy policy</a> and our  <a href="http://bizguruh.com/terms">terms of use</a>.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>




    </div>

</body>
</html>
