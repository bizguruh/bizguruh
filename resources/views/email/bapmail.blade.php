
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

 <title>BAP-6 Welcome mail</title>
  <style>
    body{
      line-height: 1.6;
      font-size: 18px;
    
      background:#f7f8fa;
    }
    .main{
      width: 80%;
      background-image: url('https://bizguruh.com/images/curve2.jpg');
      background-size: contain;
      border-radius: 5px;
    }
    a{
      text-decoration: none;
    }
    li{
      padding:15px 0; 
    }
    .welcome-header{
      font-size: 44px;
      text-align: center;
      width: 100%;
      font-family: 'Josefin Sans';
     color: #a4c2db;
    }
    h3{
      font-size: 22px;
    }
    .settle-text{
      font-size: 18px;
      color: hsl(207, 43%, 20%) ;
      text-align: center;
      width: 100%;
    }
    .elevated_btn{
      position:relative;
      max-width: 450px;
      margin-top: 16px;
      margin-right: auto;
      margin-left: auto;
      padding: 20px 30px 18px;
      -webkit-align-self: center;
      -ms-flex-item-align: center;
      -ms-grid-row-align: center;
      align-self: center;
      border: 2px none transparent;
      border-radius: 4px;
      color : #fff;
      box-shadow: 0 8px 14px 0 rgba(5, 17, 26, 0.28);
      -webkit-transition: all 400ms ease;
      transition: all 400ms ease;
      font-family: Sailec, sans-serif;
      background-color: hsl(207, 43%, 20%);
      font-size: 15px;
      line-height: 22px;
      font-weight: 700;
      text-align: center;
      letter-spacing: 1px;
      text-decoration: none;
      text-transform: capitalize;
      display:flex;
      justify-content:space-evenly;
    align-items:center;
  }
  .elevated_btn:hover{
    background-color: #fff !important;
    box-shadow: 0 9px 17px 1px rgba(5, 17, 26, 0.39);
    opacity: 0.92;
    -webkit-transform: translate(0px, -3px);
    -ms-transform: translate(0px, -3px);
    transform: translate(0px, -3px);
    color: hsl(207, 43%, 20%) !important;
  }
  p{
    font-size: 15px;
  }
  .first{
     /* background-color:#f7f8fa; */
    background-image: url('https://bizguruh.com/images/mockups/why.png');
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 40%;
  }
  .second{
    /* background-color:#f7f8fa; */
    background-image: url('https://bizguruh.com/images/account.jpg');
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 40%;
  }
  .intro{
   
  }
  td{
    font-size: 18px;
  }
  .img-contain{
    width: 100%;
    height: 300px;
    margin-bottom: 15px;
  }
  .img-contain img{
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  .bg{
    background-color: hsl(207, 43%, 95%);
    padding:40px 0
  }
    @media (max-width:575px){
      .img-contain{
    width: 100%;
    height: 150px;
  }
      .first{
        padding:10px 0;
      }
      .welcome-header{
        font-size: 18px
      }
      .settle-text{
        font-size: 17px;
      }
      .elevated_btn{
      position:relative;
      max-width: 450px;
      margin-top: 16px;
      margin-right: auto;
      margin-left: auto;
      padding: 15px 20px 13px;
      -webkit-align-self: center;
      -ms-flex-item-align: center;
      -ms-grid-row-align: center;
      align-self: center;
      border: 2px none transparent;
      border-radius: 4px;
      color : #fff;
      box-shadow: 0 8px 14px 0 rgba(5, 17, 26, 0.28);
      -webkit-transition: all 400ms ease;
      transition: all 400ms ease;
      font-family: Sailec, sans-serif;
      background-color: hsl(207, 43%, 20%);
      font-size: 15px;
      line-height: 22px;
      font-weight: 700;
      text-align: center;
      letter-spacing: 1px;
      text-decoration: none;
      text-transform: capitalize;
      display:flex;
      justify-content:space-evenly;
    align-items:center;
  }
  .main{
    width:95%;
  }
    }
  </style>

</head>
<body>
  <div class="bg">


   <table class="main" style="padding:15px;;margin:0 auto;">
   
     <tr>
       <td  class="intro" style="padding:40px;">
           <h1 class="welcome-header">Congratulations!</h1>
           <div style="text-align:center;:0 auto;" class="img-contain">
             <img src="https://bizguruh.com/images/announce.png" alt=""  style="text-align:center;margin:0 auto" srcset="">
           </div>
           <p class="settle-text" style=" font-size: 17px;">You’re now mere weeks away from living the life of your dreams— purposeful, impactful and productive. We are so excited that the world is about to hear you loud and clear, through your business.
             We believe you have signed up because you’re ready now, more than ever, not just to take your business to the next level, but to actually co-create the life you have always desired for yourself, and we’re happy to partner with you on that journey.</p>
       </td>
     </tr>
    <tr>
      <td>
       <table>
         <tr>
            <td style="padding:40px 15px">
               <h2 style="text-align: center">QUICK REALITY CHECK</h2>
               <div style="text-align:center;" class="img-contain">
                 <img src="https://bizguruh.com/images/fail.png" alt=""  style="text-align:center;margin:0 auto" srcset="">
               </div>
               <h4  style="text-align: center">You’re more likely to fail than to succeed!</h4>
              <p style=" font-size: 17px;">
               While it is true that entrepreneurship is being glorified and entrepreneurs are the rock stars of our times, statistics still show that 90% of entrepreneurs will fail— that’s 9 out of 10!
              </p>
              <ol>
                <p style=" font-size: 17px;">As dreary as it sounds, we decided to verify this information and discontained the following:</p>

                <li style=" font-size: 17px;">Many people are what we call ‘necessity entrepreneurs’, which means they did not become entrepreneurs because they wanted to solve a problem, they turned to entrepreneurship due to lack of alternative means of economic sustainability. Then there are the people who were lured by the seeming glamour.</li>
                <strong>Neither is an ideal reason to embark on the entrepreneurial journey.</strong>
                <li style=" font-size: 17px;">There are 41.2 million MSMEs in Nigeria, and they account for at least 84% of employment; wholesale/retail trade represents 42.3 per cent of these activities.</li>
                <strong>This means that almost all commercial activities in Nigeria emanate from the informal sector. A more implied fact is that people are not producing or innovating— they’re mostly selling/hustling.</strong>
              <li style=" font-size: 17px;"> Qualified artisans are readily available in most of the sectors. However, even when they have the skills and access to funds they still fail due to a combination of problems, which include inadequate business start-up knowledge. Also, formal financial institutions do not fund small businesses due to lack of business management systems for monitoring and control.</li>
               <strong style=" font-size: 17px;">Lack of money is not the only reason your business is failing— money cannot replace the lack of knowledge and competence.</strong>
             </ol>
             </td>
          
         </tr>
        
       </table>
      </td>
    </tr>
  
    <tr>
      <td>
        <table>
          <tr>
            <td>
             <tr>
            
               <td style="padding:20px 15px">
                 <h2 style="text-align: center">RELAX, YOU’RE IN GOOD HANDS
                 </h2>
                 <div style="text-align:center;:0 auto;" class="img-contain">
                   <img src="https://bizguruh.com/images/goodhand.png" alt=""  style="text-align:center;margin:0 auto" srcset="">
                 </div>
                 <h4  style="text-align: center">We carefully considered all these and more in developing this program which you are now part of.
                 </h4>
                 <strong>Here’s what to expect from the BAP-6 program:</strong>
                 <ul>
                   <li style=" font-size: 17px;">
                     Renewed self- awareness by which you will be led to tell your own story through the products and services you develop to serve humanity
                   </li>
                   <li style=" font-size: 17px;">A business roadmap that is aligned with your inner-man, and structured to be profitable even whilst serving others.</li>
                   <li style=" font-size: 17px;">Sound understanding of your business-operating environment</li>
                   <li style=" font-size: 17px;">Ability to develop and deploy strategies that best align your resources and capabilities to the requirements of your business operating environment and overall vision</li>
                   <li style=" font-size: 17px;">Implementation and control measures for sustained growth</li>
                 </ul>
                
               </td>
             </tr>
             <tr>
               <p style=" font-size: 17px;">For the duration of this program you will have access to subject matter and industry experts who will work with you as a soundboard-cum-business buddy— THEY ARE ONLY A CHAT BUTTON AWAY. And as you transition into an alumni, we will continue to keep in touch with resources that will reinforce all that you’ve gained.</p>
             </tr>
            
            </td>
          </tr>
        </table>
      </td>
    </tr>
   
    <tr>
      <td>
        <p><strong style=" font-size: 17px;">Welcome once again to the top 10% league. See you at the top!</strong></p>
      </td>
    </tr>
    <tr>
     <td style="text-align: center">
       <a href="https://bizguruh.com/bap">  <button class="elevated_btn">click here to begin your orientation</button>
       </td></a>
   </tr>
   <tr><td>

     <p style=" font-size: 17px;">
       Sincerely, <br>
The Team @BizGuruh
     </p>
     </td></tr>
  
  </table>
  <table class="bg" style=" width:80%;margin:0 auto;padding:20px 0">
    <tr  style="text-align: center">
      <td  style="font-size:12px; color:rgba(0, 0, 0, .54); text-align:center;">
        
          <p style="text-align: center;font-size:12px">Our customer service is run by real people who are committed to seeing you win, so reach out anytime.</p>
          <p  style="text-align: center;font-size:12px">You can contact us  <a href="https://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
            <p  style="text-align: center;font-size:12px">  BizGuruh values your privacy; read our  <a href="https://bizguruh.com/policies">privacy policy</a> and our  <a href="https://bizguruh.com/terms">terms of use</a>.</p>
      </td>
   </tr>
   </table>
   
  </div>
 

</body>
</html>
