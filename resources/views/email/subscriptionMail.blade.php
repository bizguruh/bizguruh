<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/mail.css">

    <title>Subscription</title>


</head>
<body>
    <div class="main" style=" background: #f2f5fe;
    padding: 10px;">
     <h2 style="text-align:center;"><span style="color:#333;font-weight:normal">BIZ</span><span style="color:#a4c2db">GURUH</span></h2>
        <div class="main" style=" width:80%; margin-left:auto;margin-right:auto;  background:#ffffff; padding:10px 20px;border-radius:5px color:rgba(0, 0, 0, .74);">
            <h4>Welcome {{$name['name']}},</h4>
            <p>
                    Congratulations! You now have Full Access to all the essentials you need to build the business of your dreams. We’re glad you’re here, and look forward to supporting you every step of the way in building your dream business.
                    </p>
                    @if ($name['level'] == 1)
                    <p>Now that you’ve got full access to our essential plan, check out some of our great features:</p>

                  <ul>
                        <li> Exclusive BizGuruh Library</li>
                     <li>Access to 15 BizGuruh Insights</li>
                     <li>3 Business Checklist of your choice</li>
                     <li>Email support during office hours</li>

                  </ul>
                    @elseif ($name['level'] == 2)

                    <p>Now that you’ve got full access to our professional plan, check out some of our great features:</p>

                 <ul>
                        <li>  Email support during office hours</li>
                        <li>Exclusive BizGuruh Library</li>
                        <li>Access to BizGuruh Insight</li>
                        <li>500 Access to BizGuruh Question-Answer Bank, (Based on areas of interest)</li>
                        <li>Access to BizGuruh Experts</li>
                        <li>5	Business Checklist/Guide</li>
                 </ul>
                    <p>and so much more ...</p>
                    @else

                    <p>Now that you’ve got full access to our premium plan, check out some of our great features:</p>

                  <ul>
                        <li> 24hrs Email support </li>
                    <li>Exclusive BizGuruh Library</li>
                    <li>Access to BizGuruh Insight</li>
                    <li>Unlimited Access to BizGuruh Question-Answer Bank, (Based on areas of interest)</li>
                    <li>Access to BizGuruh Experts</li>
                    <li>10	Business Checklist/Guide</li>
                  </ul>
                    <p>and so much more ...</p>

                    @endif





                            <div style="text-align:center;padding:20px" >
                                    <button type="button" class="btn btn-primary"  style="background:#a3c2dc; color:white;padding:5px 10px">  <a  style="background:#a3c2dc; color:white; text-decoration:none;padding:5px 10px" href="https://bizguruh.com/">Log back into my account</a></button>

                            </div>
                                    <hr >
                        <p>If you have any questions, feel free to <a href="http://bizguruh.com/contact">contact us</a>.  We wish you best of luck with taking your business to the next level. </p>
                        <address>
                                <p>Yours Sincerely,
                                    </p>
                             <span>  Your BizTribe @ BizGuruh.</span>

                        </address>

                        <hr style="width:95%; text-align:center;margin-left:auto;margin-right:auto;background: rgba(0, 0, 0,0.54);">
                        <p>Please keep the following information for your records:</p>
                       <p> {{$name['name']}},</p>
                        @if ($name['level'] == 1)
                        <p>Discounted 1 year BizGuruh Essentials Plan,</p>
                        @elseif ($name['level'] == 2)
                        <p>Discounted 1 year BizGuruh Professional Plan,</p>
                        @else
                        <p>Discounted 1 year BizGuruh Premium Plan,</p>
                        @endif

                    <table class="table table-dark">
                        <tbody>
                            <tr>
                                <td><b>Join date :</b></td>
                            <td> {{ date_format( date_create($name['created_at']),"F d, Y")   }}</td>
                            </tr>
                            <tr>
                                    <td><b>Order Id :</b></td>
                                    <td>  #{{$name['id']}}</td>
                                </tr>
                                <tr>
                                        <td><b>Order Amount :</b></td>
                                <td> NGN{{$name['price']/100}}.00</td>
                                    </tr>
                        </tbody>
                    </table>

                    <p>Your subscription begins immediately and automatically renews ‪on {{date_format(date_add(date_create($name['startDate']),date_interval_create_from_date_string("1 year")), "F d, Y ")}} to our recurring service at  NGN{{$name['price']/100}}.00 billed annually.</p>
                    <hr style="width:70%; text-align:center;margin-left:auto;margin-right:auto;color: rgba(0, 0, 0,0.54);">
                  <div style="font-size:11px; color:rgba(0, 0, 0, .54); text-align:center">
                        <p>You can cancel and manage your subscription  <a href="http://bizguruh.com/subscription">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com</p>
                    <p>  BizGuruh values your privacy; read our  <a href="http://bizguruh.com/policies">privacy policy</a> and our  <a href="http://bizguruh.com/terms">terms of use</a>.</p>
                  </div>




        </div>



    </div>

</body>
</html>
