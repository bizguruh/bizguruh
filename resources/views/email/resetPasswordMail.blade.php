<!DOCTYPE>
<html>
<head>
    <title>Reset Password</title>
</head>

<body>
        <table border="0"  cellpadding="0" cellspacing="0" width="100%" style="padding: 15px; margin-bottom: 20px; background:#fafafa" >
                <tr>
                  <td>
                    <table  align="center" border="0" cellpadding="0" cellspacing="0" width="100%"  style="border-collapse: separate;padding:15px; border-spacing: 2px 5px; box-shadow: 1px 0 1px 1px #B8B8B8; text-align:center"
                    bgcolor="#FFFFFF">
                    <tr>
                        <td style="text-align:center">
                        <p>You requested for a password request, click on the link to continue,  <a href="https://bizguruh.com/reset-password?confirmation=true&id={{$user}}">Confirm Reset</a>.
                        </td>
                     
                    </tr>
                    <tr>
                        <td>
                                <table align="center" style="text-align:center; margin:0 auto">
                                      
                                        <tr><td>
                                          <hr style="width:70%; text-align:center;margin-left:auto;margin-right:auto;color: rgba(0, 0, 0,0.54);">
                                        </td></tr>
                                        <tr>
                                          <td  style="font-size:11px; color:rgba(0, 0, 0, .54); text-align:center">
                                              <p>You can contact us  <a href="https://bizguruh.com/contact">here</a> or contact our customer service team at <a href="mailto:ask@bizguruh.com">ask@bizguruh.com </a></p>
                                                <p>  BizGuruh values your privacy; read our  <a href="https://bizguruh.com/policies">privacy policy</a> and our  <a href="https://bizguruh.com/terms">terms of use</a>.</p>
                                          </td>
                                        </tr>
                                      </table>
                        </td>
                    </tr>
                     
                    </table>
                  </td>
                </tr>
              </table>
        












</body>

</html>