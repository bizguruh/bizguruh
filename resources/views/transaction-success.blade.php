@extends('layouts.success')

<style>
    .card  {  text-align: center;}
</style>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div>Payment Completed transaction</div>
                <p>Transaction successful</p>
                <a href="/profile/products">View Purchase Product</a>
            </div>
        </div>
    </div>
</div>
@endsection


<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

<script>
    $(document).ready(function(){
       window.location.replace('https://www.bizguruh.com/transaction-successful');
    });
</script>
