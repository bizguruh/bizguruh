export const getHeader = function () {
     const vendorToken = JSON.parse(localStorage.getItem('authVendor'));
     const headers = {
         'Accept': 'application/json',
         'Authorization': 'Bearer ' + vendorToken.access_token
     };
     return headers;
};


export const getOpsHeader = function(){
    const opsToken = JSON.parse(localStorage.getItem('authOps'));
    const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

export const getAdminHeader = function(){
    const adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};


export const getCustomerHeader = function(){
  const customerToken = JSON.parse(localStorage.getItem('authUser'));
  const headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + customerToken.access_token
  };
  return headers;
};
export const getIlcHeader = function(){
    const customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
  };
