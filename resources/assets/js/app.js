/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

import VueRouter from "vue-router";
const Toasted = () => import("vue-toasted");
import { routes } from "./routes";
const BootstrapVue = () => import("bootstrap-vue");
const VuejsDialog = () => import("vuejs-dialog");
import Store from "./vendor/store";
const VueElementLoading = () => import("vue-element-loading");
const VueBootstrapTypeahead = () => import("vue-bootstrap-typeahead");
const Magnify = () => import("magnific-popup");
const Dropdown = () => import("bp-vuejs-dropdown");
import Notifications from "vue-notification";
const VueCarousel = () => import("vue-carousel");
import vueScrollto from "vue-scrollto";
import VueGraph from "vue-graph";
const Affix = () => import("vue-affix");
// const VueVideoPlayer = () => import("vue-video-player");
// import "video.js/dist/video-js.css";
const VueButtonSpinner = () => import("vue-button-spinner");
const VTooltip = () => import("v-tooltip");
import VueCurrencyInput from "vue-currency-input";

import VueAxios from "vue-axios";
import VueSocialauth from "vue-social-auth";
import VueChatScroll from "vue-chat-scroll";
import VueAwesomeSwiper from "vue-awesome-swiper";


Vue.use(VueAwesomeSwiper /* { default options with global component } */);
import "swiper/dist/css/swiper.css";


Vue.use(VueChatScroll);
Vue.config.productionTip = false;

Vue.use(require("vue-moment"));
Vue.use(VueRouter);
Vue.use(VueGraph);
Vue.use(BootstrapVue);
Vue.use(VuejsDialog);
Vue.use(vueScrollto);
Vue.use(Dropdown);
Vue.use(Notifications);
Vue.use(VueCarousel);
Vue.use(Affix);
Vue.use(Magnify);
Vue.use(VTooltip);
Vue.use(VueCurrencyInput);
Vue.use(Toasted, {
    duration: 2500,
    icon: null
    // after: true
});
// Vue.use(VueVideoPlayer);

Vue.use(VueAxios, axios);
Vue.use(VueSocialauth, {
    providers: {
        facebook: {
            clientId: "507727893472256",
            redirectUri: "http://localhost:8000/auth/facebook/callback" // Your client app URL
        },
        google: {
            clientId:
                "141773140646-53f4818stroenh1lsk43kuh4kns6lscd.apps.googleusercontent.com",
            redirectUri: "http://localhost:8000/auth/google/callback" // Your client app URL
        }
    }
});

const router = new VueRouter({
    mode: "history",
    routes,
    scrollBehavior: (to, from, savedPosition) => {
        document.getElementById("app").scrollIntoView();
        return null;
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vendor component
// Vue.component("addProduct", () =>
//     import("./vendor/components/addProductComponent.vue")
// );

Vue.component("vue-loading", VueElementLoading);
Vue.component("vue-bootstrap-typeahead", VueBootstrapTypeahead);
Vue.component("vue-spinner-button", VueButtonSpinner);
Vue.component("chat-messages", require("./user/components/chatMessages.vue"));
Vue.component("chat-form", require("./user/components/chatForms.vue"));

Vue.component("chat-users", require("./user/components/chatuser.vue"));




const app = new Vue({
    el: "#app",
    router,
    Store,

});
