import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import Toasted from 'vue-toasted'


Vue.use(Vuex);
Vue.use(Toasted, {
    duration: 4000,
    icon : null,
    after: true
});

export default new Vuex.Store({

    plugins: [createPersistedState()],
    state: {
        tokens: {
            access_token: null,
            expires_in: null,
            refresh_token: null,
            token_type: null,
            profileImage: ''
        },
        currentUser: {
            id: null,
            storeName: null,
            email: null,
        }
    },
    mutations: {
        getProfileData(state){
            let authVendor = JSON.parse(localStorage.getItem('authVendor'));
            state.tokens.access_token = authVendor.access_token;
            let profilePics = authVendor.storeName;
            axios.get(`api/getvendor/${profilePics}`, { headers: {"Authorization" : `Bearer ${authVendor.access_token}`} })
                .then((response) => {
                    console.log(response);
                }).catch((error) => {
                console.log(error);
            })
        }
    },
    actions: {
        login(context, user){
            return new Promise((resolve, reject)=> {
                let data = {
                    client_id: 2,
                    client_secret: 'UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C',
                    grant_type: 'password',
                    username: user.email,
                    password: user.password,
                    theNewProvider: 'vendor'
                };

                axios.post('/oauth/token', data).then((response) =>{
                
                 resolve(response);
                }).catch((error)=> {
                  //  console.log(new Error('Unable to login'));
                   alert('Invalid user credential');
                })
            });
        }
    }

})

//export default store
