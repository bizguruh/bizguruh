let addVendor = () => import("./vendor/components/addProductComponent");
let registerVendor = () => import("./vendor/components/auth/registerComponent");
let loginVendor = () => import("./vendor/components/auth/loginComponent");
let MainVendor = () => import("./vendor/components/homeTopComponent");
let VendorHome = () => import("./vendor/components/index.vue");
let VendorPricing = () => import("./vendor/components/pricing.vue");
let VendorAbout = () => import("./vendor/components/about.vue");
let DashboardVendor = () =>
    import("./vendor/components/mainVendorPageComponent");
let editVendor = () => import("./vendor/components/editProductComponent");
let VendorStatistics = () =>
    import("./vendor/components/vendorStatisticPageComponent");
let viewVendorProduct = () =>
    import("./vendor/components/viewProductComponent");
let showVendorProduct = () =>
    import("./vendor/components/showProductComponent");
let orderVendor = () => import("./vendor/components/orderVendorComponent");
let messagesVendor = () => import("./vendor/components/vendorMessages");
let VendorPrivateChat = () => import("./vendor/components/vendorPrivateChat");
let VendorGroupChat = () => import("./vendor/components/vendorGroupChat");
let mainVendorAuth = () => import("./vendor/components/auth/mainAuthComponent");
let vendorConfig = () => import("./vendor/components/vendorConfigComponent");
let vendorSettings = () =>
    import("./vendor/components/settingsVendorComponent");
let vendorProfile = () => import("./vendor/components/vendorProfileComponent");
let addCategory = () =>
    import("./vendor/components/addCategoriesVendorComponent");
let editCategory = () =>
    import("./vendor/components/editCategoriesVendorComponent");
let productShippingDetail = () =>
    import("./vendor/components/ProductShippingComponent");
let productEditShippingDetail = () =>
    import("./vendor/components/editProductShippingComponent");
let productDraft = () =>
    import("./vendor/components/viewDraftProductComponent");
let VendorProfilePage = () =>
    import("./vendor/components/mainProfilePageComponent");
let ExpertProfilePage = () =>
    import("./vendor/components/mainForExpertProfileComponent");

let VendorForm = () => import("./vendor/components/vendorFormComponent.vue");

let VendorFormEdit = () => import("./vendor/components/vendorEditForm.vue");

let VendorFormQuestions = () =>
    import("./vendor/components/vendorFormQuestions.vue");

let VendorFormQuestion = () =>
    import("./vendor/components/vendorFormQuestion.vue");

let opsDashboard = () => import("./operation/components/opsDasboardComponent");
let loginOps = () => import("./operation/components/opsAuthComponent");
let viewAllProduct = () =>
    import("./operation/components/viewAllProductComponent");
let showOpsProduct = () =>
    import("./operation/components/opsShowProductComponent");
let allVendorOps = () => import("./operation/components/opsAllVendorComponent");
let getVendorDetail = () =>
    import("./operation/components/opsGetVendorDetailComponent");
let opsProfile = () => import("./operation/components/opsProfileComponent");
let MainOps = () => import("./operation/components/opsMainComponent");

let adminDashboard = () => import("./admin/components/adminDashboardComponent");
let adminOps = () => import("./admin/components/adminCreateOpsComponent");
let adminVendor = () => import("./admin/components/adminVendorComponent");
let adminAuth = () => import("./admin/components/adminAuthComponent");
let adminProduct = () => import("./admin/components/adminProductComponent");
let adminShowProduct = () =>
    import("./admin/components/adminShowProductComponent");
let adminOrderProduct = () => import("./admin/components/adminOrderComponent");
let adminUser = () => import("./admin/components/adminUserComponent");
let adminProductCategory = () =>
    import("./admin/components/adminCategoryComponent");
let adminProductSubcategory = () =>
    import("./admin/components/adminSubCategoryComponent");
let adminMain = () => import("./admin/components/adminMainComponent");
let adminProductSubCategoryBrand = () =>
    import("./admin/components/adminSubCategoryBrandComponent");
let adminVendorDetail = () =>
    import("./admin/components/adminVendorDetailComponent");
let adminManage = () => import("./admin/components/adminManageComponent");
let adminReviews = () => import("./admin/components/adminReviewsComponent");
let adminUniverifyProduct = () =>
    import("./admin/components/adminUniverifyProductComponent");
let bizguruhProduct = () =>
    import("./admin/components/adminBizguruhProductComponent");
let adminProductAddCategory = () =>
    import("./admin/components/adminAddProductCategoryComponent");
let adminAddProduct = () =>
    import("./admin/components/adminAddProductComponent");
let adminDraftProduct = () =>
    import("./admin/components/adminDraftProductComponent");
let adminShowBizguruhProduct = () =>
    import("./admin/components/adminShowBizguruhComponent");
let adminProductshowSave = () =>
    import("./admin/components/showAdminProductComponent");
let adminProductEditcategory = () =>
    import("./admin/components/adminEditProductCategoryComponent");
let adminEditProduct = () =>
    import("./admin/components/adminEditProductComponent");
let SubscriptionPage = () =>
    import("./admin/components/adminSubscriptionComponent");
let adminQuestionBank = () =>
    import("./admin/components/adminQuestionBankComponent");
let adminProductType = () =>
    import("./admin/components/adminProductTypeComponent");
let checklistPage = () => import("./admin/components/checklistPageComponent");
let editChecklistPage = () => import("./admin/components/editChecklist");
let ChecklistQuestion = () =>
    import("./admin/components/checklistquestionComponent");

// import HomePage  from  './user/userHomePageComponent.vue';
let HomePage = () => import("./user/userHomePageComponent.vue");
// import IndexPage from  './user/indexComponent';
// import UserAuth from   './user/userAuthComponent';
// import InsightPage from   './user/userInsightComponent';
let InsightPage = () => import("./user/userInsightComponent");
let IndexPage = () => import("./user/indexComponent");
let ProductDetail = () => import("./user/productDetailComponent");
let AllProduct = () => import("./user/userAllProductComponent");
let UserAuth = () => import("./user/userAuthComponent");
let SocialAuth = () => import("./user/socialAuthenticate.vue");
let ProductCategoryPage = () => import("./user/userProductCategoryComponent");
let userCategoryF = () => import("./user/userCategoryFComponent");
let userSubcategoryF = () => import("./user/userSubCategoryFComponent");
let userTopic = () => import("./user/userTopicFComponent");
let userCart = () => import("./user/userCartComponent");
let userCheckout = () => import("./user/userCheckoutComponent");
let BrowseAll = () => import("./user/browserAllComponent");
let AboutUs = () => import("./user/aboutUsComponent");
let Privacy = () => import("./user/privacyComponent");
let Wishlist = () => import("./user/userWishlistComponent");
let Profile = () => import("./user/userProfilePageComponent");
let SuccessMessage = () => import("./user/userSuccessComponent");
let OrderDetail = () => import("./user/userOrderDetailComponent");
let ProductBook = () => import("./user/userBookComponent");
let ProductVideo = () => import("./user/userVideoComponent");
let ProductCourse = () => import("./user/userCourseComponent");
let ProductCourseDetail = () => import("./user/userFullCourseComponent");
let ProductPodcast = () => import("./user/userPodcastComponent");
let ProductPaper = () => import("./user/userWhitePaperComponent");
let ProductPaperDetail = () => import("./user/userPaperDetailComponent");
let ProductJournal = () => import("./user/userJournalComponent");
let MaintenanceMode = () => import("./user/userMaintenanceComponent");
let NewSuccess = () => import("./user/userNewSuccessPageComponent");
let EventsPage = () => import("./user/userEventsComponent");
// let InsightPage  = ()=> import(    './user/userInsightComponent');
let EventSinglePage = () => import("./user/userEventPageComponent");
let ArticlesPages = () => import("./user/userArticlesComponent");
let ArticleSinglePage = () => import("./user/userArticlePageComponent");
let AdminBookPage = () => import("./user/userAdminBookComponent");
let WhitePaperAdmin = () => import("./user/userAdminWhitePaperComponent");
let VideoPage = () => import("./user/userAdminVideoComponent");

let Video = () => import("./user/videoComponent");

let PodcastPage = () => import("./user/userAdminPodcastComponent");
let SubscriptionForUser = () => import("./user/userSubscriptionComponent");
let ReadOnline = () => import("./user/userViewReadOnlineComponent");
let JournalPage = () => import("./user/userAdminJournalComponent");
let CoursesPage = () => import("./user/userAdminCourseComponent");
let CourseFullPage = () => import("./user/userAdminFullCourseComponent");
let MainProfilePage = () => import("./user/userMainProfilePageComponent");
let WatchMedia = () => import("./user/watchMediaComponent");
let SubscriptionProfile = () =>
    import("./user/userProfileSubscriptionComponent");
let UserPreference = () => import("./user/userPreferenceComponent");
let ShippingForUser = () => import("./user/userShippingInformationComponent");
let ArticleFullPage = () => import("./user/userArticleFullComponent");
let BizguruhQuestionBank = () => import("./user/bizguruhQuestionComponent");
let FAQsShow = () => import("./user/userFAQsShowComponent");
let Community = () => import("./user/components/communityComponent");
let PartnerProfile = () =>
    import("./user/components/userPartnerProfileComponent.vue");
let PartnersProfile = () =>
    import("./user/components/partnerProfileComponent.vue");
let PrivacyPolicy = () => import("./user/components/privacyPolicyComponent");
let Guideline = () => import("./user/components/guidelineComponent");
let UpdateProfile = () =>
    import("./user/components/userUpdateProfileComponent");
let TermsAndCondition = () =>
    import("./user/components/termsAndConditionComponent");
let Contact = () => import("./user/components/contactComponent");
let SubjectMatters = () => import("./user/subjectMattersComponent");

let Assessment = () => import("./user/assessments.vue");

let Industry = () => import("./user/userIndustryComponent");
let Topic = () => import("./user/userTopicComponent");
let SpecificIndustry = () => import("./user/specificIndustryComponent");
let Checklist = () => import("./user/checklistComponent");
let Preference = () => import("./user/preferenceComponent");
let Mail = () => import("./user/mailComponent");

let IndexFooter = () => import("./user/components/indexFooter.vue");
let HomeSearch = () => import("./user/components/homeSearchComponent.vue");
let Sponsor = () => import("./user/components/sponsorForm.vue");
let ArticleForm = () => import("./user/components/userArticlesForm.vue");
let ResetPassword = () =>
    import("./user/components/resetPasswordComponent.vue");
let Bap = () => import("./user/bap/bapComponent.vue");
let TemplatesBap = () => import("./user/bap/templates.vue");
let MyBapTemplate = () => import("./user/bap/bapTemplate.vue");
let BapReview = () => import("./user/bap/review.vue");
let BapDashboard = () => import("./user/bap/home.vue");
let BusinessValues = () => import("./user/bap/businessValues.vue");
let BusinessVision = () => import("./user/bap/businessVision.vue");
let BusinessMission = () => import("./user/bap/businessMission.vue");
let BusinessMarketing = () => import("./user/bap/marketing.vue"); 
let BusinessSales = () => import("./user/bap/sales.vue");

let LoginAccounting = () => import("./user/account/auth.vue");

let PrivateChat = () => import("./user/components/privateChat.vue");

let GroupChat = () => import("./user/components/groupChat.vue");
let AccountHome = () => import("./user/account/home.vue");

let AccountIndex = () => import("./user/account/index.vue");

let Accounting = () => import("./user/account/accountingComponent.vue");

let AccountingDemo = () => import("./user/account/accountingDemoComponent.vue");

let AccountPricing = () => import("./user/account/pricing.vue");

let AccountAbout = () => import("./user/account/about.vue");

let Diagnostics = () => import("./user/components/diagnosticsComponents");
let ListDiagnostics = () => import("./user/listDiagnosticsComponents");
let selfDiscovery = () => import("./user/selfDiscovery");
let Guides = () => import("./user/guidesComponent");
let GuidesHistory = () => import("./user/guidesHistoryComponent");
let Discovery = () =>
    import(/* webpackChunkName: "Discovery" */ "./user/discoveryComponent");
let BrightIdea = () =>
    import(/* webpackChunkName: "BrightIdea" */ "./user/brightIdea");
let BusinessConcept = () =>
    import(/* webpackChunkName: "BusinessConcept" */ "./user/bap/businessConcept");
    let BusinessEntrepreneur = () =>
    import(/* webpackChunkName: "BusinessConcept" */ "./user/bap/entrepreneur");
let BusinessModel = () =>
    import(/* webpackChunkName: "BusinessModel" */ "./user/businessModel");
let BusinessEducation = () =>
    import(
        /* webpackChunkName: "BusinessEducation" */ "./user/businessEducation"
    );
let LibraryContent = () =>
    import(/* webpackChunkName: "LibraryContent" */ "./user/libraryContent");

let Ilc = () => import(/* webpackChunkName: "ilc" */ "./ilcuser");
let Classroom = () =>
    import(/* webpackChunkName: "classroom" */ "./ilcuser/classroom.vue");
let ClassroomCourse = () =>
    import(/* webpackChunkName: "classroom" */ "./ilcuser/ilcCourse.vue");
let Landing = () =>
    import(/* webpackChunkName: "landing" */ "./landing/index.vue");

let LandingAuth = () =>
    import(/* webpackChunkName: "landing" */ "./landing/auth.vue");

let AskMobile = () => import("./ask-bizguruh-mobile.vue");

let LoginForm = () => import("./user/components/loginForm.vue");

let TestCron = () => import("./user/components/clearExpensesComponent.vue");

let Chat = () => import("./user/components/chat.vue");
let AfterRegister = () => import("./user/components/afterRegister.vue");
let Form = () => import("./user/form/formComponent.vue");
let FormQuestion = () => import("./user/form/formQuestion.vue");
let FormQuestions = () => import("./user/form/formQuestions.vue");
let EditForm = () => import("./user/form/editForm.vue");

export let routes = [
    { path: "/", component: Landing, name: "Landing" },
    {
        path: "/welcome/auth/:type",
        component: LandingAuth,
        name: "LandingAuth"
    },
    { path: "*", redirect: "/" },
    { path: "/maintenance", component: MaintenanceMode },
    {
        path: "/transaction-successful",
        component: SuccessMessage,
        name: "SuccessMessage"
    },

    {
        path: "/entrepreneur",
        component: HomePage,

        meta: {
            breadcrumb: [{ name: "Home" }]
        },
        children: [
            {
                path: "",
                component: IndexPage,
                name: "IndexPage",
                meta: {
                    breadcrumb: [{ name: "Home" }],
                    entrepreneur:true
                }
            },
            {
                path: "product-details/:id/:productname",
                component: ProductDetail,
                name: "productDetail",
                meta:{
                    entrepreneur:true
                }
            },
            { path: "all-products", component: AllProduct , meta:{
                entrepreneur:true
            }},
            {
                path: "/auth/:name",
                component: UserAuth,
                name: "auth",
                meta:{
                    entrepreneur:true
                },
                beforeEnter: (to, from, next) => {
                    let user = JSON.parse(localStorage.getItem("authUser"));
                    if (user !== null) {
                        next("/");
                    } else {
                        next();
                    }
                }
            },
            {
                path: "/choose",
                name: "AfterRegister",
                component: AfterRegister,
                meta:{
                    entrepreneur:true
                }
            },
            {
                path: "assessment/:id",
                name: "Assessment",
                component: Assessment,
                meta:{
                    entrepreneur:true
                }
            },
            { path: "form", component: Form ,
            meta:{
                entrepreneur:true
            }},

            { path: "form-questions", component: FormQuestions },
            { path: "edit-form/:id", component: EditForm },
            { path: "social", component: SocialAuth, name: "SocialAuth" },
            {
                path: "auth/:provider/callback",
                component: {
                    template: '<div class="auth-component"></div>'
                }
            },
            { path: "reg/login", component: LoginForm, name: "LoginForm" },
            { path: "product-category", component: ProductCategoryPage },
            {
                path: "products/category/:id/:productname",
                component: userCategoryF,
                name: "userCategoryF"
            },
            {
                path: "products/subcategory/:id/:productname",
                component: userSubcategoryF,
                name: "userSubcategoryF"
            },
            {
                path: "products/topic/:id/:productname",
                component: userTopic,
                name: "userTopic"
            },
            {
                path: "products/cart",
                component: userCart,
                name: "userCart",
                meta: {
                    breadcrumb: [{ name: "Home", link: "/" }, { name: "Cart" }]
                }
            },
            {
                path: "product/checkout",
                component: userCheckout,
                name: "userCheckout",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Cart", link: "/product/cart" },
                        { name: "Checkout" }
                    ]
                }
            },
            {
                path: "browse-all/:name",
                component: BrowseAll,
                name: "browse-all"
            },
            {
                path: "about-us",
                component: AboutUs,
                name: "About-us",
                meta: {
                    breadcrumb: [{ name: "Home", link: "/" }, { name: "About" }]
                }
            },
            {
                path: "privacy",
                component: Privacy,
                name: "Privacy",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Privacy" }
                    ]
                }
            },
            {
                path: "wishlist",
                component: Wishlist,
                name: "Wishlist",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Wishlist" }
                    ]
                }
            },
            {
                path: "private-chat/:id",
                component: PrivateChat,
                name: "PrivateChat"
            },
            {
                path: "group-chat/:id",
                component: GroupChat,
                name: "GroupChat"
            },
            {
                path: "chat",
                component: Chat,
                name: "chat"
            },
            {
                path: "update-profile",
                component: UpdateProfile,
                name: "UpdateProfile",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "UpdateProfile" }
                    ]
                }
            },
            {
                path: "profile/products/:name",
                component: Profile,
                name: "Profile",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Profile", link: "/main-profile" },
                        { name: "Library" }
                    ]
                }
            },
            {
                path: "order-details/:id/:vid/:type/:title",
                component: OrderDetail,
                name: "OrderDetail",
                meta: {
                    breadcrumb: [
                        {
                            name: "Home",
                            link: "/"
                        },
                        { name: "Profile", link: "/main-profile" },
                        {
                            name: "Library",
                            link: "/profile/products/subscription"
                        },
                        { name: "Content" }
                    ]
                }
            },

            {
                path: "product-books",
                component: ProductBook,
                name: "ProductBook"
            },
            {
                path: "product-videos",
                component: ProductVideo,
                name: "ProductVideo"
            },
            {
                path: "product-courses",
                component: ProductCourse,
                name: "ProductCourse"
            },
            {
                path: "product-course/:id/course-detail",
                component: ProductCourseDetail,
                name: "ProductCourseDetail"
            },
            {
                path: "product-podcasts",
                component: ProductPodcast,
                name: "ProductPodcast"
            },
            {
                path: "product-paper/:name",
                component: ProductPaper,
                name: "ProductPaper"
            },
            {
                path: "product-paper/:id/:name/:type",
                component: ProductPaperDetail,
                name: "ProductPaperDetail"
            },
            {
                path: "product-journals",
                component: ProductJournal,
                name: "ProductJournal"
            },
            { path: "success", component: NewSuccess, name: "NewSuccess" },
            { path: "events", component: EventsPage, name: "EventsPage" },
            {
                path: "explore",
                component: InsightPage,
                name: "InsightPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore" }
                    ]
                }
            },
            {
                path: "event/:id/:name",
                component: EventSinglePage,
                name: "EventSinglePage"
            },
            {
                path: "articles",
                component: ArticlesPages,
                name: "ArticlesPages",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Articles" }
                    ]
                }
            },
            {
                path: "article-page/:id/:name",
                component: ArticleSinglePage,
                name: "ArticleSinglePage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Articles", link: "/articles" },
                        { name: "Article" }
                    ]
                }
            },
            { path: "books", component: AdminBookPage, name: "AdminBookPage" },
            {
                path: "paper/:name",
                component: WhitePaperAdmin,
                name: "WhitePaperAdmin",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Books" }
                    ]
                }
            },
            {
                path: "videos",
                component: VideoPage,
                name: "VideoPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Videos" }
                    ]
                }
            },

            {
                path: "video/:id",
                component: Video,
                name: "Video",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Videos", link: "/videos" },
                        { name: "Video" }
                    ]
                }
            },

            {
                path: "podcast",
                component: PodcastPage,
                name: "PodcastPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Podcast" }
                    ]
                }
            },
            { path: "journals", component: JournalPage, name: "JournalPage" },
            {
                path: "subscription",
                component: SubscriptionForUser,
                name: "SubscriptionForUser"
            },
            {
                path: "courses",
                component: CoursesPage,
                name: "CoursesPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Courses" }
                    ]
                }
            },
            {
                path: "courses/:id/:name",
                component: CourseFullPage,
                name: "CourseFullPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Courses", link: "/courses" },
                        { name: "Course" }
                    ]
                }
            },

            {
                path: "readonly/:id/:prodtype/:gettype",
                component: ReadOnline,
                name: "ReadOnline"
            },
            {
                path: "main-profile",
                component: MainProfilePage,
                name: "MainProfilePage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Profile" }
                    ]
                }
            },
            {
                path: "stream-media/:id/:prodtype/:gettype/:type/:vid",
                component: WatchMedia,
                name: "WatchMedia"
            },
            {
                path: "user-subscription/:level",
                component: SubscriptionProfile,
                name: "SubscriptionProfile",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Profile", link: "/main-profile" },
                        { name: "Subscription" }
                    ]
                }
            },
            {
                path: "preferences",
                component: UserPreference,
                name: "UserPreference",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Profile", link: "/main-profile" },
                        { name: "Preference" }
                    ]
                }
            },
            {
                path: "user-shipping",
                component: ShippingForUser,
                name: "ShippingForUser"
            },
            {
                path: "article-full/:type/:name",
                component: ArticleFullPage,
                name: "ArticleFullPage",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Articles", link: "/articles" },
                        { name: "Article" }
                    ]
                }
            },
            {
                path: "ask-bizguruh",
                component: BizguruhQuestionBank,
                name: "BizguruhQuestionBank",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Ask Bizguruh" }
                    ]
                }
            },
            {
                path: "faqs-show/:id/:name/question",
                component: FAQsShow,
                name: "FAQsShow"
            },
            {
                path: "community",
                component: Community,
                name: "Community",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Community" }
                    ]
                }
            },
            {
                path: "partner-profile/:username",
                component: PartnerProfile,
                name: "PartnerProfile"
            },
            {
                path: "partner/:username",
                component: PartnersProfile,
                name: "PartnersProfile"
            },

            {
                path: "policies",
                component: PrivacyPolicy,
                name: "PrivacyPolicy",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Policy" }
                    ]
                }
            },
            {
                path: "guidelines",
                component: Guideline,
                name: "Guideline",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Guideline" }
                    ]
                }
            },
            {
                path: "terms",
                component: TermsAndCondition,
                name: "TermsAndCondition",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Terms and conditions" }
                    ]
                }
            },
            {
                path: "contact",
                component: Contact,
                name: "Contact",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Contact us" }
                    ]
                }
            },
            {
                path: "subject-matters/:id/:name",
                component: SubjectMatters,
                name: "SubjectMatters",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Explore", link: "/explore" },
                        { name: "Content" }
                    ]
                }
            },

            { path: "industry/:id", component: Industry, name: "Industry" },
            {
                path: "single-industry/:id/:name",
                component: SpecificIndustry,
                name: "SpecificIndustry"
            },
            { path: "topic/:id", component: Topic, name: "Topic" },
            { path: "guides/:id", component: Checklist, name: "Checklist" },

            { path: "preference", component: Preference, name: "Preference" },
            { path: "mail", component: Mail, name: "Mail" },
            {
                path: "index-footer",
                component: IndexFooter,
                name: "IndexFooter"
            },
            {
                path: "home-search/:name",
                component: HomeSearch,
                name: "HomeSearch",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Search" }
                    ]
                }
            },
            {
                path: "sponsor",
                component: Sponsor,
                name: "Sponsor",
                meta: {
                    breadcrumb: [
                        { name: "Home", link: "/" },
                        { name: "Sponsor" }
                    ]
                }
            },
            {
                path: "create-article",
                component: ArticleForm,
                name: "ArticleForm"
            },
            {
                path: "reset-password",
                component: ResetPassword,
                name: "ResetPassword"
            },
            {
                path: "bap",
                component: Bap,
             
                children: [
                    {
                        path: "",
                        component: BapDashboard,
                        name: "BapDashboard"
                    },
                    
                    {
                        path: "review",
                        component: BapReview,
                        name: "BapReview"
                    },
                    {
                        path: "my-templates",
                        component: MyBapTemplate,
                        name: "MyBapTemplate"
                    },
                    {
                        path: "templates",
                        component: TemplatesBap,
                        name: "TemplatesBap"
                    },
                    {
                        path: "diagnostics/:id",
                        component: Diagnostics,
                        name: "Diagnostics"
                    },
                    {
                        path: "list-diagnostics/:area",
                        component: ListDiagnostics,
                        name: "ListDiagnostics"
                    },
                    {
                        path: "test-cron",
                        component: TestCron,
                        name: "TestCron"
                    },
                    {
                        path: "values",
                        component: BusinessValues,
                        name: "BusinessValues"
                    },
                    {
                        path: "mission",
                        component: BusinessMission,
                        name: "BusinessMission"
                    },
                    {
                        path: "vision",
                        component: BusinessVision,
                        name: "BusinessVision"
                    },
                    {
                        path: "self-discovery",
                        component: selfDiscovery,
                        name: "selfDiscovery"
                    },
                    {
                        path: "guides-history",
                        component: GuidesHistory,
                        name: "GuidesHistory"
                    },
                    { path: "form-question/:id", component: FormQuestion },
                    { path: "guides", component: Guides, name: "Guides" },
                    {
                        path: "discovery",
                        component: Discovery,
                        name: "Discovery"
                    },
                    { path: "idea", component: BrightIdea, name: "BrightIdea" },
                    {
                        path: "concept",
                        component: BusinessConcept,
                        name: "BusinessConcept"
                    },
                    {
                        path: "entrepreneur",
                        component: BusinessEntrepreneur,
                        name: "BusinessEntrepreneur"
                    },
                    {
                        path: "marketing/:name",
                        component: BusinessMarketing,
                        name: "BusinessMarketing"
                    },
                    {
                        path: "sales/:name",
                        component: BusinessSales,
                        name: "BusinessSales"
                    },
                    {
                        path: "model",
                        component: BusinessModel,
                        name: "BusinessModel"
                    },
                    {
                        path: "business/education",
                        component: BusinessEducation,
                        name: "BusinessEducation"
                    }
                ]
            },

            {
                path: "account-demo",
                component: AccountingDemo,
                name: "AccountingDemo"
            },

            {
                path: "library/:id/:vid/:type/:title",
                component: LibraryContent,
                name: "LibraryContent"
            }
        ]
    },

    { path: "/ilc", component: Ilc, name: "Ilc" },
    { path: "/classroom", component: Classroom, name: "Classroom" },
    {
        path: "/classroom/course/:id/:name",
        component: ClassroomCourse,
        name: "ClassroomCourse"
    },
    {
        path: "/account/auth",
        component: LoginAccounting,
        name: "LoginAccounting"
    },

    {
        path: "/account",
        component: AccountHome,
     
        children: [
            {
                path: "",
                component: AccountIndex,
                name: "AccountIndex",
                meta:{
                    accounting:true
                }
            },
            {
                path: "pricing",
                component: AccountPricing,
                name: "AccountPricing",
                meta:{
                    accounting:true
                }
            },
            {
                path: "about",
                component: AccountAbout,
                name: "AccountAbout",
                meta:{
                    accounting:true
                }
            }
        ]
    },

    { path: "/account/dashboard", component: Accounting, name: "Accounting" },
    { path: "/vendor/auth", component: mainVendorAuth , meta:{
        vendor:true
    }},
    { path: "/vendor/register", component: registerVendor , meta:{
        vendor:true
    }},
    { path: "/vendor/login", component: loginVendor , meta:{
        vendor:true
    }},
    { path: "/vendor/home", component: VendorHome, name: "VendorHome" , meta:{
        vendor:true
    }},
    {
        path: "/vendor/pricing",
        component: VendorPricing,
        name: "VendorPricing",
        meta:{
            vendor:true
        }
    },
    {
        path: "/vendor",
        component: MainVendor,
        children: [
            {
                path: "dashboard",
                component: DashboardVendor,
                name: "dashboard",
                meta:{
                    vendor:true
                }
            },
          
            { path: "about", component: VendorAbout, name: "VendorAbout", meta:{
                vendor:true
            } },
        
            {
                path: "statistics",
                component: VendorStatistics,
                name: "VendorStatistics",
                meta:{
                    vendor:true
                }
            },
            { path: ":id/edit", component: editVendor, name: "editVendor" , meta:{
                vendor:true
            }},
            {
                path: "all",
                component: viewVendorProduct,
                name: "viewVendorProduct"
            },
            {
                path: "product/:id",
                component: showVendorProduct,
                name: "showVendorProduct"
            },
            { path: "add/product/:id/:name", component: addVendor },
            { path: "user", component: vendorConfig, name: "vendorConfig" },
            {
                path: "user/:profile",
                component: vendorProfile,
                name: "vendorProfile"
            },
            { path: "/vendor-form", component: VendorForm, name: "VendorForm", meta:{
                vendor:true
            } },
            {
                path: "/vendor-form-questions",
                component: VendorFormQuestions,
                name: "VendorFormQuestions",
                meta:{
                    vendor:true
                }
            },
            {
                path: "/vendor-form-question/:id",
                component: VendorFormQuestion,
                name: "VendorFormQuestion"
            },
            {
                path: "/vendor-form-edit/:id",
                component: VendorFormEdit,
                name: "VendorFormEdit"
            },
            { path: "order/all", component: orderVendor, name: "orderVendor" },
            {
                path: "vendor/messages",
                component: messagesVendor,
                name: "messagesVendor",
                meta:{
                    vendor:true
                }
            },
            {
                path: "vendor-private-chat",
                component: VendorPrivateChat,
                name: "VendorPrivateChat",
                meta:{
                    vendor:true
                }
            },
            {
                path: "vendor-group-chat",
                component: VendorGroupChat,
                name: "VendorGroupChat"
            },
            { path: "user/settings", component: vendorSettings },
            { path: "add/category", component: addCategory },
            {
                path: "edit/category/:id",
                component: editCategory,
                name: "editVendorProduct"
            },
            {
                path: "vendor/shipping-detail",
                component: productShippingDetail,
                name: "shipVendorProduct"
            },
            {
                path: "edit/shipping/:id",
                component: productEditShippingDetail,
                name: "productEditShippingDetail",
                meta:{
                    vendor:true
                }
            },
            {
                path: "all/draft-product",
                component: productDraft,
                name: "productDraft"
            },
            {
                path: "main-profile",
                component: VendorProfilePage,
                name: "VendorProfilePage",
                meta:{
                    vendor:true
                }
            },
            {
                path: "expert-products",
                component: ExpertProfilePage,
                name: "ExpertProfilePage"
            }
        ]
    },

    { path: "/ops/login", component: loginOps },
    {
        path: "/ops",
        component: MainOps,
        children: [
            { path: "dashboard", component: opsDashboard },
            { path: "all-product", component: viewAllProduct },
            {
                path: "product/:id",
                component: showOpsProduct,
                name: "showOpsProduct"
            },
            { path: "all/vendor", component: allVendorOps },
            {
                path: "vendor/detail/:id",
                component: getVendorDetail,
                name: "getVendorDetail"
            },
            { path: "profile/:id", component: opsProfile, name: "opsProfile" }
        ]
    },

    { path: "/admin/auth/manage", component: adminAuth },
    {
        path: "/admin",
        component: adminMain,
        children: [
            { path: "dashboard", component: adminDashboard },
            { path: "ops", component: adminOps },
            { path: "vendors", component: adminVendor },
            { path: "products", component: adminProduct },
            {
                path: "product/:id",
                component: adminShowProduct,
                name: "adminShowProduct"
            },
            { path: "orders", component: adminOrderProduct },
            { path: "users", component: adminUser },
            { path: "category", component: adminProductCategory },
            {
                path: "subcategory/:id",
                component: adminProductSubcategory,
                name: "adminProductSubcategory"
            },
            {
                path: "subcategory-brand/:id",
                component: adminProductSubCategoryBrand,
                name: "adminProductSubCategoryBrand"
            },
            {
                path: "vendors/details/:id",
                component: adminVendorDetail,
                name: "adminVendorDetail"
            },
            {
                path: "manage/admin",
                component: adminManage,
                name: "adminManage"
            },
            { path: "reviews", component: adminReviews, name: "adminReviews" },
            {
                path: "univerify-products",
                component: adminUniverifyProduct,
                name: "adminUniverifyProduct"
            },
            {
                path: "bizguruh-products",
                component: bizguruhProduct,
                name: "bizguruhProduct"
            },
            {
                path: "add/product/category",
                component: adminProductAddCategory,
                name: "adminProductAddCategory"
            },
            {
                path: "add/product/:id/:name",
                component: adminAddProduct,
                name: "adminAddProduct"
            },
            {
                path: "draft-product",
                component: adminDraftProduct,
                name: "adminDraftProduct"
            },
            {
                path: "show-bizguruh-product/:id",
                component: adminShowBizguruhProduct,
                name: "adminShowBizguruhProduct"
            },
            {
                path: "show-prodct/:id",
                component: adminProductshowSave,
                name: "adminProductshowSave"
            },
            {
                path: "edit/product/category/:id",
                component: adminProductEditcategory,
                name: "adminProductEditcategory"
            },
            {
                path: "edit/product/:id/edit",
                component: adminEditProduct,
                name: "adminEditProduct"
            },
            {
                path: "subscription-plans/",
                component: SubscriptionPage,
                name: "SubscriptionPage"
            },
            {
                path: "questions-bank",
                component: adminQuestionBank,
                name: "adminQuestionBank"
            },
            {
                path: "product-type",
                component: adminProductType,
                name: "adminProductType"
            },
            {
                path: "checklist-page",
                component: checklistPage,
                name: "checklistPage"
            },
            {
                path: "edit-checklist/:id",
                component: editChecklistPage,
                name: "editChecklistPage"
            },
            {
                path: "questions",
                component: ChecklistQuestion,
                name: "ChecklistQuestion"
            }
        ]
    }
];
