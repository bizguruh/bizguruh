<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');*/

//Route::get('/', 'ProductController@getHome');

Route::get('/', function(){
   return view('welcome');
});


//Route::get('/userss', 'ProductController@getVendorProduct');

Route::get('/vendoruser/verify/{token}', 'VendorUserController@verifyVendor');

Route::get('/adminuser/verify/{token}', 'AdminUserController@verifyAdmin' );

Route::get('/opsuser/verify/{token}', 'AdminUserController@verifyOps');
Auth::routes();


/*Route::get('/transaction-successful', function(){
   // return view('transaction-success');
    $res = request('trxref');
    return redirect('http://www.bizguruh.com/transaction/?trxref=' . $res);

});*/

Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');

/*Route::get('/#', function(){
    return view('welcome');
})->where('any', '.*');*/

Route::post('sociallogin/{provider}', 'SocialController@SocialSignup');
Route::post('auth/{provider}', 'OutController@index')->where('vue', '.*');
Route::post('auth/{provider}/callback', 'OutController@index')->where('vue', '.*');


Route::get('/chat', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');