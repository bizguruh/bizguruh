<?php

use App\Events\OnlineUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    // broadcast(new OnlineUsers($request->user()))->toOthers(); 
    return $request->user();
});

Route::middleware('auth:api')->resource('cart', 'CartController');


Route::middleware('auth:api')->get('user-order-product/{id}/{vid}/{type}', 'OrderDetailController@getOrderProduct');

Route::middleware('auth:api')->post('user/changePassword', 'UserDetailController@changeUserPassword');

Route::middleware('auth:api')->post('get/articles/user', 'ArticleProductController@getSingleArticle');

Route::middleware('auth:api')->get('no-cart/{cart}', 'CartController@getCartNo');

Route::middleware('auth:api')->get('user/preference/articles', 'ArticleProductController@getArticleBasedOnUserPreferences');

Route::middleware('auth:api')->post('user/subscribe/download', 'UserSubscriptionPlanController@getItemToDownload');

Route::middleware('auth:api')->get('user/subscription-plans', 'UserSubscriptionPlanController@getSubscriptionPlanForUser');

Route::middleware('auth:api')->post('user/watchnowepisodemedia', 'UserSubscriptionPlanController@watchEpisodeMedia');

Route::middleware('auth:api')->post('user/sendvideotolibrary', 'UserSubscriptionPlanController@addVideoToLibrary');

Route::middleware('auth:api')->post('user/watchnowmedia', 'UserSubscriptionPlanController@watchNowMedia');

Route::middleware('auth:api')->post('user/streammedianow', 'UserSubscriptionPlanController@streamMedia');

Route::middleware('auth:api')->post('user/sendvideoepisodetolibrary', 'UserSubscriptionPlanController@addVideoEpisodeToLibrary');

Route::middleware('auth:api')->post('user/sendcoursetolibrary', 'UserSubscriptionPlanController@sendCourseToLibrary');

Route::middleware('auth:api')->get('user/get/subjectmatter', 'ProductCategoryController@bizguruhMatter');

Route::middleware('auth:api')->get('user/get/category', 'ProductCategoryController@bizguruhCategory');

Route::middleware('auth:api')->get('user/get/producttopic', 'ProductSubCategoryBrandController@bizguruhTopic');

Route::middleware('auth:api')->post('user/sendtolibrary', 'UserSubscriptionPlanController@sendItemToLibrary');

Route::middleware('auth:api')->resource('usersubscription', 'UserSubscriptionPlanController');

Route::middleware('auth:api')->resource('user-detail', 'UserDetailController');

Route::middleware('auth:api')->resource('user-preference', 'UserPreferenceController');

Route::middleware('auth:api')->resource('user-following', 'UserFollowingController');



Route::middleware('auth:api')->get('user-details/{detail}', 'UserDetailController@getUserDetailById');

Route::middleware('auth:api')->resource('order', 'OrderController');
Route::middleware('auth:api')->get('wishlists', 'WishlistController@getWishlistByUserId');

Route::middleware('auth:api')->get('wish-list/{list}', 'WishlistController@checkWishlistByUserId');

Route::middleware('auth:api')->resource('wishlist', 'WishlistController');

Route::middleware('auth:api')->get('user/orders', 'OrderController@getUserOrderById');
Route::middleware('auth:api')->get('user/order/{id}', 'OrderController@checkUserOrderById');

Route::middleware('auth:api')->get('user/topic', 'ProductSubCategoryBrandController@filterSubCategoryTopic');

Route::get('user/verify-order/{reference}/{type}/{id}', 'OrderController@verifyTransaction');

Route::middleware('auth:api')->get('articles/network', 'ArticleProductController@getArticleFromNetwork');

Route::middleware('auth:api')->post('user/question-send', 'UserDetailController@sendUserQuestion');


Route::middleware('auth:api')->post('user/reviews', 'ReviewController@userSaveReview');

Route::middleware('auth:api')->get('user/profile', 'UserDetailController@getUserDetailFromProfile');
Route::middleware('auth:api')->post('save-responses', 'BapController@saveResponses');
Route::middleware('auth:api')->get('get-template/{name}', 'BapController@getTemplate');
Route::middleware('auth:api')->get('get-template-history/{id}', 'BapController@getTemplateById');
Route::middleware('auth:api')->get('get-templates', 'BapController@getTemplates');
Route::middleware('auth:api')->post('save-draft', 'BapController@saveToDraft');
Route::middleware('auth:api')->post('get-draft', 'BapController@getDraft');

Route::post('send-notification', 'NotificationsController@sendNotification');

Route::post('save', 'CourseWatchedController@save');



Route::middleware('auth:account_api')->resource('account', 'AccountController');



Route::middleware('auth:account_api')->get('account-list-invoice', 'AccountController@listInvoices');
Route::middleware('auth:account_api')->get('account-edit-invoice/{invoice_id}', 'AccountController@showInvoice');
Route::middleware('auth:account_api')->get('account-get-invoice_salesorder/{order_id}', 'AccountController@getInvoiceSales');
Route::middleware('auth:account_api')->delete('account-delete-invoice/{invoice_id}', 'AccountController@deleteInvoice');

Route::middleware('auth:account_api')->get('account-list-payments', 'AccountController@listPayments');
Route::middleware('auth:account_api')->get('account-edit-payment/{payment_id}', 'AccountController@showPayment');
Route::middleware('auth:account_api')->delete('account-delete-payment/{payment_id}', 'AccountController@deletePayment');

Route::middleware('auth:account_api')->get('account-list-salesorder', 'AccountController@listSalesOrders');
Route::middleware('auth:account_api')->get('account-edit-salesorder/{order_id}', 'AccountController@showSalesOrder');
Route::middleware('auth:account_api')->delete('account-delete-salesorder/{order_id}', 'AccountController@deleteSalesOrder');

Route::middleware('auth:account_api')->get('get-statement', 'AccountController@getStatement');
Route::middleware('auth:account_api')->get('list-statements', 'AccountController@listStatements');
Route::middleware('auth:account_api')->post('add-statement', 'AccountController@addStatement');


Route::middleware('auth:account_api')->post('add-total', 'ExpenseController@addTotals');
Route::middleware('auth:account_api')->get('get-total', 'ExpenseController@getTotals');
Route::middleware('auth:account_api')->post('add-expense', 'ExpenseController@addExpense');
Route::middleware('auth:account_api')->get('get-expense', 'ExpenseController@getExpense');
Route::middleware('auth:account_api')->get('get-expense-per-month/{month}', 'ExpenseController@getExpensePerMonth');

Route::middleware('auth:account_api')->post('add-reminder', 'ExpenseController@addReminder');
Route::middleware('auth:account_api')->delete('del-book/{id}', 'ExpenseController@delBook');
Route::middleware('auth:account_api')->get('sendNotify', 'ExpenseController@sendNotification');
Route::middleware('auth:account_api')->post('save-history', 'ExpenseController@monthlySave');
Route::middleware('auth:account_api')->get('get-history/{month}', 'ExpenseController@getMonthlySave');
Route::middleware('auth:account_api')->get('all-history', 'ExpenseController@allMonthlySave');
Route::delete('test-cron', 'ExpenseController@testCron');

Route::middleware('auth:account_api')->post('create-sales-order', 'AccountController@createSalesOrder');
Route::middleware('auth:account_api')->post('create-payment', 'AccountController@createPayment');
Route::middleware('auth:account_api')->post('create-invoice', 'AccountController@createInvoice');
Route::middleware('auth:account_api')->post('create-item', 'AccountController@createItem');
Route::middleware('auth:account_api')->get('list-items', 'AccountController@listItems');
Route::middleware('auth:account_api')->get('get-item/{id}', 'AccountController@getItem');
Route::middleware('auth:account_api')->post('update-item', 'AccountController@updateItem');
Route::middleware('auth:account_api')->delete('delete-item/{id}', 'AccountController@deleteItem');


Route::middleware('auth:account_api')->post('create-customer', 'AccountController@createCustomer');
Route::middleware('auth:account_api')->get('list-customers', 'AccountController@listCustomers');
Route::middleware('auth:account_api')->get('get-customer/{id}', 'AccountController@getCustomer');
Route::middleware('auth:account_api')->put('update-customer/{id}', 'AccountController@updateCustomer');
Route::middleware('auth:account_api')->delete('delete-customer/{id}', 'AccountController@deleteCustomer');

Route::middleware('auth:account_api')->post('create-credit', 'AccountController@createCredit');
Route::middleware('auth:account_api')->get('list-credits', 'AccountController@listCredits');
Route::middleware('auth:account_api')->get('get-credit/{id}', 'AccountController@getCredit');
Route::middleware('auth:account_api')->get('get-manufacturer-credit/{id}', 'AccountController@getManufacturerCredit');
Route::middleware('auth:account_api')->delete('delete-credit/{id}', 'AccountController@deleteCredit');

Route::middleware('auth:account_api')->get('account-get-invoice/{invoice_no}', 'AccountController@getPaymentInvoice');
Route::middleware('auth:account_api')->post('total-expenses', 'AccountController@addTotalExpenses');
Route::middleware('auth:account_api')->get('get-total-expenses/{month}', 'AccountController@getTotExp');

Route::middleware('auth:account_api')->post('add-manufacturer-material', 'AccountController@addManufacturerMaterial');
Route::middleware('auth:account_api')->post('add-manufacturer-product', 'AccountController@addManufacturerProduct');
Route::middleware('auth:account_api')->get('list-manufacturer-materials', 'AccountController@listManufacturerMaterials');
Route::middleware('auth:account_api')->get('list-manufacturer-product', 'AccountController@listManufacturerProducts');
Route::middleware('auth:account_api')->post('add-product-material', 'AccountController@addProductMaterial');
Route::middleware('auth:account_api')->get('list-product-material', 'AccountController@listProductMaterial');
Route::middleware('auth:account_api')->get('get-manufacturer-products/{id}', 'AccountController@getManufacturerProducts');
Route::middleware('auth:account_api')->get('get-manufacturer-product/{id}', 'AccountController@getManufacturerProduct');
Route::middleware('auth:account_api')->get('get-manufacturer-material/{id}', 'AccountController@getManufacturerMaterial');
Route::middleware('auth:account_api')->delete('delete-manufacturer-product/{id}', 'AccountController@deleteManufacturerProduct');
Route::middleware('auth:account_api')->delete('delete-manufacturer-material/{id}', 'AccountController@deleteManufacturerMaterial');
Route::middleware('auth:account_api')->put('update-manufacturer-product/{id}', 'AccountController@updateProduct');

Route::middleware('auth:account_api')->put('update-manufacturer-material/{id}', 'AccountController@updateMaterial');

Route::middleware('auth:account_api')->post('add-user', 'AccountController@addUser');
Route::middleware('auth:account_api')->get('get-user/{id}', 'AccountController@getUser');
Route::middleware('auth:account_api')->get('get-users', 'AccountController@getUsers');
Route::middleware('auth:account_api')->put('update-user/{id}', 'AccountController@updateUser');
Route::middleware('auth:account_api')->put('verify-user/{id}', 'AccountController@verifyUser');
Route::middleware('auth:account_api')->delete('delete-user/{id}', 'AccountController@deleteUser');



 Route::middleware('auth:account_api')->get('/accountDetails', 'AccountController@accountLogin');
 Route::post('register-account', 'AccountController@accountSignup');

Route::get('/userDetails/{email}', 'Auth\RegisterController@retrieveUser');




//  Route::post('/vendorDetails', 'VendorUserController@allowUser') ;
// Route::middleware('auth:account_api')->get('/vendorDetails', function (Request $request) {
//     return $request->user();

// });


Route::middleware('auth:vendor')->group(function () {
    Route::get('/vendorDetails', function (Request $request) {
        return $request->user();
    });

    Route::resource('vendor-product', 'ProductController');
    Route::middleware('auth:vendor')->resource('vendorsubscription', 'VendorSubscriptionController');
    Route::resource('faqs', 'FAQController');
    Route::resource('shipping-detail', 'VendorShippingController');
    Route::get('shipping-detail-vendor', 'VendorShippingController@getShippingDetailForVendor');
    Route::get('/get/faq/{id}', 'FAQController@getFaqForProduct');
    Route::get('/vendorproduct/{userid}', 'ProductController@getVendorProduct');
    Route::post('/vendor/settings', 'VendorUserController@postVendorSettings');
    Route::post('vendordetail', 'VendorUserController@updateVendorProfile');
    Route::get('getvendor/{storename}', 'VendorUserController@getVendorProfileDetail');
    Route::get('get/vendor/{settings}', 'VendorUserController@getVendorSettings');
    Route::get('get/all/category', 'ProductCategoryController@getVendorCategories');
    Route::get('get/all/subcategory/{subcat}', 'ProductSubCategoryController@getVendorSubCategoryById');
    Route::get('get/subcategorybrand/{subcat}', 'ProductSubCategoryBrandController@getSubCategoryBrandById');
    Route::post('add/more/product', 'ProductController@continueAddProduct');
    Route::post('add/shipping/detail', 'ProductController@addShippingDetail');
    Route::get('get/all/subcategorybrand/{subcat}', 'ProductSubCategoryBrandController@getSubTopicForVendor');
    Route::post('edit/existing/product', 'ProductController@continueEditProduct');
    Route::get('get/subcatbrand/{subcat}/edit', 'ProductSubCategoryBrandController@getSpecificCategoryBrand');
    Route::resource('product-chapter', 'ProductDetailController');
    Route::get('get/product-chapter/{product}', 'ProductDetailController@getAllChapterForPost');
    Route::get('get/product-video/{product}', 'ProductVideoController@getEpisodeByProductChapter');
    Route::resource('product-video', 'ProductVideoController');
    Route::get('order-detail/vendor/{vendor}', 'OrderDetailController@getOrderForVendor');
    Route::get('get/all-draft-product', 'ProductController@getAllDraftNotVerifiedProduct');

    Route::post('delete/many-products', 'ProductController@destroyMany');
});


Route::middleware('auth:ops_user')->group(function () {
    Route::get('/opsDetails', function (Request $request) {
        return $request->user();
    });
    Route::get('all/product', 'OperationController@getAllProducts');
    Route::get('ops/product/{product}', 'OperationController@showProduct');
    Route::get('verify/product/{productid}', 'OperationController@verifyProduct');
    Route::get('featured/product/{productid}', 'OperationController@featuredProduct');
    Route::get('all/verify/product', 'OperationController@getAllVerifiedProduct');
    Route::get('all/vendors', 'OperationController@getAllVendor');

    Route::get('vendor/changestatus/{status}', 'OperationController@disableEnableVendor');
});

Route::group(['middleware' => ['auth:admin_user', 'can:web-admin']], function () {
    Route::resource('category', 'ProductCategoryController');

    Route::resource('subcategory', 'ProductSubCategoryController');

    Route::resource('subcategorybrand', 'ProductSubCategoryBrandController');

    Route::resource('quest-bank', 'QuestionBankController');
});

Route::group(['middleware' => ['auth:admin_user', 'can:super-admin']], function () {
    Route::get('admin/all-subject-matter', 'AdminUserController@getSubjectMatterForAdminDisplay');

    Route::resource('insights', 'InsightController');

    Route::resource('industry', 'IndustryController');

    Route::post('admin/subject-matter-delete', 'ProductSubCategoryController@destroyMany');

    Route::post('admin/delete-insights', 'InsightController@destroyMany');

    Route::post('admin/delete-concepts', 'ProductSubCategoryBrandController@destroyMany');

    Route::post('admin/delete-industry', 'IndustryController@destroyMany');

    Route::get('admin/all-topics', 'AdminUserController@getTopicForAdminDisplay');

    Route::get('admin/get-all-user', 'AdminUserController@getUserForAdmin');

    Route::post('admin/create/access-level', 'AdminUserController@createAdminLevel');

    Route::resource('subscription', 'SubscriptionController');

    Route::get('admin/all-user-types', 'AdminUserController@getAllAdminUser');

    Route::delete('admin/remove/{product}', 'AdminUserController@deleteProduct');

    Route::middleware('auth:api')->resource('reviews', 'ReviewController');

    Route::get('get/verify-bizguruh-product/{id}', 'AdminUserController@verifybizguruhProduct');

    //Route::get('admin/verify/product/{productid}', 'AdminUserController@verifyProductForAdmin');
});

Route::group(['middleware' => ['auth:admin_user', 'can:store-admin']], function () {
    Route::get('admin/all/product', 'AdminUserController@getAllProductsForAdmin');

    Route::get('admin/vendor/product', 'AdminUserController@getVendorForProduct');

    Route::get('admin/expert/product', 'AdminUserController@getExpertsForProduct');

    Route::get('admin/univerify-product', 'AdminUserController@getAllUniverifyProduct');

    Route::middleware('auth:api')->resource('reviews', 'ReviewController');

    Route::post('admin/delete/products', 'AdminUserController@destroyMany');

    Route::get('admin/get/category', 'AdminUserController@getCategory');

    Route::get('admin/get/subcategory/{subcategory}', 'AdminUserController@getSubCategoryById');

    Route::resource('product', 'ProductController');

    Route::get('admin/get/products', 'ProductController@getAdminProduct');

    Route::get('admin/get/topic/{topic}', 'AdminUserController@getSubTopicFromSubCategory');

    Route::get('admin/get/draft-product', 'ProductController@getAllDraftProductForAdmin');




    Route::delete('admin/remove/{product}', 'AdminUserController@deleteProduct');

    Route::get('admin/product/{product}', 'AdminUserController@showProductForAdmin');

    Route::get('admin/verify/product/{productid}', 'AdminUserController@verifyProductForAdmin');

    Route::get('admin/verify/reviews/{review}', 'AdminUserController@verifyReview');

    Route::get('admin/featured/product/{productid}', 'AdminUserController@featuredProductForAdmin');

    Route::get('admin/all/vendors', 'AdminUserController@getAllVendorUser');

    Route::get('admin/add/ops', 'AdminUserController@getAllOpsUser');

    Route::get('admin/vendor-details/{vendor}', 'AdminUserController@getVendorDetail');
    Route::post('admin/ops/register', 'AdminUserController@createOpsUser');

    Route::get('admin/getcategorybyid/{cat}', 'ProductCategoryController@getSubCategoryById');

    Route::get('admin/getsubcategorybyid/{cat}', 'ProductSubCategoryController@getSubCategoryById');

    Route::get('admin/getsubcategorybrand/{cat}', 'ProductSubCategoryBrandController@getSubCategoryById');


    Route::get('admin/getsubcatbrandbyid/{cat}', 'ProductSubCategoryBrandController@getSubCatByBrand');


    Route::get('admin/enabledisable/vendor/{verifiedid}', 'AdminUserController@enableOrDisableVendorForAdmin');
});



Route::middleware('auth:admin_user')->group(function () {
    Route::get('adminDetails', function (Request $request) {
        return $request->user();
    });
});



Route::post('vendor/register', 'VendorUserController@create');
Route::get('check/{data}', 'VendorUserController@checkVendor');

Route::post('/customer/register', 'Auth\RegisterController@register');

Route::post('vendor/login', 'VendorUserController@login');

Route::get('/product-category', 'ProductCategoryController@index');

Route::get('/product-all-category', 'ProductCategoryController@getAllCategorywithSubCategory');

Route::get('subcategory/{subcategory}', 'ProductSubCategoryController@getSubCategoryById');

Route::get('products', 'ProductController@getAllVerifiedProduct');

Route::get('/sponsor-products', 'ProductController@getAllSponsorProduct');

Route::get('/product-detail/{id}', 'ProductController@showProductDetail');
Route::get('/course-detail/{id}', 'ProductController@showCourseDetail');

Route::get('/get-all-vendor', 'VendorUserController@getAllVendor');

Route::get('/get-related-product/{relatedproduct}', 'ProductController@getAllRelatedProduct');

Route::get('/get-sub-categories', 'ProductSubCategoryController@index');

Route::get('/best-selling-products', 'ProductController@bestSellingProducts');

Route::get('/product/categories/{cat}', 'ProductController@getFrontCategory');

Route::post('product/get-reviews', 'ReviewController@getReviewsForProduct');

Route::get('bizguruh-topics', 'ProductSubCategoryBrandController@bizguruhTopic');


Route::get('/product/subcategories/{subcat}', 'ProductController@getFrontSubCategory');

Route::get('/product/topic/{topic}', 'ProductController@getFrontSubCategoryBrand');

Route::get('/product/info/{product}/{type}', 'CartController@getProductById');

Route::get('/get/vendor/id/{id}', 'CartController@getProductByCart');

Route::get('vendor-subject-matter', 'VendorUserController@getVendorSubjectMatter');
Route::get('vendor-insights', 'VendorUserController@getVendorInsight');
Route::get('vendor-topics', 'VendorUserController@getVendorTopic');
Route::get('vendor-industry', 'VendorUserController@getVendorIndustries');



Route::post('/post/cart/bk', 'CartController@postToCart');

Route::get('/post/cart/delete/{id}', 'CartController@deleteItem');

Route::get('get/prodtype/{prodtype}', 'ProductController@getAllProductType');

Route::get('admin/product/bizguruh/{prodtype}', 'ProductController@getAllProductTypeForBizguruh');
Route::get('all/product/{prodtype}', 'ProductController@getSpecificProductType');

Route::get('get/related/{prodtype}/{id}', 'ProductController@getRelatedProductType');

Route::get('events-page', 'ProductController@getAllVerifyEvent');

Route::get('get/popular-articles', 'ArticleProductController@getArticleBasedOnPopularity');

Route::get('articles-page', 'ProductController@getAllVerifyarticle');

Route::post('/get/free/article', 'ArticleProductController@getSingleFreeArticle');

Route::get('event/{eventid}', 'ProductController@getSingleEvent');

Route::post('getAllArticle', 'ArticleProductController@getAllArticle');

Route::get('subscription-plans', 'SubscriptionController@getSubscriptionForUser');

Route::get('articlesTopic', 'ArticleProductController@getAllTopicForArticles');

Route::get('get/getvideodetail/{getVideoDetail}', 'ProductController@getVideoDetail');
Route::get('user/product-faq/{faq}', 'FAQController@getProductFAQ');

//get subject matter
Route::get('subjectmatter', 'ProductSubCategoryController@subjectMatter');

Route::get('topics', 'ProductSubCategoryBrandController@topic');
Route::get('topics/{id}', 'ProductController@getProductByTopic');


Route::get('questionBank/{question}/question', 'QuestionBankController@getQuestionBySubjectMatterId');

Route::get('random-products', 'ProductController@getSomeProductType');

Route::get('all-industries', 'IndustryController@getAllIndustry');
Route::get('industry/{id}', 'IndustryController@getProductByIndustry');

Route::get('industries/{id}', 'ProductController@getProductByIndustry');

Route::get('vendor-products/{id}', 'ProductController@getProductByVendor');




Route::post('contact', 'UserPreferenceController@contact');


Route::get('product-bot', 'ProductController@displayBotContent');

//Route::get('productbot/subject/\?={params}', 'ProductController@getQueryProduct');

Route::get('productbot/category', 'ProductController@getBotCategory');

Route::get('productbot', 'ProductController@getSubjectMatterProduct');
Route::get('vendor/{details}', 'OperationController@getvendorDetail');
Route::get('vendorCourses/{id}', 'ProductController@getVendorCourses');

Route::get('productbot/{categoryId}/product/{searchItem}', 'ProductController@getItem');

Route::get('productbot/{searchTerm}', 'ProductController@searchProduct');


Route::post('image-upload', 'ProductController@imageUpload');

Route::get('count/{email}', 'Auth\RegisterController@getUsers');

Route::post('addcount/{email}', 'Auth\RegisterController@addCount');

Route::post('delcount/{email}', 'Auth\RegisterController@delCount');
Route::post('sendmail/{id}', 'OrderController@sendOrder');
Route::get('mail', 'Auth\RegisterController@mail');
Route::post('checkToken/{token}', 'Auth\RegisterController@checkToken');
Route::post('user/subscription-plan/{id}', 'UserSubscriptionPlanController@subscriptionPlanForUser');
Route::post('cart/check/{id}/{product}', 'CartController@checkCart');
Route::get('getfollowers/{vendor}', 'UserFollowingController@getFollowers');
Route::get('getfollowers-weekly/{vendor}', 'UserFollowingController@getFollowersWeekly');
Route::get('clearCount', 'ArticleProductController@clearCount');
Route::get('clearOrder', 'OrderController@clearOrder');
Route::get('userPreferences/{id}', 'UserPreferenceController@getPreferenceForuser');
Route::post('add/article', 'ProductController@addArticle');
Route::get('getaVendorDetail/{id}', 'ProductController@aVendor');
Route::get('sendResetLink/{email}', 'PasswordResetController@sendLink');
Route::post('changePassword', 'PasswordResetController@changePassword');
Route::resource('questions', 'QuestionController');
Route::get('questionsVerified', 'QuestionController@verifiedQuestions');
Route::get('verify-question/{id}', 'QuestionController@verify');
Route::get('unverify-question/{id}', 'QuestionController@unverify');
Route::get('get-ip', 'IpController@getUserIp');
Route::get('check-ip/{id}/{token}', 'IpController@checkLogin');
Route::resource('ip', 'IpController');
Route::resource('guides/history', 'ChecklistAnswerController');
Route::get('history/{id}', 'ChecklistAnswerController@getHistory');
Route::post('single/history', 'ChecklistAnswerController@getSingleHistory');

Route::get('accounting/{id}', 'AccountController@getUserId');

Route::post('add-activity', 'VendorStatisticController@addActivity');
Route::get('get-activity/{id}', 'VendorStatisticController@getActivity');
Route::post('add-gender', 'VendorStatisticController@addGender');
Route::get('get-gender/{id}', 'VendorStatisticController@getGender');
Route::post('add-location', 'VendorStatisticController@addLocation');
Route::get('get-location/{id}', 'VendorStatisticController@getLocation');
Route::post('add-like', 'VendorStatisticController@addLikes');
Route::get('get-likes/{user}/{product}', 'VendorStatisticController@getLikes');
Route::get('get-all-likes/{product}', 'VendorStatisticController@getAllLikes');
Route::get('get-un-likes/{product}', 'VendorStatisticController@getAllUnLikes');
Route::get('get-vendor-likes/{vendor}', 'VendorStatisticController@getVendorLikes');
Route::delete('remove-like/{id}', 'VendorStatisticController@removeLike');
Route::post('add-top-content', 'VendorStatisticController@addTopContent');
Route::get('get-top-content/{id}', 'VendorStatisticController@getTopContent');
Route::post('add-top-industry', 'VendorStatisticController@addTopIndustry');
Route::get('get-top-industry/{id}', 'VendorStatisticController@getTopIndustry');
Route::post('add-top-preference', 'VendorStatisticController@addTopPreference');
Route::get('get-top-preference/{id}', 'VendorStatisticController@getTopPreference');
Route::post('add-age', 'VendorStatisticController@addAge');
Route::get('get-ages/{id}', 'VendorStatisticController@getAge');
Route::post('upload-day', 'VendorStatisticController@uploadDay');
Route::get('get-day/{id}', 'VendorStatisticController@getUploadDay');
Route::post('add-checklist-vendor', 'VendorStatisticController@checklistVendor');
Route::get('get-checklist-vendor/{id}', 'VendorStatisticController@getChecklistVendor');
Route::post('update-profile', 'Auth\RegisterController@updateProfile');

Route::post('add-interaction', 'VendorStatisticController@addInteraction');
Route::get('get-interactions/{id}', 'VendorStatisticController@getInteractions');

Route::post('add-reach', 'VendorStatisticController@addReach');
Route::get('get-reach/{id}', 'VendorStatisticController@getReach');



Route::post('subscribe', 'NewsletterController@subscribe');
Route::get('manageMailChimp', 'NewsletterController@manageMailChimp');
// Route::post('subscribe', ['as'=>'subscribe','uses'=>'NewsletterController@subscribe']);
Route::post('sendCampaign', ['as'=>'sendCampaign','uses'=>'NewsletterController@sendCampaign']);
Route::post('create-list', 'NewsletterController@newList');


Route::get('month', 'BapController@monthly');

Route::middleware('auth:api')->get('messages', 'ChatsController@fetchMessages');
Route::middleware('auth:api')->post('message', 'ChatsController@sendMessage');

Route::middleware('auth:api')->get('user-messages/{id}', 'ChatsController@fetchUserMessages');
Route::middleware('auth:api')->post('user-message', 'ChatsController@sendUserMessage');

Route::middleware('auth:vendor')->get('vendor-messages', 'ChatsController@fetchVendorMessages');
Route::middleware('auth:vendor')->post('vendor-message', 'ChatsController@sendVendorMessage');

Route::middleware('auth:api')->get('group-messages/{id}', 'ChatsController@fetchGroupMessages');
Route::middleware('auth:api')->post('group-message', 'ChatsController@sendGroupMessage');

Route::middleware('auth:vendor')->get('vendor-group-messages', 'ChatsController@fetchVendorGroupMessages');
Route::middleware('auth:vendor')->post('vendor-group-message', 'ChatsController@sendVendorGroupMessage');


Route::middleware('auth:api')->post('start', 'BapController@start');
Route::middleware('auth:api')->get('get-active-record', 'BapController@getActiveRecord');
Route::middleware('auth:api')->get('get-all-record', 'BapController@getAllRecord');

/*Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');*/



Route::post('register-ilc','IlcController@register');

Route::middleware('auth:ilc_api')->get('/ilc-account', function (Request $request) {
   
    return $request->user();
});
Route::middleware('auth:api')->resource('get-product', 'ProductController');
Route::get('get-course/{id}','IlcController@getUserCourse');

Route::get('get-faculties', 'GeneralController@getFaculty');

Route::get('get-dept', 'GeneralController@getDept');

Route::get('get-edulevel', 'GeneralController@getEducationLevel');
Route::get('get-level', 'GeneralController@getCourseLevel');

Route::middleware('auth:vendor')->get('vendor-notifications','NotificationsController@getNotifications');

Route::middleware('auth:vendor')->get('vendor-subscribers','NotificationsController@getUsers');
Route::middleware('auth:vendor')->get('course-notifications','CourseWatchedController@getCourseNotifications');

Route::middleware('auth:api')->post('save-form-template', 'FormTemplateController@save');
Route::middleware('auth:api')->post('update-form-template', 'FormTemplateController@update');
Route::middleware('auth:api')->delete('delete-form-template/{id}', 'FormTemplateController@deleteTemplate');
Route::middleware('auth:api')->post('delete-all-form', 'FormTemplateController@deleteAllTemplate');
Route::middleware('auth:api')->get('get-form-templates', 'FormTemplateController@getAllTemplates');
Route::middleware('auth:api')->get('get-form-template/{id}', 'FormTemplateController@getSingleTemplate');

Route::middleware('auth:vendor')->post('save-form-vendor', 'VendorFormController@save');
Route::middleware('auth:vendor')->post('update-form-vendor', 'VendorFormController@update');
Route::middleware('auth:vendor')->delete('delete-form-vendor/{id}', 'VendorFormController@deleteForm');
Route::middleware('auth:vendor')->post('delete-all-vendor-forms', 'VendorFormController@deleteAllForm');
Route::middleware('auth:vendor')->get('get-form-vendors', 'VendorFormController@getAllTemplates');
Route::middleware('auth:vendor')->get('get-form-vendor/{id}', 'VendorFormController@getSingleTemplate');
Route::get('get-bap-forms/{name}','BapController@getForms');
Route::get('get-assessment/{id}', 'VendorFormController@getCourseForm');
Route::get('get-form/{id}', 'VendorFormController@getForm');

Route::get('user-sub/{id}','UserSubscriptionPlanController@getUserSub');
Route::get('account-sub/{id}','UserSubscriptionPlanController@getAccountSub');
Route::get('vendor-sub/{id}','UserSubscriptionPlanController@getVendorSub');


// USSD api routes 

Route::post('accounting', 'AccountUssdController@handleAccounting');



Route::post('subscriber', 'AccountUssdController@getdetails')->middleware('basicAuth');
Route::post('subscription', 'AccountUssdController@subscription')->middleware('basicAuth');

