<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturerMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturer_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('material_name');
            $table->integer('material_no');
            $table->string('quantity_per_unit');
            $table->integer('in_stock');
            $table->integer('cost_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturer_materials');
    }
}
