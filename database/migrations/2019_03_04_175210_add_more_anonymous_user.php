<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreAnonymousUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('anonymous_user', function(Blueprint $table){
           $table->string('duration')->nullable()->after('anonymousProductId');
           $table->string('durationCount')->nullable()->after('duration');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('anonymous_user', function(Blueprint $table){
            $table->dropColumn('durationCount');
            $table->dropColumn('duration');
        });
    }
}
