<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesPrivateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_private', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_user_id')->unsigned();
            $table->integer('user_id');
            $table->text('message');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_private');
    }
}
