<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_products', function(Blueprint $table){
           $table->string('time')->nullable()->after('date');
           $table->string('endDate')->nullable()->after('time');
           $table->string('endTime')->nullable()->after('endDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('event_products', function(Blueprint $table){
            $table->dropColumn('endTime');
            $table->dropColumn('endDate');
            $table->dropColumn('time');
        });
    }
}
