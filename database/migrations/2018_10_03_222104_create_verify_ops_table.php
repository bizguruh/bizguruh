<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifyOpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_ops', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ops_user_id');
            $table->string('token');
            $table->timestamps();
        });

        Schema::table('verify_ops', function($table) {
            $table->foreign('ops_user_id')->references('id')->on('ops_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verify_ops', function (Blueprint $table) {

            $table->dropForeign(['ops_user_id']);

        });
        Schema::dropIfExists('verify_ops');
    }
}
