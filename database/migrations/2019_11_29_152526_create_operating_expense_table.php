<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatingExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operating_expense', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('rent');
            $table->integer('salaries');
            $table->integer('light');
            $table->integer('data');
            $table->integer('bills');
            $table->integer('repairs');
            $table->integer('marketing');
            $table->integer('logistics');
            $table->integer('office');
            $table->integer('levies');
            $table->integer('others');
            $table->string('month');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operating_expense');
    }
}
