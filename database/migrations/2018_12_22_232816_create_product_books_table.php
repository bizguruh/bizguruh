<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('publisher')->nullable();
            $table->string('publicationDate')->nullable();
            $table->integer('noOfPages');
            $table->string('author');
            $table->string('isbn');
            $table->string('edition');
            $table->string('audioFile')->nullable();
            $table->string('audioName')->nullable();
            $table->string('audioPublicId')->nullable();
            $table->text('tableOfContent')->nullable();
            $table->text('aboutThisAuthor');
            $table->string('sponsorName')->nullable();
            $table->text('aboutSponsor')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('excerptFile')->nullabe();
            $table->string('excerptName')->nullable();
            $table->string('excerptPublicId')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
        });

        Schema::table('product_books', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_books', function (Blueprint $table) {

            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('product_books');
    }
}
