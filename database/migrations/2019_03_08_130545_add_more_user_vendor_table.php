<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreUserVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('vendor_users', function(Blueprint $table){
            $table->text('bio')->nullable()->after('password');
            $table->string('type')->nullable()->after('bio');
            $table->string('subjectMatter')->nullable()->after('type');
            $table->string('topic')->nullable()->after('subjectMatter');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('topic');
            $table->dropColumn('subjectMatter');
            $table->dropColumn('type');
            $table->dropColumn('bio');
        });
    }
}
