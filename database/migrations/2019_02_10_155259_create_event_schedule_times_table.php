<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventScheduleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_schedule_times', function (Blueprint $table) {
            $table->increments('id');
            $table->string('startDay')->nullable();
            $table->string('eventStart')->nullable();
            $table->string('eventEnd')->nullable();
            $table->text('moreInformation')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->timestamps();
        });

        Schema::table('event_schedule_times', function($table) {
            $table->foreign('product_id')->references('id')->on('event_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_schedule_times', function (Blueprint $table) {

            $table->dropForeign(['product_id']);

        });

        Schema::dropIfExists('event_schedule_times');
    }
}
