<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('host')->nullable();
            $table->text('aboutHost')->nullable();
            $table->string('startDate')->nullable();
            $table->string('excerpts')->nullable();
            $table->string('excerptsName')->nullable();
            $table->string('excerptsPublicId')->nullable();
            $table->string('sponsorName')->nullable();
            $table->text('aboutSponsor')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->timestamps();
        });

        Schema::table('product_chapters', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_chapters', function (Blueprint $table) {

            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('product_chapters');
    }
}
