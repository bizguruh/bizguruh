<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCourseModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_course_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
             $table->string('file')->nullable();
             $table->string('fileName')->nullable();
             $table->string('fileAudioName')->nullable();
             $table->string('audioFile')->nullable();
             $table->string('audioPublicId')->nullable();
             $table->string('filePublicId')->nullable();
             $table->string('videoFile')->nullable();
             $table->string('videoPublicId')->nullable();
             $table->string('fileVideoName')->nullable();
             $table->string('author')->nullable();
             $table->text('aboutAuthor')->nullable();
             $table->string('duration')->nullable();
             $table->string('durationPeriod')->nullable();
             $table->unsignedInteger('products_courses_id');
            $table->timestamps();
        });

        Schema::table('product_course_modules', function($table) {
            $table->foreign('products_courses_id')->references('id')->on('products_courses')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_course_modules', function (Blueprint $table) {

            $table->dropForeign(['products_courses_id']);
        });

        Schema::dropIfExists('product_course_modules');

    }
}
