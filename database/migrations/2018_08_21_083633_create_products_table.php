<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('prodType')->nullable();
            $table->string('type')->nullable();
            $table->string('quantity')->nullable();
            $table->boolean('hardCopy')->nullable();
            $table->string('document')->nullable();
            $table->string('documentName')->nullable();
            $table->string('documentPublicId')->nullable();
            $table->boolean('softCopy')->nullable();
            $table->string('coverImage')->nullable();
            $table->string('videoPrice')->nullable();
            $table->string('subscribePrice')->nullable();
            $table->string('streamOnlinePrice')->nullable();
            $table->string('webinarPrice')->nullable();
            $table->string('softCopyPrice')->nullable();
            $table->string('hardCopyPrice')->nullable();
            $table->string('readOnlinePrice')->nullable();
            $table->boolean('verify')->default(false);
            $table->string('sponsor')->default('N');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
