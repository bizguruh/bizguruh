<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreVendoruserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('vendor_users', function (Blueprint $table){
           $table->string('valid_id')->nullable()->after('topic');
           $table->text('address')->nullable()->after('valid_id');
           $table->string('occupation')->nullable()->after('address');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('vendor_users', function (Blueprint $table){
           $table->dropColumn('occupation');
           $table->dropColumn('address');
           $table->dropColumn('valid_id');
        });
    }
}
