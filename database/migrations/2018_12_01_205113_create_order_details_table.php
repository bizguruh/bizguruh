<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productId')->nullable();
            $table->string('prodType')->nullable();
            $table->string('vendorId')->nullable();
            $table->string('amount')->nullable();
            $table->integer('quantity')->nullable();
            $table->unsignedInteger('order_id');
            $table->timestamps();
        });

        Schema::table('order_details', function (Blueprint $table) {
           $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function (Blueprint $table) {

            $table->dropForeign(['order_id']);

        });

        Schema::dropIfExists('order_details');
    }
}
