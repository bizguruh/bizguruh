<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('storeName')->nullable();
            $table->string('sidebarBackgroundColor')->nullable();
            $table->string('sidebarColor')->nullable();
            $table->string('sidebarFontSize')->nullable();
            $table->string('sidebarMargin')->nullable();
            $table->string('topMenuBackgroundColor')->nullable();
            $table->string('topMenuColor')->nullable();
            $table->string('topMenuFontSize')->nullable();
            $table->string('topMenuMargin')->nullable();
            $table->string('overallBackgroundColor')->nullable();
            $table->string('overallColor')->nullable();
            $table->string('overFontSize')->nullable();
            $table->unsignedInteger('vendor_id')->nullable();
            $table->timestamps();
        });

        Schema::table('vendors_settings', function($table) {
            $table->foreign('vendor_id')->references('id')->on('vendor_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_settings', function (Blueprint $table) {

            $table->dropForeign(['vendor_id']);

        });
        Schema::dropIfExists('vendors_settings');
    }
}
