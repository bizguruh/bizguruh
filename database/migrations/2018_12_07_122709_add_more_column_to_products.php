<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
           $table->string('readOnline')->nullable()->after('verify');
           $table->boolean('audioCopy')->nullable()->after('softCopy');
           $table->boolean('videoCopy')->nullable()->after('audioCopy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('videoCopy');
            $table->dropColumn('audioCopy');
            $table->dropColumn('readOnline');

        });
    }
}
