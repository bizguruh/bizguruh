<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreTableProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products_white_paper', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author');
            $table->string('contributors')->nullable();
            $table->text('problemStatement');
            $table->text('executiveSummary')->nullable();
            $table->string('sponsorName')->nullable();
            $table->text('aboutSponsor')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('excerptFile')->nullabe();
            $table->string('excerptName')->nullable();
            $table->string('excerptPublicId')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
        });

        Schema::table('products_white_paper', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products_white_paper', function (Blueprint $table) {

            $table->dropForeign(['product_id']);

        });

        Schema::dropIfExists('products_white_paper');
    }
}
