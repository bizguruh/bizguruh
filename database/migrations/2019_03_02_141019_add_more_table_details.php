<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreTableDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_detail_videos', function(Blueprint $table){
           $table->string('episodeGuest')->nullable()->after('videoName');
           $table->string('episodedescription')->nullable()->after('episodeGuest');
           $table->string('sponsor')->nullable()->after('episodedescription');
           $table->string('aboutSponsor')->nullable()->after('sponsor');
           $table->string('myUpdatedDate')->nullable()->after('aboutSponsor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('order_detail_videos', function(Blueprint $table){
           $table->dropColumn('myUpdatedDate');
           $table->dropColumn('aboutSponsor');
           $table->dropColumn('sponsor');
           $table->dropColumn('episodedescription');
           $table->dropColumn('episodeGuest');
        });
    }
}
