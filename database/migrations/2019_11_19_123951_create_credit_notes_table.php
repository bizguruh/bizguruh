<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Integer;

class CreateCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id');
           $table->string( 'credit_no');
           $table->string('customer_name');
           $table->integer( 'item_id');

           $table->integer( 'rate');
           $table->integer( 'quantity');
           $table->string('balance_due');
           $table->string('status');
           $table->text( 'customer_note');
           $table->text( 'terms_conditions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_notes');
    }
}
