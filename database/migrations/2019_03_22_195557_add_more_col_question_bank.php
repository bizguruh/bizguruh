<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColQuestionBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('question_banks', function(Blueprint $table){
           $table->string('subjectMatterName')->nullable()->after('answer');
           $table->string('topicName')->nullable()->after('subjectMatter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('question_banks', function(Blueprint $table){
           $table->dropColumn('topicName');
           $table->dropColumn('subjectMatterName');
        });
    }
}
