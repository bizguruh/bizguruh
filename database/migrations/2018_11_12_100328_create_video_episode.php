<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoEpisode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_video_details', function(Blueprint $table){
           $table->increments('id');
           $table->string('title')->nullable();
           $table->text('overview')->nullable();
           $table->string('guest')->nullable();
           $table->text('aboutGuest')->nullable();
           $table->text('price')->nullable();
           $table->string('videoFile')->nullable();
           $table->string('videoPublicId')->nullable();
           $table->string('date')->nullable();
           $table->unsignedInteger('productId');
           $table->timestamps();
        });

        Schema::table('product_video_details', function($table){
           $table->foreign('productId')->references('id')->on('product_chapters')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_video_details', function (Blueprint $table) {

            $table->dropForeign(['productId']);

        });
        Schema::dropIfExists('product_video_details');
    }
}
