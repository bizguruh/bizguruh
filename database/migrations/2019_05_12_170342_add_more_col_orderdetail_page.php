<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColOrderdetailPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_detail_videos', function(Blueprint $table){
           $table->string('audioFile')->nullable()->after('videoName');
           $table->string('audioPublicId')->nullable()->after('audioFile');
           $table->string('audioName')->nullable()->after('audioPublicId');
           $table->string('file')->nullable()->after('audioName');
           $table->string('filePublicId')->nullable()->after('file');
           $table->string('fileName')->nullable()->after('filePublicId');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('order_detail_videos', function (Blueprint $table){
           $table->dropColumn('fileName');
           $table->dropColumn('filePublicId');
           $table->dropColumn('file');
           $table->dropColumn('audioName');
           $table->dropColumn('audioPublicId');
           $table->dropColumn('audioFile');

        });
    }
}
