<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName')->nullable();
            $table->string('storeName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('image')->nullable();
            $table->string('bio')->nullable();
            $table->unsignedInteger('vendor_id');
            $table->timestamps();
        });

        Schema::table('vendor_details', function($table) {
            $table->foreign('vendor_id')->references('id')->on('vendor_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_details', function (Blueprint $table) {

            $table->dropForeign(['vendor_id']);
        });
        Schema::dropIfExists('vendor_details');
    }
}
