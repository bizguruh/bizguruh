<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideoNameToProductvideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_video_details', function (Blueprint $table) {
           $table->string('videoName')->nullable()->after('videoFile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_video_details', function (Blueprint $table ) {
            $table->dropColumn('videoName');
        });
    }

}
