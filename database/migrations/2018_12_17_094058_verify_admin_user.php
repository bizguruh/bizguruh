<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VerifyAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('verify_admins', function(Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('admin_user_id');
           $table->string('token');
           $table->timestamps();
        });

        Schema::table('verify_admins', function($table) {
           $table->foreign('admin_user_id')->references('id')->on('admin_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('verify_admins', function (Blueprint $table) {

            $table->dropForeign(['admin_user_id']);
        });
        Schema::dropIfExists('verify_admins');

    }
}
