<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreEventProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_products', function(Blueprint $table){
           $table->unsignedInteger('product_id')->nullable()->after('aboutConvener');;
        });


        Schema::table('event_products', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('event_products', function(Blueprint $table){
            $table->dropForeign(['product_id']);
        });

    }
}
