<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_detail_videos', function (Blueprint $table) {
           $table->string('mediaPublicId')->nullable()->after('videoFile');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('order_detail_videos', function(Blueprint $table) {
           $table->dropColumn('mediaPublicId');
        });
    }
}
