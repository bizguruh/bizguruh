<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductSubCategoriesForeignColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('sub_categories_brand', function (Blueprint $table) {

            $table->unsignedInteger('sub_brand_category_id')->after('description');

            $table->foreign('sub_brand_category_id')->references('id')->on('product_sub_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sub_categories_brand', function (Blueprint $table) {

            $table->dropForeign(['sub_brand_category_id']);

        });

    }
}
