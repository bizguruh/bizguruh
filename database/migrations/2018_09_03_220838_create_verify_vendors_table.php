<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifyVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('vendor_user_id');
            $table->string('token');
            $table->timestamps();
        });

        Schema::table('verify_vendors', function($table) {
            $table->foreign('vendor_user_id')->references('id')->on('vendor_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verify_vendors', function (Blueprint $table) {

            $table->dropForeign(['vendor_user_id']);

        });
        Schema::dropIfExists('verify_vendors');
    }
}
