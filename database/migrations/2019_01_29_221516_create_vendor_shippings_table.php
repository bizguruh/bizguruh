<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location');
            $table->string('addressOne');
            $table->string('addressTwo')->nullable();
            $table->string('state');
            $table->string('country');
            $table->string('localShippingRates')->nullable();
            $table->string('localShippingTime')->nullable();
            $table->string('intlShippingRates')->nullable();
            $table->string('intlShippingTime')->nullable();
            $table->text('deliveryInformation')->nullable();
            $table->text('returnPolicy')->nullable();
            $table->text('extraInformation')->nullable();
            $table->unsignedInteger('vendor_id');
            $table->timestamps();
        });

        Schema::table('vendor_shippings', function(Blueprint $table) {
            $table->foreign('vendor_id')->references('id')->on('vendor_users')->onDelete('cascade');
        });


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('vendor_shippings', function (Blueprint $table) {

            $table->dropForeign(['vendor_id']);

        });

        Schema::dropIfExists('vendor_shippings');
    }
}
