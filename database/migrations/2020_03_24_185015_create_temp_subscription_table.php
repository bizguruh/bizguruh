<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_subscription', function (Blueprint $table) {
            $table->increments('id');
            $table->string('price')->nullable();
            $table->string('startDate')->nullable();
            $table->string('endDate')->nullable();
            $table->string('duration')->nullable();
            $table->string('durationCount')->nullable();
            $table->string('level')->nullable();
            $table->string('name')->nullable();
            $table->boolean('verify')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::table('temp_subscription', function (Blueprint $table){
           $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_subscription_plans', function(Blueprint $table){
           $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('user_subscription_plans');
    }
}
