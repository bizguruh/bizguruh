<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnonymousUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('anonymous_user', function(Blueprint $table){
            $table->increments('id');
            $table->string('anonymousUser')->nullable();
            $table->string('anonymousCount')->nullable();
            $table->string('anonymousProductId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('anonymous_user');

    }
}
