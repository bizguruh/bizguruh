<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColumnsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('overview');
           $table->string('duration')->nullable();
           $table->string('durationPeriod')->nullable();
           $table->text('courseModules')->nullable();
           $table->text('courseObjectives');
           $table->text('whoThisCourseFor');
           $table->text('keyBenefits');
            $table->text('whenYouComplete')->nullable();
           $table->string('fullOnline')->nullable();
           $table->string('mixedClass')->nullable();
           $table->text('excerpt')->nullable();
           $table->string('excerptName')->nullable();
           $table->string('excerptFile')->nullable();
           $table->string('excerptPublicId')->nullable();
           $table->string('sponsorName')->nullable();
           $table->text('aboutSponsor')->nullable();
           $table->string('level');
           $table->string('certification');
            $table->unsignedInteger('product_id');
            $table->timestamps();


        });

        Schema::table('products_courses', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products_courses', function (Blueprint $table) {

            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('products_courses');

    }
}
