<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCgatePaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('cgate_payment_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('paymentReference');
            $table->string('customerRef');
            $table->string('responseCode');
            $table->string('merchantId');
            $table->string('mobileNumber');
            $table->string('amount');
            $table->string('transactionDate');
            $table->string('shortCode');
            $table->string('currency');
            $table->string('channel');
            $table->string('hash');
            $table->string('passBackReference');
            $table->string('traceId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cgate_payment_history');
    }
}
