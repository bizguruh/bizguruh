<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_details', function (Blueprint $table){
            $table->string('type')->nullable()->after('quantity');
            $table->string('topic')->nullable()->after('type');
            $table->string('subjectMatter')->nullable()->after('topic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('order_details', function (Blueprint $table){
            $table->dropColumn('subjectMatter');
            $table->dropColumn('topic');
            $table->dropColumn('type');
        });
    }
}
