<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalColumnToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products_journal_article', function(Blueprint $table) {
            $table->increments('id');
            $table->string('articleTitle');
            $table->string('journalTitle');
            $table->text('abstract')->nullable();
            $table->string('author');
            $table->string('volume');
            $table->string('issue')->nullable();
            $table->string('monthYear');
            $table->string('pages');
            $table->string('publicationDate');
            $table->string('sponsorName')->nullable();
            $table->text('aboutSponsor')->nullable();
            $table->string('publisher');
            $table->string('issn');
            $table->text('tableOfContent')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('excerptFile')->nullabe();
            $table->string('excerptName')->nullable();
            $table->string('excerptPublicId')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();

        });

        Schema::table('products_journal_article', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products_journal_article', function (Blueprint $table) {

            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('products_journal_article');
    }
}
