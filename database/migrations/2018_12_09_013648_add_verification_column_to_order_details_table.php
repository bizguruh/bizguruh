<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerificationColumnToOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->string('orderNum')->nullable()->after('user_id');
            $table->boolean('verify')->nullable()->after('totalAmount');
            $table->string('referenceNo')->nullable()->after('orderNum');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->dropColumn('verify');
            $table->dropColumn('orderNum');
            $table->dropColumn('referenceNo');
        });
    }
}
