<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wishlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productId')->nullable();
            $table->unsignedInteger('userId')->nullable();
            $table->string('vendorId')->nullable();
            $table->timestamps();
        });

        Schema::table('wishlists', function(Blueprint $table) {
           $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wishlists', function (Blueprint $table) {
            $table->dropForeign(['userId']);
        });
        Schema::dropIfExists('wishlists');
    }
}
