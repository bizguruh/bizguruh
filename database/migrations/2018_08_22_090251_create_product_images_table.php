<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_market_report', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('contributor')->nullable();
            $table->text('aboutThisReport');
            $table->string('author');
            $table->text('overview');
            $table->text('tableOfContent');
            $table->string('sponsorName')->nullable();
            $table->text('aboutSponsor')->nullable();
            $table->text('excerpt')->nullable();
            $table->string('excerptFile')->nullabe();
            $table->string('excerptName')->nullable();
            $table->string('excerptPublicId')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
        });

        Schema::table('product_market_report', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_market_report', function (Blueprint $table) {

            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('product_market_report');
    }
}
