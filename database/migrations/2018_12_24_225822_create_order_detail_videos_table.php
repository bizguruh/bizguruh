<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('orderNum');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('videoFile');
            $table->string('videoName');
            $table->unsignedInteger('order_id');
            $table->timestamps();
        });

        Schema::table('order_detail_videos', function (Blueprint $table){
           $table->foreign('order_id')->references('id')->on('order_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_detail_videos', function (Blueprint $table) {

            $table->dropForeign(['order_id']);

        });
        Schema::dropIfExists('order_detail_videos');
    }
}
