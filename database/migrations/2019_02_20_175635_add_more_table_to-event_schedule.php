<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreTableToEventSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_schedule_times', function (Blueprint $table){
           $table->string('endDay')->nullable()->after('moreInformation');
           $table->string('eventStartEnd')->nullable()->after('endDay');
           $table->string('eventEndEnd')->nullable()->after('eventStartEnd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('event_schedule_times', function(Blueprint $table){
           $table->dropColumn('eventEndEnd');
           $table->dropColumn('eventStartEnd');
           $table->dropColumn('endDay');
        });

    }
}
