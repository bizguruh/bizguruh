<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductCategoriesForeignColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_sub_categories', function (Blueprint $table) {

            $table->unsignedInteger('sub_category_id')->after('description');

            $table->foreign('sub_category_id')->references('id')->on('product_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_sub_categories', function (Blueprint $table) {

            $table->dropForeign(['sub_category_id']);

        });
    }
}
