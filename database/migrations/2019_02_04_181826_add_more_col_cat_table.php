<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_categories', function(Blueprint $table){
            $table->string('adminCategory')->nullable()->after('description');
        });

        Schema::table('product_sub_categories', function (Blueprint $table){
            $table->string('adminCategory')->nullable()->after('description');
        });

        Schema::table('sub_categories_brand', function(Blueprint $table){
            $table->string('adminCategory')->nullable()->after('description');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('sub_categories_brand', function (Blueprint $table){
            $table->dropColumn('adminCategory');
        });


        Schema::table('product_sub_categories', function(Blueprint $table){
            $table->dropColumn('adminCategory');
        });


        Schema::table('product_categories', function(Blueprint $table){
           $table->dropColumn('adminCategory');
        });

    }
}
