<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionRowProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function(Blueprint $table){
           $table->string('subscriptionLevel')->nullable()->after('prodType');
        });

        Schema::table('subscriptions', function(Blueprint $table){
           $table->string('duration')->nullable()->after('level');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('products', function(Blueprint $table){
           $table->dropColumn(['subscriptionLevel']);
        });

        Schema::table('subscriptions', function(Blueprint $table){
           $table->dropColumn(['duration']);
        });
    }
}
