<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->unsignedInteger('typeId')->nullable();
            $table->unsignedInteger('userId')->nullable();
            $table->timestamps();
        });

        Schema::table('user_preferences', function(Blueprint $table){
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_preferences', function(Blueprint $table){
           $table->dropForeign('userId');
        });
        Schema::dropIfExists('user_preferences');
    }
}
