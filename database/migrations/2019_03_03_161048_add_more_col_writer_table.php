<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreColWriterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('article_products', function (Blueprint $table){
           $table->string('writer')->nullable()->after('featured');
           $table->text('aboutWriter')->nullable()->after('writer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('article_products', function (Blueprint $table){
           $table->dropColumn('aboutWriter');
           $table->dropColumn('writer');
        });
    }
}
