<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseWhatYouWillLearnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_what_you_will_learns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('whatYouWillLearn')->nullable();
            $table->unsignedInteger('product_course_id')->nullable();
            $table->timestamps();
        });

        Schema::table('course_what_you_will_learns', function(Blueprint $table){
            $table->foreign('product_course_id')->references('id')->on('products_courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_what_you_will_learns', function(Blueprint $table){
           $table->dropForeign(['product_course_id']);
        });
        Schema::dropIfExists('course_what_you_will_learns');
    }
}
