webpackJsonp([115],{

/***/ 1601:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1602);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("36b8863e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2231f346\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFAQsShowComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2231f346\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFAQsShowComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1602:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.titleName[data-v-2231f346] {\n        text-align: center;\n        font-weight: bold;\n        font-size: 20px;\n        text-transform: capitalize;\n}\n.askquestions[data-v-2231f346] {\n        text-align: center;\n}\n.askquestions a[data-v-2231f346] {\n        color: tomato;\n        font-size: 13px;\n}\n.viewQ[data-v-2231f346] {\n        overflow: hidden;\n}\n.inform input[data-v-2231f346] {\n        height: 53px;\n}\n.input-forms[data-v-2231f346] {\n        width: 400px;\n}\ninput[data-v-2231f346]::-webkit-input-placeholder {\n        color: #c5c5c5 !important;\n}\n.categoriesdropDown[data-v-2231f346] {\n        height: 53px;\n        width: 40px;\n        -webkit-appearance: none;\n        -moz-appearance: none;\n        appearance: none;\n        background: url(\"http://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png\") white no-repeat 50% !important;\n        border-radius: 0px !important;\n}\n.mytopic[data-v-2231f346] {\n        margin: 10px;\n}\n.img-test[data-v-2231f346] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center;\n        margin: 30px 0;\n}\n.questionA[data-v-2231f346] {\n        text-transform: none;\n}\n.section-title-wrapper[data-v-2231f346] {\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: row;\n                flex-direction: row;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        color: #505763;\n        -webkit-box-flex: 1;\n            -ms-flex: 1 1 auto;\n                flex: 1 1 auto;\n        font-size: 18px;\n        font-weight: 600;\n}\n.section-title-toggle[data-v-2231f346] {\n        color: #007791;\n        display: inline-block;\n        width: 12px;\n        height: auto !important;\n}\n.section-title-text[data-v-2231f346] {\n        font-size: 13px;\n        padding-left: 8px;\n        padding-right: 10px;\n}\n.whatLearnxss[data-v-2231f346] {\n    float: left;\n    width: 95%;\n    background-color: #ffffff;\n    border: 1px solid #DEDFE0;\n    padding: 10px 15px;\n    margin: -3px 30px;\n    font-size: 13px;\n}\n.whatLearnx[data-v-2231f346] {\n        cursor:pointer;\n        float: left;\n        width: 95%;\n        margin: 2px 30px;\n        border-radius: 5px;\n        background-color: #f9f9f9;\n        border: 1px solid #DEDFE0;\n        padding: 10px 15px;\n}\n.questiondivs[data-v-2231f346] {\n        width: 80%;\n        margin: 10px auto;\n        border: 1px solid #a3c2dc;\n        border-radius: 4px;\n}\n.questiontitle[data-v-2231f346] {\n        border-bottom: 1px solid #a3c2dc;\n        padding: 5px 4px;\n}\n.questiondiv[data-v-2231f346] {\n    margin: 10px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1603:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-f-a-qs-show-component",
    data: function data() {
        return {
            subjectMatters: [],
            epiFade: true,
            token: '',
            emailCheck: false,
            errorCheck: false,
            isLoading: false,
            status: '',
            emailContent: '',
            categorySend: '',
            id: this.$route.params.id,
            name: this.$route.params.name,
            questionsBank: [],
            pagination: {},
            myTopics: [],
            topicChange: '',
            searchItem: ''
        };
    },
    mounted: function mounted() {
        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.token = user.access_token;
            this.authenticate = true;
        }
        var data = {
            id: this.id
        };
        console.log(data);
        this.getQuestionBank();
        this.topics();
    },

    methods: {
        getQuestionBank: function getQuestionBank(page_url) {
            var _this = this;

            var vm = this;
            page_url = page_url || '/api/questionBank/' + this.id + '/question';
            axios.get(page_url).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    console.log(response);
                    _this.questionsBank = response.data.data;
                    vm.makePagination(response.data.links, response.data.meta);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        makePagination: function makePagination(link, meta) {
            var pagination = {
                current_page: meta.current_page,
                last_page: meta.last_page,
                next: link.next,
                prev: link.prev
            };

            this.pagination = pagination;
        },
        topics: function topics() {
            var _this2 = this;

            axios.get('/api/topics').then(function (response) {
                if (response.status === 200) {
                    _this2.myTopics = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },
    computed: {
        searchFilterData: function searchFilterData() {
            var _this3 = this;

            if (this.searchItem !== '') {
                return this.questionsBank.filter(function (item) {
                    return item.question.toLowerCase().includes(_this3.searchItem.toLowerCase());
                });
            }

            if (this.topicChange !== '') {
                return this.questionsBank.filter(function (item) {
                    if (_this3.topicChange === item.topic) {
                        return item;
                    }
                });
            } else {
                return this.questionsBank;
            }
        }
    }
});

/***/ }),

/***/ 1604:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "titleName" }, [_vm._v(_vm._s(_vm.name))]),
    _vm._v(" "),
    _c("div", { staticClass: "img-test" }, [
      _c("div", {}, [
        _vm.myTopics.length > 0
          ? _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.topicChange,
                    expression: "topicChange"
                  }
                ],
                staticClass: "form-control categoriesdropDown",
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.topicChange = $event.target.multiple
                      ? $$selectedVal
                      : $$selectedVal[0]
                  }
                }
              },
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._l(_vm.myTopics, function(myTopic, index) {
                  return _c(
                    "option",
                    { staticClass: "mytopic", domProps: { value: myTopic.id } },
                    [_vm._v(_vm._s(myTopic.name))]
                  )
                })
              ],
              2
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "input-forms inform" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.searchItem,
              expression: "searchItem"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", placeholder: "Search Keywords" },
          domProps: { value: _vm.searchItem },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.searchItem = $event.target.value
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "askquestions" },
      [
        _c(
          "router-link",
          { attrs: { tag: "a", to: { name: "BizguruhQuestionBank" } } },
          [_vm._v("return to BizGuruh")]
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "viewQ" },
      _vm._l(_vm.searchFilterData, function(questionBank, index) {
        return _vm.questionsBank.length > 0
          ? _c("div", { staticClass: "viewQ" }, [
              _c(
                "a",
                {
                  staticClass: " whatLearnx section-container",
                  on: {
                    click: function($event) {
                      questionBank.verify = !questionBank.verify
                    }
                  }
                },
                [
                  _c("div", { staticClass: "section-header-left" }, [
                    _c("span", { staticClass: "section-title-wrapper" }, [
                      _vm._m(1, true),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "section-title-text questionA" },
                        [_vm._v(_vm._s(questionBank.question))]
                      )
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              questionBank.verify
                ? _c("a", { staticClass: "whatLearnxss" }, [
                    questionBank.topicName !== ""
                      ? _c("div", [
                          _c("span", [
                            _c("b", [_vm._v("Concept: ")]),
                            _vm._v(" " + _vm._s(questionBank.topicName) + " ")
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", [
                      _c("span", {
                        staticClass: "questionA",
                        domProps: { innerHTML: _vm._s(questionBank.answer) }
                      })
                    ])
                  ])
                : _vm._e()
            ])
          : _c("div", [_vm._v("No FAQs for this Subject Matter")])
      }),
      0
    ),
    _vm._v(" "),
    _c("div", [
      _c(
        "nav",
        {
          staticClass: "pagination-question",
          attrs: { "aria-label": "Page navigation example" }
        },
        [
          _c("ul", { staticClass: "pagination " }, [
            _c(
              "li",
              {
                staticClass: "page-item",
                class: [{ disabled: !_vm.pagination.prev }]
              },
              [
                _c(
                  "a",
                  {
                    staticClass: "page-link",
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        return _vm.getQuestionBank(_vm.pagination.prev)
                      }
                    }
                  },
                  [_vm._v("Previous")]
                )
              ]
            ),
            _vm._v(" "),
            _c("li", { staticClass: "page-item disabled" }, [
              _c(
                "a",
                { staticClass: "page-link text-dark", attrs: { href: "#" } },
                [
                  _vm._v(
                    "Page " +
                      _vm._s(_vm.pagination.current_page) +
                      " of " +
                      _vm._s(_vm.pagination.last_page)
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass: "page-item",
                class: [{ disabled: !_vm.pagination.next }]
              },
              [
                _c(
                  "a",
                  {
                    staticClass: "page-link",
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        return _vm.getQuestionBank(_vm.pagination.next)
                      }
                    }
                  },
                  [_vm._v("Next")]
                )
              ]
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("option", [_c("i", { staticClass: "fa fa-caret-down" })])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "section-title-toggle" }, [
      _c("span", { staticClass: "section-title-toggle-plus" }, [_vm._v("+")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2231f346", module.exports)
  }
}

/***/ }),

/***/ 598:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1601)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1603)
/* template */
var __vue_template__ = __webpack_require__(1604)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2231f346"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userFAQsShowComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2231f346", Component.options)
  } else {
    hotAPI.reload("data-v-2231f346", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});