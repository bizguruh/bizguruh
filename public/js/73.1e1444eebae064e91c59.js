webpackJsonp([73],{

/***/ 1036:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1037)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1039)
/* template */
var __vue_template__ = __webpack_require__(1040)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5d2ba4ff"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/bapProgressComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d2ba4ff", Component.options)
  } else {
    hotAPI.reload("data-v-5d2ba4ff", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1037:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1038);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("61e178a0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d2ba4ff\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bapProgressComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d2ba4ff\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bapProgressComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1038:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-5d2ba4ff] {\n  padding: 25px;\n  position: relative;\n  padding-top: 65px;\n  background: #f7f8fa;\n}\n.sad[data-v-5d2ba4ff] {\n  opacity: 0.3;\n}\np[data-v-5d2ba4ff] {\n  margin-bottom: 0.8em;\n}\nh4[data-v-5d2ba4ff] {\n  text-align: center;\n}\n.topic[data-v-5d2ba4ff] {\n  font-size: 20px;\n  font-family: \"Josefin Sans\";\n}\n.templates[data-v-5d2ba4ff] {\n  display: grid;\n  grid-template-columns: auto auto;\n  grid-column-gap: 15px;\n  grid-row-gap: 15px;\n}\n.template[data-v-5d2ba4ff] {\n  height: 50px;\n  position: relative;\n  text-align: center;\n  border-radius: 4px;\n  background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(#414d57),\n    color-stop(#52616e),\n    color-stop(#647786),\n    color-stop(#778c9f),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#778c9f),\n    color-stop(#647786),\n    color-stop(#52616e),\n    to(#414d57)\n  );\n  background-image: linear-gradient(\n    to right top,\n    #414d57,\n    #52616e,\n    #647786,\n    #778c9f,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #778c9f,\n    #647786,\n    #52616e,\n    #414d57\n  );\n  -webkit-transition: all 2s;\n  transition: all 2s;\n}\n.template_text[data-v-5d2ba4ff] {\n  text-align: center;\n  position: absolute;\n  font-weight: bold;\n  font-size: 16px;\n  left: 50%;\n  margin-left: -50%;\n  top: 50%;\n  margin-top: -10px;\n  color: white;\n  font-family: \"Josefin Sans\";\n  width: 100%;\n}\n.template[data-v-5d2ba4ff]:hover {\n  background-image: -webkit-gradient(\n    linear,\n    right bottom, left top,\n    from(#a4c2db),\n    color-stop(#90abc1),\n    color-stop(#7d94a7),\n    color-stop(#6a7e8e),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#6a7e8e),\n    color-stop(#7d94a7),\n    color-stop(#90abc1),\n    to(#a4c2db)\n  );\n  background-image: linear-gradient(\n    to left top,\n    #a4c2db,\n    #90abc1,\n    #7d94a7,\n    #6a7e8e,\n    #586876,\n    #586876,\n    #586876,\n    #586876,\n    #6a7e8e,\n    #7d94a7,\n    #90abc1,\n    #a4c2db\n  );\n}\n.preview-overlay[data-v-5d2ba4ff] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  /* align-items: center; */\n  z-index: 2;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.preview[data-v-5d2ba4ff] {\n  background: white;\n  padding: 30px;\n  border-radius: 4px;\n  width: 70%;\n  margin: 70px auto;\n  margin-bottom: 0;\n  font-size: 13.5px;\n  overflow: scroll;\n  height: 80%;\n}\n.preview table[data-v-5d2ba4ff] {\n  font-size: 14px;\n}\n.resp[data-v-5d2ba4ff] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.res1[data-v-5d2ba4ff],\n.res2[data-v-5d2ba4ff] {\n  width: 50%;\n}\n.work-folder[data-v-5d2ba4ff] {\n  border: 1px solid #f7f8fa;\n  padding: 10px;\n  text-align: center;\n}\n.main-profile[data-v-5d2ba4ff] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.chatbox[data-v-5d2ba4ff] {\n  position: fixed;\n  bottom: 60px;\n  right: 30px;\n  z-index: 3;\n  background-color: #f7f8fa;\n}\n.chatIcon[data-v-5d2ba4ff] {\n  font-size: 11px;\n  margin-top: -4px;\n}\n.profile-box[data-v-5d2ba4ff] {\n  width: 25%;\n  padding: 25px;\n  background: #fff;\n}\n.progress-box[data-v-5d2ba4ff] {\n  width: 100%;\n  display: grid;\n  grid-template-rows: auto auto;\n  grid-row-gap: 15px;\n}\n.myProgress[data-v-5d2ba4ff] {\n  width: 100%;\n  background: #fff;\n}\n.myProgress-box[data-v-5d2ba4ff] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  height: 100%;\n  width: 100%;\n  font-size: 14px;\n  padding-top: 50px;\n  padding-bottom: 20px;\n  background: #fff;\n}\n.quest[data-v-5d2ba4ff] {\n  font-size: 14px;\n}\nul[data-v-5d2ba4ff] {\n  text-align: center;\n}\nul li[data-v-5d2ba4ff] {\n  display: inline-block;\n  width: 150px;\n  position: relative;\n}\nul li .fa[data-v-5d2ba4ff] {\n  padding: 15px;\n  background: #ccc;\n  border-radius: 50%;\n  color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width: 40px;\n  height: 40px;\n  margin: 0 auto;\n  font-size: 20px;\n  position: relative;\n  z-index: 1;\n}\n.faText[data-v-5d2ba4ff] {\n  position: absolute;\n  background: #f7f8fa;\n  display: none;\n  top: 39px;\n  right: 17px;\n  padding: 2px 10px;\n  font-size: 11px;\n  z-index: 1;\n  width: 70px;\n  height: 26px;\n  border-radius: 3px;\n}\nul li:hover .faText[data-v-5d2ba4ff] {\n  display: block;\n}\n.faText[data-v-5d2ba4ff]::before {\n  content: \"\";\n  position: absolute;\n  top: -5px;\n  border-top: 1px solid transparent;\n  border-left: 1px solid transparent;\n  background: #f7f8fa;\n  width: 12px;\n  height: 12px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  z-index: 1;\n}\nul li .fa[data-v-5d2ba4ff]::after {\n  content: \"\";\n  position: absolute;\n  width: 155px;\n  background: #ccc;\n  height: 5px;\n  display: block;\n  top: 50%;\n  margin-top: -2.5px;\n  z-index: -1;\n}\n.fa-user-circle[data-v-5d2ba4ff] {\n  font-size: 100px;\n}\n.progress-title[data-v-5d2ba4ff] {\n  font-size: 12px;\n  font-weight: bold;\n}\nth[data-v-5d2ba4ff] {\n  background-color: hsl(207, 43%, 20%) !important;\n  font-size: 14.5px;\n}\ntd[data-v-5d2ba4ff] {\n  text-transform: capitalize;\n  font-size: 14.5px;\n}\n.fa-1x[data-v-5d2ba4ff] {\n  font-size: 1.3em;\n}\nul li .fa.active[data-v-5d2ba4ff] {\n  background: #8e8e8e;\n}\nul li .fa.active[data-v-5d2ba4ff]::after {\n  background: #8e8e8e;\n}\nul li .fa.finished[data-v-5d2ba4ff] {\n  background: hsl(207, 43%, 20%);\n}\nul li .fa.finished[data-v-5d2ba4ff]::after {\n  background: hsl(207, 43%, 20%);\n}\n.myProgress-box .progress-1 .my_circle[data-v-5d2ba4ff] {\n}\n.mySurveys[data-v-5d2ba4ff] {\n  display: grid;\n  width: 100%;\n  grid-template-rows: auto;\n  grid-template-columns: 60% 38%;\n  grid-column-gap: 20px;\n}\n.mySurveyHistory[data-v-5d2ba4ff],\n.mySurveyScores[data-v-5d2ba4ff] {\n  text-align: center;\n  padding: 20px;\n  background: #fff;\n}\nth[data-v-5d2ba4ff],\ntd[data-v-5d2ba4ff] {\n  text-align: center;\n}\n.profile-image[data-v-5d2ba4ff] {\n  margin: 30px auto;\n  width: 150px;\n  height: 150px;\n  border-radius: 50%;\n  overflow: hidden;\n  margin-bottom: 20px;\n}\n.profile-image img[data-v-5d2ba4ff] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.info[data-v-5d2ba4ff] {\n  line-height: 30px;\n}\n.info p[data-v-5d2ba4ff] {\n  text-transform: capitalize;\n}\n@media (max-width: 768px) {\n.container-fluid[data-v-5d2ba4ff] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 15px;\n}\n.profile-box[data-v-5d2ba4ff] {\n    width: 100%;\n    padding: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.info[data-v-5d2ba4ff] {\n    width: 50%;\n}\n.progress-box[data-v-5d2ba4ff] {\n    width: 100%;\n    display: grid;\n    grid-template-rows: auto;\n    grid-row-gap: 15px;\n}\n.myProgress-box[data-v-5d2ba4ff] {\n    padding: 20px 0;\n}\n.mySurveys[data-v-5d2ba4ff] {\n    grid-template-columns: auto;\n}\n.info[data-v-5d2ba4ff] {\n    line-height: 30px;\n    font-size: 14px;\n}\nul li[data-v-5d2ba4ff] {\n    width: 95px;\n}\nul li .fa[data-v-5d2ba4ff] {\n    padding: 8px;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    width: 20px;\n    height: 20px;\n    margin: 0 auto;\n    font-size: 9px;\n}\nul li .fa[data-v-5d2ba4ff]::after {\n    width: 100px;\n    background: #ccc;\n    height: 3px;\n    display: block;\n    top: 50%;\n    margin-top: -1.5px;\n    z-index: -1;\n}\n.progress-title[data-v-5d2ba4ff] {\n    font-size: 9px;\n}\n.main-profile[data-v-5d2ba4ff] {\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n}\n}\n@media (max-width: 425px) {\n.preview[data-v-5d2ba4ff] {\n    background: white;\n    padding: 15px;\n    border-radius: 4px;\n    width: 96%;\n    margin: 90px auto;\n    margin-bottom: 0;\n    height: 80%;\n    font-size: 13px;\n    overflow: scroll;\n}\n.chatbox[data-v-5d2ba4ff] {\n    bottom: 30px;\n    right: 1px;\n}\n.container-fluid[data-v-5d2ba4ff] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    margin-top: 0;\n    padding: 10px;\n}\n.profile-box[data-v-5d2ba4ff] {\n    width: 100%;\n    padding: 10px;\n}\n.profile-image[data-v-5d2ba4ff] {\n    width: 80px;\n    height: 80px;\n}\n.info[data-v-5d2ba4ff] {\n    line-height: 28px;\n    font-size: 14px;\n    width: 70%;\n}\nul li[data-v-5d2ba4ff] {\n    width: 100px;\n}\nul li .fa[data-v-5d2ba4ff] {\n    padding: 8px;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    width: 14px;\n    height: 14px;\n    margin: 0 auto;\n    font-size: 8px;\n}\nul li .fa[data-v-5d2ba4ff]::after {\n    width: 105px;\n    background: #ccc;\n    height: 3px;\n    display: block;\n    top: 50%;\n    margin-top: -1.5px;\n    z-index: -1;\n}\nli[data-v-5d2ba4ff] {\n    margin-bottom: 25px;\n    line-height: 1.2;\n}\n.progress-title[data-v-5d2ba4ff] {\n    font-size: 9px;\n    line-height: 1.2;\n}\n.main[data-v-5d2ba4ff] {\n    padding: 0;\n    padding-top: 10px;\n}\n.work-folder[data-v-5d2ba4ff] {\n    width: 100%;\n}\n.main-profile[data-v-5d2ba4ff] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.mySurveyHistory[data-v-5d2ba4ff],\n  .mySurveyScores[data-v-5d2ba4ff] {\n    padding: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1039:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "guides-progress-component",

  data: function data() {
    return {
      mainShow: false,
      bap: false,
      preview: false,
      history: [],
      singleHistory: {},
      answer: [],
      question: [],
      questions: {},
      showChat: false,
      usersub: 0,
      all_record: [],
      active_record: {},
      active_1: false,
      pending_1: true,
      finished_1: false,
      active_2: false,
      pending_2: true,
      finished_2: false,
      active_3: false,
      pending_3: true,
      finished_3: false,
      active_4: false,
      pending_4: true,
      finished_4: false,
      active_5: false,
      pending_5: true,
      finished_5: false,
      active_6: false,
      pending_6: true,
      finished_6: false,
      template: {},
      templates: []
    };
  },
  mounted: function mounted() {
    this.getUserSub();
  },

  methods: {
    getUserSub: function getUserSub() {
      var user = JSON.parse(localStorage.getItem("authUser"));
      this.getRecords();
    },
    getRecords: function getRecords() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          _this.active_record = response.data;
          _this.switchProgress(_this.active_record.current_month);
        }
      });
      axios.get("/api/get-all-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          _this.all_record = response.data;
        }
      });
    },
    switchProgress: function switchProgress(id) {
      switch (id) {
        case "1":
          this.active_1 = this.pending_2 = this.pending_3 = this.pending_4 = this.pending_5 = this.pending_6 = true;
          this.pending_1 = this.finished_1 = false;
          this.active_2 = this.active_3 = this.active_4 = this.active_5 = this.active_6 = false;
          this.finished_1 = this.finished_2 = this.finished_3 = this.finished_4 = this.finished_5 = this.finished_6 = false;

          break;
        case "2":
          this.active_2 = this.pending_3 = this.pending_4 = this.pending_5 = this.pending_6 = this.finished_1 = true;
          this.pending_1 = this.pending_2 = false;
          this.active_1 = this.active_3 = this.active_4 = this.active_5 = this.active_6 = false;
          this.finished_2 = this.finished_3 = this.finished_4 = this.finished_5 = this.finished_6 = false;
          break;
        case "3":
          this.active_3 = this.pending_4 = this.pending_5 = this.pending_6 = this.finished_1 = this.finished_2 = true;
          this.pending_2 = this.pending_1 = this.pending_3 = false;
          this.active_1 = this.active_2 = this.active_4 = this.active_5 = this.active_6 = false;
          this.finished_3 = this.finished_4 = this.finished_5 = this.finished_6 = false;
          break;
        case "4":
          this.active_4 = this.pending_5 = this.pending_6 = this.finished_1 = this.finished_2 = this.finished_3 = true;
          this.pending_1 = this.pending_2 = this.pending_3 = this.pending_4 = false;
          this.active_1 = this.active_2 = this.active_3 = this.active_5 = this.active_6 = false;
          this.finished_4 = this.finished_5 = this.finished_6 = false;
          break;
        case "5":
          this.active_5 = this.pending_6 = this.finished_1 = this.finished_2 = this.finished_3 = this.finished_4 = true;
          this.pending_1 = this.pending_2 = this.pending_3 = this.pending_4 = this.pending_5 = false;
          this.active_1 = this.active_2 = this.active_3 = this.active_4 = this.active_6 = false;
          this.finished_5 = this.finished_6 = false;
          break;
        case "6":
          this.active_6 = this.finished_1 = this.finished_2 = this.finished_3 = this.finished_4 = this.finished_5 = true;
          this.pending_1 = this.pending_2 = this.pending_3 = this.pending_4 = this.pending_5 = this.pending_6 = false;
          this.active_1 = this.active_2 = this.active_3 = this.active_4 = this.active_5 = false;
          this.finished_6 = false;
          break;
        default:
          this.finished_1 = this.finished_2 = this.finished_3 = this.finished_4 = this.finished_5 = this.finished_6 = false;
          this.pending_1 = this.pending_2 = this.pending_3 = this.pending_4 = this.pending_5 = this.pending_6 = true;
          this.active_1 = this.active_2 = this.active_3 = this.active_4 = this.active_5 = this.active_6 = false;
          break;
      }
    }
  }
});

/***/ }),

/***/ 1040:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _c("div", { staticClass: "container-fluid flex-column" }, [
      _c("div", { staticClass: "progress-box" }, [
        _c("div", { staticClass: "myProgress rounded" }, [
          _c("h4", { staticClass: "p-2 text-left" }, [_vm._v("Your Progress")]),
          _vm._v(" "),
          _c("div", { staticClass: "myProgress-box shadow-sm" }, [
            _c("ul", [
              _c("li", [
                _vm.finished_1
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_1
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_1
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Financial Accounting")
                ]),
                _vm._v(" "),
                _vm.finished_1
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_1
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_1
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("li", [
                _vm.finished_2
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_2
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_2
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Business Education")
                ]),
                _vm._v(" "),
                _vm.finished_2
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_2
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_2
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("li", [
                _vm.finished_3
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_3
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_3
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Management System")
                ]),
                _vm._v(" "),
                _vm.finished_3
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_3
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_3
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("li", [
                _vm.finished_4
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_4
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_4
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Evaluation")
                ]),
                _vm._v(" "),
                _vm.finished_4
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_4
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_4
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("li", [
                _vm.finished_5
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_5
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_5
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Re-evaluation")
                ]),
                _vm._v(" "),
                _vm.finished_5
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_5
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_5
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("li", [
                _vm.finished_6
                  ? _c("i", {
                      staticClass:
                        "fa fa-check text-white startLine fa-2x finished"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_6
                  ? _c("i", {
                      staticClass:
                        "fa fa-refresh text-white startLine fa-2x active"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_6
                  ? _c("i", {
                      staticClass: "fa fa-times text-white startLine fa-2x"
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "progress-title" }, [
                  _vm._v("Insurance & Loans")
                ]),
                _vm._v(" "),
                _vm.finished_6
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Completed")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.active_6
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Active")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.pending_6
                  ? _c("div", { staticClass: "faText shadow-sm" }, [
                      _vm._v("Pending")
                    ])
                  : _vm._e()
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5d2ba4ff", module.exports)
  }
}

/***/ }),

/***/ 1714:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1715);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("5761467b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2650349f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./home.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2650349f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./home.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1715:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-2650349f] {\n  min-height: 100vh;\n}\n.welcome[data-v-2650349f] {\n  text-transform: capitalize;\n}\n.first_content[data-v-2650349f] {\n  padding: 15px 0;\n}\n.content[data-v-2650349f] {\n  display: grid;\n  grid-template-columns: 2fr 1fr;\n  grid-column-gap: 15px;\n  min-height: 80vh;\n}\n.grid_box[data-v-2650349f] {\n  width: 100%;\n  height: 100%;\n  background-color: white;\n}\n.first_box[data-v-2650349f] {\n  display: grid;\n}\nul[data-v-2650349f], ol[data-v-2650349f]{\n    list-style: none;\n}\nli[data-v-2650349f]{\n    margin-bottom: 14px;\n    text-align: left;\n}\n@media(max-width:768px){\n.content[data-v-2650349f] {\n  display: grid;\n  grid-template-columns: 1fr;\n  grid-template-rows: auto auto;\n  grid-column-gap: 15px;\n  min-height: 80vh;\n}\n}\n@media(max-width:768px){\n.content[data-v-2650349f] {\n  display: grid;\n  grid-template-columns: 1fr;\n  grid-template-rows: auto auto;\n  grid-column-gap: 15px;\n  min-height: 80vh;\n  padding: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1716:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bapProgressComponent__ = __webpack_require__(1036);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bapProgressComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__bapProgressComponent__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: {},
      bap: false,
      preview: false,
      history: [],
      singleHistory: {},
      answer: [],
      question: [],
      questions: {},
      template: {},
      templates: [],
      all_record: [],
      active_record: {}
    };
  },

  components: {
    Progress: __WEBPACK_IMPORTED_MODULE_0__bapProgressComponent___default.a
  },
  mounted: function mounted() {
    this.user = JSON.parse(localStorage.getItem("authUser"));
    this.getHistory();
    this.getTemplates();
    this.getRecords();
  },

  methods: _defineProperty({
    getRecords: function getRecords() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          _this.active_record = response.data;
        }
      });
      axios.get("/api/get-all-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          _this.all_record = response.data;
        }
      });
    },
    getSingleHistory: function getSingleHistory(id) {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        user_id: user.id,
        question_id: id
      };
      axios.post("/api/single/history", data).then(function (response) {
        _this2.singleHistory = response.data;
        _this2.answer = JSON.parse(response.data.answers);
      });

      axios.get("/api/questions/" + id).then(function (response) {
        _this2.question = JSON.parse(response.data.questions);
      });
    },
    getHistory: function getHistory() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/history/" + user.id).then(function (response) {
        _this3.history = response.data;
        _this3.getSingleHistory(_this3.history[0].question_id);
      });
    },
    getTemplate: function getTemplate(id) {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-template-history/" + id, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this4.template = response.data;
        _this4.questions = JSON.parse(response.data.responses);
      });
    },
    getTemplates: function getTemplates() {
      var _this5 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-templates", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this5.templates = response.data;
        _this5.getTemplate(_this5.templates[0].id);
      });
    }
  }, "getRecords", function getRecords() {
    var _this6 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    axios.get("/api/get-active-record", {
      headers: {
        Authorization: "Bearer " + user.access_token
      }
    }).then(function (response) {
      if (response.status == 200) {
        _this6.active_record = response.data;
      }
    });
    axios.get("/api/get-all-record", {
      headers: {
        Authorization: "Bearer " + user.access_token
      }
    }).then(function (response) {
      if (response.status == 200) {
        _this6.all_record = response.data;
      }
    });
  })
});

/***/ }),

/***/ 1717:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("p", { staticClass: "welcome mb-2 p-3" }, [
      _vm._v("Welcome " + _vm._s(_vm.user.name))
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "first_content" }, [_c("Progress")], 1),
    _vm._v(" "),
    _c("div", { staticClass: "content rounded" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "grid_box shadow-sm p-3" }, [
        _c("div", { staticClass: "info mb-5 text-center rounded" }, [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Information")]),
          _vm._v(" "),
          _c("ul", [
            _c("li", [
              _c("span", { staticClass: "text-muted" }, [_vm._v("Name")]),
              _vm._v(" : "),
              _c("span", { staticClass: "toCaps" }, [
                _vm._v(_vm._s(_vm.user.name))
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { staticClass: "text-muted" }, [
                _vm._v("Current month")
              ]),
              _vm._v(" : "),
              _c("span", { staticClass: "toCaps" }, [
                _vm._v(_vm._s(_vm.active_record.current_month))
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { staticClass: "text-muted" }, [
                _vm._v("Current week")
              ]),
              _vm._v(" :  "),
              _c("span", { staticClass: "toCaps" }, [
                _vm._v(_vm._s(_vm.active_record.current_week))
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { staticClass: "text-muted" }, [_vm._v("Status")]),
              _vm._v(" :  "),
              _c("span", { staticClass: "toCaps" }, [
                _vm._v(_vm._s(_vm.active_record.status))
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _c("div", { staticClass: "history w-100 text-center rounded" }, [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Completed")]),
          _vm._v(" "),
          _vm.templates.length
            ? _c("table", { staticClass: "table table-bordered" }, [
                _c(
                  "tbody",
                  _vm._l(_vm.templates, function(temp, idx) {
                    return _c("tr", { key: idx }, [
                      _c("td", { attrs: { scope: "row" } }, [
                        _c("i", {
                          staticClass: "fas fa-check-circle text-green mr-3",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v(_vm._s(temp.template) + " ")
                      ])
                    ])
                  }),
                  0
                )
              ])
            : _c("div", [
                _c("img", {
                  staticClass: "sad",
                  attrs: { src: "/images/sad.svg", alt: "" }
                }),
                _vm._v(" "),
                _c("h4", { staticClass: "text-muted form-control" }, [
                  _vm._v("No records")
                ])
              ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "grid_box shadow-sm" }, [
      _c("div", { staticClass: "intro mb-5" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2650349f", module.exports)
  }
}

/***/ }),

/***/ 624:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1714)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1716)
/* template */
var __vue_template__ = __webpack_require__(1717)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2650349f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2650349f", Component.options)
  } else {
    hotAPI.reload("data-v-2650349f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});