webpackJsonp([50],{

/***/ 1536:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1537);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("18a67a76", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f9ee8fd8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminVideoComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f9ee8fd8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminVideoComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1537:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:700);", ""]);

// module
exports.push([module.i, "\n.banner[data-v-f9ee8fd8] {\n  width: 100%;\n  background-image: url(\"/images/video-banner.jpg\");\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 420px;\n  display: grid;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.banner-text[data-v-f9ee8fd8] {\n  color: white;\n  font-size: 56px;\n  font-weight: bold;\n}\n.form-control[data-v-f9ee8fd8] {\n  border-radius: 20px;\n}\n.liked[data-v-f9ee8fd8] {\n  color: hsl(207, 43%, 20%);\n}\n.fa-stack[data-v-f9ee8fd8] {\n  font-size: 1em;\n}\ni[data-v-f9ee8fd8] {\n  vertical-align: middle;\n}\n.fa-circle[data-v-f9ee8fd8] {\n  color: hsl(207, 43%, 20%);\n}\n.aboutWriter[data-v-f9ee8fd8] {\n  width: 70%;\n  margin: 0 auto;\n  min-height: 100px;\n  background: #fff;\n  padding: 15px;\n}\nspan.hostName[data-v-f9ee8fd8] {\n  cursor: pointer;\n  color: rgba(0, 0, 0, 0.74);\n  font-weight: bold;\n}\nspan.hostName[data-v-f9ee8fd8]:hover {\n  text-decoration: underline;\n}\n.cartAdded[data-v-f9ee8fd8] {\n  position: absolute;\n  color: hsl(207, 43%, 20%);\n  top: 4px;\n  left: 4px;\n  width: 20px;\n  height: 20px;\n  z-index: 3;\n  background: #f7f8fa;\n  border-radius: 50%;\n}\n.cartA[data-v-f9ee8fd8] {\n  font-size: 11px;\n  position: absolute;\n  top: 4px;\n  left: 3px;\n}\n.addedButton[data-v-f9ee8fd8] {\n  color: rgb(255, 255, 255, 0.5) !important;\n  background: rgb(91, 132, 167, 0.5) !important;\n}\n.bottom[data-v-f9ee8fd8] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  /* background: #f7f8fa; */\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n.newButtons[data-v-f9ee8fd8] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  /* background: #f7f8fa; */\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  margin-top: 20px;\n  margin-bottom: 20px;\n  padding: 5px;\n  margin: 0 auto;\n  width: 40%;\n}\n.newButton[data-v-f9ee8fd8] {\n  position: relative;\n  width: 50px;\n  height: 50px;\n  max-width: 50px;\n  max-height: 50px;\n  border-radius: 50%;\n  color: #ffffff;\n  cursor: pointer;\n  background: hsl(207, 43%, 20%);\n  margin: 0 5px;\n}\n.newButton[data-v-f9ee8fd8]:hover {\n  -webkit-box-shadow: 2px 4px 5px 2px rgba(20, 23, 28, 0.15);\n          box-shadow: 2px 4px 5px 2px rgba(20, 23, 28, 0.15);\n  color: #ffffff;\n  background: rgb(91, 132, 167, 0.5);\n}\n.newButton[data-v-f9ee8fd8]::after {\n  -webkit-box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n          box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.newButtonClone[data-v-f9ee8fd8] {\n  position: relative;\n\n  /* margin: 0 10px; */\n}\n.newButtonClone[data-v-f9ee8fd8]:hover {\n  /* box-shadow: 2px 4px 5px 2px rgba(20, 23, 28, 0.15); */\n  color: #ffffff;\n  /* background: rgb(164, 194, 219, 0.5); */\n}\n.newButtonClone[data-v-f9ee8fd8]::after {\n  -webkit-box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n          box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.buttonText[data-v-f9ee8fd8] {\n  visibility: hidden;\n  width: 120px;\n  background-color: hsl(207, 43%, 20%);\n  font-size: 12px;\n  color: #fff;\n  text-align: center;\n  padding: 2px;\n  border-radius: 5px;\n  position: absolute;\n  z-index: 1;\n  bottom: 125%;\n  left: 50%;\n  margin-left: -75px;\n  opacity: 0;\n  -webkit-transition: opacity 0.3s;\n  transition: opacity 0.3s;\n}\n.newButton[data-v-f9ee8fd8]::after {\n  -webkit-box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n          box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.buttonText[data-v-f9ee8fd8] {\n  visibility: hidden;\n  width: 120px;\n  background-color: hsl(207, 43%, 20%);\n  font-size: 12px;\n  color: #fff;\n  text-align: center;\n  padding: 2px;\n  border-radius: 5px;\n  position: absolute;\n  z-index: 1;\n  bottom: 125%;\n  left: 50%;\n  margin-left: -75px;\n  opacity: 0;\n  -webkit-transition: opacity 0.3s;\n  transition: opacity 0.3s;\n}\n.buttonText[data-v-f9ee8fd8]::after {\n  content: \"\";\n  position: absolute;\n  top: 100%;\n  left: 50%;\n  margin-left: -5px;\n  border-width: 5px;\n  border-style: solid;\n  border-color: hsl(207, 43%, 20%) transparent transparent transparent;\n}\n.newButton:hover .buttonText[data-v-f9ee8fd8] {\n  visibility: visible;\n  opacity: 1;\n}\n.newButtonClone .buttonText[data-v-f9ee8fd8]::after {\n  content: \"\";\n  position: absolute;\n  top: 100%;\n  left: 50%;\n  margin-left: -5px;\n  border-width: 5px;\n  border-style: solid;\n  border-color: hsl(207, 43%, 20%) transparent transparent transparent;\n}\n.newButtonClone:hover .buttonText[data-v-f9ee8fd8] {\n  visibility: visible;\n  opacity: 1;\n}\n.relatedVideo[data-v-f9ee8fd8] {\n  position: relative;\n}\n.relatedVideo img[data-v-f9ee8fd8] {\n  width: 100%;\n  height: 200px;\n}\n.faBu[data-v-f9ee8fd8] {\n  padding: 15px;\n  font-size: 20px;\n}\n.strike1[data-v-f9ee8fd8] {\n  border-top: 2px solid hsl(207, 43%, 20%);\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg);\n  position: absolute;\n  top: 45%;\n  z-index: 40;\n  width: 64px;\n  left: -7px;\n}\n.strike2[data-v-f9ee8fd8] {\n  border-top: 2px solid hsl(207, 43%, 20%);\n  -webkit-transform: rotate(45deg);\n  transform: rotate(-45deg);\n  position: absolute;\n  top: 45%;\n  z-index: 40;\n  width: 64px;\n  left: -7px;\n}\np[data-v-f9ee8fd8] {\n  font-weight: normal;\n}\nul[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  list-style-type: none;\n  margin: 0;\n  margin-bottom:10px;\n  padding: 5px 0px;\n  background: #ffffff;\n}\nul > li[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  float: left;\n  height: 10px;\n  width: auto;\n  font-weight: bold;\n  font-size: 0.8em;\n  cursor: default;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul > li[data-v-f9ee8fd8]:not(:last-child)::after {\n  content: \"/\";\n  float: right;\n  font-size: 0.8em;\n  margin: 0 0.5em;\n  cursor: default;\n}\n.linked[data-v-f9ee8fd8] {\n  cursor: pointer;\n  font-size: 1em;\n  font-weight: normal;\n}\n.col-xs-8[data-v-f9ee8fd8] {\n  width: 66.66666667%;\n}\n.col-xs-4[data-v-f9ee8fd8] {\n  width: 33.33333333%;\n}\n.shareI[data-v-f9ee8fd8] {\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: pointer;\n  margin: 10px;\n  border: 1px solid hsl(207, 43%, 20%);\n}\n.share[data-v-f9ee8fd8] {\n  text-align: center;\n  color: hsl(207, 43%, 20%);\n  font-weight: bold;\n}\n.social[data-v-f9ee8fd8] {\n  text-align: center;\n  border-radius: 3px;\n  padding: 5px 0;\n  cursor: pointer;\n  margin: 10px 10px 0;\n  font-size: large;\n  color: hsl(207, 43%, 20%) !important;\n}\n.vidSearch[data-v-f9ee8fd8],\n.tagSearch[data-v-f9ee8fd8] {\n  padding: 1px 12px;\n}\n.course-Detail[data-v-f9ee8fd8] {\n  background: #fff;\n\n  margin: 0 auto !important;\n\n  font-weight: bold;\n}\n.noResult[data-v-f9ee8fd8] {\n  width: 100%;\n  font-family: fantasy;\n  text-align: center;\n  margin: auto;\n}\n.related_vid[data-v-f9ee8fd8] {\n  position: relative;\n  padding: 20px 5px;\n}\n.related_vid img[data-v-f9ee8fd8] {\n  width: 100%;\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.playCont[data-v-f9ee8fd8] {\n  position: absolute;\n  width: 90px;\n  height: 90px;\n  background: #ffffff;\n  border-radius: 50%;\n  overflow: hidden;\n  left: 50%;\n  top: 50%;\n  opacity: 0.8;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n}\n.playIcon[data-v-f9ee8fd8] {\n  position: absolute;\n  left: 54%;\n  top: 50%;\n  font-size: 40px;\n  opacity: 0.8;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  color: #000000;\n}\n.shadowA[data-v-f9ee8fd8] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  background: rgba(255, 255, 255, 0.1);\n  color: #f1f1f1;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  border-radius: 5px;\n}\n.ribbon[data-v-f9ee8fd8] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  position: absolute;\n}\n.ribbon[data-v-f9ee8fd8]::before,\n.ribbon[data-v-f9ee8fd8]::after {\n  position: absolute;\n  z-index: -1;\n  content: \"\";\n  display: block;\n  border: 5px solid #2980b9;\n}\n.ribbon span[data-v-f9ee8fd8] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 4px 0;\n  background-color: #3498db;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 700 14px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n.ribbon-free span[data-v-f9ee8fd8] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 4px 0;\n  background-color: #404346;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 500 14px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n\n/* top left*/\n.ribbon-top-left[data-v-f9ee8fd8] {\n  top: -0px;\n  left: -0px;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::before,\n.ribbon-top-left[data-v-f9ee8fd8]::after {\n  border-top-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::before {\n  top: 0;\n  right: 0;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::after {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-top-left span[data-v-f9ee8fd8] {\n  right: 9px;\n  top: 16px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  z-index: 20;\n}\n\n/* top right*/\n.ribbon-top-right[data-v-f9ee8fd8] {\n  top: -10px;\n  right: -10px;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::before,\n.ribbon-top-right[data-v-f9ee8fd8]::after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::before {\n  top: 0;\n  left: 0;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::after {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-top-right span[data-v-f9ee8fd8] {\n  left: -7px;\n  top: 22px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n/* bottom left*/\n.ribbon-bottom-left[data-v-f9ee8fd8] {\n  bottom: -10px;\n  left: -10px;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::before,\n.ribbon-bottom-left[data-v-f9ee8fd8]::after {\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::before {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::after {\n  top: 0;\n  left: 0;\n}\n.ribbon-bottom-left span[data-v-f9ee8fd8] {\n  right: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(225deg);\n          transform: rotate(225deg);\n}\n\n/* bottom right*/\n.ribbon-bottom-right[data-v-f9ee8fd8] {\n  bottom: -10px;\n  right: -10px;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::before,\n.ribbon-bottom-right[data-v-f9ee8fd8]::after {\n  border-bottom-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::before {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::after {\n  top: 0;\n  right: 0;\n}\n.ribbon-bottom-right span[data-v-f9ee8fd8] {\n  left: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(-225deg);\n          transform: rotate(-225deg);\n}\n.mobile[data-v-f9ee8fd8] {\n  display: none;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n  max-width: 1100px;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: -100px;\n  padding: 50px;\n  padding-top: 50px;\n  min-height: 70vh;\n  padding-bottom: 100px;\n  background-color: white;\n}\n.searchBarContent[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n.searchBar[data-v-f9ee8fd8] {\n  width: auto;\n  margin-left: auto;\n  margin-right: auto;\n}\ninput[data-v-f9ee8fd8]::-webkit-input-placeholder {\n  color: #c5c5c5 !important;\n}\n.smatter[data-v-f9ee8fd8] {\n  text-transform: capitalize;\n  font-size: 11px;\n  font-weight: bold;\n  color: rgba(0, 0, 0, 0.54);\n    height: 18px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.search-btn[data-v-f9ee8fd8] {\n  border: 0px;\n  color: rgba(0, 0, 0, 0.54);\n  margin: 7px 0;\n  position: absolute;\n  right: 12px;\n  top: 4px;\n}\n.search[data-v-f9ee8fd8] {\n  padding-left: 0;\n}\n.modal .row[data-v-f9ee8fd8] {\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n}\n.videoDisTile[data-v-f9ee8fd8] {\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 20px 0;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  overflow: hidden;\n  background: #ffffff;\n}\n.ddVideo[data-v-f9ee8fd8] {\n  -webkit-box-flex: 2;\n      -ms-flex-positive: 2;\n          flex-grow: 2;\n  padding: 6px 20px;\n}\n.overDesc[data-v-f9ee8fd8] {\n  background: #f7f8fa;\n  text-align: center;\n  padding: 20px;\n}\n.episodeDescription[data-v-f9ee8fd8] {\n  display: block;\n  padding: 12px 30px 12px 35px;\n  background-color: #fff;\n  border-top: none;\n  font-size: 13px;\n  color: #686f7a;\n  letter-spacing: 0.3px;\n  line-height: 1.33;\n  font-weight: normal;\n}\n.reviewAvatar[data-v-f9ee8fd8] {\n  text-transform: capitalize;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff;\n  padding: 7px 10px;\n  border-radius: 50%;\n}\n.reviewList[data-v-f9ee8fd8] {\n  margin-bottom: 20px;\n}\n.proReview[data-v-f9ee8fd8],\n.proEdi[data-v-f9ee8fd8] {\n  font-size: 13px;\n}\n.reviews[data-v-f9ee8fd8] {\n  font-weight: 500;\n}\n.floatUserRating[data-v-f9ee8fd8] {\n  margin-right: 104px;\n  margin-left: 0px;\n}\n.floatReview[data-v-f9ee8fd8] {\n  float: left;\n}\n.reviewList[data-v-f9ee8fd8] {\n  margin-bottom: 20px;\n}\n.rateStyle[data-v-f9ee8fd8] {\n  float: left;\n  margin: 2px;\n}\n.videoguest[data-v-f9ee8fd8] {\n  font-weight: bold;\n  text-transform: uppercase;\n}\n.review-div[data-v-f9ee8fd8] {\n  width: 70%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.tabre[data-v-f9ee8fd8] {\n  overflow: hidden;\n  margin-top: 10px;\n}\n.dateCreated[data-v-f9ee8fd8] {\n  font-size: 13px;\n  color: #adacac;\n  font-weight: 400;\n}\n.avatarD[data-v-f9ee8fd8] {\n  margin-right: 20px;\n  margin-top: 10px;\n}\n.reviewerName[data-v-f9ee8fd8] {\n  text-transform: capitalize;\n  margin-top: -6px;\n}\n.reviewdiv[data-v-f9ee8fd8] {\n  border-top: 1px solid #eee;\n  border-bottom: 1px solid #eee;\n  margin: 20px 0;\n  padding-bottom: 10px;\n  font-size: 14px;\n}\n.reviewAvatar[data-v-f9ee8fd8] {\n  text-transform: capitalize;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff;\n  padding: 11px 15px;\n  font-size: 15px;\n  border-radius: 50%;\n}\n.review-div[data-v-f9ee8fd8] {\n  width: 70%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.floatRT[data-v-f9ee8fd8] {\n  overflow: hidden;\n}\n.episodeTitles[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #f7f8fa;\n  border: solid 1px #f7f8fa;\n  cursor: pointer;\n  height: auto;\n  margin-top: 3px;\n  padding: 10px 30px 10px 22px;\n}\n.divC[data-v-f9ee8fd8] {\n  font-size: 40px;\n  margin: 17px 0 50px 0;\n}\n.epiPrice[data-v-f9ee8fd8] {\n  float: right;\n  margin: 0 5px;\n}\n.epiPriceDiv[data-v-f9ee8fd8] {\n  width: 100%;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-pack:space-evenly;\n      -ms-flex-pack:space-evenly;\n          justify-content:space-evenly;\n}\n.cartAdd[data-v-f9ee8fd8] {\n  background: hsl(207, 43%, 20%);\n  font-weight: bold;\n  color: #ffffff;\n  font-size: 11px;\n  padding: 3px 9px;\n  border-radius: 2px;\n}\n.cartAdd[data-v-f9ee8fd8]:hover {\n  background: rgba(164, 194, 219, 6);\n}\n.episodeDescription div[data-v-f9ee8fd8]:nth-child(1) {\n  margin-bottom: 10px;\n  font-weight: normal;\n}\n.cartAdds[data-v-f9ee8fd8] {\n  position: relative;\n  border: 1px solid hsl(207, 43%, 20%);\n  font-weight: bold;\n  color: hsl(207, 43%, 20%) !important;\n  font-size: 11px;\n  padding: 2px 9px;\n  border-radius: 2px;\n  -webkit-transition-duration: 0.4s; /* Safari */\n  transition-duration: 0.4s;\n  text-decoration: none;\n  overflow: hidden;\n  cursor: pointer;\n}\n.cartAdds[data-v-f9ee8fd8]:hover {\n  -webkit-box-shadow: 0 4px #eee;\n          box-shadow: 0 4px #eee;\n}\n.cartAdds[data-v-f9ee8fd8]:after {\n  content: \"\";\n  background: hsl(207, 43%, 20%);\n  color: #fff;\n  display: block;\n  position: absolute;\n  padding-top: 200%;\n  padding-left: 250%;\n  margin-left: -20px !important;\n  margin-top: -120%;\n  opacity: 0;\n  -webkit-transition: all 0.8s;\n  transition: all 0.8s;\n}\n.cartAdds[data-v-f9ee8fd8]:active:after {\n  padding: 0;\n  margin: 0;\n  opacity: 1;\n  -webkit-transition: 0s;\n  transition: 0s;\n}\n.shipTabSwitch[data-v-f9ee8fd8],\n.reviewTabSwitch[data-v-f9ee8fd8],\n.shareTabSwitch[data-v-f9ee8fd8],\n.episodeTabSwitch[data-v-f9ee8fd8],\n.aboutWriterTabSwitch[data-v-f9ee8fd8] {\n  background-color: hsl(207, 43%, 20%);\n  border: 1px solid hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  width: 30%;\n  margin: 20px 0;\n  padding: 10px;\n  cursor: pointer;\n}\n.shipTab[data-v-f9ee8fd8],\n.reviewTab[data-v-f9ee8fd8],\n.shareTab[data-v-f9ee8fd8],\n.episodeTab[data-v-f9ee8fd8],\n.aboutWriterTab[data-v-f9ee8fd8] {\n  color: #a4a4a4;\n  border: 1px solid #a4a4a4;\n  width: 30%;\n  margin: 20px 0;\n  padding: 10px;\n  cursor: pointer;\n}\n.Videorow[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n}\n.videoRelated[data-v-f9ee8fd8] {\n  text-align: left;\n  margin: 15px 0;\n  line-height: 1.2;\n}\n.videoRelated select[data-v-f9ee8fd8] {\n  width: 125px;\n}\n.videoRelated p[data-v-f9ee8fd8] {\n  font-weight: bold;\n  font-size: 13px;\n}\n.btn-guruh-cart[data-v-f9ee8fd8] {\n  background-color: hsl(207, 43%, 20%) !important;\n  padding: 7px 16px;\n  width: 100%;\n  text-transform: capitalize;\n  border: none;\n  -webkit-box-shadow: 0 4px #eee;\n          box-shadow: 0 4px #eee;\n  color: #fff !important;\n}\n.btn-guruh-cart[data-v-f9ee8fd8]:hover {\n  background-color: rgba(164, 194, 219, 0.64) !important ;\n}\n.btn-guruh-cart[data-v-f9ee8fd8]:active {\n  background-color: hsl(207, 43%, 20%);\n  -webkit-box-shadow: 0 2px #eee;\n          box-shadow: 0 2px #eee;\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.btn-guruh-watch[data-v-f9ee8fd8] {\n  /* padding: 11px 0 !important; */\n  padding: 7px 16px;\n  width: 100%;\n  text-transform: capitalize;\n}\n.hardCover[data-v-f9ee8fd8],\n.softCover[data-v-f9ee8fd8] {\n  cursor: pointer;\n  border: 1px solid hsl(207, 43%, 20%);\n  padding: 11px 0 !important;\n}\n.productNames[data-v-f9ee8fd8] {\n  font-size: 11px;\n  text-align: center;\n  font-weight: bold;\n  line-height: 15px;\n}\n.product-dialog[data-v-f9ee8fd8] {\n  width: 90%;\n  max-width: 90%;\n  margin-top: 130px;\n}\n.PriceDIV[data-v-f9ee8fd8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.videoDiver[data-v-f9ee8fd8] {\n  position: relative;\n  padding: 5px 35px;\n  margin: 0 20px;\n  background: hsl(207, 43%, 20%) !important;\n  font-weight: 400;\n  cursor: pointer;\n  text-transform: capitalize;\n  color: #fff !important;\n  -webkit-transition-duration: 0.4s; /* Safari */\n  transition-duration: 0.4s;\n  text-decoration: none;\n  overflow: hidden;\n  float: right;\n  font-size: 15px;\n}\n.videoDiver[data-v-f9ee8fd8]::after {\n  content: \"\";\n  background: rgba(164, 194, 219, 0.7);\n  display: block;\n  position: absolute;\n\n  opacity: 0;\n  -webkit-transition: all 0.8s;\n  transition: all 0.8s;\n}\n.videoDiver[data-v-f9ee8fd8]:active::after {\n  opacity: 1;\n  -webkit-transition: 0s;\n  transition: 0s;\n}\n.videoDiver a[data-v-f9ee8fd8] {\n  color: #fff !important;\n}\n.videoDiver a[data-v-f9ee8fd8]:hover {\n  color: #fff !important;\n}\n.related_vid[data-v-f9ee8fd8] {\n  padding-right: 0;\n}\n.videoDiver[data-v-f9ee8fd8]:hover {\n  color: #ffffff !important;\n  background: rgba(164, 194, 219, 0.6) !important;\n}\n.videoOverview[data-v-f9ee8fd8] {\n  color: rgba(0, 0, 0, 0.64);\n  height: 70px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 3;\n  -moz-line-clamp: 3;\n  -ms-line-clamp: 3;\n  -o-line-clamp: 3;\n  line-clamp: 3;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  margin-bottom: 10px;\n  font-weight: normal;\n}\n.videoCat[data-v-f9ee8fd8] {\n  color: #b6cfe4;\n}\n.videoTitle[data-v-f9ee8fd8] {\n  color: hsl(207, 43%, 20%);\n  text-transform: capitalize;\n  line-height: 1.2;\n  margin-bottom: 8px;\n  max-height: 50px;\n  overflow: hidden;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  line-clamp: 2;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  display: -webkit-box;\n  text-overflow: ellipsis;\n  white-space: normal;\n  -webkit-box-orient: vertical;\n  font-weight: bold;\n}\n.videoTitle[data-v-f9ee8fd8]:hover {\n  color: rgba(0, 0, 0, 0.84);\n}\n.vid[data-v-f9ee8fd8] {\n  text-align: center;\n  position: relative;\n}\n.vid video[data-v-f9ee8fd8] {\n  width: 100%;\n}\n.rowBanner[data-v-f9ee8fd8] {\n  margin-left: auto;\n  width:100%\n}\n.hardCopy[data-v-f9ee8fd8],\n.softCopy[data-v-f9ee8fd8],\n.readOnline[data-v-f9ee8fd8] {\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  font-weight: bold;\n  padding: 0 10px;\n  margin: 10px;\n  cursor: pointer;\n  text-align: center;\n}\n.vidContainer[data-v-f9ee8fd8] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 60%;\n}\n.videoInfo[data-v-f9ee8fd8] {\n  background: #f7f8fa;\n  padding: 20px 15px;\n  text-align: left;\n  width: 40%;\n  margin-left: auto;\n}\n.hardpriceCont[data-v-f9ee8fd8],\n.softpriceCont[data-v-f9ee8fd8],\n.readOnlinepriceCont[data-v-f9ee8fd8] {\n  border: 2px solid hsl(207, 43%, 20%);\n  padding: 0 10px;\n  color: hsl(207, 43%, 20%);\n  margin: 10px;\n  cursor: pointer;\n  text-align: center;\n}\n.videoTitles[data-v-f9ee8fd8] {\n  font-size: 1.6rem;\n  margin-bottom: 15px;\n}\n.btn-reviews[data-v-f9ee8fd8] {\n  background-color: hsl(207, 43%, 20%) !important;\n  padding: 10px 20px;\n}\n@media (min-width: 768px) and (max-width: 1024px) {\n.banner[data-v-f9ee8fd8] {\n    height: 350px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 34px;\n}\n.vidContainer[data-v-f9ee8fd8] {\n    width: 60%;\n}\n.videoInfo[data-v-f9ee8fd8] {\n    width: 40%;\n}\n}\n@media only screen and(max-width: 768px) {\n.banner[data-v-f9ee8fd8] {\n    height: 350px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 24px;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n    padding-left: 30px;\n    padding-right: 30px;\n    margin-top:0;\n}\n.videoInfo[data-v-f9ee8fd8] {\n    background: #f7f8fa;\n    padding: 20px 20px;\n    text-align: left;\n    width: 100%;\n}\n}\n@media (max-width: 768px) {\n.banner[data-v-f9ee8fd8] {\n    height: 350px;\n}\n.related_txt[data-v-f9ee8fd8] {\n    padding: 20px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 24px;\n}\n.vidContainer[data-v-f9ee8fd8] {\n    width: 100%;\n}\n.videoInfo[data-v-f9ee8fd8] {\n    background: #f7f8fa;\n    padding: 20px 15px;\n    text-align: left;\n    width: 100%;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    display: none;\n    padding: 3px 10px;\n\n    /* margin: 0 20px 15px 20px; */\n    text-align: center;\n}\n.mobile[data-v-f9ee8fd8] {\n    display: block;\n}\n.desktop[data-v-f9ee8fd8]{\n  display:none;\n}\n.col-sm-9[data-v-f9ee8fd8],\n  .col-md-9[data-v-f9ee8fd8],\n  .col-lg-9[data-v-f9ee8fd8] {\n    padding-left: 0;\n    padding-right: 0;\n}\n.videoInfo[data-v-f9ee8fd8] {\n    font-size: 14px;\n}\n.videoTitles[data-v-f9ee8fd8] {\n    font-size: 16px;\n    font-weight: 600;\n}\n.overview-buttons[data-v-f9ee8fd8] {\n}\n.epiPriceDiv[data-v-f9ee8fd8] {\n    margin-left: auto;\n}\n.shipTabSwitch[data-v-f9ee8fd8],\n  .reviewTabSwitch[data-v-f9ee8fd8],\n  .shareTabSwitch[data-v-f9ee8fd8],\n  .episodeTabSwitch[data-v-f9ee8fd8],\n  .aboutWriterTabSwitch[data-v-f9ee8fd8] {\n    font-size: 11px;\n}\n.shipTab[data-v-f9ee8fd8],\n  .reviewTab[data-v-f9ee8fd8],\n  .shareTab[data-v-f9ee8fd8],\n  .episodeTab[data-v-f9ee8fd8],\n  .aboutWriterTab[data-v-f9ee8fd8] {\n    font-size: 11px;\n}\n.episodeTitles[data-v-f9ee8fd8] {\n    padding: 0;\n}\n.cartAdds[data-v-f9ee8fd8] {\n    font-size: 8px;\n    padding: 1px 9px;\n}\n.epiPrice[data-v-f9ee8fd8] {\n    font-size: 8px;\n    margin: 5px 5px;\n}\n.cartAdd[data-v-f9ee8fd8] {\n    padding: 2px 9px;\n}\n}\n@media only screen and(max-width: 425px) {\n.banner[data-v-f9ee8fd8] {\n    height: 200px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 24px;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n    padding-left: 15px;\n    padding-right: 15px;\n}\n.courseDetail[data-v-f9ee8fd8] {\n    margin-top: 140px;\n}\ndiv.vidBanner.video-banner[data-v-f9ee8fd8] {\n    display: none;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    font-size: 12px;\n    margin: 0;\n    padding: 3px 10px;\n}\n.form-control[data-v-f9ee8fd8] {\n    height: 20px !important;\n    padding: 2px 12px !important;\n    font-size: 8px !important;\n    line-height: 1.42857143;\n    border-radius: 20px;\n}\n.charts[data-v-f9ee8fd8] {\n    padding: 70px 10px 10px !important;\n}\n.videoDisTile[data-v-f9ee8fd8] {\n    padding: 1px;\n    font-size: 14px;\n    height: auto;\n    margin: 10px 0;\n}\nvideo[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    padding: 3px 10px;\n    font-size: 8px;\n    /* margin: 0 20px 15px 20px; */\n    text-align: center;\n    border-radius: 5px;\n}\n.episodeTitles[data-v-f9ee8fd8] {\n    padding: 0;\n}\n.cartAdds[data-v-f9ee8fd8] {\n    font-size: 8px;\n}\n.epiPrice[data-v-f9ee8fd8] {\n    font-size: 8px;\n}\n}\n@media only screen and (device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) {\n.banner[data-v-f9ee8fd8] {\n    height: 250px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 24px;\n}\n.video-js[data-v-f9ee8fd8] {\n    font-size: 5px !important;\n}\n.charts[data-v-f9ee8fd8] {\n    padding: 30px 10px;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n    padding-left: 0px;\n    padding-right: 0px;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    display: none;\n    padding: 3px 10px;\n    font-size: 8px;\n    /* margin: 0 20px 15px 20px; */\n    text-align: center;\n    border-radius: 5px;\n}\nvideo[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n}\n.mobile[data-v-f9ee8fd8] {\n    display: block;\n}\n}\n@media (max-width: 425px) {\n.rowBanner[data-v-f9ee8fd8] {\n    margin: 15px 0;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n    margin-top: 0;\n    padding-top: 30px;\n}\n.banner[data-v-f9ee8fd8] {\n    height: 250px;\n}\n.banner-text[data-v-f9ee8fd8] {\n    font-size: 24px;\n}\n.relatedVideo img[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n}\n.playCont[data-v-f9ee8fd8] {\n    width: 50px;\n    height: 50px;\n}\n.playIcon[data-v-f9ee8fd8] {\n    font-size: 25px;\n}\n.bottom[data-v-f9ee8fd8] {\n    padding: 5px;\n    margin-top: 0;\n}\n.newButtonClone[data-v-f9ee8fd8] {\n    margin: 0 10px;\n}\n.faBu[data-v-f9ee8fd8] {\n    padding: 0;\n    font-size: 11px;\n    position: absolute;\n    top: 30%;\n    right: 24%;\n}\n.upgrade[data-v-f9ee8fd8] {\n    width: 95%;\n    right: 2%;\n}\nul[data-v-f9ee8fd8] {\n    margin: 15px 0;\n}\n.course-Detail[data-v-f9ee8fd8] {\n    padding: 0;\n    background: #f7f8fa;\n}\n.related_vid img[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100%;\n}\n.related_txt[data-v-f9ee8fd8] {\n    padding: 15px 8px 10px;\n}\n.rowcontainer[data-v-f9ee8fd8] {\n    padding-left: 15px;\n    padding-right: 15px;\n}\n.ribbon[data-v-f9ee8fd8] {\n    width: 150px;\n    height: 150px;\n    overflow: hidden;\n    position: absolute;\n}\n.ribbon[data-v-f9ee8fd8]::before,\n  .ribbon[data-v-f9ee8fd8]::after {\n    position: absolute;\n    z-index: -1;\n    content: \"\";\n    display: block;\n    border: 5px solid #2980b9;\n}\n.ribbon span[data-v-f9ee8fd8] {\n    position: absolute;\n    display: block;\n    width: 75px;\n    padding: 1px 0;\n    background-color: #3498db;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 700 12px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n.ribbon-free span[data-v-f9ee8fd8] {\n    position: absolute;\n    display: block;\n    width: 75px;\n    padding: 2px 0;\n    background-color: #404346;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 500 12px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n\n  /* top left*/\n.ribbon-top-left[data-v-f9ee8fd8] {\n    top: 0px;\n    left: -78px;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::before,\n  .ribbon-top-left[data-v-f9ee8fd8]::after {\n    border-top-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::before {\n    top: 0;\n    right: 0;\n}\n.ribbon-top-left[data-v-f9ee8fd8]::after {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-top-left span[data-v-f9ee8fd8] {\n    right: 14px;\n    top: 14px;\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n}\n\n  /* top right*/\n.ribbon-top-right[data-v-f9ee8fd8] {\n    top: -10px;\n    right: -10px;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::before,\n  .ribbon-top-right[data-v-f9ee8fd8]::after {\n    border-top-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::before {\n    top: 0;\n    left: 0;\n}\n.ribbon-top-right[data-v-f9ee8fd8]::after {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-top-right span[data-v-f9ee8fd8] {\n    left: -7px;\n    top: 22px;\n    -webkit-transform: rotate(45deg);\n            transform: rotate(45deg);\n}\n\n  /* bottom left*/\n.ribbon-bottom-left[data-v-f9ee8fd8] {\n    bottom: -10px;\n    left: -10px;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::before,\n  .ribbon-bottom-left[data-v-f9ee8fd8]::after {\n    border-bottom-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::before {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-bottom-left[data-v-f9ee8fd8]::after {\n    top: 0;\n    left: 0;\n}\n.ribbon-bottom-left span[data-v-f9ee8fd8] {\n    right: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(225deg);\n            transform: rotate(225deg);\n}\n\n  /* bottom right*/\n.ribbon-bottom-right[data-v-f9ee8fd8] {\n    bottom: -10px;\n    right: -10px;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::before,\n  .ribbon-bottom-right[data-v-f9ee8fd8]::after {\n    border-bottom-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::before {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-bottom-right[data-v-f9ee8fd8]::after {\n    top: 0;\n    right: 0;\n}\n.ribbon-bottom-right span[data-v-f9ee8fd8] {\n    left: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(-225deg);\n            transform: rotate(-225deg);\n}\ndiv.vidBanner.video-banner[data-v-f9ee8fd8] {\n    display: none;\n}\np[data-v-f9ee8fd8] {\n    margin-bottom: 8px;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    font-size: 12px;\n    margin: 0;\n    color: #fff !important;\n    padding: 5px;\n}\n.form-control[data-v-f9ee8fd8] {\n    height: 30px !important;\n    padding: 2px 12px !important;\n    font-size: 12px !important;\n    line-height: 1.42857143;\n    padding: 20px;\n}\n.charts[data-v-f9ee8fd8] {\n    padding: 70px 10px 10px !important;\n}\nbutton.btn.btn-sm[data-v-f9ee8fd8] {\n    font-size: 12px;\n    padding: 2px 15px;\n    text-transform: capitalize;\n}\n.fa-search[data-v-f9ee8fd8] {\n    font-size: 11px;\n}\n.search-btn[data-v-f9ee8fd8] {\n    top: 2px !important;\n}\n.video-js[data-v-f9ee8fd8] {\n    font-size: 6px !important;\n}\nvideo[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n}\n.charts[data-v-f9ee8fd8] {\n    padding: 15px 10px;\n}\n.ddVideo[data-v-f9ee8fd8] {\n    padding: 2px 0;\n}\n.videoDisTile[data-v-f9ee8fd8] {\n    padding: 0;\n    font-size: 14px;\n\n    margin: 10px 0;\n}\n.videoOverview[data-v-f9ee8fd8] {\n    height: 40px;\n    line-height: 1.4;\n    display: -webkit-box !important;\n    -webkit-line-clamp: 2;\n    -moz-line-clamp: 2;\n    -ms-line-clamp: 2;\n    -o-line-clamp: 2;\n    line-clamp: 2;\n    -webkit-box-orient: vertical;\n    -ms-box-orient: vertical;\n    -o-box-orient: vertical;\n    box-orient: vertical;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: normal;\n    margin-bottom: 6px;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    display: none;\n    padding: 2px 5px;\n    font-size: 8px;\n    text-align: center;\n}\n.mobile[data-v-f9ee8fd8] {\n    display: block;\n}\np.videoOverview[data-v-f9ee8fd8] {\n    font-size: 14px;\n    line-height: 1.4;\n}\np.videoTitle[data-v-f9ee8fd8] {\n    font-size: 14px;\n}\np.videoCat[data-v-f9ee8fd8] {\n    font-size: 8px;\n}\n.episodeTitles[data-v-f9ee8fd8] {\n    padding: 0;\n}\n}\n@media (max-width: 375px) {\n.rowcontainer[data-v-f9ee8fd8] {\n    padding-left: 0px;\n    padding-right: 0px;\n}\n.video-js[data-v-f9ee8fd8] {\n    font-size: 5px !important;\n}\nvideo[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n}\n.videoDisTile[data-v-f9ee8fd8] {\n    margin: 12px;\n\n    font-size: 8px;\n}\nvideo[data-v-f9ee8fd8] {\n    width: 100%;\n    height: 100px;\n    font-size: 5px;\n}\n.videoDiver[data-v-f9ee8fd8] {\n    display: none;\n    padding: 3px 10px;\n    font-size: 8px;\n    /* margin: 0 20px 15px 20px; */\n    text-align: center;\n    border-radius: 5px;\n}\n.mobile[data-v-f9ee8fd8] {\n    display: block;\n}\n.related_text[data-v-f9ee8fd8]{\n    padding:8px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1538:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-admin-video-component",
  data: function data() {
    return {
      inCart: false,
      url: "https://bizguruh.com/videos",
      loading: "",
      videos: [],
      videoTag: [],
      epiFade: true,
      searchItem: "",
      epiModal: true,
      videoDetail: {},

      player: "",
      relatedVideos: [],
      anonymousCart: [],
      relatedPlaylist: "",
      authenticate: false,
      price: "",
      title: "",
      shipTab: true,
      reviewTabSwitch: false,
      shareTabSwitch: false,
      episodeTabSwitch: true,
      reviewTab: true,
      shareTab: true,
      episodeadded: null,
      aboutWriterTabSwitch: false,
      aboutWriterTab: true,
      episodeTab: true,
      shipTabSwitch: false,
      episode: true,
      shipping: false,
      reviews: false,
      share: false,
      aboutWriter: false,
      allReviews: [],
      review: {
        productId: "",
        title: "",
        description: "",
        rating: 0
      },
      star: 0,
      NotratedOne: true,
      NotratedTwo: true,
      RatedTwo: false,
      RatedOne: false,
      NotratedThree: true,
      RatedThree: false,
      NotratedFour: true,
      RatedFour: false,
      NotratedFive: true,
      RatedFive: false,
      rateFa: true,
      episodeArray: [],
      isActive: true,
      breadcrumbList: [],

      user: {},
      email: "",
      user_id: "",
      loginCart: [],
      hardCopy: false,
      hardpriceCont: false,
      softCopy: false,
      softpriceCont: true,
      readOnline: false,
      readOnlinepriceCont: true,
      purchaseCheck: false,
      subjectMatter: "",
      industry_name: "",
      startDate: null,
      startDateParsed: null,
      currentDate: null,
      endDate: null,
      freeTrial: true,
      vendorInfo: [],
      checkLibrary: [],
      inLibrary: false,
      usersub: null,
      showUpgrade: false,
      added: false,
      wishes: 0,
      swiperOption: {
        slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
          320: {
            slidesPerView: 1,
            spaceBetween: 5
          },

          425: {
            slidesPerView: 2,
            spaceBetween: 5
          },
          640: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        },
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        }
      },
      isLiked: false,
      isUnliked: false,
      allLikes: [],
      allUnlikes: [],
      update: 0
    };
  },
  components: {
    "app-videoBanner": __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent___default.a,
    SocialSharing: __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default.a

  },

  mounted: function mounted() {
    var _this = this;

    this.getVideo();
    this.updateList();

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      this.email = user.email;
      this.user_id = user.id;
      this.user = user;
      this.authenticate = true;
    }

    if (this.filterItem.length === 0) {
      this.loading = "loading content ..";
      setTimeout(function () {
        _this.loading = "No content ..";
      }, 30000);
    }
  },

  computed: {
    myLikes: function myLikes() {
      return this.allLikes.length;
    },
    myUnLikes: function myUnLikes() {
      return this.allUnlikes.length;
    },
    filterItem: function filterItem() {
      var _this2 = this;

      if (this.searchItem !== "") {
        return this.videos.filter(function (item) {
          return item.title.toLowerCase().includes(_this2.searchItem.toLowerCase());
        });
      } else {
        return this.videos;
      }
    }
  },
  watch: {
    $route: function $route() {
      this.updateList();
    },

    update: "updateThis"
  },
  methods: {
    getVideo: function getVideo() {
      var _this3 = this;

      axios.get("/api/admin/product/bizguruh/Videos").then(function (response) {
        if (response.status === 200) {
          var product = response.data.data.reverse();

          product.forEach(function (item, index) {
            item.webinar.category = item.category.name;
            item.webinar.uid = item.id;
            item.webinar.prodCategoryType = item.prodCategoryType;
            item.webinar.poster = item.coverImage;
            item.webinar.subLevel = item.subscriptionLevel;
            item.webinar.videoPrice = item.videoPrice;
            item.webinar.streamPrice = item.streamPrice;
            item.webinar.subscribePrice = item.subscribePrice;
            item.webinar.subjectMatter = item.subjectMatter.name.toLowerCase();
            // item.webinar.industryName = item.industry.name;


            _this3.isActive = false;
            _this3.videos.push(item.webinar);
          });
        }
      });
    },
    shareNo: function shareNo() {
      this.$emit("interaction", vendorInfo.id, "shares");
    },
    updateThis: function updateThis() {
      this.getLike(this.videoDetail.product_id);
      this.getUnLike(this.videoDetail.product_id);
    },
    checkLike: function checkLike(user, product) {
      var _this4 = this;

      axios.get("/api/get-likes/" + user + "/" + product).then(function (response) {
        if (response.status === 200) {
          if (response.data.like == 1 && response.data.unlike == 0) {
            _this4.isLiked = true;
            _this4.isUnliked = false;
          }
          if (response.data.unlike == 1 && response.data.like == 0) {
            _this4.isLiked = false;
            _this4.isUnliked = true;
          }
          if (response.data.unlike == 0 && response.data.like == 0) {
            _this4.isLiked = false;
            _this4.isUnliked = false;
          }
        }
      });
    },
    getLike: function getLike(product) {
      var _this5 = this;

      axios.get("/api/get-all-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this5.allLikes = response.data;
        }
      });
    },
    getUnLike: function getUnLike(product) {
      var _this6 = this;

      axios.get("/api/get-un-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this6.allUnlikes = response.data;
        }
      });
    },
    like: function like(user, product, vendor, type) {
      if (!this.isLiked) {
        this.update++;
        this.isLiked = true;
        this.isUnliked = false;
        this.$emit("likes", user, product, vendor, type);
        this.$emit("interaction", vendor, "likes");
      }
    },
    unlike: function unlike(user, product, vendor, type) {
      if (!this.isUnliked) {
        this.update++;
        this.isLiked = false;
        this.isUnliked = true;
        this.$emit("likes", user, product, vendor, type);
      }
    },
    isEmpty: function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
      }
      return true;
    },
    shareNow: function shareNow() {
      this.share = !this.share;
    },
    loader: function loader() {
      this.isActive = true;
    },
    checkEpisodeInLibrary: function checkEpisodeInLibrary(id) {
      var _this7 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/user/orders", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        response.data.data.forEach(function (element) {
          element.orderDetail.forEach(function (item) {
            if (item.type === "VIDEOS") {
              _this7.checkLibrary.push(item);
            }
          });
        });
        _this7.checkLibrary.forEach(function (element) {
          if (Number(element.itemPurchase) === Number(id)) {
            _this7.episodeadded = Number(id);
          }
        });
      });
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    searchDataItem: function searchDataItem() {
      var _this8 = this;

      this.$emit("sendSearchData", this.searchItem);
      this.loading = "loading content";
      if (this.filterItem.length === 0) {
        setTimeout(function () {
          _this8.loading = "No search result ..";
        }, 3000);
      }
    },
    wishlist: function wishlist(product) {
      var _this9 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      axios.get("/api/wish-list/" + product, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status = 200) {
          if (response.data === true) {
            _this9.added = true;
          } else {
            _this9.added = false;
          }
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    addingToWishlist: function addingToWishlist(product) {
      var _this10 = this;

      if (this.authenticate === false) {
        this.$toasted.error("Login to add to wishlist");
      } else {
        var user = JSON.parse(localStorage.getItem("authUser"));

        var data = {
          userId: this.user.id,
          productId: product,
          vendorId: 1
        };

        axios.get("/api/wish-list/" + product, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          if (response.status = 200) {
            if (response.data === true) {
              _this10.$toasted.error("Already added to wishlist");
            } else {
              axios.post("/api/wishlist", JSON.parse(JSON.stringify(data)), {
                headers: {
                  Authorization: "Bearer " + _this10.user.access_token
                }
              }).then(function (response) {
                if (response.status === 201) {
                  _this10.$toasted.success("Successfully added to wishlist");
                } else {
                  _this10.$toasted.error("Already added to wishlist");
                }
              });
            }
          }
        });
      }
    },
    addToWishlist: function addToWishlist() {
      var _this11 = this;

      if (this.authenticate === false) {
        this.$toasted.error("Login to add to wishlist");
      } else {
        var data = {
          userId: this.user_id,
          productId: this.videoDetail.uid,
          vendorId: 1
        };
        if (this.added) {
          this.$toasted.error("Already added to wishlist");
        } else {
          axios.post("/api/wishlist", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this11.added = true;
              _this11.$toasted.success("Successfully added to wishlist");
            } else {
              _this11.$toasted.error("Already added to wishlist");
            }
          }).catch(function (error) {
            console.log(error);
          });
        }
      }
    },
    AddVideoToLibrary: function AddVideoToLibrary(id, name) {
      var _this12 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Subscription Video",
        name: name
      };

      if (this.freeTrial === true || this.usersub > 0) {
        if (this.usersub >= this.videoDetail.subLevel) {
          axios.post("/api/user/sendvideotolibrary", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this12.$toasted.success("Successfully added to BizLibrary");
              _this12.inLibrary = true;
            } else if (response.data == "Order Already Added") {
              _this12.$toasted.error("You have already added this product before");
            } else if (response.data.status === 99 || response.data === " " || response.data.data === "error") {
              _this12.showUpgrade = true;
            } else {
              _this12.showUpgrade = true;
            }
          }).catch(function (error) {
            if (error.response.data.message === "Unauthenticated.") {
              _this12.$router.push({
                name: "auth",
                params: { name: "login" },
                query: { redirect: _this12.$router.fullPath }
              });
            } else {
              _this12.$toasted.error(error.response.data.message);
            }
          });
        } else {
          this.showUpgrade = true;
        }
      } else {
        this.showUpgrade = true;
      }
    },
    getProductReviews: function getProductReviews(id) {
      var _this13 = this;

      var data = {
        id: id
      };
      axios.post("/api/product/get-reviews", JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          _this13.allReviews = response.data.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    watchEpisodeNow: function watchEpisodeNow(e, vid, id, price) {
      var _this14 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Stream Subscription Episode Video",
        type: "EV",
        vid: vid
      };
      if (this.freeTrial === true || this.usersub >= this.videoDetail.subLevel) {
        axios.post("/api/user/watchnowepisodemedia", JSON.parse(JSON.stringify(data)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          localStorage.removeItem("mediaStream");

          if (response.status === 200) {
            if (response.data === false) {
              _this14.showUpgrade = true;
            } else {
              localStorage.setItem("mediaStream", JSON.stringify(data));
              var routeData = _this14.$router.resolve({
                name: "WatchMedia",
                params: {
                  id: _this14.videoDetail.uid,
                  prodtype: "Videos",
                  gettype: "insight-streaming-video",
                  type: "AV",
                  vid: "videos"
                }
              });
              window.open(routeData.href);
            }
          }
        }).catch(function (error) {
          if (error.response.data.message === "Unauthenticated.") {
            _this14.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: _this14.$router.fullPath }
            });
          } else {
            _this14.$toasted.error(error.response.data.message);
          }
        });
      } else {
        this.showUpgrade = true;
      }
    },
    sendEpisodeToCart: function sendEpisodeToCart(vid, id, name) {
      var _this15 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Subscription Episode Video",
        vid: vid,
        name: name
      };

      if (this.freeTrial === true || this.usersub >= this.videoDetail.subLevel) {
        axios.post("/api/user/sendvideoepisodetolibrary", JSON.parse(JSON.stringify(data)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          if (response.status === 201) {
            _this15.$toasted.success("Successfully added to BizLibrary");
            _this15.checkEpisodeInLibrary(vid);
          } else if (response.data == "you have added this product already") {
            _this15.$toasted.error("You have already added this product before");
          } else if (response.data.status === 99 || response.data === " " || response.data.data === "error") {
            _this15.showUpgrade = true;
          } else {
            _this15.showUpgrade = true;
          }
        }).catch(function (error) {
          console.log(error);
          if (error.response.data.message === "Unauthenticated.") {
            _this15.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: _this15.$router.fullPath }
            });
          } else {
            _this15.$toasted.error(error.response.data.message);
            console.log(error.response.data.message);
          }
        });
      } else {
        //        this.$toasted.error(
        // 	 	"Library Unavailable for your subscription level"
        //   );

        this.showUpgrade = true;
      }
    },
    showTabs: function showTabs(params) {
      switch (params) {
        case "shipping":
          this.shipping = this.shipTabSwitch = this.reviewTab = this.shareTab = this.aboutWriterTab = true;
          this.reviews = this.aboutWriter = this.episode = this.shipTab = this.reviewTabSwitch = this.shareTabSwitch = this.shareTabSwitch = this.share = this.episodeTabSwitch = this.aboutWriter = this.aboutWriterTabSwitch = false;
          break;
        case "reviews":
          this.reviewTabSwitch = this.reviews = this.shipTab = this.shareTab = this.aboutWriterTab = true;
          this.shipTabSwitch = this.aboutWriter = this.episode = this.reviewTab = this.shipping = this.shareTabSwitch = this.share = this.episodeTabSwitch = this.aboutWriterTabSwitch = false;
          break;
        case "share":
          this.shipping = this.aboutWriter = this.episode = this.reviews = this.reviewTabSwitch = this.shipTab = this.shareTab = this.shipTabSwitch = this.episodeTabSwitch = this.aboutWriterTabSwitch = false;
          this.share = this.shareTabSwitch = this.reviewTab = this.shipTab = this.aboutWriterTab = true;
          break;
        case "episode":
          this.shipping = this.aboutWriter = this.share = this.reviews = this.reviewTabSwitch = this.shipTabSwitch = this.shareTabSwitch = false;
          this.episode = this.episodeTabSwitch = this.reviewTab = this.shipTab = this.shareTab = true;
          break;
        case "aboutWriter":
          this.aboutWriter = this.aboutWriterTabSwitch = this.shareTab = this.reviewTab = true;
          this.aboutWriterTab = this.shareTabSwitch = this.reviewTabSwitch = this.episodeTabSwitch = this.shipping = this.share = this.reviews = this.episode = false;

          break;
        default:
          this.shipping = false;
          this.reviews = false;
      }
    },
    getRelatedVideoProduct: function getRelatedVideoProduct(id) {
      var _this16 = this;

      axios.get("/api/get/related/Videos/" + id).then(function (response) {
        if (response.status === 200) {
          var relatedProduct = response.data.data;
          relatedProduct.forEach(function (item, index) {
            item.webinar.category = item.category.name;
            item.webinar.uid = item.id;
            item.webinar.cover = item.coverImage;
            _this16.relatedVideos.push(item.webinar);
            // this.relatedPlaylist[index].source(item.webinar.excerptsPublicId);
          });

          _this16.relatedPlaylist.forEach(function (item, index) {
            item.source(_this16.relatedVideos[index].excerptsPublicId);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    openModal: function openModal(id) {
      var _this17 = this;

      this.epiFade = false;
      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/product-detail/" + id).then(function (response) {
        if (response.status === 200) {
          _this17.videoDetail = response.data.data[0].webinar;
          _this17.videoDetail.subscribePrice = response.data.data[0].subscribePrice;
          _this17.videoDetail.streamPrice = response.data.data[0].streamPrice;
          _this17.videoDetail.uid = response.data.data[0].id;
          _this17.videoDetail.industry = response.data.data[0].industry.name;

          _this17.videoDetail.subLevel = response.data.data[0].subscriptionLevel;

          _this17.videoDetail.videoPrice = response.data.data[0].videoPrice;

          _this17.getMediaDetail(_this17.videoDetail.id);
          _this17.getProductReviews(_this17.videoDetail.uid);
          _this17.player.forEach(function (item) {
            item.source(_this17.videoDetail.excerptsPublicId);
          });
          _this17.getRelatedVideoProduct(id);
          axios.get("/api/getaVendorDetail/" + _this17.videoDetail.uid).then(function (response) {
            _this17.vendorInfo = response.data;
            if (user !== null) {
              _this17.$emit("activity", _this17.vendorInfo.id, 0, 1);
              _this17.$emit("top-content", _this17.vendorInfo.id, _this17.videoDetail.uid, 1);
              _this17.$emit("top-industry", _this17.vendorInfo.id, _this17.videoDetail.industry, 1);
              _this17.$emit("reach", user.id, _this17.videoDetail.product_id, _this17.vendorInfo.id);
              _this17.$emit("interaction", _this17.vendorInfo.id, "views");
              _this17.checkLike(user.id, _this17.videoDetail.product_id);
              _this17.getLike(_this17.videoDetail.product_id);
              _this17.$emit("add-location", _this17.vendorInfo.id, user.location);
              _this17.$emit("add-gender", _this17.vendorInfo.id, user.gender);
              _this17.getUnLike(_this17.videoDetail.product_id);
              _this17.$emit("add-age", _this17.vendorInfo.id, user.age);
            }
          });

          if (!isNaN(parseInt(_this17.videoDetail.videoPrice))) {
            _this17.title = "Video";
            _this17.hardpriceCont = false;
            _this17.hardCopy = true;
            // this.price = this.videoDetail.videoPrice;
          } else if (!isNaN(parseInt(_this17.videoDetail.subscribePrice))) {
            _this17.title = "Subscription";
            _this17.softpriceCont = false;
            _this17.softCopy = true;
            _this17.price = _this17.videoDetail.subscribePrice;
          } else if (!isNaN(parseInt(_this17.videoDetail.streamPrice))) {
            _this17.title = "Streaming";
            _this17.readOnlinepriceCont = false;
            _this17.readOnline = true;
            _this17.price = _this17.videoDetail.streamPrice;
          }

          if (_this17.videoDetail.subscribePrice !== "" || _this17.videoDetail.streamPrice !== "" || _this17.videoDetail.videoPrice !== "") {
            _this17.purchaseCheck = true;
          } else {
            _this17.purchaseCheck = false;
          }

          _this17.checkInLibrary(_this17.videoDetail.uid);
          if (_this17.user !== null) {
            _this17.wishlist(_this17.videoDetail.uid);

            axios.post("/api/cart/check/" + _this17.user.id + "/" + _this17.videoDetail.uid).then(function (response) {
              if (response.data === true) {
                _this17.inCart = true;
              } else {
                _this17.inCart = false;
              }
            });
          }
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    togglePrice: function togglePrice(price) {
      // if (price === "videoPrice") {
      // 	this.price = this.videoDetail.videoPrice;
      // 	this.hardpriceCont = this.softCopy = this.readOnline = false;
      // 	this.hardCopy = this.softpriceCont = this.readOnlinepriceCont = true;
      // 	this.title = "Video";
      // } else
      if (price === "streamPrice") {
        this.price = this.videoDetail.streamPrice;
        this.softpriceCont = this.hardCopy = this.readOnline = false;
        this.softCopy = this.hardpriceCont = this.readOnlinepriceCont = true;
        this.title = "Streaming";
      } else if (price === "subscribePrice") {
        this.price = this.videoDetail.subscribePrice;
        this.readOnlinepriceCont = this.hardCopy = this.softCopy = false;
        this.readOnline = this.softpriceCont = this.hardpriceCont = true;
        this.title = "Subscription";
      }
    },
    addEpisodeToCart: function addEpisodeToCart(e, vid, id, price) {
      e.preventDefault();

      var cart = {
        productId: id,
        mediaId: vid,
        price: price,
        prodType: "VideoEpisode",
        quantity: 0,
        vidEpisode: true
      };

      if (this.authenticate === false) {
        cart.cartNum = 1;
        this.anonymousCart.push(cart);
        if (JSON.parse(localStorage.getItem("userCart")) === null) {
          localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
          var sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var cartCount = sessionCart.length;
          this.$emit("getCartCount", cartCount);
        } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
          var _sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var ss = _sessionCart.length;
          _sessionCart[ss] = cart;
          localStorage.setItem("userCart", JSON.stringify(_sessionCart));
          var a = JSON.parse(localStorage.getItem("userCart"));
          var aCount = a.length;
          this.$emit("getCartCount", aCount);
        }
      } else {
        this.addCart(cart);
      }
    },
    rateWithStar: function rateWithStar(num) {
      switch (num) {
        case "1":
          this.NotratedOne = !this.NotratedOne;
          this.RatedOne = !this.RatedOne;
          this.review.rating = 1;
          break;
        case "2":
          this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
          this.RatedTwo = this.RatedOne = !this.RatedTwo;
          this.review.rating = 2;
          break;
        case "3":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
          this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
          this.review.rating = 3;
          break;
        case "4":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
          this.review.rating = 4;
          break;
        case "5":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
          this.review.rating = 5;
          break;
        default:
          this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
          this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
      }
    },
    submitReview: function submitReview() {
      var _this18 = this;

      if (this.authenticate === false) {
        this.$toasted.error("You must log in to review a product");
      } else {
        this.review.productId = this.videoDetail.uid;
        axios.post("/api/user/reviews", JSON.parse(JSON.stringify(this.review)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          if (response.status === 201) {
            _this18.$toasted.success("Reviews successfully saved");
            _this18.review.productId = _this18.review.title = _this18.review.description = "";
            _this18.review.rating = 0;
            _this18.NotratedOne = _this18.NotratedTwo = _this18.NotratedThree = _this18.NotratedFour = _this18.NotratedFive = true;
            _this18.RatedOne = _this18.RatedTwo = _this18.RatedThree = _this18.RatedFour = _this18.RatedFive = false;
          } else {
            _this18.$toasted.error("You cannot review this product until you purchase it");
          }
        }).catch(function (error) {
          var errors = Object.values(error.response.data.errors);
          errors.forEach(function (item) {
            _this18.$toasted.error(item[0]);
          });
        });
      }
    },
    checkInLibrary: function checkInLibrary(id) {
      var _this19 = this;

      this.checkLibrary.forEach(function (item) {
        if (Number(id) === Number(item.productId)) {
          _this19.inLibrary = true;
        } else {
          _this19.inLibrary = false;
        }
      });
    },
    getMediaDetail: function getMediaDetail(id) {
      var _this20 = this;

      axios.get("/api/get/getvideodetail/" + id).then(function (response) {
        if (response.status === 200) {
          _this20.episodeArray = response.data;
          _this20.episodeArray.forEach(function (item) {
            _this20.checkLibrary.forEach(function (element) {
              if (Number(element.itemPurchase) === Number(item.id)) {
                _this20.episodeadded = Number(item.id);
              }
            });
          });

          localStorage.setItem("episodeCount", JSON.stringify(_this20.episodeArray.length));
          _this20.episodeArray.forEach(function (item) {
            _this20.$set(item, "showHide", false);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    addToCart: function addToCart(id) {
      if (this.price === "") {
        this.$toasted.error("Please Select a price");
      } else {
        var cart = {
          productId: id,
          price: this.price
        };
        if (this.title === "Video") {
          cart.quantity = 0;
          cart.prodType = "Video";
        } else if (this.title === "Streaming") {
          cart.quantity = 0;
          cart.prodType = "Streaming";
        } else if (this.title === "Subscription") {
          cart.quantity = 0;
          cart.prodType = "Subscription";
        }

        if (this.authenticate === false) {
          cart.cartNum = 1;
          this.anonymousCart.push(cart);
          if (JSON.parse(localStorage.getItem("userCart")) === null) {
            localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
            var sessionCart = JSON.parse(localStorage.getItem("userCart"));
            var cartCount = sessionCart.length;
            this.$emit("getCartCount", cartCount);
            this.$notify({
              group: "cart",
              text: "Successfully Added to Cart!"
            });
          } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
            var _sessionCart2 = JSON.parse(localStorage.getItem("userCart"));
            var ss = _sessionCart2.length;
            _sessionCart2[ss] = cart;
            localStorage.setItem("userCart", JSON.stringify(_sessionCart2));
            var a = JSON.parse(localStorage.getItem("userCart"));
            var aCount = a.length;
            this.$emit("getCartCount", aCount);
            this.$notify({
              group: "cart",
              text: "Successfully Added to Cart!"
            });
          }
        } else {
          this.addCart(cart);
        }
      }
    },
    addCart: function addCart(cart) {
      var _this21 = this;

      if (this.inCart === true) {
        this.$toasted.error("Already in cart");
      } else {
        axios.post("/api/cart", JSON.parse(JSON.stringify(cart)), {
          headers: { Authorization: "Bearer " + this.user.access_token }
        }).then(function (response) {
          if (response.status === 201) {
            _this21.loginCart.push(response.data);
            var aCount = _this21.loginCart.length;
            _this21.$emit("getCartCount", aCount);
            _this21.$notify({
              group: "cart",
              title: _this21.videoDetail.title,
              text: "Successfully Added to Cart!"
            });
            _this21.$emit('getCart');
            _this21.inCart = true;
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    },
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    redirectUpgrade: function redirectUpgrade() {
      this.showUpgrade = false;
      var routeData = this.$router.resolve({
        name: "SubscriptionProfile",
        params: {
          level: this.videoDetail.subLevel
        }
      });

      window.open(routeData.href, "_blank");
    },
    related: function related(related_id) {
      var routeData = this.$router.resolve({
        name: "Video",
        params: { id: related_id }
      });

      window.open(routeData.href, "_self");
    }
  }
});

/***/ }),

/***/ 1539:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", { staticClass: "course-Detail" }, [
          _vm._m(1),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "row rowcontainer shadow" },
            [
              _c(
                "ul",
                { staticClass: "w-100" },
                _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
                  return _c(
                    "li",
                    {
                      key: index,
                      class: { linked: !!breadcrumb.link },
                      on: {
                        click: function($event) {
                          return _vm.routeTo(index)
                        }
                      }
                    },
                    [_vm._v(_vm._s(breadcrumb.name))]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c("div", { staticClass: "row rowBanner w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-md-6 col-sm-6 col-xs-6 search" },
                  [
                    _c("div", { staticClass: "searchBar" }, [
                      _c("div", { staticClass: "searchBarContent mb-2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.searchItem,
                              expression: "searchItem"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Search title" },
                          domProps: { value: _vm.searchItem },
                          on: {
                            keyup: function($event) {
                              return _vm.searchDataItem()
                            },
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.searchItem = $event.target.value
                            }
                          }
                        }),
                        _vm._v(" "),
                        _vm._m(2)
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _vm._m(3)
              ]),
              _vm._v(" "),
              _vm.filterItem.length === 0
                ? _c("div", { staticClass: "noResult" }, [
                    _vm._v(_vm._s(_vm.loading))
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm._l(_vm.filterItem, function(video, idx) {
                return _vm.videos.length > 0
                  ? _c(
                      "div",
                      {
                        key: idx,
                        staticClass:
                          "col-lg-12 col-md-12 col-sm-12 col-xs-12 videoDisTile shadow-sm"
                      },
                      [
                        video.prodCategoryType === "BP" ||
                        video.prodCategoryType === "EP" ||
                        video.prodCategoryType === "VP"
                          ? _c(
                              "div",
                              { staticClass: "ribbon ribbon-top-left" },
                              [_c("span", [_vm._v("Paid")])]
                            )
                          : _c(
                              "div",
                              {
                                staticClass:
                                  "ribbon ribbon-free ribbon-top-left"
                              },
                              [
                                video.subLevel == 0
                                  ? _c("span", [_vm._v("Free")])
                                  : _c(
                                      "span",
                                      { staticStyle: { "font-size": "10px" } },
                                      [_vm._v("Subscription")]
                                    )
                              ]
                            ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-4 col-xs-6 related_vid" },
                          [
                            _c("div", {
                              staticClass: "shadowA",
                              on: { click: _vm.loader }
                            }),
                            _vm._v(" "),
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  "data-dismiss": "modal",
                                  to: {
                                    name: "Video",
                                    params: { id: video.uid }
                                  }
                                }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "playCont",
                                    on: { click: _vm.loader }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-play playIcon",
                                      attrs: { "aria-hidden": "true" }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c("img", {
                                  attrs: { src: video.poster, alt: "" }
                                })
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm-8 col-xs-6 related_txt" },
                          [
                            _c(
                              "div",
                              { staticClass: "ddVideo" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "Video",
                                        params: { id: video.uid }
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "p",
                                      {
                                        staticClass: "videoTitle toCaps",
                                        on: { click: _vm.loader }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(video.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("p", { staticClass: "videoOverview" }, [
                                  _vm._v(_vm._s(video.description))
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "smatter" }, [
                                  _vm._v(
                                    "\n                " +
                                      _vm._s(video.subjectMatter) +
                                      "\n               "
                                  )
                                ])
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "bottom" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "Video",
                                        params: { id: video.uid }
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "button",
                                      {
                                        staticClass:
                                          "elevated_btn elevated_btn_sm btn-compliment text-white ",
                                        attrs: { type: "button" },
                                        on: { click: _vm.loader }
                                      },
                                      [
                                        _vm._v(
                                          "\n               \n                View\n              "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ]
                        )
                      ]
                    )
                  : _vm._e()
              })
            ],
            2
          )
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "banner" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/video-banner.jpg", alt: "banner" }
      }),
      _vm._v(" "),
      _c("h1", { staticClass: "banner-text" }, [_vm._v("Videos")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-btn mt-0 ml-0" }, [
      _c("i", { staticClass: "fa fa-search" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-6 col-xs-6 vidSearch" }, [
      _c("select", { staticClass: "form-control", attrs: { type: "search" } }, [
        _c("option", [_vm._v("Video")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f9ee8fd8", module.exports)
  }
}

/***/ }),

/***/ 583:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1536)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1538)
/* template */
var __vue_template__ = __webpack_require__(1539)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f9ee8fd8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userAdminVideoComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f9ee8fd8", Component.options)
  } else {
    hotAPI.reload("data-v-f9ee8fd8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 745:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(746)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(748)
/* template */
var __vue_template__ = __webpack_require__(749)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f443e1b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryVideoBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f443e1b", Component.options)
  } else {
    hotAPI.reload("data-v-0f443e1b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(747);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cd849544", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vidBanner[data-v-0f443e1b]{\n     position: relative;\n}\nimg[data-v-0f443e1b]{\n    margin-top: 40px;\n    width: 100%;\n    /* max-height: 300px; */\n}\n.text[data-v-0f443e1b]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size: 60px;\n    color: #ffffff;\n}\n@media(max-width: 475px){\n.vidBanner[data-v-0f443e1b]{\n     display: none;\n}\n.text[data-v-0f443e1b]{\n            font-size:30px;\n            top: 110px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-video-banner-component"
});

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vidBanner" }, [
      _c("img", { attrs: { src: "/images/vid.png" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Videos")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f443e1b", module.exports)
  }
}

/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * vue-social-sharing v2.4.7 
 * (c) 2019 nicolasbeauvais
 * Released under the MIT License.
 */


function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Vue = _interopDefault(__webpack_require__(40));

var SocialSharingNetwork = {
  functional: true,

  props: {
    network: {
      type: String,
      default: ''
    }
  },

  render: function (createElement, context) {
    var network = context.parent._data.baseNetworks[context.props.network];

    if (!network) {
      return console.warn(("Network " + (context.props.network) + " does not exist"));
    }

    return createElement(context.parent.networkTag, {
      staticClass: context.data.staticClass || null,
      staticStyle: context.data.staticStyle || null,
      class: context.data.class || null,
      style: context.data.style || null,
      attrs: {
        id: context.data.attrs.id || null,
        tabindex: context.data.attrs.tabindex || 0,
        'data-link': network.type === 'popup'
          ? '#share-' + context.props.network
          : context.parent.createSharingUrl(context.props.network),
        'data-action': network.type === 'popup' ? null : network.action
      },
      on: {
        click: network.type === 'popup' ? function () {
          context.parent.share(context.props.network);
        } : function () {
          context.parent.touch(context.props.network);
        }
      }
    }, context.children);
  }
};

var email = {"sharer":"mailto:?subject=@title&body=@url%0D%0A%0D%0A@description","type":"direct"};
var facebook = {"sharer":"https://www.facebook.com/sharer/sharer.php?u=@url&title=@title&description=@description&quote=@quote&hashtag=@hashtags","type":"popup"};
var googleplus = {"sharer":"https://plus.google.com/share?url=@url","type":"popup"};
var line = {"sharer":"http://line.me/R/msg/text/?@description%0D%0A@url","type":"popup"};
var linkedin = {"sharer":"https://www.linkedin.com/shareArticle?mini=true&url=@url&title=@title&summary=@description","type":"popup"};
var odnoklassniki = {"sharer":"https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=@url&st.comments=@description","type":"popup"};
var pinterest = {"sharer":"https://pinterest.com/pin/create/button/?url=@url&media=@media&description=@title","type":"popup"};
var reddit = {"sharer":"https://www.reddit.com/submit?url=@url&title=@title","type":"popup"};
var skype = {"sharer":"https://web.skype.com/share?url=@description%0D%0A@url","type":"popup"};
var telegram = {"sharer":"https://t.me/share/url?url=@url&text=@description","type":"popup"};
var twitter = {"sharer":"https://twitter.com/intent/tweet?text=@title&url=@url&hashtags=@hashtags@twitteruser","type":"popup"};
var viber = {"sharer":"viber://forward?text=@url @description","type":"direct"};
var vk = {"sharer":"https://vk.com/share.php?url=@url&title=@title&description=@description&image=@media&noparse=true","type":"popup"};
var weibo = {"sharer":"http://service.weibo.com/share/share.php?url=@url&title=@title","type":"popup"};
var whatsapp = {"sharer":"https://api.whatsapp.com/send?text=@description%0D%0A@url","type":"popup","action":"share/whatsapp/share"};
var sms = {"sharer":"sms:?body=@url%20@description","type":"direct"};
var sms_ios = {"sharer":"sms:;body=@url%20@description","type":"direct"};
var BaseNetworks = {
	email: email,
	facebook: facebook,
	googleplus: googleplus,
	line: line,
	linkedin: linkedin,
	odnoklassniki: odnoklassniki,
	pinterest: pinterest,
	reddit: reddit,
	skype: skype,
	telegram: telegram,
	twitter: twitter,
	viber: viber,
	vk: vk,
	weibo: weibo,
	whatsapp: whatsapp,
	sms: sms,
	sms_ios: sms_ios
};

var inBrowser = typeof window !== 'undefined';
var $window = inBrowser ? window : null;

var SocialSharing = {
  props: {
    /**
     * URL to share.
     * @var string
     */
    url: {
      type: String,
      default: inBrowser ? window.location.href : ''
    },

    /**
     * Sharing title, if available by network.
     * @var string
     */
    title: {
      type: String,
      default: ''
    },

    /**
     * Sharing description, if available by network.
     * @var string
     */
    description: {
      type: String,
      default: ''
    },

    /**
     * Facebook quote
     * @var string
     */
    quote: {
      type: String,
      default: ''
    },

    /**
     * Twitter hashtags
     * @var string
     */
    hashtags: {
      type: String,
      default: ''
    },

    /**
     * Twitter user.
     * @var string
     */
    twitterUser: {
      type: String,
      default: ''
    },

    /**
     * Flag that indicates if counts should be retrieved.
     * - NOT WORKING IN CURRENT VERSION
     * @var mixed
     */
    withCounts: {
      type: [String, Boolean],
      default: false
    },

    /**
     * Google plus key.
     * @var string
     */
    googleKey: {
      type: String,
      default: undefined
    },

    /**
     * Pinterest Media URL.
     * Specifies the image/media to be used.
     */
    media: {
      type: String,
      default: ''
    },

    /**
     * Network sub component tag.
     * Default to span tag
     */
    networkTag: {
      type: String,
      default: 'span'
    },

    /**
     * Additional or overridden networks.
     * Default to BaseNetworks
     */
    networks: {
      type: Object,
      default: function () {
        return {};
      }
    }
  },

  data: function data () {
    return {
      /**
       * Available sharing networks.
       * @param object
       */
      baseNetworks: BaseNetworks,

      /**
       * Popup settings.
       * @param object
       */
      popup: {
        status: false,
        resizable: true,
        toolbar: false,
        menubar: false,
        scrollbars: false,
        location: false,
        directories: false,
        width: 626,
        height: 436,
        top: 0,
        left: 0,
        window: undefined,
        interval: null
      }
    };
  },

  methods: {
    /**
     * Returns generated sharer url.
     *
     * @param network Social network key.
     */
    createSharingUrl: function createSharingUrl (network) {
      var ua = navigator.userAgent.toLowerCase();

      /**
       * On IOS, SMS sharing link need a special formating
       * Source: https://weblog.west-wind.com/posts/2013/Oct/09/Prefilling-an-SMS-on-Mobile-Devices-with-the-sms-Uri-Scheme#Body-only
        */
      if (network === 'sms' && (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1)) {
        network += '_ios';
      }

      var url = this.baseNetworks[network].sharer;

      /**
       * On IOS, Twitter sharing shouldn't include a hashtag parameter if the hashtag value is empty
       * Source: https://github.com/nicolasbeauvais/vue-social-sharing/issues/143
        */
      if (network === 'twitter' && this.hashtags.length === 0) {
        url = url.replace('&hashtags=@hashtags', '');
      }

      return url
        .replace(/@url/g, encodeURIComponent(this.url))
        .replace(/@title/g, encodeURIComponent(this.title))
        .replace(/@description/g, encodeURIComponent(this.description))
        .replace(/@quote/g, encodeURIComponent(this.quote))
        .replace(/@hashtags/g, this.generateHashtags(network, this.hashtags))
        .replace(/@media/g, this.media)
        .replace(/@twitteruser/g, this.twitterUser ? '&via=' + this.twitterUser : '');
    },
    /**
     * Encode hashtags for the specified social network.
     *
     * @param  network Social network key
     * @param  hashtags All hashtags specified
     */
    generateHashtags: function generateHashtags (network, hashtags) {
      if (network === 'facebook' && hashtags.length > 0) {
        return '%23' + hashtags.split(',')[0];
      }

      return hashtags;
    },
    /**
     * Shares URL in specified network.
     *
     * @param network Social network key.
     */
    share: function share (network) {
      this.openSharer(network, this.createSharingUrl(network));

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Touches network and emits click event.
     *
     * @param network Social network key.
     */
    touch: function touch (network) {
      window.open(this.createSharingUrl(network), '_self');

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Opens sharer popup.
     *
     * @param network Social network key
     * @param url Url to share.
     */
    openSharer: function openSharer (network, url) {
      var this$1 = this;

      // If a popup window already exist it will be replaced, trigger a close event.
      var popupWindow = null;
      if (popupWindow && this.popup.interval) {
        clearInterval(this.popup.interval);

        popupWindow.close();// Force close (for Facebook)

        this.$root.$emit('social_shares_change', network, this.url);
        this.$emit('change', network, this.url);
      }

      popupWindow = window.open(
        url,
        'sharer',
        'status=' + (this.popup.status ? 'yes' : 'no') +
        ',height=' + this.popup.height +
        ',width=' + this.popup.width +
        ',resizable=' + (this.popup.resizable ? 'yes' : 'no') +
        ',left=' + this.popup.left +
        ',top=' + this.popup.top +
        ',screenX=' + this.popup.left +
        ',screenY=' + this.popup.top +
        ',toolbar=' + (this.popup.toolbar ? 'yes' : 'no') +
        ',menubar=' + (this.popup.menubar ? 'yes' : 'no') +
        ',scrollbars=' + (this.popup.scrollbars ? 'yes' : 'no') +
        ',location=' + (this.popup.location ? 'yes' : 'no') +
        ',directories=' + (this.popup.directories ? 'yes' : 'no')
      );

      popupWindow.focus();

      // Create an interval to detect popup closing event
      this.popup.interval = setInterval(function () {
        if (!popupWindow || popupWindow.closed) {
          clearInterval(this$1.popup.interval);

          popupWindow = undefined;

          this$1.$root.$emit('social_shares_close', network, this$1.url);
          this$1.$emit('close', network, this$1.url);
        }
      }, 500);
    }
  },

  /**
   * Merge base networks list with user's list
   */
  beforeMount: function beforeMount () {
    this.baseNetworks = Vue.util.extend(this.baseNetworks, this.networks);
  },

  /**
   * Sets popup default dimensions.
   */
  mounted: function mounted () {
    if (!inBrowser) {
      return;
    }

    /**
     * Center the popup on dual screens
     * http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen/32261263
     */
    var dualScreenLeft = $window.screenLeft !== undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop !== undefined ? $window.screenTop : screen.top;

    var width = $window.innerWidth ? $window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width);
    var height = $window.innerHeight ? $window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height);

    this.popup.left = ((width / 2) - (this.popup.width / 2)) + dualScreenLeft;
    this.popup.top = ((height / 2) - (this.popup.height / 2)) + dualScreenTop;
  },

  /**
   * Set component aliases for buttons and links.
   */
  components: {
    'network': SocialSharingNetwork
  }
};

SocialSharing.version = '2.4.7';

SocialSharing.install = function (Vue) {
  Vue.component('social-sharing', SocialSharing);
};

if (typeof window !== 'undefined') {
  window.SocialSharing = SocialSharing;
}

module.exports = SocialSharing;

/***/ })

});