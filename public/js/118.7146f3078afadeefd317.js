webpackJsonp([118],{

/***/ 1420:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1421);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("ef39bf28", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2bf70020\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCartComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2bf70020\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCartComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1421:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-2bf70020] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n    padding: 20px;\n    background:transparent;\n    width: 100%;\n}\nul > li[data-v-2bf70020] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nul > li[data-v-2bf70020]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-2bf70020] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.cart[data-v-2bf70020]{\n    padding:0px 10px;\n    min-height:100vh;\n    background:#f7f8fa;\n}\n.toAnt[data-v-2bf70020] {\n    font-size: 30px;\n}\np[data-v-2bf70020]{\n      line-height:1.4;\n      margin-bottom: 10px;\n}\n.headerOne[data-v-2bf70020] {\n    font-size: 16px;\n    line-height: 1.2;\n}\n.headerOne h5[data-v-2bf70020]{\n      font-size: 16px;\n}\n.btn-checkout[data-v-2bf70020] {\n    background-color: #ffffff !important;\n    color: #abc8df !important;\n}\n.summ[data-v-2bf70020] {\n    color: #ffffff;\n}\n.totalRow-one[data-v-2bf70020] {\n    text-align: center;\n    color: #ffffff;\n    padding-top: 20px;\n    font-weight: bold;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.totalRow[data-v-2bf70020] {\n    padding: 10px 0;\n\n    color: #ffffff;\n    text-align: center;\n}\n.cartI[data-v-2bf70020] {\n    margin-left: auto;\n    width: 20%;\n    margin-right: auto;\n    /* border: 2px solid #383749; */\n}\n.nav[data-v-2bf70020] {\n        display: block;\n}\n.cartItem[data-v-2bf70020] {\n    text-align: center;\n}\n.itemImg[data-v-2bf70020] {\n    width: 100px;\n}\n.cartTitle[data-v-2bf70020] {\n    text-align: center;\n    padding:10px;\n    margin:10px;\n    width:100%;\n}\n.cartTit[data-v-2bf70020] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    padding:15px;\n    background:white;\n    margin:10px;\n    width:100%;\n    height:auto\n}\n.imageDiver[data-v-2bf70020] {\n    float:left;\n    width:80px;\n    height:100px;\n}\n.imageDiverr[data-v-2bf70020] {\n    float:left;\n    width:100%;\n    height:100px;\n}\n.imageDiver img[data-v-2bf70020]{\n      width:100%;\n      height:100%;\n      -o-object-fit: cover;\n         object-fit: cover;\n}\n.trashRemove[data-v-2bf70020] {\n    cursor: pointer;\n}\n.cartSummary[data-v-2bf70020] {\n    background-color: hsl(207, 43%, 20%);\n    padding: 20px 0;\n    margin: 10px;\n}\n.continue[data-v-2bf70020] {\n    background-color: #29303b !important;\n    border: 1px solid black;\n}\n@media(max-width: 425px){\n.desktop[data-v-2bf70020]{\n          display : none;\n}\nh2[data-v-2bf70020]{\n        font-size: 17px;\n}\n.summ[data-v-2bf70020]{\n        font-size: 16px;\n}\n.pd-0[data-v-2bf70020]{\n          padding:0;\n}\n.p-2[data-v-2bf70020]{\n          padding:0.7rem !important;\n}\n.cart[data-v-2bf70020]{\n          padding: 0px;\n}\n.cartI[data-v-2bf70020]{\n          width: 70%;\n}\n.totalRow[data-v-2bf70020]{\n          padding:10px 0;\n}\n.toAnt[data-v-2bf70020]{\n          font-size: 16px;\n}\n.totalRow-one[data-v-2bf70020]{\n          padding-top:10px;\n}\n.trashRemove[data-v-2bf70020]{\n          text-align: center;\n}\n.cartTit[data-v-2bf70020]{\n          margin:0;\n          margin-bottom: 10px;\n}\n.cartTitle[data-v-2bf70020]{\n          margin:10px 0;\n}\n.cartSummary[data-v-2bf70020]{\n          margin:0;\n}\n.btn-checkout[data-v-2bf70020]{\n          text-transform: capitalize;\n}\n.imageCont[data-v-2bf70020]{\n          width:120px;\n          height:80px;\n}\n.headerOne[data-v-2bf70020]{\n        font-size: 14px;\n}\n.cartTit[data-v-2bf70020]{\n        font-size: 14px;\n}\nul[data-v-2bf70020]{\n        padding-bottom:0 ;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-cart-component",
    data: function data() {
        return {
            carts: [],
            totalAmount: '',
            breadcrumbList: [],
            update: 0
        };
    },
    watch: {
        '$route': function $route() {
            this.updateList();
        },

        update: 'getItems'
    },
    mounted: function mounted() {
        this.updateList();
        this.getItems();
    },

    methods: {
        getItems: function getItems() {
            var _this = this;

            var customerUser = JSON.parse(localStorage.getItem('authUser')) ? JSON.parse(localStorage.getItem('authUser')) : 0;
            var sessionCart = JSON.parse(localStorage.getItem('userCart')) ? JSON.parse(localStorage.getItem('userCart')) : 0;
            var itemArray = [];
            if (sessionCart instanceof Array) {
                sessionCart.forEach(function (item) {
                    itemArray.push({
                        id: item.productId,
                        quantity: item.quantity,
                        prodType: item.prodType,
                        mediaId: item.mediaId,
                        quantities: item.quantities
                    });
                });
            }

            var cart = {
                customerId: customerUser.id,
                itemId: itemArray,
                sessionCheck: customerUser.id === undefined ? 'NC' : 'C'
            };

            axios.post('/api/post/cart/bk', JSON.parse(JSON.stringify(cart))).then(function (response) {
                if (response.status === 200) {

                    _this.carts = response.data.item;

                    _this.totalAmount = response.data.totalAmount;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        },
        scrollToTop: function scrollToTop() {
            window.scrollTo(0, 0);
        },
        removeItem: function removeItem(index, uid) {
            var _this2 = this;

            if (index === undefined) {
                axios.get('/api/post/cart/delete/' + uid).then(function (response) {

                    if (response.status === 200) {
                        _this2.update++;
                        _this2.$emit('removeCart');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                var appArray = JSON.parse(localStorage.getItem('userCart'));
                appArray.splice(index, 1);
                localStorage.setItem('userCart', JSON.stringify(appArray));
                this.update++;
            }
        }
    }
});

/***/ }),

/***/ 1423:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "cart course-Detail" }, [
    _c(
      "ul",
      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
        return _c(
          "li",
          {
            key: index,
            class: { linked: !!breadcrumb.link },
            on: {
              click: function($event) {
                return _vm.routeTo(index)
              }
            }
          },
          [
            _vm._v(
              "\n             " + _vm._s(breadcrumb.name) + "\n           "
            )
          ]
        )
      }),
      0
    ),
    _vm._v(" "),
    _vm.carts.length > 0
      ? _c("div", {}, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-md-8 pd-0" },
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._l(_vm.carts, function(cart, index) {
                  return _c("div", { key: index, staticClass: "cartTit" }, [
                    _c(
                      "div",
                      { staticClass: "imageDiver col-md-3 imageCont pd-0" },
                      [
                        _c("img", {
                          staticClass: "itemImg",
                          attrs: { src: cart.coverImage }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "imageDiverr  col-md-6 p-2" }, [
                      _c("p", { staticClass: "headerOne" }, [
                        _vm._v(_vm._s(cart.title))
                      ]),
                      _vm._v(" "),
                      _c("p", [_vm._v(_vm._s(cart.prodType))])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-xs-3 p-2" }, [
                      cart.amount
                        ? _c("p", [_vm._v("₦" + _vm._s(cart.amount) + ".00")])
                        : _c("p", [
                            _vm._v("₦" + _vm._s(cart.hardCopyPrice) + ".00")
                          ]),
                      _vm._v(" "),
                      cart.author || cart.publisher
                        ? _c("p", [
                            _vm._v(
                              " " +
                                _vm._s(cart.author) +
                                "/" +
                                _vm._s(cart.publisher)
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      cart.myQty
                        ? _c("div", [_vm._v("Quantity: " + _vm._s(cart.myQty))])
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "trashRemove",
                          on: {
                            click: function($event) {
                              return _vm.removeItem(cart.count, cart.uid)
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fa fa-trash" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "desktop" }, [
                            _vm._v("Remove")
                          ])
                        ]
                      )
                    ])
                  ])
                })
              ],
              2
            ),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 pd-0" }, [
              _c("div", { staticClass: "cartSummary" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", [
                  _c("div", { staticClass: "totalRow" }, [
                    _vm._v("Do you have a Promo Code?")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "totalRow final" }, [
                    _c("span", { staticClass: "label" }, [_vm._v("Subtotal")]),
                    _c("span", { staticClass: "value" }, [
                      _vm._v("₦" + _vm._s(_vm.totalAmount) + ".00")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "totalRow-one toAnt final" }, [
                    _c("span", { staticClass: "label" }, [_vm._v("Total")]),
                    _c("span", { staticClass: "value" }, [
                      _vm._v("₦" + _vm._s(_vm.totalAmount) + ".00")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "totalRow-one" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "elevated_btn elevated_btn_sm text-main",
                          attrs: { to: { name: "userCheckout" } },
                          nativeOn: {
                            click: function($event) {
                              return _vm.scrollToTop($event)
                            }
                          }
                        },
                        [_vm._v("Checkout")]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        ])
      : _c("div", { staticClass: "row" }, [_vm._m(2)])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12 cartTitle" }, [
      _c("h2", [_vm._v("Your Cart")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "totalRow" }, [
      _c("h1", { staticClass: "summ" }, [_vm._v("Summary")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12 cartItem" }, [
      _c("div", { staticClass: "cartI" }, [_vm._v(" No Items in Cart")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2bf70020", module.exports)
  }
}

/***/ }),

/***/ 558:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1420)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1422)
/* template */
var __vue_template__ = __webpack_require__(1423)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2bf70020"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userCartComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2bf70020", Component.options)
  } else {
    hotAPI.reload("data-v-2bf70020", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});