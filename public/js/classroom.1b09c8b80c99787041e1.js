webpackJsonp([1],{

/***/ 1912:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1913);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("16c7b312", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37dd416d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./classroom.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37dd416d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./classroom.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1913:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:700);", ""]);

// module
exports.push([module.i, "\n.banner[data-v-37dd416d] {\n  height: 50vh;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.banner img[data-v-37dd416d] {\n  width: 100%;\n  height: 100%;\n  z-index: 2;\n}\n.text[data-v-37dd416d] {\n  position: absolute;\n  z-index: 3;\n  color: white;\n  margin-left: 50px;\n}\n.text h2[data-v-37dd416d] {\n  font-size: 60px;\n}\n.text p[data-v-37dd416d] {\n  font-size: 18px;\n}\n.courses_content[data-v-37dd416d] {\n  padding: 15px;\n}\n.mcont[data-v-37dd416d] {\n  font-weight: bold;\n  background: white;\n}\n.ribbon[data-v-37dd416d] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  position: absolute;\n  z-index: 70;\n}\n.ribbon[data-v-37dd416d]::before,\n.ribbon[data-v-37dd416d]::after {\n  position: absolute;\n  z-index: -1;\n  content: \"\";\n  display: block;\n  border: 5px solid #2980b9;\n}\n.ribbon span[data-v-37dd416d] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 8px 0;\n  background-color: #3498db;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 700 18px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n.ribbon-free span[data-v-37dd416d] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 8px 0;\n  background-color: #404346;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 700 18px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n\n/* top left*/\n.ribbon-top-left[data-v-37dd416d] {\n  top: -15px;\n  right: -30px;\n}\n.ribbon-top-left[data-v-37dd416d]::before,\n.ribbon-top-left[data-v-37dd416d]::after {\n  border-top-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-top-left[data-v-37dd416d]::before {\n  top: 0;\n  right: 0;\n}\n.ribbon-top-left[data-v-37dd416d]::after {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-top-left span[data-v-37dd416d] {\n  right: -25px;\n  top: 30px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n/* top right*/\n.ribbon-top-right[data-v-37dd416d] {\n  top: -10px;\n  right: -10px;\n}\n.ribbon-top-right[data-v-37dd416d]::before,\n.ribbon-top-right[data-v-37dd416d]::after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-top-right[data-v-37dd416d]::before {\n  top: 0;\n  left: 0;\n}\n.ribbon-top-right[data-v-37dd416d]::after {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-top-right span[data-v-37dd416d] {\n  left: -7px;\n  top: 22px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n/* bottom left*/\n.ribbon-bottom-left[data-v-37dd416d] {\n  bottom: -10px;\n  left: -10px;\n}\n.ribbon-bottom-left[data-v-37dd416d]::before,\n.ribbon-bottom-left[data-v-37dd416d]::after {\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-37dd416d]::before {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-bottom-left[data-v-37dd416d]::after {\n  top: 0;\n  left: 0;\n}\n.ribbon-bottom-left span[data-v-37dd416d] {\n  right: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(225deg);\n          transform: rotate(225deg);\n}\n\n/* bottom right*/\n.ribbon-bottom-right[data-v-37dd416d] {\n  bottom: -10px;\n  right: -10px;\n}\n.ribbon-bottom-right[data-v-37dd416d]::before,\n.ribbon-bottom-right[data-v-37dd416d]::after {\n  border-bottom-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-37dd416d]::before {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-bottom-right[data-v-37dd416d]::after {\n  top: 0;\n  right: 0;\n}\n.ribbon-bottom-right span[data-v-37dd416d] {\n  left: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(-225deg);\n          transform: rotate(-225deg);\n}\n.courseTit[data-v-37dd416d] {\n  display: -webkit-box !important;\n  text-align: left;\n  text-transform: capitalize;\n  color: #5b84a7;\n  font-size: 16px;\n  max-height: 40px;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  word-break: break-word;\n  margin-top: 5px;\n  margin-bottom: 10px;\n  line-height: 1.2;\n}\n.courseOver[data-v-37dd416d] {\n  font-size: 16px;\n  color: rgba(0, 0, 0, 0.64);\n  display: -webkit-box !important;\n  text-align: left;\n  text-transform: capitalize;\n  font-size: 14px;\n  max-height: 20px;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  word-break: break-word;\n}\n.courseLevel[data-v-37dd416d] {\n  font-size: 12px;\n  color: rgba(0, 0, 0, 0.54);\n  text-transform: capitalize;\n  line-height: 1.7;\n}\n.deskMargin[data-v-37dd416d] {\n  margin: 0 20px;\n}\n.pry-color[data-v-37dd416d] {\n  color: #5b84a7;\n}\n.swiper-container[data-v-37dd416d] {\n  height: auto !important;\n  margin-left: auto;\n  margin-right: auto;\n  padding: 5px 15px;\n}\n.swiper-slide[data-v-37dd416d] {\n  margin-top: 0 !important;\n}\n.swiper-button-prev[data-v-37dd416d],\n.swiper-button-next[data-v-37dd416d] {\n  opacity: 0.4 !important;\n}\n.container[data-v-37dd416d] {\n  padding-top: 20px;\n  padding-bottom: 40px;\n  padding-left: 20px;\n  padding-right: 20px;\n  margin-top: 20px;\n}\n.container-fluid[data-v-37dd416d] {\n  margin-top: 20px;\n}\n.col-lg-offset-1[data-v-37dd416d],\n.col-md-offset-1[data-v-37dd416d],\n.col-sm-offset-1[data-v-37dd416d] {\n  margin-left: 2.33333333%;\n}\n.industry[data-v-37dd416d] {\n  position: relative;\n  width: 100%;\n  height: 150px;\n  margin: 10px;\n}\n.industry_content[data-v-37dd416d] {\n  position: absolute; /* Position the background text */\n  top: 0;\n  background: -webkit-gradient(linear, left top, right top, from(rgb(0, 0, 0)), to(rgb(79, 79, 79)));\n  background: linear-gradient(to right, rgb(0, 0, 0), rgb(79, 79, 79));\n  background: -webkit-gradient(\n    linear,\n    left top, right top,\n    from(rgb(0, 0, 0, 0.5)),\n    to(rgb(79, 79, 79, 0.5))\n  );\n  background: linear-gradient(\n    to right,\n    rgb(0, 0, 0, 0.5),\n    rgb(79, 79, 79, 0.5)\n  );\n  color: hsl(0, 0%, 90%);\n  width: 100%;\n  padding: 60px 0;\n  height: 150px;\n  text-align: center;\n  /* padding:130px 0; */\n}\n.industry_title[data-v-37dd416d] {\n  font-size: calc(17px + (20 - 17) * ((100vw - 300px) / (1600-300)));\n  font-weight: normal;\n}\n.industry img[data-v-37dd416d] {\n  vertical-align: middle;\n  width: 100%;\n  height: 100%;\n}\n.industry_header[data-v-37dd416d] {\n  padding: 0 0 15px 0;\n  text-align: center;\n}\nh1.industries.title[data-v-37dd416d] {\n  margin: 20px !important;\n}\n.shadow[data-v-37dd416d] {\n  position: absolute; /* Position the background text */\n  top: 0;\n  background: radial-gradient(rgb(79, 79, 79, 0.5), rgb(0, 0, 0, 0.5));\n  color: #f1f1f1;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  z-index: 1;\n  border-radius: 5px;\n}\n.container[data-v-37dd416d] {\n  padding-top: 20px;\n  padding-bottom: 40px;\n  padding-left: 0;\n  padding-right: 0;\n}\n.mobile[data-v-37dd416d] {\n  display: none;\n}\n.student[data-v-37dd416d] {\n  background-color: #fff;\n  border-radius: 5px;\n  -webkit-box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n          box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n  height: auto;\n  padding: 10px 5px;\n  text-align: center;\n  border-radius: 30px;\n}\n.course_header[data-v-37dd416d] {\n  text-align: left;\n  font-size: calc(16px + (50 - 16) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.6 - 1.3) * ((100vw - 300px) / (1600 - 300)));\n  font-weight: 800 !important;\n  font-family: \"Josefin Sans\";\n}\n.classHis[data-v-37dd416d],\n.topCat[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 40px;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  padding: 0;\n}\n.VueCarousel-inner[data-v-37dd416d] {\n  padding: 0 15px;\n}\n.secondView[data-v-37dd416d] {\n  /* background: rgb(163, 194, 220, 0.2); */\n  padding: 30px 0 80px 0;\n  margin-bottom: -18px;\n}\n.studentView[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.studentViews.mobile[data-v-37dd416d] {\n  display: none;\n}\n.subject_title[data-v-37dd416d] {\n  margin-top: 20px;\n  text-align: center;\n  padding: 10px 200px;\n  /* background: #5b84a7; */\n  color: #000;\n}\n.subjects[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  margin-top: 20px;\n  margin-bottom: 20px;\n}\n.subject[data-v-37dd416d] {\n  position: relative;\n  padding: 0px;\n  margin: 0 5px;\n  height: 150px;\n  -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n          box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n  border-radius: 5px;\n}\n.subject a img[data-v-37dd416d] {\n  border-radius: 5px;\n}\n.cover[data-v-37dd416d] {\n  font-size: 9px;\n}\n.coursePrice[data-v-37dd416d] {\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  margin-top: 0;\n}\n.text-content[data-v-37dd416d] {\n  font-size: calc(17px + (24 - 17) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  color: hsl(0, 0%, 90%);\n  text-align: center;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  z-index: 2;\n  font-family: \"Josefin Sans\";\n}\n.test_name[data-v-37dd416d] {\n  font-weight: bold;\n  margin-bottom: 5px;\n}\n.test_story[data-v-37dd416d] {\n  text-align: left;\n  color: #777;\n}\n.fa-arrow-circle-right[data-v-37dd416d] {\n  font-size: calc(14px + (16 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  color: #fff;\n  float: right;\n  position: absolute;\n  bottom: 8px;\n  right: 16px;\n}\n.topCatDiv[data-v-37dd416d] {\n  /* float: left; */\n  margin: 10px;\n  text-align: center;\n  text-transform: capitalize;\n  /* width: 250px;\n  padding: 23px 0; */\n  -webkit-box-shadow: 0 0 1px 1px rgba(20, 23, 28, 0.1),\n    0 3px 1px 0 rgba(20, 23, 28, 0.1);\n          box-shadow: 0 0 1px 1px rgba(20, 23, 28, 0.1),\n    0 3px 1px 0 rgba(20, 23, 28, 0.1);\n}\n.outline-banner[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  color: #ffffff;\n  padding: 20px 0;\n  /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#3b679e+0,2b88d9+50,207cca+51,7db9e8+100;Blue+Gloss+%232 */\n  background: rgb(59, 103, 158); /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\n  background: linear-gradient(\n    45deg,\n    rgba(59, 103, 158, 1) 0%,\n    rgba(43, 136, 217, 1) 50%,\n    rgba(32, 124, 202, 1) 51%,\n    rgba(125, 185, 232, 1) 100%\n  ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3b679e', endColorstr='#7db9e8',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */\n}\n.learn[data-v-37dd416d] {\n  font-size: calc(16px + (40 - 16) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.6 - 1.3) * ((100vw - 300px) / (1600 - 300)));\n  font-family: \"Josefin Sans\";\n}\ninput[data-v-37dd416d] {\n  width: 400px;\n}\n.price[data-v-37dd416d] {\n  padding: 5px;\n  color: rgba(0, 0, 0, 0.7);\n  text-align: center;\n  font-size: 16px;\n  line-height: 1.4;\n}\n.grow[data-v-37dd416d] {\n  font-size: calc(14px + (18 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.6 - 1.3) * ((100vw - 300px) / (1600 - 300)));\n  font-weight: normal;\n}\n.courseText[data-v-37dd416d] {\n  position: absolute;\n  top: 130px;\n  color: hsl(0, 0%, 90%);\n  margin: 100px;\n}\n.imgDiv[data-v-37dd416d] {\n  position: relative;\n  width: 100%;\n  height: 450px;\n}\nimg[data-v-37dd416d] {\n  width: 100%;\n  height: 100%;\n}\n.imgHold[data-v-37dd416d] {\n  position: relative;\n  height: auto;\n  /* box-shadow: 0 0 1px 1px rgba(20, 23, 28, 0.1),\n\t\t0 3px 1px 0 rgba(20, 23, 28, 0.1); */\n}\n.routerlink[data-v-37dd416d] {\n  background: #fff;\n  height: auto;\n  display: block;\n  overflow: hidden;\n  position: relative;\n}\n.couseComp[data-v-37dd416d] {\n  margin-top: 50px;\n}\n.imgHolder[data-v-37dd416d] {\n  width: 100%;\n  height: 160px;\n}\n.catImage[data-v-37dd416d] {\n  width: 100%;\n  display: block;\n  height: 100%;\n}\n.prodDis[data-v-37dd416d] {\n  padding: 0 0 5px 0;\n  height: auto;\n  -webkit-box-shadow: 0 0 1px 1px rgba(20, 23, 28, 0.1),\n    0 3px 1px 0 rgba(20, 23, 28, 0.1);\n          box-shadow: 0 0 1px 1px rgba(20, 23, 28, 0.1),\n    0 3px 1px 0 rgba(20, 23, 28, 0.1);\n  margin-bottom: 20px;\n  overflow: hidden;\n  border-radius: 5px;\n  background: #ffffff;\n}\n.courseDiv[data-v-37dd416d] {\n  display: block;\n  padding: 15px 10px 0 15px;\n  font-size: small;\n  color: #383838;\n  font-weight: 500;\n}\n.courseDiv div[data-v-37dd416d] {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n@media (max-width: 425px) {\n.text h2[data-v-37dd416d] {\n    font-size: 32px;\n}\n.text p[data-v-37dd416d] {\n    font-size: 18px;\n}\nul[data-v-37dd416d] {\n    margin: 15px 0;\n}\n.courseLevel[data-v-37dd416d] {\n    font-size: 9px;\n    line-height: 1.4;\n}\n.swiper-button-prev[data-v-37dd416d],\n  .swiper-button-next[data-v-37dd416d] {\n    opacity: 0.4 !important;\n    height: 20px;\n}\n.ribbon[data-v-37dd416d] {\n    width: 150px;\n    height: 150px;\n    overflow: hidden;\n    position: absolute;\n    z-index: 70;\n}\n.ribbon[data-v-37dd416d]::before,\n  .ribbon[data-v-37dd416d]::after {\n    position: absolute;\n    z-index: -1;\n    content: \"\";\n    display: block;\n    border: 5px solid #2980b9;\n}\n.ribbon span[data-v-37dd416d] {\n    position: absolute;\n    display: block;\n    width: 225px;\n    padding: 2px 0;\n    background-color: #3498db;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 600 14px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n.ribbon-free span[data-v-37dd416d] {\n    position: absolute;\n    display: block;\n    width: 225px;\n    padding: 2px 0;\n    background-color: #404346;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 600 14px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n  /* top left*/\n.ribbon-top-left[data-v-37dd416d] {\n    top: -10px;\n    left: -10px;\n}\n.ribbon-top-left[data-v-37dd416d]::before,\n  .ribbon-top-left[data-v-37dd416d]::after {\n    border-top-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-top-left[data-v-37dd416d]::before {\n    top: 0;\n    right: 0;\n}\n.ribbon-top-left[data-v-37dd416d]::after {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-top-left span[data-v-37dd416d] {\n    right: -25px;\n    top: 30px;\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n}\n\n  /* top right*/\n.ribbon-top-right[data-v-37dd416d] {\n    top: -15px;\n    right: -40px;\n}\n.ribbon-top-right[data-v-37dd416d]::before,\n  .ribbon-top-right[data-v-37dd416d]::after {\n    border-top-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-top-right[data-v-37dd416d]::before {\n    top: 0;\n    left: 0;\n}\n.ribbon-top-right[data-v-37dd416d]::after {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-top-right span[data-v-37dd416d] {\n    left: -25px;\n    top: 30px;\n    -webkit-transform: rotate(45deg);\n            transform: rotate(45deg);\n}\n\n  /* bottom left*/\n.ribbon-bottom-left[data-v-37dd416d] {\n    bottom: -10px;\n    left: -10px;\n}\n.ribbon-bottom-left[data-v-37dd416d]::before,\n  .ribbon-bottom-left[data-v-37dd416d]::after {\n    border-bottom-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-37dd416d]::before {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-bottom-left[data-v-37dd416d]::after {\n    top: 0;\n    left: 0;\n}\n.ribbon-bottom-left span[data-v-37dd416d] {\n    right: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(225deg);\n            transform: rotate(225deg);\n}\n\n  /* bottom right*/\n.ribbon-bottom-right[data-v-37dd416d] {\n    bottom: -10px;\n    right: -10px;\n}\n.ribbon-bottom-right[data-v-37dd416d]::before,\n  .ribbon-bottom-right[data-v-37dd416d]::after {\n    border-bottom-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-37dd416d]::before {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-bottom-right[data-v-37dd416d]::after {\n    top: 0;\n    right: 0;\n}\n.ribbon-bottom-right span[data-v-37dd416d] {\n    left: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(-225deg);\n            transform: rotate(-225deg);\n}\n.container[data-v-37dd416d] {\n    padding-top: 20px;\n    padding-bottom: 40px;\n    padding-left: 20px;\n    padding-right: 20px;\n}\n.courseTit[data-v-37dd416d] {\n    text-transform: capitalize;\n    font-size: 13px;\n    font-weight: normal;\n    max-height: 40px;\n}\n.price[data-v-37dd416d] {\n    font-size: 12px;\n    padding: 0;\n}\n.courseOver[data-v-37dd416d] {\n    font-size: 12.2px;\n}\np.courseTit[data-v-37dd416d] {\n    margin-bottom: 0;\n}\n  /* .swiper-slide {\n    height: 130px;\n  } */\n  /* .prodDis {\n    height: 270px !important;\n  } */\n.imgHold[data-v-37dd416d] {\n    position: relative;\n    height: auto;\n}\n.industry[data-v-37dd416d] {\n    height: 150px;\n    margin: 0;\n}\n.industry_content[data-v-37dd416d] {\n    height: 150px;\n    padding: 60px 0;\n}\n.imgDiv[data-v-37dd416d] {\n    position: relative;\n    width: 100%;\n    height: 250px;\n}\nimg[data-v-37dd416d] {\n    width: 100%;\n    height: 100%;\n}\n.courseText[data-v-37dd416d] {\n    position: absolute;\n    top: -20px;\n    color: #ffffff;\n    margin: 100px 20px;\n}\ninput[data-v-37dd416d] {\n    width: 200px;\n}\n.outline-banner[data-v-37dd416d] {\n    padding: 10px 1px 10px 20px;\n    font-size: calc(11px + (15 - 11) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.mobile[data-v-37dd416d] {\n    display: block;\n}\n.desktop[data-v-37dd416d] {\n    display: none;\n}\n  /* .subjects,\n\t.subject {\n\t\theight: auto;\n\t} */\n.student[data-v-37dd416d] {\n    min-height: 150px;\n}\n.studentViews.mobile[data-v-37dd416d] {\n    display: block;\n    margin: 10px 30px;\n    background: white;\n}\n.studentView.desktop[data-v-37dd416d] {\n    display: none;\n}\n.stud[data-v-37dd416d] {\n}\n.classHis[data-v-37dd416d] {\n    margin-top: 0;\n}\n.courseDiv[data-v-37dd416d] {\n    padding: 5px;\n    margin-top: 0;\n}\n.imgHolder[data-v-37dd416d] {\n    height: 110px;\n}\n.subject_title[data-v-37dd416d] {\n    padding: 0;\n    font-size: 18px;\n}\n.test_story[data-v-37dd416d] {\n    padding: 0 30px;\n}\n}\n@media (max-width: 320px) {\n.imgDiv[data-v-37dd416d] {\n    position: relative;\n    width: 100%;\n    height: 200px;\n}\nimg[data-v-37dd416d] {\n    width: 100%;\n    height: 100%;\n}\n.courseText[data-v-37dd416d] {\n    position: absolute;\n    top: -20px;\n    color: #ffffff;\n    margin: 100px 20px;\n}\ninput[data-v-37dd416d] {\n    width: 150px;\n}\n}\n.industry[data-v-37dd416d] {\n  position: relative;\n  width: 100%;\n  height: 150px;\n  margin: 10px;\n}\n.industry_content[data-v-37dd416d] {\n  position: absolute; /* Position the background text */\n  top: 0;\n  background: radial-gradient(rgb(0, 0, 0), rgb(79, 79, 79));\n  background: radial-gradient(rgb(0, 0, 0, 0.5), rgb(79, 79, 79, 0.5));\n  color: #f1f1f1;\n  width: 100%;\n  padding: 60px 0;\n  height: 150px;\n  text-align: center;\n  /* padding:130px 0; */\n}\n.industry_title[data-v-37dd416d] {\n  font-size: calc(14px + (30 - 14) * ((100vw - 300px) / (1600-300)));\n}\n.ind[data-v-37dd416d] {\n  /* background: #f1f1f1; */\n  padding-top: 40px;\n}\n.industry img[data-v-37dd416d] {\n  vertical-align: middle;\n  width: 100%;\n  height: 100%;\n}\n.shadowA[data-v-37dd416d] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  background: #1620284d;\n  color: #f1f1f1;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n}\n.inText[data-v-37dd416d] {\n  position: absolute;\n  top: 12%;\n  left: 5%;\n  color: #fff;\n  font-size: 1.7em;\n}\n.businessBgVid[data-v-37dd416d] {\n  background-color: #f5f5f5 !important;\n  padding: 15px 19px 300px 25px;\n  /* height: 278px; */\n  margin: 10px;\n  position: relative;\n  color: #5b84a7;\n  font-weight: bold;\n  font-size: 25px;\n}\n.courseDet[data-v-37dd416d] {\n  /* background: #5b84a7; */\n  color: #ffffff;\n  padding: 0px 20px;\n  border-radius: 5px;\n}\n.courseDet a[data-v-37dd416d] {\n  color: #ffffff !important;\n}\n.courseDet a[data-v-37dd416d]:hover {\n  color: #000000 !important;\n  font-weight: bold;\n}\n.diverHeader[data-v-37dd416d] {\n  text-align: center;\n  font-size: 16px;\n  font-weight: bold;\n  margin-bottom: 30px;\n}\n.courseDet[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin: 0 30px;\n}\n.businessDiv[data-v-37dd416d] {\n  background-color: #fff !important;\n  padding-top: 30px;\n}\n.cat-arrow[data-v-37dd416d] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: #000000;\n  font-size: 34px;\n}\n.skills[data-v-37dd416d] {\n  margin: 5px;\n  position: relative;\n  font-weight: bold;\n  text-transform: capitalize;\n  font-size: calc(9px + (16 - 9) * ((100vw - 300px) / (1600 - 300))) !important;\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  text-align: center;\n  height: 250px;\n}\n.skills img[data-v-37dd416d] {\n  width: 100%;\n  height: 100%;\n}\n.gainShadow[data-v-37dd416d] {\n  position: absolute; /* Position the background text */\n  top: 0;\n  background: radial-gradient(rgb(118, 117, 150), rgb(27, 25, 25));\n  /* background: radial-gradient(\n\t\tto right,\n\t\trgb(46, 44, 117),\n\t\trgb(79, 79, 79, 0.5)); */\n  color: #f1f1f1;\n  width: 100%;\n  /* padding: 60px 0; */\n  height: 100%;\n  text-align: center;\n  /* padding:130px 0; */\n}\n.brandShadow[data-v-37dd416d] {\n  position: absolute; /* Position the background text */\n  top: 0;\n  background: radial-gradient(rgb(79, 79, 79, 0.5), rgb(0, 0, 0, 0.5));\n  /* background: radial-gradient(\n\t\tto right,\n\t\trgb(0, 0, 0,0.5),\n\t\trgb(79, 79, 79, 0.5)); */\n  color: #f1f1f1;\n  width: 100%;\n  /* padding: 60px 0; */\n  height: 100%;\n  text-align: center;\n  /* padding:130px 0; */\n}\n.brandingContent[data-v-37dd416d] {\n  position: absolute;\n  width: 100%;\n  text-align: center;\n  margin-left: 0;\n  margin-right: 0;\n  text-transform: capitalize;\n  color: #ffffff;\n  font-size: calc(16px + (56-16) * ((100vw - 300px) / (1600-300)));\n  line-height: 1.5;\n  margin-top: auto;\n  margin-bottom: auto;\n  padding: 110px 0px;\n  font-weight: initial;\n}\n\n/*  */\n.businessSkill[data-v-37dd416d] {\n  background-color: #f5f5f5 !important;\n  padding: 10px 0;\n  margin: 0 15px;\n  border-radius: 10px;\n}\n.cat-arrow[data-v-37dd416d] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: #ffffff;\n  font-size: 34px;\n}\n.branding[data-v-37dd416d] {\n  background-color: #5b84a7 !important;\n}\n.innovation[data-v-37dd416d] {\n  background-color: #d4eaf3 !important;\n}\n.timemgt[data-v-37dd416d] {\n  background-color: #d2e0eb !important;\n}\n.procurement[data-v-37dd416d] {\n  background-color: #f8cadf !important;\n}\n.gainTitle[data-v-37dd416d] {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  text-align: center;\n  color: hsl(0, 0%, 90%);\n  margin-left: 0;\n  margin-right: 0;\n  width: 100%;\n  font-size: calc(12px + (16-11) * ((100vw - 300px) / (1600-300)));\n  line-height: 1.5;\n  font-family: \"Josefin Sans\";\n}\n@media (max-width: 768px) {\n.gainTitle[data-v-37dd416d] {\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    text-align: center;\n    color: #ffffff;\n    margin-left: 0;\n    margin-right: 0;\n    width: 100%;\n    line-height: 1.5;\n}\n.test_name[data-v-37dd416d] {\n    font-size: 14px;\n}\n.test_story[data-v-37dd416d] {\n    font-size: 14px;\n}\n}\n.socialMediaTitle[data-v-37dd416d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: #e1e1e1 !important;\n\n  width: 100%;\n  color: #ffffff;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n}\n.socialMediaTitle i[data-v-37dd416d] {\n  margin: 10px 12px;\n}\n.artTitle[data-v-37dd416d] {\n  height: 100px;\n  position: relative;\n  -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n          box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n  margin-bottom: 10px;\n}\n.artImage[data-v-37dd416d] {\n  left: 0;\n  right: 0;\n  height: 100%;\n}\n.artImage img[data-v-37dd416d] {\n  /* height: 100%; */\n}\n.mobile-slide[data-v-37dd416d] {\n  display: none;\n  margin-bottom: 20px;\n}\n.ipad-slide[data-v-37dd416d] {\n  display: none;\n  margin-bottom: 20px;\n}\n.desktop-slide[data-v-37dd416d] {\n  margin-top: 40px;\n  margin-bottom: 30px;\n  padding: 30px 20px;\n}\n.bpr[data-v-37dd416d] {\n  /* background: #f1f1f1; */\n  margin: 15px;\n  border-radius: 10px;\n  padding: 5px;\n}\n@media only screen and (max-width: 768px) {\n.mobile-slide[data-v-37dd416d] {\n    display: none;\n}\n.ipad-slide[data-v-37dd416d] {\n    display: inline;\n}\n.ipad-business[data-v-37dd416d] {\n    display: inline;\n}\n.desktop-slide[data-v-37dd416d] {\n    display: none;\n}\n.mobile-business[data-v-37dd416d] {\n    display: none;\n}\n.businessDiv[data-v-37dd416d] {\n    display: none;\n}\n.businessBg[data-v-37dd416d] {\n    width: 100%;\n}\n.inText[data-v-37dd416d] {\n    font-size: 1.2em;\n}\n.artTitle[data-v-37dd416d] {\n    height: 100px;\n    margin: 10px 0;\n}\n}\n@media (max-width: 575px) {\n  /* .prodDis{\n\t\twidth:150px !important;\n\t} */\n.industry_header[data-v-37dd416d] {\n    font-size: 16px !important;\n}\n.businessDiv[data-v-37dd416d] {\n    display: none;\n}\n.mobile-slide[data-v-37dd416d] {\n    display: inline;\n}\n  /* .mobile-slide.ind {\n\t\tmargin-top: -;\n\t} */\n.ipad-slide[data-v-37dd416d] {\n    display: none;\n}\n.ipad-business[data-v-37dd416d] {\n    display: none;\n}\n.desktop-slide[data-v-37dd416d] {\n    display: none;\n}\n.mobile-business[data-v-37dd416d] {\n    display: inline;\n}\n  /* .VueCarousel-slide {\n\t\theight: 300px;\n\t} */\n\n  /* .artImage {\n\t\tleft: unset;\n\t\tright: 40px;\n\t\theight: 4100%;\n\t} */\n.artTitle[data-v-37dd416d] {\n    height: 100px;\n}\n.skills[data-v-37dd416d] {\n    height: 150px;\n}\n.brandingContent[data-v-37dd416d] {\n    padding: 70px 0px;\n}\n.industry[data-v-37dd416d] {\n    height: 150px;\n    margin: 0;\n}\n.industry_content[data-v-37dd416d] {\n    height: 150px;\n    padding: 60px 0;\n}\n.businessBg[data-v-37dd416d] {\n    font-size: 16px;\n}\n.VueCarousel-navigation-button[data-v-37dd416d] {\n    top: 26% !important;\n}\n}\n@media (max-width: 375px) {\n.socialMediaTitle[data-v-37dd416d] {\n    font-size: 8px;\n}\n}\n@media only screen and (max-width: 320px) {\n.businessDiv[data-v-37dd416d] {\n    display: none;\n}\n.mobile-slide[data-v-37dd416d] {\n    display: inline;\n}\n.ipad-slide[data-v-37dd416d] {\n    display: none;\n}\n.ipad-business[data-v-37dd416d] {\n    display: none;\n}\n.desktop-slide[data-v-37dd416d] {\n    display: none;\n}\n.mobile-business[data-v-37dd416d] {\n    display: inline;\n}\n.brandingContent[data-v-37dd416d] {\n    padding: 70px 0px;\n}\n  /* .VueCarousel-slide {\n    height: 310px;\n  } */\n.artTitle[data-v-37dd416d] {\n    height: 80px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1914:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__navigation__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__navigation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__navigation__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      courses: [],
      user: {},
      swiperOption: {
        slidesPerView: 5,
        // slidesPerColumn: 2,
        spaceBetween: 20,
        breakpoints: {
          425: {
            slidesPerView: 2,
            slidesPerColumn: 2,
            spaceBetween: 15
          }
        },
        // loop: true,
        // loopFillGroupWithBlank: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        }
      }
    };
  },

  components: {
    Navigation: __WEBPACK_IMPORTED_MODULE_0__navigation___default.a
  },
  mounted: function mounted() {
    this.getCourses();
    var user = JSON.parse(localStorage.getItem('authUser'));
    this.user = user;
  },

  methods: {
    getCourses: function getCourses() {
      var _this = this;

      axios.get("/api/all-industries").then(function (response) {
        if (response.status === 200) {
          _this.industry_name = response.data;

          axios.get("/api/get-course/" + user.id).then(function (response) {
            if (response.status === 200) {

              var course = response.data.data;

              course.forEach(function (item) {
                item.courses.category = item.category.name;
                item.courses.uid = item.id;
                item.courses.price = item.hardCopyPrice;
                item.courses.softPrice = item.softCopyPrice;
                item.courses.readOnlinePrice = item.readOnlinePrice;
                item.courses.audioPrice = item.audioPrice;
                item.courses.videoPrice = item.videoPrice;
                item.courses.prodCategory = item.prodCategoryType;
                item.courses.vendor = item.vendor.storeName;
                item.courses.coverImage = item.coverImage;
                item.courses.subjectMatter = item.subjectMatter.name.toLowerCase();
                item.courses.industryName = item.industry.name;
                _this.courses.push(item.courses);
              });
            }
          }).catch(function (error) {
            console.log(error);
          }), axios.get("/api/topics").then(function (response) {
            if (response.status === 200) {
              _this.topics = response.data;
            }
          });
        }
      });
    }
  }
});

/***/ }),

/***/ 1915:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main_container" },
    [
      _c("Navigation"),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "courses_content" },
        [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Courses")]),
          _vm._v(" "),
          _c(
            "swiper",
            { attrs: { options: _vm.swiperOption } },
            [
              _vm._l(_vm.courses, function(course, index) {
                return _c(
                  "swiper-slide",
                  { key: index, staticClass: "prodDis mt-4" },
                  [
                    _c(
                      "div",
                      { staticClass: "imgHold" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "routerlink",
                            attrs: {
                              to: {
                                name: "ClassroomCourse",
                                params: {
                                  id: course.uid,
                                  name: course.title.replace(/ /g, "-")
                                }
                              },
                              tag: "a"
                            }
                          },
                          [
                            _c("div", { staticClass: "imgHolder" }, [
                              course.prodCategory === "BP" ||
                              course.prodCategory === "EP" ||
                              course.prodCategory === "VP"
                                ? _c(
                                    "div",
                                    { staticClass: "ribbon ribbon-top-right" },
                                    [_c("span", [_vm._v("Paid")])]
                                  )
                                : _c(
                                    "div",
                                    {
                                      staticClass:
                                        "ribbon ribbon-free ribbon-top-right"
                                    },
                                    [_c("span", [_vm._v("free")])]
                                  ),
                              _vm._v(" "),
                              _c("img", {
                                staticClass: "catImage",
                                attrs: { src: course.coverImage }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "courseDiv" }, [
                              _c("p", { staticClass: "courseTit" }, [
                                _vm._v(_vm._s(course.title))
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "courseOver" }, [
                                _vm._v(_vm._s(course.overview))
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "courseLevel" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(course.vendor) +
                                    "\n                  "
                                ),
                                _c("br"),
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(course.subjectMatter) +
                                    " / " +
                                    _vm._s(course.industryName) +
                                    "\n                  "
                                ),
                                _c("br"),
                                _vm._v(
                                  "\n                  Level : " +
                                    _vm._s(course.level) +
                                    "\n                "
                                )
                              ])
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "swiper-button-prev swiper-button-black",
                attrs: { slot: "button-prev" },
                slot: "button-prev"
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "swiper-button-next swiper-button-black",
                attrs: { slot: "button-next" },
                slot: "button-next"
              })
            ],
            2
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "banner" }, [
      _c("img", { attrs: { src: "/images/course.jpeg" } }),
      _vm._v(" "),
      _c("div", { staticClass: "text" }, [
        _c("h2", [_vm._v("Your Online Classroom")]),
        _vm._v(" "),
        _c("p", [_vm._v("Learn, Save and grow your expertise")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-37dd416d", module.exports)
  }
}

/***/ }),

/***/ 1916:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1917);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("a6acdc50", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-92ca8a16\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ilcCourse.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-92ca8a16\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ilcCourse.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1917:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.courseTitle[data-v-92ca8a16] {\n    background: hsl(207, 43%, 20%);\n    background-size: cover;\n    padding: 82px 90px;\n    text-align: left;\n    color: hsl(207, 43%, 20%);\n}\n.liked[data-v-92ca8a16]{\n  color:hsl(207, 43%, 20%);\n}\n.cartAdded[data-v-92ca8a16] {\n  position: absolute;\n  color: hsl(207, 43%, 20%);\n  top: 8px;\n  left: 8px;\n  width: 20px;\n  height: 20px;\n  z-index: 3;\n  background: #f7f8fa;\n  border-radius: 50%;\n}\nth[data-v-92ca8a16],td[data-v-92ca8a16]{\n  text-align:left\n}\n.cartA[data-v-92ca8a16] {\n  font-size: 11px;\n  position: absolute;\n  top: 4px;\n  left: 3px;\n}\n.newButtons[data-v-92ca8a16] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  /* background: #f7f8fa; */\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  margin-top: 20px;\n  margin-bottom: 20px;\n  padding: 5px;\n}\n.newButton[data-v-92ca8a16] {\n  position: relative;\n  width: 50px;\n  height: 50px;\n  max-width: 50px;\n  max-height: 50px;\n  border-radius: 50%;\n  color: #ffffff;\n  cursor: pointer;\n  background: hsl(207, 43%, 20%);\n}\n.addedButton[data-v-92ca8a16] {\n  color: rgb(255, 255, 255, 0.5);\n  background: rgb(91, 132, 167, 0.5);\n}\n.newButton[data-v-92ca8a16]:hover {\n  -webkit-box-shadow: 2px 4px 5px 2px rgba(20, 23, 28, 0.15);\n          box-shadow: 2px 4px 5px 2px rgba(20, 23, 28, 0.15);\n  color: #ffffff;\n  background: rgb(91, 132, 167, 0.5);\n}\n.newButton[data-v-92ca8a16]::after {\n  -webkit-box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n          box-shadow: 4px 6px 5px 3px rgba(20, 23, 28, 0.15);\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.buttonText[data-v-92ca8a16] {\n  visibility: hidden;\n  width: 120px;\n  background-color: hsl(207, 43%, 20%);\n  font-size: 12px;\n  color: #fff;\n  text-align: center;\n  padding: 2px;\n  border-radius: 5px;\n  position: absolute;\n  z-index: 1;\n  bottom: 125%;\n  left: 50%;\n  margin-left: -75px;\n  opacity: 0;\n  -webkit-transition: opacity 0.3s;\n  transition: opacity 0.3s;\n}\n.buttonText[data-v-92ca8a16]::after {\n  content: \"\";\n  position: absolute;\n  top: 100%;\n  left: 50%;\n  margin-left: -5px;\n  border-width: 5px;\n  border-style: solid;\n  border-color: hsl(207, 43%, 20%) transparent transparent transparent;\n}\n.newButton:hover .buttonText[data-v-92ca8a16] {\n  visibility: visible;\n  opacity: 1;\n}\n.faBu[data-v-92ca8a16] {\n  padding: 15px;\n  font-size: 20px;\n}\n.strike1[data-v-92ca8a16] {\n  border-top: 2px solid hsl(207, 43%, 20%);\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg);\n  position: absolute;\n  top: 45%;\n  z-index: 40;\n  width: 64px;\n  left: -7px;\n}\n.strike2[data-v-92ca8a16] {\n  border-top: 2px solid hsl(207, 43%, 20%);\n  -webkit-transform: rotate(45deg);\n  transform: rotate(-45deg);\n  position: absolute;\n  top: 45%;\n  z-index: 40;\n  width: 64px;\n  left: -7px;\n}\n.bread ul[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  list-style-type: none;\n  padding: 5px 15px;\n  background: #fff;\n}\n.bread ul > li[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  float: left;\n  height: 10px;\n  width: auto;\n  font-weight: bold;\n  font-size: 0.8em;\n  cursor: default;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bread ul > li[data-v-92ca8a16]:not(:last-child)::after {\n  content: \"/\";\n  float: right;\n  font-size: 0.8em;\n  margin: 0 0.5em;\n  cursor: default;\n}\n.linked[data-v-92ca8a16] {\n  cursor: pointer;\n  font-size: 1em;\n  font-weight: normal;\n}\n.container-fluid[data-v-92ca8a16] {\n  padding-bottom: 80px;\n}\nol[data-v-92ca8a16],\nul[data-v-92ca8a16] {\n  list-style: none !important;\n}\n.view[data-v-92ca8a16] {\n  display: none;\n}\n/* .facebook__design__flat[data-v-347cbe4d] {\n\tbackground-color: hsl(207, 43%, 20%) !important;\n\tcolor: #fefefe;\n} */\n.table[data-v-92ca8a16] {\n  margin-bottom: 0 !important;\n}\n/* .buttons{\n    padding:10px 40px;\n} */\n.button-social[data-v-347cbe4d][data-v-92ca8a16] {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  cursor: pointer;\n  padding: 0;\n  margin: 0;\n  border-radius: 3px;\n  -moz-border-radius: 3px;\n  -webkit-border-radius: 3px;\n}\n.read[data-v-92ca8a16] {\n  cursor: pointer;\n}\n.instructor[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 65%;\n\n  height: 500px;\n  margin: 20px 30px;\n  padding: 40px 15px;\n}\n.cover[data-v-92ca8a16] {\n  font-size: 9px;\n}\n.price[data-v-92ca8a16] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  line-height: 1.4;\n}\n.seeMore[data-v-92ca8a16] {\n  display: none;\n  color: hsl(207, 43%, 20%);\n  text-transform: capitalize;\n}\n.info[data-v-92ca8a16] {\n  width: 30%;\n  line-height: 1;\n}\n.instructor_container[data-v-92ca8a16] {\n  width: 100px;\n  height: 100px;\n  /* border-radius: 50%; */\n}\n.instructor_name[data-v-92ca8a16] {\n  width: 100%;\n  font-size: 18px;\n  color: #5c7991;\n  padding: 10px;\n}\n.instructor_container img[data-v-92ca8a16] {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n.text[data-v-92ca8a16] {\n  width: 60%;\n  padding: 20px 20px 10px 30px;\n  height: 300px;\n  scroll-behavior: smooth;\n  overflow-y: scroll;\n  border-bottom: 1px thin #888888;\n}\n.course_content[data-v-92ca8a16] {\n  width: 68%;\n  padding: 20px 20px 20px 30px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.rightC[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-left: auto;\n  color: #777777;\n}\n.cc[data-v-92ca8a16] {\n  padding: 0 0px 0 40px;\n  font-size:14px;\n}\n.writtenBy[data-v-92ca8a16] {\n  color: rgba(0, 0, 0, 0.84);\n}\n.writtenBy[data-v-92ca8a16]:hover {\n  cursor: pointer;\n  text-decoration: underline;\n}\n.faqs[data-v-92ca8a16] {\n  padding: 0 30px;\n}\n.titleC[data-v-92ca8a16] {\n  float: left;\n  width: 100%;\n  margin-bottom:14px;\n}\n.titleCC[data-v-92ca8a16] {\n  float: left;\n  width: 100%;\n  padding: 20px;\n}\n.fw[data-v-92ca8a16] {\n  font-weight: 400 !important;\n}\n.ratingSt[data-v-92ca8a16] {\n  overflow: hidden;\n}\n.reviewList[data-v-92ca8a16] {\n  margin-bottom: 20px;\n}\n.rateStyle[data-v-92ca8a16] {\n  float: left;\n  margin: 2px;\n}\n.fa-star-o[data-v-92ca8a16] {\n  color: #000000;\n}\n.lob[data-v-92ca8a16] {\n  display: none;\n}\n.courseDivs[data-v-92ca8a16] {\n  width: 70%;\n}\nvideo[data-v-92ca8a16] {\n  width: 100%;\n}\n.reviewAvatar[data-v-92ca8a16] {\n  text-transform: capitalize;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff;\n  padding: 10px;\n  max-width: 40px;\n  max-height: 40px;\n  border-radius: 50%;\n}\n.rev[data-v-92ca8a16] {\n  width: 100%;\n  height: 100%;\n}\n.courseT[data-v-92ca8a16] {\n  font-size: 31px !important;\n  text-transform: capitalize;\n  font-weight: 500;\n}\n.coursepara[data-v-92ca8a16] {\n  color: hsl(207, 45%, 99%);\n  line-height: 1.5;\n  font-size: 16px;\n  clear: left;\n  font-family:'Josefin Sans';\n}\n.courseOutline[data-v-92ca8a16] {\n  background: #ece9e9;\n  margin: 17px 0 0 0;\n  padding: 6px;\n  font-size: 20px;\n  text-transform: capitalize;\n}\n.courseOutlines[data-v-92ca8a16] {\n  background: #dddddd;\n  padding: 6px;\n}\n.what-you-get__title[data-v-92ca8a16] {\n  font-size: 22px;\n  font-weight: 600;\n  margin: 0 0 10px 0;\n}\n.ui-course[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n}\n.mobile[data-v-92ca8a16] {\n  display: none;\n}\n.ui-course li[data-v-92ca8a16] {\n  width: 45%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: 10px;\n}\n.fa-check[data-v-92ca8a16] {\n  -ms-flex-item-align: start;\n      align-self: flex-start;\n  color: #a1a7b3;\n  font-size: 16px;\n  margin-right: 15px;\n  margin-top: 2px;\n}\n.fa-check.add[data-v-92ca8a16] {\n  color: #ffffff;\n  font-size: inherit;\n  margin: 0;\n}\n.file[data-v-92ca8a16] {\n  font-size: x-small;\n  color: rgba(0, 0, 0, 0.54);\n  text-align: right;\n}\n.whatLearns[data-v-92ca8a16] {\n  float: left;\n  width: 65%;\n  margin: 15px 30px;\n  border-radius: 5px;\n  background-color: #f7f8fa;\n  border: 1px solid #dedfe0;\n  padding: 10px 15px;\n}\n.requirements__list li[data-v-92ca8a16] {\n  margin-bottom: 10px;\n}\n.whatL[data-v-92ca8a16] {\n  float: left;\n  width: 65%;\n  margin: 2px 30px;\n  padding: 10px 15px;\n}\n/* div.whatL ul {\n    list-style: disc;\n} */\n.whatLearnx[data-v-92ca8a16] {\n  float: left;\n  width: 65%;\n  margin: 0px 30px;\n  border-radius: 5px;\n  background-color: #f7f8fa;\n  border: 1px solid #dedfe0;\n  padding: 10px 15px;\n}\n.whatLearnxx[data-v-92ca8a16] {\n  float: left;\n  width: 65%;\n  margin: 0px 30px;\n  border: 1px solid #dedfe0;\n  padding: 10px 15px;\n}\n.shareI[data-v-92ca8a16] {\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: pointer;\n  margin: 10px;\n  border: 1px solid hsl(207, 43%, 20%);\n}\n.sendToLibrary[data-v-92ca8a16] {\n  position: relative;\n  background: hsl(207, 43%, 20%);\n  border: 1px solid hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: pointer;\n  margin: 10px;\n  -webkit-transition-duration: 0.4s; /* Safari */\n  transition-duration: 0.4s;\n  text-decoration: none;\n  overflow: hidden;\n  cursor: pointer;\n}\n.sendToLibrary[data-v-92ca8a16]:hover {\n  background: rgb(149, 172, 191, 0.64);\n  color: #ffffff !important;\n  border: none;\n}\n.sendToLibrary[data-v-92ca8a16]:after {\n  content: \"\";\n  background: rgb(149, 172, 191);\n  display: block;\n  position: absolute;\n  padding-top: 200%;\n  padding-left: 250%;\n  margin-left: -20px !important;\n  margin-top: -120%;\n  opacity: 0;\n  -webkit-transition: all 0.8s;\n  transition: all 0.8s;\n}\n.sendToLibrary[data-v-92ca8a16]:active:after {\n  padding: 0;\n  margin: 0;\n  opacity: 1;\n  -webkit-transition: 0s;\n  transition: 0s;\n}\n.sentToLibrary[data-v-92ca8a16] {\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: default;\n  margin: 10px;\n}\n.addedToWishlist[data-v-92ca8a16] {\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: default;\n  margin: 10px;\n  border: 1px solid hsl(207, 43%, 20%);\n}\n.sendToWishlist[data-v-92ca8a16]:hover {\n  background: rgb(149, 172, 191, 0.64);\n  color: #ffffff !important;\n  border: none;\n}\n.sendToWishlist[data-v-92ca8a16] {\n  position: relative;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: pointer;\n  margin: 10px;\n  -webkit-transition-duration: 0.4s; /* Safari */\n  transition-duration: 0.4s;\n  text-decoration: none;\n  overflow: hidden;\n  cursor: pointer;\n}\n.sendToWishlist[data-v-92ca8a16]:after {\n  content: \"\";\n  background: hsl(207, 43%, 20%);\n  display: block;\n  position: absolute;\n  padding-top: 200%;\n  padding-left: 250%;\n  margin-left: -20px !important;\n  margin-top: -120%;\n  opacity: 0;\n  -webkit-transition: all 0.8s;\n  transition: all 0.8s;\n}\n.sendToWishlist[data-v-92ca8a16]:active:after {\n  padding: 0;\n  margin: 0;\n  opacity: 1;\n  -webkit-transition: 0s;\n  transition: 0s;\n}\n.social[data-v-92ca8a16] {\n  background: #ffffff;\n  text-align: center;\n  border-radius: 3px;\n  padding: 9px 0;\n  cursor: pointer;\n  margin: 10px 0;\n  font-size: x-large !important;\n  color: hsl(207, 43%, 20%) !important;\n}\n.titleWhat[data-v-92ca8a16] {\n  font-size: 20px;\n}\n.whatLearn[data-v-92ca8a16] {\n  float: left;\n  width: 65%;\n  background: #ece9e9;\n  margin: 15px 30px;\n  border-radius: 5px;\n  padding: 10px;\n  border: 1px solid #b7b5b5;\n  -webkit-box-shadow: 0 2px 2px #eee;\n          box-shadow: 0 2px 2px #eee;\n}\n.courseTab[data-v-92ca8a16] {\n  position: relative;\n  font-size: 16px;\n}\n.excerptRealTab[data-v-92ca8a16] {\n  overflow: hidden;\n  background: white;\n}\n.excerptTab[data-v-92ca8a16] {\n  float: right;\n  position: absolute;\n  top: 100px;\n  right: 50px;\n  background: #ffffff;\n  padding: 10px;\n  border: 1px solid #ccc;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  -webkit-box-shadow: 0 2px 2px #ccc;\n          box-shadow: 0 2px 2px #ccc;\n  width: 25%;\n}\n.butt[data-v-92ca8a16] {\n  display: none;\n}\n.faqDiv[data-v-92ca8a16] {\n  margin-bottom: 20px;\n  padding: 3px;\n  border-radius: 3px;\n  border: 1px solid #333;\n}\n.faqQuestion[data-v-92ca8a16] {\n  font-weight: bold;\n  padding-bottom: 5px;\n}\n.iconMoney[data-v-92ca8a16] {\n  font-size: 40px;\n  padding-right: 20px;\n}\n.subSectionHeader[data-v-92ca8a16] {\n  font-size: 22px;\n  line-height: 24px;\n  margin: 36px 0 16px;\n  font-weight: 400;\n  max-width: 100%;\n  padding: 0;\n}\n.relatedCourses[data-v-92ca8a16] {\n  background-color: #f7f8fa;\n}\n.reviewAll[data-v-92ca8a16] {\n  overflow: hidden;\n  font-size: 13px;\n}\n.reviewRate[data-v-92ca8a16],\n.reviewContent[data-v-92ca8a16] {\n  float: left;\n  margin: 3px;\n  font-size: 13px !important;\n  color: #eee;\n}\n.courseLoc[data-v-92ca8a16] {\n  float: none !important;\n  margin-left: 10px;\n}\n.btn-reviews[data-v-92ca8a16] {\n  background-color: hsl(207, 43%, 20%) !important;\n  padding: 10px 20px;\n}\n.reviews[data-v-92ca8a16] {\n  font-size: 14px;\n}\n.reviewSize[data-v-92ca8a16] {\n  font-size: 11px;\n  color: rgba(0, 0, 0, 0.54);\n}\n.reviewSizes[data-v-92ca8a16] {\n  font-size: 14px;\n  color: rgba(0, 0, 0, 0.84);\n  white-space: nowrap;\n}\n.coursePrice[data-v-92ca8a16] {\n  text-align: center;\n  padding: 10px 0;\n  font-weight: bold;\n  font-size: 16px;\n  cursor: pointer;\n}\n.enrolDiv[data-v-92ca8a16] {\n  float: right;\n  margin-top: 30px;\n}\n.roww[data-v-92ca8a16] {\n  padding: 80px;\n}\n.rowws[data-v-92ca8a16] {\n  padding: 80px 0;\n}\n.btn-enroll[data-v-92ca8a16] {\n  background-color: hsl(207, 43%, 20%) !important;\n  color: #ffffff;\n  font-weight: bolder;\n}\n.learn[data-v-92ca8a16] {\n  font-weight: bold;\n}\n.product-dialog[data-v-92ca8a16] {\n  width: 70%;\n  max-width: 70%;\n  margin-top: 130px;\n}\n.rowBottom[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  float: left;\n  width: 70%;\n  margin-top: 6px;\n}\n.enrollContent[data-v-92ca8a16] {\n  float: right;\n  width: 30%;\n}\n.headerCourse[data-v-92ca8a16] {\n  border-bottom: 1px solid #dadada;\n  -webkit-box-shadow: 0px 1px 2px #888888;\n          box-shadow: 0px 1px 2px #888888;\n  overflow: hidden;\n  padding: 10px 0;\n}\n.listCourse[data-v-92ca8a16] {\n  color: #333;\n  line-height: 3;\n  font-size: 20px;\n  font-weight: 400;\n}\n.fa-star-o[data-v-92ca8a16] {\n  color: #000000;\n}\n.cseT[data-v-92ca8a16] {\n  font-size: 30px;\n  font-weight: bold;\n  text-transform: capitalize;\n  line-height: 1;\n}\n.ratingSt[data-v-92ca8a16] {\n  overflow: hidden;\n  font-size: 14px;\n}\n.courseContentOverview[data-v-92ca8a16],\n.courseContentTableOfContent[data-v-92ca8a16],\n.courseContentSponsor[data-v-92ca8a16],\n.courseContentFaq[data-v-92ca8a16],\n.whatYouWillLearn[data-v-92ca8a16] {\n  padding: 50px;\n}\n.fa-circle-o[data-v-92ca8a16] {\n  margin-right: 20px;\n}\n.reviewSign[data-v-92ca8a16] {\n  color: #ffffff;\n}\n.section-header-left[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -ms-flex-preferred-size: 65%;\n      flex-basis: 65%;\n}\n.section-title-wrapper[data-v-92ca8a16] {\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  color: #505763;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  font-size: 18px;\n  font-weight: 600;\n}\n.section-title-toggle[data-v-92ca8a16] {\n  color: #007791;\n  display: inline-block;\n  width: 10px;\n  height: auto !important;\n  margin-left: auto;\n}\n.section-title-toggle-plus[data-v-92ca8a16] {\n  font-size: 20px;\n  cursor: pointer;\n}\n.section-title-text[data-v-92ca8a16] {\n  font-size: 15px;\n  padding-left: 8px;\n  padding-right: 10px;\n  font-weight: 400;\n}\n.section-header-right[data-v-92ca8a16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  color: #505763;\n  -ms-flex-preferred-size: 40%;\n      flex-basis: 40%;\n}\n.num-items-in-section[data-v-92ca8a16] {\n  display: block;\n  margin-right: 20px;\n  white-space: nowrap;\n}\n.section-header-length[data-v-92ca8a16] {\n  text-align: right;\n  width: 42%;\n}\n@media (max-width: 1024px) {\n.whatLearns[data-v-92ca8a16] {\n    width: 60%;\n    margin: 15px 10px;\n}\n.whatL[data-v-92ca8a16] {\n    width: 60%;\n    margin: 10px 10px;\n}\n}\n@media (max-width: 768px) {\n.whatLearns[data-v-92ca8a16] {\n    width: 50%;\n    margin: 15px 10px;\n}\n.whatL[data-v-92ca8a16] {\n    width: 50%;\n    margin: 10px 10px;\n}\n}\n@media (max-width: 425px) {\n.whatL[data-v-92ca8a16]{\n    font-size: 14px;\n}\n.cartAdded[data-v-92ca8a16] {\n    position: absolute;\n    color: hsl(207, 43%, 20%);\n    top: 35px;\n    left: 4px;\n    width: 20px;\n    height: 20px;\n    z-index: 3;\n    background: #f7f8fa;\n    border-radius: 50%;\n}\n.newButtons[data-v-92ca8a16] {\n    padding: 10px;\n    margin-top: 0;\n    background: #ffffff;\n}\n.container-fluid[data-v-92ca8a16] {\n    padding-left: 0px;\n    padding-right: 0px;\n}\n.coursePrice[data-v-92ca8a16] {\n    color: hsl(207, 45%, 99%);\n    text-align: center;\n    padding: 5px 10px;\n    font-weight: bold;\n    font-size: 32px;\n    cursor: pointer;\n}\n.courseTab[data-v-92ca8a16] {\n    margin-top: 0;\n}\n.lob[data-v-92ca8a16] {\n    display: inline;\n}\n.reviews[data-v-92ca8a16] {\n    padding: 0;\n}\n.reviewww[data-v-92ca8a16] {\n    padding: 0;\n}\n.courseT[data-v-92ca8a16] {\n    font-size: 18px !important;\n}\n.ui-course li[data-v-92ca8a16]{\n    width:100%;\n}\nli[data-v-92ca8a16]{\n    font-size:14px;\n}\n.table > tbody > tr > td[data-v-92ca8a16]{\n   padding:4px;\n}\n.coursepara[data-v-92ca8a16] {\n    line-height: 1.2;\n    margin-bottom: 5px;\n}\n.course_content[data-v-92ca8a16] {\n    width: 100%;\n    padding: 20px 0 30px 0;\n}\n.courseDivs[data-v-92ca8a16] {\n    width: 100%;\n    padding: 0 20px;\n}\nh3[data-v-92ca8a16] {\n    font-size: 16px;\n    font-weight: bold;\n}\n.seeMore[data-v-92ca8a16] {\n    display: block;\n    font-size: 11px;\n}\n.whatL[data-v-92ca8a16] {\n    width: 100%;\n    margin: 15px 0;\n}\n.toggleOff[data-v-92ca8a16] {\n    display: none;\n}\n.toggleOn[data-v-92ca8a16] {\n    display: block;\n}\n.info[data-v-92ca8a16],\n  .text[data-v-92ca8a16] {\n    width: 100%;\n}\n.info[data-v-92ca8a16] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.instructor[data-v-92ca8a16] {\n    width: 100%;\n    margin: 20px 0;\n    height: auto;\n}\n.instructor_container[data-v-92ca8a16] {\n}\n.instructor_stat[data-v-92ca8a16] {\n    margin: 0 auto;\n}\n.text[data-v-92ca8a16] {\n    width: 100%;\n    padding: 5px;\n    height: auto;\n    /* scroll-behavior: smooth;\n     overflow-y: hidden;\n     border-bottom:1px thin #888888; */\n}\n.excerptTab[data-v-92ca8a16] {\n    display: none;\n}\n.whatLearns[data-v-92ca8a16] {\n    float: left;\n    width: 100%;\n    margin: 0;\n}\n.faqs[data-v-92ca8a16] {\n    padding: 0 10px;\n}\n.whatLearnx[data-v-92ca8a16] {\n    width: 100% !important;\n    margin: 0;\n}\n.whatLearnxx[data-v-92ca8a16] {\n    width: 100% !important;\n    margin: 0;\n}\n.instructot[data-v-92ca8a16]{\n    padding:40px 0;\n}\n.courseTitle[data-v-92ca8a16] {\n    padding: 5px 0px 10px 0px;\n}\n.excerptRealTab[data-v-92ca8a16] {\n    padding: 20px;\n}\n.mobile[data-v-92ca8a16] {\n    display: block;\n    margin-bottom: 5px;\n    text-align: left;\n}\n.bread[data-v-92ca8a16]{\n    display:none;\n}\n.butt[data-v-92ca8a16] {\n    display: block;\n    margin-top: 20px;\n    text-align: center;\n    padding:10px 20px 0;\n}\n.share[data-v-92ca8a16] {\n    text-align: center;\n    color: hsl(207, 43%, 20%);\n    font-weight: bold;\n}\n.includes_header[data-v-92ca8a16] {\n    font-size: 16px;\n    margin-top: 10px;\n    font-weight: bold;\n    text-align: left;\n}\n.includes[data-v-92ca8a16] {\n    margin-top: 10px;\n    text-align: left;\n}\nsmall.money_back[data-v-92ca8a16] {\n    text-align: center !important;\n    font-size: 75% !important;\n}\n.section-title-text[data-v-92ca8a16]{\n    font-size:14px;\n    padding:0;\n    line-height: 1.3;\n}\ntd[data-v-92ca8a16]{\n    font-size: 12px !important;\n    font-weight: 400;\n}\np[data-v-92ca8a16]{\n    font-size:14px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1918:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navigation__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navigation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__navigation__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-admin-full-course-component",
  data: function data() {
    return _defineProperty({
      id: this.$route.params.id,
      title: this.$route.params.name,
      url: "https://bizguruh.com/courses/" + this.$route.params.id + "/" + this.$route.params.name,
      courseDetail: {},
      courseModule: [],
      epiFade: true,
      epiModal: true,
      cartDi: true,
      inCart: false,
      price: "",
      breadcrumbList: [],
      email: "",
      showHide: "",
      user_id: "",
      hiddenAudio: "",
      hiddenVideo: "",
      videoDuration: "NaN:NaN:NaN",
      audioDuration: "NaN:NaN:NaN",
      user: {},
      vendor: {},
      vendorDetail: [],
      vendorCourses: [],
      anonymousCart: [],
      loginCart: [],
      storeName: "",
      review: {
        productId: "",
        title: "",
        description: "",
        rating: 0
      },
      star: 0,
      rateFa: true,
      NotratedOne: true,
      NotratedTwo: true,
      RatedTwo: false,
      RatedOne: false,
      NotratedThree: true,
      RatedThree: false,
      NotratedFour: true,
      RatedFour: false,
      NotratedFive: true,
      RatedFive: false,
      modulesCount: "",
      authenticate: false,
      allReviews: [],
      whatYouWillLearn: [],
      indexCheck: "",
      faqCheck: "",
      isShown: false,
      isClicked: false,
      added: false,
      wishes: 0,
      read: "ellipsis",
      over: "hidden",
      plus: "+",
      faq_plus: "+",
      update: 0,
      bought: false,
      vidDur: [],
      audDur: [],
      startDate: null,
      startDateParsed: null,
      currentDate: null,
      endDate: null,
      freeTrial: true,
      isActive: true,
      isLiked: false,
      isUnliked: false,
      allLikes: [],
      allUnlikes: []
    }, "update", 0);
  },
  components: {
    SocialSharing: __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default.a,
    Navigation: __WEBPACK_IMPORTED_MODULE_2__navigation___default.a
  },
  watch: {
    $route: function $route() {
      this.updateList();
    },

    'update': 'updateThis'
  },
  created: function created() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user != null) {
      this.token = user.access_token;
      axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_1__config__["b" /* getCustomerHeader */])() }).then(function (response) {
        var userDate = response.data.created_at;
        _this.startDate = new Date(userDate);

        _this.startDateParsed = Date.parse(new Date(userDate));

        _this.currentDate = Date.parse(new Date());

        _this.endDate = Date.parse(new Date(_this.startDate.setDate(_this.startDate.getDate(userDate) + 30)));

        if (_this.currentDate >= _this.endDate) {
          console.log("Trial Over");
          _this.freeTrial = false;
        } else {
          console.log("Still on Trial");
        }
      });
    }

    this.updateList();
    axios.get("/api/course-detail/" + this.id).then(function (response) {
      if (response.status === 200) {
        _this.courseModule = response.data;

        _this.courseModule.forEach(function (item) {
          _this.vidDur.push(Number(item.vidTime), Number(item.audTime));
        });
        _this.getLike(_this.id);
        _this.getUnLike(_this.id);
      }
    });

    axios.get("/api/product-detail/" + this.id).then(function (response) {
      if (response.status === 200) {

        _this.courseDetail = response.data.data[0].courses;
        _this.courseDetail.modules = response.data.data[0].coursesModule;
        _this.courseDetail.modules.forEach(function (item) {
          item.hideNot = 0;
        });
        _this.courseDetail.subscriptionLevel = response.data.data[0].subscriptionLevel;
        _this.courseDetail.uid = response.data.data[0].id;
        _this.courseDetail.vendor = response.data.data[0].vendor;
        _this.storeName = response.data.data[0].vendor.storeName;
        _this.courseDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
        _this.courseDetail.industry = response.data.data[0].industry.name;
        _this.courseDetail.audioPrice = response.data.data[0].audioPrice;
        _this.courseDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
        _this.courseDetail.whatYouWillLearnHere = response.data.data[0].whatYouWillLearn;
        _this.courseDetail.softCopyPrice = response.data.data[0].softCopyPrice;
        _this.courseDetail.videoPrice = response.data.data[0].videoPrice;
        _this.courseDetail.faq = response.data.data[0].faq;
        _this.courseDetail.rating = response.data.data[0].rating;
        _this.courseDetail.ratingCount = response.data.data[0].ratingCount;
        _this.courseDetail.broughtByStu = response.data.data[0].broughtByStu;
        _this.modulesCount = _this.courseDetail.modules.length;
        _this.courseDetail.prodCategoryType = response.data.data[0].prodCategoryType;

        _this.getProductReviews(_this.courseDetail.uid);
        if (_this.user !== null) {
          axios.post("/api/cart/check/" + _this.user.id + "/" + _this.courseDetail.uid).then(function (response) {
            if (response.data === true) {
              _this.inCart = true;
            } else {
              _this.inCart = false;
            }
          });
          // this.wishlist();
        }

        axios.get("/api/vendor/" + response.data.data[0].vendor.id).then(function (response) {
          _this.vendor = response.data.vendor[0];

          _this.vendorDetail = response.data.vendorDetail[0];
          if (_this.user !== null) {
            _this.$emit('reach', user.id, _this.courseDetail.uid, _this.vendorDetail.id);
            _this.$emit("activity", _this.vendorDetail.id, 0, 1);
            _this.$emit("top-content", _this.vendorDetail.id, _this.courseDetail.uid, 1);
            _this.$emit("top-industry", _this.vendorDetail.id, _this.courseDetail.industry, 1);
            _this.$emit("add-location", _this.vendorDetail.id, user.location);
            _this.$emit("add-gender", _this.vendorDetail.id, user.gender);
            _this.$emit('add-age', _this.vendorDetail.id, user.age);
            _this.$emit('interaction', _this.vendorDetail.id, 'views');
          }
        });
        axios.get("/api/vendorCourses/" + response.data.data[0].vendor.id).then(function (response) {
          _this.vendorCourses = response.data.data;
          _this.isActive = false;
        });
      }
    }).catch(function (error) {
      console.log(error);
    });
  },
  beforeMount: function beforeMount() {
    var _this2 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      this.authenticate = true;
      this.email = user.email;
      this.user_id = user.id;
      this.user = user;
      this.authenticate = true;
      this.token = user.access_token;

      axios.get("/api/user/order/" + this.id, {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        if (response.data === true) {
          _this2.bought = true;
        } else {
          _this2.bought = false;
        }
      });
    }
  },


  computed: {
    TotalvideoDuration: function TotalvideoDuration() {
      return this.vidDur.reduce(function (a, b) {
        return a + b;
      }, 0);
    },
    myLikes: function myLikes() {
      return this.allLikes.length;
    },
    myUnLikes: function myUnLikes() {
      return this.allUnlikes.length;
    }
  },

  methods: {
    updateThis: function updateThis() {
      this.getLike(this.id);
      this.getUnLike(this.id);
    },
    checkLike: function checkLike(user, product) {
      var _this3 = this;

      axios.get("/api/get-likes/" + user + "/" + product).then(function (response) {
        if (response.status === 200) {
          if (response.data.like == 1 && response.data.unlike == 0) {
            _this3.isLiked = true;
            _this3.isUnliked = false;
          }
          if (response.data.unlike == 1 && response.data.like == 0) {
            _this3.isLiked = false;
            _this3.isUnliked = true;
          }
          if (response.data.unlike == 0 && response.data.like == 0) {
            _this3.isLiked = false;
            _this3.isUnliked = false;
          }
        }
      });
    },
    getLike: function getLike(product) {
      var _this4 = this;

      axios.get("/api/get-all-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this4.allLikes = response.data;
        }
      });
    },
    getUnLike: function getUnLike(product) {
      var _this5 = this;

      axios.get("/api/get-un-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this5.allUnlikes = response.data;
        }
      });
    },
    like: function like(user, product, vendor, type) {
      if (!this.isLiked) {
        this.update++;
        this.isLiked = true;
        this.isUnliked = false;
        this.$emit('likes', user, product, vendor, type);
        this.$emit('interaction', vendor, 'likes');
      }
    },
    unlike: function unlike(user, product, vendor, type) {
      if (!this.isUnliked) {
        this.update++;
        this.isLiked = false;
        this.isUnliked = true;
        this.$emit('likes', user, product, vendor, type);
      }
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    time: function time(num) {
      var secs = String(Math.floor(num % 60)).padStart(2, "0");
      var mins = String(Math.floor(num / 60)).padStart(2, "0");
      var hours = String(Math.floor(num / 3600)).padStart(2, "0");

      if (hours === "00") {
        return mins + ":" + secs;
      } else {
        return hours + ":" + mins + ":" + secs;
      }
    },
    seeMore: function seeMore() {
      this.isShown = !this.isShown;
    },
    click: function click() {
      this.isClicked = !this.isClicked;
    },
    courseHide: function courseHide(params, type) {
      if (type === "modules") {
        if (params === this.indexCheck) {
          this.indexCheck = "";
          this.plus = "+";
        } else {
          this.indexCheck = params;
          this.plus = "-";
        }
      } else if (type === "faq") {
        if (params === this.faqCheck) {
          this.faqCheck = "";
          this.faq_plus = "+";
        } else {
          this.faqCheck = params;
          this.faq_plus = "-";
        }
      }
    },
    getProductReviews: function getProductReviews(id) {
      var _this6 = this;

      var data = {
        id: id
      };

      axios.post("/api/product/get-reviews", JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          _this6.allReviews = response.data.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    subscribedForItem: function subscribedForItem(id) {
      var _this7 = this;

      var data = {
        id: id,
        prodType: "Courses",
        getType: "download"
      };

      axios.post("/api/user/sendcoursetolibrary", JSON.parse(JSON.stringify(data)), {
        headers: {
          Authorization: "Bearer " + this.user.access_token
        }
      }).then(function (response) {
        if (response.status === 201) {
          _this7.$toasted.success("Successfully added to library");
          _this7.bought = true;
          _this7.update++;
        } else if (response.data.status === 99) {
          _this7.$toasted.error("Product unavailable for your subscription level");

          var routeData = _this7.$router.resolve({
            name: "SubscriptionProfile",
            params: {
              level: _this7.courseDetail.subLevel
            }
          });

          setTimeout(function () {
            window.open(routeData.href, "_blank");
          }, 5000);
        } else if (response.data === " ") {
          var _routeData = _this7.$router.resolve({
            name: "SubscriptionProfile"
          });
          window.open(_routeData.href, "_blank");
        } else {
          _this7.$toasted.error(response.data);
        }
      }).catch(function (error) {
        console.log(error);
        if (error.response.data.message === "Unauthenticated.") {
          _this7.$router.push({
            name: "auth",
            params: { name: "login" },
            query: { redirect: _this7.$router.fullPath }
          });
        } else {
          _this7.$toasted.error(error.response.data.message);
        }
      });
    },
    enroll: function enroll(id) {
      if (this.courseDetail.prodCategoryType === "BS" || this.courseDetail.prodCategoryType === "ES" || this.courseDetail.prodCategoryType === "VS") {
        this.subscribedForItem(id);
      } else {
        this.addToCart(id);
      }
    },
    submitReview: function submitReview() {
      var _this8 = this;

      if (this.authenticate === false) {
        this.$toasted.error("You must log in to review a product");
      } else {
        this.review.productId = this.courseDetail.uid;
        axios.post("/api/user/reviews", JSON.parse(JSON.stringify(this.review)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          if (response.status === 201) {
            _this8.$toasted.success("Reviews successfully saved");
            _this8.review.productId = _this8.review.title = _this8.review.description = "";
            _this8.review.rating = 0;
            _this8.NotratedOne = _this8.NotratedTwo = _this8.NotratedThree = _this8.NotratedFour = _this8.NotratedFive = true;
            _this8.RatedOne = _this8.RatedTwo = _this8.RatedThree = _this8.RatedFour = _this8.RatedFive = false;
          } else {
            _this8.$toasted.error("You cannot review this product until you purchase it");
          }
        }).catch(function (error) {
          var errors = Object.values(error.response.data.errors);
          errors.forEach(function (item) {
            _this8.$toasted.error(item[0]);
          });
        });
      }
    },
    openModal: function openModal() {
      this.epiFade = false;
    },
    rateWithStar: function rateWithStar(num) {
      switch (num) {
        case "1":
          this.NotratedOne = !this.NotratedOne;
          this.RatedOne = !this.RatedOne;
          this.review.rating = 1;
          break;
        case "2":
          this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
          this.RatedTwo = this.RatedOne = !this.RatedTwo;
          this.review.rating = 2;
          break;
        case "3":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
          this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
          this.review.rating = 3;
          break;
        case "4":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
          this.review.rating = 4;
          break;
        case "5":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
          this.review.rating = 5;
          break;
        default:
          this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
          this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
      }
    },
    addToCart: function addToCart(id) {
      var cart = {
        productId: id,
        price: this.courseDetail.hardCopyPrice,
        prodType: "All-Format"
      };
      if (this.price.format === "HardCopy") {
        cart.quantity = 1;
      } else {
        cart.quantity = 0;
      }

      if (this.authenticate === false) {
        cart.cartNum = 1;
        this.anonymousCart.push(cart);
        if (JSON.parse(localStorage.getItem("userCart")) === null) {
          localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
          var sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var cartCount = sessionCart.length;
          this.$emit("getCartCount", cartCount);
          this.$notify({
            group: "cart",
            title: this.courseDetail.title,
            text: " Added to Cart!"
          });
        } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
          var _sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var ss = _sessionCart.length;
          _sessionCart[ss] = cart;
          localStorage.setItem("userCart", JSON.stringify(_sessionCart));
          var a = JSON.parse(localStorage.getItem("userCart"));
          var aCount = a.length;
          this.$emit("getCartCount", aCount);
          this.$notify({
            group: "cart",
            title: this.courseDetail.title,
            text: "Successfully Added to Cart!"
          });
        }
      } else {
        this.addCart(cart);
      }
    },
    addCart: function addCart(cart) {
      var _this9 = this;

      if (this.inCart === true) {
        this.$toasted.error("Already in cart");
      } else {
        axios.post("/api/cart", JSON.parse(JSON.stringify(cart)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          if (response.status === 201) {
            _this9.update++;
            _this9.inCart = true;
            _this9.loginCart.push(response.data);
            var aCount = _this9.loginCart.length;
            _this9.$emit("getCartCount", aCount);
            _this9.$notify({
              group: "cart",
              title: _this9.courseDetail.title,
              text: "<p><small>Added to Cart!</small></p><p><small>Proceed to Checkout</small></p>"
            });
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    },
    wishlist: function wishlist() {
      var _this10 = this;

      axios.get("/api/wish-list/" + this.courseDetail.uid, {
        headers: {
          Authorization: "Bearer " + this.user.access_token
        }
      }).then(function (response) {
        if (response.data === true) {
          _this10.added = true;
        } else {
          _this10.added = false;
        }
      });
    },
    readMore: function readMore() {
      if (this.read === "ellipsis") {
        this.read = "unset";
        this.over = "unset";
      } else {
        this.read = "ellipsis";
        this.over = "hidden";
      }
    },
    addToWishlist: function addToWishlist() {
      var _this11 = this;

      if (this.authenticate === false) {
        this.$toasted.error("Login to add to wishlist");
      } else {
        var data = {
          userId: this.user_id,
          productId: this.courseDetail.uid,
          vendorId: 1
        };
        if (this.added) {
          this.$toasted.error("Already added to wishlist");
        } else {
          axios.post("/api/wishlist", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this11.added = true;
              _this11.$toasted.success("Successfully added to wishlist");
            } else {
              _this11.$toasted.error("Already added to wishlist");
            }
          }).catch(function (error) {
            console.log(error);
          });
        }
      }
    }
  }
});

/***/ }),

/***/ 1919:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Navigation"),
      _vm._v(" "),
      _vm.isActive
        ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
        : _vm._e(),
      _vm._v(" "),
      _vm.courseDetail && !_vm.isActive
        ? _c("div", { staticClass: "container-fluid" }, [
            _c(
              "div",
              {
                class: { modal: _vm.epiModal, fade: _vm.epiFade },
                attrs: {
                  id: "exampleModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "exampleModalLabel",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  {
                    staticClass: "modal-dialog product-dialog",
                    attrs: { role: "document" }
                  },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _vm._m(1),
                      _vm._v(" "),
                      _c("div", { staticClass: "modal-body" }, [
                        _c("div", { staticClass: "row price" }, [
                          _c("div", { staticClass: "col-md-3" }, [
                            _vm.courseDetail.hardCopyPrice
                              ? _c("div", { staticClass: "coursePric" }, [
                                  _vm._v(
                                    "\n                  ₦" +
                                      _vm._s(_vm.courseDetail.hardCopyPrice) +
                                      ".00\n                  "
                                  )
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-md-3" }, [
                            _vm.courseDetail.softCopyPrice
                              ? _c("div", { staticClass: "coursePric" }, [
                                  _vm._v(
                                    "\n                  ₦" +
                                      _vm._s(_vm.courseDetail.softCopyPrice) +
                                      ".00\n                  "
                                  )
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-md-3" }, [
                            _vm.courseDetail.readOnlinePrice
                              ? _c("div", { staticClass: "coursePric" }, [
                                  _vm._v(
                                    "\n                  ₦" +
                                      _vm._s(_vm.courseDetail.readOnlinePrice) +
                                      ".00\n                  "
                                  )
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-md-3" }, [
                            _vm.courseDetail.videoPrice
                              ? _c("div", { staticClass: "coursePric" }, [
                                  _vm._v(
                                    "\n                  ₦" +
                                      _vm._s(_vm.courseDetail.videoPrice) +
                                      ".00\n                  "
                                  )
                                ])
                              : _vm._e()
                          ])
                        ]),
                        _vm._v(" "),
                        _vm._m(2)
                      ])
                    ])
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "courseTab" }, [
              _c("div", { staticClass: "bread mobile" }, [
                _c(
                  "ul",
                  _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
                    return _c(
                      "li",
                      {
                        key: index,
                        class: { linked: !!breadcrumb.link },
                        on: {
                          click: function($event) {
                            return _vm.routeTo(index)
                          }
                        }
                      },
                      [_vm._v(_vm._s(breadcrumb.name))]
                    )
                  }),
                  0
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "courseTitle" }, [
                _vm.inCart
                  ? _c("span", { staticClass: "cartAdded" }, [
                      _c("i", { staticClass: "fas fa-cart-plus cartA" })
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "video",
                  {
                    staticClass: "mobile",
                    attrs: {
                      controls: "",
                      autoplay: "",
                      src: _vm.courseDetail.excerpt
                    }
                  },
                  [_c("source")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "row price" }, [
                  _vm.courseDetail.hardCopyPrice
                    ? _c("div", { staticClass: "mobile coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.hardCopyPrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.videoPrice
                    ? _c("div", { staticClass: "mobile coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.videoPrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.softCopyPrice
                    ? _c("div", { staticClass: "mobile coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.softCopyPrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.readOnlinePrice
                    ? _c("div", { staticClass: "mobile coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.readOnlinePrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "courseDivs" }, [
                  _c("p", { staticClass: "courseT coursepara" }, [
                    _vm._v(_vm._s(_vm.courseDetail.title))
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "coursepara" }, [
                    _vm._v(_vm._s(_vm.courseDetail.overview))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "reviewAll" }, [
                    _vm.allReviews.length > 0
                      ? _c(
                          "div",
                          _vm._l(_vm.allReviews, function(allReview) {
                            return _c(
                              "div",
                              { staticClass: "row reviewRate" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "ratingSt" },
                                  [
                                    _vm._l(allReview.rating, function(star) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [_c("i", { staticClass: "fa fa-star" })]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _vm._l(5 - allReview.rating, function(
                                      star
                                    ) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-star-o"
                                          })
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ]
                            )
                          }),
                          0
                        )
                      : _c("div", [
                          _c("i", { staticClass: "fa fa-star-o" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star-o" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star-o" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star-o" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star-o" }),
                          _vm._v(" "),
                          _c("span", { staticClass: "reviewSign" }, [
                            _vm._v("No Reviews")
                          ])
                        ]),
                    _vm._v(" "),
                    _vm.courseDetail.rating !== undefined
                      ? _c("div", { staticClass: "reviewContent" }, [
                          _vm._v(
                            "\n              " +
                              _vm._s(_vm.courseDetail.rating) +
                              " (" +
                              _vm._s(
                                _vm.courseDetail.ratingCount + " ratings"
                              ) +
                              ")\n              "
                          ),
                          _c("span", { staticClass: "courseLoc" }, [
                            _vm._v(
                              "\n                " +
                                _vm._s(_vm.courseDetail.broughtByStu) +
                                " student(s)\n                enrolled\n              "
                            )
                          ])
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "reviewAll" }, [
                    _vm.storeName
                      ? _c("div", { staticClass: "coursepara reviewRate" }, [
                          _vm._v("Created by " + _vm._s(_vm.storeName))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "coursepara reviewContent" }, [
                      _vm._v(
                        "\n              Last updated\n              " +
                          _vm._s(
                            _vm._f("moment")(
                              _vm.courseDetail.updated_at,
                              "MM/YYYY"
                            )
                          ) +
                          "\n            "
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "butt" }, [
                _c("div", [
                  _vm.courseDetail.prodCategoryType === "BS" ||
                  _vm.courseDetail.prodCategoryType === "ES" ||
                  _vm.courseDetail.prodCategoryType === "VS"
                    ? _c(
                        "div",
                        {
                          class: {
                            sendToLibrary: !_vm.bought,
                            sentToLibrary: _vm.bought
                          }
                        },
                        [
                          _vm.bought
                            ? _c("span", [
                                _c("i", {
                                  staticClass: "fa fa-list-alt pr-2",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" Added To Library\n            ")
                              ])
                            : _c(
                                "span",
                                {
                                  staticClass: "buttons",
                                  on: {
                                    click: function($event) {
                                      return _vm.enroll(_vm.courseDetail.uid)
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-paper-plane pr-2",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Send To BizLibrary\n            ")
                                ]
                              )
                        ]
                      )
                    : _c(
                        "div",
                        {
                          class: {
                            sendToLibrary: !_vm.bought,
                            sentToLibrary: _vm.bought
                          }
                        },
                        [
                          _vm.bought
                            ? _c("span", [
                                _c("i", {
                                  staticClass: "fa fa-list-alt pr-2",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" Purchased\n            ")
                              ])
                            : _c(
                                "span",
                                {
                                  staticClass: "buttons",
                                  on: {
                                    click: function($event) {
                                      return _vm.enroll(_vm.courseDetail.uid)
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-cart-plus pr-2",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(
                                    "\n              Purchase\n            "
                                  )
                                ]
                              )
                        ]
                      )
                ]),
                _vm._v(" "),
                _c("small", { staticClass: "money_back" }, [
                  _vm._v("30-Day Money-Back Guarantee")
                ]),
                _vm._v(" "),
                _c("div", [
                  _vm.added
                    ? _c("div", { staticClass: "addedToWishlist" }, [
                        _c("i", { staticClass: "fas fa-heart add pr-2" }),
                        _vm._v(" Added to Wishlist\n          ")
                      ])
                    : _c(
                        "div",
                        {
                          staticClass: "sendToWishlist",
                          on: { click: _vm.addToWishlist }
                        },
                        [
                          _c("i", { staticClass: "fas fa-plus pr-2" }),
                          _vm._v(" Add to Wishlist\n          ")
                        ]
                      )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "share" }, [
                  _c(
                    "span",
                    {
                      class: { view: _vm.isClicked },
                      on: { click: _vm.click }
                    },
                    [
                      _c("i", { staticClass: "fas fa-share-square" }),
                      _vm._v(" Share\n          ")
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      class: { view: !_vm.isClicked },
                      on: { click: _vm.click }
                    },
                    [
                      _c("i", { staticClass: "fas fa-window-close" }),
                      _vm._v(" close\n          ")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", [
                  _c(
                    "div",
                    {},
                    [
                      _c("social-sharing", {
                        attrs: {
                          url: _vm.url,
                          title: _vm.courseDetail.title,
                          description: _vm.courseDetail.overview,
                          quote: _vm.courseDetail.overview,
                          hashtags: "bizguruh,business"
                        },
                        inlineTemplate: {
                          render: function() {
                            var _vm = this
                            var _h = _vm.$createElement
                            var _c = _vm._self._c || _h
                            return _c(
                              "div",
                              {
                                staticClass: "social",
                                class: { view: !_vm.isClicked }
                              },
                              [
                                _c(
                                  "network",
                                  { attrs: { network: "facebook" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-facebook fb"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "twitter" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-twitter tweet"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "googleplus" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-google-plus google"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "linkedin" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-linkedin linkedin"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "whatsapp" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-whatsapp whatsapp"
                                    })
                                  ]
                                )
                              ],
                              1
                            )
                          },
                          staticRenderFns: []
                        }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "includes" }, [
                  _c("p", { staticClass: "includes_header" }, [
                    _vm._v("This course includes")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _vm.courseDetail.duration !== ""
                      ? _c("li", [
                          _vm._v(
                            "Duration: " +
                              _vm._s(_vm.time(_vm.TotalvideoDuration))
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "Modules: " + _vm._s(_vm.modulesCount) + " module(s)"
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v("Level: " + _vm._s(_vm.courseDetail.level))
                    ]),
                    _vm._v(" "),
                    _vm.courseDetail.fullOnline
                      ? _c("li", [_vm._v("Full Online")])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.courseDetail.certificate
                      ? _c("li", [_vm._v("Certificate of Completion")])
                      : _vm._e()
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mt-3" }, [
                  _c(
                    "i",
                    {
                      staticClass: "fas fa-thumbs-up mr-3 liked",
                      attrs: { "aria-hidden": "true" },
                      on: {
                        click: function($event) {
                          return _vm.like(
                            _vm.user.id,
                            _vm.id,
                            _vm.vendorDetail.id,
                            "like"
                          )
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.myLikes))]
                  ),
                  _vm._v(" "),
                  _c(
                    "i",
                    {
                      staticClass: "fas fa-thumbs-down  liked  ml-3",
                      on: {
                        click: function($event) {
                          return _vm.unlike(
                            _vm.user.id,
                            _vm.id,
                            _vm.vendorDetail.id,
                            "unlike"
                          )
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.myUnLikes))]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "excerptTab" }, [
                _vm.inCart
                  ? _c("span", { staticClass: "cartAdded" }, [
                      _c("i", { staticClass: "fas fa-cart-plus cartA" })
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "video",
                  {
                    attrs: {
                      id: "testV",
                      controls: "",
                      width: "320px",
                      autoplay: "",
                      src: _vm.courseDetail.excerpt
                    }
                  },
                  [_c("source")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "row price" }, [
                  _vm.courseDetail.hardCopyPrice
                    ? _c("div", { staticClass: "coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.hardCopyPrice) +
                            ".00\n            "
                        ),
                        _vm._v(
                          "\n            " +
                            _vm._s(_vm.courseDetail.excerpt.duration) +
                            "\n          "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.videoPrice
                    ? _c("div", { staticClass: "coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.videoPrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.softCopyPrice
                    ? _c("div", { staticClass: "coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.softCopyPrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.courseDetail.readOnlinePrice
                    ? _c("div", { staticClass: "coursePrice" }, [
                        _vm._v(
                          "\n            ₦" +
                            _vm._s(_vm.courseDetail.readOnlinePrice) +
                            ".00\n            "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "newButtons" }, [
                  _vm.courseDetail.prodCategoryType === "BS" ||
                  _vm.courseDetail.prodCategoryType === "ES" ||
                  _vm.courseDetail.prodCategoryType === "VS"
                    ? _c("div", [
                        !_vm.bought
                          ? _c(
                              "div",
                              {
                                staticClass: "newButton",
                                attrs: { id: "send" },
                                on: {
                                  click: function($event) {
                                    return _vm.enroll(_vm.courseDetail.uid)
                                  }
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-paper-plane faBu",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "buttonText" }, [
                                  _vm._v("Add to BizLibrary")
                                ])
                              ]
                            )
                          : _c(
                              "div",
                              {
                                staticClass: "newButton addedButton",
                                attrs: { id: "send" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-paper-plane faBu",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "buttonText" }, [
                                  _vm._v("Added to BizLibrary")
                                ])
                              ]
                            )
                      ])
                    : _c("div", [
                        !_vm.bought
                          ? _c(
                              "div",
                              {
                                staticClass: "newButton",
                                attrs: { id: "addCart" },
                                on: {
                                  click: function($event) {
                                    return _vm.enroll(_vm.courseDetail.uid)
                                  }
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-cart-plus faBu",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "buttonText" }, [
                                  _vm._v("Add to Cart")
                                ])
                              ]
                            )
                          : _c(
                              "div",
                              {
                                staticClass: "newButton addedButton",
                                attrs: { id: "addCart" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-cart-plus faBu",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" "),
                                _c("span", { staticClass: "buttonText" }, [
                                  _vm._v("Purchased")
                                ])
                              ]
                            )
                      ]),
                  _vm._v(" "),
                  _vm.added
                    ? _c(
                        "div",
                        {
                          staticClass: "newButton addedButton",
                          attrs: { id: "addWishlist" }
                        },
                        [
                          _c("i", {
                            staticClass: "fa fa-heart faBu",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "buttonText" }, [
                            _vm._v("Added to Wishlist")
                          ])
                        ]
                      )
                    : _c(
                        "div",
                        {
                          staticClass: "newButton",
                          attrs: { id: "addWishlist" },
                          on: { click: _vm.addToWishlist }
                        },
                        [
                          _c("i", {
                            staticClass: "fa fa-heart faBu",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "buttonText" }, [
                            _vm._v("Add to Wishlist")
                          ])
                        ]
                      ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "newButton",
                      class: { view: _vm.isClicked },
                      attrs: { id: "share" },
                      on: { click: _vm.click }
                    },
                    [
                      _c("i", {
                        staticClass: "fa fa-share faBu",
                        attrs: { "aria-hidden": "true" }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "buttonText" }, [
                        _vm._v("Share")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "newButton",
                      class: { view: !_vm.isClicked },
                      attrs: { id: "share" },
                      on: { click: _vm.click }
                    },
                    [
                      _c("i", {
                        staticClass: "fa fa-times faBu",
                        attrs: { "aria-hidden": "true" }
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", [
                  _c(
                    "div",
                    {},
                    [
                      _c("social-sharing", {
                        attrs: {
                          url: _vm.url,
                          title: _vm.courseDetail.title,
                          description: _vm.courseDetail.overview,
                          quote: _vm.courseDetail.overview,
                          hashtags: "bizguruh,business"
                        },
                        inlineTemplate: {
                          render: function() {
                            var _vm = this
                            var _h = _vm.$createElement
                            var _c = _vm._self._c || _h
                            return _c(
                              "div",
                              {
                                staticClass: "social",
                                class: { view: !_vm.isClicked }
                              },
                              [
                                _c(
                                  "network",
                                  { attrs: { network: "facebook" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-facebook fb"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "twitter" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-twitter tweet"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "googleplus" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-google-plus google"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "linkedin" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-linkedin linkedin"
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "network",
                                  { attrs: { network: "whatsapp" } },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-whatsapp whatsapp"
                                    })
                                  ]
                                )
                              ],
                              1
                            )
                          },
                          staticRenderFns: []
                        }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("small", [_vm._v("30-Day Money-Back Guarantee")]),
                _vm._v(" "),
                _c("div", [
                  _c("p", { staticClass: "includes_header" }, [
                    _vm._v("This course includes")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _vm.courseDetail.duration !== ""
                      ? _c("li", [
                          _vm._v(
                            "Duration: " +
                              _vm._s(_vm.time(_vm.TotalvideoDuration))
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        "Modules: " + _vm._s(_vm.modulesCount) + " module(s)"
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v("Level: " + _vm._s(_vm.courseDetail.level))
                    ]),
                    _vm._v(" "),
                    _vm.courseDetail.fullOnline
                      ? _c("li", [_vm._v("Full Online")])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.courseDetail.certificate
                      ? _c("li", [_vm._v("Certificate of Completion")])
                      : _vm._e()
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mt-3" }, [
                  _c(
                    "i",
                    {
                      staticClass: "fas fa-thumbs-up mr-4 liked",
                      attrs: { "aria-hidden": "true" },
                      on: {
                        click: function($event) {
                          return _vm.like(
                            _vm.user.id,
                            _vm.id,
                            _vm.vendorDetail.id,
                            "like"
                          )
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.myLikes))]
                  ),
                  _vm._v(" "),
                  _c(
                    "i",
                    {
                      staticClass: "fas fa-thumbs-down  liked  ",
                      on: {
                        click: function($event) {
                          return _vm.unlike(
                            _vm.user.id,
                            _vm.id,
                            _vm.vendorDetail.id,
                            "unlike"
                          )
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.myUnLikes))]
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "excerptRealTab" },
                [
                  _c("div", { staticClass: "bread my-2" }, [
                    _c(
                      "ul",
                      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
                        return _c(
                          "li",
                          {
                            key: index,
                            class: { linked: !!breadcrumb.link },
                            on: {
                              click: function($event) {
                                return _vm.routeTo(index)
                              }
                            }
                          },
                          [_vm._v(_vm._s(breadcrumb.name))]
                        )
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "whatLearns" }, [
                    _c("div", { staticClass: "what-you-get__title" }, [
                      _vm._v("What you'll learn")
                    ]),
                    _vm._v(" "),
                    _c(
                      "ul",
                      { staticClass: "ui-course" },
                      _vm._l(_vm.courseDetail.whatYouWillLearnHere, function(
                        course,
                        index
                      ) {
                        return _vm.courseDetail.whatYouWillLearnHere.length > 0
                          ? _c("li", { key: index }, [
                              _c("span", { staticClass: "fa fa-check" }),
                              _vm._v(
                                "\n              " +
                                  _vm._s(course.whatYouWillLearn) +
                                  "\n            "
                              )
                            ])
                          : _vm._e()
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "course_content" }, [
                    _vm._m(3),
                    _vm._v(" "),
                    _c("div", { staticClass: "rightC" }, [
                      _vm.courseDetail.modules
                        ? _c("div", { staticClass: "cc" }, [
                            _vm._v(
                              _vm._s(_vm.courseDetail.modules.length) +
                                " module(s)"
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "cc" }, [
                        _vm._v(_vm._s(_vm.time(_vm.TotalvideoDuration)))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.courseModule, function(course, index) {
                    return _vm.courseModule.length > 0
                      ? _c("div", { key: course.id, staticClass: "titleC" }, [
                          _c(
                            "a",
                            { staticClass: "whatLearnx section-container" },
                            [
                              _c(
                                "div",
                                { staticClass: "section-header-left" },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "section-title-wrapper" },
                                    [
                                      _c(
                                        "span",
                                        { staticClass: "section-title-text" },
                                        [
                                          _vm._v(
                                            "\n                  " +
                                              _vm._s(course.title) +
                                              "\n                "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass: "section-title-toggle",
                                          on: {
                                            click: function($event) {
                                              return _vm.courseHide(
                                                index,
                                                "modules"
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "span",
                                            {
                                              staticClass:
                                                "section-title-toggle-plus"
                                            },
                                            [_vm._v(_vm._s(_vm.plus))]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          (index === _vm.indexCheck
                          ? true
                          : false)
                            ? _c(
                                "a",
                                {
                                  staticClass: "whatLearnxx section-container"
                                },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "section-header-left" },
                                    [
                                      _c(
                                        "span",
                                        {
                                          staticClass: "section-title-wrapper"
                                        },
                                        [
                                          _c(
                                            "table",
                                            { staticClass: "table" },
                                            [
                                              _c("tbody", [
                                                course.fileVideoName !==
                                                "Upload Video"
                                                  ? _c("tr", [
                                                      _c("td", {
                                                        attrs: { scope: "row" }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        {
                                                          staticClass:
                                                            "section-title-text"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              course.fileVideoName
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        { staticClass: "file" },
                                                        [_vm._v("Video")]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        { staticClass: "file" },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.time(
                                                                course.vidTime
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ])
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                course.fileAudioName !==
                                                "Upload Audio"
                                                  ? _c("tr", [
                                                      _c("td", {
                                                        attrs: { scope: "row" }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        {
                                                          staticClass:
                                                            "section-title-text"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              course.fileAudioName
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        { staticClass: "file" },
                                                        [_vm._v("Audio")]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        { staticClass: "file" },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.time(
                                                                course.audTime
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ])
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                course.fileName !==
                                                "Upload Course"
                                                  ? _c("tr", [
                                                      _c("td", {
                                                        attrs: { scope: "row" }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        {
                                                          staticClass:
                                                            "section-title-text"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              course.fileName
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "td",
                                                        { staticClass: "file" },
                                                        [_vm._v("Pdf")]
                                                      ),
                                                      _vm._v(" "),
                                                      _vm._m(4, true)
                                                    ])
                                                  : _vm._e()
                                              ])
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]
                              )
                            : _vm._e()
                        ])
                      : _vm._e()
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "whatL" }, [
                    _c("h3", { staticClass: "mb-2" }, [
                      _vm._v("Requirement(s)")
                    ]),
                    _vm._v(" "),
                    _c("ul", [
                      _c("li", {
                        domProps: {
                          innerHTML: _vm._s(_vm.courseDetail.courseModules)
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "whatL mb-4" }, [
                    _c("h3", { staticClass: "mb-2" }, [
                      _vm._v("Course Description")
                    ]),
                    _vm._v(" "),
                    _c("div", {
                      domProps: {
                        innerHTML: _vm._s(_vm.courseDetail.courseObjectives)
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "titleC" }, [
                    _c(
                      "div",
                      [
                        _vm._m(5),
                        _vm._v(" "),
                        _vm._l(_vm.courseDetail.faq, function(course, index) {
                          return _vm.courseDetail.faq.length > 0
                            ? _c("div", { staticClass: "titleC" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "whatLearnx section-container"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "section-header-left" },
                                      [
                                        _c(
                                          "span",
                                          {
                                            staticClass: "section-title-wrapper"
                                          },
                                          [
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "section-title-text"
                                              },
                                              [
                                                _vm._v(
                                                  "\n                      " +
                                                    _vm._s(course.question) +
                                                    "\n                    "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "section-title-toggle"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "section-title-toggle-plus",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.courseHide(
                                                          index,
                                                          "faq"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v(_vm._s(_vm.faq_plus))]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                (index === _vm.faqCheck
                                ? true
                                : false)
                                  ? _c(
                                      "a",
                                      {
                                        staticClass:
                                          "whatLearnxx section-container"
                                      },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass: "section-header-left"
                                          },
                                          [
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "section-title-wrapper"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "section-title-text fw"
                                                  },
                                                  [
                                                    _c("ul", [
                                                      _c("li", [
                                                        _vm._v(
                                                          _vm._s(course.answer)
                                                        )
                                                      ])
                                                    ])
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              ])
                            : _vm._e()
                        })
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _vm.vendorDetail.firstName !== null
                    ? _c("div", { staticClass: "row instructor" }, [
                        _c(
                          "h3",
                          {
                            staticClass: "mb-2",
                            staticStyle: { width: "100%" }
                          },
                          [_vm._v("About The Instructor")]
                        ),
                        _vm._v(" "),
                        _vm.vendorDetail.firstName !== null
                          ? _c("p", { staticClass: "instructor_name mb-2" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.vendorDetail.firstName +
                                    " " +
                                    _vm.vendorDetail.lastName
                                )
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _c("div", { staticClass: "info mb-2" }, [
                          _c(
                            "div",
                            { staticClass: "instructor_container mb-3" },
                            [
                              _c("img", {
                                attrs: { src: _vm.vendor.valid_id, alt: "" }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "instructor_stat" }, [
                            _c("p", { staticClass: "mb-3" }, [
                              _c("i", { staticClass: "fas fa-star" }),
                              _vm._v(" "),
                              _c("strong", [
                                _vm._v(_vm._s(_vm.courseDetail.rating))
                              ]),
                              _vm._v(" Instructor Rating\n              ")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "mb-3" }, [
                              _c("i", { staticClass: "fas fa-comment" }),
                              _vm._v(" "),
                              _c("strong", [
                                _vm._v(_vm._s(_vm.allReviews.length))
                              ]),
                              _vm._v(" Reviews\n              ")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "mb-3" }, [
                              _c("i", { staticClass: "fas fa-users" }),
                              _vm._v(" "),
                              _c("strong", [
                                _vm._v(_vm._s(_vm.courseDetail.broughtByStu))
                              ]),
                              _vm._v(" Students\n              ")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "mb-3" }, [
                              _c("i", { staticClass: "fas fa-play-circle" }),
                              _vm._v(" "),
                              _c("strong", [
                                _vm._v(_vm._s(_vm.vendorCourses.length))
                              ]),
                              _vm._v(" Courses\n              ")
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "text" }, [
                          _vm.vendor.storeName !== " "
                            ? _c("div", { staticClass: "writtenBy" }, [
                                _c("p", [
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.vendor.storeName))
                                  ])
                                ])
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("strong", [_vm._v(_vm._s(_vm.vendor.occupation))]),
                          _vm._v(" "),
                          _c(
                            "p",
                            {
                              staticClass: "toggleOff",
                              class: { toggleOn: _vm.isShown }
                            },
                            [_vm._v(_vm._s(_vm.vendor.bio))]
                          ),
                          _vm._v(" "),
                          _c(
                            "p",
                            {
                              staticClass: "seeMore",
                              on: { click: _vm.seeMore }
                            },
                            [
                              _vm.isShown
                                ? _c("span", [
                                    _c("i", { staticClass: "fas fa-minus" }),
                                    _vm._v(" see less\n              ")
                                  ])
                                : _c("span", [
                                    _c("i", { staticClass: "fas fa-plus" }),
                                    _vm._v(" see more\n              ")
                                  ])
                            ]
                          )
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.allReviews.length > 0
                    ? _c("div", { staticClass: "whatLearns" }, [
                        _c("p", { staticClass: "titleWhat mb-3" }, [
                          _vm._v("Featured review")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "courseOutline" },
                          _vm._l(_vm.allReviews, function(allReview) {
                            return _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-5 col-xs-12" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-md-2 col-xs-2 reviewAvatar"
                                  },
                                  [
                                    _c("div", { staticClass: "rev" }, [
                                      _vm._v(
                                        "\n                    " +
                                          _vm._s(
                                            allReview.user.name.charAt(0)
                                          ) +
                                          "\n                  "
                                      )
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-md-10 col-xs-10" },
                                  [
                                    _c("div", { staticClass: "reviewSize" }, [
                                      _vm._v(
                                        "\n                    " +
                                          _vm._s(
                                            _vm._f("moment")(
                                              allReview.created_at.date,
                                              "from"
                                            )
                                          ) +
                                          "\n                  "
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "reviews" }, [
                                      _vm._v(
                                        "\n                    " +
                                          _vm._s(allReview.user.name) +
                                          "\n                    " +
                                          _vm._s(allReview.user.lastName) +
                                          "\n                  "
                                      )
                                    ])
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-xs-2 lob" }),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-7 col-xs-10" }, [
                                _c(
                                  "div",
                                  { staticClass: "col-md-12 reviewww" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "ratingSt" },
                                      [
                                        _vm._l(allReview.rating, function(
                                          star
                                        ) {
                                          return _c(
                                            "div",
                                            { staticClass: "rateStyle" },
                                            [
                                              _c("i", {
                                                staticClass: "fa fa-star"
                                              })
                                            ]
                                          )
                                        }),
                                        _vm._v(" "),
                                        _vm._l(5 - allReview.rating, function(
                                          star
                                        ) {
                                          return _c(
                                            "div",
                                            { staticClass: "rateStyle" },
                                            [
                                              _c("i", {
                                                staticClass: "fa fa-star-o"
                                              })
                                            ]
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-md-12 reviews" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass: "reviewSizes",
                                        style: {
                                          textOverflow: _vm.read,
                                          overflow: _vm.over
                                        }
                                      },
                                      [_vm._v(_vm._s(allReview.description))]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "small",
                                      {
                                        staticClass: "read",
                                        on: { click: _vm.readMore }
                                      },
                                      [_vm._v("Read more")]
                                    )
                                  ]
                                )
                              ])
                            ])
                          }),
                          0
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "whatLearns" }, [
                    _c("div", { staticClass: "review-div" }, [
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.review.description,
                              expression: "review.description"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { rows: "5", placeholder: "Enter Reviews" },
                          domProps: { value: _vm.review.description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.review,
                                "description",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("i", {
                          class: {
                            fa: _vm.rateFa,
                            "fa-star-o": _vm.NotratedOne,
                            "fa-star": _vm.RatedOne
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateWithStar("1")
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("i", {
                          class: {
                            fa: _vm.rateFa,
                            "fa-star-o": _vm.NotratedTwo,
                            "fa-star": _vm.RatedTwo
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateWithStar("2")
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("i", {
                          class: {
                            fa: _vm.rateFa,
                            "fa-star-o": _vm.NotratedThree,
                            "fa-star": _vm.RatedThree
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateWithStar("3")
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("i", {
                          class: {
                            fa: _vm.rateFa,
                            "fa-star-o": _vm.NotratedFour,
                            "fa-star": _vm.RatedFour
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateWithStar("4")
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("i", {
                          class: {
                            fa: _vm.rateFa,
                            "fa-star-o": _vm.NotratedFive,
                            "fa-star": _vm.RatedFive
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateWithStar("5")
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("i", { staticClass: "ml-lg-5" }, [
                          _vm._v(
                            "\n                " +
                              _vm._s(_vm.review.rating) +
                              "\n                " +
                              _vm._s(_vm.review.rating > 1 ? "stars" : "star") +
                              "\n              "
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass:
                            "elevated_btn elevated_btn_sm text-white btn-compliment mb-2",
                          on: {
                            click: function($event) {
                              return _vm.submitReview()
                            }
                          }
                        },
                        [_vm._v("Submit")]
                      )
                    ])
                  ])
                ],
                2
              )
            ])
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "enrolDiv" }, [
      _c("button", { staticClass: "btn btn-enroll" }, [_vm._v("Enroll")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "leftC" }, [
      _c("h3", [_vm._v("Course Content")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", { staticClass: "file" }, [
      _c("i", { staticClass: "fas fa-marker" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "faqs mb-3" }, [_c("b", [_vm._v("FAQS")])])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-92ca8a16", module.exports)
  }
}

/***/ }),

/***/ 650:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1912)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1914)
/* template */
var __vue_template__ = __webpack_require__(1915)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-37dd416d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/ilcuser/classroom.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-37dd416d", Component.options)
  } else {
    hotAPI.reload("data-v-37dd416d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 651:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1916)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1918)
/* template */
var __vue_template__ = __webpack_require__(1919)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-92ca8a16"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/ilcuser/ilcCourse.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-92ca8a16", Component.options)
  } else {
    hotAPI.reload("data-v-92ca8a16", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * vue-social-sharing v2.4.7 
 * (c) 2019 nicolasbeauvais
 * Released under the MIT License.
 */


function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Vue = _interopDefault(__webpack_require__(40));

var SocialSharingNetwork = {
  functional: true,

  props: {
    network: {
      type: String,
      default: ''
    }
  },

  render: function (createElement, context) {
    var network = context.parent._data.baseNetworks[context.props.network];

    if (!network) {
      return console.warn(("Network " + (context.props.network) + " does not exist"));
    }

    return createElement(context.parent.networkTag, {
      staticClass: context.data.staticClass || null,
      staticStyle: context.data.staticStyle || null,
      class: context.data.class || null,
      style: context.data.style || null,
      attrs: {
        id: context.data.attrs.id || null,
        tabindex: context.data.attrs.tabindex || 0,
        'data-link': network.type === 'popup'
          ? '#share-' + context.props.network
          : context.parent.createSharingUrl(context.props.network),
        'data-action': network.type === 'popup' ? null : network.action
      },
      on: {
        click: network.type === 'popup' ? function () {
          context.parent.share(context.props.network);
        } : function () {
          context.parent.touch(context.props.network);
        }
      }
    }, context.children);
  }
};

var email = {"sharer":"mailto:?subject=@title&body=@url%0D%0A%0D%0A@description","type":"direct"};
var facebook = {"sharer":"https://www.facebook.com/sharer/sharer.php?u=@url&title=@title&description=@description&quote=@quote&hashtag=@hashtags","type":"popup"};
var googleplus = {"sharer":"https://plus.google.com/share?url=@url","type":"popup"};
var line = {"sharer":"http://line.me/R/msg/text/?@description%0D%0A@url","type":"popup"};
var linkedin = {"sharer":"https://www.linkedin.com/shareArticle?mini=true&url=@url&title=@title&summary=@description","type":"popup"};
var odnoklassniki = {"sharer":"https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=@url&st.comments=@description","type":"popup"};
var pinterest = {"sharer":"https://pinterest.com/pin/create/button/?url=@url&media=@media&description=@title","type":"popup"};
var reddit = {"sharer":"https://www.reddit.com/submit?url=@url&title=@title","type":"popup"};
var skype = {"sharer":"https://web.skype.com/share?url=@description%0D%0A@url","type":"popup"};
var telegram = {"sharer":"https://t.me/share/url?url=@url&text=@description","type":"popup"};
var twitter = {"sharer":"https://twitter.com/intent/tweet?text=@title&url=@url&hashtags=@hashtags@twitteruser","type":"popup"};
var viber = {"sharer":"viber://forward?text=@url @description","type":"direct"};
var vk = {"sharer":"https://vk.com/share.php?url=@url&title=@title&description=@description&image=@media&noparse=true","type":"popup"};
var weibo = {"sharer":"http://service.weibo.com/share/share.php?url=@url&title=@title","type":"popup"};
var whatsapp = {"sharer":"https://api.whatsapp.com/send?text=@description%0D%0A@url","type":"popup","action":"share/whatsapp/share"};
var sms = {"sharer":"sms:?body=@url%20@description","type":"direct"};
var sms_ios = {"sharer":"sms:;body=@url%20@description","type":"direct"};
var BaseNetworks = {
	email: email,
	facebook: facebook,
	googleplus: googleplus,
	line: line,
	linkedin: linkedin,
	odnoklassniki: odnoklassniki,
	pinterest: pinterest,
	reddit: reddit,
	skype: skype,
	telegram: telegram,
	twitter: twitter,
	viber: viber,
	vk: vk,
	weibo: weibo,
	whatsapp: whatsapp,
	sms: sms,
	sms_ios: sms_ios
};

var inBrowser = typeof window !== 'undefined';
var $window = inBrowser ? window : null;

var SocialSharing = {
  props: {
    /**
     * URL to share.
     * @var string
     */
    url: {
      type: String,
      default: inBrowser ? window.location.href : ''
    },

    /**
     * Sharing title, if available by network.
     * @var string
     */
    title: {
      type: String,
      default: ''
    },

    /**
     * Sharing description, if available by network.
     * @var string
     */
    description: {
      type: String,
      default: ''
    },

    /**
     * Facebook quote
     * @var string
     */
    quote: {
      type: String,
      default: ''
    },

    /**
     * Twitter hashtags
     * @var string
     */
    hashtags: {
      type: String,
      default: ''
    },

    /**
     * Twitter user.
     * @var string
     */
    twitterUser: {
      type: String,
      default: ''
    },

    /**
     * Flag that indicates if counts should be retrieved.
     * - NOT WORKING IN CURRENT VERSION
     * @var mixed
     */
    withCounts: {
      type: [String, Boolean],
      default: false
    },

    /**
     * Google plus key.
     * @var string
     */
    googleKey: {
      type: String,
      default: undefined
    },

    /**
     * Pinterest Media URL.
     * Specifies the image/media to be used.
     */
    media: {
      type: String,
      default: ''
    },

    /**
     * Network sub component tag.
     * Default to span tag
     */
    networkTag: {
      type: String,
      default: 'span'
    },

    /**
     * Additional or overridden networks.
     * Default to BaseNetworks
     */
    networks: {
      type: Object,
      default: function () {
        return {};
      }
    }
  },

  data: function data () {
    return {
      /**
       * Available sharing networks.
       * @param object
       */
      baseNetworks: BaseNetworks,

      /**
       * Popup settings.
       * @param object
       */
      popup: {
        status: false,
        resizable: true,
        toolbar: false,
        menubar: false,
        scrollbars: false,
        location: false,
        directories: false,
        width: 626,
        height: 436,
        top: 0,
        left: 0,
        window: undefined,
        interval: null
      }
    };
  },

  methods: {
    /**
     * Returns generated sharer url.
     *
     * @param network Social network key.
     */
    createSharingUrl: function createSharingUrl (network) {
      var ua = navigator.userAgent.toLowerCase();

      /**
       * On IOS, SMS sharing link need a special formating
       * Source: https://weblog.west-wind.com/posts/2013/Oct/09/Prefilling-an-SMS-on-Mobile-Devices-with-the-sms-Uri-Scheme#Body-only
        */
      if (network === 'sms' && (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1)) {
        network += '_ios';
      }

      var url = this.baseNetworks[network].sharer;

      /**
       * On IOS, Twitter sharing shouldn't include a hashtag parameter if the hashtag value is empty
       * Source: https://github.com/nicolasbeauvais/vue-social-sharing/issues/143
        */
      if (network === 'twitter' && this.hashtags.length === 0) {
        url = url.replace('&hashtags=@hashtags', '');
      }

      return url
        .replace(/@url/g, encodeURIComponent(this.url))
        .replace(/@title/g, encodeURIComponent(this.title))
        .replace(/@description/g, encodeURIComponent(this.description))
        .replace(/@quote/g, encodeURIComponent(this.quote))
        .replace(/@hashtags/g, this.generateHashtags(network, this.hashtags))
        .replace(/@media/g, this.media)
        .replace(/@twitteruser/g, this.twitterUser ? '&via=' + this.twitterUser : '');
    },
    /**
     * Encode hashtags for the specified social network.
     *
     * @param  network Social network key
     * @param  hashtags All hashtags specified
     */
    generateHashtags: function generateHashtags (network, hashtags) {
      if (network === 'facebook' && hashtags.length > 0) {
        return '%23' + hashtags.split(',')[0];
      }

      return hashtags;
    },
    /**
     * Shares URL in specified network.
     *
     * @param network Social network key.
     */
    share: function share (network) {
      this.openSharer(network, this.createSharingUrl(network));

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Touches network and emits click event.
     *
     * @param network Social network key.
     */
    touch: function touch (network) {
      window.open(this.createSharingUrl(network), '_self');

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Opens sharer popup.
     *
     * @param network Social network key
     * @param url Url to share.
     */
    openSharer: function openSharer (network, url) {
      var this$1 = this;

      // If a popup window already exist it will be replaced, trigger a close event.
      var popupWindow = null;
      if (popupWindow && this.popup.interval) {
        clearInterval(this.popup.interval);

        popupWindow.close();// Force close (for Facebook)

        this.$root.$emit('social_shares_change', network, this.url);
        this.$emit('change', network, this.url);
      }

      popupWindow = window.open(
        url,
        'sharer',
        'status=' + (this.popup.status ? 'yes' : 'no') +
        ',height=' + this.popup.height +
        ',width=' + this.popup.width +
        ',resizable=' + (this.popup.resizable ? 'yes' : 'no') +
        ',left=' + this.popup.left +
        ',top=' + this.popup.top +
        ',screenX=' + this.popup.left +
        ',screenY=' + this.popup.top +
        ',toolbar=' + (this.popup.toolbar ? 'yes' : 'no') +
        ',menubar=' + (this.popup.menubar ? 'yes' : 'no') +
        ',scrollbars=' + (this.popup.scrollbars ? 'yes' : 'no') +
        ',location=' + (this.popup.location ? 'yes' : 'no') +
        ',directories=' + (this.popup.directories ? 'yes' : 'no')
      );

      popupWindow.focus();

      // Create an interval to detect popup closing event
      this.popup.interval = setInterval(function () {
        if (!popupWindow || popupWindow.closed) {
          clearInterval(this$1.popup.interval);

          popupWindow = undefined;

          this$1.$root.$emit('social_shares_close', network, this$1.url);
          this$1.$emit('close', network, this$1.url);
        }
      }, 500);
    }
  },

  /**
   * Merge base networks list with user's list
   */
  beforeMount: function beforeMount () {
    this.baseNetworks = Vue.util.extend(this.baseNetworks, this.networks);
  },

  /**
   * Sets popup default dimensions.
   */
  mounted: function mounted () {
    if (!inBrowser) {
      return;
    }

    /**
     * Center the popup on dual screens
     * http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen/32261263
     */
    var dualScreenLeft = $window.screenLeft !== undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop !== undefined ? $window.screenTop : screen.top;

    var width = $window.innerWidth ? $window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width);
    var height = $window.innerHeight ? $window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height);

    this.popup.left = ((width / 2) - (this.popup.width / 2)) + dualScreenLeft;
    this.popup.top = ((height / 2) - (this.popup.height / 2)) + dualScreenTop;
  },

  /**
   * Set component aliases for buttons and links.
   */
  components: {
    'network': SocialSharingNetwork
  }
};

SocialSharing.version = '2.4.7';

SocialSharing.install = function (Vue) {
  Vue.component('social-sharing', SocialSharing);
};

if (typeof window !== 'undefined') {
  window.SocialSharing = SocialSharing;
}

module.exports = SocialSharing;

/***/ }),

/***/ 869:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(879)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(881)
/* template */
var __vue_template__ = __webpack_require__(882)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e3b0de2c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/ilcuser/navigation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e3b0de2c", Component.options)
  } else {
    hotAPI.reload("data-v-e3b0de2c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 879:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(880);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2b3928ef", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e3b0de2c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e3b0de2c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 880:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.navigation[data-v-e3b0de2c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 15px 20px;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.06);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.06);\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 15.5px;\n\n  position: relative;\n}\n.mobile_bar[data-v-e3b0de2c] {\n  display: none;\n}\n.nav[data-v-e3b0de2c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.nav li a[data-v-e3b0de2c] {\n  padding: 10px 20px;\n  color: #777777;\n  font-weight: bold;\n}\n.nav li:hover a[data-v-e3b0de2c] {\n  color: #333333;\n  background: white;\n}\n.logo[data-v-e3b0de2c] {\n  font-size: 32px;\n}\n@media (max-width: 425px) {\n.mobile_bar[data-v-e3b0de2c] {\n    display: block;\n}\n.navigation[data-v-e3b0de2c] {\n    background: white;\n    z-index: 2;\n}\n.nav[data-v-e3b0de2c] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    position: absolute;\n    right: 0;\n    padding: 10px;\n    top: 40px;\n    z-index: 5;\n    background: white;\n}\n.nav li a[data-v-e3b0de2c] {\n    padding: 10px 30px;\n}\n.logo[data-v-e3b0de2c]{\n      font-size: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: true,
      name: "name"
    };
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      this.name = user.name;
    }
    if (window.innerWidth <= 425) {
      this.show = false;
    }
  },

  methods: {
    switchNav: function switchNav() {
      this.show = !this.show;
    },
    logout: function logout() {
      this.$emit('logout');
    }
  }
});

/***/ }),

/***/ 882:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "navigation shadow-lg" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "mobile_bar" }, [
      _c("i", {
        staticClass: "fa fa-bars",
        attrs: { "aria-hidden": "true" },
        on: { click: _vm.switchNav }
      })
    ]),
    _vm._v(" "),
    _vm.show
      ? _c("ul", { staticClass: "nav" }, [
          _c(
            "li",
            [
              _c("router-link", { attrs: { to: "/classroom" } }, [
                _vm._v("Classroom")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "router-link",
                { attrs: { to: "profile/products/subscription" } },
                [_vm._v("Bizlibrary")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c("router-link", { attrs: { to: "#" } }, [
                _c("i", {
                  staticClass: "fas fa-user-circle",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v("\n        " + _vm._s(_vm.name) + "\n      ")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("li", { on: { click: _vm.logout } }, [_vm._v("Logout")])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
      _c("span", { staticClass: "guruh" }, [_vm._v("Guruh")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e3b0de2c", module.exports)
  }
}

/***/ })

});