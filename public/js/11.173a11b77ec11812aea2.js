webpackJsonp([11,19,178],{

/***/ 1796:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__landing_navigation__ = __webpack_require__(983);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__landing_navigation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__landing_navigation__);
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      auth: false,
      message: false,
      accountSub: 0
    };
  },

  components: {
    Nav: __WEBPACK_IMPORTED_MODULE_0__landing_navigation___default.a
  },
  mounted: function mounted() {
    this.checkMessage();
    this.getAccountSub();
  },

  watch: {
    $route: "checkMessage"
  },

  methods: {
    getAccountSub: function getAccountSub() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("accountUser"));
      if (user !== null) {
        axios.get("/api/account-sub/" + user.id).then(function (res) {
          if (res.data.status !== 'failed') {
            _this.accountSub = res.data;
          }
        });
      }
    },
    checkMessage: function checkMessage() {
      if (this.$route.query.session == "pricing") {
        console.log("hello");

        this.message = true;
        window.scrollTo(0, window.innerHeight * 0.9);
      }
    }
  }
});

/***/ }),

/***/ 1797:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Nav"),
      _vm._v(" "),
      _c("router-view", {
        attrs: { message: _vm.message, accountSub: _vm.accountSub }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c353fdfa", module.exports)
  }
}

/***/ }),

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(875)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(877)
/* template */
var __vue_template__ = __webpack_require__(878)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules/bp-vuejs-dropdown/Dropdown.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42ca018e", Component.options)
  } else {
    hotAPI.reload("data-v-42ca018e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(924)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(926)
/* template */
var __vue_template__ = __webpack_require__(938)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-689ae71c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userHomePageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-689ae71c", Component.options)
  } else {
    hotAPI.reload("data-v-689ae71c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 632:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1796)
/* template */
var __vue_template__ = __webpack_require__(1797)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c353fdfa", Component.options)
  } else {
    hotAPI.reload("data-v-c353fdfa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 751:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(752)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(754)
/* template */
var __vue_template__ = __webpack_require__(755)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-81c0cb64"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/chatComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-81c0cb64", Component.options)
  } else {
    hotAPI.reload("data-v-81c0cb64", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 752:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(753);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("425ab458", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-81c0cb64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chatComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-81c0cb64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chatComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 753:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-81c0cb64] {\n  height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: #f7f8fa;\n}\n.mini[data-v-81c0cb64] {\n  height: auto;\n  background-color: #f7f8fa;\n  width: 350px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-shadow: rgba(0, 0, 0, 0.1) 2px 4px 15px;\n          box-shadow: rgba(0, 0, 0, 0.1) 2px 4px 15px;\n  border-radius: 5px;\n  /* padding: 15px; */\n}\n.chat-messages[data-v-81c0cb64] {\n  width: 100%;\n}\n.chat-users[data-v-81c0cb64] {\n  width: 30%;\n}\n.isTyping[data-v-81c0cb64] {\n  font-size: 12px;\n}\n", ""]);

// exports


/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["left"],
  data: function data() {
    return {
      messages: [],
      user: JSON.parse(localStorage.getItem("authUser")),
      users: [],
      activeUser: false,
      typingTimer: false,
      large: false,
      height: 350
    };
  },
  created: function created() {
    var _this = this;

    if (this.$route.query.name == "large") {
      this.large = true;
      this.height = 800;
    }
    this.fetchMessages();

    Echo.join("chat").here(function (user) {
      _this.users = user;
    }).joining(function (user) {
      _this.users.push(user);

      if (user.vendor_user_id != 0) {
        if (_this.$route.path === "/chat") {
          _this.$toasted.info(user.name + " is now available for chat");
        }
      } else {
        if (_this.$route.path === "/chat") {
          _this.$toasted.info(user.name + " joined");
        }
      }
    }).leaving(function (user) {
      _this.users = _this.users.filter(function (u) {
        return u.id != user.id;
      });
      if (_this.$route.path === "/chat") {}
    }).listen("MessageSent", function (e) {
      _this.messages.push({
        message: e.message.message,
        created_at: e.message.created_at,
        user: e.user
      });
    }).listenForWhisper("typing", function (user) {
      _this.activeUser = user;
      if (_this.typingTimer) {
        clearTimeout(_this.typingTimer);
      }
      _this.typingTimer = setTimeout(function () {
        _this.activeUser = false;
      }, 1000);
    });
  },


  methods: {
    switchChat: function switchChat() {
      this.$emit("switch");
    },
    sendTypingEvent: function sendTypingEvent() {
      Echo.join("chat").whisper("typing", this.user);
    },
    fetchMessages: function fetchMessages() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/messages", {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this2.messages = response.data;
        });
      }
    },
    addMessage: function addMessage(message) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        this.messages.push(message);

        axios.post("/api/message", message, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {});
      }
    }
  }
});

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _c(
      "div",
      { staticClass: "mini shadow rounded", class: { "w-100": _vm.large } },
      [
        _c("chat-users", {
          staticClass: "d-none",
          attrs: { users: _vm.users }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "chat-messages" },
          [
            _c("chat-messages", {
              attrs: {
                messages: _vm.messages,
                activeUser: _vm.activeUser,
                height: _vm.height,
                left: _vm.left
              },
              on: { switch: _vm.switchChat }
            }),
            _vm._v(" "),
            _c("chat-form", {
              attrs: { user: _vm.user },
              on: {
                messagesent: _vm.addMessage,
                sendTypingEvent: _vm.sendTypingEvent
              }
            })
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-81c0cb64", module.exports)
  }
}

/***/ }),

/***/ 869:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(879)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(881)
/* template */
var __vue_template__ = __webpack_require__(882)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e3b0de2c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/ilcuser/navigation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e3b0de2c", Component.options)
  } else {
    hotAPI.reload("data-v-e3b0de2c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 875:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(876);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("bcb0d66c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../css-loader/index.js!../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42ca018e\",\"scoped\":false,\"hasInlineConfig\":true}!../vue-loader/lib/selector.js?type=styles&index=0!./Dropdown.vue", function() {
     var newContent = require("!!../css-loader/index.js!../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42ca018e\",\"scoped\":false,\"hasInlineConfig\":true}!../vue-loader/lib/selector.js?type=styles&index=0!./Dropdown.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 876:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bp-dropdown--sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__btn,\n.bp-dropdown--sub .bp-dropdown__sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__icon {\n    margin-left: auto;\n}\n.bp-dropdown__btn {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 3px 5px;\n    border: 1px solid #efefef;\n    cursor: pointer;\n    -webkit-transition: background-color .1s ease;\n    transition: background-color .1s ease;\n}\n.bp-dropdown__sub {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bp-dropdown__btn--active {\n    background-color: #eee;\n}\n.bp-dropdown__icon {\n    display: inline-block;\n    width: 15px;\n    height: 15px;\n    overflow: visible;\n    -webkit-transition: -webkit-transform .1s ease;\n    transition: -webkit-transform .1s ease;\n    transition: transform .1s ease;\n    transition: transform .1s ease, -webkit-transform .1s ease;\n}\n.bp-dropdown__icon--spin {\n    width: 12px;\n    height: 12px;\n    -webkit-animation: spin 2s infinite linear;\n            animation: spin 2s infinite linear;\n}\n.bp-dropdown__icon--top {\n    -webkit-transform: rotate(-180deg);\n            transform: rotate(-180deg);\n}\n.bp-dropdown__icon--right {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n}\n.bp-dropdown__icon--bottom {\n    -webkit-transform: rotate(0);\n            transform: rotate(0);\n}\n.bp-dropdown__icon--left {\n    -webkit-transform: rotate(-270deg);\n            transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--top,\n.bp-dropdown__sub--active .bp-dropdown__icon--top {\n    -webkit-transform: rotate(0);\n            transform: rotate(0);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--right,\n.bp-dropdown__sub--active .bp-dropdown__icon--right {\n    -webkit-transform: rotate(-270deg);\n            transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--bottom,\n.bp-dropdown__sub--active .bp-dropdown__icon--bottom {\n    -webkit-transform: rotate(-180deg);\n            transform: rotate(-180deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--left,\n.bp-dropdown__sub--active .bp-dropdown__icon--left {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n}\n.bp-dropdown__body {\n    position: fixed;\n    top: 0;\n    left: 0;\n    padding: 6px 8px;\n    background-color: #fff;\n    -webkit-box-shadow: 0 5px 15px -5px rgba(0, 0, 0, .5);\n            box-shadow: 0 5px 15px -5px rgba(0, 0, 0, .5);\n    z-index: 9999;\n}\n.fade-enter-active, .fade-leave-active {\n    -webkit-transition: opacity .1s;\n    transition: opacity .1s;\n}\n.fade-enter, .fade-leave-to {\n    opacity: 0;\n}\n@-webkit-keyframes spin {\n0% {\n        -webkit-transform:rotate(0);\n                transform:rotate(0)\n}\n100% {\n        -webkit-transform:rotate(360deg);\n                transform:rotate(360deg)\n}\n}\n@keyframes spin {\n0% {\n        -webkit-transform:rotate(0);\n                transform:rotate(0)\n}\n100% {\n        -webkit-transform:rotate(360deg);\n                transform:rotate(360deg)\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'bp-vuejs-dropdown',

    props: {
        role: {
            type: String,
            required: false,
            default: ''
        },

        unscroll: {
            type: [HTMLElement, String],
            required: false,
            default: null
        },

        align: {
            type: String,
            required: false,
            default: 'bottom'
        },

        x: {
            type: Number,
            required: false,
            default: 0
        },

        y: {
            type: Number,
            required: false,
            default: 0
        },

        beforeOpen: {
            type: Function,
            required: false,
            default: function _default(resolve) {
                return resolve();
            }
        },

        trigger: {
            type: String,
            required: false,
            default: 'click'
        },

        closeOnClick: {
            type: Boolean,
            required: false,
            default: false
        },

        isIcon: {
            type: Boolean,
            required: false,
            default: true
        },

        className: {
            type: String,
            required: false,
            default: ''
        }
    },

    data: function data() {
        return {
            isHidden: true,
            isLoading: false,

            id: null,
            timeout: null,

            top: undefined,
            right: undefined,
            bottom: undefined,
            left: undefined,
            width: undefined
        };
    },


    watch: {
        isHidden: function isHidden(_isHidden) {
            if (this.unscroll) {
                var el = this.unscroll instanceof HTMLElement ? this.unscroll : document.querySelector(this.unscroll);

                if (el) {
                    el.style.overflow = !_isHidden ? 'hidden' : '';
                }
            }
        }
    },

    created: function created() {
        var _this = this;

        var $root = this.$root;

        // --- hide dropdown if other dropdowns show
        // --- or document clicked
        $root.$on('bp-dropdown:open', function () {
            return _this.isHidden = true;
        });
        $root.$on('bp-dropdown:hide', function () {
            return _this.isHidden = true;
        });

        // --- hide dropdown on document click event
        if (this.trigger === 'click' && !$root['is-bp-dropdown']) {
            Object.defineProperty($root, 'is-bp-dropdown', {
                enumerable: false,
                configurable: false,
                writable: false,
                value: true
            });

            document.onmousedown = function (e) {
                var target = e.target;
                var dropdown = target.closest('.bp-dropdown__btn') || target.closest('.bp-dropdown__body');

                if (!dropdown) {
                    $root.$emit('bp-dropdown:hide');
                }
            };
        }

        this.id = 'bp-dropdown-' + this.generateRandomId();
    },


    methods: {
        // --- generate random id for query selector
        generateRandomId: function generateRandomId() {
            return Math.random().toString(36).substr(2, 10);
        },
        _onToggle: function _onToggle(e) {
            if (this.trigger !== 'click') {
                return;
            }

            this.checkCustomCallback(e);
        },
        _onBtnEnter: function _onBtnEnter(e) {
            if (this.trigger !== 'hover' || !this.isHidden) {
                return;
            }

            this.checkCustomCallback(e);
        },
        _onBtnLeave: function _onBtnLeave(e) {
            var _this2 = this;

            if (this.trigger !== 'hover') {
                return;
            }

            if (this.role) {
                this.timeout = setTimeout(function () {
                    return _this2.isHidden = true;
                }, 100);
            }

            var to = e.toElement;
            if (!to) {
                return;
            }

            var isDropdown = to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__body');
            if (isDropdown) {
                return;
            }

            this.prepare();
        },
        _onBodyClick: function _onBodyClick() {
            if (this.closeOnClick) {
                this.isHidden = true;
            }
        },
        _onBodyEnter: function _onBodyEnter() {
            if (this.timeout) {
                clearTimeout(this.timeout);
            }
        },
        _onBodyLeave: function _onBodyLeave(e) {
            if (this.trigger !== 'hover') {
                return;
            }

            var to = e.toElement;
            if (!to) {
                return;
            }

            if (to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__sub')) {
                return;
            }

            this.prepare();
        },
        checkCustomCallback: function checkCustomCallback(e) {
            var _this3 = this;

            if (!this.isHidden) {
                this.prepare();
                return;
            }

            // --- custom callback before open
            var promise = new Promise(function (resolve) {
                _this3.isLoading = true;
                _this3.beforeOpen.call(_this3, resolve);
            });

            promise.then(function () {
                _this3.isLoading = false;
                if (!e.target.closest('.bp-dropdown__body')) {
                    // --- hide dropdown if other dropdowns show
                    _this3.$root.$emit('bp-dropdown:open');
                }

                setTimeout(_this3.prepare, 0);
            });

            promise.catch(function () {
                throw Error('bp-dropdown promise error');
            });
        },
        prepare: function prepare() {
            var _this4 = this;

            this.isHidden = !this.isHidden;
            if (!this.isHidden) {
                this.$nextTick(function () {
                    var button = _this4.$el.firstElementChild;
                    var container = document.getElementById(_this4.id);

                    _this4.setWidth(button.offsetWidth);
                    _this4.setPosition(button, container);
                });
            }
        },
        setWidth: function setWidth(width) {
            this.width = width;
        },
        setPosition: function setPosition(btn, body) {
            if (!btn || !body) {
                return;
            }

            var coords = this.getCoords(btn);

            // --- current position
            var currentTop = coords.top;
            var currentLeft = coords.left;

            // --- btn size
            var btnWidth = btn.offsetWidth;
            var btnHeight = btn.offsetHeight;

            // --- body size
            var bodyWidth = body.offsetWidth;
            var bodyHeight = body.offsetHeight;

            switch (this.align) {
                case 'top':
                    this.top = currentTop + pageYOffset - bodyHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
                case 'right':
                    this.top = currentTop + pageYOffset;
                    this.left = currentLeft + pageXOffset + btnWidth;
                    break;
                case 'bottom':
                    this.top = currentTop + pageYOffset + btnHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
                case 'left':
                    this.top = currentTop + pageYOffset;
                    this.left = currentLeft + pageXOffset - bodyWidth;
                    break;
                default:
                    this.top = currentTop + pageYOffset + btnHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
            }

            this.top += this.y;
            this.left += this.x;
        },
        getCoords: function getCoords(el) {
            el = el.getBoundingClientRect();
            return {
                top: el.top - pageYOffset,
                left: el.left - pageXOffset
            };
        }
    }
});

/***/ }),

/***/ 878:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _obj, _obj$1, _obj$2
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "bp-dropdown",
      class: { className: _vm.className, "bp-dropdown--sub": _vm.role }
    },
    [
      _c(
        "span",
        {
          class:
            ((_obj = {}),
            (_obj["bp-dropdown__" + (_vm.role ? "sub" : "btn")] = true),
            (_obj[
              "bp-dropdown__" + (_vm.role ? "sub" : "btn") + "--active"
            ] = !_vm.isHidden),
            (_obj[_vm.className + "-bp__btn"] = _vm.className),
            (_obj[_vm.className + "-bp__btn--active"] = !_vm.isHidden),
            _obj),
          on: {
            click: _vm._onToggle,
            mouseenter: _vm._onBtnEnter,
            mouseleave: _vm._onBtnLeave
          }
        },
        [
          _vm._t("btn"),
          _vm._v(" "),
          _vm.isIcon
            ? _vm._t("icon", [
                _vm.isLoading
                  ? _c(
                      "svg",
                      {
                        staticClass:
                          "bp-dropdown__icon bp-dropdown__icon--spin",
                        attrs: { viewBox: "0 0 512 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"
                          }
                        })
                      ]
                    )
                  : _c(
                      "svg",
                      {
                        staticClass: "bp-dropdown__icon",
                        class:
                          ((_obj$1 = {}),
                          (_obj$1["bp-dropdown__icon--" + _vm.align] =
                            _vm.align),
                          _obj$1),
                        attrs: { viewBox: "0 0 256 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"
                          }
                        })
                      ]
                    )
              ])
            : _vm._e()
        ],
        2
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        !_vm.isHidden
          ? _c(
              "div",
              {
                staticClass: "bp-dropdown__body",
                class:
                  ((_obj$2 = {}),
                  (_obj$2[_vm.className + "-bp__body"] = _vm.className),
                  _obj$2),
                style: {
                  minWidth: _vm.width + "px",
                  top: _vm.top + "px",
                  left: _vm.left + "px"
                },
                attrs: { id: _vm.id },
                on: {
                  click: _vm._onBodyClick,
                  mouseenter: _vm._onBodyEnter,
                  mouseleave: _vm._onBodyLeave
                }
              },
              [_vm._t("body")],
              2
            )
          : _vm._e()
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42ca018e", module.exports)
  }
}

/***/ }),

/***/ 879:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(880);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2b3928ef", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e3b0de2c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e3b0de2c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 880:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.navigation[data-v-e3b0de2c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 15px 20px;\n  -webkit-box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.06);\n          box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.06);\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 15.5px;\n\n  position: relative;\n}\n.mobile_bar[data-v-e3b0de2c] {\n  display: none;\n}\n.nav[data-v-e3b0de2c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.nav li a[data-v-e3b0de2c] {\n  padding: 10px 20px;\n  color: #777777;\n  font-weight: bold;\n}\n.nav li:hover a[data-v-e3b0de2c] {\n  color: #333333;\n  background: white;\n}\n.logo[data-v-e3b0de2c] {\n  font-size: 32px;\n}\n@media (max-width: 425px) {\n.mobile_bar[data-v-e3b0de2c] {\n    display: block;\n}\n.navigation[data-v-e3b0de2c] {\n    background: white;\n    z-index: 2;\n}\n.nav[data-v-e3b0de2c] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    position: absolute;\n    right: 0;\n    padding: 10px;\n    top: 40px;\n    z-index: 5;\n    background: white;\n}\n.nav li a[data-v-e3b0de2c] {\n    padding: 10px 30px;\n}\n.logo[data-v-e3b0de2c]{\n      font-size: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: true,
      name: "name"
    };
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      this.name = user.name;
    }
    if (window.innerWidth <= 425) {
      this.show = false;
    }
  },

  methods: {
    switchNav: function switchNav() {
      this.show = !this.show;
    },
    logout: function logout() {
      this.$emit('logout');
    }
  }
});

/***/ }),

/***/ 882:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "navigation shadow-lg" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "mobile_bar" }, [
      _c("i", {
        staticClass: "fa fa-bars",
        attrs: { "aria-hidden": "true" },
        on: { click: _vm.switchNav }
      })
    ]),
    _vm._v(" "),
    _vm.show
      ? _c("ul", { staticClass: "nav" }, [
          _c(
            "li",
            [
              _c("router-link", { attrs: { to: "/classroom" } }, [
                _vm._v("Classroom")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "router-link",
                { attrs: { to: "profile/products/subscription" } },
                [_vm._v("Bizlibrary")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c("router-link", { attrs: { to: "#" } }, [
                _c("i", {
                  staticClass: "fas fa-user-circle",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v("\n        " + _vm._s(_vm.name) + "\n      ")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("li", { on: { click: _vm.logout } }, [_vm._v("Logout")])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
      _c("span", { staticClass: "guruh" }, [_vm._v("Guruh")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e3b0de2c", module.exports)
  }
}

/***/ }),

/***/ 924:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(925);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2704f106", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689ae71c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userHomePageComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689ae71c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userHomePageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 925:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.group_list[data-v-689ae71c] {\n  border-radius: 5px;\n  height: 350px;\n  overflow: scroll;\n  min-width: 250px;\n}\n.form-group[data-v-689ae71c]{\n  position:relative;\n   border-bottom: 1px solid rgb(204, 204, 204, 0.5);\n}\n.search[data-v-689ae71c]{\n  position:absolute;\n  top: 50%;\n  font-size: 14px;\n  margin-top: -7px;\n  right:15px;\n  color: rgba(0, 0, 0, 0.5);\n}\nul[data-v-689ae71c],\nol[data-v-689ae71c] {\n  list-style: none;\n}\nli[data-v-689ae71c] {\n  font-size: 14px;\n}\n.group_list ul li[data-v-689ae71c] {\n  border-bottom: 1px solid rgb(204, 204, 204, 0.5);\n}\n.group_list ul li[data-v-689ae71c]:hover {\n  background: rgb(204, 204, 204, 0.5);\n  cursor: pointer;\n}\nheader[data-v-689ae71c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\nheader p[data-v-689ae71c] {\n  font-size: 14.5px;\n  text-transform: capitalize;\n}\n.chat-box[data-v-689ae71c] {\n  font-size: 14px;\n  border-radius: 3px;\n}\n.chat-box .icon[data-v-689ae71c] {\n}\n.chat-box .text[data-v-689ae71c] {\n}\n.chatbox[data-v-689ae71c] {\n  position: fixed;\n  bottom: 50px;\n  left: 30px;\n  z-index: 9;\n  background-color: #f7f8fa;\n}\n.chatIcon[data-v-689ae71c] {\n  font-size: 14px;\n  margin-top: -4px;\n}\n.mobile[data-v-689ae71c] {\n  display: none;\n}\n.up[data-v-689ae71c] {\n  -webkit-transform: rotate(-90deg);\n          transform: rotate(-90deg);\n  margin: 0;\n}\n.sad[data-v-689ae71c]{\n  opacity: .4;\n  width:80px;\n}\n.chat[data-v-689ae71c] {\n  position: relative;\n}\n.notify[data-v-689ae71c] {\n  position: absolute;\n  top: -10px;\n  right: -10px;\n  z-index: 5;\n}\n.scrollUp[data-v-689ae71c] {\n  display: -webkit-box !important;\n  display: -ms-flexbox !important;\n  display: flex !important;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: fixed;\n  padding: 8px;\n  background-color: rgba(0, 0, 0, 0.74);\n  opacity: 0.5;\n  right: 30px;\n  z-index: 99;\n  bottom: 30px;\n  color: white;\n  font-size: 14px;\n}\n.scrollUp[data-v-689ae71c]:hover {\n  opacity: 1;\n}\n.fa-1x[data-v-689ae71c] {\n  font-size: 0.7em;\n}\n.foot[data-v-689ae71c] {\n  /* position: absolute;\n  bottom: 0;\n  height: 70px;\n  padding-top: 70px; */\n  width: 100%;\n}\n.fa-2x[data-v-689ae71c] {\n  font-size: 1.45em;\n}\n@media (max-width: 425px) {\n.chatbox[data-v-689ae71c] {\n    bottom: 40px;\n    left: 10px;\n}\n.scrollUp[data-v-689ae71c] {\n    font-size: 12px;\n    right: 15px;\n}\n.desktop[data-v-689ae71c] {\n    display: none;\n}\n.mobile[data-v-689ae71c] {\n    display: block;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 926:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_chatComponent_vue__ = __webpack_require__(751);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_chatComponent_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_chatComponent_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userHeaderComponent__ = __webpack_require__(927);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_userHeaderComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_userPopupComponent__ = __webpack_require__(933);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_userPopupComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_userPopupComponent__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-home-page-component",
  data: function data() {
    var _ref;

    return _ref = {
      showChat: false,
      usersub: 0,
      cartNo: "",
      cartId: "",
      sendPaymentValue: [],
      authUs: false,
      searchMydata: "",
      showIcon: true,
      showNotify: 0,
      left: 45,
      scrollPos: 0,
      currentHeight: 0,
      scrollUpButton: false
    }, _defineProperty(_ref, "scrollPos", 0), _defineProperty(_ref, "currentHeight", 0), _defineProperty(_ref, "swing", false), _defineProperty(_ref, "badge", 0), _defineProperty(_ref, "realOrder", []), _defineProperty(_ref, "newRealOrder", []), _defineProperty(_ref, "currentSub", null), _ref;
  },
  watch: {
    $route: "update",
    scrollPos: "scrolling"
  },

  created: function created() {
    var _this = this;

    Echo.join("chat").listen("MessageSent", function (e) {
      _this.showNotify++;
      localStorage.setItem("notify", _this.showNotify);
    });
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user === null) {
      this.showIcon = false;
    } else {
      this.getUserSub();
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    this.getCourse();
    window.addEventListener("scroll", function (e) {
      _this2.scrollPos = window.scrollY;
      _this2.currentHeight = window.innerHeight;
    });
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      window.addEventListener("scroll", function (e) {
        _this2.scrollPos = window.scrollY;
        _this2.currentHeight = window.innerHeight;
      });
    }
    var note = localStorage.getItem("notify");
    if (note !== null) {
      this.showNotify = note;
    }
    this.chatLeft();

    var sessionCart = JSON.parse(localStorage.getItem("userCart"));
    if (sessionCart !== null) {
      this.cartNo = sessionCart.length;
    }
    this.getCart();
    if (user != null) {
      this.authUs = true;

      axios.get("/api/clearOrder", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {});
    }
  },

  methods: {
    goTo: function goTo(id) {
      this.$router.push({
        path: "/group-chat/" + id
      });
    },
    getCourse: function getCourse() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user != null) {
        this.token = user.access_token;
        var newOrder = [];
        axios.get("/api/user/orders", {
          headers: { Authorization: "Bearer " + user.access_token }
        }).then(function (response) {
          _this3.items = response.data.data.filter(function (item) {
            return item.referenceNo !== null;
          });
          _this3.items.forEach(function (item, index) {
            item.orderDetail.forEach(function (items, indexs) {
              var product = {};
              product.title = items.title;
              product.id = items.productId;
              _this3.realOrder.push(product);
            });
          });
          var jsonObject = _this3.realOrder.map(JSON.stringify);
          var uniqueSet = new Set(jsonObject);
          _this3.newRealOrder = Array.from(uniqueSet).map(JSON.parse);
        });
      }
    },
    plusCart: function plusCart() {
      this.badge++;
    },
    removeCart: function removeCart() {
      this.badge--;
    },
    getCart: function getCart() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/no-cart/" + user.id, {
          headers: { Authorization: "Bearer " + user.access_token }
        }).then(function (response) {
          if (response.status === 200) {
            _this4.badge = response.data;
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    },
    notify: function notify(type, product) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        type: type,
        product: product
      };
      axios.post("/api/send-notification", data, {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (res) {}).catch(function (err) {});
    },
    scrollToTop: function scrollToTop() {
      window.scrollTo(0, 0);
    },
    scrolling: function scrolling() {
      if (this.scrollPos > window.innerHeight * 0.3) {
        this.scrollUpButton = true;
      } else {
        this.scrollUpButton = false;
      }

      if (this.scrollPos > window.innerHeight * 0.6) {
        this.swing = true;
      }
    },
    chatLeft: function chatLeft() {
      if (this.$route.path == "/chat") {
        this.left = 45;
      } else {
        this.left = 30;
      }
    },
    switchChat: function switchChat() {
      this.showChat = !this.showChat;
      localStorage.removeItem("notify");
      this.showNotify = 0;
    },
    closeChat: function closeChat() {
      this.showChat = false;
    },
    gotoChat: function gotoChat() {
      this.$router.push("/chat");
    },
    getUserSub: function getUserSub() {
      var _this5 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      if (user !== null) {
        axios.get("/api/user-sub/" + user.id).then(function (response) {
          if (response.status == 200) {
            _this5.usersub = Math.round(response.data);
            _this5.currentSub = Math.round(response.data);
            _this5.hideChat();
          }
        });
      }
    },
    hideChat: function hideChat() {
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        // if (this.usersub == 3 || user.vendor_user_id != 0) {
        if (this.$route.path === "/chat" || this.$route.name === "PrivateChat" || this.$route.name === "GroupChat") {
          this.showIcon = false;
        } else {
          this.showIcon = true;
        }
        // } else {
        //   this.showIcon = false;
        // }
      }
    },
    addInteraction: function addInteraction(vendor, type) {
      var data = {};
      if (type === "views") {
        data = {
          vendor_id: vendor,
          likes: 0,
          shares: 0,
          views: 1
        };
      }
      if (type === "shares") {
        data = {
          vendor_id: vendor,
          likes: 0,
          shares: 1,
          views: 0
        };
      }
      if (type === "likes") {
        data = {
          vendor_id: vendor,
          likes: 1,
          shares: 0,
          views: 0
        };
      }

      axios.post("/api/add-interaction", data).then();
    },
    updateActivity: function updateActivity(id, profile, content) {
      var data = {
        vendor_id: id,
        profile_visits: profile,
        content_views: content
      };

      axios.post("/api/add-activity", data).then(function (response) {});
    },
    topContent: function topContent(id, content_id, count) {
      var data = {
        vendor_id: id,
        content_id: content_id,
        count: count
      };

      axios.post("/api/add-top-content", data).then(function (response) {});
    },
    topIndustry: function topIndustry(id, industry, count) {
      var data = {
        vendor_id: id,
        industry: industry,
        count: count
      };

      axios.post("/api/add-top-industry", data).then(function (response) {});
    },
    addGender: function addGender(id, type) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {};
      if (type === "male") {
        data = {
          user_id: user.id,
          vendor_id: id,
          male: 1,
          female: 0
        };
      }
      if (type === "female") {
        data = {
          user_id: user.id,
          vendor_id: id,
          male: 0,
          female: 1
        };
      }
      if (user.gender !== undefined && user.gender !== null) {
        axios.post("/api/add-gender", data).then(function (response) {});
      }
    },
    addLocation: function addLocation(id, locale, count) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        user_id: user.id,
        vendor_id: id,
        locale: locale,
        count: 1
      };
      if (user.location !== undefined && user.location !== null) {
        axios.post("/api/add-location", data).then(function (response) {});
      }
    },
    addAge: function addAge(id, age) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        user_id: user.id,
        vendor_id: id,
        age: age,
        count: 1
      };

      if (user.age !== undefined && user.age !== null) {
        axios.post("/api/add-age", data).then(function (response) {});
      }
    },
    addReach: function addReach(user, product, vendor) {
      var data = {
        user_id: user,
        product_id: product,
        vendor_id: vendor
      };

      axios.post("/api/add-reach", data).then(function (response) {});
    },
    like: function like(user, product, vendor, type) {
      if (type === "like") {
        var data = {
          user_id: user,
          product_id: product,
          vendor_id: vendor,
          like: 1,
          unlike: 0
        };
      }
      if (type === "unlike") {
        var data = {
          user_id: user,
          product_id: product,
          vendor_id: vendor,
          like: 0,
          unlike: 1
        };
      }

      axios.post("/api/add-like", data).then(function (response) {});
    },
    update: function update() {
      this.hideChat();
      this.chatLeft();
      this.closeChat();
      this.getUserSub();
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user != null) {
        this.authUs = true;
      } else {
        this.authUs = false;
      }
    }
  },
  components: {
    "app-header": __WEBPACK_IMPORTED_MODULE_1__components_userHeaderComponent___default.a,
    "app-popup": __WEBPACK_IMPORTED_MODULE_2__components_userPopupComponent___default.a,

    chat: __WEBPACK_IMPORTED_MODULE_0__components_chatComponent_vue___default.a
  }
});

/***/ }),

/***/ 927:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(928)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(930)
/* template */
var __vue_template__ = __webpack_require__(932)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6640caa4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6640caa4", Component.options)
  } else {
    hotAPI.reload("data-v-6640caa4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 928:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(929);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("07bef48e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6640caa4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6640caa4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 929:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.explore-container[data-v-6640caa4] {\n  font-size: 14px;\n  color: #767676;\n  border: 1px solid #d1d1d1;\n  text-align: center;\n  border-radius: 4px;\n  padding: 3px 10px;\n  width: 130px;\n  font-weight: bold;\n}\n.bg-search[data-v-6640caa4] {\n  background: #f7f8fa;\n}\n.explore-container[data-v-6640caa4]:hover {\n  border-color: #767676 !important;\n}\n.closeSearch[data-v-6640caa4] {\n  position: relative;\n  padding: 5px 10px;\n  text-align: right;\n  cursor: pointer;\n  font-size: 18px;\n  color: rgba(118, 118, 118, 0.4);\n}\n.searchContainer[data-v-6640caa4] {\n  position: relative;\n  height: 34px;\n  width: 70%;\n}\n.search[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n  width: 100%;\n  margin-bottom: 5px;\n  height: 35px;\n  border-top-right-radius: 2px;\n  border-bottom-right-radius: 2px;\n}\n.showSearch[data-v-6640caa4] {\n  position: absolute;\n  background: #ffffff;\n  border-radius: 2px;\n  -webkit-box-shadow: 0 4px 16px rgba(20, 23, 28, 0.25);\n          box-shadow: 0 4px 16px rgba(20, 23, 28, 0.25);\n  width: 100%;\n  max-height: 500px;\n  overflow-y: scroll;\n  text-align: left;\n  z-index: 8;\n  padding: 10px;\n}\n.pryHeader[data-v-6640caa4] {\n  color: rgba(0, 0, 0, 0.54) !important;\n  font-size: 14px;\n  padding: 5px;\n}\n.pryHeader[data-v-6640caa4]:hover {\n  background: #fafafa;\n}\n.pryHeader .fa.fa-search[data-v-6640caa4] {\n  color: rgba(0, 0, 0, 0.24) !important;\n}\n.title[data-v-6640caa4] {\n  color: rgba(118, 118, 118, 0.8) !important;\n  font-size: 14px !important;\n  padding: 10px;\n  width: 100%;\n  max-height: 30px;\n  line-height: 1.6 !important;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  margin-bottom: 8px;\n  text-transform: capitalize;\n}\n.title[data-v-6640caa4]:hover {\n  color: hsl(207, 43%, 20%) !important;\n}\n.form-control[data-v-6640caa4] {\n  border-radius: 2px;\n  height: 34px;\n  font-size: 12px;\n}\n[data-v-6640caa4]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.4) !important;\n  opacity: 1; /* Firefox */\n  font-size: 13px;\n}\n[data-v-6640caa4]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.4) !important;\n  opacity: 1; /* Firefox */\n  font-size: 13px;\n}\n[data-v-6640caa4]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.4) !important;\n  opacity: 1; /* Firefox */\n  font-size: 13px;\n}\n[data-v-6640caa4]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.4) !important;\n  opacity: 1; /* Firefox */\n  font-size: 13px;\n}\n[data-v-6640caa4]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.4);\n}\n[data-v-6640caa4]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.4);\n}\n.sea[data-v-6640caa4] {\n  color: rgb(118, 118, 118, 0.6);\n  margin-left: -45px;\n  border: none;\n  border-top-left-radius: 2px;\n  border-bottom-left-radius: 2px;\n  height: 100%;\n}\n.sea[data-v-6640caa4]:hover {\n  background: #a4c2db;\n  border: none;\n}\n.update-profile[data-v-6640caa4] {\n  position: relative;\n  padding: 0 10px;\n}\n.profImage[data-v-6640caa4] {\n  width: 24px;\n  height: 24px;\n}\n.profImage img[data-v-6640caa4] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 50%;\n  margin-left: 10px;\n}\n.fa-2x[data-v-6640caa4] {\n  font-size: 1.5em;\n}\n.update-text[data-v-6640caa4] {\n  position: absolute;\n  background: #f7f8fa;\n  display: none;\n  width: 200px;\n  bottom: -30px;\n  left: -90%;\n  cursor: pointer;\n  padding: 5px;\n  color: rgba(0, 0, 0, 0.74);\n  font-size: 14px;\n}\n.update-profile:hover .update-text[data-v-6640caa4] {\n  display: inline;\n}\n.fa-inverse[data-v-6640caa4] {\n  color: #5b84a7;\n}\nol[data-v-6640caa4],\nul[data-v-6640caa4] {\n  list-style: none;\n}\n.navLink[data-v-6640caa4] {\n  color: #5b84a7;\n}\n.mobile_cart[data-v-6640caa4] {\n  display: none;\n  background: #a4c2db;\n  color: #ffffff;\n  border-radius: 50%;\n  color: #a4c2db;\n  padding-top: 10px;\n  height: 30px;\n  width: 25px;\n  left: auto;\n  right: 10px;\n  position: absolute;\n  top: 10px;\n  z-index: 1001;\n}\n.exploreBar[data-v-6640caa4] {\n  font-size: 12px;\n}\n.fa-cart-plus[data-v-6640caa4] {\n  font-size: 18px;\n  margin-top: -2px;\n}\n.register-nav[data-v-6640caa4] {\n  font-size: 16px;\n  border-bottom: none !important;\n}\n.cartNum[data-v-6640caa4] {\n  position: absolute;\n  top: 5px;\n  left: 15px;\n  min-width: 10px;\n  padding: 3px 5px;\n  font-size: 11px;\n  font-weight: bold;\n  line-height: 1;\n  color: hsl(207, 43%, 20%);\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: baseline;\n  background-color: #f7f8fa;\n  border-radius: 10px;\n}\n.m10[data-v-6640caa4] {\n  font-size: 16px;\n  margin-bottom: 1px;\n}\n.cartLink[data-v-6640caa4] {\n  color: #333 !important;\n  -webkit-transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n  transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n}\n.cartLink[data-v-6640caa4]:hover {\n  color: hsl(207, 43%, 20%) !important;\n}\n.cartIcon div[data-v-6640caa4] {\n  background-color: #f7f8fa;\n  color: hsl(207, 43%, 20%);\n  position: absolute;\n  min-width: 10px;\n  font-size: 11px;\n  top: -8px;\n  left: 16px;\n  padding: 3px 5px;\n  font-weight: bold;\n  line-height: 1;\n  text-align: center;\n  white-space: nowrap;\n  vertical-align: baseline;\n  border-radius: 50%;\n}\n.navDrop[data-v-6640caa4] {\n  padding: 5px 15px;\n  font-size: 14px;\n}\n.navDrop a[data-v-6640caa4] {\n  color: hsl(207, 43%, 20%) !important;\n  padding: 5px 15px;\n  font-weight: normal;\n}\n.navDrop[data-v-6640caa4]:hover {\n  background: #fff;\n}\n.cartIcon[data-v-6640caa4] {\n  color: white;\n  float: left;\n  padding-top: 0;\n  position: relative;\n  width: 20px;\n}\nul .menu li a[data-v-6640caa4] {\n  font-size: 14px;\n}\n.navLinkBar[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  /* color: #5b84a7; */\n  /* padding: 12px 20px 10px 20px; */\n  font-size: 14px;\n  margin: 0 30px;\n}\n.mainExplore[data-v-6640caa4] {\n  cursor: pointer;\n  margin-bottom: 5px;\n  padding: 20px 0;\n  font-weight: normal;\n}\n.explore[data-v-6640caa4]::before {\n  content: \"\";\n  position: absolute;\n  top: -4.95px;\n  right: auto;\n  left: 20px;\n  width: 15px;\n  height: 15px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  -webkit-box-shadow: rgba(82, 95, 127, 0.04) -3px -3px 5px;\n          box-shadow: rgba(82, 95, 127, 0.04) -3px -3px 5px;\n  background: #fafafa;\n  border-radius: 2px;\n  border-top: 1px solid rgba(0, 0, 0, 0.2);\n  border-left: 1px solid rgba(0, 0, 0, 0.2);\n}\n.explore[data-v-6640caa4] {\n  margin-top: 16px;\n  position: absolute;\n  background: #fafafa;\n  font-weight: normal;\n  padding: 20px 1px 40px;\n  -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.5) inset;\n          box-shadow: 0 0 2px rgba(0, 0, 0, 0.5) inset;\n  border-radius: 2px;\n}\n.one[data-v-6640caa4],\n.two[data-v-6640caa4],\n.three[data-v-6640caa4],\n.four[data-v-6640caa4],\n.five[data-v-6640caa4],\n.six[data-v-6640caa4],\n.seven[data-v-6640caa4] {\n  padding: 5px 15px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  cursor: pointer;\n  color: rgba(0, 0, 0, 0.64) !important;\n}\n.one a[data-v-6640caa4],\n.two a[data-v-6640caa4],\n.three a[data-v-6640caa4],\n.four a[data-v-6640caa4],\n.five a[data-v-6640caa4],\n.six a[data-v-6640caa4],\n.seven a[data-v-6640caa4] {\n  color: rgba(0, 0, 0, 0.64) !important;\n}\n.one[data-v-6640caa4]:hover,\n.two[data-v-6640caa4]:hover,\n.three[data-v-6640caa4]:hover,\n.four[data-v-6640caa4]:hover,\n.five[data-v-6640caa4]:hover,\n.six[data-v-6640caa4]:hover,\n.seven[data-v-6640caa4]:hover {\n  background: #ffffff;\n  cursor: pointer;\n}\n.cartIconLogged[data-v-6640caa4]::before {\n  border-left: 1px solid #ccc;\n}\n.navBod[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  border-right: 1px solid #ccc;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.cartIconLogged[data-v-6640caa4] {\n  color: #a4c2db;\n  float: left;\n  padding-top: 0px;\n  position: relative;\n  width: 20px;\n}\n.primary-nav li.nav-tab a[data-v-6640caa4]:hover {\n  color: #333333 !important;\n}\n.profile_name[data-v-6640caa4] {\n  padding: 20px 0 20px 0;\n  text-transform: capitalize;\n}\n.primary-nav[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  color: #a4c2db !important;\n  margin-top: 10px;\n  font-weight: bold;\n  font-size: 14px;\n  margin-left: 30px;\n}\n.primary-nav li.nav-tab a[data-v-6640caa4] {\n  color: #a4c2db !important;\n  margin: 10px;\n  text-transform: capitalize;\n}\n.nameTab[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 4px 20px;\n  /* border: 1px solid #ccc; */\n  border-radius: 100px;\n  background: rgba(164, 194, 219, 0.2);\n}\n.nameTab[data-v-6640caa4]:hover {\n  background: rgba(164, 194, 219, 0.5);\n}\n.insights[data-v-6640caa4],\n.ask_biz[data-v-6640caa4] {\n  display: none;\n}\n.ask_biz[data-v-6640caa4] {\n  font-weight: bold;\n}\n.myProfile[data-v-6640caa4] {\n  font-size: 16px;\n  font-weight: normal;\n  width: 100%;\n  cursor: pointer;\n  padding: 9px 9px 0 9px;\n  color: #a4c2db;\n}\n.myProfile[data-v-6640caa4]:hover {\n  color: #333333;\n}\n.profile-header[data-v-6640caa4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.fa-user-circle[data-v-6640caa4]::before {\n  font-size: 24px;\n}\n.profile-pos[data-v-6640caa4] {\n  position: relative;\n  font-size: 16px;\n}\n.dropMenu-total li[data-v-6640caa4] {\n  padding: 10px 10px 10px;\n}\n.container[data-v-6640caa4] {\n  padding: 0 !important;\n}\n.firstText[data-v-6640caa4] {\n  color: #333333 !important;\n}\n.bp-dropdown__btn[data-v-6640caa4] {\n  background: #19c880;\n  padding: 20px;\n  color: #fff;\n}\n.listItem[data-v-6640caa4] {\n  float: left;\n  cursor: pointer;\n}\n.logout[data-v-6640caa4] {\n  color: #5b84a7 !important;\n  font-size: 16px;\n  cursor: pointer;\n  padding: 0 10px;\n  font-weight: normal;\n}\n.logout[data-v-6640caa4]:hover {\n  color: #373a3c !important;\n  font-size: 16px;\n}\n.cartBadge[data-v-6640caa4] {\n  position: absolute;\n  margin-right: -15px;\n  margin-top: -13px;\n  background-color: #a4c2db;\n}\n.userStyle .header-bar-menu li a[data-v-6640caa4]::after {\n  content: \"\";\n}\n.header-bar-menu[data-v-6640caa4] {\n  width: 100%;\n  padding-left: 30px;\n  padding-top: 5px;\n}\n.v-menu[data-v-6640caa4] {\n  float: left;\n}\n.btn-browse[data-v-6640caa4] {\n  background-color: #a4c2db !important;\n  color: #ffffff;\n  width: 80%;\n}\n.test[data-v-6640caa4] {\n  float: left;\n}\n.browser-all[data-v-6640caa4] {\n  text-align: center;\n}\na[data-v-6640caa4] {\n  text-decoration: none;\n}\n.profile-wrapper[data-v-6640caa4] {\n  position: absolute;\n  -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.25);\n          box-shadow: 0 0 2px rgba(0, 0, 0, 0.25);\n  background: #f7f8fa;\n  padding: 1px;\n  right: -15px;\n  border-radius: 2px;\n  width: 180px;\n  top: 35px;\n  display: none;\n}\n.profile-wrapper[data-v-6640caa4]:hover::after {\n  background: #f7f8fa;\n}\n.profile-wrapper[data-v-6640caa4]::before {\n  content: \"\";\n  position: absolute;\n  top: -5px;\n  right: auto;\n  left: 50%;\n  width: 15px;\n  height: 15px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n\n  background: #f7f8fa;\n  /* border-top:1px solid rgba(0, 0, 0, 0.5);\n     border-left:1px solid rgba(0, 0, 0, 0.5); */\n}\n.profile-pos:hover .profile-wrapper[data-v-6640caa4] {\n  display: block;\n}\n.myProfile[data-v-6640caa4] {\n  float: left;\n}\n#dd[data-v-6640caa4] {\n  float: right;\n}\n.profile[data-v-6640caa4] {\n  border-radius: 3px;\n  overflow: initial;\n}\n.profile[data-v-6640caa4]:hover {\n  cursor: pointer;\n  /* background: #fafafa; */\n}\n.profile .name[data-v-6640caa4] {\n  font-size: 12px;\n  color: #fff;\n  line-height: 26px;\n  margin-left: 10px;\n}\n.profile .name[data-v-6640caa4]:hover {\n  color: #0088cc;\n}\n.profile img[data-v-6640caa4] {\n  display: inline;\n  border: 1px solid #111;\n  border-radius: 3px;\n  -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, 0.5) inset;\n          box-shadow: 0 0 3px rgba(0, 0, 0, 0.5) inset;\n}\n.profileMenu[data-v-6640caa4] {\n  -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.5) inset;\n          box-shadow: 0 0 2px rgba(0, 0, 0, 0.5) inset;\n}\n.upgradeNav[data-v-6640caa4] {\n  padding: 20px 0px;\n  font-size: 14px;\n  color: #767676 !important;\n  cursor: pointer;\n  font-weight: bold;\n  -webkit-transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n  transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n  margin-right: 30px;\n}\n.upgradeNav[data-v-6640caa4]:hover {\n  color: hsl(207, 43%, 20%) !important;\n}\n.bottom[data-v-6640caa4] {\n  color: #767676 !important;\n  font-size: 15px;\n  padding: 15px 3px;\n  cursor: pointer;\n  font-weight: bold;\n  -webkit-transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n  transition: color 0.1s ease-in-out, opacity 0.1s ease-in-out;\n}\n.bottom[data-v-6640caa4]:hover {\n  color: hsl(207, 43%, 20%) !important;\n}\n.miniNav[data-v-6640caa4] {\n  color: white !important;\n  font-size: 15px;\n  padding: 8px 15px;\n  cursor: pointer;\n}\n.miniNav[data-v-6640caa4]:hover {\n  color: #5b84a7 !important;\n}\n/* hide menu */\n.menu.profileMenu[data-v-6640caa4]::before {\n  content: \"\";\n  position: absolute;\n  top: -4.95px;\n  right: auto;\n  left: 20px;\n  width: 15px;\n  height: 15px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  -webkit-box-shadow: rgba(82, 95, 127, 0.04) -3px -3px 5px;\n          box-shadow: rgba(82, 95, 127, 0.04) -3px -3px 5px;\n  background: #ffffff;\n  border-radius: 2px;\n  border-top: 1px solid rgba(0, 0, 0, 0.5);\n  border-left: 1px solid rgba(0, 0, 0, 0.5);\n}\n.menu[data-v-6640caa4] {\n  clear: both;\n  margin: 44px 0 0 10px;\n  padding: 0 5px;\n}\n.menu li[data-v-6640caa4] {\n  font-size: 12px;\n  margin: 0;\n  font-size: 13px;\n  padding: 4px 3px;\n  /* padding: 10px 4px;*/\n}\n.menu li a[data-v-6640caa4] {\n  color: #a4c2db;\n}\n.menu li:hover > a[data-v-6640caa4] {\n  color: #333333;\n}\n.menu li[data-v-6640caa4]:hover {\n  border-radius: 3px;\n}\n\n/* hover profile show menu */\n/* #dd:checked ~ .menu {\n\tdisplay: block;\n} */\n.user_name[data-v-6640caa4] {\n  color: #333333;\n  font-weight: 400;\n  font-size: 14px;\n}\n/* .user_name:hover {\n  font-weight: bold;\n} */\n.mb-2[data-v-6640caa4] {\n  margin-bottom: 14px !important;\n}\n@media (max-width: 1024px) {\n.searchContainer[data-v-6640caa4] {\n    display: none;\n}\n.upgradeNav[data-v-6640caa4] {\n    background: white !important;\n    padding: 8px 10px;\n    font-size: 14px;\n    font-weight: 700;\n    color: #333 !important;\n    cursor: pointer;\n    font-weight: 400;\n    border-radius: 3px;\n    -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.25) inset;\n    box-shadow: inset 0 0 2px rgba(0, 0, 0, 0.25);\n    margin-right: 10px;\n}\n.profImage[data-v-6640caa4] {\n    width: 80px;\n    height: 80px;\n    margin-bottom: 15px;\n}\n.profImage img[data-v-6640caa4] {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    border-radius: 50%;\n}\n.user_name[data-v-6640caa4] {\n    color: #fff;\n    font-size: 18px;\n    font-weight: bold;\n}\n.bm-item-list > *[data-v-6640caa4] {\n    display: block;\n    padding: 0.7em;\n    text-decoration: none;\n}\n.fa-user[data-v-6640caa4] {\n    font-size: 80px;\n}\n.mobColor[data-v-6640caa4] {\n    color: #5b84a7 !important;\n    font-size: 16px;\n}\n.navLinkBar[data-v-6640caa4] {\n    display: none;\n}\n.fa-cart-plus[data-v-6640caa4] {\n    font-size: 18px;\n}\n.navLink[data-v-6640caa4] {\n    margin-right: auto !important;\n    margin-left: auto !important;\n}\n.mobile_cart[data-v-6640caa4] {\n    display: inline;\n}\n.mobile-menu[data-v-6640caa4] {\n    display: block;\n    background-color: #5b84a7 !important;\n}\n.bm-burger-button[data-v-6640caa4] {\n    height: 30px !important;\n    left: 14px !important;\n}\n.bm-burger-button[data-v-6640caa4] {\n    display: block !important;\n    background-color: #fff !important;\n    top: 10px !important;\n}\nspan.bm-burger-bars[data-v-6640caa4] {\n    background-color: #333 !important;\n}\nspan.bm-burger-bar[data-v-6640caa4] {\n    background-color: #333 !important;\n}\n.navItem--right[data-v-6640caa4] {\n    display: none;\n}\n.primary-nav[data-v-6640caa4] {\n    display: none;\n}\n.insights[data-v-6640caa4],\n  .ask_biz[data-v-6640caa4] {\n    display: block;\n    font-weight: bold;\n    padding-bottom: 5px;\n}\nli.nav-tab a[data-v-6640caa4] {\n    color: #5b84a7 !important;\n    margin: 0;\n    text-transform: capitalize;\n    padding-bottom: 5px;\n}\n.myProfile[data-v-6640caa4] {\n    font-size: 16px;\n    padding: 0;\n    font-weight: normal;\n}\nli.mobile_insights.nav-tab a[data-v-6640caa4] {\n    padding-bottom: 5px;\n}\n.profile[data-v-6640caa4] {\n    background: none;\n}\n.logout[data-v-6640caa4] {\n    padding: 10px;\n    position: absolute;\n    bottom: 30px;\n    right: 0;\n    font-size: 18px;\n}\n.bm-menu[data-v-6640caa4] {\n    height: 100vh !important;\n    padding-top: 40px !important;\n    border-right: 1px solid #ccc;\n}\n.fa-user-circle[data-v-6640caa4]::before {\n    font-size: 80px !important;\n}\n}\n@media (max-width: 1024px) {\n.update-profile[data-v-6640caa4] {\n    display: none;\n}\n.logoImg[data-v-6640caa4] {\n    max-width: 120px;\n    height: 30px;\n}\n.update-profile[data-v-6640caa4] {\n    position: absolute;\n    margin-right: 50px;\n    right: 10%;\n}\n.fa-2x[data-v-6640caa4] {\n    font-size: 1.4em;\n}\n.update-text[data-v-6640caa4] {\n    right: 30px;\n    left: unset;\n}\n.login[data-v-6640caa4] {\n    margin-bottom: 40px;\n}\n.fa-inverse[data-v-6640caa4] {\n    color: #fff;\n}\n.logout[data-v-6640caa4] {\n    padding: 10px;\n    position: absolute;\n    bottom: 30px;\n    right: 0;\n    font-size: 17px;\n}\n.bm-menu[data-v-6640caa4] {\n    height: 100vh !important;\n    padding-top: 40px !important;\n}\n.fa-user-circle[data-v-6640caa4]::before {\n    font-size: 80px !important;\n}\n.mobColor[data-v-6640caa4] {\n    color: #fff !important;\n}\nli[data-v-6640caa4] {\n    color: #fff !important;\n}\n.navLinkBar[data-v-6640caa4] {\n    display: none;\n}\n.fa-cart-plus[data-v-6640caa4] {\n    font-size: 18px;\n}\n.site-navbar[data-v-6640caa4] {\n    padding: 5px !important;\n}\n.navLink[data-v-6640caa4] {\n    margin-right: auto !important;\n    margin-left: auto !important;\n}\n.mobile_cart[data-v-6640caa4] {\n    display: inline;\n}\n.mobile-menu[data-v-6640caa4] {\n    display: block;\n    background-color: #5b84a7 !important;\n}\n.bm-burger-button[data-v-6640caa4] {\n    height: 16px !important;\n    left: 14px !important;\n    width: 16px !important;\n}\n.bm-burger-button[data-v-6640caa4] {\n    display: block;\n    background-color: #5b84a7 !important;\n    top: 22px !important;\n}\nspan.bm-burger-bars[data-v-6640caa4] {\n    background-color: #333 !important;\n}\nspan.bm-burger-bar[data-v-6640caa4] {\n    background-color: #333 !important;\n}\n.navItem--right[data-v-6640caa4] {\n    display: none;\n}\n.primary-nav[data-v-6640caa4] {\n    display: none;\n}\n.insights[data-v-6640caa4],\n  .ask_biz[data-v-6640caa4] {\n    display: block;\n    font-weight: bold;\n    padding-bottom: 5px;\n}\nli.nav-tab a[data-v-6640caa4] {\n    color: #fff !important;\n    margin: 0;\n    text-transform: capitalize;\n    padding-bottom: 5px;\n}\n.myProfile[data-v-6640caa4] {\n    font-size: 16px;\n    padding: 0;\n    font-weight: normal;\n}\nli.mobile_insights.nav-tab a[data-v-6640caa4] {\n    padding-bottom: 5px;\n}\nli[data-v-6640caa4] {\n    padding-bottom: 10px;\n}\n.profile[data-v-6640caa4] {\n    background: none;\n}\n.cartNum[data-v-6640caa4] {\n    left: 15px !important;\n}\n.bm-menu[data-v-6640caa4] {\n    background-color: #333 !important;\n    border-right: 1px solid #ccc;\n}\n.bgWhite[data-v-6640caa4] {\n    background-color: #0088cc;\n}\n.logoText[data-v-6640caa4]{\n    font-size:24px;\n}\n}\n@media (max-width: 375px) {\n  /* .mobile_cart {\n\n    margin-left: 180px;\n  } */\n}\n@media (max-width: 320px) {\n  /* .mobile_cart {\n    margin-left: 120px;\n  } */\n}\n", ""]);

// exports


/***/ }),

/***/ 930:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_bp_vuejs_dropdown__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_bp_vuejs_dropdown___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_bp_vuejs_dropdown__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_burger_menu__ = __webpack_require__(931);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_burger_menu___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_burger_menu__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ilcuser_navigation__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ilcuser_navigation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__ilcuser_navigation__);
var _components$props$nam;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




Engagespot.init("cywCppTlinzpmjVHxFcYoiuuKlnefc");
var user = JSON.parse(localStorage.getItem("authUser"));
if (user !== null) {
  if (user.vendor_user_id === 0) {
    Engagespot.identifyUser(user.id);
  } else {
    Engagespot.identifyUser(user.vendor_user_id);
  }
}

/* harmony default export */ __webpack_exports__["default"] = (_components$props$nam = {
  components: { BpVuejsDropdown: __WEBPACK_IMPORTED_MODULE_0_bp_vuejs_dropdown___default.a, Slide: __WEBPACK_IMPORTED_MODULE_1_vue_burger_menu__["Slide"] },
  props: ["cartNo", "authUs", "badge"],
  name: "user-header-component",
  data: function data() {
    var _ref;

    return _ref = {
      dontShowNav: false,
      showNavItems: true,
      sidebarCategories: [],
      dropDownShow: false,
      profile: false,
      hideMenu: true,
      id: "",
      name: "",
      token: "",
      email: "",
      headerDis: false,
      isOpen: false,
      authenticate: false,
      explore: "none",
      exploreHover: false,
      profileHover: false,
      userSub: null,
      show: false,
      showUpdate: false,
      accountShow: false,
      username: "",
      search: ""
    }, _defineProperty(_ref, "show", false), _defineProperty(_ref, "videoProduct", []), _defineProperty(_ref, "researchProduct", []), _defineProperty(_ref, "podcastProduct", []), _defineProperty(_ref, "articleProduct", []), _defineProperty(_ref, "courseProduct", []), _defineProperty(_ref, "allProduct", []), _defineProperty(_ref, "allVendors", []), _defineProperty(_ref, "scrollPos", 0), _defineProperty(_ref, "currentHeight", 0), _defineProperty(_ref, "whiteBg", true), _defineProperty(_ref, "user", {}), _ref;
  }

}, _defineProperty(_components$props$nam, "components", {
  navigation: __WEBPACK_IMPORTED_MODULE_2__ilcuser_navigation___default.a,
  Slide: __WEBPACK_IMPORTED_MODULE_1_vue_burger_menu__["Slide"]
}), _defineProperty(_components$props$nam, "computed", {
  profileImage: function profileImage() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      if (user.logo !== undefined) {
        return user.logo;
      }
    }
  },
  vendors: function vendors() {
    var _this = this;

    return this.allVendors.filter(function (element) {
      return element.storeName.toLowerCase().includes(_this.search.toLowerCase());
    });
  },
  researches: function researches() {
    var _this2 = this;

    return this.researchProduct.filter(function (element) {
      return element.title.toLowerCase().includes(_this2.search.toLowerCase());
    });
  },
  videos: function videos() {
    var _this3 = this;

    return this.videoProduct.filter(function (element) {
      return element.title.toLowerCase().includes(_this3.search.toLowerCase());
    });
  },
  podcasts: function podcasts() {
    var _this4 = this;

    return this.podcastProduct.filter(function (element) {
      return element.title.toLowerCase().includes(_this4.search.toLowerCase());
    });
  },
  articles: function articles() {
    var _this5 = this;

    return this.articleProduct.filter(function (element) {
      return element.title.toLowerCase().includes(_this5.search.toLowerCase());
    });
  },
  courses: function courses() {
    var _this6 = this;

    return this.courseProduct.filter(function (element) {
      return element.title.toLowerCase().includes(_this6.search.toLowerCase());
    });
  },

  //  researchesVendor(){
  //     return this.researchProduct.filter(element=>{
  //          return element.host.toLowerCase().includes(this.search.toLowerCase());
  //                 })
  // },
  videosVendors: function videosVendors() {
    var _this7 = this;

    return this.videoProduct.filter(function (element) {
      return element.host.toLowerCase().includes(_this7.search.toLowerCase());
    });
  },
  podcastsVendors: function podcastsVendors() {
    var _this8 = this;

    return this.podcastProduct.filter(function (element) {
      return element.host.toLowerCase().includes(_this8.search.toLowerCase());
    });
  },
  articlesVendors: function articlesVendors() {
    var _this9 = this;

    return this.articleProduct.filter(function (element) {
      return element.writer.toLowerCase().includes(_this9.search.toLowerCase());
    });
  }
  //  coursesVendors(){
  //     return this.courseProduct.filter(element=>{
  //          return element.host.toLowerCase().includes(this.search.toLowerCase());
  //                 })
  // },

}), _defineProperty(_components$props$nam, "mounted", function mounted() {
  var _this10 = this;

  window.addEventListener("scroll", function (e) {
    _this10.scrollPos = window.scrollY;
    _this10.currentHeight = window.innerHeight;
  });

  axios.get("/api/products").then(function (response) {
    response.data.data.forEach(function (item) {
      if (item.prodType == "Market Research") {
        _this10.researchProduct.push(item.marketResearch);
      } else if (item.prodType == "Videos") {
        _this10.videoProduct.push(item.webinar);
      } else if (item.prodType == "Podcast") {
        _this10.podcastProduct.push(item.webinar);
      } else if (item.prodType == "Articles") {
        _this10.articleProduct.push(item.articles);
      } else if (item.prodType == "Courses") {
        _this10.courseProduct.push(item.courses);
      } else {
        return;
      }
    });
  });
  this.updateNav();
  if (this.$route.path == "/account") {
    this.accountShow = true;
  }
  this.checkStatus();
  this.checkUpdate();
  var customerUser = JSON.parse(localStorage.getItem("authUser"));
  if (customerUser != null) {
    this.id = customerUser.id;
    this.token = customerUser.access_token;
    this.name = customerUser.name;
    this.email = customerUser.email;
    if (customerUser.vendor_username) {
      this.username = customerUser.vendor_username;
    }

    this.profile = true;
    this.headerDis = this.authUs;
    this.authenticate = true;
  } else {
    this.profile = false;
  }
}), _defineProperty(_components$props$nam, "watch", {
  $route: "updateNav",
  scrollPos: "scrolling"
}), _defineProperty(_components$props$nam, "methods", {
  hideNavBar: function hideNavBar() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    this.user = user;
    if (user.school) {
      this.dontShowNav = true;
    } else {
      this.dontShowNav = false;
    }
  },
  scrolling: function scrolling() {
    if (this.$route.path === "/") {
      if (this.scrollPos > window.innerHeight * 0.12) {
        this.whiteBg = true;
      } else {
        this.whiteBg = false;
      }
    } else {
      this.whiteBg = true;
    }
  },
  searchNow: function searchNow() {
    if (this.search !== "") {
      this.$router.push({
        name: "HomeSearch",
        params: { name: this.search }
      });
    } else {
      this.$toasted.error("search cannot be empty");
    }
  },
  clearSearch: function clearSearch() {
    this.search = "";
  },
  checkUpdate: function checkUpdate() {
    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      if (user.gender === undefined || user.age === undefined || user.location === undefined) {
        this.showUpdate = true;
      }
      if (user.gender && user.age && user.location) {
        this.showUpdate = false;
      }
    }
  },
  updateProfile: function updateProfile() {
    this.$router.push("/update-profile");
  },
  checkStatus: function checkStatus() {
    var _this11 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    this.user = user;
    if (user != null) {
      this.token = user.access_token;

      axios.post("/api/user/subscription-plan/" + user.id).then(function (response) {
        if (response.status === 200) {
          if (response.data.length !== 0) {
            _this11.userSub = Number(response.data[0].level);
          }
        }
      });
    }
  },
  checkLogin: function checkLogin() {
    var _this12 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    var login_token = localStorage.getItem("login_token");
    if (user !== null) {
      axios.get("/api/check-ip/" + user.email + "/" + login_token).then(function (response) {
        if (response.status === 200) {
          if (response.data.status === "logout") {
            _this12.$toasted.error("Your account is logged in somewhere else !");
            _this12.logout();
          }
        }
      });
    }
  },
  delIp: function delIp() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    axios.delete("/api/ip/" + user.email).then(function (response) {});
  },
  notify: function notify() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      axios.get("/api/sendNotify", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {});
    }
  },
  updateNav: function updateNav() {
    this.scrolling();

    if (this.$route.path == "/auth/register" || this.$route.path == "/auth/login" || this.$route.path == "/account" || this.$route.path == "/account-demo") {
      this.dontShowNav = true;
    }
    if (this.$route.path !== "/auth/register" && this.$route.path !== "/auth/login" && this.$route.path !== "/account" && this.$route.path !== "/account-demo") {
      this.dontShowNav = false;
    }

    if (window.innerWidth <= 768 && (this.$route.path == "/account" || this.$route.path == "/account-demo")) {
      this.dontShowNav = false;
    }
    if (this.$route.path !== "/") {
      this.showNavItems = true;
    }
    if (this.$route.path == "/") {
      this.showNavItems = false;
    }
    if (this.$route.path == "/account" || this.$route.path == "/account-demo") {
      this.accountShow = true;
    }
    if (this.$route.path !== "/account" || this.$route.path == "/account-demo") {
      this.accountShow = false;
    }
    var customerUser = JSON.parse(localStorage.getItem("authUser"));

    if (customerUser !== null) {
      this.id = customerUser.id;
      this.token = customerUser.access_token;
      this.name = customerUser.name;
      this.email = customerUser.email;
      this.profile = true;
      this.headerDis = this.authUs;
      this.authenticate = true;

      // this.checkLogin();
      // this.notify();
      this.checkUpdate();
      this.hideNavBar();
    } else {
      this.profile = false;
    }
  },
  openExplore: function openExplore() {
    if (this.explore === "none") {
      this.explore = "block";
    } else {
      this.explore = "none";
    }
  },
  ddd: function ddd() {
    this.hideMenu = false;
  },
  homePage: function homePage() {
    window.location.href = "/";
  },
  toggle: function toggle() {
    this.isOpen = !this.isOpen;
  },
  logout: function logout() {
    localStorage.removeItem("authUser");
    localStorage.removeItem("accountUser");
    localStorage.removeItem("login_token");
    this.profile = false;
    this.headerDis = false;
    this.$router.push("/entrepreneur");
    // Engagespot.clearUser();
    window.location.reload();
  }
}), _components$props$nam);

/***/ }),

/***/ 931:
/***/ (function(module, exports) {

module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "01f9":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("2d00");
var $export = __webpack_require__("5ca1");
var redefine = __webpack_require__("2aba");
var hide = __webpack_require__("32e9");
var Iterators = __webpack_require__("84f2");
var $iterCreate = __webpack_require__("41a0");
var setToStringTag = __webpack_require__("7f20");
var getPrototypeOf = __webpack_require__("38fd");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "097d":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// https://github.com/tc39/proposal-promise-finally

var $export = __webpack_require__("5ca1");
var core = __webpack_require__("8378");
var global = __webpack_require__("7726");
var speciesConstructor = __webpack_require__("ebd6");
var promiseResolve = __webpack_require__("bcaa");

$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
  var C = speciesConstructor(this, core.Promise || global.Promise);
  var isFunction = typeof onFinally == 'function';
  return this.then(
    isFunction ? function (x) {
      return promiseResolve(C, onFinally()).then(function () { return x; });
    } : onFinally,
    isFunction ? function (e) {
      return promiseResolve(C, onFinally()).then(function () { throw e; });
    } : onFinally
  );
} });


/***/ }),

/***/ "0d58":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("ce10");
var enumBugKeys = __webpack_require__("e11e");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "1495":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var anObject = __webpack_require__("cb7c");
var getKeys = __webpack_require__("0d58");

module.exports = __webpack_require__("9e1e") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "1991":
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__("9b43");
var invoke = __webpack_require__("31f4");
var html = __webpack_require__("fab2");
var cel = __webpack_require__("230e");
var global = __webpack_require__("7726");
var process = global.process;
var setTask = global.setImmediate;
var clearTask = global.clearImmediate;
var MessageChannel = global.MessageChannel;
var Dispatch = global.Dispatch;
var counter = 0;
var queue = {};
var ONREADYSTATECHANGE = 'onreadystatechange';
var defer, channel, port;
var run = function () {
  var id = +this;
  // eslint-disable-next-line no-prototype-builtins
  if (queue.hasOwnProperty(id)) {
    var fn = queue[id];
    delete queue[id];
    fn();
  }
};
var listener = function (event) {
  run.call(event.data);
};
// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
if (!setTask || !clearTask) {
  setTask = function setImmediate(fn) {
    var args = [];
    var i = 1;
    while (arguments.length > i) args.push(arguments[i++]);
    queue[++counter] = function () {
      // eslint-disable-next-line no-new-func
      invoke(typeof fn == 'function' ? fn : Function(fn), args);
    };
    defer(counter);
    return counter;
  };
  clearTask = function clearImmediate(id) {
    delete queue[id];
  };
  // Node.js 0.8-
  if (__webpack_require__("2d95")(process) == 'process') {
    defer = function (id) {
      process.nextTick(ctx(run, id, 1));
    };
  // Sphere (JS game engine) Dispatch API
  } else if (Dispatch && Dispatch.now) {
    defer = function (id) {
      Dispatch.now(ctx(run, id, 1));
    };
  // Browsers with MessageChannel, includes WebWorkers
  } else if (MessageChannel) {
    channel = new MessageChannel();
    port = channel.port2;
    channel.port1.onmessage = listener;
    defer = ctx(port.postMessage, port, 1);
  // Browsers with postMessage, skip WebWorkers
  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
    defer = function (id) {
      global.postMessage(id + '', '*');
    };
    global.addEventListener('message', listener, false);
  // IE8-
  } else if (ONREADYSTATECHANGE in cel('script')) {
    defer = function (id) {
      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
        html.removeChild(this);
        run.call(id);
      };
    };
  // Rest old browsers
  } else {
    defer = function (id) {
      setTimeout(ctx(run, id, 1), 0);
    };
  }
}
module.exports = {
  set: setTask,
  clear: clearTask
};


/***/ }),

/***/ "1eb2":
/***/ (function(module, exports, __webpack_require__) {

// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}


/***/ }),

/***/ "1fa8":
/***/ (function(module, exports, __webpack_require__) {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__("cb7c");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "2350":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "23c6":
/***/ (function(module, exports, __webpack_require__) {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__("2d95");
var TAG = __webpack_require__("2b4c")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "27ee":
/***/ (function(module, exports, __webpack_require__) {

var classof = __webpack_require__("23c6");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var Iterators = __webpack_require__("84f2");
module.exports = __webpack_require__("8378").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "2aba":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var has = __webpack_require__("69a8");
var SRC = __webpack_require__("ca5a")('src');
var TO_STRING = 'toString';
var $toString = Function[TO_STRING];
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("8378").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "2aeb":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("cb7c");
var dPs = __webpack_require__("1495");
var enumBugKeys = __webpack_require__("e11e");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("230e")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("fab2").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "2b4c":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("5537")('wks');
var uid = __webpack_require__("ca5a");
var Symbol = __webpack_require__("7726").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "2d00":
/***/ (function(module, exports) {

module.exports = false;


/***/ }),

/***/ "2d95":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "31f4":
/***/ (function(module, exports) {

// fast apply, http://jsperf.lnkit.com/fast-apply/5
module.exports = function (fn, args, that) {
  var un = that === undefined;
  switch (args.length) {
    case 0: return un ? fn()
                      : fn.call(that);
    case 1: return un ? fn(args[0])
                      : fn.call(that, args[0]);
    case 2: return un ? fn(args[0], args[1])
                      : fn.call(that, args[0], args[1]);
    case 3: return un ? fn(args[0], args[1], args[2])
                      : fn.call(that, args[0], args[1], args[2]);
    case 4: return un ? fn(args[0], args[1], args[2], args[3])
                      : fn.call(that, args[0], args[1], args[2], args[3]);
  } return fn.apply(that, args);
};


/***/ }),

/***/ "32e9":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");
module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "33a4":
/***/ (function(module, exports, __webpack_require__) {

// check on default Array iterator
var Iterators = __webpack_require__("84f2");
var ITERATOR = __webpack_require__("2b4c")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "38fd":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__("69a8");
var toObject = __webpack_require__("4bf8");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "41a0":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__("2aeb");
var descriptor = __webpack_require__("4630");
var setToStringTag = __webpack_require__("7f20");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__("32e9")(IteratorPrototype, __webpack_require__("2b4c")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "4588":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "4630":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "499e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return addStylesClient; });
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "4a59":
/***/ (function(module, exports, __webpack_require__) {

var ctx = __webpack_require__("9b43");
var call = __webpack_require__("1fa8");
var isArrayIter = __webpack_require__("33a4");
var anObject = __webpack_require__("cb7c");
var toLength = __webpack_require__("9def");
var getIterFn = __webpack_require__("27ee");
var BREAK = {};
var RETURN = {};
var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
  var f = ctx(fn, that, entries ? 2 : 1);
  var index = 0;
  var length, step, iterator, result;
  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
  // fast case for arrays with default iterator
  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
    if (result === BREAK || result === RETURN) return result;
  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
    result = call(iterator, f, step.value, entries);
    if (result === BREAK || result === RETURN) return result;
  }
};
exports.BREAK = BREAK;
exports.RETURN = RETURN;


/***/ }),

/***/ "4bf8":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "551c":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__("2d00");
var global = __webpack_require__("7726");
var ctx = __webpack_require__("9b43");
var classof = __webpack_require__("23c6");
var $export = __webpack_require__("5ca1");
var isObject = __webpack_require__("d3f4");
var aFunction = __webpack_require__("d8e8");
var anInstance = __webpack_require__("f605");
var forOf = __webpack_require__("4a59");
var speciesConstructor = __webpack_require__("ebd6");
var task = __webpack_require__("1991").set;
var microtask = __webpack_require__("8079")();
var newPromiseCapabilityModule = __webpack_require__("a5b8");
var perform = __webpack_require__("9c80");
var userAgent = __webpack_require__("a25f");
var promiseResolve = __webpack_require__("bcaa");
var PROMISE = 'Promise';
var TypeError = global.TypeError;
var process = global.process;
var versions = process && process.versions;
var v8 = versions && versions.v8 || '';
var $Promise = global[PROMISE];
var isNode = classof(process) == 'process';
var empty = function () { /* empty */ };
var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

var USE_NATIVE = !!function () {
  try {
    // correct subclassing with @@species support
    var promise = $Promise.resolve(1);
    var FakePromise = (promise.constructor = {})[__webpack_require__("2b4c")('species')] = function (exec) {
      exec(empty, empty);
    };
    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
    return (isNode || typeof PromiseRejectionEvent == 'function')
      && promise.then(empty) instanceof FakePromise
      // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
      // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
      // we can't detect it synchronously, so just check versions
      && v8.indexOf('6.6') !== 0
      && userAgent.indexOf('Chrome/66') === -1;
  } catch (e) { /* empty */ }
}();

// helpers
var isThenable = function (it) {
  var then;
  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
};
var notify = function (promise, isReject) {
  if (promise._n) return;
  promise._n = true;
  var chain = promise._c;
  microtask(function () {
    var value = promise._v;
    var ok = promise._s == 1;
    var i = 0;
    var run = function (reaction) {
      var handler = ok ? reaction.ok : reaction.fail;
      var resolve = reaction.resolve;
      var reject = reaction.reject;
      var domain = reaction.domain;
      var result, then, exited;
      try {
        if (handler) {
          if (!ok) {
            if (promise._h == 2) onHandleUnhandled(promise);
            promise._h = 1;
          }
          if (handler === true) result = value;
          else {
            if (domain) domain.enter();
            result = handler(value); // may throw
            if (domain) {
              domain.exit();
              exited = true;
            }
          }
          if (result === reaction.promise) {
            reject(TypeError('Promise-chain cycle'));
          } else if (then = isThenable(result)) {
            then.call(result, resolve, reject);
          } else resolve(result);
        } else reject(value);
      } catch (e) {
        if (domain && !exited) domain.exit();
        reject(e);
      }
    };
    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
    promise._c = [];
    promise._n = false;
    if (isReject && !promise._h) onUnhandled(promise);
  });
};
var onUnhandled = function (promise) {
  task.call(global, function () {
    var value = promise._v;
    var unhandled = isUnhandled(promise);
    var result, handler, console;
    if (unhandled) {
      result = perform(function () {
        if (isNode) {
          process.emit('unhandledRejection', value, promise);
        } else if (handler = global.onunhandledrejection) {
          handler({ promise: promise, reason: value });
        } else if ((console = global.console) && console.error) {
          console.error('Unhandled promise rejection', value);
        }
      });
      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
    } promise._a = undefined;
    if (unhandled && result.e) throw result.v;
  });
};
var isUnhandled = function (promise) {
  return promise._h !== 1 && (promise._a || promise._c).length === 0;
};
var onHandleUnhandled = function (promise) {
  task.call(global, function () {
    var handler;
    if (isNode) {
      process.emit('rejectionHandled', promise);
    } else if (handler = global.onrejectionhandled) {
      handler({ promise: promise, reason: promise._v });
    }
  });
};
var $reject = function (value) {
  var promise = this;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  promise._v = value;
  promise._s = 2;
  if (!promise._a) promise._a = promise._c.slice();
  notify(promise, true);
};
var $resolve = function (value) {
  var promise = this;
  var then;
  if (promise._d) return;
  promise._d = true;
  promise = promise._w || promise; // unwrap
  try {
    if (promise === value) throw TypeError("Promise can't be resolved itself");
    if (then = isThenable(value)) {
      microtask(function () {
        var wrapper = { _w: promise, _d: false }; // wrap
        try {
          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
        } catch (e) {
          $reject.call(wrapper, e);
        }
      });
    } else {
      promise._v = value;
      promise._s = 1;
      notify(promise, false);
    }
  } catch (e) {
    $reject.call({ _w: promise, _d: false }, e); // wrap
  }
};

// constructor polyfill
if (!USE_NATIVE) {
  // 25.4.3.1 Promise(executor)
  $Promise = function Promise(executor) {
    anInstance(this, $Promise, PROMISE, '_h');
    aFunction(executor);
    Internal.call(this);
    try {
      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
    } catch (err) {
      $reject.call(this, err);
    }
  };
  // eslint-disable-next-line no-unused-vars
  Internal = function Promise(executor) {
    this._c = [];             // <- awaiting reactions
    this._a = undefined;      // <- checked in isUnhandled reactions
    this._s = 0;              // <- state
    this._d = false;          // <- done
    this._v = undefined;      // <- value
    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
    this._n = false;          // <- notify
  };
  Internal.prototype = __webpack_require__("dcbc")($Promise.prototype, {
    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
    then: function then(onFulfilled, onRejected) {
      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
      reaction.fail = typeof onRejected == 'function' && onRejected;
      reaction.domain = isNode ? process.domain : undefined;
      this._c.push(reaction);
      if (this._a) this._a.push(reaction);
      if (this._s) notify(this, false);
      return reaction.promise;
    },
    // 25.4.5.1 Promise.prototype.catch(onRejected)
    'catch': function (onRejected) {
      return this.then(undefined, onRejected);
    }
  });
  OwnPromiseCapability = function () {
    var promise = new Internal();
    this.promise = promise;
    this.resolve = ctx($resolve, promise, 1);
    this.reject = ctx($reject, promise, 1);
  };
  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
    return C === $Promise || C === Wrapper
      ? new OwnPromiseCapability(C)
      : newGenericPromiseCapability(C);
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
__webpack_require__("7f20")($Promise, PROMISE);
__webpack_require__("7a56")(PROMISE);
Wrapper = __webpack_require__("8378")[PROMISE];

// statics
$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
  // 25.4.4.5 Promise.reject(r)
  reject: function reject(r) {
    var capability = newPromiseCapability(this);
    var $$reject = capability.reject;
    $$reject(r);
    return capability.promise;
  }
});
$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
  // 25.4.4.6 Promise.resolve(x)
  resolve: function resolve(x) {
    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
  }
});
$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__("5cc5")(function (iter) {
  $Promise.all(iter)['catch'](empty);
})), PROMISE, {
  // 25.4.4.1 Promise.all(iterable)
  all: function all(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var resolve = capability.resolve;
    var reject = capability.reject;
    var result = perform(function () {
      var values = [];
      var index = 0;
      var remaining = 1;
      forOf(iterable, false, function (promise) {
        var $index = index++;
        var alreadyCalled = false;
        values.push(undefined);
        remaining++;
        C.resolve(promise).then(function (value) {
          if (alreadyCalled) return;
          alreadyCalled = true;
          values[$index] = value;
          --remaining || resolve(values);
        }, reject);
      });
      --remaining || resolve(values);
    });
    if (result.e) reject(result.v);
    return capability.promise;
  },
  // 25.4.4.4 Promise.race(iterable)
  race: function race(iterable) {
    var C = this;
    var capability = newPromiseCapability(C);
    var reject = capability.reject;
    var result = perform(function () {
      forOf(iterable, false, function (promise) {
        C.resolve(promise).then(capability.resolve, reject);
      });
    });
    if (result.e) reject(result.v);
    return capability.promise;
  }
});


/***/ }),

/***/ "5537":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("8378");
var global = __webpack_require__("7726");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("2d00") ? 'pure' : 'global',
  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "5ca1":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var core = __webpack_require__("8378");
var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var ctx = __webpack_require__("9b43");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "5cc5":
/***/ (function(module, exports, __webpack_require__) {

var ITERATOR = __webpack_require__("2b4c")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "5e6f":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("6d18");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("55c52291", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "613b":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("5537")('keys');
var uid = __webpack_require__("ca5a");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "626a":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("2d95");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "6821":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("626a");
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "69a8":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "6d18":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\nhtml{height:100%\n}\n.bm-burger-button{cursor:pointer;height:30px;left:36px;position:absolute;top:36px;width:36px\n}\n.bm-burger-button.hidden{display:none\n}\n.bm-burger-bars{background-color:#373a47\n}\n.line-style{height:20%;left:0;position:absolute;right:0\n}\n.cross-style{cursor:pointer;position:absolute;right:2px;top:12px\n}\n.bm-cross{background:#bdc3c7\n}\n.bm-cross-button{height:24px;width:24px\n}\n.bm-cross-button.hidden{display:none\n}\n.bm-menu{background-color:#3f3f41;height:100%;left:0;overflow-x:hidden;padding-top:60px;position:fixed;top:0;transition:.5s;width:0;z-index:1000\n}\n.bm-overlay{background:rgba(0,0,0,.3)\n}\n.bm-item-list{color:#b8b7ad;font-size:20px;margin-left:10%\n}\n.bm-item-list>*{display:flex;padding:.7em;text-decoration:none\n}\n.bm-item-list>*>span{color:#fff;font-weight:700;margin-left:10px\n}", ""]);

// exports


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "77f1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("4588");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7a56":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var dP = __webpack_require__("86cc");
var DESCRIPTORS = __webpack_require__("9e1e");
var SPECIES = __webpack_require__("2b4c")('species');

module.exports = function (KEY) {
  var C = global[KEY];
  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
    configurable: true,
    get: function () { return this; }
  });
};


/***/ }),

/***/ "7f20":
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__("86cc").f;
var has = __webpack_require__("69a8");
var TAG = __webpack_require__("2b4c")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "8079":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var macrotask = __webpack_require__("1991").set;
var Observer = global.MutationObserver || global.WebKitMutationObserver;
var process = global.process;
var Promise = global.Promise;
var isNode = __webpack_require__("2d95")(process) == 'process';

module.exports = function () {
  var head, last, notify;

  var flush = function () {
    var parent, fn;
    if (isNode && (parent = process.domain)) parent.exit();
    while (head) {
      fn = head.fn;
      head = head.next;
      try {
        fn();
      } catch (e) {
        if (head) notify();
        else last = undefined;
        throw e;
      }
    } last = undefined;
    if (parent) parent.enter();
  };

  // Node.js
  if (isNode) {
    notify = function () {
      process.nextTick(flush);
    };
  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
    var toggle = true;
    var node = document.createTextNode('');
    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
    notify = function () {
      node.data = toggle = !toggle;
    };
  // environments with maybe non-completely correct, but existent Promise
  } else if (Promise && Promise.resolve) {
    // Promise.resolve without an argument throws an error in LG WebOS 2
    var promise = Promise.resolve(undefined);
    notify = function () {
      promise.then(flush);
    };
  // for other environments - macrotask based on:
  // - setImmediate
  // - MessageChannel
  // - window.postMessag
  // - onreadystatechange
  // - setTimeout
  } else {
    notify = function () {
      // strange IE + webpack dev server bug - use .call(global)
      macrotask.call(global, flush);
    };
  }

  return function (fn) {
    var task = { fn: fn, next: undefined };
    if (last) last.next = task;
    if (!head) {
      head = task;
      notify();
    } last = task;
  };
};


/***/ }),

/***/ "8378":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.7' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "84f2":
/***/ (function(module, exports) {

module.exports = {};


/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "9b43":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("d8e8");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "9c6c":
/***/ (function(module, exports, __webpack_require__) {

// 22.1.3.31 Array.prototype[@@unscopables]
var UNSCOPABLES = __webpack_require__("2b4c")('unscopables');
var ArrayProto = Array.prototype;
if (ArrayProto[UNSCOPABLES] == undefined) __webpack_require__("32e9")(ArrayProto, UNSCOPABLES, {});
module.exports = function (key) {
  ArrayProto[UNSCOPABLES][key] = true;
};


/***/ }),

/***/ "9c80":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};


/***/ }),

/***/ "9def":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("4588");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "a25f":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var navigator = global.navigator;

module.exports = navigator && navigator.userAgent || '';


/***/ }),

/***/ "a5b8":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 25.4.1.5 NewPromiseCapability(C)
var aFunction = __webpack_require__("d8e8");

function PromiseCapability(C) {
  var resolve, reject;
  this.promise = new C(function ($$resolve, $$reject) {
    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
    resolve = $$resolve;
    reject = $$reject;
  });
  this.resolve = aFunction(resolve);
  this.reject = aFunction(reject);
}

module.exports.f = function (C) {
  return new PromiseCapability(C);
};


/***/ }),

/***/ "bcaa":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var isObject = __webpack_require__("d3f4");
var newPromiseCapability = __webpack_require__("a5b8");

module.exports = function (C, x) {
  anObject(C);
  if (isObject(x) && x.constructor === C) return x;
  var promiseCapability = newPromiseCapability.f(C);
  var resolve = promiseCapability.resolve;
  resolve(x);
  return promiseCapability.promise;
};


/***/ }),

/***/ "be13":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "c366":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("6821");
var toLength = __webpack_require__("9def");
var toAbsoluteIndex = __webpack_require__("77f1");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "ca5a":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "cadf":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__("9c6c");
var step = __webpack_require__("d53b");
var Iterators = __webpack_require__("84f2");
var toIObject = __webpack_require__("6821");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__("01f9")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "ce10":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("69a8");
var toIObject = __webpack_require__("6821");
var arrayIndexOf = __webpack_require__("c366")(false);
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "d53b":
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "d8e8":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "dcbc":
/***/ (function(module, exports, __webpack_require__) {

var redefine = __webpack_require__("2aba");
module.exports = function (target, src, safe) {
  for (var key in src) redefine(target, key, src[key], safe);
  return target;
};


/***/ }),

/***/ "e11e":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "ebd6":
/***/ (function(module, exports, __webpack_require__) {

// 7.3.20 SpeciesConstructor(O, defaultConstructor)
var anObject = __webpack_require__("cb7c");
var aFunction = __webpack_require__("d8e8");
var SPECIES = __webpack_require__("2b4c")('species');
module.exports = function (O, D) {
  var C = anObject(O).constructor;
  var S;
  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
};


/***/ }),

/***/ "efa6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("5e6f");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "f605":
/***/ (function(module, exports) {

module.exports = function (it, Constructor, name, forbiddenField) {
  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
    throw TypeError(name + ': incorrect invocation!');
  } return it;
};


/***/ }),

/***/ "fab2":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("7726").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
var setPublicPath = __webpack_require__("1eb2");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/slide.vue?vue&type=template&id=70eb7296&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.openMenu,"closeMenu":_vm.closeMenu}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/slide.vue?vue&type=template&id=70eb7296&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu.vue?vue&type=template&id=0a659602&
var Menuvue_type_template_id_0a659602_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('div',{ref:"sideNav",staticClass:"bm-menu"},[_c('nav',{staticClass:"bm-item-list"},[_vm._t("default")],2),_c('span',{staticClass:"bm-cross-button cross-style",class:{ hidden: !_vm.crossIcon },on:{"click":_vm.closeMenu}},_vm._l((2),function(x,index){return _c('span',{key:x,staticClass:"bm-cross",style:({ position: 'absolute', width: '3px', height: '14px',transform: index === 1 ? 'rotate(45deg)' : 'rotate(-45deg)'})})}))]),_c('div',{ref:"bmBurgerButton",staticClass:"bm-burger-button",class:{ hidden: !_vm.burgerIcon },on:{"click":_vm.openMenu}},_vm._l((3),function(x,index){return _c('span',{key:index,staticClass:"bm-burger-bars line-style",style:({top:20 * (index * 2) + '%'})})}))])}
var Menuvue_type_template_id_0a659602_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu.vue?vue&type=template&id=0a659602&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Menuvue_type_script_lang_js_ = ({
  name: 'menubar',
  data: function data() {
    return {
      isSideBarOpen: false
    };
  },
  props: {
    isOpen: {
      type: Boolean,
      required: false
    },
    right: {
      type: Boolean,
      required: false
    },
    width: {
      type: [String],
      required: false,
      default: '300'
    },
    disableEsc: {
      type: Boolean,
      required: false
    },
    noOverlay: {
      type: Boolean,
      required: false
    },
    onStateChange: {
      type: Function,
      required: false
    },
    burgerIcon: {
      type: Boolean,
      required: false,
      default: true
    },
    crossIcon: {
      type: Boolean,
      required: false,
      default: true
    },
    disableOutsideClick: {
      type: Boolean,
      required: false,
      default: false
    },
    closeOnNavigation: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit('openMenu');
      this.isSideBarOpen = true;

      if (!this.noOverlay) {
        document.body.classList.add('bm-overlay');
      }

      if (this.right) {
        this.$refs.sideNav.style.left = 'auto';
        this.$refs.sideNav.style.right = '0px';
      }

      this.$nextTick(function () {
        this.$refs.sideNav.style.width = this.width ? this.width + 'px' : '300px';
      });
    },
    closeMenu: function closeMenu() {
      this.$emit('closeMenu');
      this.isSideBarOpen = false;
      document.body.classList.remove('bm-overlay');
      this.$refs.sideNav.style.width = '0px';
    },
    closeMenuOnEsc: function closeMenuOnEsc(e) {
      e = e || window.event;

      if (e.key === 'Escape' || e.keyCode === 27) {
        this.closeMenu();
      }
    },
    documentClick: function documentClick(e) {
      var element = this.$refs.bmBurgerButton;
      var target = null;

      if (e && e.target) {
        target = e.target;
      }

      if (element && element !== target && !element.contains(target) && !this.hasClass(target, 'bm-menu') && this.isSideBarOpen && !this.disableOutsideClick) {
        this.closeMenu();
      } else if (element && this.hasClass(target, 'bm-menu') && this.isSideBarOpen && this.closeOnNavigation) {
        this.closeMenu();
      }
    },
    hasClass: function hasClass(element, className) {
      do {
        if (element.classList && element.classList.contains(className)) {
          return true;
        }

        element = element.parentNode;
      } while (element);

      return false;
    }
  },
  mounted: function mounted() {
    if (!this.disableEsc) {
      document.addEventListener('keyup', this.closeMenuOnEsc);
    }
  },
  created: function created() {
    document.addEventListener('click', this.documentClick);
  },
  destroyed: function destroyed() {
    document.removeEventListener('keyup', this.closeMenuOnEsc);
    document.removeEventListener('click', this.documentClick);
  },
  watch: {
    isOpen: {
      deep: true,
      immediate: true,
      handler: function handler(newValue, oldValue) {
        var _this = this;

        this.$nextTick(function () {
          if (!oldValue && newValue) {
            _this.openMenu();
          }

          if (oldValue && !newValue) {
            _this.closeMenu();
          }
        });
      }
    },
    right: {
      deep: true,
      immediate: true,
      handler: function handler(oldValue, newValue) {
        var _this2 = this;

        if (oldValue) {
          this.$nextTick(function () {
            _this2.$refs.bmBurgerButton.style.left = 'auto';
            _this2.$refs.bmBurgerButton.style.right = '36px';
            _this2.$refs.sideNav.style.left = 'auto';
            _this2.$refs.sideNav.style.right = '0px';
            document.querySelector('.bm-burger-button').style.left = 'auto';
            document.querySelector('.bm-burger-button').style.right = '36px';
            document.querySelector('.bm-menu').style.left = 'auto';
            document.querySelector('.bm-menu').style.right = '0px';
            document.querySelector('.cross-style').style.right = '250px';
          });
        }

        if (newValue) {
          if (this.$refs.bmBurgerButton.hasAttribute('style')) {
            this.$refs.bmBurgerButton.removeAttribute('style');
            this.$refs.sideNav.style.right = 'auto';
            document.querySelector('.bm-burger-button').removeAttribute('style');
            document.getElementById('sideNav').style.right = 'auto';
            document.querySelector('.cross-style').style.right = '0px';
          }
        }
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Menuvue_type_script_lang_js_ = (Menuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Menu.vue?vue&type=style&index=0&lang=css&
var Menuvue_type_style_index_0_lang_css_ = __webpack_require__("efa6");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/Menu.vue






/* normalize component */

var component = normalizeComponent(
  components_Menuvue_type_script_lang_js_,
  Menuvue_type_template_id_0a659602_render,
  Menuvue_type_template_id_0a659602_staticRenderFns,
  false,
  null,
  null,
  null
  
)

component.options.__file = "Menu.vue"
/* harmony default export */ var Menu = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/slide.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var slidevue_type_script_lang_js_ = ({
  name: 'slide',
  components: {
    Menu: Menu
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit('openMenu');
    },
    closeMenu: function closeMenu() {
      this.$emit('closeMenu');
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/slide.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_slidevue_type_script_lang_js_ = (slidevue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/slide.vue





/* normalize component */

var slide_component = normalizeComponent(
  Menu_slidevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

slide_component.options.__file = "slide.vue"
/* harmony default export */ var slide = (slide_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/bubble.vue?vue&type=template&id=e3d93326&
var bubblevue_type_template_id_e3d93326_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({ref:"sideNav",on:{"openMenu":_vm.openMenu,"closeMenu":_vm.closeMenu}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var bubblevue_type_template_id_e3d93326_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/bubble.vue?vue&type=template&id=e3d93326&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/bubble.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var bubblevue_type_script_lang_js_ = ({
  name: 'bubble',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      propsToPass: {
        isOpen: this.$attrs.isOpen,
        right: this.$attrs.right,
        width: this.$attrs.width,
        disableEsc: this.$attrs.disableEsc,
        noOverlay: this.$attrs.noOverlay,
        onStateChange: this.$attrs.onStateChange
      }
    };
  },
  methods: {
    openMenu: function openMenu() {
      //this.$emit("openMenu")
      var set = this.$refs.sideNav.$el.querySelector('.bm-menu'); //console.log(set,"lallan")

      set.style.borderRadius = '150% / 70%';

      if (this.$attrs.right) {
        set.style.borderTopRightRadius = '0px 900px';
        set.style.borderBottomRightRadius = '0px';
      } else {
        set.style.borderTopLeftRadius = '0px 900px';
        set.style.borderBottomLeftRadius = '0px';
      }

      set.style.transitionTimingFunction = 'easy-in';
      this.$emit("openMenu");
      setTimeout(function () {
        set.style.transitionTimingFunction = 'cubic-bezier(.29, 1.01, 1, -0.68)';
        set.style.borderRadius = '0px';
      }, 300);
    },
    closeMenu: function closeMenu() {
      //this.$emit("closeMenu")
      var set = this.$refs.sideNav.$el.querySelector('.bm-menu');
      set.style.transitionTimingFunction = null;
      this.$emit("closeMenu");
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/bubble.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_bubblevue_type_script_lang_js_ = (bubblevue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/bubble.vue





/* normalize component */

var bubble_component = normalizeComponent(
  Menu_bubblevue_type_script_lang_js_,
  bubblevue_type_template_id_e3d93326_render,
  bubblevue_type_template_id_e3d93326_staticRenderFns,
  false,
  null,
  null,
  null
  
)

bubble_component.options.__file = "bubble.vue"
/* harmony default export */ var bubble = (bubble_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/reveal.vue?vue&type=template&id=01427236&
var revealvue_type_template_id_01427236_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.push,"closeMenu":_vm.pull}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var revealvue_type_template_id_01427236_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/reveal.vue?vue&type=template&id=01427236&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/reveal.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var revealvue_type_script_lang_js_ = ({
  name: 'reveal',
  data: function data() {
    return {
      bodyOldStyle: ''
    };
  },
  components: {
    Menu: Menu
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    },
    push: function push() {
      this.openMenu();
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '300px';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, 0px )");
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, 0px )");
      }

      document.querySelector('#page-wrap').style.position = 'relative';
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
    },
    pull: function pull() {
      this.closeMenu();
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.querySelector('#page-wrap').style.position = '';
      document.body.setAttribute('style', this.bodyOldStyle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/reveal.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_revealvue_type_script_lang_js_ = (revealvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/reveal.vue





/* normalize component */

var reveal_component = normalizeComponent(
  Menu_revealvue_type_script_lang_js_,
  revealvue_type_template_id_01427236_render,
  revealvue_type_template_id_01427236_staticRenderFns,
  false,
  null,
  null,
  null
  
)

reveal_component.options.__file = "reveal.vue"
/* harmony default export */ var reveal = (reveal_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/push.vue?vue&type=template&id=1e67cf34&
var pushvue_type_template_id_1e67cf34_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.push,"closeMenu":_vm.pull}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var pushvue_type_template_id_1e67cf34_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/push.vue?vue&type=template&id=1e67cf34&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.array.iterator.js
var es6_array_iterator = __webpack_require__("cadf");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.promise.js
var es6_promise = __webpack_require__("551c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es7.promise.finally.js
var es7_promise_finally = __webpack_require__("097d");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/push.vue?vue&type=script&lang=js&



//
//
//
//
//
//
//
//

/* harmony default export */ var pushvue_type_script_lang_js_ = ({
  name: 'push',
  data: function data() {
    return {
      bodyOldStyle: ''
    };
  },
  components: {
    Menu: Menu
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    },
    push: function push() {
      this.openMenu();
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '300px';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, 0px )");
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, 0px )");
      }

      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
    },
    pull: function pull() {
      this.closeMenu();
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.body.setAttribute('style', this.bodyOldStyle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/push.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_pushvue_type_script_lang_js_ = (pushvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/push.vue





/* normalize component */

var push_component = normalizeComponent(
  Menu_pushvue_type_script_lang_js_,
  pushvue_type_template_id_1e67cf34_render,
  pushvue_type_template_id_1e67cf34_staticRenderFns,
  false,
  null,
  null,
  null
  
)

push_component.options.__file = "push.vue"
/* harmony default export */ var push = (push_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/elastic.vue?vue&type=template&id=378b43ec&
var elasticvue_type_template_id_378b43ec_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({attrs:{"openMenu":"openMenu"},on:{"closeMenu":_vm.closeMenu}},'Menu',_vm.propsToPass,false),[_vm._t("default")],2)],1)}
var elasticvue_type_template_id_378b43ec_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/elastic.vue?vue&type=template&id=378b43ec&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/elastic.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var elasticvue_type_script_lang_js_ = ({
  name: 'elastic',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      propsToPass: {
        isOpen: this.$attrs.isOpen,
        right: this.$attrs.right,
        width: this.$attrs.width,
        disableEsc: this.$attrs.disableEsc,
        noOverlay: this.$attrs.noOverlay,
        onStateChange: this.$attrs.onStateChange
      }
    };
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/elastic.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_elasticvue_type_script_lang_js_ = (elasticvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/elastic.vue





/* normalize component */

var elastic_component = normalizeComponent(
  Menu_elasticvue_type_script_lang_js_,
  elasticvue_type_template_id_378b43ec_render,
  elasticvue_type_template_id_378b43ec_staticRenderFns,
  false,
  null,
  null,
  null
  
)

elastic_component.options.__file = "elastic.vue"
/* harmony default export */ var elastic = (elastic_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/fallDown.vue?vue&type=template&id=1b3f33be&
var fallDownvue_type_template_id_1b3f33be_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({ref:"sideNav",on:{"openMenu":_vm.openMenu,"closeMenu":_vm.closeMenu}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var fallDownvue_type_template_id_1b3f33be_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/fallDown.vue?vue&type=template&id=1b3f33be&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/fallDown.vue?vue&type=script&lang=js&



//
//
//
//
//
//
//
//

/* harmony default export */ var fallDownvue_type_script_lang_js_ = ({
  name: 'elastic',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      bodyOldStyle: '',
      propsToPass: {
        isOpen: this.$attrs.isOpen,
        right: this.$attrs.right,
        width: this.$attrs.width,
        disableEsc: this.$attrs.disableEsc,
        noOverlay: this.$attrs.noOverlay,
        onStateChange: this.$attrs.onStateChange
      }
    };
  },
  methods: {
    openMenu: function openMenu() {
      var _this = this;

      this.$emit("openMenu");
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '300px';
      this.$refs.sideNav.$el.querySelector('.bm-menu').style.overflowY = 'hidden';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';
      this.$refs.sideNav.$el.querySelector('.bm-menu').style.transition = '0.5s';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, 0px )");
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, 0px )");
      }

      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      this.$nextTick(function () {
        _this.$refs.sideNav.$el.querySelector('.bm-menu').style.height = '100%';
      });
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.body.setAttribute('style', this.bodyOldStyle);
      this.$refs.sideNav.$el.querySelector('.bm-menu').style.height = '0px';
    }
  },
  mounted: function mounted() {
    this.$refs.sideNav.$el.querySelector('.bm-menu').style.height = '0px';
  }
});
// CONCATENATED MODULE: ./src/components/Menu/fallDown.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_fallDownvue_type_script_lang_js_ = (fallDownvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/fallDown.vue





/* normalize component */

var fallDown_component = normalizeComponent(
  Menu_fallDownvue_type_script_lang_js_,
  fallDownvue_type_template_id_1b3f33be_render,
  fallDownvue_type_template_id_1b3f33be_staticRenderFns,
  false,
  null,
  null,
  null
  
)

fallDown_component.options.__file = "fallDown.vue"
/* harmony default export */ var fallDown = (fallDown_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/pushRotate.vue?vue&type=template&id=b055c8f2&
var pushRotatevue_type_template_id_b055c8f2_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.push,"closeMenu":_vm.pull}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var pushRotatevue_type_template_id_b055c8f2_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/pushRotate.vue?vue&type=template&id=b055c8f2&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/pushRotate.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var pushRotatevue_type_script_lang_js_ = ({
  name: 'pushrotate',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      bodyOldStyle: '',
      appOldStyle: ''
    };
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    },
    push: function push() {
      this.openMenu();
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '300px';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, 0px ) rotateY(15deg)");
        document.querySelector('#page-wrap').style.transformOrigin = '100% 50% 0px';
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, 0px ) rotateY(-15deg)");
        document.querySelector('#page-wrap').style.transformOrigin = '0% 50% 0px';
      }

      document.querySelector('#page-wrap').style.transformStyle = 'preserve-3d';
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      this.appOldStyle = document.querySelector('#app').getAttribute('style') || '';
      document.querySelector('#app').style.perspective = '1500px';
      document.querySelector('#app').style.overflow = 'hidden';
    },
    pull: function pull() {
      this.closeMenu();
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.querySelector('#page-wrap').style.transformStyle = '';
      document.querySelector('#page-wrap').style.transformOrigin = '';
      document.querySelector('#app').setAttribute('style', this.appOldStyle);
      document.body.setAttribute('style', this.bodyOldStyle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/pushRotate.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_pushRotatevue_type_script_lang_js_ = (pushRotatevue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/pushRotate.vue





/* normalize component */

var pushRotate_component = normalizeComponent(
  Menu_pushRotatevue_type_script_lang_js_,
  pushRotatevue_type_template_id_b055c8f2_render,
  pushRotatevue_type_template_id_b055c8f2_staticRenderFns,
  false,
  null,
  null,
  null
  
)

pushRotate_component.options.__file = "pushRotate.vue"
/* harmony default export */ var pushRotate = (pushRotate_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/stack.vue?vue&type=template&id=9093ae04&
var stackvue_type_template_id_9093ae04_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({attrs:{"openMenu":"openMenu"},on:{"closeMenu":_vm.closeMenu}},'Menu',_vm.propsToPass,false),[_vm._t("default")],2)],1)}
var stackvue_type_template_id_9093ae04_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/stack.vue?vue&type=template&id=9093ae04&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/stack.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var stackvue_type_script_lang_js_ = ({
  name: 'stack',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      propsToPass: {
        isOpen: this.$attrs.isOpen,
        right: this.$attrs.right,
        width: this.$attrs.width,
        disableEsc: this.$attrs.disableEsc,
        noOverlay: this.$attrs.noOverlay,
        onStateChange: this.$attrs.onStateChange
      }
    };
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/stack.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_stackvue_type_script_lang_js_ = (stackvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/stack.vue





/* normalize component */

var stack_component = normalizeComponent(
  Menu_stackvue_type_script_lang_js_,
  stackvue_type_template_id_9093ae04_render,
  stackvue_type_template_id_9093ae04_staticRenderFns,
  false,
  null,
  null,
  null
  
)

stack_component.options.__file = "stack.vue"
/* harmony default export */ var stack = (stack_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/scaleRotate.vue?vue&type=template&id=5c3b7fd8&
var scaleRotatevue_type_template_id_5c3b7fd8_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.push,"closeMenu":_vm.pull}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var scaleRotatevue_type_template_id_5c3b7fd8_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/scaleRotate.vue?vue&type=template&id=5c3b7fd8&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/scaleRotate.vue?vue&type=script&lang=js&



//
//
//
//
//
//
//
//

/* harmony default export */ var scaleRotatevue_type_script_lang_js_ = ({
  name: 'scalerotate',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      bodyOldStyle: '',
      appOldStyle: ''
    };
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    },
    push: function push() {
      this.openMenu();
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '100px';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, -600px ) rotateY(20deg)");
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, -600px ) rotateY(-20deg)");
      }

      document.querySelector('#page-wrap').style.transformStyle = 'preserve-3d';
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.overflow = 'hidden';
      this.appOldStyle = document.querySelector('#app').getAttribute('style') || '';
      document.querySelector('#app').style.perspective = '1500px';
      document.querySelector('#app').style.overflow = 'hidden';
      document.querySelector('#app').style.height = '100%';
    },
    pull: function pull() {
      this.closeMenu();
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.querySelector('#page-wrap').style.transformStyle = '';
      document.querySelector('#page-wrap').style.transformOrigin = '';
      document.querySelector('#page-wrap').style.overflow = 'auto';
      document.querySelector('#app').setAttribute('style', this.appOldStyle);
      document.body.setAttribute('style', this.bodyOldStyle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/scaleRotate.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_scaleRotatevue_type_script_lang_js_ = (scaleRotatevue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/scaleRotate.vue





/* normalize component */

var scaleRotate_component = normalizeComponent(
  Menu_scaleRotatevue_type_script_lang_js_,
  scaleRotatevue_type_template_id_5c3b7fd8_render,
  scaleRotatevue_type_template_id_5c3b7fd8_staticRenderFns,
  false,
  null,
  null,
  null
  
)

scaleRotate_component.options.__file = "scaleRotate.vue"
/* harmony default export */ var scaleRotate = (scaleRotate_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9cdd6cc0-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/scaleDown.vue?vue&type=template&id=34b586d9&
var scaleDownvue_type_template_id_34b586d9_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('Menu',_vm._b({on:{"openMenu":_vm.push,"closeMenu":_vm.pull}},'Menu',this.$attrs,false),[_vm._t("default")],2)],1)}
var scaleDownvue_type_template_id_34b586d9_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Menu/scaleDown.vue?vue&type=template&id=34b586d9&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Menu/scaleDown.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var scaleDownvue_type_script_lang_js_ = ({
  name: 'scaledown',
  components: {
    Menu: Menu
  },
  data: function data() {
    return {
      bodyOldStyle: '',
      appOldStyle: ''
    };
  },
  methods: {
    openMenu: function openMenu() {
      this.$emit("openMenu");
    },
    closeMenu: function closeMenu() {
      this.$emit("closeMenu");
    },
    push: function push() {
      this.openMenu();
      var width = this.$attrs.width ? this.$attrs.width + 'px' : '100px';
      this.bodyOldStyle = document.body.getAttribute('style') || '';
      document.body.style.overflowX = 'hidden';

      if (this.$attrs.right) {
        document.querySelector('#page-wrap').style.transform = "translate3d(-".concat(width, ", 0px, -600px ) ");
      } else {
        document.querySelector('#page-wrap').style.transform = "translate3d(".concat(width, ", 0px, -600px ) ");
      }

      document.querySelector('#page-wrap').style.transformStyle = 'preserve-3d';
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.overflow = 'hidden';
      this.appOldStyle = document.querySelector('#app').getAttribute('style') || '';
      document.querySelector('#app').style.perspective = '1500px';
      document.querySelector('#app').style.overflow = 'hidden';
      document.querySelector('#app').style.height = '100%';
    },
    pull: function pull() {
      this.closeMenu();
      document.querySelector('#page-wrap').style.transition = 'all 0.5s ease 0s';
      document.querySelector('#page-wrap').style.transform = '';
      document.querySelector('#page-wrap').style.transformStyle = '';
      document.querySelector('#page-wrap').style.transformOrigin = '';
      document.querySelector('#page-wrap').style.overflow = 'auto';
      document.querySelector('#app').setAttribute('style', this.appOldStyle);
      document.body.setAttribute('style', this.bodyOldStyle);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Menu/scaleDown.vue?vue&type=script&lang=js&
 /* harmony default export */ var Menu_scaleDownvue_type_script_lang_js_ = (scaleDownvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Menu/scaleDown.vue





/* normalize component */

var scaleDown_component = normalizeComponent(
  Menu_scaleDownvue_type_script_lang_js_,
  scaleDownvue_type_template_id_34b586d9_render,
  scaleDownvue_type_template_id_34b586d9_staticRenderFns,
  false,
  null,
  null,
  null
  
)

scaleDown_component.options.__file = "scaleDown.vue"
/* harmony default export */ var scaleDown = (scaleDown_component.exports);
// CONCATENATED MODULE: ./src/components/index.js
// Different CSS animations











/* harmony default export */ var components = ({
  Menu: Menu,
  Slide: slide,
  Bubble: bubble,
  Reveal: reveal,
  Push: push,
  PushRotate: pushRotate,
  ScaleDown: scaleDown,
  ScaleRotate: scaleRotate,
  Stack: stack,
  FallDown: fallDown,
  Elastic: elastic
});
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (components);



/***/ })

/******/ })["default"];
//# sourceMappingURL=vue-burger-menu.common.js.map

/***/ }),

/***/ 932:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "div" },
    [
      !_vm.dontShowNav
        ? _c(
            "div",
            { staticClass: "site-navbar" },
            [
              _c("div", { staticClass: "navLink" }, [
                _c(
                  "h1",
                  { staticClass: "site-title" },
                  [
                    _c("router-link", { attrs: { to: "/", tag: "a" } }, [
                      _c("span", { staticClass: "logo" }, [
                        _c("div", { staticClass: "logoText" }, [
                          _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
                          _c("span", { staticClass: "guruh" }, [
                            _vm._v("Guruh")
                          ])
                        ])
                      ])
                    ])
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "navLinkBar" }, [
                _c(
                  "div",
                  { staticClass: "explore-container" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "explore-button",
                        attrs: { to: "/entrepreneur" }
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-home exploreBar pr-2",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("\n            My Home\n          ")
                      ]
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "searchContainer" }, [
                _c("div", { staticClass: "form-group search" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control rounded-pill border-0 bg-search",
                    attrs: {
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "What do you wish to know?",
                      autocomplete: "off"
                    },
                    domProps: { value: _vm.search },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary sea",
                      attrs: { type: "button" },
                      on: { click: _vm.searchNow }
                    },
                    [
                      _c("i", {
                        staticClass: "fa fa-search seaIcon",
                        class: { "text-main": _vm.whiteBg },
                        attrs: { "aria-hidden": "true" }
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _vm.search !== ""
                  ? _c("div", { staticClass: "showSearch" }, [
                      _c(
                        "div",
                        {
                          staticClass: "closeSearch",
                          on: { click: _vm.clearSearch }
                        },
                        [
                          _c("i", {
                            staticClass: "fa fa-times",
                            attrs: { "aria-hidden": "true" }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _vm.vendors.length > 0
                        ? _c(
                            "div",
                            _vm._l(_vm.vendors, function(vendor, index) {
                              return _c(
                                "div",
                                { key: index },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "title",
                                      attrs: {
                                        to: {
                                          name: "PartnerProfile",
                                          params: { username: vendor.username }
                                        }
                                      }
                                    },
                                    [
                                      _c("span", [
                                        _vm._v(_vm._s(vendor.storeName))
                                      ])
                                    ]
                                  )
                                ],
                                1
                              )
                            }),
                            0
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.articles.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: { to: "/entrepreneur/articles" }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Article\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.articles, function(article, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "ArticleSinglePage",
                                            params: {
                                              id: article.product_id,
                                              name: article.title.replace(
                                                / /g,
                                                "-"
                                              )
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(article.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.researches.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: {
                                    to: "/entrepreneur/paper/market-research"
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Market Research\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.researches, function(research, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "ProductPaperDetail",
                                            params: {
                                              id: research.product_id,
                                              name: research.title
                                                .replace(/\s+/g, "-")
                                                .toLowerCase(),
                                              type: "subscribe"
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(research.title.toLowerCase()) +
                                            " - " +
                                            _vm._s(research.host)
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.videos.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: { to: "/entrepreneur/videos" }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Video\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.videos, function(video, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "Video",
                                            params: { id: video.product_id }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(video.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.videosVendors.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: { to: "/entrepreneur/videos" }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Video\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.videosVendors, function(video, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "Video",
                                            params: { id: video.product_id }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(video.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.podcasts.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: { to: "/podcast" }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Podcast\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.podcasts, function(podcast, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "SingleVideoPage",
                                            params: { id: podcast.product_id }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(podcast.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.courses.length > 0
                        ? _c(
                            "div",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "pryHeader",
                                  attrs: { to: "/courses" }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-search",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Course\n            ")
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.courses, function(course, index) {
                                return _c(
                                  "div",
                                  { key: index },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "title",
                                        attrs: {
                                          to: {
                                            name: "CourseFullPage",
                                            params: {
                                              id: course.product_id,
                                              name: course.title.replace(
                                                / /g,
                                                "-"
                                              )
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(course.title.toLowerCase())
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e()
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "navItem--right desktop-menu" }, [
                _c("div", { staticClass: "navBod" }, [
                  !_vm.authUs
                    ? _c(
                        "div",
                        { staticClass: "m10" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "upgradeNav",
                              attrs: { to: "/entrepreneur/user-subscription/0" }
                            },
                            [_vm._v("Subscribe")]
                          )
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.authUs
                    ? _c(
                        "div",
                        { staticClass: "m10" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "upgradeNav",
                              attrs: { to: "/entrepreneur/explore" }
                            },
                            [_vm._v("Explore")]
                          ),
                          _vm._v(" "),
                          this.userSub >= 3
                            ? _c(
                                "router-link",
                                {
                                  staticClass: "upgradeNav",
                                  attrs: {
                                    to: "/entrepreneur/user-subscription/0"
                                  }
                                },
                                [_vm._v("Pricing")]
                              )
                            : _c(
                                "router-link",
                                {
                                  staticClass: "upgradeNav",
                                  attrs: {
                                    to: "/entrepreneur/user-subscription/0"
                                  }
                                },
                                [_vm._v("Upgrade")]
                              )
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "m10 hov" },
                    [
                      _c(
                        "router-link",
                        { staticClass: "upgradeNav", attrs: { to: "/about" } },
                        [_vm._v("How it works")]
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.authUs,
                      expression: "authUs"
                    }
                  ],
                  attrs: { id: "notification" }
                }),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "cartIcon",
                    class: { cartIconLogged: _vm.authenticate }
                  },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "cartLink",
                        attrs: { to: { name: "userCart" } }
                      },
                      [
                        _c("i", { staticClass: "fa fa-cart-plus" }, [
                          _vm.badge > 0
                            ? _c("div", [_vm._v(_vm._s(_vm.badge))])
                            : _vm._e()
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.showUpdate
                  ? _c("div", { staticClass: "update-profile" }, [
                      _c("i", {
                        staticClass:
                          "fa fa-info-circle fa-2x animated bounceIn slower infinite bg-white fa-inverse pointer",
                        staticStyle: { color: "#5b84a7" },
                        attrs: { "aria-hidden": "true" },
                        on: { click: _vm.updateProfile }
                      }),
                      _vm._v(" "),
                      _c(
                        "p",
                        {
                          staticClass: "update-text shadow-sm",
                          on: { click: _vm.updateProfile }
                        },
                        [_vm._v("Complete your profile")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                !_vm.authUs && !_vm.accountShow
                  ? _c("ul", { staticClass: "m10" }, [
                      _c("li", { staticClass: "navLink" }, [
                        _c(
                          "span",
                          { staticClass: "register-nav hov" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "bottom",
                                attrs: {
                                  tag: "a",
                                  to: {
                                    name: "auth",
                                    params: { name: "login" }
                                  }
                                }
                              },
                              [_vm._v("Log In")]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "navLink login" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "rounded-pill",
                                attrs: {
                                  tag: "a",
                                  to: {
                                    name: "auth",
                                    params: { name: "register" }
                                  }
                                }
                              },
                              [_vm._v("Join Our BizTribe")]
                            )
                          ],
                          1
                        )
                      ])
                    ])
                  : _c("div", { staticClass: "profile-pos" }, [
                      !_vm.accountShow
                        ? _c(
                            "span",
                            { staticClass: "nameTab" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "d-flex align-items-center",
                                  attrs: {
                                    to: {
                                      name: "MainProfilePage",
                                      params: { username: _vm.username }
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "span",
                                    {
                                      staticClass: "user_name",
                                      class: { "text-main": _vm.whiteBg }
                                    },
                                    [_vm._v("Hi " + _vm._s(_vm.name))]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "profImage" }, [
                                    _vm.profileImage === undefined ||
                                    _vm.profileImage === null
                                      ? _c("i", {
                                          staticClass:
                                            "fas fa-user-circle mobColor ml-2"
                                        })
                                      : _c("img", {
                                          attrs: {
                                            src: _vm.profileImage,
                                            alt: ""
                                          }
                                        })
                                  ])
                                ]
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("ul", { staticClass: "profile-wrapper" }, [
                        _c("li", { staticClass: "p-2" }),
                        _vm._v(" "),
                        _c(
                          "li",
                          { staticClass: "navDrop" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "bottom text-main",
                                attrs: { to: "/entrepreneur/explore" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-eye pr-1 text-main"
                                }),
                                _vm._v(" Explore\n              ")
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          { staticClass: "navDrop" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "bottom text-main",
                                attrs: {
                                  to:
                                    "/entrepreneur/profile/products/subscription"
                                }
                              },
                              [
                                _c("i", {
                                  staticClass:
                                    "fas fa-folder fa-inverse pr-1 text-main",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" BizLibary\n              ")
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          { staticClass: "navDrop" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "bottom text-main",
                                attrs: { to: "/entrepreneur/choose" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-cog pr-1 text-main",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" Settings\n              ")
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            staticClass: "logout navDrop py-2 text-main",
                            on: {
                              click: function($event) {
                                return _vm.logout()
                              }
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "fas fa-sign-out-alt text-main"
                            }),
                            _vm._v("\n              Logout\n            ")
                          ]
                        )
                      ])
                    ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "profile mobile_cart" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "cartLink",
                      attrs: { to: { name: "userCart" } }
                    },
                    [
                      _c("i", { staticClass: "fa fa-cart-plus" }, [
                        _vm.badge > 0
                          ? _c("div", { staticClass: "cartNum" }, [
                              _vm._v(_vm._s(_vm.badge))
                            ])
                          : _vm._e()
                      ])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "Slide",
                {
                  staticClass: "mobile-menu bgWhite",
                  attrs: {
                    burgerIcon: true,
                    width: "300",
                    left: "",
                    closeOnNavigation: true
                  }
                },
                [
                  _c("div", [
                    _c(
                      "ul",
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "bottom d-flex justify-content-start",
                            attrs: {
                              to: {
                                name: "MainProfilePage",
                                params: { username: _vm.username }
                              }
                            }
                          },
                          [
                            _vm.authUs
                              ? _c("li", { staticClass: "profile_name" }, [
                                  _c("div", { staticClass: "profImage" }, [
                                    _vm.profileImage === undefined ||
                                    _vm.profileImage === null
                                      ? _c("i", {
                                          staticClass:
                                            "fas fa-user-circle fa-inverse pr-2"
                                        })
                                      : _c("img", {
                                          attrs: {
                                            src: _vm.profileImage,
                                            alt: ""
                                          }
                                        })
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    { staticClass: "user_name pr-2 mb-2" },
                                    [_vm._v("Hi " + _vm._s(_vm.name))]
                                  )
                                ])
                              : _vm._e()
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            staticClass: "mobile_insights nav-tab mb-2 mobColor"
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "bottom",
                                attrs: { to: "/entrepreneur" }
                              },
                              [
                                _c("i", {
                                  staticClass:
                                    "fas fa-home fa-inverse pr-2 text-white-50",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" My Home\n              ")
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        !_vm.authUs
                          ? _c(
                              "li",
                              { staticClass: "mb-2" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "upgradeNav",
                                    attrs: {
                                      to: "/entrepreneur/user-subscription/0"
                                    }
                                  },
                                  [_vm._v("Subscribe")]
                                )
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.authUs
                          ? _c(
                              "li",
                              { staticClass: "mb-2" },
                              [
                                this.userSub >= 3
                                  ? _c(
                                      "router-link",
                                      {
                                        staticClass: "upgradeNav",
                                        attrs: {
                                          to:
                                            "/entrepreneur/user-subscription/0"
                                        }
                                      },
                                      [_vm._v("Pricing")]
                                    )
                                  : _c(
                                      "router-link",
                                      {
                                        staticClass: "upgradeNav",
                                        attrs: {
                                          to:
                                            "/entrepreneur/user-subscription/0"
                                        }
                                      },
                                      [_vm._v("Upgrade")]
                                    )
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("hr"),
                        _vm._v(" "),
                        _c(
                          "li",
                          { staticClass: "mobile_insights nav-tab mb-2" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "mobColor bottom",
                                attrs: { tag: "a", to: { name: "InsightPage" } }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-th pr-2 text-white-50",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" Explore\n              ")
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.authUs
                          ? _c(
                              "li",
                              {
                                staticClass:
                                  "mobile_insights nav-tab mb-2 mobColor"
                              },
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "bottom",
                                    attrs: {
                                      to:
                                        "/entrepreneur/profile/products/subscription"
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass:
                                        "fas fa-folder fa-inverse pr-2 text-white-50",
                                      attrs: { "aria-hidden": "true" }
                                    }),
                                    _vm._v(" BizLibary\n              ")
                                  ]
                                )
                              ],
                              1
                            )
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("hr", { staticClass: "mb-1" }),
                    _vm._v(" "),
                    _vm.authUs && _vm.showUpdate
                      ? _c(
                          "li",
                          {
                            staticClass: "mt-2 mobColor",
                            staticStyle: { "list-style": "none" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "mobColor bottom",
                                attrs: { to: "/entrepreneur/update-profile" }
                              },
                              [
                                _c("i", {
                                  staticClass:
                                    "fas fa-user-edit fa-inverse pr-2 text-white-50",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v("Profile\n              "),
                                _c("i", {
                                  staticClass:
                                    "fa fa-info-circle animated bounceIn slower infinite fa-inverse pointer",
                                  attrs: { "aria-hidden": "true" }
                                })
                              ]
                            )
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.authUs
                      ? _c(
                          "li",
                          {
                            staticClass: "mt-2 mobColor",
                            staticStyle: { "list-style": "none" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "mobColor bottom",
                                attrs: { to: "/entrepreneur/wishlist" }
                              },
                              [
                                _c("i", {
                                  staticClass:
                                    "fas fa-heart fa-inverse pr-2 text-white-50",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" Wishlist\n            ")
                              ]
                            )
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.authUs
                      ? _c(
                          "li",
                          {
                            staticClass: "mt-2 mobColor",
                            staticStyle: { "list-style": "none" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "mobColor bottom",
                                attrs: { to: "/preferences" }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-cog pr-2 text-white-50"
                                }),
                                _vm._v(" Preference\n            ")
                              ]
                            )
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "li",
                      {
                        staticClass: "py-2 mobColor",
                        staticStyle: { "list-style": "none" }
                      },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "mobColor bottom",
                            attrs: { to: "/entrepreneur/contact" }
                          },
                          [
                            _c("i", {
                              staticClass:
                                "fas fa-envelope fa-inverse pr-2 text-white-50",
                              attrs: { "aria-hidden": "true" }
                            }),
                            _vm._v(" Contact Us\n            ")
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    !_vm.authUs
                      ? _c("ul", { staticClass: "mb-2" }, [
                          _c("hr"),
                          _vm._v(" "),
                          _c(
                            "li",
                            { staticClass: "mb-2 mt-4 mobColor login" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mobColor bottom",
                                  attrs: {
                                    tag: "a",
                                    to: {
                                      name: "auth",
                                      params: { name: "login" }
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-sign-in pr-2",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(" Log In\n              ")
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "li",
                            { staticClass: "mb-2 mobColor" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mobColor bottom",
                                  attrs: {
                                    tag: "a",
                                    to: {
                                      name: "auth",
                                      params: { name: "register" }
                                    }
                                  }
                                },
                                [_vm._v("Join Our BizTribe")]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mobColor bottom",
                                  attrs: { to: "/vendor/auth?type=login" }
                                },
                                [_vm._v("Teach Business")]
                              )
                            ],
                            1
                          )
                        ])
                      : _c("ul", [
                          _c("hr"),
                          _vm._v(" "),
                          _c("li", { staticClass: "mobColor" }, [
                            _c(
                              "div",
                              {
                                staticClass: "listItem logout mobColor",
                                on: {
                                  click: function($event) {
                                    return _vm.logout()
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                Logout\n                "
                                ),
                                _c("i", {
                                  staticClass: "fa fa-sign-out pl-1",
                                  attrs: { "aria-hidden": "true" }
                                })
                              ]
                            )
                          ])
                        ])
                  ])
                ]
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.user !== null && _vm.dontShowNav && _vm.user.school
        ? _c("navigation", { on: { logout: _vm.logout } })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6640caa4", module.exports)
  }
}

/***/ }),

/***/ 933:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(934)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(936)
/* template */
var __vue_template__ = __webpack_require__(937)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-76c7cbe3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userPopupComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-76c7cbe3", Component.options)
  } else {
    hotAPI.reload("data-v-76c7cbe3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 934:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(935);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("d0a6f592", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-76c7cbe3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPopupComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-76c7cbe3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPopupComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 935:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.popButton[data-v-76c7cbe3]{\n    position: absolute;\n    right: 11px;\n    bottom: 0;\n    z-index: 999;\n}\n.mpop[data-v-76c7cbe3]{\n    margin-left: auto;\n    width: 90%;\n}\n.questionCheck[data-v-76c7cbe3] {\n        color: tomato;\n        font-weight: bold;\n        font-size: 14px;\n}\n.imageHolder[data-v-76c7cbe3] {\n        height: 60px;\n        width: 60px\n}\n.imageHolder img[data-v-76c7cbe3]{\n        width:100%;\n        height: 100%;\n}\n.popup-display[data-v-76c7cbe3] {\n       \n        position: absolute;\n        bottom: -55px;\n        width: 100%;\n        background-color: #ffffff;\n        margin-bottom: 0;\n        z-index: 998;\n}\n.fa-user-circle-o[data-v-76c7cbe3] {\n        font-size: 60px;\n        color: #a3c2dc;\n}\n.btn-popup[data-v-76c7cbe3] {\n        background-color: #a3c2dc !important;\n}\n.col-img[data-v-76c7cbe3] {\n        float: left;\n        margin: 13px;\n}\n.popup-inner[data-v-76c7cbe3] {\n        width: 80%;\n        margin-left: auto;\n        margin-right: auto;\n}\n.input-forms[data-v-76c7cbe3] {\n        float: left;\n        width: 400px;\n}\ninput[data-v-76c7cbe3]::-webkit-input-placeholder {\n        color: #c5c5c5 !important;\n}\n.img-test[data-v-76c7cbe3] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        margin-top: 23px;\n        -webkit-box-pack: end;\n            -ms-flex-pack: end;\n                justify-content: flex-end;\n}\n.categoriesdropDown[data-v-76c7cbe3] {\n        width: 30px;\n        -webkit-appearance: none;\n        -moz-appearance: none;\n        appearance: none;\n        background: url(\"http://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png\") white no-repeat 50% !important;\n        border-radius: 0px !important;\n}\n.btn-cont-pop[data-v-76c7cbe3] {\n        position: relative;\n        top: -2px;\n        float:left;\n}\n@media only screen and (max-width:768px){\n.popup-display[data-v-76c7cbe3]{\n\n    height: 80px;\n}\n}\n@media only screen and (max-height:768px){\n.popup-display[data-v-76c7cbe3] {\n\n    position:absolute;\n}\n}\n@media  (max-width:425px){\n.popButton[data-v-76c7cbe3]{\n               display:none;\n}\n.col-xs-1[data-v-76c7cbe3]{\n               padding:15px 5px;\n}\n.popup-display[data-v-76c7cbe3] {\n        display:none;\n        height: 30px;\n        margin-top: 30px;\n}\n.popup-inner[data-v-76c7cbe3] {\n        display:-webkit-box !important;\n        display:-ms-flexbox !important;\n        display:flex !important;\n        width: 100%;\n}\n.fa-user-circle-o[data-v-76c7cbe3] {\n        font-size: 40px;\n        color: #a3c2dc;\n}\n.imageHolder[data-v-76c7cbe3] {\n        height: 40px;\n        width: 40px\n}\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 936:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-popup-component",
    data: function data() {
        return {
            subjectMatters: [],
            token: '',
            authenticate: false,
            emailContent: '',
            categorySend: '',
            emailCheck: false,
            showPop: 'block'

        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.token = user.access_token;
            this.authenticate = true;
        }
        axios.get('/api/subjectmatter').then(function (response) {
            if (response.status === 200) {
                _this.subjectMatters = response.data;
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        togglePop: function togglePop() {

            if (this.showPop === 'block') {
                this.showPop = 'none';
            } else {
                this.showPop = 'block';
            }
            console.log("Data: showPop -> this.showPop", this.showPop);
        },
        sendEmail: function sendEmail() {
            var _this2 = this;

            this.emailCheck = true;
            if (this.authenticate) {
                var data = {
                    subjectMatter: this.categorySend,
                    content: this.emailContent
                };

                axios.post('/api/user/question-send', data, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this2.emailCheck = false;
                        _this2.$notify({
                            group: 'cart',
                            title: 'Success',
                            text: 'Your question has been successfully sent'
                        });
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                this.$notify({
                    group: 'cart',
                    title: 'Unauthenicated',
                    text: 'You must log in to ask question'
                });
            }
        }
    }
});

/***/ }),

/***/ 937:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "popup-display", style: { display: _vm.showPop } },
      [
        _c("div", { staticClass: "row popup-inner" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-9 col-sm-9 col-xs-9" }, [
            _c("div", { staticClass: "mpop" }, [
              _c("div", { staticClass: "img-test" }, [
                _c("div", [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.categorySend,
                          expression: "categorySend"
                        }
                      ],
                      staticClass: "form-control categoriesdropDown",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.categorySend = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _vm._m(3),
                      _vm._v(" "),
                      _vm._l(_vm.subjectMatters, function(
                        subjectMatter,
                        index
                      ) {
                        return _vm.subjectMatters.length > 0
                          ? _c(
                              "option",
                              { domProps: { value: subjectMatter.name } },
                              [_vm._v(_vm._s(subjectMatter.name))]
                            )
                          : _vm._e()
                      })
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "input-forms" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.emailContent,
                        expression: "emailContent"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      placeholder:
                        "Got a Business Related Question? Go on - ask!"
                    },
                    domProps: { value: _vm.emailContent },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.emailContent = $event.target.value
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "btn-cont-pop" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-popup",
                      on: {
                        click: function($event) {
                          return _vm.sendEmail()
                        }
                      }
                    },
                    [_c("i", { staticClass: "fa fa-arrow-right" })]
                  )
                ])
              ]),
              _vm._v(" "),
              _vm.emailCheck
                ? _c("div", { staticClass: "questionCheck" }, [
                    _vm._v("We are sending your question...")
                  ])
                : _vm._e()
            ])
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _vm.showPop === "block"
      ? _c("div", { staticClass: "popButton", on: { click: _vm.togglePop } }, [
          _c("i", {
            staticClass: "fa fa-times-circle",
            attrs: { "aria-hidden": "true" }
          })
        ])
      : _c("div", { staticClass: "popButton", on: { click: _vm.togglePop } }, [
          _c("i", { staticClass: "fas fa-question-circle" })
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-1 col-sm-1 col-xs-1" }, [
      _c("div", { staticClass: "img-1" }, [
        _c("div", { staticClass: "imageHolder" }, [
          _c("img", { attrs: { src: "/images/BizGuruhs-one-bg.png" } })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-1 col-sm-1 col-xs-1" }, [
      _c("div", { staticClass: "img-2" }, [
        _c("div", { staticClass: "imageHolder" }, [
          _c("img", { attrs: { src: "/images/BizGuruhs-two-bg.png" } })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-1 col-sm-1 col-xs-1" }, [
      _c("div", { staticClass: "img-2" }, [
        _c("div", { staticClass: "imageHolder" }, [
          _c("img", { attrs: { src: "/images/BizGuruhs-three-bg.png" } })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("option", [_c("i", { staticClass: "fa fa-caret-down" })])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-76c7cbe3", module.exports)
  }
}

/***/ }),

/***/ 938:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "userStyle" },
    [
      _c(
        "div",
        {
          staticClass: "d-none shadow rounded",
          class: { scrollUp: _vm.scrollUpButton },
          on: { click: _vm.scrollToTop }
        },
        [
          _c("i", {
            staticClass: "fa fa-long-arrow-up",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" "),
          _c("p", { staticClass: "up" }, [_vm._v("UP")])
        ]
      ),
      _vm._v(" "),
      _vm.showIcon
        ? _c(
            "div",
            {
              staticClass: "chatbox ",
              class: { "bg-transparent": !_vm.showChat }
            },
            [
              !_vm.showChat
                ? _c(
                    "span",
                    {
                      staticClass:
                        "chat-box chat bg-main px-2 py-1 cpointer shadow-lg",
                      on: { click: _vm.switchChat }
                    },
                    [
                      _vm.showNotify
                        ? _c(
                            "span",
                            {
                              staticClass:
                                "notify fa-1x fa-stack animated bounceIn infinite slower"
                            },
                            [
                              _c("i", {
                                staticClass:
                                  "fas fa-circle fa-stack-2x text-white",
                                attrs: { "aria-hidden": "true" }
                              }),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "fa-stack-1x text-main" },
                                [_vm._v(_vm._s(_vm.showNotify))]
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c("span", { staticClass: "text text-white" }, [
                        _vm._v("Chat")
                      ])
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.showChat
                ? _c("div", { staticClass: "group_list shadow" }, [
                    _c(
                      "header",
                      { staticClass: "p-2 px-3 btn-compliment text-white" },
                      [
                        _c("p", [_vm._v("Group chat")]),
                        _vm._v(" "),
                        _c("p", [
                          _c("i", {
                            staticClass: "fa fa-minus text-white",
                            attrs: { "aria-hidden": "true" },
                            on: { click: _vm.closeChat }
                          })
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _vm._m(1),
                    _vm._v(" "),
                    _vm.newRealOrder.length
                      ? _c(
                          "ul",
                          _vm._l(_vm.newRealOrder, function(group, idx) {
                            return _c(
                              "li",
                              {
                                key: idx,
                                staticClass: "p-2 px-3",
                                on: {
                                  click: function($event) {
                                    return _vm.goTo(group.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(group.title))]
                            )
                          }),
                          0
                        )
                      : _c("ul", [
                          _c("li", { staticClass: "p-2 px-3 text-center" }, [
                            _c("p", [_vm._v("No Group yet")]),
                            _vm._v(" "),
                            _c("img", {
                              staticClass: "sad",
                              attrs: { src: "/images/sad.svg", alt: "" }
                            }),
                            _vm._v(" "),
                            _c(
                              "p",
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "text-main",
                                    attrs: { to: "/courses" }
                                  },
                                  [_vm._v("Get a course now")]
                                )
                              ],
                              1
                            )
                          ])
                        ])
                  ])
                : _vm._e()
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "hero-content" },
        [
          _c("app-header", {
            attrs: { cartNo: _vm.cartNo, authUs: _vm.authUs, badge: _vm.badge }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("notifications", { attrs: { group: "cart", classes: "my-style" } }),
      _vm._v(" "),
      _c("router-view", {
        attrs: { currentSub: _vm.currentSub, badge: _vm.badge },
        on: {
          "update-cart": function($event) {
            _vm.cartNo = $event
          },
          clearCartCount: function($event) {
            _vm.cartNo = $event
          },
          activity: _vm.updateActivity,
          "top-content": _vm.topContent,
          "top-industry": _vm.topIndustry,
          "add-gender": _vm.addGender,
          "add-location": _vm.addLocation,
          "add-age": _vm.addAge,
          likes: _vm.like,
          reach: _vm.addReach,
          getCartCount: function($event) {
            _vm.cartNo = $event
          },
          getrr: function($event) {
            _vm.cartId = $event
          },
          interaction: _vm.addInteraction,
          sendValue: function($event) {
            _vm.sendPaymentValue = $event
          },
          authenicatedHeader: function($event) {
            _vm.authUs = $event
          },
          notify: _vm.notify,
          getCart: _vm.getCart,
          plusCart: _vm.plusCart,
          removeCart: _vm.removeCart
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", {}, [
      _c("i", { staticClass: "fas fa-comment-alt text-white pr-2" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group p-2" }, [
      _c("i", {
        staticClass: "fa fa-search search",
        attrs: { "aria-hidden": "true" }
      }),
      _vm._v(" "),
      _c("input", {
        staticClass: "form-control rounded-pill",
        attrs: {
          type: "text",
          "aria-describedby": "helpId",
          placeholder: "search"
        }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-689ae71c", module.exports)
  }
}

/***/ }),

/***/ 983:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(984)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(986)
/* template */
var __vue_template__ = __webpack_require__(987)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-59cb5cbe"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/landing/navigation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-59cb5cbe", Component.options)
  } else {
    hotAPI.reload("data-v-59cb5cbe", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 984:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(985);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("5a1416a9", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59cb5cbe\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59cb5cbe\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./navigation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 985:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-59cb5cbe] {\n  z-index: 999;\n  font-family: \"Open Sans\", \"Helvetica\", serif;\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n}\n.prof[data-v-59cb5cbe]{\n  position:relative;\n}\n.prof_hover[data-v-59cb5cbe]{\n  position: absolute;\n  display: none;\n  top:40px;\n  background: #f7f8fa;\n  right:0;\n  z-index: 2;\n  border-radius: 5px;\n  -webkit-box-shadow: 0 0 1px 1px #ccc;\n          box-shadow: 0 0 1px 1px #ccc;\n}\n.prof:hover .prof_hover[data-v-59cb5cbe]{\n  display: block;\n}\n.profile[data-v-59cb5cbe] {\n  background: #f7f8fa;\n  padding: 5px 15px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.nav_item.cpointer[data-v-59cb5cbe]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.image[data-v-59cb5cbe]{\n  font-size:30px;\n  width:30px;\n  height: 30px;\n  border-radius: 50%;\n}\n.button-blue[data-v-59cb5cbe]{\n  font-size:14px;\n}\n.user_icon[data-v-59cb5cbe] {\n  font-size: 22px;\n  margin-right: 10px;\n}\n.logo[data-v-59cb5cbe] {\n  color: #333;\n  font-size: 24px;\n  cursor: pointer;\n  margin-right: 32px;\n}\n.guruh[data-v-59cb5cbe] {\n  color: #a4c2db;\n}\n.auth_buttons[data-v-59cb5cbe] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.scroll_bg[data-v-59cb5cbe] {\n  background: white;\n  -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n          box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n}\n.main nav .top_nav[data-v-59cb5cbe] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 20px 15px;\n}\n.nav_items[data-v-59cb5cbe] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.nav_item[data-v-59cb5cbe] {\n  padding: 1px 15px;\n  font-size: 15px;\n  color: rgba(0, 0, 0, 0.64);\n  margin-left: 15px;\n  font-weight: bold;\n}\n.nav_item[data-v-59cb5cbe]:hover {\n  color: rgba(0, 0, 0, 0.84);\n}\n.sign_in[data-v-59cb5cbe] {\n  color: hsl(207, 43%, 20%);\n  border: 1px solid hsl(207, 43%, 20%);\n  padding: 1px 30px;\n  border-radius: 5px;\n  margin-left: 15px;\n}\n.sign_in[data-v-59cb5cbe]:hover {\n  color: hsl(207, 43%, 10%);\n  border: 1px solid hsl(207, 43%, 10%);\n}\n.join[data-v-59cb5cbe] {\n  background-color: hsl(207, 43%, 20%);\n  color: white;\n  padding: 1px 30px;\n  border-radius: 5px;\n}\n.join[data-v-59cb5cbe]:hover {\n  background-color: hsl(207, 43%, 10%);\n  color: white;\n}\n@media (max-width: 1024px) {\n.desktop[data-v-59cb5cbe] {\n    display: none;\n}\n.auth_buttons[data-v-59cb5cbe] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n  /* .scroll_bg{\n   background-color: rgba(164, 194, 219,.5);\n } */\n.nav_items[data-v-59cb5cbe] {\n    padding: 100px 50px;\n    position: fixed;\n    height:100vh;\n    right: 0;\n    top: 0;\n    z-index: 9;\n}\n.nav_items ul[data-v-59cb5cbe] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.nav_item[data-v-59cb5cbe] {\n    margin-left: 0;\n    margin-bottom: 16px;\n    padding: 0;\n    font-size: 18px;\n    font-weight: normal;\n     display: block !important;\n}\n.join[data-v-59cb5cbe],\n  .sign_in[data-v-59cb5cbe] {\n    padding: 10px 30px;\n    text-align: center;\n}\n.main nav .top_nav[data-v-59cb5cbe] {\n    -webkit-box-align: unset;\n        -ms-flex-align: unset;\n            align-items: unset;\n    -webkit-box-pack: unset;\n        -ms-flex-pack: unset;\n            justify-content: unset;\n}\n.main .mob ul[data-v-59cb5cbe] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.top_n[data-v-59cb5cbe] {\n    margin-bottom: 40px;\n}\n.mobile-nav[data-v-59cb5cbe] {\n    position: absolute;\n    right: 20px;\n    top: 10px;\n    display: block;\n    z-index: 999;\n}\n}\n@media (max-width: 768px) {\n.main-banner[data-v-59cb5cbe] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n}\n.img-phone[data-v-59cb5cbe] {\n    width: 80%;\n}\n.banner-text[data-v-59cb5cbe] {\n    width: 80%;\n}\n.text_1[data-v-59cb5cbe] {\n    font-size: 24px;\n}\n.text_2[data-v-59cb5cbe] {\n    font-size: 18px;\n}\n.hamburger[data-v-59cb5cbe] {\n    padding: 0;\n}\n.main nav .top_nav[data-v-59cb5cbe] {\n    padding: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_userHomePageComponent_vue__ = __webpack_require__(476);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_userHomePageComponent_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__user_userHomePageComponent_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      shadow: false,
      show_nav_bar: false,
      desktop: true,
      auth: false,
      name: "",
      vendor: false,
      account: false,
      user: false,
      logo: ''
    };
  },
  created: function created() {
    this.checkRoute();
    this.setName();
  },

  watch: {
    $route: "checkRoute"
  },
  methods: {
    setName: function setName() {
      var accountUser = JSON.parse(localStorage.getItem("accountUser"));
      var vendorUser = JSON.parse(localStorage.getItem("authVendor"));
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        this.user = true;
      } else {
        this.user = false;
      }
      if (vendorUser !== null) {
        this.vendor = true;
      } else {
        this.vendor = false;
      }

      if (accountUser !== null) {
        this.account = true;
      } else {
        this.account = false;
      }
      if (accountUser !== null || vendorUser !== null || user !== null) {
        this.auth = true;
        if (this.$route.meta.accounting) {
          if (accountUser !== null) {
            this.auth = true;
            this.name = accountUser.name;
            this.logo = accountUser.logo;
          } else {
            this.auth = false;
            this.name = "";
          }
        } else if (this.$route.meta.vendor) {
          if (vendorUser !== null) {

            this.auth = true;
            this.name = vendorUser.storeName;
            this.logo = vendorUser.image;
          } else {
            this.auth = false;
            this.name = "";
          }
        } else if (this.$route.name == "IndexPage") {
          if (user !== null) {
            this.auth = true;
            this.name = user.name;
          } else {
            this.auth = false;
            this.name = "";
          }
        }
      } else {
        this.auth = false;
      }
    },
    gohome: function gohome() {

      this.$router.push("/");
    },
    showNav: function showNav() {
      this.show_nav_bar = !this.show_nav_bar;
    },
    checkRoute: function checkRoute() {
      var _this = this;

      this.setName();
      if (this.$route.name == "Landing") {
        window.addEventListener("scroll", function (e) {

          if (window.scrollY > window.innerHeight * 0.08) {
            _this.shadow = true;
          } else {
            _this.shadow = false;
          }
        });
      } else {
        this.shadow = true;
      }
      if (window.innerWidth <= 1024) {
        this.show_nav_bar = false;
        this.desktop = false;
      } else {
        this.show_nav_bar = true;
        this.desktop = true;
      }
    },
    logOut: function logOut() {
      if (this.$route.meta.accounting) {
        localStorage.removeItem('accountUser');
      } else if (this.$route.meta.vendor) {
        localStorage.removeItem('authVendor');
      }
      this.$router.push('/');
    }
  }
});

/***/ }),

/***/ 987:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main", class: { scroll_bg: _vm.shadow } }, [
    _c("nav", [
      _c("ul", { staticClass: "top_nav" }, [
        _c(
          "div",
          { staticClass: "d-flex justify-content-between align-items-end" },
          [
            _c("div", { staticClass: "logo" }, [
              _c("li", { on: { click: _vm.gohome } }, [
                _vm._v("\n          Biz"),
                _c("span", { class: { guruh: _vm.shadow } }, [_vm._v("Guruh")])
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "dashboard desktop" },
              [
                _vm.$route.meta.vendor
                  ? _c("router-link", { attrs: { to: "/vendor/home" } }, [
                      _c("li", { staticClass: "nav_item m-0" }, [
                        _c("i", {
                          staticClass: "fa fa-home",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v(" My Home\n          ")
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.meta.accounting
                  ? _c("router-link", { attrs: { to: "/account" } }, [
                      _c("li", { staticClass: "nav_item m-0" }, [
                        _c("i", {
                          staticClass: "fa fa-home",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v(" My Home\n          ")
                      ])
                    ])
                  : _vm._e()
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _vm.desktop
          ? _c(
              "div",
              { staticClass: "nav_items" },
              [
                _vm.$route.name == "VendorHome"
                  ? _c("router-link", { attrs: { to: "/vendor/pricing" } }, [
                      _c("li", { staticClass: "nav_item" }, [
                        _c("button", { staticClass: "button-blue" }, [
                          _vm._v("Pricing")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.meta.accounting
                  ? _c("router-link", { attrs: { to: "/account/pricing" } }, [
                      _c("li", { staticClass: "nav_item" }, [
                        _c("button", { staticClass: "button-blue" }, [
                          _vm._v("Pricing")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.name == "Landing"
                  ? _c(
                      "router-link",
                      { attrs: { target: "_blank", to: "/entrepreneur" } },
                      [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm.user && _vm.$route.name == "Landing"
                            ? _c("img", {
                                staticClass:
                                  "animated slideInRight mr-4 infinite",
                                attrs: {
                                  src: "/images/right-arrow.svg",
                                  width: "20",
                                  alt: ""
                                }
                              })
                            : _vm._e(),
                          _vm._v(" For Entrepreneurs\n          ")
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.name == "Landing"
                  ? _c(
                      "router-link",
                      { attrs: { target: "_blank", to: "/account" } },
                      [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm.account && _vm.$route.name == "Landing"
                            ? _c("img", {
                                staticClass:
                                  "animated slideInRight mr-4 infinite",
                                attrs: {
                                  src: "/images/right-arrow.svg",
                                  width: "20",
                                  alt: ""
                                }
                              })
                            : _vm._e(),
                          _vm._v(" For Business Owners\n          ")
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.name == "Landing"
                  ? _c(
                      "router-link",
                      { attrs: { target: "_blank", to: "/vendor/home" } },
                      [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm.vendor && _vm.$route.name == "Landing"
                            ? _c("img", {
                                staticClass:
                                  "animated slideInRight mr-4 infinite",
                                attrs: {
                                  src: "/images/right-arrow.svg",
                                  width: "20",
                                  alt: ""
                                }
                              })
                            : _vm._e(),
                          _vm._v(" For Experts\n          ")
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "dashboard" },
                  [
                    _vm.auth && _vm.$route.name == "VendorHome"
                      ? _c(
                          "router-link",
                          { attrs: { to: "/vendor/dashboard" } },
                          [
                            _c("li", { staticClass: "nav_item" }, [
                              _vm._v("\n            Dashboard\n          ")
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.auth && _vm.$route.meta.accounting
                      ? _c(
                          "router-link",
                          { attrs: { to: "/account/dashboard" } },
                          [
                            _c("li", { staticClass: "nav_item" }, [
                              _vm._v("\n             Dashboard\n          ")
                            ])
                          ]
                        )
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.$route.name == "VendorHome"
                  ? _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.name }
                          }
                        }
                      },
                      [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm._v("How it works")
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.$route.meta.accounting
                  ? _c("router-link", { attrs: { to: "/account/about" } }, [
                      _c("li", { staticClass: "nav_item" }, [
                        _vm._v("How it works")
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("li", { staticClass: "nav_item" }, [
                  _c("div", {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.auth,
                        expression: "auth"
                      }
                    ],
                    attrs: { id: "notification" }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "prof" },
                  [
                    _c("div", { staticClass: "prof_hover p-3" }, [
                      _c("ul", [
                        _c(
                          "li",
                          {
                            staticClass: "nav_item cpointer",
                            on: { click: _vm.logOut }
                          },
                          [_vm._v("\n                Logout\n              ")]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _vm.auth && _vm.$route.meta.accounting
                      ? _c(
                          "router-link",
                          { attrs: { to: "/account/dashboard?goto=profile" } },
                          [
                            _c("li", { staticClass: "nav_item cpointer" }, [
                              _vm.logo !== null
                                ? _c("img", {
                                    staticClass: "image mr-3",
                                    attrs: { src: _vm.logo, alt: "" }
                                  })
                                : _c("i", {
                                    staticClass: "fas fa-user-circle image",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                              _vm._v(
                                "\n      \n          " +
                                  _vm._s(_vm.name) +
                                  "\n        "
                              )
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.auth && _vm.$route.meta.vendor
                      ? _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "vendorProfile",
                                params: {
                                  profile: _vm.name
                                }
                              }
                            }
                          },
                          [
                            _c("li", { staticClass: "nav_item cpointer" }, [
                              _vm.logo !== null
                                ? _c("img", {
                                    staticClass: "image mr-3",
                                    attrs: { src: _vm.logo, alt: "" }
                                  })
                                : _c("i", {
                                    staticClass: "fas fa-user-circle image",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                              _vm._v(
                                "\n          " + _vm._s(_vm.name) + "\n        "
                              )
                            ])
                          ]
                        )
                      : _vm._e()
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.$route.name == "VendorHome"
                  ? _c(
                      "div",
                      { staticClass: "auth_buttons" },
                      [
                        !_vm.auth
                          ? _c(
                              "router-link",
                              { attrs: { to: "/vendor/auth?type=login" } },
                              [
                                _c("li", { staticClass: "nav_item sign_in" }, [
                                  _vm._v("Sign in")
                                ])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        !_vm.auth
                          ? _c(
                              "router-link",
                              { attrs: { to: "/vendor/auth?type=register" } },
                              [
                                _c("li", { staticClass: "nav_item join" }, [
                                  _vm._v("Join today")
                                ])
                              ]
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  : _vm.$route.meta.accounting
                  ? _c(
                      "div",
                      { staticClass: "auth_buttons" },
                      [
                        !_vm.auth
                          ? _c(
                              "router-link",
                              { attrs: { to: "/account/auth?type=login" } },
                              [
                                _c("li", { staticClass: "nav_item sign_in" }, [
                                  _vm._v("Sign in")
                                ])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        !_vm.auth
                          ? _c(
                              "router-link",
                              { attrs: { to: "/account/auth?type=register" } },
                              [
                                _c("li", { staticClass: "nav_item join" }, [
                                  _vm._v("Join today")
                                ])
                              ]
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            )
          : _vm._e(),
        _vm._v(" "),
        !_vm.desktop
          ? _c(
              "span",
              {
                staticClass:
                  "d-flex justify-content-start align-items-center mobile-nav",
                on: { click: _vm.showNav }
              },
              [
                _c(
                  "button",
                  {
                    staticClass: "hamburger hamburger--collapse",
                    class: { "is-active": _vm.show_nav_bar },
                    attrs: {
                      tabindex: "0",
                      "aria-label": "Menu",
                      role: "button",
                      "aria-controls": "navigation",
                      type: "button"
                    }
                  },
                  [_vm._m(0)]
                )
              ]
            )
          : _vm._e()
      ])
    ]),
    _vm._v(" "),
    _vm.show_nav_bar && !_vm.desktop
      ? _c(
          "nav",
          { staticClass: "nav_items bg-white mob animated slideInRight" },
          [
            _c("ul", [
              _c(
                "span",
                { staticClass: "top_n" },
                [
                  _vm.$route.name == "VendorHome"
                    ? _c("router-link", { attrs: { to: "/vendor/pricing" } }, [
                        _c("li", { staticClass: "nav_item" }, [
                          _c("button", { staticClass: "button-blue" }, [
                            _vm._v("Upgrade")
                          ])
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.auth && _vm.$route.meta.accounting
                    ? _c(
                        "router-link",
                        { attrs: { to: "/account/dashboard?goto=profile" } },
                        [
                          _vm.auth && _vm.$route.meta.accounting
                            ? _c("li", { staticClass: "nav_item cpointer" }, [
                                _c("i", {
                                  staticClass: "fas fa-user-circle mr-2",
                                  attrs: { "aria-hidden": "true" }
                                }),
                                _vm._v(" " + _vm._s(_vm.name) + "\n        ")
                              ])
                            : _vm._e()
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.auth && _vm.$route.meta.vendor
                    ? _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "vendorProfile",
                              params: {
                                profile: _vm.name
                              }
                            }
                          }
                        },
                        [
                          _c("li", { staticClass: "nav_item cpointer" }, [
                            _c("i", {
                              staticClass: "fas fa-user-circle fa-2x",
                              attrs: { "aria-hidden": "true" }
                            }),
                            _vm._v(
                              "\n          " + _vm._s(_vm.name) + "\n        "
                            )
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.meta.accounting
                    ? _c("router-link", { attrs: { to: "/account/pricing" } }, [
                        _c("li", { staticClass: "nav_item" }, [
                          _c("button", { staticClass: "button-blue" }, [
                            _vm._v("Upgrade")
                          ])
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("router-link", { attrs: { target: "_blank", to: "/" } }, [
                    _c("li", { staticClass: "nav_item" }, [_vm._v("Home")])
                  ]),
                  _vm._v(" "),
                  _vm.$route.name == "Landing"
                    ? _c(
                        "router-link",
                        { attrs: { target: "_blank", to: "/entrepreneur" } },
                        [
                          _c("li", { staticClass: "nav_item" }, [
                            _vm._v("For Entrepreneurs")
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.name == "Landing"
                    ? _c(
                        "router-link",
                        { attrs: { target: "_blank", to: "/account" } },
                        [
                          _c("li", { staticClass: "nav_item" }, [
                            _vm._v("For Business Owners")
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.name == "Landing"
                    ? _c(
                        "router-link",
                        {
                          staticClass: "mb-5",
                          attrs: { target: "_blank", to: "/vendor/home" }
                        },
                        [
                          _c("li", { staticClass: "nav_item" }, [
                            _vm._v("For Experts")
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.auth && _vm.$route.name == "VendorHome"
                    ? _c(
                        "router-link",
                        { attrs: { to: "/vendor/dashboard" } },
                        [
                          _c("li", { staticClass: "nav_item" }, [
                            _vm._v("\n             Dashboard\n          ")
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.name == "VendorHome"
                    ? _c("router-link", { attrs: { to: "/vendor/about" } }, [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm._v("How it works")
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.auth && _vm.$route.meta.accounting
                    ? _c(
                        "router-link",
                        { attrs: { to: "/account/dashboard" } },
                        [
                          _c("li", { staticClass: "nav_item" }, [
                            _vm._v("\n           Dashboard\n          ")
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.meta.accounting
                    ? _c("router-link", { attrs: { to: "/account/about" } }, [
                        _c("li", { staticClass: "nav_item" }, [
                          _vm._v("How it works")
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("li", { staticClass: "nav_item" }, [
                    _c("div", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.auth,
                          expression: "auth"
                        }
                      ],
                      attrs: { id: "notification" }
                    })
                  ]),
                  _vm._v(" "),
                  _vm.auth
                    ? _c(
                        "li",
                        {
                          staticClass: "nav_item cpointer",
                          on: { click: _vm.logOut }
                        },
                        [_vm._v("\n                Logout\n              ")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$route.name == "VendorHome"
                    ? _c(
                        "div",
                        { staticClass: "auth_buttons" },
                        [
                          !_vm.auth
                            ? _c(
                                "router-link",
                                { attrs: { to: "/vendor/auth?type=login" } },
                                [
                                  _c(
                                    "li",
                                    { staticClass: "nav_item sign_in" },
                                    [_vm._v("Sign in")]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.auth
                            ? _c(
                                "router-link",
                                { attrs: { to: "/vendor/auth?type=register" } },
                                [
                                  _c("li", { staticClass: "nav_item join" }, [
                                    _vm._v("Join today")
                                  ])
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    : _vm.$route.meta.accounting
                    ? _c(
                        "div",
                        { staticClass: "auth_buttons" },
                        [
                          !_vm.auth
                            ? _c(
                                "router-link",
                                { attrs: { to: "/account/auth?type=login" } },
                                [
                                  _c(
                                    "li",
                                    { staticClass: "nav_item sign_in" },
                                    [_vm._v("Sign in")]
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          !_vm.auth
                            ? _c(
                                "router-link",
                                {
                                  attrs: { to: "/account/auth?type=register" }
                                },
                                [
                                  _c("li", { staticClass: "nav_item join" }, [
                                    _vm._v("Join today")
                                  ])
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ])
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "hamburger-box" }, [
      _c("span", { staticClass: "hamburger-inner" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-59cb5cbe", module.exports)
  }
}

/***/ })

});