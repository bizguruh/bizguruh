webpackJsonp([133],{

/***/ 1617:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1618);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7cb58d6e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a8f2646c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privacyPolicyComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a8f2646c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privacyPolicyComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1618:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bread ul[data-v-a8f2646c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n   margin: 10px 0px 30px;\n    padding:5px 15px;\n    background:#fff;\n}\n.bread ul > li[data-v-a8f2646c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bread ul > li[data-v-a8f2646c]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-a8f2646c] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.container[data-v-a8f2646c]{\n    padding-top:80px;\n    padding-bottom:90px;\n    line-height:1.8;\n    font-size:16px;\n}\ndiv ol[data-v-a8f2646c]{\n    list-style: circle;\n}\nh3[data-v-a8f2646c]{\n    margin:5px 0;\n}\nul[data-v-a8f2646c]{\n    list-style: disc;\n}\n.boldText[data-v-a8f2646c]{\n    font-weight:bold;\n    font-size:16px;\n    margin-bottom: 3px;\n}\na[data-v-a8f2646c]{\n    color:#a3c2dc !important;\n}\n.boldText[data-v-a8f2646c]{\n   color: rgba(0, 0,0 , 0.64);\n}\n.uline[data-v-a8f2646c]{\n    text-decoration:underline;\n}\n.tCenter[data-v-a8f2646c]{\n    margin: 10px auto;\n  text-align: center;\n}\n.pryColor[data-v-a8f2646c]{\n    color: #a3c2dc;\n    font-weight: bold;\n}\n@media(max-width:425px){\nh2[data-v-a8f2646c]{\n        font-size: 18px;\n}\nh3[data-v-a8f2646c]{\n        font-size: 16px;\n}\np[data-v-a8f2646c]{\n        font-size: 16px;\n}\n.boldText[data-v-a8f2646c]{\n        font-size:16px;\n}\nol ul[data-v-a8f2646c]{\n        margin-bottom: 10px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1619:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "privacy-policy-component",
    data: function data() {
        return {
            breadcrumbList: []
        };
    },
    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        }
    }
});

/***/ }),

/***/ 1620:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "bread" }, [
      _c(
        "ul",
        _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
          return _c(
            "li",
            {
              key: index,
              class: { linked: !!breadcrumb.link },
              on: {
                click: function($event) {
                  return _vm.routeTo(index)
                }
              }
            },
            [
              _vm._v(
                "\n               " +
                  _vm._s(breadcrumb.name) +
                  "\n             "
              )
            ]
          )
        }),
        0
      )
    ]),
    _vm._v(" "),
    _c("div", {}, [
      _c("h2", { staticClass: "tCenter uline mb-4" }, [
        _vm._v(" PRIVACY POLICY")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    Your privacy is important to Bizguruh and always has been. So we've developed a Privacy Policy that covers how we collect, use, disclose, transfer, and store your information. Please take a moment to familiarize yourself with our privacy practices and let us know if you have any questions.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " By visiting Bizguruh, you are accepting the practices described in this Privacy Notice."
        )
      ]),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [_vm._v("OVERVIEW")]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("1.\tWhat kinds of information do we collect? ")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "To provide the Bizguruh Services, we must process information about you. The type of information that we collect depends on how you use our Services."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("Things that you and others do and provide:")
      ]),
      _vm._v(" "),
      _vm._m(1),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("2.\tHow do we use this information?")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We use the information we have to deliver our Services, including to personalise features and make suggestions for you (such as groups or  events you may be interested in or topics you may want to follow) on and off our Products. To create personalised Services that are unique and relevant to you, we use your connections, preferences, interests and activities based on the data that we collect and learn from you and others (including any data with special protection you choose to provide); how you use and interact with our Products; and the people, places or things that you're connected to and interested in on and off our Products."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("Product research and development:")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " We use the information we have to develop, test and improve our Services, including by conducting surveys and research, and testing and troubleshooting new products and features."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [_vm._v("Communicate with you.")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We use the information that we have to communicate with you about our Services and let you know about our Policies and Terms. We also use your information to respond to you when you contact us."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("Research and innovate for social good.")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We use the information that we have (including from research partners who we collaborate with) to conduct and support research and innovation on business education concepts, insights and technological advancement in business activities within African markets."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("3.\tHow is this information shared? ")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v("Your information is shared with others in the following ways:")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("People and accounts that you share and communicate with: ")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "When, you use Bizguruh to communicate with people or businesses, those people and businesses can see the content you send. Your network can also see actions that you have taken on our Products, including engagement with ads and sponsored content. We also let other accounts see who has viewed Bizguruh Stories. "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\nPublic information can be seen by anyone, on or off our Services, including if they don't have an account. This includes your Bizguruh username, any information you share with a public audience, information in your public profile on Bizguruh, and content you share on any other public forum relating to Bizguruh. You, other people using Bizguruh and we can provide access to or send public information to anyone on or off our Services, in search results or through tools and APIs. Public information can also be seen, accessed, reshared or downloaded through third-party services such as search engines, APIs and offline media such as TV, and by apps, websites and other services that integrate with our Products.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v(
          "\n    Information about your active status or presence on our Platform:\n"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "People in your networks can see signals telling them whether you are active on our platform, or when you last used our Services.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v(
          "Apps, websites and third-party integrations on or using our Services."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "When you choose to use third-party apps, websites or other services that use, or are integrated with, our Products, they can receive information about what you post or share. Information collected by these third-party services is subject to their own terms and policies, not this one.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("Sharing with third-party partners")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We work with third-party partners who help us provide and improve our Services or who use Bizguruh Business Tools to grow their businesses, which makes it possible to operate our companies and provide free services to people around the world. We don't sell any of your information to anyone and we never will. We also impose strict restrictions on how our partners can use and disclose the data we provide. Here are the types of third parties that we share information with:\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("\n    Partners who use our analytics services.\n")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We provide aggregated statistics and insights that help people and businesses understand how people are engaging with their posts, listings, Pages, videos and other content on and off the Bizguruh Platform. For example, Page admins and Bizguruh business profiles receive information about the number of people or accounts who viewed, reacted to or commented on their posts, as well as aggregate demographic and other information that helps them understand interactions with their Page or account.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [_vm._v("\n    Advertisers.\n")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We provide advertisers with reports about the kinds of people seeing their ads and how their ads are performing, but we don't share information that personally identifies you (information such as your name or email address that by itself can be used to contact you or identifies who you are) unless you give us permission. For example, we provide general demographic and interest information to advertisers (for example, that an ad was seen by a woman between the ages of 25 and 34 who lives in Ghana and likes software engineering) to help them better understand their audience. We also confirm which Bizguruh ads led you to make a purchase or take an action with an advertiser.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("\n    Device and Connection Information.\n")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We collect device-specific information when you install, access, or use our Services. This includes information such as hardware model, operating system information, browser information, IP address, mobile network information including phone number, and device identifiers. We collect device location information if you use our location features, such as when you choose to share your location with your contacts, view locations nearby or those others have shared with you, and the like, and for diagnostics and troubleshooting purposes such as if you are having trouble with our app’s location features.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("\n    Researchers and academics.\n")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We also provide information and content to research partners and academics to conduct research that advances scholarship and innovation that supports our business or mission and enhances discovery and innovation on business education, African economies and technological advancement in business activities within African markets.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v("\n    Law enforcement or legal requests.\n")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We share information with law enforcement or in response to legal requests.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("4.\tHow do we protect your personal data?")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Bizguruh takes the security of your personal data very seriously. We make every effort to protect your personal data from misuse, interference, loss, unauthorised access, modification or disclosure."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Our measures include implementing appropriate access controls, investing in the latest Information Security Capabilities to protect the IT environments we leverage, and ensuring we encrypt, pseudonymise and anonymise personal data wherever possible."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Access to your personal data is only permitted among our employees and agents on a need-to-know basis and subject to strict contractual confidentiality obligations when processed by third-parties."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [_vm._v("5.\tWhat are your rights?")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Your rights in relation to your personal data and how it is processed."
        )
      ]),
      _vm._v(" "),
      _c(
        "p",
        [
          _vm._v(
            "Where we process your personal data, you have a number of rights over how the data is processed and can exercise these rights at any point. We have provided an overview of these rights below together with what this entails for you. You can exercise your rights by sending an email or submitting a request through the\n    “ "
          ),
          _c("router-link", { attrs: { to: "/contact" } }, [
            _vm._v("Contact Us")
          ]),
          _vm._v("” form on our websites.")
        ],
        1
      ),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("6.\tHow can I manage or delete information about me?")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We provide you with the ability to access, rectify, port and delete your data."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We store data until it is no longer necessary to provide our services or until your account is deleted – whichever comes first. This is a case-by-case determination that depends on things such as the nature of the data, why it is collected and processed, and relevant legal or operational retention needs. For example, when you search for something on Bizguruh, you can access and delete that query from within your search history at any time, but the log of that search is deleted after six months. If you submit a copy of your valid photo ID for account verification purposes, we delete that copy 30 days after submission."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    When you delete your account, we delete things that you have posted, and you won't be able to recover this information later. Information that others have shared about you isn't part of your account and won't be deleted. If you don't want to delete your account but want to temporarily stop using the Products, you can deactivate your account instead. To delete your account at any time, please visit the Bizguruh settings.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [_vm._v("7.\tLaw And Protection")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    We may collect, use, preserve, and share your information if we have a good-faith belief that it is reasonably necessary to: (a) respond pursuant to applicable law or regulations, to legal process, or to government requests; (b) enforce our Terms and any other applicable terms and policies, including for investigations of potential violations; (c) detect, investigate, prevent, and address fraud and other illegal activity, security, or technical issues; or (d) protect the rights, property, and safety of our users, or others.\n"
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", { staticClass: "mb-2" }, [
        _vm._v("8.\tHow will we notify you of changes to this Policy?")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We'll notify you before we make changes to this Policy and give you the opportunity to review the revised Policy before you choose to continue using our Products."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("p", { staticClass: "pryColor" }, [_vm._v("Contact Us")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If you have questions about our Privacy Policy, please contact us via our Message Centre or,"
        )
      ]),
      _vm._v(" "),
      _vm._m(3)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-2" }, [
      _c("ol", [
        _c("li", [_vm._v("What kinds of information do we collect?")]),
        _vm._v(" "),
        _c("li", [_vm._v("How do we use this information?")]),
        _vm._v(" "),
        _c("li", [_vm._v("How is this information shared?")]),
        _vm._v(" "),
        _c("li", [_vm._v("\tHow do we protect your personal data?")]),
        _vm._v(" "),
        _c("li", [_vm._v("\tWhat are your rights?")]),
        _vm._v(" "),
        _c("li", [
          _vm._v("\tHow can I manage or delete information about me? ")
        ]),
        _vm._v(" "),
        _c("li", [_vm._v("Law And Protection")]),
        _vm._v(" "),
        _c("li", [_vm._v("How will we notify you of changes to this Policy?")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _c("span", { staticClass: "boldText" }, [
          _vm._v(" Information and content you provide. ")
        ]),
        _vm._v(
          " We collect the content, communications and other information you provide when you use our Services, including when you sign up for an account, create or share content and message or communicate with others. "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("Networks and connections.")
        ]),
        _vm._v(
          " We collect information about the people, pages accounts, groups that you are connected to and how you interact with them across our Products, such as people you communicate with the most or groups that you are part of.\n"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("span", { staticClass: "boldText" }, [_vm._v("Your usage.")]),
        _vm._v(
          " We collect information about how you use our Services, such as the types of content that you view or engage with, the features you use, the actions you take, the people or accounts you interact with and the time, frequency and duration of your activities. For example, we log when you're using and have last used our Products, and what posts, videos and other content you view on our Products.\n"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("span", { staticClass: "boldText" }, [
          _vm._v(" Information about transactions made on our Products.")
        ]),
        _vm._v(
          " If you use our business education resources or harness the business development Chatbot or engage in any of the interactive forum provided on our platform, we collect information about the transaction.\n\n"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          "\tThe right to be informed. You have the right to be provided with clear, transparent and easily understandable information about how we use your personal data and your rights. Therefore, we’re providing you with the information in this Notice."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\tThe right to access and rectification. You have the right to access, correct or update your personal data at any time. We understand the importance of this and should you want to exercise your rights, please contact us."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\tThe right to data portability. The personal data you have provided us with is portable. This means it can be moved, copied or transmitted electronically under certain circumstances."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\tThe right to be forgotten. Under certain circumstances, you have right to request that we delete your data. If you wish to delete the personal data we hold about you, please let us know and we will take reasonable steps to respond to your request in accordance with legal requirements. If the personal data we collect is no longer needed for any purposes and we are not required by law to retain it, we will do what we can to delete, destroy or permanently de-identify it."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\tThe right to object. Under certain circumstances, you have the right to object to certain types of processing, including processing for direct marketing (i.e., receiving emails from us notifying you or being contacted with varying potential opportunities)."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\n    \tThe right to lodge a complaint with a Supervisory Authority. You have the right to lodge a complaint directly with any local Supervisory Authority about how we process your personal data.\n"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\n     \tThe right to withdraw consent. If you have given your consent to anything we do with your personal data (i.e., we rely on consent as a legal basis for processing your personal data), you have the right to withdraw your consent at any time (although if you do so, it does not mean that anything we have done with your personal data with your consent up to that point is unlawful). You can withdraw your consent to the processing of your personal data at any time by contacting us with the details provided below.\n "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "\n    \tRights related to automated decision-making. You have the right not to be subject to a decision which is based solely on automated processing and which produces legal or other significant effects on you. In particular, you have the right:\n"
        )
      ]),
      _vm._v(" "),
      _c("ul", [
        _c("li", [_vm._v("    \tto obtain human intervention;")]),
        _vm._v(" "),
        _c("li", [_vm._v("\tto express your point of view;")]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "\tto obtain an explanation of the decision reached after an assessment; and to challenge such a decision."
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "line-height": ".8" } }, [
      _c("p", [_vm._v("Coeur Consulting Options Limited.")]),
      _vm._v(" "),
      _c("p", [_vm._v("Privacy Policy")]),
      _vm._v(" "),
      _c("p", [_vm._v("Captain House, 34 Aje Road,")]),
      _vm._v(" "),
      _c("p", [_vm._v("Sabo Yaba, Lagos,")]),
      _vm._v(" "),
      _c("p", [_vm._v("Nigeria.")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a8f2646c", module.exports)
  }
}

/***/ }),

/***/ 602:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1617)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1619)
/* template */
var __vue_template__ = __webpack_require__(1620)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a8f2646c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/privacyPolicyComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a8f2646c", Component.options)
  } else {
    hotAPI.reload("data-v-a8f2646c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});