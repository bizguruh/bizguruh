webpackJsonp([34],{

/***/ 1527:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1528);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cb63b6ee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e196910\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminWhitePaperComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e196910\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminWhitePaperComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1528:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.market[data-v-5e196910]{\n  background: rgba(163, 194, 220, 0.1) ;\n}\n.container[data-v-5e196910]{\n     padding: 60px 10px 120px;\n        background: #fff;\n     max-width: 900px;\n   \n    font-weight: bold;\n}\n.market_img[data-v-5e196910]{\n    position: relative;\n    width:100%;\n    height: 300px;\n    min-height: 300px;\n    margin-top:50px\n}\n.market_img img[data-v-5e196910]{\n    width:100%;\n    height: 100%;\n}\n.text[data-v-5e196910]{\n    position: absolute;\n    top: 30%;\n    left: 10px;\n    font-size: 40px;\n    color: #000;\n}\n@media(max-width: 425px){\n.container[data-v-5e196910]{\n    padding: 31px 0px;\n}\n.market_img[data-v-5e196910]{\n         /* display: none; */\n         height: 150px;\n}\n.text[data-v-5e196910]{\n            font-size:20px;\n}\n}\n.catBg[data-v-5e196910] {\n        background: linear-gradient(110deg,#e9eeef,#fff 33%);\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent__ = __webpack_require__(955);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userAdminPaperContentComponent__ = __webpack_require__(1530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userAdminPaperContentComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_userAdminPaperContentComponent__);
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-admin-white-paper-component",
    data: function data() {
        return {
            cartNo: '',
            cartId: ''
        };
    },
    components: {
        'app-side-category': __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent___default.a,
        'app-paper-content': __WEBPACK_IMPORTED_MODULE_1__components_userAdminPaperContentComponent___default.a
    },
    mounted: function mounted() {},

    methods: {
        addToCart: function addToCart() {
            this.$emit('update-cart', this.cartNo);
        }
    }
});

/***/ }),

/***/ 1530:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1531)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1533)
/* template */
var __vue_template__ = __webpack_require__(1534)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1ec67153"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userAdminPaperContentComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1ec67153", Component.options)
  } else {
    hotAPI.reload("data-v-1ec67153", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1531:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1532);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("54945d78", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1ec67153\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminPaperContentComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1ec67153\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAdminPaperContentComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1532:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:700);", ""]);

// module
exports.push([module.i, "\n.banner[data-v-1ec67153] {\n  width: 100%;\n  height: 420px;\n  display: grid;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background-image: url(\"/images/gears.jpg\");\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.banner-text[data-v-1ec67153] {\n  color: white;\n  font-size: 56px;\n  font-weight: bold;\n}\n.main-cont[data-v-1ec67153] {\n  min-height: 100vh;\n  overflow: auto;\n}\n.form-control[data-v-1ec67153] {\n  border-radius: 20px;\n}\n.rowBa[data-v-1ec67153] {\n   max-width: 1100px;\n    margin-left: auto;\n    margin-right: auto;\n    margin-top: -100px;\n    padding: 50px;\n    padding-top: 50px;\n    min-height: 70vh;\n    padding-bottom: 100px;\n    background-color: white;\n     overflow: auto;\n}\n.rowBanner[data-v-1ec67153] {\n  margin: 40px 0;\n}\n@media (max-width: 768px) {\n.banner[data-v-1ec67153] {\n    height: 350px;\n}\n.banner-text[data-v-1ec67153] {\n    font-size: 36px;\n}\n.fa-1x[data-v-1ec67153] {\n    font-size: 0.8em;\n    color: #b5b5b5;\n}\n}\n@media (max-width: 575px) {\n.search[data-v-1ec67153] {\n    padding-left: 0;\n}\n.rowBanner[data-v-1ec67153] {\n    margin: 20px 0;\n}\n.rowcontainer[data-v-1ec67153] {\n    padding-top: 0;\n}\n.banner[data-v-1ec67153] {\n    height: 250px;\n}\n.banner-text[data-v-1ec67153] {\n    font-size: 24px;\n}\n}\nul[data-v-1ec67153] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  list-style-type: none;\n  margin: 0;\n  padding: 5px 0;\n  background: #ffffff;\n}\nul > li[data-v-1ec67153] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  float: left;\n  height: 10px;\n  width: auto;\n  font-weight: bold;\n  font-size: 0.8em;\n  cursor: default;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul > li[data-v-1ec67153]:not(:last-child)::after {\n  content: \"/\";\n  float: right;\n  font-size: 0.8em;\n  margin: 0 0.5em;\n  cursor: default;\n}\n.linked[data-v-1ec67153] {\n  cursor: pointer;\n  font-size: 1em;\n  font-weight: normal;\n}\n.noResult[data-v-1ec67153] {\n  font-family: fantasy;\n  text-align: center;\n  margin: auto;\n}\n.ribbon[data-v-1ec67153] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  position: absolute;\n}\n.ribbon[data-v-1ec67153]::before,\n.ribbon[data-v-1ec67153]::after {\n  position: absolute;\n  z-index: -1;\n  content: \"\";\n  display: block;\n  border: 5px solid #2980b9;\n}\n.ribbon span[data-v-1ec67153] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 4px 0;\n  background-color: #3498db;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 700 14px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n.ribbon-free span[data-v-1ec67153] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 4px 0;\n  background-color: #404346;\n  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  color: #fff;\n  font: 500 14px/1 \"Lato\", sans-serif;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n\n/* top left*/\n.ribbon-top-left[data-v-1ec67153] {\n  top: -0px;\n}\n.ribbon-top-left[data-v-1ec67153]::before,\n.ribbon-top-left[data-v-1ec67153]::after {\n  border-top-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-top-left[data-v-1ec67153]::before {\n  top: 0;\n  right: 0;\n}\n.ribbon-top-left[data-v-1ec67153]::after {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-top-left span[data-v-1ec67153] {\n  right: 9px;\n  top: 13px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n/* top right*/\n.ribbon-top-right[data-v-1ec67153] {\n  top: -10px;\n  right: -10px;\n}\n.ribbon-top-right[data-v-1ec67153]::before,\n.ribbon-top-right[data-v-1ec67153]::after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-top-right[data-v-1ec67153]::before {\n  top: 0;\n  left: 0;\n}\n.ribbon-top-right[data-v-1ec67153]::after {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-top-right span[data-v-1ec67153] {\n  left: -7px;\n  top: 22px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n/* bottom left*/\n.ribbon-bottom-left[data-v-1ec67153] {\n  bottom: -10px;\n  left: -10px;\n}\n.ribbon-bottom-left[data-v-1ec67153]::before,\n.ribbon-bottom-left[data-v-1ec67153]::after {\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-1ec67153]::before {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-bottom-left[data-v-1ec67153]::after {\n  top: 0;\n  left: 0;\n}\n.ribbon-bottom-left span[data-v-1ec67153] {\n  right: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(225deg);\n          transform: rotate(225deg);\n}\n.smatter[data-v-1ec67153] {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: bold;\n  color: rgba(0, 0, 0, 0.54);\n  height: 18px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n/* bottom right*/\n.ribbon-bottom-right[data-v-1ec67153] {\n  bottom: -10px;\n  right: -10px;\n}\n.ribbon-bottom-right[data-v-1ec67153]::before,\n.ribbon-bottom-right[data-v-1ec67153]::after {\n  border-bottom-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-1ec67153]::before {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-bottom-right[data-v-1ec67153]::after {\n  top: 0;\n  right: 0;\n}\n.ribbon-bottom-right span[data-v-1ec67153] {\n  left: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(-225deg);\n          transform: rotate(-225deg);\n}\n.rowBanner[data-v-1ec67153] {\n  margin-left: auto;\n}\n.markSearch[data-v-1ec67153],\n.tagSearch[data-v-1ec67153] {\n  padding: 1px 12px;\n}\n.searchBarContent[data-v-1ec67153] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n.searchBar[data-v-1ec67153] {\n  width: auto;\n  margin-left: auto;\n  margin-right: auto;\n}\ninput[data-v-1ec67153]::-webkit-input-placeholder {\n  color: #c5c5c5 !important;\n}\n.search-btn[data-v-1ec67153] {\n  border: 0px;\n  color: rgba(0, 0, 0, 0.54);\n  margin: 7px 0;\n  position: absolute;\n  right: 4px;\n  top: 4px;\n}\na button.btn.btn-sm[data-v-1ec67153] {\n  background: #5b84a7 !important;\n  text-transform: capitalize;\n  color: #fff !important;\n  font-size: 14px;\n  padding: 5px 35px;\n}\nbutton.btn.btn-sm[data-v-1ec67153] {\n  background: #5b84a7 !important;\n  text-transform: capitalize;\n  color: #fff !important;\n}\na button.btn.btn-sm[data-v-1ec67153]:hover {\n  background: rgba(164, 194, 219, 0.64) !important;\n  color: #fff !important;\n  border: none !important;\n}\n.paperTitle[data-v-1ec67153] {\n  color: #5b84a7;\n  line-height: 1.2;\n  margin-bottom: 6px;\n}\n.mobile[data-v-1ec67153] {\n  display: none;\n}\n.btn-send-to-library[data-v-1ec67153] {\n  background: #5b84a7 !important;\n}\n.side-buttons[data-v-1ec67153] {\n  text-align: right;\n}\n.formatName[data-v-1ec67153] {\n  font-weight: bold;\n  font-size: 12px;\n}\n.formatStyle[data-v-1ec67153] {\n  float: left;\n  margin-right: 10px;\n}\n.vieww[data-v-1ec67153] {\n  margin-bottom: 10px;\n}\n.productsPagePrice[data-v-1ec67153] {\n  border: 1px solid #5b84a7;\n  padding: 10px;\n  cursor: pointer;\n}\n.productsPrice[data-v-1ec67153] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.datePaper[data-v-1ec67153] {\n  color: #cccccc;\n}\n.smatter[data-v-1ec67153] {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: bold;\n  color: rgba(0, 0, 0, 0.54);\n  height: 18px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.paper[data-v-1ec67153] {\n  height: 150px;\n}\n.paperTitle[data-v-1ec67153] {\n  text-transform: uppercase;\n  font-weight: bold;\n}\n.viewDetail[data-v-1ec67153] {\n  padding: 6px;\n  border: 1px solid #ffffff;\n  border-radius: 5px;\n  background: #5b84a7;\n  text-align: center;\n  cursor: pointer;\n}\n.viewDetailCart[data-v-1ec67153] {\n  background-color: #5b84a7 !important;\n  color: #ffffff;\n  /* padding: 6px 52px; */\n  border-radius: 4px;\n  text-align: center;\n  cursor: pointer;\n  border: 1px solid #5b84a7;\n}\n.img_cover[data-v-1ec67153] {\n  /* min-width: 150px; */\n  overflow: hidden;\n  background-position: center;\n  background-size: cover;\n  padding: 0 15px;\n}\nimg[data-v-1ec67153] {\n  width: 100%;\n  height: auto;\n  /* box-shadow: 0 1px 2px 0 hsla(0, 0%, 0%, 0.2); */\n}\n.paperDiv[data-v-1ec67153] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  padding: 15px 0;\n  background: #ffffff;\n  border-radius: 5px;\n}\n.whitePP[data-v-1ec67153] {\n  /* width: 600px; */\n  font-weight: normal;\n  height: 50px;\n  white-space: initial;\n  line-height: 1.6;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  color: rgba(0, 0, 0, 0.54);\n  margin-bottom: 6px;\n}\n.modal-mask[data-v-1ec67153] {\n  position: fixed;\n  z-index: 9998;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.5);\n  display: table;\n  -webkit-transition: opacity 0.3s ease;\n  transition: opacity 0.3s ease;\n}\n.modal-wrapper[data-v-1ec67153] {\n  display: table-cell;\n  vertical-align: middle;\n}\n.modal-container[data-v-1ec67153] {\n  width: 300px;\n  margin: 0px auto;\n  padding: 20px 30px;\n  background-color: #fff;\n  border-radius: 2px;\n  -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n          box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n  -webkit-transition: all 0.3s ease;\n  transition: all 0.3s ease;\n  font-family: Helvetica, Arial, sans-serif;\n}\n.modal-header h3[data-v-1ec67153] {\n  margin-top: 0;\n  color: #42b983;\n}\n.modal-body[data-v-1ec67153] {\n  margin: 20px 0;\n}\n.modal-default-button[data-v-1ec67153] {\n  float: right;\n}\n\n/*\n     * The following styles are auto-applied to elements with\n     * transition=\"modal\" when their visibility is toggled\n     * by Vue.js.\n     *\n     * You can easily play with the modal transition by editing\n     * these styles.\n     */\n.modal-enter[data-v-1ec67153] {\n  opacity: 0;\n}\n.modal-leave-active[data-v-1ec67153] {\n  opacity: 0;\n}\n.modal-enter .modal-container[data-v-1ec67153],\n.modal-leave-active .modal-container[data-v-1ec67153] {\n  -webkit-transform: scale(1.1);\n  transform: scale(1.1);\n}\n@media (max-width: 768px) {\n.paper[data-v-1ec67153] {\n    font-size: 14px;\n    /* padding: 0; */\n}\n.img_cover[data-v-1ec67153] {\n    padding: 10px;\n}\n.viewDetail[data-v-1ec67153] {\n    font-size: 11px;\n}\n.viewDetailCart[data-v-1ec67153] {\n    font-size: 11px;\n}\n.btn-send-to-library[data-v-1ec67153] {\n    font-size: 812x;\n}\n}\n@media (max-width: 425px) {\n.markSearch[data-v-1ec67153],\n  .tagSearch[data-v-1ec67153] {\n    padding: 1px 5px;\n}\n.form-control[data-v-1ec67153] {\n    font-size: 12px;\n}\n.p-2[data-v-1ec67153] {\n    padding: 0 !important;\n}\nul[data-v-1ec67153] {\n    margin: 15px 0;\n}\n.rowBa[data-v-1ec67153] {\n    padding: 15px;\n    margin-top: 0;\n}\n.ribbon[data-v-1ec67153] {\n    width: 150px;\n    height: 150px;\n    overflow: hidden;\n    position: absolute;\n}\n.ribbon[data-v-1ec67153]::before,\n  .ribbon[data-v-1ec67153]::after {\n    position: absolute;\n    z-index: -1;\n    content: \"\";\n    display: block;\n    border: 5px solid #2980b9;\n}\n.ribbon span[data-v-1ec67153] {\n    position: absolute;\n    display: block;\n    width: 75px;\n    padding: 1px 0;\n    background-color: #3498db;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 700 12px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n.ribbon-free span[data-v-1ec67153] {\n    position: absolute;\n    display: block;\n    width: 75px;\n    padding: 2px 0;\n    background-color: #404346;\n    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n    color: #fff;\n    font: 500 10px/1 \"Lato\", sans-serif;\n    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);\n    text-transform: capitalize;\n    text-align: center;\n}\n\n  /* top left*/\n.ribbon-top-left[data-v-1ec67153] {\n    top: 10px;\n    left: -84px;\n}\n.ribbon-top-left[data-v-1ec67153]::before,\n  .ribbon-top-left[data-v-1ec67153]::after {\n    border-top-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-top-left[data-v-1ec67153]::before {\n    top: 0;\n    right: 0;\n}\n.ribbon-top-left[data-v-1ec67153]::after {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-top-left span[data-v-1ec67153] {\n    right: 10px;\n    top: 14px;\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n}\n\n  /* top right*/\n.ribbon-top-right[data-v-1ec67153] {\n    top: -10px;\n    right: -10px;\n}\n.ribbon-top-right[data-v-1ec67153]::before,\n  .ribbon-top-right[data-v-1ec67153]::after {\n    border-top-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-top-right[data-v-1ec67153]::before {\n    top: 0;\n    left: 0;\n}\n.ribbon-top-right[data-v-1ec67153]::after {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-top-right span[data-v-1ec67153] {\n    left: -7px;\n    top: 22px;\n    -webkit-transform: rotate(45deg);\n            transform: rotate(45deg);\n}\n\n  /* bottom left*/\n.ribbon-bottom-left[data-v-1ec67153] {\n    bottom: -10px;\n    left: -10px;\n}\n.ribbon-bottom-left[data-v-1ec67153]::before,\n  .ribbon-bottom-left[data-v-1ec67153]::after {\n    border-bottom-color: transparent;\n    border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-1ec67153]::before {\n    bottom: 0;\n    right: 0;\n}\n.ribbon-bottom-left[data-v-1ec67153]::after {\n    top: 0;\n    left: 0;\n}\n.ribbon-bottom-left span[data-v-1ec67153] {\n    right: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(225deg);\n            transform: rotate(225deg);\n}\n\n  /* bottom right*/\n.ribbon-bottom-right[data-v-1ec67153] {\n    bottom: -10px;\n    right: -10px;\n}\n.ribbon-bottom-right[data-v-1ec67153]::before,\n  .ribbon-bottom-right[data-v-1ec67153]::after {\n    border-bottom-color: transparent;\n    border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-1ec67153]::before {\n    bottom: 0;\n    left: 0;\n}\n.ribbon-bottom-right[data-v-1ec67153]::after {\n    top: 0;\n    right: 0;\n}\n.ribbon-bottom-right span[data-v-1ec67153] {\n    left: -25px;\n    bottom: 30px;\n    -webkit-transform: rotate(-225deg);\n            transform: rotate(-225deg);\n}\n.container[data-v-1ec67153] {\n    padding: 0;\n}\n.whitePP[data-v-1ec67153] {\n    line-height: 1.42;\n    height: 35px;\n}\n.mobile[data-v-1ec67153] {\n    display: inline;\n}\n.vieww[data-v-1ec67153] {\n    margin-left: auto;\n    margin-bottom: 0;\n}\n.form-control[data-v-1ec67153] {\n    height: 30px;\n}\na button.btn.btn-sm[data-v-1ec67153] {\n    font-size: 12px;\n    padding: 3px 15px;\n    border-radius: 2px;\n    border: none;\n    clear: both;\n    text-transform: capitalize;\n}\nbutton.btn.btn-sm[data-v-1ec67153]:hover {\n    background: rgba(164, 194, 219, 0.64) !important ;\n    color: #fff !important;\n    border: none !important;\n}\n.paper[data-v-1ec67153] {\n    font-size: 12px;\n    padding: 0;\n    height: 75%;\n}\n.paperDiv[data-v-1ec67153] {\n    padding: 10px;\n}\n.side-market[data-v-1ec67153] {\n    padding: 5px 0 0 0;\n}\n.viewDetail[data-v-1ec67153] {\n    font-size: 8px;\n    padding: 1px 5px;\n}\n.viewDetailCart[data-v-1ec67153] {\n    font-size: 8px;\n    padding: 4px 10px;\n}\n.btn-send-to-library[data-v-1ec67153] {\n    font-size: 8px;\n    padding: 4px 10px;\n}\n.side-buttons[data-v-1ec67153] {\n    padding: 0;\n    margin-top: 5px;\n}\n.vieww[data-v-1ec67153] {\n    padding: 0 3px 0 3px;\n}\n.img_cover[data-v-1ec67153] {\n    height: auto;\n\n    background-position: 50%;\n    background-size: cover;\n    padding: 10px 6px 5px 1px;\n}\nimg[data-v-1ec67153] {\n    width: 100%;\n    height: 100%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-admin-paper-content-component",
  data: function data() {
    return {
      whitePapers: [],
      name: this.$route.params.name,
      loading: "",
      price: "",
      title: "",
      priceT: "",
      searchItem: "",
      authenticate: false,
      email: "",
      user_id: "",
      user: {},
      showModal: false,
      anonymousCart: [],
      epiModal: true,
      epiFade: true,
      loginCart: [],
      journal: false,
      whitepaper: false,
      marketResearch: false,
      marketReport: false,
      subscribeCheck: false,
      subjectMatter: "",
      breadcrumbList: [],
      isActive: true,
      loaderShow: true,
      industry_name: "",
      usersub: null,
      freeTrial: true,
      checkLibrary: [],
      showUpgrade: false
    };
  },
  created: function created() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    this.updateList();

    if (user != null) {
      this.token = user.access_token;
      axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_0__config__["b" /* getCustomerHeader */])() }).then(function (response) {
        var userDate = response.data.created_at;
        _this.startDate = new Date(userDate);

        _this.startDateParsed = Date.parse(new Date(userDate));

        _this.currentDate = Date.parse(new Date());

        _this.endDate = Date.parse(new Date(_this.startDate.setDate(_this.startDate.getDate(userDate) + 30)));

        if (_this.currentDate >= _this.endDate) {

          _this.freeTrial = false;
        } else {}
      });

      axios.get("/api/user/orders", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        response.data.data.forEach(function (element) {
          element.orderDetail.forEach(function (item) {
            _this.checkLibrary.push(item);
          });
        });
      });
      axios.get("/api/user/subscription-plans", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        _this.usersub = Number(response.data[0].level);
      });
    }
  },
  beforeMount: function beforeMount() {},
  mounted: function mounted() {
    var _this2 = this;

    var name = this.name.replace("-", " ").toLowerCase().split(" ").map(function (s) {
      return s.charAt(0).toUpperCase() + s.substring(1);
    }).join(" ");

    axios.get("/api/all-industries").then(function (response) {
      if (response.status === 200) {
        _this2.industry_name = response.data;

        axios.get("/api/admin/product/bizguruh/" + name).then(function (response) {
          if (response.status === 200) {
            _this2.isActive = false;
            _this2.loaderShow = false;
            var product = response.data.data;

            product.forEach(function (item) {
              if (item.prodType === "White Paper") {
                _this2.whitepaper = true;
                item.whitePaper.category = item.category.name;
                item.whitePaper.hardCopyPrice = item.hardCopyPrice;
                item.whitePaper.uid = item.id;
                item.whitePaper.productCategoryType = item.prodCategoryType;
                item.whitePaper.price = "";
                item.whitePaper.ptitle = "";
                item.whitePaper.priceT = "";
                item.whitePaper.softCopyPrice = item.softCopyPrice;
                item.whitePaper.readOnlinePrice = item.readOnlinePrice;
                item.whitePaper.coverImage = item.coverImage;
                item.whitePaper.uid = item.id;
                item.whitePaper.sublevel = item.subscriptionLevel;
                item.whitePaper.industry_id = item.industry.name;
                item.whitePaper.subjectMatter = item.subjectMatter.name.toLowerCase();
                if (item.whitePaper.hardCopyPrice !== "" || item.whitePaper.softCopyPrice !== "" || item.whitePaper.readOnlinePrice !== "") {
                  item.whitePaper.subscribeCheck = false;
                } else {
                  item.whitePaper.subscribeCheck = true;
                }
                _this2.whitePapers.push(item.whitePaper);
              } else if (item.prodType === "Market Reports") {
                _this2.marketReport = true;
                item.marketReport.category = item.category.name;
                item.marketReport.hardCopyPrice = item.hardCopyPrice;
                item.marketReport.uid = item.id;
                item.marketReport.productCategoryType = item.prodCategoryType;
                item.marketReport.softCopyPrice = item.softCopyPrice;
                item.marketReport.price = "";
                item.marketReport.priceT = "";
                item.marketReport.ptitle = "";
                item.marketReport.readOnlinePrice = item.readOnlinePrice;
                item.marketReport.coverImage = item.coverImage;
                item.marketReport.uid = item.id;
                item.marketReport.industry_id = item.industry_id;
                if (item.marketReport.hardCopyPrice === "" || item.marketReport.softCopyPrice === "" || item.marketReport.readOnlinePrice === "") {
                  item.marketReport.subscribeCheck = true;
                } else {
                  item.marketReport.subscribeCheck = false;
                }

                _this2.whitePapers.push(item.marketReport);
              } else if (item.prodType === "Market Research") {
                _this2.marketResearch = true;
                item.marketResearch.category = item.category.name;
                item.marketResearch.coverImage = item.coverImage;
                item.marketResearch.hardCopyPrice = item.hardCopyPrice;
                item.marketResearch.uid = item.id;
                item.marketResearch.productCategoryType = item.prodCategoryType;

                item.marketResearch.priceT = "";
                item.marketResearch.price = "";
                item.marketResearch.ptitle = "";
                item.marketResearch.softCopyPrice = item.softCopyPrice;
                item.marketResearch.readOnlinePrice = item.readOnlinePrice;
                item.marketResearch.uid = item.id;
                item.marketResearch.industryName = item.industry.name;
                item.marketResearch.industry_id = item.industry.id;
                item.marketResearch.sublevel = item.subscriptionLevel;

                item.marketResearch.subjectMatter = item.subjectMatter.name.toLowerCase();
                if (item.marketResearch.hardCopyPrice !== "" || item.marketResearch.softCopyPrice !== "" || item.marketResearch.readOnlinePrice !== "") {
                  item.marketResearch.subscribeCheck = false;
                } else {
                  item.marketResearch.subscribeCheck = true;
                }

                _this2.whitePapers.push(item.marketResearch);
              } else if (item.prodType === "Journals") {
                _this2.journal = true;
                item.journal.title = item.journal.articleTitle;
                item.journal.category = item.category.name;
                item.journal.coverImage = item.coverImage;
                item.journal.hardCopyPrice = item.hardCopyPrice;
                item.journal.productCategoryType = item.prodCategoryType;
                item.journal.uid = item.id;
                item.journal.priceT = "";
                item.journal.price = "";
                item.journal.ptitle = "";
                item.journal.softCopyPrice = item.softCopyPrice;
                item.journal.readOnlinePrice = item.readOnlinePrice;
                item.journal.uid = item.id;
                item.journal.industry_id = item.industry_id;
                item.journal.subjectMatter = item.subjectMatter.name.toLowerCase();
                item.journal.executiveSummary = item.journal.abstract;
                item.journal.journalTitle = item.journal.journalTitle;
                if (item.journal.hardCopyPrice === "" || item.journal.softCopyPrice === "" || item.journal.readOnlinePrice === "") {
                  item.journal.subscribeCheck = true;
                } else {
                  item.journal.subscribeCheck = false;
                }
                _this2.whitePapers.push(item.journal);
              }
            });
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    });
  },

  watch: {
    $route: function $route() {
      this.updateList();
    }
  },
  computed: {
    filterItem: function filterItem() {
      var _this3 = this;

      if (this.searchItem !== "") {
        return this.whitePapers.filter(function (item) {
          return item.title.toLowerCase().includes(_this3.searchItem.toLowerCase());
        });
      } else {
        return this.whitePapers;
      }
    }
  },
  methods: {
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    searchDataItem: function searchDataItem() {
      var _this4 = this;

      this.$emit("sendSearchData", this.searchItem);
      this.loading = "loading content";
      if (this.filterItem.length === 0) {
        setTimeout(function () {
          _this4.loading = "No search result ..";
        }, 3000);
      }
    },
    togglePrice: function togglePrice(params, index) {
      if (params === "hardCopy") {
        this.whitePapers[index].price = this.whitePapers[index].hardCopyPrice;
        this.whitePapers[index].ptitle = "HardCopy";
      } else if (params === "softCopy") {
        this.whitePapers[index].price = this.whitePapers[index].softCopyPrice;

        this.whitePapers[index].ptitle = "SoftCopy";
      } else if (params === "readOnline") {
        this.whitePapers[index].price = this.whitePapers[index].readOnlinePrice;

        this.whitePapers[index].ptitle = "ReadOnline";
      }
    },
    sendItemToLibrary: function sendItemToLibrary(id, params) {
      var _this5 = this;

      var data = {
        id: id,
        prodType: params,
        getType: "Insight Subscription Journal"
      };
      axios.post("/api/user/sendtolibrary", JSON.parse(JSON.stringify(data)), {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 201) {
          _this5.$toasted.success("Successfully added to library");
        } else if (response.data === " ") {
          var routeData = _this5.$router.resolve({
            name: "SubscriptionForUser"
          });
          window.open(routeData.href, "_blank");
        } else {
          _this5.$toasted.error(response.data);
        }
      }).catch(function (error) {
        if (error.response.data.message === "Unauthenticated.") {
          var routeData = _this5.$router.resolve({
            name: "auth",
            params: { name: "login" }
          });
          window.open(routeData.href, "_blank");
        } else {
          _this5.$toasted.error(error.response.data.message);
        }
      });
    }
  }
});

/***/ }),

/***/ 1534:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-cont bg-white" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", [
          _vm._m(1),
          _vm._v(" "),
          !_vm.loaderShow
            ? _c("div", { staticClass: "rowBa bg-white shadow" }, [
                _c(
                  "ul",
                  { staticClass: "w-100" },
                  _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
                    return _c(
                      "li",
                      {
                        key: index,
                        class: { linked: !!breadcrumb.link },
                        on: {
                          click: function($event) {
                            return _vm.routeTo(index)
                          }
                        }
                      },
                      [_vm._v(_vm._s(breadcrumb.name))]
                    )
                  }),
                  0
                ),
                _vm._v(" "),
                _c("div", { staticClass: "rowBanner w-100" }, [
                  _c(
                    "div",
                    {
                      staticClass: "col-xs-6 col-md-6 col-sm-6 col-lg-6 search"
                    },
                    [
                      _c("div", { staticClass: "searchBar" }, [
                        _c("div", { staticClass: "searchBarContent mb-2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.searchItem,
                                expression: "searchItem"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              placeholder: "Search title"
                            },
                            domProps: { value: _vm.searchItem },
                            on: {
                              keyup: function($event) {
                                return _vm.searchDataItem()
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.searchItem = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(2)
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(3)
                ]),
                _vm._v(" "),
                _vm.filterItem.length < 1
                  ? _c("h6", { staticClass: "noResult" }, [
                      _vm._v(_vm._s(_vm.loading))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.whitePapers.length > 0
                  ? _c(
                      "div",
                      _vm._l(_vm.filterItem, function(whitePaper, index) {
                        return _c(
                          "div",
                          {
                            key: index,
                            staticClass:
                              "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-2 mb-4 shadow-sm"
                          },
                          [
                            _c("div", { staticClass: "paperDiv" }, [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "col-lg-4 col-md-4 col-sm-3 col-xs-4 img_cover"
                                },
                                [
                                  whitePaper.productCategoryType === "v" ||
                                  whitePaper.productCategoryType === "EP" ||
                                  whitePaper.productCategoryType === "VP" ||
                                  whitePaper.softCopyPrice !== "" ||
                                  whitePaper.hardCopyPrice !== "" ||
                                  whitePaper.readOnlinePrice !== ""
                                    ? _c(
                                        "div",
                                        {
                                          staticClass: "ribbon ribbon-top-left"
                                        },
                                        [_c("span", [_vm._v("Paid")])]
                                      )
                                    : _c("div", [
                                        whitePaper.sublevel == "0"
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "ribbon ribbon-free ribbon-top-left"
                                              },
                                              [_c("span", [_vm._v("free")])]
                                            )
                                          : _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "ribbon ribbon-free ribbon-top-left"
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v("subscribe")
                                                ])
                                              ]
                                            )
                                      ]),
                                  _vm._v(" "),
                                  _c("img", {
                                    attrs: { src: whitePaper.coverImage }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "col-lg-8 col-md-8 col-sm-8 col-xs-8 side-market"
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-lg-12 col-md-12 col-sm-12 col-xs-12 paper"
                                    },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "ProductPaperDetail",
                                              params: {
                                                id: whitePaper.uid,
                                                name: (whitePaper.title
                                                  ? whitePaper.title
                                                  : whitePaper.articleTitle
                                                )
                                                  .replace(/[^a-z0-9]/gi, "")
                                                  .toLowerCase(),
                                                type: "subscribe"
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "paperTitle text-main"
                                            },
                                            [_vm._v(_vm._s(whitePaper.title))]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      whitePaper.executiveSummary
                                        ? _c(
                                            "div",
                                            { staticClass: "whitePP" },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  whitePaper.executiveSummary
                                                )
                                              )
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      whitePaper.journalTitle
                                        ? _c("div", [
                                            _c("small", [
                                              _vm._v(
                                                _vm._s(whitePaper.journalTitle)
                                              )
                                            ])
                                          ])
                                        : _c(
                                            "div",
                                            { staticClass: "whitePP" },
                                            [_vm._v(_vm._s(whitePaper.excerpt))]
                                          ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "smatter" }, [
                                        _vm._v(
                                          "\n                  " +
                                            _vm._s(whitePaper.subjectMatter) +
                                            "\n                  "
                                        ),
                                        _c("b", [_vm._v("/")]),
                                        _vm._v(
                                          "\n                  " +
                                            _vm._s(whitePaper.industryName) +
                                            "\n                "
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "small",
                                        { staticClass: "datePaper" },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                whitePaper.created_at,
                                                "MMM YYYY"
                                              )
                                            )
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "side-buttons col-lg-12 col-md-12 col-sm-12 col-xs-12"
                                    },
                                    [
                                      _c("div", {}, [
                                        whitePaper.subscribeCheck
                                          ? _c("div", [
                                              _vm.journal
                                                ? _c("div", [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-12 col-xs-4 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "router-link",
                                                          {
                                                            staticClass:
                                                              "viewDetailCart",
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "ReadOnline",
                                                                params: {
                                                                  id:
                                                                    whitePaper.uid,
                                                                  prodtype:
                                                                    "Journals",
                                                                  gettype:
                                                                    "download"
                                                                }
                                                              },
                                                              target: "_blank"
                                                            }
                                                          },
                                                          [_vm._v("Download")]
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-12 col-xs-4 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "router-link",
                                                          {
                                                            staticClass:
                                                              "viewDetailCart",
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "ReadOnline",
                                                                params: {
                                                                  id:
                                                                    whitePaper.uid,
                                                                  prodtype:
                                                                    "Journals",
                                                                  gettype:
                                                                    "readonline"
                                                                }
                                                              },
                                                              target: "_blank"
                                                            }
                                                          },
                                                          [_vm._v("Read Now")]
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-12 col-xs-4 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "button",
                                                          {
                                                            staticClass:
                                                              "btn btns btn-send-to-library",
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.sendItemToLibrary(
                                                                  whitePaper.uid,
                                                                  "Journals"
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Send To My Library"
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ])
                                                : _vm._e()
                                            ])
                                          : _vm._e(),
                                        _vm._v(" "),
                                        whitePaper.subscribeCheck
                                          ? _c("div", [
                                              _vm.whitepaper
                                                ? _c("div", [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-12 col-xs-4 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "router-link",
                                                          {
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "ReadOnline",
                                                                params: {
                                                                  id:
                                                                    whitePaper.uid,
                                                                  prodtype:
                                                                    "white-paper",
                                                                  gettype:
                                                                    "download"
                                                                }
                                                              },
                                                              target: "_blank"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "button",
                                                              {
                                                                staticClass:
                                                                  "btn"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Download"
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-4 col-xs-12 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "router-link",
                                                          {
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "ReadOnline",
                                                                params: {
                                                                  id:
                                                                    whitePaper.uid,
                                                                  prodtype:
                                                                    "white-paper",
                                                                  gettype:
                                                                    "readonline"
                                                                }
                                                              },
                                                              target: "_blank"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "button",
                                                              {
                                                                staticClass:
                                                                  "btn"
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Read Now"
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-lg-12 col-md-12 col-sm-4 col-xs-12 vieww"
                                                      },
                                                      [
                                                        _c(
                                                          "button",
                                                          {
                                                            staticClass:
                                                              "btn btn-send-to-library",
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.sendItemToLibrary(
                                                                  whitePaper.uid,
                                                                  "White Paper"
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Send To My Library"
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ])
                                                : _vm._e()
                                            ])
                                          : _vm._e(),
                                        _vm._v(" "),
                                        whitePaper.subscribeCheck
                                          ? _c("div")
                                          : _vm._e()
                                      ]),
                                      _vm._v(" "),
                                      whitePaper.subscribeCheck
                                        ? _c("div", [
                                            _vm.marketReport
                                              ? _c("div")
                                              : _vm._e()
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-lg-12 col-md-12 col-sm-12 col-xs-12 vieww"
                                        },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "ProductPaperDetail",
                                                  params: {
                                                    id: whitePaper.uid,
                                                    name: (whitePaper.title
                                                      ? whitePaper.title
                                                      : whitePaper.articleTitle
                                                    )
                                                      .replace(
                                                        /[^a-z0-9]/gi,
                                                        ""
                                                      )
                                                      .toLowerCase(),
                                                    type: "subscribe"
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "elevated_btn elevated_btn_sm btn-compliment text-white ml-auto",
                                                  attrs: { type: "button" }
                                                },
                                                [_vm._v("View")]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                ]
                              )
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  : _vm._e()
              ])
            : _vm._e()
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "banner" }, [
      _c("h1", { staticClass: "banner-text" }, [_vm._v("Books")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-btn mt-0 ml-0" }, [
      _c("i", { staticClass: "fa fa-search fa-1x" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-xs-3 col-md-3 col-sm-3 col-lg-3 markSearch" },
      [
        _c(
          "select",
          { staticClass: "form-control", attrs: { type: "search" } },
          [_c("option", [_vm._v("Market Research")])]
        )
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1ec67153", module.exports)
  }
}

/***/ }),

/***/ 1535:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "market" }, [
    _c(
      "div",
      { staticClass: "container-fluid p-0" },
      [_c("app-paper-content")],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5e196910", module.exports)
  }
}

/***/ }),

/***/ 582:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1527)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1529)
/* template */
var __vue_template__ = __webpack_require__(1535)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5e196910"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userAdminWhitePaperComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5e196910", Component.options)
  } else {
    hotAPI.reload("data-v-5e196910", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 955:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(956)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(958)
/* template */
var __vue_template__ = __webpack_require__(959)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1468ed38"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userCourseCategoriesComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1468ed38", Component.options)
  } else {
    hotAPI.reload("data-v-1468ed38", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 956:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(957);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3a2541c4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1468ed38\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCourseCategoriesComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1468ed38\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCourseCategoriesComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 957:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.catName[data-v-1468ed38] {\n    text-transform: capitalize;\n}\n.sidebarcat[data-v-1468ed38] {\n    margin-top: 30px;\n}\n.envCat[data-v-1468ed38] {\n    margin: 30px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-course-categories",
    data: function data() {
        return {
            categories: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/api/product-all-category').then(function (response) {
            if (response.status === 200) {
                _this.categories = response.data.data;
            }
        }).catch(function (error) {
            console.log(error);
        });
    }
});

/***/ }),

/***/ 959:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sidebarcat" }, [
    _c(
      "ul",
      [
        _c("li", { staticClass: "envCat" }, [_vm._v("All Courses")]),
        _vm._v(" "),
        _vm._l(_vm.categories, function(category, index) {
          return _vm.categories.length > 0
            ? _c("li", { staticClass: "envCat" }, [
                _c("div", { staticClass: "catName" }, [
                  _vm._v(_vm._s(category.name))
                ]),
                _vm._v(" "),
                _c("small")
              ])
            : _vm._e()
        })
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1468ed38", module.exports)
  }
}

/***/ })

});