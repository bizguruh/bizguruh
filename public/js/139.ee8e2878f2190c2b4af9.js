webpackJsonp([139],{

/***/ 1634:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1635);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0624dcf7", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9d0d4cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./contactComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9d0d4cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./contactComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1635:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-9d0d4cf8] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n    margin: 30px 0;\n}\nul > li[data-v-9d0d4cf8] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nul > li[data-v-9d0d4cf8]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-9d0d4cf8] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.container[data-v-9d0d4cf8]{\n    font-size: 1em;\n    padding:100px 200px;\n    background: #f7f7f7;\n    height:auto;\n}\n.form-control[data-v-9d0d4cf8]{\n    border-radius: 15px;\n}\n.btn[data-v-9d0d4cf8]{\n    -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12) !important;\n            box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12) !important;\n    border-radius: 20px;\n    padding: .84rem 2.14rem !important;\n    font-size: .81rem !important;\n}\n.btn-outline-primary[data-v-9d0d4cf8] {\n    border: 2px solid #a4c2db!important;\n    background-color:#fff!important;\n    color: #000 !important;\n}\n.danger[data-v-9d0d4cf8]{\n    color:red;\n}\n.success[data-v-9d0d4cf8]{\n    color: green;\n    font-style: italic;\n}\n@media(max-width:768px){\n.container[data-v-9d0d4cf8]{\n    padding:80px 60px 100px;\n}\n}\n@media(max-width:425px){\n.container[data-v-9d0d4cf8]{\n    padding: 60px 20px 100px;\n}\n}\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1636:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'contact-component',
    data: function data() {
        return {
            fields: {
                contactName: null,
                contactEmail: null,
                contactSubject: null,
                contactMessage: null
            },
            error: true,
            breadcrumbList: [],

            errors: {
                name: [],
                email: [],
                subject: [],
                message: []
            }
        };
    },

    components: {},

    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        },
        checkErrors: function checkErrors(e) {
            this.errors.name = [];
            this.errors.email = [];
            this.errors.subject = [];
            this.errors.message = [];

            if (!this.fields.contactName) {

                this.errors.name.push('Your name is required');
            }
            if (!this.fields.contactEmail) {

                this.errors.email.push('Your email is required');
            }
            if (!this.fields.contactSubject) {

                this.errors.subject.push('A subject is required');
            }
            if (!this.fields.contactMessage) {

                this.errors.message.push('Your message is required');
            }
        },
        submit: function submit() {
            var _this = this;

            this.checkErrors();

            if (this.errors.name.length === 0 && this.errors.email.length === 0 && this.errors.subject.length === 0 && this.errors.message.length === 0) {
                this.error = false;
                this.fields.contactName = null;
                this.fields.contactEmail = null;
                this.fields.contactSubject = null;
                this.fields.contactMessage = null;
                axios.post('/api/contact', this.fields).then(function (response) {
                    console.log(response);
                    if (response.status === 200) {
                        _this.$notify({
                            group: "cart",
                            title: "Message sent",
                            text: "Thank you for the feedback"
                        });
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }

});

/***/ }),

/***/ 1637:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c(
      "ul",
      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
        return _c(
          "li",
          {
            key: index,
            class: { linked: !!breadcrumb.link },
            on: {
              click: function($event) {
                return _vm.routeTo(index)
              }
            }
          },
          [
            _vm._v(
              "\n              " + _vm._s(breadcrumb.name) + "\n            "
            )
          ]
        )
      }),
      0
    ),
    _vm._v(" "),
    _c(
      "form",
      {
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.submit($event)
          }
        }
      },
      [
        _c("h3", { staticClass: "  mb-4" }, [_vm._v("Leave us a Message")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            " Hey there! if you have any questions or concerns, simply place a call to us or just use the contact form below and we will get back to you shortly."
          )
        ]),
        _vm._v(" "),
        _c(
          "label",
          {
            staticClass: "grey-text",
            attrs: { for: "defaultFormContactNameEx" }
          },
          [_vm._v("Your name")]
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.fields.contactName,
              expression: "fields.contactName"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "defaultFormContactNameEx", required: "" },
          domProps: { value: _vm.fields.contactName },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.fields, "contactName", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _vm.errors.name
          ? _c("div", { staticClass: "danger" }, [
              _vm._v(" " + _vm._s(_vm.errors.name[0]))
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "label",
          {
            staticClass: "grey-text",
            attrs: { for: "defaultFormContactEmailEx" }
          },
          [_vm._v("Your email")]
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.fields.contactEmail,
              expression: "fields.contactEmail"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "email",
            id: "defaultFormContactEmailEx",
            required: ""
          },
          domProps: { value: _vm.fields.contactEmail },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.fields, "contactEmail", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _vm.errors.email
          ? _c("div", { staticClass: "danger" }, [
              _vm._v(" " + _vm._s(_vm.errors.email[0]))
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "label",
          {
            staticClass: "grey-text",
            attrs: { for: "defaultFormContactSubjectEx" }
          },
          [_vm._v("Subject")]
        ),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.fields.contactSubject,
              expression: "fields.contactSubject"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "defaultFormContactSubjectEx" },
          domProps: { value: _vm.fields.contactSubject },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.fields, "contactSubject", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _vm.errors.subject
          ? _c("div", { staticClass: "danger" }, [
              _vm._v(" " + _vm._s(_vm.errors.subject[0]))
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "label",
          {
            staticClass: "grey-text",
            attrs: { for: "defaultFormContactMessageEx" }
          },
          [_vm._v("Your message")]
        ),
        _vm._v(" "),
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.fields.contactMessage,
              expression: "fields.contactMessage"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "defaultFormContactMessageEx", rows: "3" },
          domProps: { value: _vm.fields.contactMessage },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.fields, "contactMessage", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _vm.errors.message
          ? _c("div", { staticClass: "danger" }, [
              _vm._v(" " + _vm._s(_vm.errors.message[0]))
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm._m(0)
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center m-4" }, [
      _c(
        "button",
        { staticClass: "btn btn-outline-primary", attrs: { type: "submit" } },
        [_vm._v("Send"), _c("i", { staticClass: "far fa-paper-plane ml-2" })]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9d0d4cf8", module.exports)
  }
}

/***/ }),

/***/ 606:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1634)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1636)
/* template */
var __vue_template__ = __webpack_require__(1637)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-9d0d4cf8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/contactComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9d0d4cf8", Component.options)
  } else {
    hotAPI.reload("data-v-9d0d4cf8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});