webpackJsonp([136],{

/***/ 1621:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1622);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("396a746e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6d54d2f4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidelineComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6d54d2f4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidelineComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1622:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bread ul[data-v-6d54d2f4] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n   margin: 10px 0px 30px;\n    padding:5px 15px;\n    background:#fff;\n}\n.bread ul > li[data-v-6d54d2f4] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bread ul > li[data-v-6d54d2f4]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-6d54d2f4] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.container[data-v-6d54d2f4]{\n    padding-top:80px;\n    padding-bottom: 100px;\n    line-height:1.8;\n    font-size:16px;\n}\nh3[data-v-6d54d2f4]{\n    margin:10px 0;\n}\nul[data-v-6d54d2f4]{\n    list-style: disc;\n}\n.boldText[data-v-6d54d2f4]{\n    font-weight:bold;\n    font-size:16px;\n    color:rgba(0, 0, 0, 0.64);\n    margin-bottom: 2px;\n}\n.uline[data-v-6d54d2f4]{\n    text-decoration:underline;\n}\n.tCenter[data-v-6d54d2f4]{\n  margin: 10px auto;\n  text-align: center;\n}\n@media(max-width:425px){\nh2[data-v-6d54d2f4]{\n        font-size: 18px;\n}\nh3[data-v-6d54d2f4]{\n        font-size: 16px;\n}\n.boldText[data-v-6d54d2f4]{\n        font-size:16px;\n}\nol ul[data-v-6d54d2f4]{\n        margin-bottom: 10px;\n}\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1623:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "guideline-component",
    data: function data() {
        return {
            breadcrumbList: []
        };
    },
    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        }
    }
});

/***/ }),

/***/ 1624:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "bread" }, [
      _c(
        "ul",
        _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
          return _c(
            "li",
            {
              key: index,
              class: { linked: !!breadcrumb.link },
              on: {
                click: function($event) {
                  return _vm.routeTo(index)
                }
              }
            },
            [
              _vm._v(
                "\n               " +
                  _vm._s(breadcrumb.name) +
                  "\n             "
              )
            ]
          )
        }),
        0
      )
    ]),
    _vm._v(" "),
    _c("div", {}, [
      _c("h2", { staticClass: "tCenter uline mb-4" }, [
        _vm._v("  BIZGURUH COMMUNITY GUIDELINES")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    As a platform for proffering honest and practical Africa-focuses business education and strategic market insight, BizGuruh is deeply committed to expounding and increasing the knowledge base of the African’s informal sector.  At the same time, we draw lines around a few narrowly defined but deeply important categories of content and behavior that jeopardize our users, threaten our infrastructure, and damage our community.\n"
        )
      ]),
      _vm._v(" "),
      _c("h3", { staticClass: "uline" }, [_vm._v("What BizGuruh is for:")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    BizGuruh creates and curates Africa-focused business education resources and strategic market insight. We want you to express yourself freely and use BizGuruh to learn how to grow a sustainable business for yourself.\n"
        )
      ]),
      _vm._v(" "),
      _c("h3", { staticClass: "uline" }, [_vm._v("What BizGuruh is not for:")]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    If we conclude that you are violating these guidelines, you may receive a notice via email. If you don't explain or correct your behavior, we may take action against your account. We do our best to ensure fair outcomes, but in all cases we reserve the right to suspend accounts, or remove content, without notice, for any reason, but particularly to protect our services, infrastructure, users, and community. We reserve the right to enforce, or not enforce, these guidelines in our sole discretion, and these guidelines don't create a duty or contractual obligation for us to act in any particular manner. We’re committed to these guidelines and we hope you are too.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\nYou can report violations of these guidelines to us directly\n"
        )
      ]),
      _vm._v(" "),
      _c(
        "p",
        [
          _vm._v(
            "We also reserve the right to amend these guidelines using the procedures set forth in our "
          ),
          _c("router-link", { attrs: { to: "/terms" } }, [
            _vm._v("Terms and Conditions")
          ]),
          _vm._v(".\n")
        ],
        1
      ),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "For more information, check out our Help Center and Terms of Use."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Thanks for reading all of this, by the way. Welcome to BizGuruh Community.\n"
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "mb-2" }, [
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [_vm._v("\tTerrorism.")]),
        _vm._v(
          " We don't tolerate content that promotes, encourages, or incites acts of terrorism. That includes content which supports or celebrates terrorist organizations, their leaders, or associated violent activities. "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v("Report terrorism")
        ]),
        _vm._v(".\n")
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [_vm._v(" \tHate Speech.")]),
        _vm._v(
          " Don't encourage violence or hatred. Don't post content for the purpose of promoting or inciting the hatred of, or dehumanizing, individuals or groups based on race, ethnic or national origin, religion, gender, gender identity, age, disability or disease. If you encounter content that violates our hate speech policies, please report it. "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v("Report hate speech")
        ]),
        _vm._v(
          ". Keep in mind that a post might be mean, tasteless, or offensive without necessarily encouraging violence or hatred. In cases like that, you can express your concerns to them directly, or use your BizGist to speak up, challenge ideas, raise awareness or generate discussion and debate.\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [_vm._v("\tAdult Content.")]),
        _vm._v(
          " Don't upload images, videos, or GIFs that show real-life human genitals or female-presenting nipples — this includes content that is so photorealistic that it could be mistaken for featuring real-life humans (nice try, though). Don’t upload any content; including images, videos, GIFs, or illustrations, that depicts sex acts."
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("  \tViolent Content and Threats, Gore and Mutilation. ")
        ]),
        _vm._v(
          " Don't post content which includes violent threats toward individuals or groups - this includes threats of theft, property damage, or financial harm. Don't post violent content or gore just to be shocking. Don't showcase the mutilation or torture of human beings, animals (including bestiality), or their remains. Don't post content that encourages or incites violence, or glorifies acts of violence or the perpetrators. "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v(" Report violent threats.")
        ])
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("  \tDeceptive or Fraudulent Links. ")
        ]),
        _vm._v(
          " Don't post deceptive or fraudulent links in your posts. This includes giving links misleading descriptions, putting the wrong “source” field in a post, setting misleading click-through links on images, or embedding links to interstitial or pop-up ads.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v(" \tMisattribution or Non-Attribution.")
        ]),
        _vm._v(
          " Make sure you always give proper attribution and include full links back to original sources. When you find something awesome on BizGuruh, reblog it instead of reposting it. It's less work and more fun, anyway. When reblogging something, DO NOT inject a link back to your blog just to steal attention from the original post.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("  \tAccount Dormancy.")
        ]),
        _vm._v(
          " Use BizGuruh! Use BizGuruh all the time! Or at the very least, use BizGuruh once a year. If you don’t, we may mark your account as dormant. Your content won't go anywhere—it'll be archived exactly as you left it—but your URL(s) will be released for someone else to use.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [_vm._v(" \tSpam.")]),
        _vm._v(
          " Don't spam people. Don't make spammy posts, don't post spammy replies, and don’t send people spammy messages. Be a regular human. Don't put tags on your posts that will mislead or deceive searchers. Don't put dubious code in your posts, like using JavaScript to cause redirects or inject unwanted ads in blogs. Don't use deceptive means to generate revenue or traffic, or create blogs with the primary purpose of affiliate marketing. Spam doesn't belong on BizGuruh.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v(" \tCopyright or Trademark Infringement.")
        ]),
        _vm._v(
          " Respect the copyrights and trademarks of others. If you aren't allowed to use someone else's copyrighted or trademarked work (either by license or by legal exceptions and limitations such as fair use), don't post it.\n\n"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n    With regard to repeat copyright infringement, we use a three-strike system to evaluate the standing of a user's account, where, generally, each valid copyright infringement notice constitutes a strike, and three strikes results in the termination of a user's account. "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v("Report copyright infringement. Report trademark infringement")
        ]),
        _vm._v(".\n\n")
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [_vm._v(" \tHarassment.")]),
        _vm._v(
          " Don't engage in targeted abuse or harassment. Don't engage in the unwanted sexualization or sexual harassment of others. If anyone is sending you unwanted messages or reblogging your posts in an abusive way, we encourage you to be proactive and "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v("Report harassment.")
        ])
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("\tPrivacy Violations.")
        ]),
        _vm._v(
          " Don't use BizGuruh to deceptively obtain personal information. Don't post content that violates anyone's privacy, especially personally identifying or confidential information like credit card numbers, or unlisted contact information. "
        ),
        _c("span", { staticClass: "boldText uline" }, [
          _vm._v("Report privacy violations.")
        ])
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("\tUnlawful Uses or Content.")
        ]),
        _vm._v(
          " Don't use BizGuruh to conduct illegal behavior, like fraud or phishing.\n"
        )
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "mb-2" }, [
        _c("span", { staticClass: "boldText" }, [
          _vm._v("\tHuman Trafficking and Prostitution.")
        ]),
        _vm._v(
          " Don't use BizGuruh to facilitate sex trafficking, other forms of human trafficking, or illegal prostitution. If you see this activity on BizGuruh, report it, and encourage victims to contact law enforcement.\n\n\n\n"
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6d54d2f4", module.exports)
  }
}

/***/ }),

/***/ 603:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1621)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1623)
/* template */
var __vue_template__ = __webpack_require__(1624)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6d54d2f4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/guidelineComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6d54d2f4", Component.options)
  } else {
    hotAPI.reload("data-v-6d54d2f4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});