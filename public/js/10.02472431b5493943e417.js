webpackJsonp([10],{

/***/ 1007:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1008)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1010)
/* template */
var __vue_template__ = __webpack_require__(1011)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5e268f22"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5e268f22", Component.options)
  } else {
    hotAPI.reload("data-v-5e268f22", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1008:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1009);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2ef2ccbb", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e268f22\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e268f22\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1009:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mobile-d[data-v-5e268f22]{\n  display: none;\n}\n.banner[data-v-5e268f22] {\n  width: 100%;\n  height: 85vh;\n  position: relative;\n  overflow: hidden;\n  background:#a4c2db;\n}\n.image-box[data-v-5e268f22]{\n   position: absolute;\n  width: 60%;\n  right: -5%;\n  height: 100%;\n   background-image: url(\"/images/bane1.jpg\");\n  background-position: right;\n  background-size: cover;\n}\n.slant_1[data-v-5e268f22] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n-webkit-transform:skewX(-25deg);\n        transform:skewX(-25deg);\n  top: -50%;\n    z-index: 2;\n    border-right:10px solid white;\n}\n.slant_2[data-v-5e268f22] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n  -webkit-transform:skewX(25deg);\n          transform:skewX(25deg);\n  bottom: -49.7%;\n  z-index: 2;\n  border-right:10px solid white;\n}\n.text_box[data-v-5e268f22] {\n  position: absolute;\n  width: 45%;\n  left: 0;\n  height: 100%;\n background: #a4c2db;\n  z-index: 3;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-orient:vertical;\n  -webkit-box-direction:normal;\n      -ms-flex-direction:column;\n          flex-direction:column;\n  padding-left:80px;\n}\n.text-container[data-v-5e268f22] {\n  font-family: \"Josefin Sans\", sans-serif;\n  line-height: 1.4;\n}\n.bannerA[data-v-5e268f22] {\n  font-size: 38px;\n  line-height: 1.4;\n}\n.bannerB[data-v-5e268f22] {\n  font-size: 28px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bannerC[data-v-5e268f22] {\n  font-size: 24px;\n  font-family: \"Open Sans\", sans-serif;\n}\n@media (max-width: 1024px) {\n.desktop-d[data-v-5e268f22]{\n    display: none;\n}\n.mobile-d[data-v-5e268f22]{\n    display: block;\n}\n.banner-container[data-v-5e268f22] {\n  position: relative;\n  overflow: hidden;\n  width: 100%;\n  height: 91vh;\n  border-right: 6px solid white;\n  border-left: 6px solid white;\n}\n.banner-over[data-v-5e268f22]{\n  position: absolute;\n  top: 0;\n  bottom:0;\n  left:0;\n  right:0;\n   background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(169, 197, 221,.9)),\n    color-stop(rgb(175, 201, 227 ,.9)),\n    color-stop(rgb(180, 204, 225,.9)),\n    color-stop(rgb(192, 212, 230,.9)),\n    color-stop(rgb(203, 220, 234,.9)),\n    color-stop(rgb(215, 228, 239,.9)),\n    to(rgb(227, 236, 244 ,.9))\n  );\n   background-image: linear-gradient(\n    to right top,\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(169, 197, 221,.9),\n    rgb(175, 201, 227 ,.9),\n    rgb(180, 204, 225,.9),\n    rgb(192, 212, 230,.9),\n    rgb(203, 220, 234,.9),\n    rgb(215, 228, 239,.9),\n    rgb(227, 236, 244 ,.9)\n  );\n  z-index: 2;\n}\n.banner-img[data-v-5e268f22] {\n    background-image: url(\"/images/bane1.jpg\");\n  background-position: right;\n  background-size: cover;\n  height: 100%;\n  width: 100%;\n}\n.banner-box[data-v-5e268f22] {\n  width: 60%;\n  height: 140%;\n \n  position: absolute;\n  top: -10%;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  left: -12%;\n  color: hsl(207, 43%, 20%);\n  background-image:url('/images/curve1.jpg');\n  background-size:contain;\n  z-index: 1;\n}\n.banner-white-box[data-v-5e268f22] {\n  width: 1%;\n  height: 140%;\n  background-color: #fff;\n  position: absolute;\n  top: -20%;\n  -webkit-transform: rotate(0deg);\n          transform: rotate(0deg);\n  right: 0;\n  z-index: 3;\n}\n.banner-text[data-v-5e268f22] {\n  position: absolute;\n  -webkit-transform: rotate(20deg);\n          transform: rotate(20deg);\n  right: 73px;\n  bottom: 30%;\n  width: 500px;\n  height: 500px;\n  z-index: 3;\n}\n.text-container[data-v-5e268f22] {\n  font-family: \"Josefin Sans\", sans-serif;\n  line-height: 1.4;\n}\n.bannerA[data-v-5e268f22] {\n  font-size: 38px;\n  line-height: 1.4;\n}\n.bannerB[data-v-5e268f22] {\n  font-size: 28px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bannerC[data-v-5e268f22] {\n  font-size: 24px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.mobile-banner[data-v-5e268f22] {\n  display: none;\n}\n.banner-arrow[data-v-5e268f22] {\n  position: absolute;\n  width: 40px;\n  height: auto;\n  z-index: 7;\n  bottom: 15px;\n  left: 50%;\n  margin-left: -20px;\n  display: grid;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  cursor: pointer;\n  -webkit-transition: ease 0.4s;\n  transition: ease 0.4s;\n}\n.fa-caret-down[data-v-5e268f22] {\n  font-size: 36px;\n  color: white;\n}\n.banner-arrow:hover .fa1[data-v-5e268f22] {\n  color: hsl(207, 43%, 20%);\n}\n.banner-arrow:hover .fa2[data-v-5e268f22] {\n  color: white !important;\n}\n.mobile-banner-img[data-v-5e268f22] {\n  display: none;\n}\n.bannerA[data-v-5e268f22] {\n    line-height: 1.4;\n}\n.banner-text[data-v-5e268f22] {\n    right: 43px;\n    bottom: 28%;\n    width: 380px;\n    height: auto;\n}\n}\n@media (max-width: 768px) {\n.banner-box[data-v-5e268f22] {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n    width: 100%;\n    height: 100%;\n    top: 0;\n    left: 0;\n}\n.bannerA[data-v-5e268f22] {\n    line-height: 1.4;\n}\n.banner-white-box[data-v-5e268f22] {\n    width: 20%;\n    height: 100%;\n    top: 0;\n    right: 0;\n    background-color: #333;\n    z-index: 4;\n}\n.mobile-banner[data-v-5e268f22] {\n    display: block;\n    position: absolute;\n    z-index: 3;\n    bottom: 6%;\n    height: 400px;\n    width: 400px;\n    right: -15%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mini-border-1[data-v-5e268f22] {\n    border: 1px solid #bfd4e5;\n    padding: 10px;\n    height: 100%;\n    width: 100%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mini-border-2[data-v-5e268f22] {\n    border: 1px solid #bfd4e5;\n    padding: 10px;\n    height: 100%;\n    width: 100%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    display: block;\n    background: transparent;\n    width: 356px;\n    height: 356px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 9%;\n    right: -12%;\n    z-index: 5;\n    overflow: hidden;\n}\n.mobile-banner-img img[data-v-5e268f22] {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: right;\n       object-position: right;\n}\n.banner-text[data-v-5e268f22] {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n    width: 75%;\n    bottom: 25%;\n    height: auto;\n    padding: 40px;\n    left: 0;\n    right: 0;\n    height: auto;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 32px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 17px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 17px;\n}\n.banner-arrow[data-v-5e268f22] {\n    left: 45%;\n}\n.banner-border[data-v-5e268f22] {\n    border-bottom: 1px solid white;\n    position: absolute;\n    width: 50%;\n    height: auto;\n    z-index: 7;\n    bottom: 50px;\n    right: 0;\n}\n}\n@media (max-width: 425px) {\n.banner-container[data-v-5e268f22] {\n    height: 90vh;\n    border: none;\n}\n.mobile-banner[data-v-5e268f22] {\n    bottom: 10%;\n    height: 300px;\n    width: 300px;\n    right: -25%;\n}\n.fa-caret-down[data-v-5e268f22] {\n    font-size: 24px;\n    color: white;\n}\n.banner-text[data-v-5e268f22] {\n    width: 81%;\n    bottom: 35%;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 24px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 17px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 17px;\n}\n.fa-caret-down[data-v-5e268f22] {\n    font-size: 24px;\n    color: white;\n}\n.banner-text[data-v-5e268f22] {\n    padding: 20px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 256px;\n    height: 256px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -20%;\n}\n}\n@media (max-width: 375px) {\n.mobile-banner[data-v-5e268f22] {\n    height: 274px;\n    width: 274px;\n    right: -30%;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 15px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 16px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 230px;\n    height: 230px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -24%;\n}\n}\n@media (max-width: 320px) {\n.mobile-banner[data-v-5e268f22] {\n    height: 274px;\n    width: 274px;\n    right: -30%;\n}\n.banner-text[data-v-5e268f22] {\n    bottom: 30%;\n    padding: 15px;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 20px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 14px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 14px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 230px;\n    height: 230px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -24%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1010:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-banner-component",
  data: function data() {
    return {
      auth: false,
      search: "",
      show: false,
      scrollPos: 0,
      currentHeight: 0
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
    var user = localStorage.getItem('authUser');
    if (user !== null) {
      this.auth = true;
    }
  },


  methods: {
    scrollDown: function scrollDown() {
      window.scrollTo(0, window.innerHeight * 0.85);
    },
    mail: function mail() {
      axios.get('/api/mail').then(function (res) {});
    }
  }
});

/***/ }),

/***/ 1011:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "banner desktop-d" }, [
      _c("div", { staticClass: "slant_1" }),
      _vm._v(" "),
      _c("div", { staticClass: "slant_2" }),
      _vm._v(" "),
      _c("div", { staticClass: "slant_2" }),
      _vm._v(" "),
      _c("div", { staticClass: "text_box" }, [
        !_vm.auth
          ? _c("div", { staticClass: "text-container" }, [
              _c("p", { staticClass: "bannerA mb-3" }, [
                _vm._v(
                  " Join a community of entrepreneurs who can connect with experts and access practical business insights and resources, on the go."
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "bannerB mb-2" }, [
                _vm._v(
                  "We can take your business from 0 to 100 within six weeks."
                )
              ])
            ])
          : _c("div", { staticClass: "text-container" }, [
              _c("p", { staticClass: "bannerA mb-3" }, [
                _vm._v(
                  " Ready to be heard loud and clear through your business?"
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "bannerB mb-2" }, [
                _vm._v(
                  "We can take your business from 0 to 100 within six weeks."
                )
              ])
            ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "text-container" },
          [
            _c("p", { staticClass: "bannerC mb-2" }, [
              _vm._v("Don’t take our word for it.")
            ]),
            _vm._v(" "),
            !_vm.auth
              ? _c("router-link", { attrs: { to: "/auth/register" } }, [
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn btn-white text-main animated fadeIn slow"
                    },
                    [_vm._v(" Start for Free")]
                  )
                ])
              : _c("router-link", { attrs: { to: "/user-subscription/3" } }, [
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn btn-white text-main animated fadeIn slow"
                    },
                    [_vm._v("Start BAP-6 program")]
                  )
                ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "image-box" })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "banner-container mobile-d" }, [
      _c("div", { staticClass: "banner-img" }),
      _vm._v(" "),
      _c("div", { staticClass: "banner-box" }, [
        _c("div", { staticClass: "banner-over" }),
        _vm._v(" "),
        _c("div", { staticClass: "banner-text animated fadeIn slow" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "text-container" },
            [
              _c("p", { staticClass: "bannerC mb-4" }, [
                _vm._v("Don’t take our word for it.")
              ]),
              _vm._v(" "),
              !_vm.auth
                ? _c("router-link", { attrs: { to: "/auth/register" } }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn btn-white text-main animated fadeIn slow"
                      },
                      [_vm._v(" Start for Free")]
                    )
                  ])
                : _c("router-link", { attrs: { to: "/user-subscription/3" } }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn btn-white text-main animated fadeIn slow"
                      },
                      [_vm._v("Start BAP-6 program")]
                    )
                  ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "banner-white-box" }),
        _vm._v(" "),
        _vm._m(2)
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "banner-arrow animated bounce",
          on: { click: _vm.scrollDown }
        },
        [
          _c("i", {
            staticClass: "fa fa-caret-down fa1",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" "),
          _c("i", {
            staticClass: "fa fa-caret-down fa1",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" "),
          _c("i", {
            staticClass: "fa fa-caret-down text-main fa2",
            attrs: { "aria-hidden": "true" }
          })
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "banner-border" }),
      _vm._v(" "),
      _c("div", { staticClass: "banner-bottom-bg" })
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-container" }, [
      _c("p", { staticClass: "bannerA mb-3" }, [
        _vm._v(
          " Join a community of entrepreneurs who can connect with experts and access practical business insights and resources, on the go."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "bannerB mb-2" }, [
        _vm._v("We can take your business from 0 to 100 within six weeks.")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "mobile-banner-img animated slideInRight" },
      [_c("img", { attrs: { src: "/images/optmobile.jpg", alt: "" } })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mobile-banner" }, [
      _c("div", { staticClass: "mini-border-1" }, [
        _c("div", { staticClass: "mini-border-2" })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5e268f22", module.exports)
  }
}

/***/ }),

/***/ 1012:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1013)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1015)
/* template */
var __vue_template__ = __webpack_require__(1016)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e1df3cf8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/maincontentComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e1df3cf8", Component.options)
  } else {
    hotAPI.reload("data-v-e1df3cf8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1013:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1014);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3c01a6b2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e1df3cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./maincontentComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e1df3cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./maincontentComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1014:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-e1df3cf8] {\n  min-height: 70vh;\n     background:rgba(255, 255, 255, 0.6);\n    border-top: 1px solid #eaeaea;\n     padding-bottom: 45px;\n}\n.card[data-v-e1df3cf8] {\n  border-radius: 2px;\n  -webkit-box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.05) !important;\n          box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.05) !important;\n  background: #fff;\n  color: #4a4a4a;\n  cursor: pointer;\n  -webkit-transition: box-shadow 0.3s ease-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-out;\n  transition: -webkit-box-shadow 0.3s ease-out;\n  transition: box-shadow 0.3s ease-out;\n  transition: box-shadow 0.3s ease-out, -webkit-box-shadow 0.3s ease-out;\n}\n.active[data-v-e1df3cf8] {\n  border-color: hsl(207, 43%, 20%) !important;\n}\nh3[data-v-e1df3cf8] {\n  text-transform: initial;\n  font-size: 34px;\n  color: #333;\n}\n.menu-bar[data-v-e1df3cf8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  border-bottom: 1px solid #d4d6db;\n  margin-bottom: 36px;\n}\n.barItem[data-v-e1df3cf8] {\n  padding: 15px 20px;\n  font-size: 15px;\n  cursor: pointer;\n  widows: inherit;\n  border-bottom: 4px solid transparent;\n}\n.barItem[data-v-e1df3cf8]:hover {\n  color: hsl(207, 43%, 20%);\n}\n.rightSide[data-v-e1df3cf8] {\n  width: 100%;\n  height: auto;\n  padding: 0;\n  margin: 0 auto;\n}\n.my-content[data-v-e1df3cf8] {\n  width: 93%;\n  min-height: 50vh;\n  padding: 20px 0;\n  padding-top: 60px;\n \n  margin: 0 0 0 auto ;\n}\n.hover-text[data-v-e1df3cf8] {\n  display: none;\n  border-radius: 5px;\n  position: absolute;\n  height: 50%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  color: white;\n  background-color: hsl(207, 43%, 20%);\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 10px;\n  z-index: 9;\n}\n.card:hover .hover-text[data-v-e1df3cf8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.swiper-inner[data-v-e1df3cf8] {\n  width: 100%;\n  height: 500px;\n  padding-top: 50px;\n  padding-bottom: 50px;\n}\n.swiper-slide[data-v-e1df3cf8] {\n  background-position: center;\n  background-size: cover;\n  width: 500px;\n  height: auto;\n}\n.extraBg[data-v-e1df3cf8] {\n  width: 100%;\n  height: 45%;\n  bottom: 0;\n  position: absolute;\n}\n.btn-primary[data-v-e1df3cf8] {\n  background-color: #3c8dbc !important;\n  border-color: #367fa9 !important;\n  text-transform: capitalize;\n}\n.bg-offwhite[data-v-e1df3cf8] {\n  background-color: #ffffff;\n}\n.card[data-v-e1df3cf8] {\n  position: relative;\n  height: auto;\n}\n.card-img[data-v-e1df3cf8] {\n  height: 150px;\n  overflow: hidden;\n}\n.card-img-top[data-v-e1df3cf8] {\n  height: 150px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n  overflow: hidden;\n}\n.card-body[data-v-e1df3cf8] {\n  cursor: pointer;\n}\n.card:hover .card-img .card-img-top[data-v-e1df3cf8] {\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1);\n}\n.card-title.toCaps[data-v-e1df3cf8] {\n  color: hsl(207, 43%, 20%);\n  text-align: left;\n  font-weight: bold;\n  font-size: 16.5px;\n  height: 44px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.hover-title.toCaps[data-v-e1df3cf8] {\n  color: hsl(207, 43%, 20%);\n  text-align: left;\n  font-weight: bold;\n  font-size: 15px;\n  line-height: 1.3;\n  height: 60px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n  margin-bottom: 11px;\n}\n.card-text.industry[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 15px;\n  height: 20px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n  color: rgba(0, 0, 0, 0.54);\n}\n.expert-heading[data-v-e1df3cf8] {\n  width: 30%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n}\n.expert-heading strong[data-v-e1df3cf8] {\n  font-size: 24.5px;\n  color: #fff;\n}\n.card-desc[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 15px;\n  max-height: 113px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 3;\n  -moz-line-clamp: 3;\n  -ms-line-clamp: 3;\n  -o-line-clamp: 3;\n  line-clamp: 3;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.text[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 16px;\n  height: 300px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 12;\n  -moz-line-clamp: 12;\n  -ms-line-clamp: 12;\n  -o-line-clamp: 12;\n  line-clamp: 12;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.overlay-text[data-v-e1df3cf8] {\n  position: absolute;\n  display: none !important;\n  width: 93%;\n  height: 95%;\n  padding: 10px;\n  background-color: rgba(0, 0, 0, 0.9);\n  border-radius: 4px;\n}\n.card:hover .overlay-text[data-v-e1df3cf8] {\n  display: block !important;\n}\n.card-body[data-v-e1df3cf8] {\n  background-color: #ffffff;\n}\n.fa-4x[data-v-e1df3cf8] {\n  font-size: 2em;\n}\n@media (max-width: 768px) {\nh3[data-v-e1df3cf8] {\n    font-size: 24px;\n}\n.my-content[data-v-e1df3cf8] {\n    padding: 0;\n    padding-top: 60px;\n    padding-bottom: 45px;\n}\n.rightSide[data-v-e1df3cf8] {\n    width: 100%;\n    padding: 0;\n}\n.barItem[data-v-e1df3cf8] {\n    padding: 14px;\n    font-size: 14px;\n}\n.expert-heading[data-v-e1df3cf8] {\n    width: 90%;\n    padding: 15px 20px 10px;\n}\n.expert-heading strong[data-v-e1df3cf8] {\n    font-size: 18px;\n}\n}\n@media (max-width: 425px) {\n.expert-heading strong[data-v-e1df3cf8] {\n    font-size: 16px;\n}\nh3[data-v-e1df3cf8] {\n    font-size: 22px;\n}\n.barItem[data-v-e1df3cf8] {\n    padding: 6px;\n    font-size: 13px;\n}\n.d-no[data-v-e1df3cf8] {\n    display: none;\n}\n.mybox[data-v-e1df3cf8] {\n    width: 10%;\n}\n.fa-3x[data-v-e1df3cf8] {\n    font-size: 1em;\n}\n.card-title.toCaps[data-v-e1df3cf8] {\n    font-size: 13px;\n    line-height: 1.3;\n    height: 34px;\n    margin-bottom: 8px;\n}\n.card-body[data-v-e1df3cf8] {\n    padding: 10px;\n    line-height: 1.2;\n}\n.card-img[data-v-e1df3cf8] {\n    height: 100px;\n}\n.card-img-top[data-v-e1df3cf8] {\n    height: 120px;\n}\n.card[data-v-e1df3cf8] {\n    height: auto;\n    padding: 0 0 10px 0;\n}\n.card-text.industry[data-v-e1df3cf8] {\n    font-size: 12px;\n    margin-bottom: 8px;\n    height: 15px;\n}\n.card-text[data-v-e1df3cf8] {\n    font-size: 12px;\n    margin-bottom: 8px;\n}\n.card-desc[data-v-e1df3cf8] {\n    font-size: 13px;\n    height: 100px;\n    line-height: 1.6;\n    padding: 10px;\n}\n.short-text[data-v-e1df3cf8] {\n    height: 75px;\n    line-height: 1.2;\n}\n.btn[data-v-e1df3cf8] {\n    padding: 4px 18px 6px;\n}\n.bg-offwhite[data-v-e1df3cf8] {\n    background: none !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1015:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "main-content-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      type: "all",
      first: true,
      second: false,
      third: false,
      fourth: false,
      fifth: false,
      sixth: false,
      seventh: false,
      swiperOption: {
        slidesPerView: 5,
        slidesPerColumn: 1,
        spaceBetween: 15,
        breakpoints: {
          425: {
            slidesPerView: 2,
            spaceBetween: 10
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 10
          }
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        }
      },
      videoProduct: [],
      researchProduct: [],
      podcastProduct: [],
      articleProduct: [],
      courseProduct: [],
      allProduct: [],
      prevTop: 0
    };
  },
  created: function created() {
    this.getProducts();
  },
  mounted: function mounted() {},

  watch: {
    // scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 1.44) {
        this.swing = true;
      }
    },
    changeType: function changeType(type) {
      switch (type) {
        case "all":
          this.type = "all";
          this.first = true;
          this.second = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "finance":
          this.type = "finance";
          this.second = true;
          this.first = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "sales":
          this.type = "sales";
          this.third = true;
          this.first = this.second = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "marketing":
          this.type = "marketing";
          this.fourth = true;
          this.first = this.second = this.third = this.fifth = this.sixth = this.seventh = false;
          break;
        case "modeling":
          this.type = "business modeling";
          this.fifth = true;
          this.first = this.second = this.third = this.fourth = this.sixth = this.seventh = false;
          break;
        case "personal_development":
          this.type = "personal development";
          this.sixth = true;
          this.first = this.second = this.third = this.fourth = this.fifth = this.seventh = false;
          break;
        case "branding":
          this.type = "branding";
          this.seventh = true;
          this.first = this.second = this.third = this.fourth = this.fifth = this.sixth = false;
          break;

        default:
          this.first = this.second = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
      }
    },
    topSlide: function topSlide() {
      this.prevTop = this.$refs.articleSlide.swiper.activeIndex;
    },
    articleNext: function articleNext() {
      this.$refs.articleSlide.swiper.slideNext();
      this.topSlide();
    },
    articlePrev: function articlePrev() {
      this.$refs.articleSlide.swiper.slidePrev();
      this.topSlide();
    },
    getProducts: function getProducts() {
      var _this = this;

      axios.get("/api/products").then(function (response) {
        if (response.status === 200) {
          _this.isActive = false;

          response.data.data.forEach(function (item) {
            if (item.prodType == "Market Research") {
              item.marketResearch.coverImage = item.coverImage;
              item.marketResearch.subLevel = item.subscriptionLevel;
              _this.researchProduct.push(item.marketResearch);
            } else if (item.prodType == "Videos") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this.videoProduct.push(item.webinar);
            } else if (item.prodType == "Podcast") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this.podcastProduct.push(item.webinar);
            } else if (item.prodType == "Articles") {
              item.articles.coverImage = item.coverImage;
              item.articles.subLevel = item.subscriptionLevel;
              _this.articleProduct.push(item.articles);
            } else if (item.prodType == "Courses") {
              item.courses.coverImage = item.coverImage;
              item.courses.subLevel = item.subscriptionLevel;

              _this.courseProduct.push(item.courses);
            } else {
              return;
            }
          });

          _this.allProduct = response.data.data;
        }
      });
    },
    watch: function watch(id) {
      this.$router.push({
        name: "Video",
        params: { id: id }
      });
    }
  },
  computed: {
    myProducts: function myProducts() {
      var _this2 = this;

      return this.allProduct.reverse().filter(function (item) {
        if (item.subjectMatter.name.toLowerCase() == _this2.type) {
          return item;
        }
        if (_this2.type === "all") {
          return item;
        }
      });
    }
  },
  components: {}
});

/***/ }),

/***/ 1016:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "my-content d-flex align-center" }, [
      _c(
        "div",
        { staticClass: "rightSide" },
        [
          _c("div", { staticClass: "menu-bar" }, [
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.first },
                on: {
                  click: function($event) {
                    return _vm.changeType("all")
                  }
                }
              },
              [_vm._v("All")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.second },
                on: {
                  click: function($event) {
                    return _vm.changeType("finance")
                  }
                }
              },
              [_vm._v("Finance")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.third },
                on: {
                  click: function($event) {
                    return _vm.changeType("sales")
                  }
                }
              },
              [_vm._v("Sales")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.fourth },
                on: {
                  click: function($event) {
                    return _vm.changeType("marketing")
                  }
                }
              },
              [_vm._v("Marketing")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.fifth },
                on: {
                  click: function($event) {
                    return _vm.changeType("modeling")
                  }
                }
              },
              [_vm._v("Modelling")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.seventh },
                on: {
                  click: function($event) {
                    return _vm.changeType("branding")
                  }
                }
              },
              [_vm._v("Branding")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem d-no",
                class: { active: _vm.sixth },
                on: {
                  click: function($event) {
                    return _vm.changeType("personal_development")
                  }
                }
              },
              [_vm._v("Personal Development")]
            )
          ]),
          _vm._v(" "),
          _c(
            "swiper",
            {
              ref: "articleSlide",
              staticClass: "bg-white p-2",
              attrs: { options: _vm.swiperOption }
            },
            [
              _vm._l(_vm.myProducts, function(item, idx) {
                return _c(
                  "swiper-slide",
                  { key: idx, staticClass: "card border-0" },
                  [
                    _c(
                      "div",
                      { staticClass: "hover-text" },
                      [
                        item.prodType == "Articles"
                          ? _c(
                              "p",
                              { staticClass: "hover-title toCaps text-white" },
                              [
                                _vm._v(
                                  _vm._s(item.articles.title.toLowerCase())
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c(
                              "p",
                              { staticClass: "card-title toCaps text-white" },
                              [_vm._v(_vm._s(item.webinar.title.toLowerCase()))]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Courses"
                          ? _c(
                              "p",
                              { staticClass: "card-title toCaps text-white" },
                              [_vm._v(_vm._s(item.courses.title.toLowerCase()))]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Articles"
                          ? _c("p", {
                              staticClass: "card-desc",
                              domProps: {
                                innerHTML: _vm._s(item.articles.description)
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c("p", { staticClass: "card-desc" }, [
                              _vm._v(_vm._s(item.webinar.description))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Courses"
                          ? _c("p", { staticClass: "card-desc" }, [
                              _vm._v(_vm._s(item.courses.overview))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Articles"
                          ? _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "ArticleSinglePage",
                                    params: {
                                      id: item.articles.product_id,
                                      name: item.articles.title.replace(
                                        /[^a-z0-9]/gi,
                                        ""
                                      )
                                    }
                                  }
                                }
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "elevated_btn btn-sm elevated_btn_sm rounded-pill mt-0 text-main",
                                    attrs: { type: "button" }
                                  },
                                  [_vm._v("View")]
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "Video",
                                    params: { id: item.webinar.product_id }
                                  }
                                }
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "elevated_btn btn-compliment elevated_btn_sm rounded-pill mt-0 text-white",
                                    attrs: { type: "button" }
                                  },
                                  [_vm._v("View")]
                                )
                              ]
                            )
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "card-img" }, [
                      _c("img", {
                        staticClass: "card-img-top",
                        attrs: { src: item.coverImage, alt: "" }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "card-body" }, [
                      item.prodType == "Articles"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.articles.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Videos"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.webinar.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Courses"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.courses.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("p", { staticClass: "card-text text-left mb-1" }, [
                        _vm._v(_vm._s(item.prodType))
                      ]),
                      _vm._v(" "),
                      item.industry
                        ? _c(
                            "p",
                            {
                              staticClass: "card-text industry text-left mb-1"
                            },
                            [_vm._v(_vm._s(item.industry.name))]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Videos"
                        ? _c("p", { staticClass: "card-text text-left mb-1" }, [
                            _vm._v(_vm._s(item.webinar.host))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Articles"
                        ? _c("p", { staticClass: "card-text text-left mb-1" }, [
                            _vm._v(_vm._s(item.articles.writer))
                          ])
                        : _vm._e()
                    ])
                  ]
                )
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "swiper-button-next swiper-button-black",
                attrs: { slot: "button-next" },
                on: { click: _vm.articleNext },
                slot: "button-next"
              }),
              _vm._v(" "),
              _vm.prevTop !== 0
                ? _c("div", {
                    staticClass: "swiper-button-prev swiper-button-black",
                    attrs: { slot: "button-prev" },
                    on: { click: _vm.articlePrev },
                    slot: "button-prev"
                  })
                : _vm._e()
            ],
            2
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "explore-all  px-3" },
      [
        _c(
          "router-link",
          { staticClass: "ml-auto", attrs: { to: "/explore" } },
          [
            _c(
              "button",
              {
                staticClass: "elevated_btn elevated_btn_sm  text-main ml-auto"
              },
              [_vm._v("Explore all")]
            )
          ]
        )
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "expert-heading" }, [
      _c("strong", [_vm._v("Some of our featured resources")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e1df3cf8", module.exports)
  }
}

/***/ }),

/***/ 1022:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1023)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1025)
/* template */
var __vue_template__ = __webpack_require__(1026)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-78dc3c7c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/expertComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-78dc3c7c", Component.options)
  } else {
    hotAPI.reload("data-v-78dc3c7c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1023:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1024);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2fe2169b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-78dc3c7c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./expertComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-78dc3c7c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./expertComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1024:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-78dc3c7c] {\n  padding: 10px 0;\n  padding-bottom: 45px;\n  min-height: 70vh;\n  background-color: rgb(247, 248, 250,.7);\n /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f5f8fb+0,f5f8fb+50,e3ecf4+51,e3ecf4+100 */\n\noverflow: hidden;\nposition: relative;\n}\nh4[data-v-78dc3c7c]{\n  font-size:17px;\n}\n.bg-row[data-v-78dc3c7c]{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n   position: absolute;\n}\n.bg1[data-v-78dc3c7c]{\n    width: 60%;\n    background: rgb(227, 236, 244,.7);\n    -webkit-transform: rotate(20deg);\n            transform: rotate(20deg);\n    height: 156%;\n    top: -44%;\n    position: absolute;\n    left: -14%;\n}\n.expert-heading[data-v-78dc3c7c] {\n  width: 30%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n  margin-top: -10px;\n  position: absolute;\n  z-index: 4;\n}\n.expert-heading strong[data-v-78dc3c7c] {\n  font-size: 24.5px;\n  color: #fff;\n}\n.expert-box[data-v-78dc3c7c] {\n  padding-bottom: 20px;\n}\n.expert-about[data-v-78dc3c7c] {\n  text-align: center;\n  font-size: 15px;\n  color: rgba(55, 58, 60, 0.7);\n  height: 100.5px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 4;\n  -moz-line-clamp: 4;\n  -ms-line-clamp: 4;\n  -o-line-clamp: 4;\n  line-clamp: 4;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  margin-bottom: 8px;\n}\n.my-content[data-v-78dc3c7c] {\n  position: relative;\n  padding: 0px;\n  padding-top: 100px;\n  padding-bottom: 45px;\n  margin: 0 auto;\n  width: 85%;\n}\n.contain[data-v-78dc3c7c] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  padding: 10px;\n  margin: 0 auto;\n}\n.contain img[data-v-78dc3c7c]{\n  width: 100%;\n  height: 100%;\n}\n.expert-image[data-v-78dc3c7c] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border:4px solid hsl(207, 43%, 94%);\n}\n.expert-box:hover .contain .expert-image[data-v-78dc3c7c]{\n  -webkit-transform: scale(1.05);\n          transform: scale(1.05);\n}\n.about[data-v-78dc3c7c] {\n  margin-top: -70px;\n  padding: 20px;\n  height: 315px;\n  background-color: #fff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  border-radius: 5px;\n}\n.about[data-v-78dc3c7c]:hover{\n-webkit-box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n}\n@media (max-width: 1024px) {\n.contain[data-v-78dc3c7c],\n  .expert-image[data-v-78dc3c7c] {\n    width: 110px;\n    height: 110px;\n}\n.my-content[data-v-78dc3c7c] {\n  width: 90%;\n}\n.about[data-v-78dc3c7c]{\n  padding:10px;\n}\n}\n@media (max-width: 768px) {\n.my-content[data-v-78dc3c7c] {\n  width: 100%;\n  padding-top: 60px;\n}\n.main[data-v-78dc3c7c] {\n    min-height: 50vh;\n    padding: 0;\n}\n.my-content[data-v-78dc3c7c] {\n    padding-left: 15px;\n    padding-right: 15px;\n}\n.expert-heading[data-v-78dc3c7c] {\n    width: 50%;\n    padding: 15px 20px 10px;\n}\n.expert-heading strong[data-v-78dc3c7c] {\n    font-size: 18px;\n}\n.contain[data-v-78dc3c7c],\n  .expert-image[data-v-78dc3c7c] {\n    width: 100px;\n    height: 100px;\n}\n.about[data-v-78dc3c7c] {\n    margin-top: -55px;\n    padding: 10px;\n    height: 230px;\n}\nh4[data-v-78dc3c7c] {\n    font-size: 12px;\n}\np[data-v-78dc3c7c]{\n    font-size: 12px;\n}\n.p-3[data-v-78dc3c7c]{\n    padding: 0 !important;\n}\n.mb-3[data-v-78dc3c7c]{\n    margin-bottom: 9px !important;\n}\n.mb-1[data-v-78dc3c7c] {\n    margin-bottom: 0px!important;\n}\n.expert-about[data-v-78dc3c7c]{\n    font-size: 12px;\n    line-height: 1.4;\n    height: 65.5px;\n    margin-bottom: 11px;\n}\n}\n@media (max-width: 375px) {\n.expert-heading strong[data-v-78dc3c7c] {\n  font-size: 16px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'experts-component',
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      allVendors: [],
      swiperOption: {
        slidesPerView: 4,
        spaceBetween: 30,
        breakpoints: {
          425: {
            slidesPerView: 2,
            spaceBetween: 15
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 15
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        },

        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        }
      }
    };
  },
  mounted: function mounted() {
    this.getVendors();
  },


  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 2.3) {
        this.swing = true;
      }
    },
    getVendors: function getVendors() {
      var _this = this;

      axios.get("/api/get-all-vendor").then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            if (item.id !== 1) {
              _this.allVendors.push(item);
            }
          });
        }
      });
    }
  }
});

/***/ }),

/***/ 1026:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "my-content " },
      [
        _c(
          "swiper",
          {
            staticClass: "expertContainer py-3",
            attrs: { options: _vm.swiperOption }
          },
          [
            _vm._l(_vm.allVendors, function(vendor, index) {
              return _c(
                "swiper-slide",
                { key: index, staticClass: "expert-box" },
                [
                  _c("div", { staticClass: "contain rounded-circle" }, [
                    _c("img", {
                      staticClass: "expert-image rounded-circle",
                      attrs: { src: vendor.valid_id, alt: "" }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "about text-center text-white shadow" },
                    [
                      _c("h4", { staticClass: "toCaps mb-1 text-dark" }, [
                        _vm._v(_vm._s(vendor.storeName.toLowerCase()))
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted mb-3" }, [
                        _vm._v("@" + _vm._s(vendor.username))
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "expert-about" }, [
                        _vm._v(_vm._s(vendor.bio))
                      ]),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "PartnerProfile",
                              params: {
                                username: vendor.username
                              }
                            }
                          }
                        },
                        [
                          _c(
                            "span",
                            {
                              staticClass:
                                "elevated_btn elevated_btn_sm btn-compliment text-white"
                            },
                            [_vm._v("read more")]
                          )
                        ]
                      )
                    ],
                    1
                  )
                ]
              )
            }),
            _vm._v(" "),
            _c("div", {
              staticClass: "swiper-button-next swiper-button-black",
              attrs: { slot: "button-next" },
              slot: "button-next"
            }),
            _vm._v(" "),
            _c("div", {
              staticClass: "swiper-button-prev swiper-button-black",
              attrs: { slot: "button-prev" },
              slot: "button-prev"
            })
          ],
          2
        )
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "expert-heading" }, [
      _c("strong", [_vm._v("Our Experts")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-78dc3c7c", module.exports)
  }
}

/***/ }),

/***/ 1027:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1028)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1030)
/* template */
var __vue_template__ = __webpack_require__(1031)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-34594018"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/whyBizguruhComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-34594018", Component.options)
  } else {
    hotAPI.reload("data-v-34594018", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1028:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1029);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("fc111562", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34594018\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./whyBizguruhComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-34594018\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./whyBizguruhComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1029:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-34594018]{\n  background-color: rgba(164, 194, 219,.5);\n}\n.card-img-top[data-v-34594018] {\n  width: 80px;\n  height: 80px;\n  margin: 0 auto;\n}\n.fa-star[data-v-34594018] {\n  color: gold;\n}\n.swiper-inner[data-v-34594018] {\n  width: 100%;\n  height: 400px;\n  padding-top: 50px;\n  padding-bottom: 50px;\n}\n.swiper-slide[data-v-34594018] {\n  background-position: center;\n  background-size: cover;\n  width: 500px;\n  height: 350px;\n}\n.extraBg[data-v-34594018] {\n  width: 100%;\n  height: 45%;\n  bottom: 0;\n  position: absolute;\n  background-color: #f2f5fe;\n}\n.col-md-12[data-v-34594018] {\n  padding: 0;\n}\n.pry-color[data-v-34594018] {\n  color: #24557d;\n}\n.secondView[data-v-34594018] {\n  /* background: #fff; */\n  min-height: 100vh;\n  margin-bottom: -20px;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.underline[data-v-34594018]{\n  border-bottom: 3px solid #333;\n    width: 40%;\n    margin-top: -5px;\n    margin-bottom: 16px;\n}\n.studentView[data-v-34594018] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.studentViews.mobile[data-v-34594018] {\n  display: none;\n}\n.subject_title[data-v-34594018] {\n  margin-top: 10px;\n  font-weight: bold;\n  text-align: center;\n}\n.learn[data-v-34594018] {\n  width: 50%;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 50px !important;\n  color: #a4c2db !important;\n}\n.separator[data-v-34594018] {\n  color: #ffffff;\n}\n.separator2[data-v-34594018] {\n  color: #373a3c;\n}\n.separator-danger[data-v-34594018] {\n  color: #a4c2db;\n}\n.separator[data-v-34594018] {\n  color: #ffffff;\n  margin: 0 auto 20px;\n  max-width: 240px;\n  min-width: 200px;\n  text-align: left;\n  position: relative;\n}\n.separator2[data-v-34594018] {\n  color: #373a3c;\n  margin: 0 auto 20px;\n  max-width: 240px;\n  text-align: center;\n  position: relative;\n}\n*[data-v-34594018] {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator-danger[data-v-34594018]:before,\n.separator-danger[data-v-34594018]:after {\n  border-color: white;\n}\n.separator[data-v-34594018]:before {\n  float: left;\n}\n.separator[data-v-34594018]:before,\n.separator[data-v-34594018]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid white;\n}\n*[data-v-34594018]:before,\n*[data-v-34594018]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator[data-v-34594018]:after {\n  border-color: #a4c2db;\n}\n.separator-danger[data-v-34594018]:before,\n.separator-danger[data-v-34594018]:after {\n  border-color: #a4c2db;\n}\n.separator[data-v-34594018]:after {\n  float: right;\n}\n.separator[data-v-34594018]:before,\n.separator[data-v-34594018]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #a4c2db;\n}\n*[data-v-34594018]:before,\n*[data-v-34594018]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator2[data-v-34594018]:before {\n  float: left;\n}\n.separator2[data-v-34594018]:before,\n.separator2[data-v-34594018]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #a4c2db;\n}\n*[data-v-34594018]:before,\n*[data-v-34594018]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator2[data-v-34594018]:after {\n  border-color: #a4c2db;\n}\n.separator-danger[data-v-34594018]:before,\n.separator-danger[data-v-34594018]:after {\n  border-color: #a4c2db;\n}\n.separator2[data-v-34594018]:after {\n  float: right;\n}\n.separator2[data-v-34594018]:before,\n.separator2[data-v-34594018]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #a4c2db;\n}\n.why-biz[data-v-34594018] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  background-color: transparent;\n  padding: 10px 0;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: row-reverse;\n            flex-direction: row-reverse;\n    width:80%;\n    margin: 0 auto;\n      padding-top: 50px;\n    padding-bottom: 45px;\n}\n.whyB[data-v-34594018] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: auto;\n  background-position: center;\n  background-attachment: fixed;\n  background-repeat: no-repeat;\n  background-size: cover;\n  /* margin-bottom:50px; */\n}\n.whyImg[data-v-34594018] {\n  width: 50%;\n  -webkit-transition: all .4s;\n  transition: all .4s;\n  overflow: hidden;\n  padding:30px 0;\n}\n.whyImg:hover img[data-v-34594018] {\n  -webkit-transform: scale(1.01);\n          transform: scale(1.01);\n}\n.whyImg img[data-v-34594018] {\n  width: 100%;\n  height: 100%;\n}\n.whyFirst[data-v-34594018] {\n  width: 40%;\n  margin-left: 0px;\n  padding: 25px 0;\n  color:#373a3c;\n}\n/* .bgShadow {\n  position: absolute;\n  height: 500px;\n  width: 100%;\n  background: rgb(0, 0, 0,.6);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n} */\n.askBody[data-v-34594018] {\n  padding: 10px 40px;\n}\n.card[data-v-34594018] {\n  border: none !important;\n  padding: 10px 15px;\n}\n.card-body[data-v-34594018] {\n  padding: 0 !important;\n}\n.card[data-v-34594018] {\n  -webkit-box-shadow: none;\n          box-shadow: none;\n}\n.student[data-v-34594018] {\n  background: rgba(163, 194, 220, 0.2);\n  border-radius: 5px;\n  -webkit-box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n          box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n  height: auto;\n  padding: 10px 5px;\n  text-align: center;\n  border-radius: 30px;\n}\n.test_name[data-v-34594018] {\n  font-weight: bold;\n  margin-bottom: 5px;\n}\n.test_story[data-v-34594018] {\n  text-align: left;\n}\nh3.card-title[data-v-34594018] {\n  font-weight: bolder;\n  font-size: 34px;\n  color:#333;\n}\n.card-text[data-v-34594018] {\n  font-weight: 400;\n  font-size: 16px;\n}\n.card-body[data-v-34594018] {\n  color: #373a3c !important;\n}\n/* .card-text:last-child {\n  margin-bottom: 0;\n  font-size: 18px;\n} */\n.believe[data-v-34594018] {\n  font-size: 16px;\n  font-weight:bold;\n  text-transform: uppercase;\n}\nb[data-v-34594018] {\n  color: #373a3c !important;\n}\n/* img {\n  max-height: 83px;\n} */\n.bizguruh[data-v-34594018] {\n  margin-left: auto;\n  margin-right: auto;\n}\n.btn-bizguruh[data-v-34594018] {\n  width: 100%;\n  margin-left: auto;\n  margin-right: auto;\n  background: hsl(207, 45%, 75%) !important;\n  color: #f3f3f3;\n  cursor: pointer;\n  padding: 10px 15px;\n  border-radius: 10px !important;\n  text-transform: inherit;\n}\n.btn-bizguruh[data-v-34594018]:hover {\n  background: #f3f3f3 !important;\n  color: #a4c2db;\n}\n.know[data-v-34594018] {\n  color: #a4c2db !important;\n}\n.mobile-slide[data-v-34594018] {\n  display: none;\n}\n.main-text[data-v-34594018] {\n  font-size: 20px;\n  line-height: 1.6;\n}\n.ptext[data-v-34594018] {\n  color: #373a3c;\n  overflow: hidden;\n}\n.theme--dark.v-btn[data-v-34594018]:not(.v-btn--icon):not(.v-btn--flat) {\n  background-color: #ffffff;\n}\n.v-btn:not(.v-btn--outline).accent[data-v-34594018],\n.v-btn:not(.v-btn--outline).error[data-v-34594018],\n.v-btn:not(.v-btn--outline).info[data-v-34594018],\n.v-btn:not(.v-btn--outline).primary[data-v-34594018],\n.v-btn:not(.v-btn--outline).secondary[data-v-34594018],\n.v-btn:not(.v-btn--outline).success[data-v-34594018],\n.v-btn:not(.v-btn--outline).warning[data-v-34594018] {\n  color: #a4c2db;\n}\n.v-btn__content[data-v-34594018] {\n  color: #a4c2db;\n}\n.v-btn[data-v-34594018] {\n  height: 40px;\n  border-radius: 5px;\n}\n.why-biz p.ptext[data-v-34594018] {\n  font-size: 16px;\n  line-height: 1.6;\n  color: rgba(0, 0, 0, 0.64);\n  font-family: \"Poppins\";\n}\n.why-icons[data-v-34594018] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  width: 100%;\n  margin-top: 30px;\n}\n.why-icon[data-v-34594018] {\n  height: auto;\n  padding: 10px;\n  background: rgba(163, 194, 220, 0.2);\n  margin-bottom: 12px;\n  -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n          box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n  text-align: center;\n}\n.why-icon img[data-v-34594018] {\n  width: 80px;\n  height: auto;\n}\n.why-icon.ptext[data-v-34594018] {\n  font-size: 14px;\n}\n.why-icon[data-v-34594018] {\n  border-radius: 5px;\n}\n@media (max-width: 768px) {\n.whyImg[data-v-34594018]{\n    padding:30px 0;\n}\n.main[data-v-34594018] {\n    width: 100%;\n}\n.why-biz[data-v-34594018]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n}\n.whyFirst[data-v-34594018]{\n    width: 100%;\n    margin-left: 0;\n    padding: 15px;\n    color: #373a3c;\n    text-align: center !important;\n    min-height: 250px;\n}\n.whyImg[data-v-34594018]{\n    width:90%;\n}\nh3.card-title[data-v-34594018]{\n    font-size:28px;\n}\nh4.card-title[data-v-34594018]{\n    font-size:16px;\n}\n.card-text[data-v-34594018]{\n    font-size: 18px;\n}\n.underline[data-v-34594018]{\n    margin-left:auto;\n    margin-right:auto;\n    width:20%;\n}\n}\n@media (max-width: 575px) {\n.why-icon[data-v-34594018] {\n    margin-bottom: 40px;\n}\n.askBody[data-v-34594018] {\n    padding: 0 15px;\n}\n}\n@media (max-width: 575px) {\n.why-biz[data-v-34594018]{\n    width:90%;\n}\n.w-25[data-v-34594018] {\n    width: 50% !important;\n}\n.card-img-top[data-v-34594018] {\n    width: 50px;\n    height: 50px;\n    margin: 0 auto;\n}\n  /* .why-biz {\n    height: auto;\n  } */\n.swiper-inner[data-v-34594018] {\n    width: 100%;\n    height: 300px;\n    padding-top: 0px;\n    padding-bottom: 0px;\n}\n.swiper-slide[data-v-34594018] {\n    background-position: center;\n    background-size: cover;\n    width: 300px;\n    height: 250px;\n}\n.subject_title[data-v-34594018] {\n    padding: 10px 10px;\n    font-size: 24px;\n}\n.askBody[data-v-34594018] {\n    padding: 0 15px;\n}\n.learn[data-v-34594018] {\n    margin-top: 30px !important;\n}\n.whyB[data-v-34594018],\n  .bgShadow[data-v-34594018] {\n    height: 400px;\n}\n.bgShadow[data-v-34594018] {\n    margin: 0;\n}\n.main-text[data-v-34594018] {\n    width: 100%;\n    padding: 0 20px;\n    font-size: 16px;\n}\n.why-icons[data-v-34594018] {\n    display: none !important;\n}\n.why-icon[data-v-34594018] {\n    margin-bottom: 0;\n    margin-top: 10px;\n    height: auto;\n}\n  /* .card-body{\n      padding: 0;\n  } */\n.btn-bizguruh[data-v-34594018] {\n    border-radius: 5px !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1030:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "why-bizguruh-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
  },

  watch: {
    scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 2.75) {
        this.swing = true;
      }
    }
  }
});

/***/ }),

/***/ 1031:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "pt-3 main" }, [
    _c("div", { staticClass: "why-biz" }, [
      _c("div", { staticClass: "whyImg animated fadeIn" }, [
        _vm.swing
          ? _c("img", {
              staticClass: "animated zoomIn",
              attrs: { src: "/images/mockups/why.png", alt: "" }
            })
          : _vm._e()
      ]),
      _vm._v(" "),
      _vm._m(0)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-left whyFirst" }, [
      _c("h3", { staticClass: " animated fadeIn mb-5 why" }, [
        _vm._v("Why Bizguruh ?")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "card-text main-text animated fadeIn " }, [
        _vm._v(
          "\n        We really want to see you win, so we collaborate with industry experts to empower you with\n        honest and practical business insight across various sectors; and highlight business opportunities for you to engage.\n      "
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c(
        "p",
        {
          staticClass: "card-text believe text-compliment animated slideInUp "
        },
        [_vm._v("We believe that you can begin right where you are!.")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-34594018", module.exports)
  }
}

/***/ }),

/***/ 1380:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1381);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("09c21e22", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-515e2a4d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./indexComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-515e2a4d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./indexComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1381:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-515e2a4d] {\n  padding: 0px !important;\n}\n.bg[data-v-515e2a4d] {\n  background-image: url(\"/images/curve2.jpg\");\n  background-size: cover;\n}\n", ""]);

// exports


/***/ }),

/***/ 1382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent__ = __webpack_require__(1007);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent__ = __webpack_require__(1012);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent__ = __webpack_require__(944);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_expertComponent__ = __webpack_require__(1022);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_expertComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_expertComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_whyBizguruhComponent__ = __webpack_require__(1027);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_whyBizguruhComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_whyBizguruhComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_userFooterComponent__ = __webpack_require__(939);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_userFooterComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_userFooterComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/* harmony default export */ __webpack_exports__["default"] = ({
  name: "index-component",
  data: function data() {
    return {
      productListing: true,
      courseWork: false,
      helpCourse: false,
      certificate: false
    };
  },
  components: {
    "app-banner": __WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent___default.a,
    "app-category": __WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent___default.a,
    // 'app-exclusive': ExclusiveResource,
    "app-bizguruh": __WEBPACK_IMPORTED_MODULE_5__components_whyBizguruhComponent___default.a,

    "app-footer": __WEBPACK_IMPORTED_MODULE_6__components_userFooterComponent___default.a,
    MainContent: __WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent___default.a,
    Testimonial: __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent___default.a,
    Expert: __WEBPACK_IMPORTED_MODULE_4__components_expertComponent___default.a
  },
  methods: {
    tabDisplay: function tabDisplay(s) {
      switch (s) {
        case 1:
          this.productListing = true;
          this.courseWork = false;
          this.helpCourse = false;
          this.certificate = false;
          break;
        case 2:
          this.productListing = false;
          this.courseWork = true;
          this.helpCourse = false;
          this.certificate = false;
          break;
        case 3:
          this.productListing = false;
          this.courseWork = false;
          this.helpCourse = true;
          this.certificate = false;
          break;
        case 4:
          this.productListing = false;
          this.courseWork = false;
          this.helpCourse = false;
          this.certificate = true;
          break;
      }
    }
  }
});

/***/ }),

/***/ 1383:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("app-banner"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "bg" },
        [
          _c("main-content"),
          _vm._v(" "),
          _c("expert"),
          _vm._v(" "),
          _c("app-category"),
          _vm._v(" "),
          _c("app-bizguruh"),
          _vm._v(" "),
          _c("testimonial"),
          _vm._v(" "),
          _c("app-footer")
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-515e2a4d", module.exports)
  }
}

/***/ }),

/***/ 550:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1380)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1382)
/* template */
var __vue_template__ = __webpack_require__(1383)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-515e2a4d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/indexComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-515e2a4d", Component.options)
  } else {
    hotAPI.reload("data-v-515e2a4d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 883:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(884)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(886)
/* template */
var __vue_template__ = __webpack_require__(887)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a85176d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoriesBarComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a85176d", Component.options)
  } else {
    hotAPI.reload("data-v-7a85176d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 884:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(885);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("35bd739c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 885:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.overlay-bus[data-v-7a85176d]{\n  position:absolute;\n  width:100%;\n  height:100%;\n  top:0;\n  background:rgba(255, 255, 255,.5);\n  z-index:0;\n}\n.it[data-v-7a85176d]{\n   position:relative;\n}\n.howIt[data-v-7a85176d]{\n  padding:65px 0 100px;\n  z-index:3;\n  position:relative;\n}\n.form[data-v-7a85176d] {\n  width: 65%;\n  margin-right: auto;\n  background: hsl(207, 43%, 20%);\n}\nh3.text-center[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n}\n.bms-heading[data-v-7a85176d] {\n  width: 40%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n}\n.expert-heading strong[data-v-7a85176d] {\n  font-size: 34px;\n  color: #fff;\n}\n.bms[data-v-7a85176d] {\n  padding: 0px;\n  width: 100%;\n  margin: 0 auto;\n  padding-top: 40px;\n  padding-bottom: 5px;\n}\n.bms_em[data-v-7a85176d]{\n  padding:40px;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  background:#f7f8fa;\n}\n.leftSlide p[data-v-7a85176d]{\n  font-size: 38px;\n  line-height:1.3;\n}\n.swiper[data-v-7a85176d] {\n  height: 600px;\n  width: 100%;\n}\n.accounting_slider[data-v-7a85176d] {\n  margin-top: 50px;\n  min-height: 400px;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient:horizontal;\n  -webkit-box-direction:reverse;\n      -ms-flex-direction:row-reverse;\n          flex-direction:row-reverse;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width:80%;\n  margin:0 auto;\n}\n.leftSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  text-align:center;\n}\n.rightSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0;\n}\n.rightSlide p[data-v-7a85176d]{\n  font-size: 32px;\n}\n.rightSlide img[data-v-7a85176d]{\n  width:70%;\n  height:auto;\n}\n.slide_button[data-v-7a85176d] {\n  width: 80%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin:0 auto\n}\n.my-slides[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n  background: white;\n}\n.bms_img[data-v-7a85176d]{\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: left;\n     object-position: left;\n}\n.profit[data-v-7a85176d] {\n  height: 50px;\n  margin: 30px auto 10px;\n}\n.gp[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n  text-align: center;\n}\n.gProfit[data-v-7a85176d] {\n  font-size: 24px;\n  font-weight: bold;\n  color: #fff;\n}\n.w-40[data-v-7a85176d] {\n  width: 40% !important;\n}\n.w-60[data-v-7a85176d] {\n  width: 70% !important;\n}\n.spin[data-v-7a85176d] {\n  right: 10px;\n  top: 43%;\n}\n.boxes[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 80%;\n  min-height: 50vh;\n  margin: 0 auto;\n \n  padding-bottom: 45px;\n  padding-top: 60px;\n}\n.leftBox[data-v-7a85176d] {\n  width: 50%;\n  padding: 40px 0px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.rightBox[data-v-7a85176d] {\n  width: 50%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 60px 0px;\n}\n.generate-text[data-v-7a85176d] {\n  padding: 10px 0;\n  font-size: 32px;\n  line-height: 1.6;\n  color: rgba(55, 58, 60);\n}\n.input-group[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n.input-group-text[data-v-7a85176d] {\n  font-size: 16px;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  right: -12px;\n  top: -17px;\n  cursor: pointer;\n  color: black;\n  padding: 3px 4px !important;\n  border-radius: 50%;\n  z-index: 6;\n  background: white;\n}\n.askBiz[data-v-7a85176d] {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  cursor: pointer;\n  z-index: 100;\n  text-align: center;\n  width: 25%;\n  height: auto;\n}\n.fa-2x[data-v-7a85176d] {\n  font-size: 1.5em;\n}\n.bizzy[data-v-7a85176d] {\n  width: 50px;\n  height: 50px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.goAsk[data-v-7a85176d] {\n  font-size: 18px;\n  font-weight: bold;\n  color: #a4c2db;\n}\n.askTimes[data-v-7a85176d] {\n  position: absolute;\n  top: -18px;\n  right: -18px;\n  cursor: pointer;\n}\n.askClose[data-v-7a85176d] {\n  margin-left: -7%;\n  text-align: right;\n  opacity: 0.4;\n  width: 9% !important;\n  padding: 0 !important;\n}\n.askClose[data-v-7a85176d]:hover {\n  opacity: 1;\n}\n.askIcons[data-v-7a85176d] {\n  -webkit-box-orient: vertical !important;\n  -webkit-box-direction: normal !important;\n      -ms-flex-direction: column !important;\n          flex-direction: column !important;\n  margin: 0 !important;\n  margin-left: auto !important;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  top: 10px;\n  right: 10px;\n  cursor: pointer;\n}\n.news-box[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.news-button[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.newsletter[data-v-7a85176d] {\n  width: 400px;\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: fixed;\n  top: 40%;\n  right: 15px;\n  z-index: 99;\n  overflow: hidden;\n  background: #d1e8fd;\n}\n.newsletter_image[data-v-7a85176d] {\n  position: absolute;\n  right: -26px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: right;\n     object-position: right;\n  height: 100%;\n}\n.btn-red[data-v-7a85176d] {\n  background-color: red !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.btn-success[data-v-7a85176d] {\n  background-color: green !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.form-control[data-v-7a85176d]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d] {\n  border: none;\n  border-bottom: 1px solid #ccc;\n  border-radius: 5px;\n  height: 38px;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n/* .howw {\n  height: 100vh;\n} */\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.separator-danger[data-v-7a85176d] {\n  color: #5b84a7;\n}\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n  margin: 0 auto 20px;\n  max-width: 240px;\n  text-align: center;\n  position: relative;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:before {\n  float: left;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:after {\n  float: right;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.center[data-v-7a85176d] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.column[data-v-7a85176d] {\n  float: left;\n  width: 33.3%;\n  padding: 5px;\n  background: #ffffff;\n}\n.card-header[data-v-7a85176d] {\n  background: hsl(207, 45%, 95%);\n}\n.taskImg[data-v-7a85176d] {\n  color: #5b84a7 !important;\n  text-align: center;\n  font-weight: normal;\n  text-align: center;\n  font-size: calc(14px + (20 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  width: 100%;\n}\n.howw[data-v-7a85176d] {\n  padding-bottom: 60px;\n}\n\n/* .taskImg[data-v-632021da] {\n    color: #ffffff !important;\n    font-weight: 600;\n    text-align: center;\n    font-size: 14px;\n    background: #143048;\n    #6a88a2\n    border-left: 2px solid white;\n    border-right: 2px solid white;\n} */\n.taskImg[data-v-7a85176d]:hover {\n  background: #5b84a7 !important;\n  color: #ffffff !important;\n}\n.taskImg .f-35[data-v-7a85176d] {\n  font-size: 25px;\n}\n/* .taskImg .f-35:hover{\n\n \tcolor: #ffffff !important;\n} */\n\n/* Clear floats after image containers */\n.row[data-v-7a85176d]::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n.courseBody[data-v-7a85176d] {\n  font-size: calc(13px + (18 - 16) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.imgTask[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.text-grey[data-v-7a85176d] {\n  color: #ccc !important;\n}\n.bg-grey[data-v-7a85176d] {\n  background: #ccc !important;\n  border: 1px solid #ccc !important;\n}\n.works[data-v-7a85176d] {\n  text-align: center;\n  padding: 60px 0 0px 0;\n  font-weight: bolder;\n  color: rgba(3, 12, 38, 0.6);\n}\n.oneContainer[data-v-7a85176d],\n.twoContainer[data-v-7a85176d],\n.threeContainer[data-v-7a85176d],\n.fourContainer[data-v-7a85176d],\n.fiveContainer[data-v-7a85176d] {\n  position: relative;\n  height: 100%;\n  border-radius: 10px;\n  width: 130%;\n  /* overflow: hidden; */\n}\n.leftContainer[data-v-7a85176d] {\n  width: 50%;\n  max-height: 675px;\n  margin-right: 20px;\n  /* overflow-y: scroll; */\n}\n.leftContainer[data-v-7a85176d]::-webkit-scrollbar {\n  color: #5b84a7;\n}\n.d[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.oneContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.twoContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.threeContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fourContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fiveContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.howContent[data-v-7a85176d] {\n  background: #ffffff;\n  /* margin-left: 20px; */\n  max-width: 55%;\n  border-radius: 5px;\n\n  overflow: hidden;\n}\n.courseTitles[data-v-7a85176d] {\n  color: rgba(3, 12, 38, 0.74);\n  margin-bottom: 8px;\n  font-weight: bold;\n  font-size: 16px;\n}\n.hws[data-v-7a85176d] {\n  padding: 10px 0;\n  cursor: pointer;\n}\n.howBorderFive[data-v-7a85176d] {\n  border-top: 1px solid #5b84a7;\n}\n.howBorder[data-v-7a85176d],\n.howBorderThird[data-v-7a85176d],\n.howBorderFour[data-v-7a85176d] {\n  border-bottom: 1px solid #5b84a7;\n  border-top: 1px solid #5b84a7;\n}\n.howCount[data-v-7a85176d] {\n  position: relative;\n  width: 16px;\n  height: 16px;\n  border: 1px solid rgb(3, 12, 38, 0.7);\n  border-radius: 60%;\n  background: hsl(225, 86%, 8%);\n  margin-right: 10px;\n  font-size: 12px;\n  margin-top: 3px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.howCon[data-v-7a85176d] {\n  width: 80%;\n}\n.num[data-v-7a85176d] {\n  padding: 6px;\n  color: white;\n}\n.cat-arrow[data-v-7a85176d] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: white;\n  font-size: 34px;\n}\n.brand[data-v-7a85176d] {\n  background: #c3f1d4;\n  margin: 13px;\n  padding: 13px 40px 400px;\n  position: relative;\n}\n.innovation[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7f0fb;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.procurement[data-v-7a85176d] {\n  margin: 13px;\n  background: #ffd3e6;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.timemgt[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7e6f4;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.sales[data-v-7a85176d] {\n  margin: 13px;\n  background: #dbf1fa;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.diver[data-v-7a85176d] {\n  margin: 40px;\n}\n@media (max-width: 1024px) {\n.form[data-v-7a85176d]{\n    width:90%;\n}\n}\n@media (max-width: 768px) {\n}\n@media only screen and (max-width: 768px) {\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:90%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:50%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.boxes[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 14px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 0;\n    min-height: 100px;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 28px;\n    text-align: center;\n}\n.form[data-v-7a85176d] {\n    width: 90%;\n}\n.accounting_slider[data-v-7a85176d]{\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:28px;\n}\n}\n@media only screen and (max-width: 575px) {\n.boxes[data-v-7a85176d] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    padding: 30px 20px;\n}\n.gp[data-v-7a85176d] {\n    font-size: 12px;\n}\n.gProfit[data-v-7a85176d] {\n    font-size: 16px;\n}\n.form[data-v-7a85176d] {\n    margin: 0 auto;\n    width: 90%;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 13px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 18.5px;\n    text-align: center;\n}\n.newsletter[data-v-7a85176d] {\n    width: 300px;\n}\n.taskImg .f-35[data-v-7a85176d] {\n    font-size: 16px;\n}\n.oneContainer[data-v-7a85176d],\n  .twoContainer[data-v-7a85176d],\n  .threeContainer[data-v-7a85176d],\n  .fourContainer[data-v-7a85176d],\n  .fiveContainer[data-v-7a85176d] {\n    border-radius: 0;\n}\n.howItWork[data-v-7a85176d] {\n    display: block !important;\n    margin-right: 0;\n    margin-left: 0;\n}\n.leftContainer[data-v-7a85176d] {\n    padding: 0 35px;\n    width: 100%;\n}\n.howContent[data-v-7a85176d] {\n    width: 100%;\n    max-width: 100%;\n    height: auto;\n}\n.hws[data-v-7a85176d] {\n    margin: 0;\n\n    padding: 20px 0 5px;\n}\n.howCount[data-v-7a85176d] {\n    width: 15px;\n    height: 15px;\n}\n.howCon[data-v-7a85176d] {\n    width: 100%;\n}\n.taskImg[data-v-7a85176d] {\n    color: #5b84a7 !important;\n    font-size: calc(8px + (26 - 8) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.works[data-v-7a85176d] {\n    font-size: calc(20px + (26 - 20) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n    margin: 20px 0 10px 0;\n    padding: 20px 0 0 0;\n}\n.num[data-v-7a85176d] {\n    top: -5px;\n    right: 3px;\n    font-size: 12px;\n}\n.fa-2x[data-v-7a85176d] {\n    font-size: 1em;\n}\n.scrollUp[data-v-7a85176d] {\n    padding: 5px 7px;\n}\n@media (max-width: 425px) {\n.goAsk[data-v-7a85176d] {\n      font-size: 16px !important;\n}\n.askBiz[data-v-7a85176d] {\n      width: 75%;\n}\n.bizzy[data-v-7a85176d] {\n      width: 40px;\n}\n.askClose[data-v-7a85176d] {\n      width: 11% !important;\n}\n.fa-times[data-v-7a85176d],\n    .fa-plus[data-v-7a85176d] {\n      font-size: 0.7em;\n      top: 0;\n      right: -16px;\n}\n.askTimes[data-v-7a85176d] {\n      top: -8px;\n      right: 8px;\n}\n.card-title[data-v-7a85176d]{\n      font-size:13px;\n}\n.card-text[data-v-7a85176d]{\n       font-size:13px;\n}\n.slide_button[data-v-7a85176d]{\n    width:100%\n}\n.leftSlide[data-v-7a85176d]{\n    width:100%;\n    padding:20px 5px\n}\n.mini-content[data-v-7a85176d]{\n    padding-top:10px;\n}\n.mini-content-title[data-v-7a85176d],  .bms-heading strong[data-v-7a85176d]{\n    font-size:16px;\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.mini-content-title[data-v-7a85176d]{\n    width:60%;\n    margin-left:-4.5%;\n}\n.business-words[data-v-7a85176d]{\n    display: grid;\n    grid-template-rows: auto auto;\n    grid-template-columns: repeat(10,145px);\n    grid-column-gap: 20px;\n    grid-row-gap: 10px;\n    margin-bottom: 20px;\n    overflow-x: scroll;\n}\n.single-word[data-v-7a85176d]{\n    padding: 10px 9px;\n    font-size: 12px;\n    text-align: center;\n}\n.bms-heading strong[data-v-7a85176d]{\n    font-size:18px;\n}\n.mini-content[data-v-7a85176d]{\n    padding-left:15px;\n    padding-right:15px;\n    padding-top:30px;\n}\n.mini-content-title[data-v-7a85176d]{\n    font-size:18px;\n    margin-left:-3%;\n      padding: 15px 20px 13px;\n}\n.overviewMain[data-v-7a85176d]{\n    width:80%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:85%;\n    padding: 20px 20px 15px;\n}\n.accounting_slider[data-v-7a85176d]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height:auto;\n    width:100%;\n    padding: 40px 0;\n}\n.rightSlide[data-v-7a85176d],.leftSlide[data-v-7a85176d]{\n    width:80%;\n    margin:0 auto\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.leftSlide[data-v-7a85176d]{\n    padding:20px;\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:24px;\n    text-align:center !important;\n    padding:10px;\n}\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "categories-bar-component",
  data: function data() {
    var _ref;

    return _ref = {
      gp: 0,
      revenue: null,
      cost: null,
      loader: false,
      calculate: "Continue to opex",
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      askOpen: false,
      show: false,
      first_name: "",
      last_name: "",
      email: "",
      one: true,
      two: false,
      three: false,
      four: false,
      five: false,
      howBor: false,
      howBoro: false,
      howBorod: false,
      howBorod4: false,
      howBorod5: false,
      textGreyOne: false,
      textGreyTwo: true,
      textGreyThree: true,
      textGreyFour: true,
      textGreyFive: true
    }, _defineProperty(_ref, "scrollPos", 0), _defineProperty(_ref, "currentHeight", 0), _defineProperty(_ref, "scrollUpButton", false), _defineProperty(_ref, "currencySymbol", "NGN "), _defineProperty(_ref, "position", "prefix"), _ref;
  },

  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      window.addEventListener("scroll", function (e) {
        _this.scrollPos = window.scrollY;
        _this.currentHeight = window.innerHeight;
      });
    }
  },

  computed: {
    currency: function currency() {
      return _defineProperty({}, this.position, this.currencySymbol);
    }
  },
  watch: {
    scrollPos: "scrolling",
    cost: "calcGp"
  },
  methods: {
    goToDemo: function goToDemo() {
      var user = localStorage.getItem('authUser');
      if (user !== null) {
        var demo = localStorage.getItem('demo');

        this.$router.push({
          name: 'AccountingDemo',
          query: {
            redirect_to: 'demo'
          }
        });
      } else {
        this.$router.push({
          name: 'auth',
          params: { name: 'register' },
          query: {
            redirect_from: 'explore'
          }
        });
      }
    },
    scrolling: function scrolling() {
      if (this.scrollPos > window.innerHeight * 0.3) {
        this.scrollUpButton = true;
      } else {
        this.scrollUpButton = false;
      }

      if (this.scrollPos > window.innerHeight * 1.2) {
        this.swing = true;
      }
    },
    calcGp: function calcGp() {
      this.gp = this.revenue - this.cost;
    },
    calc: function calc() {
      var _this2 = this;

      if (this.gp != 0) {
        this.loader = true;
        this.calculate = "processing ...";
        setTimeout(function () {
          _this2.calculate = "calculating ...";
        }, 2000);

        setTimeout(function () {
          _this2.calculate = "calculate";
          _this2.loader = false;
          _this2.$router.push('/account/pricing?session=pricing');
        }, 4000);
      }
    },
    currencyFormat: function currencyFormat(num) {
      if (num !== null) {
        return "NGN " + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      } else {
        return "NGN 0.00";
      }
    },
    openAsk: function openAsk() {
      this.$router.push("/ask-bizguruh");
    },
    askClose: function askClose() {
      this.askOpen = !this.askOpen;
    },
    showNews: function showNews() {
      this.show = false;
    },
    subscribe: function subscribe() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        email: user.email,
        first_name: user.name,
        last_name: user.name
      };
      axios.post("/api/subscribe", data).then(function (response) {
        if (response.status === 200) {
          _this3.$toasted.success("Subscription successful");
          localStorage.removeItem("news");
          _this3.email = _this3.first_name = _this3.last_name = "";
          _this3.show = false;
        }
        {}
      });
    },
    switchTab: function switchTab(params) {
      switch (params) {
        case "one":
          this.one = this.howBor = this.textGreyTwo = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.two = this.three = this.four = this.five = this.howBoro = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyOne = false;
          break;
        case "two":
          this.two = this.howBoro = this.textGreyOne = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.one = this.three = this.four = this.five = this.howBor = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyTwo = false;
          break;
        case "three":
          this.three = this.howBorod = this.textGreyOne = this.textGreyTwo = this.textGreyFour = this.textGreyFive = true;
          this.one = this.two = this.four = this.five = this.howBor = this.howBoro = this.howBorod4 = this.howBorod5 = this.textGreyThree = false;
          break;
        case "four":
          this.four = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.textGreyFive = this.howBorod4 = true;
          this.one = this.two = this.three = this.five = this.howBor = this.howBoro = this.howBorod5 = this.howBorod = this.textGreyFour = false;
          break;
        case "five":
          this.five = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.howBorod5 = this.textGreyFour = true;
          this.one = this.two = this.three = this.four = this.howBor = this.howBoro = this.howBorod4 = this.howBorod = this.textGreyFive = false;
          break;
        default:
          this.one = this.two = this.three = this.four = this.five = false;
      }
    }
  }
});

/***/ }),

/***/ 887:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "it" }, [
    _c("div", { staticClass: "overlay-bus" }),
    _vm._v(" "),
    _c("div", { staticClass: "howIt" }, [
      _c("h3", { staticClass: "mb-5 desc_header josefin " }, [
        _vm._v("Business Management Solutions")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "boxes" }, [
        _c("div", { staticClass: "leftBox" }, [
          _c(
            "form",
            {
              staticClass: "form shadow p-lg-5 p-md-4 p-sm-3 p-4 rounded",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.calc($event)
                }
              }
            },
            [
              _c("h3", { staticClass: "text-center mb-3" }, [
                _vm._v("Account Statement Generator")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.revenue,
                      callback: function($$v) {
                        _vm.revenue = $$v
                      },
                      expression: "revenue"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(1)
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.cost,
                      callback: function($$v) {
                        _vm.cost = $$v
                      },
                      expression: "cost"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(3)
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "profit" }, [
                _c("p", { staticClass: "gp" }, [
                  _c("span", { staticClass: "gProfit" }, [
                    _vm._v(_vm._s(_vm.currencyFormat(this.gp)))
                  ]),
                  _vm._v(" Gross profit\n           ")
                ])
              ]),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "elevated_btn bg-white text-main mx-auto",
                  attrs: { type: "submit" }
                },
                [
                  _vm._v(
                    "\n           " + _vm._s(_vm.calculate) + "\n           "
                  ),
                  _vm.loader
                    ? _c("span", {
                        staticClass:
                          "spinner-grow spinner-grow-sm spin text-grey"
                      })
                    : _vm._e()
                ]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "rightBox" }, [
          _c(
            "p",
            {
              staticClass: "generate-text animated slideInUp",
              class: { "d-none": !_vm.swing }
            },
            [
              _vm._v(
                "\n         Ditch the paperwork, get organized and track your business\n         "
              ),
              _c("strong", [_vm._v("PROFITABILITY")]),
              _vm._v(" and\n         "),
              _c("strong", [_vm._v("GROWTH")]),
              _vm._v(" in\n         "),
              _c("strong", [_vm._v("REAL-TIME")]),
              _vm._v("!\n       ")
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _vm.$route.name !== "IndexPage"
        ? _c("section", {}, [
            _c("div", { staticClass: "bms" }, [
              _c("div", { staticClass: "accounting_slider" }, [
                _c("div", { staticClass: "rightSlide" }, [
                  _vm.swing
                    ? _c("img", {
                        staticClass: "bms_img animated slideInUp",
                        attrs: { src: "/images/word_threed.png", alt: "" }
                      })
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "leftSlide" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "slide_button" },
                    [
                      _c(
                        "button",
                        {
                          staticClass: "elevated_btn btn-compliment text-white",
                          attrs: { type: "button" },
                          on: { click: _vm.goToDemo }
                        },
                        [_vm._v("Free Demo Now")]
                      ),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "SubscriptionProfile",
                              params: {
                                level: 0
                              },
                              query: {
                                name: "subscribe-accounting"
                              }
                            }
                          }
                        },
                        [
                          _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn btn-outline-compliment bg-white",
                              attrs: { type: "button" }
                            },
                            [_vm._v("Subscribe")]
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Revenue")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Your revenue refers to fees earned from providing goods or services. Under the accrual basis of accounting, revenues are recorded at the time of delivering the service or the merchandise, even if cash is not received at the time of delivery. Often the term income is used instead of revenue."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("COGS")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Cost of goods sold (COGS) refers to the direct costs of producing the goods sold by your company. This amount includes the cost of the materials and labor directly used to create the good but excludes indirect expenses, such as distribution costs and sales force costs."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "w-100  mb-4" }, [
      _c("span", { staticClass: "text-main bold" }, [_vm._v("BE EMPOWERED")]),
      _vm._v(" - To make decisions that "),
      _c("span", { staticClass: "text-main bold" }, [_vm._v("GROW")]),
      _vm._v(" your business")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a85176d", module.exports)
  }
}

/***/ }),

/***/ 939:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(940)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(942)
/* template */
var __vue_template__ = __webpack_require__(943)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-61a5ae60"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userFooterComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-61a5ae60", Component.options)
  } else {
    hotAPI.reload("data-v-61a5ae60", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 940:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(941);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("e6cb859c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-61a5ae60\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFooterComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-61a5ae60\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFooterComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 941:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.footer[data-v-61a5ae60]{\n  padding:65px 0 100px;\n  background: #20262b;\n}\n.side[data-v-61a5ae60]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-orient:vertical;\n  -webkit-box-direction:normal;\n      -ms-flex-direction:column;\n          flex-direction:column;\n  -webkit-box-pack:justify;\n      -ms-flex-pack:justify;\n          justify-content:space-between;\n  -webkit-box-align:start;\n      -ms-flex-align:start;\n          align-items:flex-start\n}\n.display-4[data-v-61a5ae60] {\n    font-size: 3.5rem;\n    font-weight: 300;\n    line-height: 1.2;\n}\n.text-bl[data-v-61a5ae60]{\n   color:#a4c2db;\n}\n.pr-1[data-v-61a5ae60] {\n  padding-right: 1px !important;\n}\n.pl-1[data-v-61a5ae60] {\n  padding-left: 1px !important;\n}\nli[data-v-61a5ae60] {\n    margin-bottom: 14px !important;\n}\n.btn[data-v-61a5ae60] {\n  background-color: #fff !important;\n  color: #333;\n  text-transform: capitalize !important;\n  font-weight: normal;\n}\n.form-control[data-v-61a5ae60]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60] {\n  border: none;\n}\n.container[data-v-61a5ae60] {\n  padding: 0px !important;\n}\n.col-xs-6[data-v-61a5ae60] {\n  width: 50%;\n}\nh2[data-v-61a5ae60] {\n  font-size: 16px !important;\n  margin-bottom:16px;\n}\n.site-footer[data-v-61a5ae60] {\n  font-size: 16px;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n width:80%;\n margin:0 auto;\n}\n.side[data-v-61a5ae60]{\n  width:30%\n}\n.footer-widgets[data-v-61a5ae60]{\n  width:70%;\n}\n.footer-widgets h2[data-v-61a5ae60] {\n  color: hsl(207, 15%, 70%);\n}\n.footer_body[data-v-61a5ae60] {\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-align:center;\n      -ms-flex-align:center;\n          align-items:center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-bottom:30px;\n}\n.foot[data-v-61a5ae60] {\n  font-size: 16px;\n  line-height: 1.4;\n  text-align:left;\n}\n.col-6[data-v-61a5ae60] {\n  background-color: #ffffff;\n}\n.socials[data-v-61a5ae60] {\n  padding: 10px 0;\n   display: -webkit-box;\n   display: -ms-flexbox;\n   display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.social_icons[data-v-61a5ae60] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 10%;\n  margin: 0 auto;\n  list-style: none;\n}\nul[data-v-61a5ae60],\nol[data-v-61a5ae60] {\n  list-style: none;\n}\nul li a[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%) !important;\nmargin-bottom:14px;\n}\n.text-bg[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%);\n}\na[data-v-61a5ae60] {\ncolor: hsl(207, 15%, 70%);\n}\n.reserve[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%);\n}\n.rules[data-v-61a5ae60] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  font-size: 16px;\n}\n.copyright[data-v-61a5ae60] {\n  color: rgba(0, 0, 0, 0.54);\n  font-size: 16px;\n}\n.footer_policy[data-v-61a5ae60] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  border-top: 1px dashed rgba(255, 255, 255, 0.84);\n  padding: 0;\n  font-size: 16px;\n  margin: 0;\n}\n.fa-inverse[data-v-61a5ae60] {\n color: rgba(0, 0, 0, 0.54);\n}\n.guidelines[data-v-61a5ae60]::after {\n  content: \" |\";\n  color: rgba(0, 0, 0, 0.54);\n  height: 10px;\n}\n.terms[data-v-61a5ae60]::after {\n  content: \" |\";\n color: rgba(0, 0, 0, 0.54);\n  height: 10px;\n}\n@media (max-width: 768px) {\n.footer_policy[data-v-61a5ae60] {\n    padding: 10px;\n}\nh2[data-v-61a5ae60] {\n    font-size: 17px !important;\n    margin-bottom:10px;\n}\n.rules[data-v-61a5ae60] {\n    font-size: 16px;\n}\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.foot[data-v-61a5ae60] {\n    margin-bottom:50px;\n}\n.rules[data-v-61a5ae60] {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.footer_policy[data-v-61a5ae60] {\n    text-align: center;\n}\n}\n@media (max-width: 1024px) {\n.site-footer[data-v-61a5ae60]{\n  width:90%;\n     -webkit-box-orient:vertical;\n     -webkit-box-direction:reverse;\n         -ms-flex-direction:column-reverse;\n             flex-direction:column-reverse;\n     -webkit-box-align:center;\n         -ms-flex-align:center;\n             align-items:center;\n}\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align:start;\n        -ms-flex-align:start;\n            align-items:flex-start;\n}\n.display-4[data-v-61a5ae60]{\n  font-size:2.5em;\n}\nul li a[data-v-61a5ae60]{\n  font-size:15px;\n  font-weight:300;\n}\n.footer-widgets[data-v-61a5ae60]{\n  width:100%;\n}\n.side[data-v-61a5ae60]{\n    width:100%;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n}\n}\n@media (max-width: 768px) {\n}\n@media (max-width: 575px) {\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align:start;\n        -ms-flex-align:start;\n            align-items:flex-start;\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n}\n.rules[data-v-61a5ae60] {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.footer_policy[data-v-61a5ae60] {\n    text-align: center;\n}\nh2[data-v-61a5ae60] {\n    font-size: 16px !important;\n}\n.site-footer[data-v-61a5ae60] {\n    font-size: 12px;\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:reverse;\n        -ms-flex-direction:column-reverse;\n            flex-direction:column-reverse;\n}\n.side[data-v-61a5ae60]{\n    width:100%;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n}\n.foot[data-v-61a5ae60] {\n    font-size: 16px;\n    line-height: 1;\n}\n.mt-5[data-v-61a5ae60] {\n    margin-top: 0px !important;\n}\n.footer_body[data-v-61a5ae60] {\n    padding: 5px 10px;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.footer_policy[data-v-61a5ae60] {\n    padding: 15px;\n}\n.reserve[data-v-61a5ae60] {\n    text-align: center;\n}\n.copyright[data-v-61a5ae60] {\n    padding: 0;\n}\n.rules[data-v-61a5ae60] {\n    padding: 0;\n    font-size: 12px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 942:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-new-arrival-component",

  data: function data() {
    return {
      year: 0,
      first_name: "",
      last_name: "",
      email: "",
      hide: false
    };
  },

  methods: {
    hideFooter: function hideFooter() {
      if (this.$route.path == '/private-chat') {
        this.hide = true;
      } else {
        this.hide = false;
      }
    }
  },
  mounted: function mounted() {
    this.hideFooter();
  },

  watch: {
    '$route': 'hideFooter'
  },
  computed: {
    getYear: function getYear() {
      var date = new Date();
      return this.year = date.getFullYear();
    }
  }
});

/***/ }),

/***/ 943:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "footer" }, [
    !_vm.hide
      ? _c("footer", { staticClass: "site-footer" }, [
          _c("div", { staticClass: "side" }, [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "copyright" }, [
              _c("p", { staticClass: "reserve" }, [
                _vm._v(
                  "© " + _vm._s(_vm.getYear) + " Bizguruh . All rights reserved"
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "footer-widgets" }, [
            _c("div", { staticClass: "footer_body" }, [
              _c("div", { staticClass: "foot \n          " }, [
                _c("div", { staticClass: "foot-about" }, [
                  _c("h2", [_vm._v("BIZGURUH")]),
                  _vm._v(" "),
                  _c("ul", [
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/guidelines" } }, [
                          _vm._v("Guidelines")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/faqs" } }, [
                          _vm._v("FAQs")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/terms" } }, [
                          _vm._v("T&C's")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/policies" } }, [
                          _vm._v("Privacy")
                        ])
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "foot " }, [
                _c("div", { staticClass: "foot-contact" }, [
                  _c("h2", [_vm._v("BROWSE")]),
                  _vm._v(" "),
                  _c("ul", [
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/explore" } }, [
                          _vm._v("Explore")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/videos" } }, [
                          _vm._v("Videos")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/articles" } }, [
                          _vm._v("Articles")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/community" } }, [
                          _vm._v("Community")
                        ])
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _vm._m(3)
            ])
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c("span", { staticClass: "display-4 text-bl" }, [_vm._v("BizGuruh")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "socials" }, [
      _c("i", {
        staticClass: "fa fa-facebook-square text-bg fa-1x mr-3",
        attrs: { "aria-hidden": "true" }
      }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-instagram text-bg fa-1x mr-3" }),
      _vm._v(" "),
      _c("i", {
        staticClass: "fa fa-twitter-square text-bg fa-1x",
        attrs: { "aria-hidden": "true" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "foot " }, [
      _c("div", { staticClass: "quick-links flex flex-wrap" }, [
        _c("h2", { staticClass: "w-100" }, [_vm._v("SOLUTIONS")]),
        _vm._v(" "),
        _c("ul", { staticClass: "w-100" }, [
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/market-research" } }, [
              _vm._v("Market Research")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Market Reports")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Consulting")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Business Research")
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "foot " }, [
      _c("div", { staticClass: "quick-links flex flex-wrap" }, [
        _c("h2", { staticClass: "w-100" }, [_vm._v("CONTACT US")]),
        _vm._v(" "),
        _c("ul", { staticClass: "w-100" }, [
          _c("li", [
            _c("a", { attrs: { href: "http://bizguruh.com/contact" } }, [
              _c("i", {
                staticClass: "fa fa-envelope",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" Mail\n                  ")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href: "https://www.facebook.com/BizGuruh-314446332731552/"
                }
              },
              [
                _c("i", {
                  staticClass: "fa fa-facebook",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Facebook\n                  ")
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "https://twitter.com/BizGuruh" } }, [
              _c("i", {
                staticClass: "fa fa-twitter",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" Twitter\n                  ")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              { attrs: { href: "https://www.instagram.com/bizguruh/" } },
              [
                _c("i", {
                  staticClass: "fa fa-instagram",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Instagram\n                  ")
              ]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-61a5ae60", module.exports)
  }
}

/***/ }),

/***/ 944:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(945)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(947)
/* template */
var __vue_template__ = __webpack_require__(948)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a1b2036"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/testimonialComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a1b2036", Component.options)
  } else {
    hotAPI.reload("data-v-7a1b2036", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 945:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(946);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("460f1c1b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 946:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nh3[data-v-7a1b2036] {\n  text-transform: initial;\n  font-size: 34px;\n  color: #333;\n}\n.main[data-v-7a1b2036] {\n background-color: white;\n  padding-top: 65px;\n  padding-bottom: 100px;\n\n  overflow: hidden;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bg-row[data-v-7a1b2036] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.bg1[data-v-7a1b2036] {\n  width: 60%;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  height: 156%;\n  top: -44%;\n  position: absolute;\n  right: -14%;\n}\n.body[data-v-7a1b2036]{\n\n    width: 100%;\n    padding: 15px 15px 45px;\n    border-radius: 5px;\n    z-index: 2;\n}\n.my-content[data-v-7a1b2036] {\n  display: grid;\n  grid-template-columns: auto auto;\n  grid-template-rows: auto auto;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 80%;\n  margin: 0 auto;\n  grid-column-gap: 50px;\n  grid-row-gap: 50px;\n  padding-top:20px\n}\n.testimonial-box[data-v-7a1b2036] {\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background:#f7f8fa;\n  height:200px;\n}\n.testimonial-image[data-v-7a1b2036] {\n  background:rgba(164, 194, 219,.5);\n  width: 40%;\n  height:100%;\n  text-align: center;\n  padding: 15px;\n}\n.testimonial-image img[data-v-7a1b2036] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.testimonial-text[data-v-7a1b2036] {\n  width: 60%;\n  padding: 15px;\n}\n@media (max-width: 768px) {\nh3[data-v-7a1b2036] {\n    font-size: 28px;\n    margin-bottom: 16px;\n}\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n}\n.testimonial-box shadow-sm[data-v-7a1b2036] {\n    padding: 15px;\n    background: white;\n    border-radius: 5px;\n}\nstrong[data-v-7a1b2036] {\n    font-size: 14px;\n}\np[data-v-7a1b2036]{\n  font-size:14px;\n}\n.body[data-v-7a1b2036]{\n  padding:0;\n}\n}\n@media (max-width: 575px) {\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n    grid-template-columns: auto;\n    grid-template-rows: auto;\n}\n.bg1[data-v-7a1b2036] {\n    width: 60%;\n    /* background: #e3ecf4; */\n    -webkit-transform: rotate(-20deg);\n    transform: rotate(-20deg);\n    height: 156%;\n    top: -60%;\n    position: absolute;\n    right: -14%;\n}\n.body[data-v-7a1b2036] {\n    background: transparent;\n    width: 100%;\n    padding: 15px;\n    border-radius: 5px;\n    z-index: 3;\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n}\n.testimonial-text[data-v-7a1b2036]{\n  font-size: 14px;\n}\nstrong[data-v-7a1b2036]{\n  font-size: 14.5px;\n}\n/* .testimonial-image img{\n  width: 60px;\n  height: auto;\n} */\n}\n", ""]);

// exports


/***/ }),

/***/ 947:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "testimonial-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
  },

  watch: {
    scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 3.85) {
        this.swing = true;
      }
    }
  }
});

/***/ }),

/***/ 948:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main" }, [
      _c("div", { staticClass: "body " }, [
        _c("h3", { staticClass: "mb-5 w-100 text-center desc_header" }, [
          _vm._v("What our users think")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "my-content animated fadeIn" }, [
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-three.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          The post on registering ones business was\n          particularly beneficial. I’m currently saving\n          towards it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @oilbyo")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I am here because\n          @freesiafoodies\n          asked us to, but seriously I must say it is worth\n          it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Oladunni Omotola")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I heard about your page from Nwanyiakamu and I\n          followed you. Your content is very educative and\n          qualitative always. Keep it up and thank you.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Tinuke Adegboye")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-one.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "I find your insights very inspiring, I stopped dreaming and started doing."
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @theefosa")])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a1b2036", module.exports)
  }
}

/***/ })

});