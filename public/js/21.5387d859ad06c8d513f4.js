webpackJsonp([21],{

/***/ 1041:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1042);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("85ae26b4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-10af5d4c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./auth.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-10af5d4c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./auth.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1042:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.m[data-v-10af5d4c]{\n    position: relative;\n}\n.back[data-v-10af5d4c]{\n    position: absolute;\n    top: 20px;\n    left: 20px;\n    z-index: 6;\n    color: white;\n}\n.s[data-v-10af5d4c]{\n    position: absolute;\n    bottom: 0;\n    width: 100%;\n    padding: 15px;\n    background: white;\n    z-index: 8;\n    text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1043:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__accountLoginComponent__ = __webpack_require__(1044);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__accountLoginComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__accountLoginComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__register__ = __webpack_require__(1049);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__register___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__register__);
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['name'],
    data: function data() {
        return {
            login: null
        };
    },

    components: {
        Login: __WEBPACK_IMPORTED_MODULE_0__accountLoginComponent___default.a,
        Register: __WEBPACK_IMPORTED_MODULE_1__register___default.a

    },
    mounted: function mounted() {
        if (this.$route.query.type == 'login' || this.$props.name == 'login') {
            this.login = true;
        } else {
            this.login = false;
        }
    },

    methods: {
        switchLogin: function switchLogin() {
            this.login = !this.login;
        }
    }
});

/***/ }),

/***/ 1044:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1045)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1047)
/* template */
var __vue_template__ = __webpack_require__(1048)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-604129ad"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/accountLoginComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-604129ad", Component.options)
  } else {
    hotAPI.reload("data-v-604129ad", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1045:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1046);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7e4f7d65", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-604129ad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./accountLoginComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-604129ad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./accountLoginComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1046:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.logoText[data-v-604129ad] {\n  padding: 15px;\n  text-align: left;\n}\n.fa-long-arrow-left[data-v-604129ad]{\n  z-index: 99;\n    position: absolute;\n    top: 15px;\n    left: 15px;\n    color: white;\n}\n.logoBan[data-v-604129ad] {\n  font-size: 30px;\n  position: absolute;\n  z-index: 2;\n  background: white;\n  padding: 10px 15px;\n}\n/* removed log out button here */\n.fa-sign-out-alt[data-v-604129ad] {\n  cursor: pointer;\n}\n.imgHome[data-v-604129ad] {\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.fa-sign-out-alt:hover .logText[data-v-604129ad] {\n  display: block;\n}\n.account_login[data-v-604129ad] {\n  position: relative;\n  width: 100%;\n  min-height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: url(\"/images/accountBg.jpg\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.outstanding-invoice[data-v-604129ad],\n.last-payment[data-v-604129ad],\n.sales-order[data-v-604129ad] {\n}\n.login_overlay[data-v-604129ad] {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.84);\n  z-index: 2;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.account_login2[data-v-604129ad] {\n  position: absolute;\n  width: 40%;\n  background-color: #e1e1e1;\n  z-index: 3;\n}\n.passwordOverlay[data-v-604129ad] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.8);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n}\n.passwordBox[data-v-604129ad] {\n  position: relative;\n  width: 35%;\n  height: 40%;\n  background-color: #e1e1e1;\n  border-radius: 5px;\n  display: grid;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mygrid[data-v-604129ad] {\n  grid-template-columns: auto;\n  grid-template-rows: auto auto auto;\n  grid-row-gap: 20px;\n  padding-left: 0;\n}\n.bookTrans[data-v-604129ad] {\n  display: grid;\n  grid-template-columns: auto auto auto;\n  grid-column-gap: 15px;\n}\n.passwordX[data-v-604129ad] {\n  position: absolute;\n  top: -15px;\n  right: -15px;\n}\n.myFont[data-v-604129ad] {\n  font-size: 16px !important;\n}\n.sel_button[data-v-604129ad] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.small-text[data-v-604129ad] {\n  font-size: 11px;\n  font-weight: 300;\n}\n.form-control[data-v-604129ad] {\n  height: calc(1.6em + 0.75rem + 2px);\n}\ntable[data-v-604129ad] {\n  font-size: 14px;\n}\n.container-fluid[data-v-604129ad] {\n  position: relative;\n  padding: 0;\n  -webkit-transition: all 0.1s;\n  transition: all 0.1s;\n  z-index: 9;\n}\n.passer[data-v-604129ad] {\n  width: 100%;\n}\n.loginBoard[data-v-604129ad] {\n  position: absolute;\n  width: 250px;\n  bottom: 30%;\n  right: 0;\n  z-index: 2;\n  font-style: oblique;\n}\n.col-md-4[data-v-604129ad] {\n  padding-right: 0;\n}\n.loginCancel[data-v-604129ad] {\n  position: absolute;\n  top: -15px;\n  left: -15px;\n  z-index: 2;\n}\n.text-faded[data-v-604129ad] {\n  color: rgba(0, 0, 0, 0.5);\n  margin-bottom: 0;\n}\nul[data-v-604129ad],\nol[data-v-604129ad] {\n  text-decoration: none;\n  list-style: none;\n  padding: 0;\n}\n.main-board[data-v-604129ad] {\n  height: 100vh;\n}\n.border[data-v-604129ad] {\n  border: none !important;\n  -webkit-box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;\n  width: 100%;\n  margin-bottom: auto;\n}\n.table-responsive[data-v-604129ad] {\n  padding: 10px;\n}\n.form-control[data-v-604129ad]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-604129ad]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-604129ad]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-604129ad]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-604129ad]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-604129ad]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.add-user-form[data-v-604129ad] {\n  background-color: #e1e1e1;\n  width: 50%;\n}\n.form[data-v-604129ad] {\n  background-color: #f5f5f5;\n}\n.revenue[data-v-604129ad],\n.cogs th[data-v-604129ad],\n.pbt th[data-v-604129ad],\n.tax th[data-v-604129ad] {\n  background-color: white !important;\n  color: rgba(0, 0, 0, 0.74);\n}\n.exp th[data-v-604129ad] {\n  background-color: rgba(0, 0, 0, 0.74) !important;\n}\n#statement[data-v-604129ad] {\n  background-color: #f2f5fe;\n}\nli.nav-item[data-v-604129ad]:hover {\n  background: rgba(255, 255, 255, 0.9);\n}\n.sideNav[data-v-604129ad] {\n  width: 15%;\n  min-height: 100vh;\n  padding: 0;\n  background-color: #f2f5fe;\n  color: hsl(225, 86%, 8%);\n}\n.fa-inverse[data-v-604129ad] {\n  color: hsl(225, 86%, 8%);\n}\n.fa-circle[data-v-604129ad] {\n  color: white;\n}\n.main-body[data-v-604129ad] {\n  position: relative;\n  width: 85%;\n  min-height: 100vh;\n  overflow-y: scroll;\n  background: #f7f8fa;\n}\nlabel[data-v-604129ad],\ninput[data-v-604129ad] {\n  display: inline;\n}\n.info.fa-1x[data-v-604129ad] {\n  font-size: 0.7em;\n}\n.business_name[data-v-604129ad] {\n  font-weight: normal;\n  font-size: 15px;\n  text-transform: uppercase;\n  color: hsl(207, 43%, 20%);\n}\n.d-fle[data-v-604129ad] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.btn-secondary[data-v-604129ad],\n.btn-info[data-v-604129ad],\n.btn-success[data-v-604129ad],\n.btn-danger[data-v-604129ad],\n.btn-default[data-v-604129ad],\n.btn-primary[data-v-604129ad] {\n  border: none !important;\n}\n.btn-primary[disabled][data-v-604129ad],\n.btn-primary[disabled][data-v-604129ad] {\n  color: #fff;\n  background-color: rgb(51, 122, 183, 0.3);\n  border-color: rgb(51, 122, 183, 0.3);\n  cursor: not-allowed;\n}\n.thead-light[data-v-604129ad] {\n  color: #495057;\n  background-color: #e9ecef;\n  border-color: #dee2e6;\n}\nhr[data-v-604129ad] {\n  margin-bottom: 5px;\n  margin-top: 5px;\n}\n.input-group[data-v-604129ad] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\nth[data-v-604129ad] {\n  background-color: hsl(207, 43%, 20%);\n  text-shadow: 0 0.04em 0.04em rgba(0, 0, 0, 0.35);\n  padding: 5px;\n}\n.homeText[data-v-604129ad] {\n  position: absolute;\n  width: 80%;\n  bottom: 0%;\n  right: 0%;\n  left: 0;\n  font-weight: 400;\n  top: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  z-index: 2;\n  margin: 0 auto;\n}\n.homeShadow[data-v-604129ad] {\n  position: absolute;\n  width: 100%;\n  bottom: 0%;\n  right: 0%;\n  left: 0;\n  background: rgba(0, 0, 0, 0.5);\n\n  top: 0;\n}\n.top-butt[data-v-604129ad] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.bottom-butt[data-v-604129ad] {\n  margin-top: 50px;\n}\nimg[data-v-604129ad] {\n  height: 100%;\n  width: 100%;\n}\n.homeImage[data-v-604129ad] {\n  position: relative;\n  width: 100%;\n  height: 100vh;\n}\n.upgradeContainer[data-v-604129ad] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  top: 0;\n}\n.upgrade[data-v-604129ad] {\n  position: absolute;\n  width: 50%;\n  height: auto;\n  background: #ffffff;\n  top: 30%;\n  right: 25%;\n  padding: 20px 40px;\n  border-radius: 3px;\n}\n.upgradeButtons[data-v-604129ad] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  padding: 10px 0;\n  font-size: 18px;\n}\n.upgradeButton[data-v-604129ad] {\n  cursor: pointer;\n  font-size: 16px;\n  font-weight: bold;\n  border: 1px solid;\n  border-radius: 2px;\n  padding: 2px 10px;\n  margin: 10px;\n}\n.opened[data-v-604129ad] {\n  max-width: 100%;\n  overflow: hidden;\n}\n.upgradeButton[data-v-604129ad]:hover {\n  background: hsl(207, 43%, 20%);\n  color: #fff;\n  font-weight: bold;\n}\n.upgradeText[data-v-604129ad] {\n  font-style: oblique;\n}\n.boldText[data-v-604129ad] {\n  font-size: 18px;\n  padding: 5px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.fa-xx[data-v-604129ad] {\n  font-size: 0.6rem;\n}\n.nav-item[data-v-604129ad] {\n  padding: 8px 15px;\n  text-transform: capitalize;\n  font-size: 14px;\n  cursor: pointer;\n}\n.remind[data-v-604129ad] {\n  position: none;\n}\nth[data-v-604129ad],\ntd[data-v-604129ad] {\n  text-align: center;\n  text-transform: capitalize;\n}\ntd[data-v-604129ad] {\n  background: white;\n}\n.textRed[data-v-604129ad] {\n  color: red !important;\n}\n.textGreen[data-v-604129ad] {\n  color: green !important;\n}\n.tablee[data-v-604129ad] {\n  cursor: pointer;\n}\n.tablee[data-v-604129ad]:hover {\n  background: rgba(0, 0, 0, 0.5);\n}\n.tabs[data-v-604129ad] {\n  position: relative;\n  min-height: 100vh;\n  height: 100%;\n  width: 100%;\n}\n.expense td[data-v-604129ad] {\n  padding: 0.25rem !important;\n}\n.under[data-v-604129ad] {\n  width: 30px;\n  margin-top: 5px;\n  margin-right: auto;\n  margin-left: 0;\n  border: 0;\n  border-top: 2px solid #eee;\n}\n.display-5[data-v-604129ad] {\n  font-size: 2rem !important;\n  font-weight: 300;\n  line-height: 1.2;\n}\n.header[data-v-604129ad] {\n  width: 100%;\n  font-size: 14px;\n  color: hsl(207, 43%, 20%);\n  padding: 20px 30px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #fff;\n  -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n          box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n}\n.border[data-v-604129ad] {\n  background: #fff;\n}\n.body-content[data-v-604129ad] {\n  width: 100%;\n  min-height: 100%;\n  margin: 0 auto;\n  border-top: 1px solid #ccc;\n  background-color: white;\n  padding: 20px 30px;\n  text-transform: capitalize;\n  position: relative;\n  background: #f7f8fa;\n  /* display:flex;\n  justify-content:center;\n  align-items:center;\n  flex-direction:column; */\n}\n.body-footer[data-v-604129ad] {\n  width: 100%;\n  background-color: #f8f9fa;\n  padding: 20px;\n}\n.fa-stackk[data-v-604129ad] {\n  display: inline-block;\n  height: 5em;\n  line-height: 5em;\n  position: relative;\n  vertical-align: middle;\n  width: 5em;\n}\n.fa-stack-5x[data-v-604129ad] {\n  font-size: 5em;\n}\n.fa-stack-2xx[data-v-604129ad] {\n  font-size: 2em;\n  left: 0;\n  position: absolute;\n  text-align: center;\n  width: 100%;\n  top: 25%;\n}\n.checkCircle[data-v-604129ad] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.checkCircle:hover .fa-stackk[data-v-604129ad],\n.checkCircle:hover .fa-stackk.fa-stack-2xx[data-v-604129ad] {\n  -webkit-transform: scale(1.2);\n          transform: scale(1.2);\n}\n.order-overlay[data-v-604129ad] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: #333;\n  z-index: 1;\n}\n.btn-pdf[data-v-604129ad] {\n  height: 25px;\n  padding: 1px 8px 9px;\n}\n.invoice-overlay[data-v-604129ad] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.85);\n  /* display: flex;\n  justify-content: space-evenly;\n  align-items: center; */\n  z-index: 4;\n}\n.home-overlay-content[data-v-604129ad] {\n  top: 40%;\n  position: fixed;\n  z-index: 5;\n  right: 25%;\n}\n.overlay-content[data-v-604129ad] {\n  top: 50%;\n  position: fixed;\n  z-index: 5;\n  right: 19%;\n  margin-top: -40vh;\n}\n.inv-text[data-v-604129ad] {\n  font-weight: bold;\n  cursor: pointer;\n}\n.bg-grey[data-v-604129ad] {\n  background: #fafafa;\n  padding: 10px 15px;\n}\n.inv-text[data-v-604129ad]:hover {\n  text-decoration: underline;\n}\n.payment-overlay[data-v-604129ad] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.8);\n  z-index: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.expense-overlay[data-v-604129ad] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.85);\n  z-index: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.fa-times[data-v-604129ad] {\n  position: absolute;\n  right: -12px;\n  top: -17px;\n  cursor: pointer;\n  color: black;\n  border: 2px solid;\n  padding: 3px 5px !important;\n  border-radius: 50%;\n  z-index: 6;\n  background: white;\n}\n.fa[data-v-604129ad],\n.fas[data-v-604129ad] {\n  cursor: pointer;\n}\n.mobile[data-v-604129ad] {\n  display: none !important;\n}\nlegend[data-v-604129ad] {\n  display: block;\n}\n@media (max-width: 768px) {\n.logoText[data-v-604129ad] {\n    display: none;\n}\n.col-md-4[data-v-604129ad] {\n    padding: 0;\n}\ntable[data-v-604129ad] {\n    font-size: 13px;\n}\n.mygrid[data-v-604129ad] {\n    padding-left: 0;\n}\n.sideNav[data-v-604129ad] {\n    padding-top: 20px;\n}\n.myText[data-v-604129ad] {\n    text-align: center !important;\n    line-height: 1.6;\n    padding-top: 0 !important;\n}\n.overlay-content[data-v-604129ad] {\n    right: 0;\n}\n.t2[data-v-604129ad] {\n    font-weight: normal !important;\n}\n.bookTrans[data-v-604129ad] {\n    grid-template-columns: auto;\n}\n.fa-stackk[data-v-604129ad] {\n    display: inline-block;\n    height: 3em;\n    line-height: 3em;\n    position: relative;\n    vertical-align: middle;\n    width: 3em;\n}\n.fa-stack-5x[data-v-604129ad] {\n    font-size: 3em;\n}\n.fa-stack-2xx[data-v-604129ad] {\n    font-size: 1.5em;\n    left: 0;\n    position: absolute;\n    text-align: center;\n    width: 100%;\n    top: 25%;\n}\n.smallButton[data-v-604129ad] {\n    padding: 3px 10px 4px !important;\n    height: 40px;\n}\n.account_login2[data-v-604129ad] {\n    width: 85%;\n}\n.add-user-form[data-v-604129ad] {\n    background-color: #e1e1e1;\n    width: 90%;\n}\n.homeText[data-v-604129ad] {\n    font-size: 14px;\n    width: 100%;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nth[data-v-604129ad] {\n    font-size: 12px;\n}\n.boldText[data-v-604129ad] {\n    font-size: 14px;\n}\n.fa-xx[data-v-604129ad] {\n    font-size: 0.5rem;\n}\ntd input.form-control[data-v-604129ad] {\n    height: 27px;\n    padding: 4px 5px;\n    font-size: 11px;\n}\n.mobTab[data-v-604129ad] {\n    width: 180px;\n}\n.business_name[data-v-604129ad] {\n    font-size: 14px;\n    margin-bottom: 2px;\n    padding: 2px;\n}\n.upgrade[data-v-604129ad] {\n    width: 95%;\n    right: 2%;\n}\n.p-5[data-v-604129ad] {\n    padding: 1rem !important;\n}\n.p-4[data-v-604129ad] {\n    padding: 1rem !important;\n}\n.fa-times[data-v-604129ad] {\n    position: absolute;\n    right: -12px;\n    top: -17px;\n    cursor: pointer;\n    color: black;\n    border: 2px solid;\n    padding: 3px 5px !important;\n    border-radius: 50%;\n    z-index: 6;\n    background: white;\n}\n.btn-secondary[data-v-604129ad],\n  .btn-info[data-v-604129ad],\n  .btn-success[data-v-604129ad],\n  .btn-danger[data-v-604129ad],\n  .btn-default[data-v-604129ad],\n  .btn-primary[data-v-604129ad] {\n    padding: 2px 7px 3px;\n    font-size: 12px;\n}\n.desktop[data-v-604129ad] {\n    display: none !important;\n}\n.mobile[data-v-604129ad] {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n}\n.table > thead > tr > th[data-v-604129ad],\n  .table > tbody > tr > th[data-v-604129ad],\n  .table > tfoot > tr > th[data-v-604129ad],\n  .table > thead > tr > td[data-v-604129ad],\n  .table > tbody > tr > td[data-v-604129ad],\n  .table > tfoot > tr > td[data-v-604129ad] {\n    padding: 3px;\n}\nh2[data-v-604129ad],\n  h3[data-v-604129ad],\n  h4[data-v-604129ad],\n  h5[data-v-604129ad] {\n    font-size: 16px;\n    line-height: 1.6;\n    margin-bottom: 8px;\n}\n.main-body[data-v-604129ad] {\n    width: 100%;\n    background: #f7f8fa;\n    margin-top: 73px;\n}\n.sideNav[data-v-604129ad] {\n    display: none;\n    padding: 0;\n    position: absolute;\n    z-index: 5;\n    height: 100%;\n}\n.openNav[data-v-604129ad] {\n    width: 200px;\n    display: block;\n}\n.body-content[data-v-604129ad] {\n    width: 100%;\n    padding: 15px;\n}\n.w-full-sm[data-v-604129ad] {\n    width: 100%;\n}\n.w-25[data-v-604129ad] {\n    width: 100% !important;\n}\n.w-95[data-v-604129ad] {\n    width: 95% !important;\n}\n.display-5[data-v-604129ad] {\n    font-size: 1.2rem !important;\n}\n.flex-xs-column[data-v-604129ad] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.header[data-v-604129ad] {\n    width: 100%;\n    font-size: 14px;\n    padding: 10px 15px;\n}\n.header[data-v-604129ad] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n\n  /* .fa-ellipsis-v {\n    color: white;\n  } */\n.fa-1x[data-v-604129ad] {\n    font-size: 0.8em;\n}\n.w-x-100[data-v-604129ad] {\n    width: 100% !important;\n}\n.flex-x-col[data-v-604129ad] {\n    -webkit-box-orient: vertical !important;\n    -webkit-box-direction: normal !important;\n        -ms-flex-direction: column !important;\n            flex-direction: column !important;\n}\n.passwordBox[data-v-604129ad] {\n    position: relative;\n    width: 80%;\n    height: 40%;\n}\n}\n@media (max-width: 425px) {\n.mygrid[data-v-604129ad] {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 0 !important;\n}\n.outstanding-invoice[data-v-604129ad],\n  .last-payment[data-v-604129ad],\n  .sales-order[data-v-604129ad] {\n    margin-bottom: 20px;\n}\n.main-body[data-v-604129ad] {\n    margin-top: 53px;\n}\n.sel_button[data-v-604129ad] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n}\n@media (max-width: 320px) {\n.table[data-v-604129ad],\n  th[data-v-604129ad],\n  td[data-v-604129ad] {\n    font-size: 8px !important;\n    padding: 3px;\n}\nbutton[data-v-604129ad] {\n    font-size: 12px !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1047:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue2_timepicker_src_vue_timepicker_vue__ = __webpack_require__(960);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue2_timepicker_src_vue_timepicker_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue2_timepicker_src_vue_timepicker_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_phoneCodes__ = __webpack_require__(714);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      myDials: [],
      isNumber: false,
      loginAccount: true,
      account: {
        email: "",
        password: "",
        phone_code: "+234"
      },
      role: "admin",
      account_users: [],
      account_user: {},
      user: {
        name: "",
        email: "",
        role: "default",
        password: "",
        confirm_password: "",
        account_name: ""
      },

      isActive: false,
      account_name: "",
      userSub: 0
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    var user = JSON.parse(localStorage.getItem("accountUser"));
    if (user !== null) {
      next(function (vm) {
        vm.$router.push("/account");
      });
    } else {
      next();
    }
  },
  created: function created() {

    this.myDials = Object(__WEBPACK_IMPORTED_MODULE_2__components_phoneCodes__["a" /* dialCodes */])();
    this.initialLoad();
    if (this.$route.query.id) {
      this.verify(this.$route.query.id);
    }
  },


  components: {
    VueTimepicker: __WEBPACK_IMPORTED_MODULE_1_vue2_timepicker_src_vue_timepicker_vue___default.a
  },

  computed: {
    currency: function currency() {
      return _defineProperty({}, this.position, this.currencySymbol);
    },
    transactionHistory: function transactionHistory() {
      var _this = this;

      return this.allTransactions.filter(function (ele) {
        if (Math.round(_this.getMonth(ele.date)) === Math.round(_this.selectedMonth)) {
          return ele;
        } else if (_this.selectedMonth === "all") {
          return ele;
        } else {
          return;
        }
      });
    }
  },
  methods: {
    closeSideNav: function closeSideNav() {
      this.showNow = false;
    },
    toggleEmail: function toggleEmail() {
      this.isNumber = !this.isNumber;
      if (this.isNumber === true) {
        this.account.email = null;
      }
      if (this.isNumber === false) {
        this.account.email = "";
      }
    },
    initialLoad: function initialLoad() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("accountUser"));
      if (user !== null) {
        this.account_name = user.name;
        this.authenticate = true;
        if (user.business_name) {
          this.business_name = user.business_name;
          this.user.account_name = user.business_name;
        }
        if (user.business_type) {
          this.supplier = user.business_type;
        }

        this.showBoard = false;
        if (this.userSub < 2) {
          this.isActive = false;
        }
      } else {
        setInterval(function () {
          _this2.showBoard = true;
        }, 20000);
      }
    },
    checkLogin: function checkLogin() {
      var _this3 = this;

      if (this.userSub >= 2) {
        var user = JSON.parse(localStorage.getItem("accountUser"));

        axios.get("/api/accountDetails", {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          if (response.status === 200) {
            if (response.data.verify == 1) {
              _this3.isActive = true;

              _this3.$toasted.success("Login successful");
              _this3.role = response.data.role;
              _this3.loginAccount = false;
              _this3.initialLoad();
            } else {
              _this3.$toasted.error("Invalid User");
            }
          }
        });
      } else {
        this.$toasted.error("Invalid user credentials");
      }
    },
    accountLogin: function accountLogin() {
      var _this4 = this;

      var data = {
        client_id: 2,
        client_secret: "UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C",
        grant_type: "password",
        username: this.account.email,
        password: this.account.password,
        theNewProvider: "account_api"
      };

      var accountUser = {
        email: this.account.email,
        password: this.account.password
      };
      axios.post("/oauth/token", data).then(function (response) {
        accountUser.access_token = response.data.access_token;
        accountUser.refresh_token = response.data.refresh_token;

        axios.get("/api/accountDetails", {
          headers: {
            Authorization: "Bearer " + response.data.access_token
          }
        }).then(function (response) {
          if (response.status === 200) {
            if (response.data.verify == 0) {
              _this4.$toasted.info("Email Unverified ");
            }
            _this4.isActive = true;
            accountUser.id = response.data.user_id;
            accountUser.role = response.data.role;

            accountUser.email = response.data.email;
            accountUser.business_name = response.data.business_name;
            accountUser.business_type = response.data.business_type;

            accountUser.name = response.data.name;
            localStorage.setItem("accountUser", JSON.stringify(accountUser));
            _this4.$toasted.success("Login successful");
            _this4.$router.push('/account/dashboard');
          }
        });
      }).catch(function (error) {
        _this4.$toasted.error("Invalid user credentials");
      });
    }
  }
});

/***/ }),

/***/ 1048:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "account_login" }, [
      _c("div", { staticClass: "login_overlay" }),
      _vm._v(" "),
      _c("div", { staticClass: "account_login2 p-4" }, [
        _c(
          "form",
          {
            staticClass: "form p-3",
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.accountLogin($event)
              }
            }
          },
          [
            _c("legend", { staticClass: "text-center mb-3" }, [
              _vm._v(" Login")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("div", { staticClass: "form-group" }, [
                !_vm.isNumber
                  ? _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.account.email,
                          expression: "account.email"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        autocorrect: "off",
                        autocapitalize: "off",
                        type: "email",
                        placeholder: " Email address"
                      },
                      domProps: { value: _vm.account.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.account, "email", $event.target.value)
                        }
                      }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isNumber
                  ? _c("div", { staticClass: "input-group" }, [
                      _c(
                        "div",
                        {
                          staticClass: "input-group-prepend",
                          staticStyle: { width: "25%" }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.account.phone_code,
                                  expression: "account.phone_code"
                                }
                              ],
                              staticClass: "input-group-text form-control",
                              attrs: { required: "" },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.account,
                                    "phone_code",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            _vm._l(_vm.myDials, function(dial, idx) {
                              return _c(
                                "option",
                                {
                                  key: idx,
                                  domProps: { value: dial.dial_code }
                                },
                                [
                                  _vm._v(
                                    _vm._s(dial.dial_code + " - " + dial.name)
                                  )
                                ]
                              )
                            }),
                            0
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "input-group-append",
                          staticStyle: { width: "75%" }
                        },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.account.email,
                                expression: "account.email"
                              }
                            ],
                            staticClass:
                              "input-group-text form-control text-left",
                            attrs: {
                              required: "",
                              type: "number",
                              "aria-describedby": "helpId",
                              placeholder: "8102588399",
                              minlength: "6",
                              maxlength: "10"
                            },
                            domProps: { value: _vm.account.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.account,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]
                      )
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.account.password,
                      expression: "account.password"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "password",
                    type: "password",
                    placeholder: "Enter password"
                  },
                  domProps: { value: _vm.account.password },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.account, "password", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "text-center mb-2" }, [
              _vm.isNumber
                ? _c(
                    "small",
                    {
                      staticClass: "mx-auto switch",
                      on: { click: _vm.toggleEmail }
                    },
                    [_vm._v("Log in with email address")]
                  )
                : _vm._e(),
              _vm._v(" "),
              !_vm.isNumber
                ? _c(
                    "small",
                    {
                      staticClass: "mx-auto switch",
                      on: { click: _vm.toggleEmail }
                    },
                    [_vm._v("Log in with phone number")]
                  )
                : _vm._e()
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c(
        "button",
        {
          staticClass:
            "elevated_btn btn-success text-white elevated_btn_sm mb-3 mx-auto",
          attrs: { type: "submit" }
        },
        [_vm._v("\n            Log In\n            ")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-604129ad", module.exports)
  }
}

/***/ }),

/***/ 1049:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1050)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1052)
/* template */
var __vue_template__ = __webpack_require__(1053)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-205752c7"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/register.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-205752c7", Component.options)
  } else {
    hotAPI.reload("data-v-205752c7", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1050:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1051);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("5d834fea", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-205752c7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./register.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-205752c7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./register.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1051:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.logoText[data-v-205752c7] {\n  padding: 15px;\n  text-align: left;\n}\n.fa-long-arrow-left[data-v-205752c7] {\n  z-index: 99;\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  color: white;\n}\n.logoBan[data-v-205752c7] {\n  font-size: 30px;\n  position: absolute;\n  z-index: 2;\n  background: white;\n  padding: 10px 15px;\n}\n/* removed log out button here */\n.fa-sign-out-alt[data-v-205752c7] {\n  cursor: pointer;\n}\n.imgHome[data-v-205752c7] {\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.fa-sign-out-alt:hover .logText[data-v-205752c7] {\n  display: block;\n}\n.account_login[data-v-205752c7] {\n  position: relative;\n  width: 100%;\n  min-height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: url(\"/images/accountBg.jpg\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  font-size: 15px;\n}\n.login_overlay[data-v-205752c7] {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.84);\n  z-index: 2;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.account_login2[data-v-205752c7] {\n  position: absolute;\n  width: 60%;\n  background-color: #e1e1e1;\n  z-index: 3;\n}\n.passwordOverlay[data-v-205752c7] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.8);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n}\n.passwordBox[data-v-205752c7] {\n  position: relative;\n  width: 35%;\n  height: 40%;\n  background-color: #e1e1e1;\n  border-radius: 5px;\n  display: grid;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mygrid[data-v-205752c7] {\n  grid-template-columns: auto;\n  grid-template-rows: auto auto auto;\n  grid-row-gap: 20px;\n  padding-left: 0;\n}\n.bookTrans[data-v-205752c7] {\n  display: grid;\n  grid-template-columns: auto auto auto;\n  grid-column-gap: 15px;\n}\n.passwordX[data-v-205752c7] {\n  position: absolute;\n  top: -15px;\n  right: -15px;\n}\n.myFont[data-v-205752c7] {\n  font-size: 16px !important;\n}\n.sel_button[data-v-205752c7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.small-text[data-v-205752c7] {\n  font-size: 11px;\n  font-weight: 300;\n}\n.form-control[data-v-205752c7] {\n  height: calc(1.6em + 0.75rem + 2px);\n}\ntable[data-v-205752c7] {\n  font-size: 14px;\n}\n.container-fluid[data-v-205752c7] {\n  position: relative;\n  padding: 0;\n  -webkit-transition: all 0.1s;\n  transition: all 0.1s;\n  z-index: 9;\n}\n.passer[data-v-205752c7] {\n  width: 100%;\n}\n.loginBoard[data-v-205752c7] {\n  position: absolute;\n  width: 250px;\n  bottom: 30%;\n  right: 0;\n  z-index: 2;\n  font-style: oblique;\n}\n.col-md-4[data-v-205752c7] {\n  padding-right: 0;\n}\n.loginCancel[data-v-205752c7] {\n  position: absolute;\n  top: -15px;\n  left: -15px;\n  z-index: 2;\n}\n.text-faded[data-v-205752c7] {\n  color: rgba(0, 0, 0, 0.5);\n  margin-bottom: 0;\n}\nul[data-v-205752c7],\nol[data-v-205752c7] {\n  text-decoration: none;\n  list-style: none;\n  padding: 0;\n}\n.main-board[data-v-205752c7] {\n  height: 100vh;\n}\n.border[data-v-205752c7] {\n  border: none !important;\n  -webkit-box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;\n  width: 100%;\n  margin-bottom: auto;\n}\n.table-responsive[data-v-205752c7] {\n  padding: 10px;\n}\n.form-control[data-v-205752c7]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-205752c7]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-205752c7]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-205752c7]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-205752c7]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-205752c7]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.add-user-form[data-v-205752c7] {\n  background-color: #e1e1e1;\n  width: 50%;\n}\n.form[data-v-205752c7] {\n  background-color: #f5f5f5;\n}\n.revenue[data-v-205752c7],\n.cogs th[data-v-205752c7],\n.pbt th[data-v-205752c7],\n.tax th[data-v-205752c7] {\n  background-color: white !important;\n  color: rgba(0, 0, 0, 0.74);\n}\n.exp th[data-v-205752c7] {\n  background-color: rgba(0, 0, 0, 0.74) !important;\n}\n#statement[data-v-205752c7] {\n  background-color: #f2f5fe;\n}\nli.nav-item[data-v-205752c7]:hover {\n  background: rgba(255, 255, 255, 0.9);\n}\n.sideNav[data-v-205752c7] {\n  width: 15%;\n  min-height: 100vh;\n  padding: 0;\n  background-color: #f2f5fe;\n  color: hsl(225, 86%, 8%);\n}\n.fa-inverse[data-v-205752c7] {\n  color: hsl(225, 86%, 8%);\n}\n.fa-circle[data-v-205752c7] {\n  color: white;\n}\n.main-body[data-v-205752c7] {\n  position: relative;\n  width: 85%;\n  min-height: 100vh;\n  overflow-y: scroll;\n  background: #f7f8fa;\n}\nlabel[data-v-205752c7],\ninput[data-v-205752c7] {\n  display: inline;\n}\n.info.fa-1x[data-v-205752c7] {\n  font-size: 0.7em;\n}\n.business_name[data-v-205752c7] {\n  font-weight: normal;\n  font-size: 15px;\n  text-transform: uppercase;\n  color: hsl(207, 43%, 20%);\n}\n.d-fle[data-v-205752c7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.btn-secondary[data-v-205752c7],\n.btn-info[data-v-205752c7],\n.btn-success[data-v-205752c7],\n.btn-danger[data-v-205752c7],\n.btn-default[data-v-205752c7],\n.btn-primary[data-v-205752c7] {\n  border: none !important;\n}\n.btn-primary[disabled][data-v-205752c7],\n.btn-primary[disabled][data-v-205752c7] {\n  color: #fff;\n  background-color: rgb(51, 122, 183, 0.3);\n  border-color: rgb(51, 122, 183, 0.3);\n  cursor: not-allowed;\n}\n.thead-light[data-v-205752c7] {\n  color: #495057;\n  background-color: #e9ecef;\n  border-color: #dee2e6;\n}\nhr[data-v-205752c7] {\n  margin-bottom: 5px;\n  margin-top: 5px;\n}\n.input-group[data-v-205752c7] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\nth[data-v-205752c7] {\n  background-color: hsl(207, 43%, 20%);\n  text-shadow: 0 0.04em 0.04em rgba(0, 0, 0, 0.35);\n  padding: 5px;\n}\n.homeText[data-v-205752c7] {\n  position: absolute;\n  width: 80%;\n  bottom: 0%;\n  right: 0%;\n  left: 0;\n  font-weight: 400;\n  top: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  z-index: 2;\n  margin: 0 auto;\n}\n.homeShadow[data-v-205752c7] {\n  position: absolute;\n  width: 100%;\n  bottom: 0%;\n  right: 0%;\n  left: 0;\n  background: rgba(0, 0, 0, 0.5);\n\n  top: 0;\n}\n.top-butt[data-v-205752c7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.bottom-butt[data-v-205752c7] {\n  margin-top: 50px;\n}\nimg[data-v-205752c7] {\n  height: 100%;\n  width: 100%;\n}\n.homeImage[data-v-205752c7] {\n  position: relative;\n  width: 100%;\n  height: 100vh;\n}\n.upgradeContainer[data-v-205752c7] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  top: 0;\n}\n.upgrade[data-v-205752c7] {\n  position: absolute;\n  width: 50%;\n  height: auto;\n  background: #ffffff;\n  top: 30%;\n  right: 25%;\n  padding: 20px 40px;\n  border-radius: 3px;\n}\n.upgradeButtons[data-v-205752c7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  padding: 10px 0;\n  font-size: 18px;\n}\n.upgradeButton[data-v-205752c7] {\n  cursor: pointer;\n  font-size: 16px;\n  font-weight: bold;\n  border: 1px solid;\n  border-radius: 2px;\n  padding: 2px 10px;\n  margin: 10px;\n}\n.opened[data-v-205752c7] {\n  max-width: 100%;\n  overflow: hidden;\n}\n.upgradeButton[data-v-205752c7]:hover {\n  background: hsl(207, 43%, 20%);\n  color: #fff;\n  font-weight: bold;\n}\n.upgradeText[data-v-205752c7] {\n  font-style: oblique;\n}\n.boldText[data-v-205752c7] {\n  font-size: 18px;\n  padding: 5px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.fa-xx[data-v-205752c7] {\n  font-size: 0.6rem;\n}\n.nav-item[data-v-205752c7] {\n  padding: 8px 15px;\n  text-transform: capitalize;\n  font-size: 14px;\n  cursor: pointer;\n}\n.remind[data-v-205752c7] {\n  position: none;\n}\nth[data-v-205752c7],\ntd[data-v-205752c7] {\n  text-align: center;\n  text-transform: capitalize;\n}\ntd[data-v-205752c7] {\n  background: white;\n}\n.textRed[data-v-205752c7] {\n  color: red !important;\n}\n.textGreen[data-v-205752c7] {\n  color: green !important;\n}\n.tablee[data-v-205752c7] {\n  cursor: pointer;\n}\n.tablee[data-v-205752c7]:hover {\n  background: rgba(0, 0, 0, 0.5);\n}\n.tabs[data-v-205752c7] {\n  position: relative;\n  min-height: 100vh;\n  height: 100%;\n  width: 100%;\n}\n.expense td[data-v-205752c7] {\n  padding: 0.25rem !important;\n}\n.under[data-v-205752c7] {\n  width: 30px;\n  margin-top: 5px;\n  margin-right: auto;\n  margin-left: 0;\n  border: 0;\n  border-top: 2px solid #eee;\n}\n.display-5[data-v-205752c7] {\n  font-size: 2rem !important;\n  font-weight: 300;\n  line-height: 1.2;\n}\n.header[data-v-205752c7] {\n  width: 100%;\n  font-size: 14px;\n  color: hsl(207, 43%, 20%);\n  padding: 20px 30px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #fff;\n  -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n          box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.05);\n}\n.border[data-v-205752c7] {\n  background: #fff;\n}\n.body-content[data-v-205752c7] {\n  width: 100%;\n  min-height: 100%;\n  margin: 0 auto;\n  border-top: 1px solid #ccc;\n  background-color: white;\n  padding: 20px 30px;\n  text-transform: capitalize;\n  position: relative;\n  background: #f7f8fa;\n  /* display:flex;\n  justify-content:center;\n  align-items:center;\n  flex-direction:column; */\n}\n.body-footer[data-v-205752c7] {\n  width: 100%;\n  background-color: #f8f9fa;\n  padding: 20px;\n}\n.fa-stackk[data-v-205752c7] {\n  display: inline-block;\n  height: 5em;\n  line-height: 5em;\n  position: relative;\n  vertical-align: middle;\n  width: 5em;\n}\n.fa-stack-5x[data-v-205752c7] {\n  font-size: 5em;\n}\n.fa-stack-2xx[data-v-205752c7] {\n  font-size: 2em;\n  left: 0;\n  position: absolute;\n  text-align: center;\n  width: 100%;\n  top: 25%;\n}\n.checkCircle[data-v-205752c7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.checkCircle:hover .fa-stackk[data-v-205752c7],\n.checkCircle:hover .fa-stackk.fa-stack-2xx[data-v-205752c7] {\n  -webkit-transform: scale(1.2);\n          transform: scale(1.2);\n}\n.order-overlay[data-v-205752c7] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: #333;\n  z-index: 1;\n}\n.btn-pdf[data-v-205752c7] {\n  height: 25px;\n  padding: 1px 8px 9px;\n}\n.invoice-overlay[data-v-205752c7] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.85);\n  /* display: flex;\n  justify-content: space-evenly;\n  align-items: center; */\n  z-index: 4;\n}\n.home-overlay-content[data-v-205752c7] {\n  top: 40%;\n  position: fixed;\n  z-index: 5;\n  right: 25%;\n}\n.overlay-content[data-v-205752c7] {\n  top: 50%;\n  position: fixed;\n  z-index: 5;\n  right: 19%;\n  margin-top: -40vh;\n}\n.inv-text[data-v-205752c7] {\n  font-weight: bold;\n  cursor: pointer;\n}\n.bg-grey[data-v-205752c7] {\n  background: #fafafa;\n  padding: 10px 15px;\n}\n.inv-text[data-v-205752c7]:hover {\n  text-decoration: underline;\n}\n.payment-overlay[data-v-205752c7] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.8);\n  z-index: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.expense-overlay[data-v-205752c7] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.85);\n  z-index: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.fa-times[data-v-205752c7] {\n  position: absolute;\n  right: -12px;\n  top: -17px;\n  cursor: pointer;\n  color: black;\n  border: 2px solid;\n  padding: 3px 5px !important;\n  border-radius: 50%;\n  z-index: 6;\n  background: white;\n}\n.fa[data-v-205752c7],\n.fas[data-v-205752c7] {\n  cursor: pointer;\n}\n.mobile[data-v-205752c7] {\n  display: none !important;\n}\nlegend[data-v-205752c7] {\n  display: block;\n}\n@media (max-width: 768px) {\n.logoText[data-v-205752c7] {\n    display: none;\n}\n.col-md-4[data-v-205752c7] {\n    padding: 0;\n}\ntable[data-v-205752c7] {\n    font-size: 13px;\n}\n.mygrid[data-v-205752c7] {\n    padding-left: 0;\n}\n.sideNav[data-v-205752c7] {\n    padding-top: 20px;\n}\n.myText[data-v-205752c7] {\n    text-align: center !important;\n    line-height: 1.6;\n    padding-top: 0 !important;\n}\n.overlay-content[data-v-205752c7] {\n    right: 0;\n}\n.t2[data-v-205752c7] {\n    font-weight: normal !important;\n}\n.bookTrans[data-v-205752c7] {\n    grid-template-columns: auto;\n}\n.fa-stackk[data-v-205752c7] {\n    display: inline-block;\n    height: 3em;\n    line-height: 3em;\n    position: relative;\n    vertical-align: middle;\n    width: 3em;\n}\n.fa-stack-5x[data-v-205752c7] {\n    font-size: 3em;\n}\n.fa-stack-2xx[data-v-205752c7] {\n    font-size: 1.5em;\n    left: 0;\n    position: absolute;\n    text-align: center;\n    width: 100%;\n    top: 25%;\n}\n.smallButton[data-v-205752c7] {\n    padding: 3px 10px 4px !important;\n    height: 40px;\n}\n.account_login2[data-v-205752c7] {\n    width: 85%;\n}\n.add-user-form[data-v-205752c7] {\n    background-color: #e1e1e1;\n    width: 90%;\n}\n.homeText[data-v-205752c7] {\n    font-size: 14px;\n    width: 100%;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nth[data-v-205752c7] {\n    font-size: 12px;\n}\n.boldText[data-v-205752c7] {\n    font-size: 14px;\n}\n.fa-xx[data-v-205752c7] {\n    font-size: 0.5rem;\n}\ntd input.form-control[data-v-205752c7] {\n    height: 27px;\n    padding: 4px 5px;\n    font-size: 11px;\n}\n.mobTab[data-v-205752c7] {\n    width: 180px;\n}\n.business_name[data-v-205752c7] {\n    font-size: 14px;\n    margin-bottom: 2px;\n    padding: 2px;\n}\n.upgrade[data-v-205752c7] {\n    width: 95%;\n    right: 2%;\n}\n.p-5[data-v-205752c7] {\n    padding: 1rem !important;\n}\n.p-4[data-v-205752c7] {\n    padding: 1rem !important;\n}\n.fa-times[data-v-205752c7] {\n    position: absolute;\n    right: -12px;\n    top: -17px;\n    cursor: pointer;\n    color: black;\n    border: 2px solid;\n    padding: 3px 5px !important;\n    border-radius: 50%;\n    z-index: 6;\n    background: white;\n}\n.btn-secondary[data-v-205752c7],\n  .btn-info[data-v-205752c7],\n  .btn-success[data-v-205752c7],\n  .btn-danger[data-v-205752c7],\n  .btn-default[data-v-205752c7],\n  .btn-primary[data-v-205752c7] {\n    padding: 2px 7px 3px;\n    font-size: 12px;\n}\n.desktop[data-v-205752c7] {\n    display: none !important;\n}\n.mobile[data-v-205752c7] {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n}\n.table > thead > tr > th[data-v-205752c7],\n  .table > tbody > tr > th[data-v-205752c7],\n  .table > tfoot > tr > th[data-v-205752c7],\n  .table > thead > tr > td[data-v-205752c7],\n  .table > tbody > tr > td[data-v-205752c7],\n  .table > tfoot > tr > td[data-v-205752c7] {\n    padding: 3px;\n}\nh2[data-v-205752c7],\n  h3[data-v-205752c7],\n  h4[data-v-205752c7],\n  h5[data-v-205752c7] {\n    font-size: 16px;\n    line-height: 1.6;\n    margin-bottom: 8px;\n}\n.main-body[data-v-205752c7] {\n    width: 100%;\n    background: #f7f8fa;\n    margin-top: 73px;\n}\n.sideNav[data-v-205752c7] {\n    display: none;\n    padding: 0;\n    position: absolute;\n    z-index: 5;\n    height: 100%;\n}\n.openNav[data-v-205752c7] {\n    width: 200px;\n    display: block;\n}\n.body-content[data-v-205752c7] {\n    width: 100%;\n    padding: 15px;\n}\n.w-full-sm[data-v-205752c7] {\n    width: 100%;\n}\n.w-25[data-v-205752c7] {\n    width: 100% !important;\n}\n.w-95[data-v-205752c7] {\n    width: 95% !important;\n}\n.display-5[data-v-205752c7] {\n    font-size: 1.2rem !important;\n}\n.flex-xs-column[data-v-205752c7] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.header[data-v-205752c7] {\n    width: 100%;\n    font-size: 14px;\n    padding: 10px 15px;\n}\n.header[data-v-205752c7] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n\n  /* .fa-ellipsis-v {\n    color: white;\n  } */\n.fa-1x[data-v-205752c7] {\n    font-size: 0.8em;\n}\n.w-x-100[data-v-205752c7] {\n    width: 100% !important;\n}\n.flex-x-col[data-v-205752c7] {\n    -webkit-box-orient: vertical !important;\n    -webkit-box-direction: normal !important;\n        -ms-flex-direction: column !important;\n            flex-direction: column !important;\n}\n.passwordBox[data-v-205752c7] {\n    position: relative;\n    width: 80%;\n    height: 40%;\n}\n}\n@media (max-width: 425px) {\n.mygrid[data-v-205752c7] {\n    display: -webkit-box !important;\n    display: -ms-flexbox !important;\n    display: flex !important;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 0 !important;\n}\n.outstanding-invoice[data-v-205752c7],\n  .last-payment[data-v-205752c7],\n  .sales-order[data-v-205752c7] {\n    margin-bottom: 20px;\n}\n.main-body[data-v-205752c7] {\n    margin-top: 53px;\n}\n.sel_button[data-v-205752c7] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n}\n@media (max-width: 320px) {\n.table[data-v-205752c7],\n  th[data-v-205752c7],\n  td[data-v-205752c7] {\n    font-size: 8px !important;\n    padding: 3px;\n}\nbutton[data-v-205752c7] {\n    font-size: 12px !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1052:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_phoneCodes__ = __webpack_require__(714);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_components_cloudinaryComponent__ = __webpack_require__(989);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_components_cloudinaryComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__user_components_cloudinaryComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      oldimage: "",
      myDials: [],
      isNumber: false,
      isLoading: false,
      user: {
        name: "",
        password: "",
        email: "",
        phone_code: "+234",
        phone_no: null,
        business_name: "",
        business_type: "default",
        logo: ""
      }
    };
  },

  components: {
    uploadForm: __WEBPACK_IMPORTED_MODULE_2__user_components_cloudinaryComponent___default.a
  },
  created: function created() {
    this.myDials = Object(__WEBPACK_IMPORTED_MODULE_1__components_phoneCodes__["a" /* dialCodes */])();
  },

  watch: {},
  methods: {
    closeSideNav: function closeSideNav() {
      this.showNow = false;
    },
    getUpload: function getUpload(value) {
      this.user.logo = value;
    },
    toggleEmail: function toggleEmail() {
      this.isNumber = !this.isNumber;
      if (this.isNumber === true) {
        this.account.email = null;
      }
      if (this.isNumber === false) {
        this.account.email = "";
      }
    },
    register: function register() {
      var _this = this;

      this.isLoading = true;
      axios.post("/api/register-account", this.user).then(function (res) {
        if (res.status == 200) {
          _this.accountLogin();
        }
      }).catch(function (err) {
        _this.isLoading = false;
      });
    },
    accountLogin: function accountLogin() {
      var _this2 = this;

      var data = {
        client_id: 2,
        client_secret: "UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C",
        grant_type: "password",
        username: this.user.email,
        password: this.user.password,
        theNewProvider: "account_api"
      };

      var accountUser = {
        email: this.user.email,
        password: this.user.password
      };
      axios.post("/oauth/token", data).then(function (response) {
        accountUser.access_token = response.data.access_token;
        accountUser.refresh_token = response.data.refresh_token;

        axios.get("/api/accountDetails", {
          headers: {
            Authorization: "Bearer " + response.data.access_token
          }
        }).then(function (response) {
          if (response.status === 200) {

            accountUser.id = response.data.user_id;
            accountUser.role = response.data.role;
            accountUser.email = response.data.email;
            accountUser.business_name = response.data.business_name;
            accountUser.business_type = response.data.business_type;
            accountUser.name = response.data.name;
            localStorage.setItem("accountUser", JSON.stringify(accountUser));
            _this2.isLoading = false;
            _this2.$toasted.success(" successful");
            _this2.$router.push("/account/pricing?session=pricing");
          }
        });
      }).catch(function (error) {
        _this2.$toasted.error("Invalid user credentials");
        _this2.isLoading = false;
      });
    }
  }
});

/***/ }),

/***/ 1053:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "account_login" },
      [
        _vm.$route.meta.accounting
          ? _c("router-link", { attrs: { to: "/account" } }, [
              _c(
                "i",
                {
                  staticClass: "fa fa-long-arrow-left text-white",
                  attrs: { "aria-hidden": "true" }
                },
                [_vm._v("Back to Home")]
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "login_overlay" }),
        _vm._v(" "),
        _c("div", { staticClass: "account_login2 p-4" }, [
          _c(
            "form",
            {
              staticClass: "form p-3",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.register($event)
                }
              }
            },
            [
              _c("legend", { staticClass: "text-center mb-3" }, [
                _vm._v("Register")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "d-flex justify-content-between p-2" }, [
                _c("div", { staticClass: "p-2 w-50" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [_vm._v("Full name")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.user.name,
                          expression: "user.name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        "aria-describedby": "helpId",
                        placeholder: "John doe"
                      },
                      domProps: { value: _vm.user.name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.user, "name", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [_vm._v("Email")]),
                    _vm._v(" "),
                    !_vm.isNumber
                      ? _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.user.email,
                              expression: "user.email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            autocorrect: "off",
                            autocapitalize: "off",
                            type: "email",
                            placeholder: " example@email.com"
                          },
                          domProps: { value: _vm.user.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.user, "email", $event.target.value)
                            }
                          }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.isNumber
                      ? _c("div", { staticClass: "input-group" }, [
                          _c(
                            "div",
                            {
                              staticClass: "input-group-prepend",
                              staticStyle: { width: "20%" }
                            },
                            [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.user.phone_code,
                                      expression: "user.phone_code"
                                    }
                                  ],
                                  staticClass: "input-group-text form-control",
                                  attrs: { required: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        _vm.user,
                                        "phone_code",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                _vm._l(_vm.myDials, function(dial, idx) {
                                  return _c(
                                    "option",
                                    {
                                      key: idx,
                                      domProps: { value: dial.dial_code }
                                    },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          dial.dial_code + " - " + dial.name
                                        )
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "input-group-append",
                              staticStyle: { width: "45%" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.phone_no,
                                    expression: "user.phone_no"
                                  }
                                ],
                                staticClass:
                                  "input-group-text form-control text-left",
                                attrs: {
                                  required: "",
                                  type: "number",
                                  "aria-describedby": "helpId",
                                  placeholder: "8102588399",
                                  minlength: "6",
                                  maxlength: "10"
                                },
                                domProps: { value: _vm.user.phone_no },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "phone_no",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", {}, [
                      _vm.isNumber
                        ? _c(
                            "small",
                            {
                              staticClass: "mx-auto switch",
                              on: { click: _vm.toggleEmail }
                            },
                            [_vm._v("Sign up with email address")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.isNumber
                        ? _c(
                            "small",
                            {
                              staticClass: "mx-auto switch",
                              on: { click: _vm.toggleEmail }
                            },
                            [_vm._v("Sign up with phone number")]
                          )
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [_vm._v("Password")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.user.password,
                          expression: "user.password"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "password",
                        "aria-describedby": "helpId",
                        placeholder: "********"
                      },
                      domProps: { value: _vm.user.password },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.user, "password", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [
                      _vm._v("Business name")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.user.business_name,
                          expression: " user.business_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        "aria-describedby": "helpId",
                        placeholder: "Example inc"
                      },
                      domProps: { value: _vm.user.business_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.user,
                            "business_name",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [
                      _vm._v("Business Activity")
                    ]),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.user.business_type,
                            expression: "user.business_type"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { required: "" },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              _vm.user,
                              "business_type",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "default" } }, [
                          _vm._v("Select Activity")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "retailer" } }, [
                          _vm._v("Retailer")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "manufacturer" } }, [
                          _vm._v("Manufacturer")
                        ])
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p-2 w-50" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("label", { attrs: { for: "" } }, [
                        _vm._v("Business logo")
                      ]),
                      _vm._v(" "),
                      _c("uploadForm", {
                        attrs: { oldimage: _vm.oldimage },
                        on: { getUpload: _vm.getUpload }
                      })
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mt-2" }, [
                _c(
                  "button",
                  {
                    staticClass:
                      "elevated_btn btn-success text-white elevated_btn_sm mb-3 mx-auto",
                    attrs: { type: "submit" }
                  },
                  [
                    _vm._v("\n            Register\n            "),
                    _vm.isLoading
                      ? _c("span", {
                          staticClass: "spinner-grow spinner-grow-sm text-light"
                        })
                      : _vm._e()
                  ]
                )
              ])
            ]
          )
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-205752c7", module.exports)
  }
}

/***/ }),

/***/ 1054:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "m" },
    [
      _vm.$route.name !== "LandingAuth"
        ? _c(
            "router-link",
            { staticClass: "back", attrs: { to: "/account" } },
            [
              _c("i", { staticClass: "fas fa-long-arrow-alt-left    " }, [
                _vm._v("Back to Home")
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.login ? _c("Login") : _c("Register"),
      _vm._v(" "),
      _c("div", { staticClass: "s" }, [
        _vm.login
          ? _c("p", [
              _vm._v("Dont't have an account? "),
              _c(
                "span",
                { staticClass: "text-main", on: { click: _vm.switchLogin } },
                [_vm._v("Sign Up")]
              )
            ])
          : _c("p", [
              _vm._v("Already have an account? "),
              _c(
                "span",
                { staticClass: "text-main", on: { click: _vm.switchLogin } },
                [_vm._v("Sign in")]
              )
            ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-10af5d4c", module.exports)
  }
}

/***/ }),

/***/ 482:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1041)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1043)
/* template */
var __vue_template__ = __webpack_require__(1054)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-10af5d4c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/auth.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-10af5d4c", Component.options)
  } else {
    hotAPI.reload("data-v-10af5d4c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dialCodes; });
var dialCodes = function dialCodes() {
    var dials = [{ name: "Afghanistan", dial_code: "+93", code: "AF" }, { name: "Albania", dial_code: "+355", code: "AL" }, { name: "Algeria", dial_code: "+213", code: "DZ" }, { name: "AmericanSamoa", dial_code: "+1 684", code: "AS" }, { name: "Andorra", dial_code: "+376", code: "AD" }, { name: "Angola", dial_code: "+244", code: "AO" }, { name: "Anguilla", dial_code: "+1 264", code: "AI" }, { name: "Antarctica", dial_code: "+672", code: "AQ" }, { name: "Antigua and Barbuda", dial_code: "+1268", code: "AG" }, { name: "Argentina", dial_code: "+54", code: "AR" }, { name: "Armenia", dial_code: "+374", code: "AM" }, { name: "Aruba", dial_code: "+297", code: "AW" }, { name: "Australia", dial_code: "+61", code: "AU" }, { name: "Austria", dial_code: "+43", code: "AT" }, { name: "Azerbaijan", dial_code: "+994", code: "AZ" }, { name: "Bahamas", dial_code: "+1 242", code: "BS" }, { name: "Bahrain", dial_code: "+973", code: "BH" }, { name: "Bangladesh", dial_code: "+880", code: "BD" }, { name: "Barbados", dial_code: "+1 246", code: "BB" }, { name: "Belarus", dial_code: "+375", code: "BY" }, { name: "Belgium", dial_code: "+32", code: "BE" }, { name: "Belize", dial_code: "+501", code: "BZ" }, { name: "Benin", dial_code: "+229", code: "BJ" }, { name: "Bermuda", dial_code: "+1 441", code: "BM" }, { name: "Bhutan", dial_code: "+975", code: "BT" }, { name: "Bolivia, Plurinational State of", dial_code: "+591", code: "BO" }, { name: "Bosnia and Herzegovina", dial_code: "+387", code: "BA" }, { name: "Botswana", dial_code: "+267", code: "BW" }, { name: "Brazil", dial_code: "+55", code: "BR" }, { name: "British Indian Ocean Territory", dial_code: "+246", code: "IO" }, { name: "Brunei Darussalam", dial_code: "+673", code: "BN" }, { name: "Bulgaria", dial_code: "+359", code: "BG" }, { name: "Burkina Faso", dial_code: "+226", code: "BF" }, { name: "Burundi", dial_code: "+257", code: "BI" }, { name: "Cambodia", dial_code: "+855", code: "KH" }, { name: "Cameroon", dial_code: "+237", code: "CM" }, { name: "Canada", dial_code: "+1", code: "CA" }, { name: "Cape Verde", dial_code: "+238", code: "CV" }, { name: "Cayman Islands", dial_code: "+ 345", code: "KY" }, { name: "Central African Republic", dial_code: "+236", code: "CF" }, { name: "Chad", dial_code: "+235", code: "TD" }, { name: "Chile", dial_code: "+56", code: "CL" }, { name: "China", dial_code: "+86", code: "CN" }, { name: "Christmas Island", dial_code: "+61", code: "CX" }, { name: "Cocos (Keeling) Islands", dial_code: "+61", code: "CC" }, { name: "Colombia", dial_code: "+57", code: "CO" }, { name: "Comoros", dial_code: "+269", code: "KM" }, { name: "Congo", dial_code: "+242", code: "CG" }, {
        name: "Congo, The Democratic Republic of the",
        dial_code: "+243",
        code: "CD"
    }, { name: "Cook Islands", dial_code: "+682", code: "CK" }, { name: "Costa Rica", dial_code: "+506", code: "CR" }, { name: "Cote d'Ivoire", dial_code: "+225", code: "CI" }, { name: "Croatia", dial_code: "+385", code: "HR" }, { name: "Cuba", dial_code: "+53", code: "CU" }, { name: "Cyprus", dial_code: "+357", code: "CY" }, { name: "Czech Republic", dial_code: "+420", code: "CZ" }, { name: "Denmark", dial_code: "+45", code: "DK" }, { name: "Djibouti", dial_code: "+253", code: "DJ" }, { name: "Dominica", dial_code: "+1 767", code: "DM" }, { name: "Dominican Republic", dial_code: "+1 849", code: "DO" }, { name: "Ecuador", dial_code: "+593", code: "EC" }, { name: "Egypt", dial_code: "+20", code: "EG" }, { name: "El Salvador", dial_code: "+503", code: "SV" }, { name: "Equatorial Guinea", dial_code: "+240", code: "GQ" }, { name: "Eritrea", dial_code: "+291", code: "ER" }, { name: "Estonia", dial_code: "+372", code: "EE" }, { name: "Ethiopia", dial_code: "+251", code: "ET" }, { name: "Falkland Islands (Malvinas)", dial_code: "+500", code: "FK" }, { name: "Faroe Islands", dial_code: "+298", code: "FO" }, { name: "Fiji", dial_code: "+679", code: "FJ" }, { name: "Finland", dial_code: "+358", code: "FI" }, { name: "France", dial_code: "+33", code: "FR" }, { name: "French Guiana", dial_code: "+594", code: "GF" }, { name: "French Polynesia", dial_code: "+689", code: "PF" }, { name: "Gabon", dial_code: "+241", code: "GA" }, { name: "Gambia", dial_code: "+220", code: "GM" }, { name: "Georgia", dial_code: "+995", code: "GE" }, { name: "Germany", dial_code: "+49", code: "DE" }, { name: "Ghana", dial_code: "+233", code: "GH" }, { name: "Gibraltar", dial_code: "+350", code: "GI" }, { name: "Greece", dial_code: "+30", code: "GR" }, { name: "Greenland", dial_code: "+299", code: "GL" }, { name: "Grenada", dial_code: "+1 473", code: "GD" }, { name: "Guadeloupe", dial_code: "+590", code: "GP" }, { name: "Guam", dial_code: "+1 671", code: "GU" }, { name: "Guatemala", dial_code: "+502", code: "GT" }, { name: "Guernsey", dial_code: "+44", code: "GG" }, { name: "Guinea", dial_code: "+224", code: "GN" }, { name: "Guinea-Bissau", dial_code: "+245", code: "GW" }, { name: "Guyana", dial_code: "+595", code: "GY" }, { name: "Haiti", dial_code: "+509", code: "HT" }, { name: "Holy See (Vatican City State)", dial_code: "+379", code: "VA" }, { name: "Honduras", dial_code: "+504", code: "HN" }, { name: "Hong Kong", dial_code: "+852", code: "HK" }, { name: "Hungary", dial_code: "+36", code: "HU" }, { name: "Iceland", dial_code: "+354", code: "IS" }, { name: "India", dial_code: "+91", code: "IN" }, { name: "Indonesia", dial_code: "+62", code: "ID" }, { name: "Iran, Islamic Republic of", dial_code: "+98", code: "IR" }, { name: "Iraq", dial_code: "+964", code: "IQ" }, { name: "Ireland", dial_code: "+353", code: "IE" }, { name: "Isle of Man", dial_code: "+44", code: "IM" }, { name: "Israel", dial_code: "+972", code: "IL" }, { name: "Italy", dial_code: "+39", code: "IT" }, { name: "Jamaica", dial_code: "+1 876", code: "JM" }, { name: "Japan", dial_code: "+81", code: "JP" }, { name: "Jersey", dial_code: "+44", code: "JE" }, { name: "Jordan", dial_code: "+962", code: "JO" }, { name: "Kazakhstan", dial_code: "+7 7", code: "KZ" }, { name: "Kenya", dial_code: "+254", code: "KE" }, { name: "Kiribati", dial_code: "+686", code: "KI" }, {
        name: "Korea, Democratic People's Republic of",
        dial_code: "+850",
        code: "KP"
    }, { name: "Korea, Republic of", dial_code: "+82", code: "KR" }, { name: "Kuwait", dial_code: "+965", code: "KW" }, { name: "Kyrgyzstan", dial_code: "+996", code: "KG" }, { name: "Lao People's Democratic Republic", dial_code: "+856", code: "LA" }, { name: "Latvia", dial_code: "+371", code: "LV" }, { name: "Lebanon", dial_code: "+961", code: "LB" }, { name: "Lesotho", dial_code: "+266", code: "LS" }, { name: "Liberia", dial_code: "+231", code: "LR" }, { name: "Libyan Arab Jamahiriya", dial_code: "+218", code: "LY" }, { name: "Liechtenstein", dial_code: "+423", code: "LI" }, { name: "Lithuania", dial_code: "+370", code: "LT" }, { name: "Luxembourg", dial_code: "+352", code: "LU" }, { name: "Macao", dial_code: "+853", code: "MO" }, {
        name: "Macedonia, The Former Yugoslav Republic of",
        dial_code: "+389",
        code: "MK"
    }, { name: "Madagascar", dial_code: "+261", code: "MG" }, { name: "Malawi", dial_code: "+265", code: "MW" }, { name: "Malaysia", dial_code: "+60", code: "MY" }, { name: "Maldives", dial_code: "+960", code: "MV" }, { name: "Mali", dial_code: "+223", code: "ML" }, { name: "Malta", dial_code: "+356", code: "MT" }, { name: "Marshall Islands", dial_code: "+692", code: "MH" }, { name: "Martinique", dial_code: "+596", code: "MQ" }, { name: "Mauritania", dial_code: "+222", code: "MR" }, { name: "Mauritius", dial_code: "+230", code: "MU" }, { name: "Mayotte", dial_code: "+262", code: "YT" }, { name: "Mexico", dial_code: "+52", code: "MX" }, { name: "Micronesia, Federated States of", dial_code: "+691", code: "FM" }, { name: "Moldova, Republic of", dial_code: "+373", code: "MD" }, { name: "Monaco", dial_code: "+377", code: "MC" }, { name: "Mongolia", dial_code: "+976", code: "MN" }, { name: "Montenegro", dial_code: "+382", code: "ME" }, { name: "Montserrat", dial_code: "+1664", code: "MS" }, { name: "Morocco", dial_code: "+212", code: "MA" }, { name: "Mozambique", dial_code: "+258", code: "MZ" }, { name: "Myanmar", dial_code: "+95", code: "MM" }, { name: "Namibia", dial_code: "+264", code: "NA" }, { name: "Nauru", dial_code: "+674", code: "NR" }, { name: "Nepal", dial_code: "+977", code: "NP" }, { name: "Netherlands", dial_code: "+31", code: "NL" }, { name: "Netherlands Antilles", dial_code: "+599", code: "AN" }, { name: "New Caledonia", dial_code: "+687", code: "NC" }, { name: "New Zealand", dial_code: "+64", code: "NZ" }, { name: "Nicaragua", dial_code: "+505", code: "NI" }, { name: "Niger", dial_code: "+227", code: "NE" }, { name: "Nigeria", dial_code: "+234", code: "NG" }, { name: "Niue", dial_code: "+683", code: "NU" }, { name: "Norfolk Island", dial_code: "+672", code: "NF" }, { name: "Northern Mariana Islands", dial_code: "+1 670", code: "MP" }, { name: "Norway", dial_code: "+47", code: "NO" }, { name: "Oman", dial_code: "+968", code: "OM" }, { name: "Pakistan", dial_code: "+92", code: "PK" }, { name: "Palau", dial_code: "+680", code: "PW" }, { name: "Palestinian Territory, Occupied", dial_code: "+970", code: "PS" }, { name: "Panama", dial_code: "+507", code: "PA" }, { name: "Papua New Guinea", dial_code: "+675", code: "PG" }, { name: "Paraguay", dial_code: "+595", code: "PY" }, { name: "Peru", dial_code: "+51", code: "PE" }, { name: "Philippines", dial_code: "+63", code: "PH" }, { name: "Pitcairn", dial_code: "+872", code: "PN" }, { name: "Poland", dial_code: "+48", code: "PL" }, { name: "Portugal", dial_code: "+351", code: "PT" }, { name: "Puerto Rico", dial_code: "+1 939", code: "PR" }, { name: "Qatar", dial_code: "+974", code: "QA" }, { name: "Romania", dial_code: "+40", code: "RO" }, { name: "Russia", dial_code: "+7", code: "RU" }, { name: "Rwanda", dial_code: "+250", code: "RW" }, { name: "Réunion", dial_code: "+262", code: "RE" }, { name: "Saint Barthélemy", dial_code: "+590", code: "BL" }, {
        name: "Saint Helena, Ascension and Tristan Da Cunha",
        dial_code: "+290",
        code: "SH"
    }, { name: "Saint Kitts and Nevis", dial_code: "+1 869", code: "KN" }, { name: "Saint Lucia", dial_code: "+1 758", code: "LC" }, { name: "Saint Martin", dial_code: "+590", code: "MF" }, { name: "Saint Pierre and Miquelon", dial_code: "+508", code: "PM" }, {
        name: "Saint Vincent and the Grenadines",
        dial_code: "+1 784",
        code: "VC"
    }, { name: "Samoa", dial_code: "+685", code: "WS" }, { name: "San Marino", dial_code: "+378", code: "SM" }, { name: "Sao Tome and Principe", dial_code: "+239", code: "ST" }, { name: "Saudi Arabia", dial_code: "+966", code: "SA" }, { name: "Senegal", dial_code: "+221", code: "SN" }, { name: "Serbia", dial_code: "+381", code: "RS" }, { name: "Seychelles", dial_code: "+248", code: "SC" }, { name: "Sierra Leone", dial_code: "+232", code: "SL" }, { name: "Singapore", dial_code: "+65", code: "SG" }, { name: "Slovakia", dial_code: "+421", code: "SK" }, { name: "Slovenia", dial_code: "+386", code: "SI" }, { name: "Solomon Islands", dial_code: "+677", code: "SB" }, { name: "Somalia", dial_code: "+252", code: "SO" }, { name: "South Africa", dial_code: "+27", code: "ZA" }, {
        name: "South Georgia and the South Sandwich Islands",
        dial_code: "+500",
        code: "GS"
    }, { name: "Spain", dial_code: "+34", code: "ES" }, { name: "Sri Lanka", dial_code: "+94", code: "LK" }, { name: "Sudan", dial_code: "+249", code: "SD" }, { name: "Suriname", dial_code: "+597", code: "SR" }, { name: "Svalbard and Jan Mayen", dial_code: "+47", code: "SJ" }, { name: "Swaziland", dial_code: "+268", code: "SZ" }, { name: "Sweden", dial_code: "+46", code: "SE" }, { name: "Switzerland", dial_code: "+41", code: "CH" }, { name: "Syrian Arab Republic", dial_code: "+963", code: "SY" }, { name: "Taiwan, Province of China", dial_code: "+886", code: "TW" }, { name: "Tajikistan", dial_code: "+992", code: "TJ" }, { name: "Tanzania, United Republic of", dial_code: "+255", code: "TZ" }, { name: "Thailand", dial_code: "+66", code: "TH" }, { name: "Timor-Leste", dial_code: "+670", code: "TL" }, { name: "Togo", dial_code: "+228", code: "TG" }, { name: "Tokelau", dial_code: "+690", code: "TK" }, { name: "Tonga", dial_code: "+676", code: "TO" }, { name: "Trinidad and Tobago", dial_code: "+1 868", code: "TT" }, { name: "Tunisia", dial_code: "+216", code: "TN" }, { name: "Turkey", dial_code: "+90", code: "TR" }, { name: "Turkmenistan", dial_code: "+993", code: "TM" }, { name: "Turks and Caicos Islands", dial_code: "+1 649", code: "TC" }, { name: "Tuvalu", dial_code: "+688", code: "TV" }, { name: "Uganda", dial_code: "+256", code: "UG" }, { name: "Ukraine", dial_code: "+380", code: "UA" }, { name: "United Arab Emirates", dial_code: "+971", code: "AE" }, { name: "United Kingdom", dial_code: "+44", code: "GB" }, { name: "United States", dial_code: "+1", code: "US" }, { name: "Uruguay", dial_code: "+598", code: "UY" }, { name: "Uzbekistan", dial_code: "+998", code: "UZ" }, { name: "Vanuatu", dial_code: "+678", code: "VU" }, { name: "Venezuela, Bolivarian Republic of", dial_code: "+58", code: "VE" }, { name: "Viet Nam", dial_code: "+84", code: "VN" }, { name: "Virgin Islands, British", dial_code: "+1 284", code: "VG" }, { name: "Virgin Islands, U.S.", dial_code: "+1 340", code: "VI" }, { name: "Wallis and Futuna", dial_code: "+681", code: "WF" }, { name: "Yemen", dial_code: "+967", code: "YE" }, { name: "Zambia", dial_code: "+260", code: "ZM" }, { name: "Zimbabwe", dial_code: "+263", code: "ZW" }, { name: "Åland Islands", dial_code: "+358", code: "AX" }];
    return dials;
};

/***/ }),

/***/ 960:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(961)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(963)
/* template */
var __vue_template__ = __webpack_require__(964)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules/vue2-timepicker/src/vue-timepicker.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c4588f32", Component.options)
  } else {
    hotAPI.reload("data-v-c4588f32", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 961:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(962);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("04d296eb", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4588f32\",\"scoped\":false,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./vue-timepicker.vue", function() {
     var newContent = require("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4588f32\",\"scoped\":false,\"hasInlineConfig\":true}!../../vue-loader/lib/selector.js?type=styles&index=0!./vue-timepicker.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 962:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vue__time-picker {\n  display: inline-block;\n  position: relative;\n  font-size: 1em;\n  width: 10em;\n  font-family: sans-serif;\n  vertical-align: middle;\n}\n.vue__time-picker * {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n.vue__time-picker input.display-time {\n  border: 1px solid #d2d2d2;\n  width: 10em;\n  height: 2.2em;\n  padding: 0.3em 0.5em;\n  font-size: 1em;\n}\n.vue__time-picker input.display-time.invalid:not(.skip-error-style) {\n  border-color: #cc0033;\n  outline-color: #cc0033;\n}\n.vue__time-picker input.display-time:disabled,\n.vue__time-picker input.display-time.disabled {\n  color: #d2d2d2;\n}\n.vue__time-picker .clear-btn {\n  position: absolute;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: column nowrap;\n          flex-flow: column nowrap;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  width: 1.3em;\n  z-index: 3;\n  font-size: 1.1em;\n  line-height: 1em;\n  vertical-align: middle;\n  color: #d2d2d2;\n  background: rgba(255,255,255,0);\n  text-align: center;\n  font-style: normal;\n\n  /* Vertical align fixes for webkit browsers only */\n  -webkit-margin-before: -0.15em;\n\n  -webkit-transition: color .2s;\n  transition: color .2s;\n}\n.vue__time-picker .clear-btn:hover {\n  color: #797979;\n  cursor: pointer;\n}\n.vue__time-picker .clear-btn:active {\n  outline: 0;\n}\n.vue__time-picker .time-picker-overlay {\n  z-index: 2;\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n.vue__time-picker .dropdown {\n  position: absolute;\n  z-index: 5;\n  top: calc(2.2em + 2px);\n  left: 0;\n  background: #fff;\n  -webkit-box-shadow: 0 1px 6px rgba(0,0,0,0.15);\n          box-shadow: 0 1px 6px rgba(0,0,0,0.15);\n  width: 10em;\n  height: 10em;\n  font-weight: normal;\n}\n.vue__time-picker .dropdown .select-list {\n  width: 10em;\n  height: 10em;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: row nowrap;\n          flex-flow: row nowrap;\n  -webkit-box-align: stretch;\n      -ms-flex-align: stretch;\n          align-items: stretch;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.vue__time-picker .dropdown .select-list:focus,\n.vue__time-picker .dropdown .select-list:active {\n  outline: 0;\n}\n.vue__time-picker .dropdown ul {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  outline: 0;\n\n  -webkit-box-flex: 1;\n\n      -ms-flex: 1 1 0.00001px;\n\n          flex: 1 1 0.00001px;\n  overflow-x: hidden;\n  overflow-y: auto;\n}\n.vue__time-picker .dropdown ul.minutes,\n.vue__time-picker .dropdown ul.seconds,\n.vue__time-picker .dropdown ul.apms{\n  border-left: 1px solid #fff;\n}\n.vue__time-picker .dropdown ul li {\n  list-style: none;\n  text-align: center;\n  padding: 0.3em 0;\n  color: #161616;\n}\n.vue__time-picker .dropdown ul li:not(.hint):not([disabled]):hover,\n.vue__time-picker .dropdown ul li:not(.hint):not([disabled]):focus {\n  background: rgba(0,0,0,.08);\n  color: #161616;\n  cursor: pointer;\n}\n.vue__time-picker .dropdown ul li:not([disabled]).active,\n.vue__time-picker .dropdown ul li:not([disabled]).active:hover,\n.vue__time-picker .dropdown ul li:not([disabled]).active:focus {\n  background: #41B883;\n  color: #fff;\n}\n.vue__time-picker .dropdown ul li[disabled],\n.vue__time-picker .dropdown ul li[disabled]:hover {\n  background: transparent;\n  opacity: 0.3;\n  cursor: not-allowed;\n}\n.vue__time-picker .dropdown .hint {\n  color: #a5a5a5;\n  cursor: default;\n  font-size: 0.8em;\n}\n", ""]);

// exports


/***/ }),

/***/ 963:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var CONFIG = {
  HOUR_TOKENS: ['HH', 'H', 'hh', 'h', 'kk', 'k'],
  MINUTE_TOKENS: ['mm', 'm'],
  SECOND_TOKENS: ['ss', 's'],
  APM_TOKENS: ['A', 'a'],
  BASIC_TYPES: ['hour', 'minute', 'second', 'apm']
};

var DEFAULT_OPTIONS = {
  format: 'HH:mm',
  minuteInterval: 1,
  secondInterval: 1,
  hourRange: null,
  minuteRange: null,
  secondRange: null,
  hideDisabledHours: false,
  hideDisabledMinutes: false,
  hideDisabledSeconds: false,
  hideDisabledItems: false,
  advancedKeyboard: false,
  hideDropdown: false,
  blurDelay: 300,
  manualInputTimeout: 1000
};

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'VueTimepicker',
  props: {
    value: { type: [Object, String] },
    format: { type: String },
    minuteInterval: { type: [Number, String] },
    secondInterval: { type: [Number, String] },

    hourRange: { type: Array },
    minuteRange: { type: Array },
    secondRange: { type: Array },

    hideDisabledHours: { type: Boolean, default: false },
    hideDisabledMinutes: { type: Boolean, default: false },
    hideDisabledSeconds: { type: Boolean, default: false },
    hideDisabledItems: { type: Boolean, default: false },

    hideClearButton: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    closeOnComplete: { type: Boolean, default: false },

    id: { type: String },
    name: { type: String },
    inputClass: { type: [String, Object, Array] },
    placeholder: { type: String },
    tabindex: { type: [Number, String], default: 0 },
    inputWidth: { type: String },
    autocomplete: { type: String, default: 'off' },

    hourLabel: { type: String },
    minuteLabel: { type: String },
    secondLabel: { type: String },
    apmLabel: { type: String },
    amText: { type: String },
    pmText: { type: String },

    blurDelay: { type: [Number, String] },
    advancedKeyboard: { type: Boolean, default: false },
    lazy: { type: Boolean, default: false },

    autoScroll: { type: Boolean, default: false },
    manualInput: { type: Boolean, default: false },
    manualInputTimeout: { type: [Number, String] },
    hideDropdown: { type: Boolean, default: false },

    debugMode: { type: Boolean, default: false }
  },

  data: function data() {
    return {
      timeValue: {},

      hours: [],
      minutes: [],
      seconds: [],
      apms: [],

      showDropdown: false,
      isFocusing: false,
      debounceTimer: undefined,

      hourType: 'HH',
      minuteType: 'mm',
      secondType: '',
      apmType: '',
      hour: '',
      minute: '',
      second: '',
      apm: '',
      fullValues: undefined,
      bakDisplayTime: undefined,

      selectionTimer: undefined,
      kbInputTimer: undefined,
      kbInputLog: '',
      bakCurrentPos: null
    };
  },


  computed: {
    opts: function opts() {
      var options = Object.assign({}, DEFAULT_OPTIONS);

      if (this.format && this.format.length) {
        options.format = String(this.format);
      }

      if (this.isNumber(this.minuteInterval)) {
        options.minuteInterval = +this.minuteInterval;
      }
      // minuteInterval failsafe
      if (!options.minuteInterval || options.minuteInterval < 1 || options.minuteInterval > 60) {
        if (this.debugMode) {
          if (options.minuteInterval > 60) {
            this.debugLog('"minute-interval" should be less than 60. Current value is ' + this.minuteInterval);
          } else if (options.minuteInterval === 0 || options.minuteInterval < 1) {
            this.debugLog('"minute-interval" should be NO less than 1. Current value is ' + this.minuteInterval);
          }
        }
        if (options.minuteInterval === 0) {
          options.minuteInterval = 60;
        } else {
          options.minuteInterval = 1;
        }
      }

      if (this.isNumber(this.secondInterval)) {
        options.secondInterval = +this.secondInterval;
      }
      // secondInterval failsafe
      if (!options.secondInterval || options.secondInterval < 1 || options.secondInterval > 60) {
        if (this.debugMode) {
          if (options.secondInterval > 60) {
            this.debugLog('"second-interval" should be less than 60. Current value is ' + this.secondInterval);
          } else if (options.secondInterval === 0 || options.secondInterval < 1) {
            this.debugLog('"second-interval" should be NO less than 1. Current value is ' + this.secondInterval);
          }
        }
        if (options.secondInterval === 0) {
          options.secondInterval = 60;
        } else {
          options.secondInterval = 1;
        }
      }

      if (this.hourRange && Array.isArray(this.hourRange)) {
        options.hourRange = JSON.parse(JSON.stringify(this.hourRange));
        if (!this.hourRange.length && this.debugMode) {
          this.debugLog('The "hour-range" array is empty (length === 0)');
        }
      }

      if (this.minuteRange && Array.isArray(this.minuteRange)) {
        options.minuteRange = JSON.parse(JSON.stringify(this.minuteRange));
        if (!this.minuteRange.length && this.debugMode) {
          this.debugLog('The "minute-range" array is empty (length === 0)');
        }
      }

      if (this.secondRange && Array.isArray(this.secondRange)) {
        options.secondRange = JSON.parse(JSON.stringify(this.secondRange));
        if (!this.secondRange.length && this.debugMode) {
          this.debugLog('The "second-range" array is empty (length === 0)');
        }
      }

      if (this.hideDisabledItems) {
        options.hideDisabledItems = true;
      }

      if (this.hideDisabledHours || this.hideDisabledItems) {
        options.hideDisabledHours = true;
      }
      if (this.hideDisabledMinutes || this.hideDisabledItems) {
        options.hideDisabledMinutes = true;
      }
      if (this.hideDisabledSeconds || this.hideDisabledItems) {
        options.hideDisabledSeconds = true;
      }

      if (this.hideDropdown) {
        if (this.manualInput) {
          options.hideDropdown = true;
        } else if (this.debugMode) {
          this.debugLog('"hide-dropdown" only works with "manual-input" mode');
        }
      }

      if (this.advancedKeyboard) {
        if (!(this.hideDropdown && this.manualInput)) {
          options.advancedKeyboard = true;
        } else if (this.debugMode) {
          this.debugLog('"advanced-keyboard" has no effect when dropdown is force hidden by "hide-dropdown"');
        }
      }

      if (this.blurDelay && +this.blurDelay > 0) {
        options.blurDelay = +this.blurDelay;
      }

      if (this.manualInputTimeout && +this.manualInputTimeout > 0) {
        options.manualInputTimeout = +this.manualInputTimeout;
      }

      return options;
    },
    useStringValue: function useStringValue() {
      return typeof this.value === 'string';
    },
    formatString: function formatString() {
      return this.opts.format || DEFAULT_OPTIONS.format;
    },
    displayTime: function displayTime() {
      var formatString = String(this.formatString);
      if (this.hour) {
        formatString = formatString.replace(new RegExp(this.hourType, 'g'), this.hour);
      }
      if (this.minute) {
        formatString = formatString.replace(new RegExp(this.minuteType, 'g'), this.minute);
      }
      if (this.second && this.secondType) {
        formatString = formatString.replace(new RegExp(this.secondType, 'g'), this.second);
      }
      if (this.apm && this.apmType) {
        formatString = formatString.replace(new RegExp(this.apmType, 'g'), this.apm);
      }
      return formatString;
    },
    customDisplayTime: function customDisplayTime() {
      if (!this.amText && !this.pmText) {
        return this.displayTime;
      }
      return this.displayTime.replace(new RegExp(this.apm, 'g'), this.apmDisplayText(this.apm));
    },
    inputIsEmpty: function inputIsEmpty() {
      return this.formatString === this.displayTime;
    },
    allValueSelected: function allValueSelected() {
      if (!this.hour || !this.hour.length || !this.minute || !this.minute.length || this.secondType && (!this.second || !this.second.length) || this.apmType && (!this.apm || !this.apm.length)) {
        return false;
      }
      return true;
    },
    showClearBtn: function showClearBtn() {
      if (this.hideClearButton || this.disabled) {
        return false;
      }
      return !this.inputIsEmpty;
    },
    baseOn12Hours: function baseOn12Hours() {
      return this.hourType === 'h' || this.hourType === 'hh';
    },
    hourRangeIn24HrFormat: function hourRangeIn24HrFormat() {
      var _this = this;

      if (!this.opts.hourRange) {
        return false;
      }
      if (!this.opts.hourRange.length) {
        return [];
      }

      var range = [];
      this.opts.hourRange.forEach(function (value) {
        if (value instanceof Array) {
          if (value.length > 2 && _this.debugMode) {
            _this.debugLog('Nested array within "hour-range" must contain no more than two items. Only the first two items of ' + JSON.stringify(value) + ' will be taken into account.');
          }

          var start = value[0];
          var end = value[1] || value[0];

          if (_this.is12hRange(start)) {
            start = _this.translate12hRange(start);
          }
          if (_this.is12hRange(end)) {
            end = _this.translate12hRange(end);
          }

          for (var i = +start; i <= +end; i++) {
            if (i < 0 || i > 24) {
              continue;
            }
            if (!range.includes(i)) {
              range.push(i);
            }
          }
        } else {
          if (_this.is12hRange(value)) {
            value = _this.translate12hRange(value);
          } else {
            value = +value;
          }
          if (value < 0 || value > 24) {
            return;
          }
          if (!range.includes(value)) {
            range.push(value);
          }
        }
      });
      range.sort(function (l, r) {
        return l - r;
      });
      return range;
    },
    restrictedHourRange: function restrictedHourRange() {
      // No restriction
      if (!this.hourRangeIn24HrFormat) {
        return false;
      }
      // 12-Hour
      if (this.baseOn12Hours) {
        var range = this.hourRangeIn24HrFormat.map(function (value) {
          if (value === 12) {
            return '12p';
          } else if (value === 24 || value === 0) {
            return '12a';
          }
          return value > 12 ? value % 12 + 'p' : value + 'a';
        });
        return range;
      }
      // 24-Hour
      return this.hourRangeIn24HrFormat;
    },
    validHoursList: function validHoursList() {
      var _this2 = this;

      if (!this.manualInput) {
        return false;
      }
      if (this.restrictedHourRange) {
        var list = [];
        if (this.baseOn12Hours) {
          list = this.restrictedHourRange.map(function (hr) {
            var l = hr.substr(0, hr.length - 1);
            var r = hr.substr(-1);
            return '' + _this2.formatValue(_this2.hourType, l) + r;
          });
          var am12Index = list.findIndex(function (hr) {
            return hr === '12a';
          });
          if (am12Index > 0) {
            // Make '12a' the first item in h/hh
            list.unshift(list.splice(am12Index, 1)[0]);
          }
          return list;
        }
        list = this.restrictedHourRange.map(function (hr) {
          return _this2.formatValue(_this2.hourType, hr);
        });
        if (list.length > 1 && list[0] && list[0] === '24') {
          // Make '24' the last item in k/kk
          list.push(list.shift());
        }
        return list;
      }
      if (this.baseOn12Hours) {
        return [].concat([], this.hours.map(function (hr) {
          return hr + 'a';
        }), this.hours.map(function (hr) {
          return hr + 'p';
        }));
      }
      return this.hours;
    },
    has: function has() {
      var result = {
        am: true,
        pm: true,
        customApmText: false
      };
      if (this.hourRangeIn24HrFormat && this.hourRangeIn24HrFormat.length) {
        var range = [].concat([], this.hourRangeIn24HrFormat);
        result.am = range.some(this.hasAm);
        result.pm = range.some(this.hasPm);
      }
      if (this.amText && this.amText.length || this.pmText && this.pmText.length) {
        result.customApmText = true;
      }
      return result;
    },
    minuteRangeList: function minuteRangeList() {
      var _this3 = this;

      if (!this.opts.minuteRange) {
        return false;
      }
      if (!this.opts.minuteRange.length) {
        return [];
      }
      var range = [];
      var formatedValue = void 0;
      this.opts.minuteRange.forEach(function (value) {
        if (value instanceof Array) {
          if (value.length > 2 && _this3.debugMode) {
            _this3.debugLog('Nested array within "minute-range" must contain no more than two items. Only the first two items of ' + JSON.stringify(value) + ' will be taken into account.');
          }
          var start = value[0];
          var end = value[1] || value[0];
          for (var i = +start; i <= +end; i++) {
            if (i < 0 || i > 59) {
              continue;
            }
            formatedValue = _this3.formatValue(_this3.minuteType, i);
            if (!range.includes(formatedValue)) {
              range.push(formatedValue);
            }
          }
        } else {
          if (+value < 0 || +value > 59) {
            return;
          }
          formatedValue = _this3.formatValue(_this3.minuteType, value);
          if (!range.includes(formatedValue)) {
            range.push(formatedValue);
          }
        }
      });
      range.sort(function (l, r) {
        return l - r;
      });
      // Debug Mode
      if (this.debugMode) {
        var validItems = (this.minutes || []).filter(function (item) {
          return range.includes(item);
        });
        if (!validItems || !validItems.length) {
          this.debugLog('The minute list is empty due to the "minute-range" config\nminute-range: ' + JSON.stringify(this.minuteRange) + '\nminute-interval: ' + this.opts.minuteInterval);
        }
      }
      return range;
    },
    secondRangeList: function secondRangeList() {
      var _this4 = this;

      if (!this.opts.secondRange) {
        return false;
      }
      if (!this.opts.secondRange.length) {
        return [];
      }
      var range = [];
      var formatedValue = void 0;
      this.opts.secondRange.forEach(function (value) {
        if (value instanceof Array) {
          if (value.length > 2 && _this4.debugMode) {
            _this4.debugLog('Nested array within "second-range" must contain no more than two items. Only the first two items of ' + JSON.stringify(value) + ' will be taken into account.');
          }
          var start = value[0];
          var end = value[1] || value[0];
          for (var i = +start; i <= +end; i++) {
            if (i < 0 || i > 59) {
              continue;
            }
            formatedValue = _this4.formatValue(_this4.secondType, i);
            if (!range.includes(formatedValue)) {
              range.push(formatedValue);
            }
          }
        } else {
          if (+value < 0 || +value > 59) {
            return;
          }
          formatedValue = _this4.formatValue(_this4.secondType, value);
          if (!range.includes(formatedValue)) {
            range.push(formatedValue);
          }
        }
      });
      range.sort(function (l, r) {
        return l - r;
      });
      // Debug Mode
      if (this.debugMode) {
        var validItems = (this.seconds || []).filter(function (item) {
          return range.includes(item);
        });
        if (!validItems || !validItems.length) {
          this.debugLog('The second list is empty due to the "second-range" config\nsecond-range: ' + JSON.stringify(this.secondRange) + '\nsecond-interval: ' + this.opts.secondInterval);
        }
      }
      return range;
    },
    hourLabelText: function hourLabelText() {
      return this.hourLabel || this.hourType;
    },
    minuteLabelText: function minuteLabelText() {
      return this.minuteLabel || this.minuteType;
    },
    secondLabelText: function secondLabelText() {
      return this.secondLabel || this.secondType;
    },
    apmLabelText: function apmLabelText() {
      return this.apmLabel || this.apmType;
    },
    inputWidthStyle: function inputWidthStyle() {
      if (!this.inputWidth || !this.inputWidth.length) {
        return;
      }
      return {
        width: this.inputWidth
      };
    },
    tokenRegexBase: function tokenRegexBase() {
      if (!this.manualInput && !this.useStringValue) {
        return false;
      }
      var regexStr = this.hourType + '|' + this.minuteType;
      if (this.secondType) {
        regexStr += '|' + this.secondType;
      }
      if (this.apmType) {
        regexStr += '|' + this.apmType;
      }
      return regexStr;
    },
    tokenChunks: function tokenChunks() {
      if (!this.manualInput && !this.useStringValue) {
        return false;
      }

      var formatString = String(this.formatString);
      var tokensRegxStr = '[(' + this.tokenRegexBase + ')]+';
      var tokensMatchAll = this.getMatchAllByRegex(formatString, tokensRegxStr);

      var tokenChunks = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = tokensMatchAll[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var tkMatch = _step.value;

          var rawToken = tkMatch[0];
          var tokenMatchItem = {
            index: tkMatch.index,
            token: rawToken,
            type: this.getTokenType(rawToken),
            needsCalibrate: rawToken.length < 2,
            len: (rawToken || '').length
          };
          tokenChunks.push(tokenMatchItem);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return tokenChunks;
    },
    needsPosCalibrate: function needsPosCalibrate() {
      if (!this.manualInput) {
        return false;
      }
      return this.tokenChunks.some(function (chk) {
        return chk.needsCalibrate;
      });
    },
    tokenChunksPos: function tokenChunksPos() {
      var _this5 = this;

      if (!this.manualInput) {
        return false;
      }
      if (!this.needsPosCalibrate) {
        return this.tokenChunks.map(function (chk) {
          return {
            token: chk.token,
            type: chk.type,
            start: chk.index,
            end: chk.index + chk.len
          };
        });
      }
      var list = [];
      var calibrateLen = 0;
      this.tokenChunks.forEach(function (chk) {
        var chunkCurrentLen = void 0;
        // Adjust for customized AM/PM text
        if (chk.type === 'apm' && _this5.has.customApmText) {
          if (_this5.apm && _this5.apm.length) {
            var customApmText = _this5.apm.toLowerCase() === 'am' ? _this5.amText : _this5.pmText;
            chunkCurrentLen = customApmText && customApmText.length ? customApmText.length : chk.len;
          } else {
            chunkCurrentLen = chk.len;
          }
          // Others
        } else {
          chunkCurrentLen = _this5[chk.type] && _this5[chk.type].length ? _this5[chk.type].length : chk.len;
        }
        list.push({
          token: chk.token,
          type: chk.type,
          start: chk.index + calibrateLen,
          end: chk.index + calibrateLen + chunkCurrentLen
        });
        if (chk.needsCalibrate && chunkCurrentLen > chk.len) {
          calibrateLen += chunkCurrentLen - chk.len;
        }
      });
      return list;
    },
    invalidValues: function invalidValues() {
      if (this.inputIsEmpty) {
        return [];
      }
      if (!this.restrictedHourRange && !this.minuteRangeList && !this.secondRangeList && this.opts.minuteInterval === 1 && this.opts.secondInterval === 1) {
        return [];
      }

      var result = [];
      if (!this.isEmptyValue(this.hourType, this.hour) && (!this.isValidValue(this.hourType, this.hour) || this.isDisabled('hour', this.hour))) {
        result.push('hour');
      }
      if (!this.isEmptyValue(this.minuteType, this.minute) && (!this.isValidValue(this.minuteType, this.minute) || this.isDisabled('minute', this.minute) || this.notInMinuteInterval(this.minute))) {
        result.push('minute');
      }
      if (this.secondType && !this.isEmptyValue(this.secondType, this.second) && (!this.isValidValue(this.secondType, this.second) || this.isDisabled('second', this.second) || this.notInSecondInterval(this.second))) {
        result.push('second');
      }
      if (this.apmType && !this.isEmptyValue(this.apmType, this.apm) && (!this.isValidValue(this.apmType, this.apm) || this.isDisabled('apm', this.apm))) {
        result.push('apm');
      }
      if (result.length) {
        return result;
      }
      return [];
    },
    hasInvalidInput: function hasInvalidInput() {
      return Boolean(this.invalidValues && this.invalidValues.length);
    }
  },

  watch: {
    'opts.format': function optsFormat(newValue) {
      this.renderFormat(newValue);
    },
    'opts.minuteInterval': function optsMinuteInterval(newInteval) {
      this.renderList('minute', newInteval);
    },
    'opts.secondInterval': function optsSecondInterval(newInteval) {
      this.renderList('second', newInteval);
    },

    value: {
      deep: true,
      handler: function handler() {
        this.readValues();
      }
    },
    displayTime: function displayTime() {
      this.fillValues();
    },
    disabled: function disabled(toDisabled) {
      // Force close the dropdown when disabled
      if (toDisabled && this.showDropdown) {
        this.showDropdown = false;
      }
    },
    'invalidValues.length': function invalidValuesLength(newLength, oldLength) {
      if (newLength && newLength >= 1) {
        this.$emit('error', this.invalidValues);
      } else if (oldLength && oldLength >= 1) {
        this.$emit('error', []);
      }
    }
  },

  methods: {
    formatValue: function formatValue(token, i) {
      i = +i;
      switch (token) {
        case 'H':
        case 'h':
        case 'k':
        case 'm':
        case 's':
          if (['h', 'k'].includes(token) && i === 0) {
            return token === 'k' ? '24' : '12';
          }
          return String(i);
        case 'HH':
        case 'mm':
        case 'ss':
        case 'hh':
        case 'kk':
          if (['hh', 'kk'].includes(token) && i === 0) {
            return token === 'kk' ? '24' : '12';
          }
          return i < 10 ? '0' + i : String(i);
        default:
          return '';
      }
    },
    checkAcceptingType: function checkAcceptingType(validValues, formatString, fallbackValue) {
      if (!validValues || !formatString || !formatString.length) {
        return '';
      }
      for (var i = 0; i < validValues.length; i++) {
        if (formatString.indexOf(validValues[i]) > -1) {
          return validValues[i];
        }
      }
      return fallbackValue || '';
    },
    renderFormat: function renderFormat(newFormat) {
      var _this6 = this;

      newFormat = newFormat || this.opts.format || DEFAULT_OPTIONS.format;

      this.hourType = this.checkAcceptingType(CONFIG.HOUR_TOKENS, newFormat, 'HH');
      this.minuteType = this.checkAcceptingType(CONFIG.MINUTE_TOKENS, newFormat, 'mm');
      this.secondType = this.checkAcceptingType(CONFIG.SECOND_TOKENS, newFormat);
      this.apmType = this.checkAcceptingType(CONFIG.APM_TOKENS, newFormat);

      this.renderHoursList();
      this.renderList('minute');

      if (this.secondType) {
        this.renderList('second');
      } else {
        this.seconds = [];
      }

      if (this.apmType) {
        this.renderApmList();
      } else {
        this.apms = [];
      }

      this.$nextTick(function () {
        _this6.readValues();
      });
    },
    renderHoursList: function renderHoursList() {
      var hoursCount = this.baseOn12Hours ? 12 : 24;
      var hours = [];
      for (var i = 0; i < hoursCount; i++) {
        if (this.hourType === 'k' || this.hourType === 'kk') {
          hours.push(this.formatValue(this.hourType, i + 1));
        } else {
          hours.push(this.formatValue(this.hourType, i));
        }
      }
      this.hours = hours;
    },
    renderList: function renderList(listType, interval) {
      if (!['minute', 'second'].includes(listType)) {
        return;
      }

      var isMinute = listType === 'minute';
      interval = interval || (isMinute ? this.opts.minuteInterval || DEFAULT_OPTIONS.minuteInterval : this.opts.secondInterval || DEFAULT_OPTIONS.secondInterval);

      var result = [];
      for (var i = 0; i < 60; i += interval) {
        result.push(this.formatValue(isMinute ? this.minuteType : this.secondType, i));
      }

      isMinute ? this.minutes = result : this.seconds = result;
    },
    renderApmList: function renderApmList() {
      this.apms = this.apmType === 'A' ? ['AM', 'PM'] : ['am', 'pm'];
    },
    readValues: function readValues() {
      if (this.useStringValue) {
        if (this.debugMode) {
          this.debugLog('Received a string value: "' + this.value + '"');
        }
        this.readStringValues(this.value);
      } else {
        if (this.debugMode) {
          this.debugLog('Received an object value: "' + JSON.stringify(this.value || {}) + '"');
        }
        this.readObjectValues(this.value);
      }
    },
    readObjectValues: function readObjectValues(objValue) {
      var _this7 = this;

      var timeValue = JSON.parse(JSON.stringify(objValue || {}));
      var values = Object.keys(timeValue);

      // Failsafe for empty `v-model` object
      if (values.length === 0) {
        this.addFallbackValues();
        return;
      }

      CONFIG.BASIC_TYPES.forEach(function (section) {
        var sectionType = _this7[section + 'Type'];
        if (values.indexOf(sectionType) > -1) {
          var sanitizedValue = _this7.sanitizedValue(sectionType, timeValue[sectionType]);
          _this7[section] = sanitizedValue;
          timeValue[sectionType] = sanitizedValue;
        } else {
          _this7[section] = '';
        }
      });
      this.timeValue = timeValue;
    },
    getMatchAllByRegex: function getMatchAllByRegex(testString, regexString) {
      var str = 'polyfillTest';
      var needsPolyfill = Boolean(!str.matchAll || typeof str.matchAll !== 'function');
      return needsPolyfill ? this.polyfillMatchAll(testString, regexString) : testString.matchAll(new RegExp(regexString, 'g'));
    },
    readStringValues: function readStringValues(stringValue) {
      var _this8 = this;

      // Failsafe for empty `v-model` string
      if (!stringValue || !stringValue.length) {
        this.addFallbackValues();
        return;
      }

      var formatString = String(this.formatString);
      var tokensRegxStr = '[(' + this.tokenRegexBase + ')]+';
      var othersRegxStr = '[^(' + this.tokenRegexBase + ')]+';

      var tokensMatchAll = this.getMatchAllByRegex(formatString, tokensRegxStr);
      var othersMatchAll = this.getMatchAllByRegex(formatString, othersRegxStr);

      var chunks = [];
      var tokenChunks = [];

      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = tokensMatchAll[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var tkMatch = _step2.value;

          var tokenMatchItem = {
            index: tkMatch.index,
            token: tkMatch[0],
            isValueToken: true
          };
          chunks.push(tokenMatchItem);
          tokenChunks.push(tokenMatchItem);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = othersMatchAll[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var otMatch = _step3.value;

          chunks.push({
            index: otMatch.index,
            token: otMatch[0]
          });
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }

      chunks.sort(function (l, r) {
        return l.index < r.index ? -1 : 1;
      });

      var regexCombo = '';
      chunks.forEach(function (chunk) {
        if (chunk.isValueToken) {
          var tokenRegex = _this8.getTokenRegex(chunk.token) || '';
          regexCombo += tokenRegex;
        } else {
          var safeChars = chunk.token.replace(/\\{0}(\*|\?|\.|\+)/g, '\\$1');
          regexCombo += '(?:' + safeChars + ')';
        }
      });

      var comboReg = new RegExp(regexCombo);

      // Do test before match
      if (comboReg.test(stringValue)) {
        var matchResults = stringValue.match(new RegExp(regexCombo));
        var valueResults = matchResults.slice(1, tokenChunks.length + 1);
        var timeValue = {};
        valueResults.forEach(function (value, vrIndex) {
          if (tokenChunks[vrIndex]) {
            var targetToken = tokenChunks[vrIndex].token;
            timeValue[targetToken] = _this8.setValueFromString(value, targetToken);
          }
        });
        this.timeValue = timeValue;

        if (this.debugMode) {
          var tokenChunksForLog = tokenChunks.map(function (tChunk) {
            return tChunk && tChunk.token;
          });
          this.debugLog('Successfully parsed values ' + JSON.stringify(valueResults) + '\nfor ' + JSON.stringify(tokenChunksForLog) + '\nin format pattern \'' + this.formatString + '\'');
        }
      } else {
        if (this.debugMode) {
          this.debugLog('The input string in "v-model" does NOT match the "format" pattern\nformat: ' + this.formatString + '\nv-model: ' + stringValue);
        }
      }
    },
    polyfillMatchAll: function polyfillMatchAll(targetString, regxStr) {
      var matchesList = targetString.match(new RegExp(regxStr, 'g'));
      var result = [];
      var indicesReg = [];
      if (matchesList && matchesList.length) {
        matchesList.forEach(function (matchedItem) {
          var existIndex = indicesReg.findIndex(function (idxItem) {
            return idxItem.str === matchedItem;
          });
          var index = void 0;
          if (existIndex >= 0) {
            if (indicesReg[existIndex] && indicesReg[existIndex].regex) {
              index = indicesReg[existIndex].regex.exec(targetString).index;
            }
          } else {
            var itemIndicesRegex = new RegExp(matchedItem, 'g');
            index = itemIndicesRegex.exec(targetString).index;
            indicesReg.push({
              str: String(matchedItem),
              regex: itemIndicesRegex
            });
          }
          result.push({
            0: String(matchedItem),
            index: index
          });
        });
      }
      return result;
    },
    addFallbackValues: function addFallbackValues() {
      var timeValue = {};
      timeValue[this.hourType] = '';
      timeValue[this.minuteType] = '';
      if (this.secondType) {
        timeValue[this.secondType] = '';
      }
      if (this.apmType) {
        timeValue[this.apmType] = '';
      }
      this.timeValue = timeValue;
    },
    setValueFromString: function setValueFromString(parsedValue, token) {
      if (!token || !parsedValue) {
        return '';
      }
      var tokenType = this.getTokenType(token);
      if (!tokenType || !tokenType.length) {
        return '';
      }
      var stdValue = parsedValue !== this[tokenType + 'Type'] ? parsedValue : '';
      this[tokenType] = stdValue;
      return stdValue;
    },
    fillValues: function fillValues(forceEmit) {
      var _this9 = this;

      var fullValues = {};

      var baseHour = this.hour;
      var baseHourType = this.hourType;

      var hourValue = this.isNumber(baseHour) ? +baseHour : '';
      var apmValue = this.baseOn12Hours && this.apm ? this.lowerCasedApm(this.apm) : false;

      CONFIG.HOUR_TOKENS.forEach(function (token) {
        if (token === baseHourType) {
          fullValues[token] = baseHour;
          return;
        }

        var value = void 0;
        var apm = void 0;
        switch (token) {
          case 'H':
          case 'HH':
            if (!String(hourValue).length) {
              fullValues[token] = '';
              return;
            } else if (_this9.baseOn12Hours) {
              if (apmValue === 'pm') {
                value = hourValue < 12 ? hourValue + 12 : hourValue;
              } else {
                value = hourValue % 12;
              }
            } else {
              value = hourValue % 24;
            }
            fullValues[token] = token === 'HH' && value < 10 ? '0' + value : String(value);
            break;
          case 'k':
          case 'kk':
            if (!String(hourValue).length) {
              fullValues[token] = '';
              return;
            } else if (_this9.baseOn12Hours) {
              if (apmValue === 'pm') {
                value = hourValue < 12 ? hourValue + 12 : hourValue;
              } else {
                value = hourValue === 12 ? 24 : hourValue;
              }
            } else {
              value = hourValue === 0 ? 24 : hourValue;
            }
            fullValues[token] = token === 'kk' && value < 10 ? '0' + value : String(value);
            break;
          case 'h':
          case 'hh':
            if (apmValue) {
              value = hourValue;
              apm = apmValue || 'am';
            } else {
              if (!String(hourValue).length) {
                fullValues[token] = '';
                fullValues.a = '';
                fullValues.A = '';
                return;
              } else if (hourValue > 11 && hourValue < 24) {
                apm = 'pm';
                value = hourValue === 12 ? 12 : hourValue % 12;
              } else {
                if (_this9.baseOn12Hours) {
                  apm = '';
                } else {
                  apm = 'am';
                }
                value = hourValue % 12 === 0 ? 12 : hourValue;
              }
            }
            fullValues[token] = token === 'hh' && value < 10 ? '0' + value : String(value);
            fullValues.a = apm;
            fullValues.A = apm.toUpperCase();
            break;
        }
      });

      if (this.isNumber(this.minute)) {
        var minuteValue = +this.minute;
        fullValues.m = String(minuteValue);
        fullValues.mm = minuteValue < 10 ? '0' + minuteValue : String(minuteValue);
      } else {
        fullValues.m = '';
        fullValues.mm = '';
      }

      if (this.isNumber(this.second)) {
        var secondValue = +this.second;
        fullValues.s = String(secondValue);
        fullValues.ss = secondValue < 10 ? '0' + secondValue : String(secondValue);
      } else {
        fullValues.s = '';
        fullValues.ss = '';
      }

      this.fullValues = fullValues;

      // On lazy mode, emit `input` and `change` events only when:
      // - The user pick a new value and then close the dropdown
      // - The user click the ("x") clear button
      if (!this.lazy || forceEmit) {
        this.emitTimeValue();
      }

      if (this.closeOnComplete && this.allValueSelected && this.showDropdown) {
        this.toggleDropdown();
      }
    },
    emitTimeValue: function emitTimeValue() {
      if (!this.fullValues) {
        return;
      }

      if (this.lazy && this.bakDisplayTime === this.displayTime) {
        if (this.debugMode) {
          this.debugLog('The value does not change on `lazy` mode. Skip the emitting `input` and `change` event.');
        }
        return;
      }

      var fullValues = JSON.parse(JSON.stringify(this.fullValues));
      var baseTimeValue = JSON.parse(JSON.stringify(this.timeValue || {}));
      var timeValue = {};

      Object.keys(baseTimeValue).forEach(function (key) {
        timeValue[key] = fullValues[key] || '';
      });

      if (this.useStringValue) {
        this.$emit('input', this.inputIsEmpty ? '' : String(this.displayTime));
      } else {
        this.$emit('input', JSON.parse(JSON.stringify(timeValue)));
      }

      this.$emit('change', {
        data: fullValues,
        displayTime: this.inputIsEmpty ? '' : String(this.displayTime)
      });
    },
    translate12hRange: function translate12hRange(value) {
      var valueT = value.match(/^(\d{1,2})(a|p|A|P)$/);
      if (+valueT[1] === 12) {
        return +valueT[1] + (valueT[2].toLowerCase() === 'p' ? 0 : 12);
      }
      return +valueT[1] + (valueT[2].toLowerCase() === 'p' ? 12 : 0);
    },
    hasAm: function hasAm(value) {
      return value < 12 || value === 24;
    },
    hasPm: function hasPm(value) {
      return value >= 12 && value < 24;
    },
    isDisabled: function isDisabled(type, value) {
      if (!this.isBasicType(type)) {
        return true;
      }
      switch (type) {
        case 'hour':
          return this.isDisabledHour(value);
        case 'minute':
        case 'second':
          if (!this[type + 'RangeList']) {
            return false;
          }
          return !this[type + 'RangeList'].includes(value);
        case 'apm':
          if (!this.restrictedHourRange) {
            return false;
          }
          return !this.has[this.lowerCasedApm(value)];
        default:
          return true;
      }
    },
    isDisabledHour: function isDisabledHour(value) {
      if (!this.restrictedHourRange) {
        return false;
      }
      if (this.baseOn12Hours) {
        if (!this.apm || !this.apm.length) {
          return false;
        } else {
          var token = this.apm.toLowerCase() === 'am' ? 'a' : 'p';
          return !this.restrictedHourRange.includes('' + +value + token);
        }
      }
      // Fallback for 'HH' and 'H hour format with a `hour-range` in a 12-hour form
      if ((this.hourType === 'HH' || this.hourType === 'H') && +value === 0 && this.restrictedHourRange.includes(24)) {
        return false;
      }
      return !this.restrictedHourRange.includes(+value);
    },
    notInMinuteInterval: function notInMinuteInterval(value) {
      if (this.opts.minuteInterval === 1) {
        return false;
      }
      return +value % this.opts.minuteInterval !== 0;
    },
    notInSecondInterval: function notInSecondInterval(value) {
      if (this.opts.secondInterval === 1) {
        return false;
      }
      return +value % this.opts.secondInterval !== 0;
    },
    forceApmSelection: function forceApmSelection() {
      if (!this.apm || !this.apm.length) {
        if (this.manualInput) {
          // In Manual Input Mode
          // Skip this to allow users to paste a string value from clipboard
          return;
        }
        if (this.has.am) {
          this.apm = this.apmType === 'A' ? 'AM' : 'am';
        } else if (this.has.pm) {
          this.apm = this.apmType === 'A' ? 'PM' : 'pm';
        }
      }
    },
    emptyApmSelection: function emptyApmSelection() {
      if (this.hour === '' && this.minute === '' && this.second === '') {
        this.apm = '';
      }
    },
    apmDisplayText: function apmDisplayText(apmValue) {
      if (this.amText && this.lowerCasedApm(apmValue) === 'am') {
        return this.amText;
      }
      if (this.pmText && this.lowerCasedApm(apmValue) === 'pm') {
        return this.pmText;
      }
      return apmValue;
    },
    toggleDropdown: function toggleDropdown() {
      if (this.disabled) {
        return;
      }
      this.showDropdown = !this.showDropdown;

      if (this.showDropdown) {
        if (!this.opts.hideDropdown) {
          this.$emit('open');
        }
        this.isFocusing = true;
        this.$emit('focus');
        // Record to check if value did changed in the later phase
        if (this.lazy) {
          this.bakDisplayTime = String(this.displayTime || '');
        }
      } else {
        if (!this.opts.hideDropdown) {
          this.$emit('close');
        }
        this.isFocusing = false;
        this.$emit('blur');
        if (this.lazy) {
          this.fillValues(true);
          this.bakDisplayTime = undefined;
        }
      }

      if (this.showDropdown) {
        if (this.manualInput) {
          return;
        }
        if (this.restrictedHourRange && this.baseOn12Hours) {
          this.forceApmSelection();
        }
        this.checkForAutoScroll();
      } else if (this.restrictedHourRange && this.baseOn12Hours) {
        this.emptyApmSelection();
      }
    },
    select: function select(type, value) {
      if (this.isBasicType(type) && !this.isDisabled(type, value)) {
        this[type] = value;
      }
    },
    clearTime: function clearTime() {
      if (this.disabled) {
        return;
      }
      this.hour = '';
      this.minute = '';
      this.second = '';
      this.apm = '';

      if (this.manualInput && this.$refs && this.$refs.input && this.$refs.input.value.length) {
        this.$refs.input.value = '';
      }

      if (this.lazy) {
        this.fillValues(true);
      }
    },


    //
    // Auto-Scroll
    //

    checkForAutoScroll: function checkForAutoScroll() {
      var _this10 = this;

      if (this.inputIsEmpty) {
        return;
      }
      if (this.autoScroll) {
        this.$nextTick(function () {
          _this10.scrollToSelectedValues();
        });
      } else if (this.opts.advancedKeyboard) {
        // Auto-focus on selected hour value for advanced-keyboard
        this.$nextTick(function () {
          _this10.scrollToSelected('hours');
        });
      }
    },
    scrollToSelected: function scrollToSelected(columnClass) {
      if (!this.timeValue || this.inputIsEmpty) {
        return;
      }
      var targetList = this.$el.querySelectorAll('ul.' + columnClass)[0];
      var targetValue = this.$el.querySelectorAll('ul.' + columnClass + ' li.active:not(.hint)')[0];
      if (targetList && targetValue) {
        targetList.scrollTop = targetValue.offsetTop || 0;
        if (this.opts.advancedKeyboard && columnClass === 'hours') {
          targetValue.focus();
        }
      }
    },
    scrollToSelectedValues: function scrollToSelectedValues() {
      if (!this.timeValue || this.inputIsEmpty) {
        return;
      }
      this.scrollToSelected('hours');
      this.scrollToSelected('minutes');
      if (this.secondType) {
        this.scrollToSelected('seconds');
      }
    },


    //
    // Additional Keyboard Navigation
    //

    onFocus: function onFocus() {
      if (this.disabled) {
        return;
      }
      if (!this.isFocusing) {
        this.isFocusing = true;
      }
      if (!this.showDropdown) {
        this.toggleDropdown();
      }
    },
    escBlur: function escBlur() {
      if (this.disabled) {
        return;
      }
      this.isFocusing = false;
      var inputBox = this.$el.querySelectorAll('input.display-time')[0];
      if (inputBox) {
        inputBox.blur();
      }
    },
    debounceBlur: function debounceBlur() {
      var _this11 = this;

      if (this.disabled) {
        return;
      }
      this.isFocusing = false;
      window.clearTimeout(this.debounceTimer);
      this.debounceTimer = window.setTimeout(function () {
        window.clearTimeout(_this11.debounceTimer);
        _this11.onBlur();
      }, this.opts.blurDelay);
    },
    onBlur: function onBlur() {
      if (this.disabled) {
        return;
      }
      if (!this.isFocusing) {
        if (this.showDropdown) {
          this.toggleDropdown();
        }
      }
    },
    keepFocusing: function keepFocusing() {
      if (!this.isFocusing) {
        this.isFocusing = true;
      }
    },
    validItemsInCol: function validItemsInCol(columnClass) {
      return this.$el.querySelectorAll('ul.' + columnClass + ' > li:not(.hint):not([disabled])');
    },
    activeItemInCol: function activeItemInCol(columnClass) {
      return this.$el.querySelectorAll('ul.' + columnClass + ' > li.active:not(.hint)');
    },
    getClosestSibling: function getClosestSibling(columnClass, dataKey) {
      var getPrevious = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var siblingsInCol = this.validItemsInCol(columnClass);
      var selfIndex = Array.prototype.findIndex.call(siblingsInCol, function (sbl) {
        return sbl.getAttribute('data-key') === dataKey;
      });

      // Already the first item
      if (getPrevious && selfIndex === 0) {
        return siblingsInCol[siblingsInCol.length - 1];
      }
      // Already the last item
      if (!getPrevious && selfIndex === siblingsInCol.length - 1) {
        return siblingsInCol[0];
      }
      // Selected value not in the valid values list
      if (selfIndex < 0) {
        return siblingsInCol[0];
      }

      if (getPrevious) {
        return siblingsInCol[selfIndex - 1];
      }
      return siblingsInCol[selfIndex + 1];
    },
    prevItem: function prevItem(columnClass, dataKey) {
      var isManualInput = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var targetItem = this.getClosestSibling(columnClass, dataKey, true);
      if (targetItem) {
        return isManualInput ? targetItem : targetItem.focus();
      }
    },
    nextItem: function nextItem(columnClass, dataKey) {
      var isManualInput = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var targetItem = this.getClosestSibling(columnClass, dataKey, false);
      if (targetItem) {
        return isManualInput ? targetItem : targetItem.focus();
      }
    },
    getSideColumnClass: function getSideColumnClass(columnClass) {
      var toLeft = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var targetColumn = void 0;
      // Nav to Left
      if (toLeft) {
        switch (columnClass) {
          case 'hours':
            targetColumn = -1;
            break;
          case 'minutes':
            targetColumn = 'hours';
            break;
          case 'seconds':
            targetColumn = 'minutes';
            break;
          case 'apms':
            if (this.secondType) {
              targetColumn = 'seconds';
            } else {
              targetColumn = 'minutes';
            }
            break;
        }
        // Nav to Right
      } else {
        switch (columnClass) {
          case 'hours':
            targetColumn = 'minutes';
            break;
          case 'minutes':
            if (this.secondType) {
              targetColumn = 'seconds';
            } else if (this.apmType) {
              targetColumn = 'apms';
            } else {
              targetColumn = 1;
            }
            break;
          case 'seconds':
            if (this.apmType) {
              targetColumn = 'apms';
            } else {
              targetColumn = 1;
            }
            break;
          case 'apms':
            targetColumn = 1;
            break;
        }
      }

      if (targetColumn === -1) {
        if (this.debugMode) {
          this.debugLog('You\'re in the leftmost list already');
        }
        return;
      } else if (targetColumn === 1) {
        if (this.debugMode) {
          this.debugLog('You\'re in the rightmost list already');
        }
        return;
      }

      return targetColumn;
    },
    getFirstItemInSideColumn: function getFirstItemInSideColumn(columnClass) {
      var toLeft = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var targetColumnClass = this.getSideColumnClass(columnClass, toLeft);
      if (!targetColumnClass) {
        return;
      }
      var listItems = this.validItemsInCol(targetColumnClass);
      if (listItems && listItems[0]) {
        return listItems[0];
      }
    },
    getActiveItemInSideColumn: function getActiveItemInSideColumn(columnClass) {
      var toLeft = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var targetColumnClass = this.getSideColumnClass(columnClass, toLeft);
      if (!targetColumnClass) {
        return;
      }
      var activeItems = this.activeItemInCol(targetColumnClass);
      if (activeItems && activeItems[0]) {
        return activeItems[0];
      }
    },
    toLeftColumn: function toLeftColumn(columnClass) {
      var targetItem = this.getActiveItemInSideColumn(columnClass, true) || this.getFirstItemInSideColumn(columnClass, true);
      if (targetItem) {
        targetItem.focus();
      }
    },
    toRightColumn: function toRightColumn(columnClass) {
      var targetItem = this.getActiveItemInSideColumn(columnClass, false) || this.getFirstItemInSideColumn(columnClass, false);
      if (targetItem) {
        targetItem.focus();
      }
    },


    //
    // Manual Input
    //

    onMouseDown: function onMouseDown() {
      var _this12 = this;

      if (!this.manualInput) {
        return;
      }
      window.clearTimeout(this.selectionTimer);
      this.selectionTimer = window.setTimeout(function () {
        window.clearTimeout(_this12.selectionTimer);
        if (_this12.$refs && _this12.$refs.input) {
          var nearestSlot = _this12.getNearestChunkByPos(_this12.$refs.input.selectionStart || 0);
          _this12.debounceSetInputSelection(nearestSlot);
        }
      }, 50);
    },
    keyDownHandler: function keyDownHandler(evt) {
      if (evt.isComposing || evt.keyCode === 229) {
        // Skip IME inputs
        evt.preventDefault();
        evt.stopPropagation();
        return false;
      }
      // Numbers
      if (evt.keyCode >= 48 && evt.keyCode <= 57 || evt.keyCode >= 96 && evt.keyCode <= 105) {
        evt.preventDefault();
        this.keyboardInput(evt.key);
        // A|P|M
      } else if ([65, 80, 77].includes(evt.keyCode)) {
        evt.preventDefault();
        this.keyboardInput(evt.key, true);
        // Arrow keys
      } else if (evt.keyCode >= 37 && evt.keyCode <= 40) {
        evt.preventDefault();
        this.clearKbInputLog();
        this.arrowHandler(evt);
        // Delete|Backspace
      } else if (evt.keyCode === 8 || evt.keyCode === 46) {
        evt.preventDefault();
        this.clearKbInputLog();
        this.clearTime();
        // Tab
      } else if (evt.keyCode === 9) {
        this.clearKbInputLog();
        this.tabHandler(evt);
        // Prevent any Non-ESC and non-pasting inputs
      } else if (evt.keyCode !== 27 && !(evt.metaKey || evt.ctrlKey)) {
        evt.preventDefault();
      }
    },
    onCompostionStart: function onCompostionStart(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      this.bakCurrentPos = this.getCurrentTokenChunk();
      return false;
    },
    onCompostionEnd: function onCompostionEnd(evt) {
      var _this13 = this;

      evt.preventDefault();
      evt.stopPropagation();

      var cpsData = evt.data;
      var inputIsCustomApmText = false;
      if (this.has.customApmText) {
        inputIsCustomApmText = this.isCustomApmText(cpsData);
      }
      if (inputIsCustomApmText) {
        this.setSanitizedValueToSection('apm', inputIsCustomApmText);
      }

      this.$refs.input.value = this.has.customApmText ? this.customDisplayTime : this.displayTime;

      this.$nextTick(function () {
        if (_this13.bakCurrentPos) {
          var bakPos = JSON.parse(JSON.stringify(_this13.bakCurrentPos));
          if (inputIsCustomApmText) {
            bakPos.end = bakPos.start + cpsData.length;
          }
          _this13.debounceSetInputSelection(bakPos);
          _this13.bakCurrentPos = null;
        }
      });
      return false;
    },
    pasteHandler: function pasteHandler(evt) {
      evt.preventDefault();
      var pastingText = (evt.clipboardData || window.clipboardData).getData('text');
      if (this.debugMode) {
        this.debugLog('Pasting value "' + pastingText + '" from clipboard');
      }
      if (!pastingText || !pastingText.length) {
        return;
      }

      // Replace custom AM/PM text (if any)
      if (this.has.customApmText) {
        pastingText = this.replaceCustomApmText(pastingText);
      }

      if (this.inputIsEmpty) {
        this.readStringValues(pastingText);
      } else {
        this.kbInputLog = pastingText.substr(-2, 2);
        this.setKbInput();
        this.debounceClearKbLog();
      }
    },
    arrowHandler: function arrowHandler(evt) {
      var direction = { 37: 'L', 38: 'U', 39: 'R', 40: 'D' }[evt.keyCode];
      if (direction === 'U' || direction === 'D') {
        if (this.inputIsEmpty) {
          this.selectFirstValidValue();
        } else {
          var currentChunk = this.getCurrentTokenChunk();
          if (!currentChunk) {
            this.selectFirstValidValue();
            return;
          }
          var tokenType = currentChunk.type;
          this.getClosestValidItemInCol(tokenType, this[tokenType], direction);
          var newChunkPos = this.getCurrentTokenChunk();
          this.debounceSetInputSelection(newChunkPos);
        }
      } else if (direction === 'R') {
        this.toLateralToken(false);
      } else if (direction === 'L') {
        this.toLateralToken(true);
      }
    },
    tabHandler: function tabHandler(evt) {
      if (!this.inputIsEmpty && this.tokenChunksPos && this.tokenChunksPos.length) {
        var currentChunk = this.getCurrentTokenChunk();
        if (!currentChunk) {
          return;
        }
        var lastChunk = this.tokenChunksPos[this.tokenChunksPos.length - 1];
        if (currentChunk.token !== lastChunk.token) {
          evt.preventDefault();
          this.toLateralToken(false);
        }
      }
    },
    keyboardInput: function keyboardInput(newChar) {
      var isApm = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var currentChunk = this.getCurrentTokenChunk();
      if (!currentChunk || currentChunk.type !== 'apm' && isApm || currentChunk.type === 'apm' && !isApm) {
        return;
      }
      this.kbInputLog = '' + this.kbInputLog.substr(-1) + newChar;
      this.setKbInput();
      this.debounceClearKbLog();
    },
    clearKbInputLog: function clearKbInputLog() {
      window.clearTimeout(this.kbInputTimer);
      this.kbInputLog = '';
    },
    debounceClearKbLog: function debounceClearKbLog() {
      var _this14 = this;

      window.clearTimeout(this.kbInputTimer);
      this.kbInputTimer = window.setTimeout(function () {
        _this14.clearKbInputLog();
      }, this.opts.manualInputTimeout);
    },
    setKbInput: function setKbInput(value) {
      value = value || this.kbInputLog;
      var currentChunk = this.getCurrentTokenChunk();
      if (!currentChunk || !value || !value.length) {
        return;
      }
      var chunkType = currentChunk.type;
      var chunkToken = currentChunk.token;

      var validValue = void 0;
      if (chunkType === 'apm') {
        if (this.lowerCasedApm(value).includes('a')) {
          validValue = 'am';
        } else if (this.lowerCasedApm(value).includes('p')) {
          validValue = 'pm';
        }
        if (validValue) {
          validValue = chunkToken === 'A' ? validValue.toUpperCase() : validValue;
        }
      } else {
        if (this.isValidValue(chunkToken, value)) {
          validValue = value;
        } else {
          var lastInputValue = this.formatValue(chunkToken, value.substr(-1));
          if (this.isValidValue(chunkToken, lastInputValue)) {
            validValue = lastInputValue;
          }
        }
      }

      if (validValue) {
        this.setSanitizedValueToSection(chunkType, validValue);
        var newChunkPos = this.getCurrentTokenChunk();
        this.debounceSetInputSelection(newChunkPos);
      }
      if (this.debugMode) {
        if (validValue) {
          this.debugLog('Successfully set value "' + validValue + '" from latest input "' + value + '" for the "' + chunkType + '" slot');
        } else {
          this.debugLog('Value "' + value + '" is invalid in the "' + chunkType + '" slot');
        }
      }
    },


    // Form Autofill
    onChange: function onChange() {
      if (!this.$refs || !this.$refs.input || !this.manualInput) {
        return;
      }
      var autoFillValue = this.$refs.input.value || '';
      if (autoFillValue && autoFillValue.length) {
        this.readStringValues(autoFillValue);
      }
    },
    getNearestChunkByPos: function getNearestChunkByPos(startPos) {
      if (!this.tokenChunksPos || !this.tokenChunksPos.length) {
        return;
      }
      var nearest = void 0;
      var nearestDelta = -1;
      for (var i = 0; i < this.tokenChunksPos.length; i++) {
        var chunk = JSON.parse(JSON.stringify(this.tokenChunksPos[i]));
        if (chunk.start === startPos) {
          return chunk;
        }
        var delta = Math.abs(chunk.start - startPos);
        if (nearestDelta < 0) {
          nearest = chunk;
          nearestDelta = delta;
        } else {
          if (nearestDelta <= delta) {
            return nearest;
          }
          nearestDelta = delta;
          nearest = chunk;
        }
      }
      return nearest;
    },
    selectFirstValidValue: function selectFirstValidValue() {
      if (!this.tokenChunksPos || !this.tokenChunksPos.length) {
        return;
      }
      var firstTokenType = this.tokenChunksPos[0].type;
      if (firstTokenType) {
        this.selectFirstValidValueInCol(firstTokenType);
      }
    },
    selectFirstValidValueInCol: function selectFirstValidValueInCol(tokenType) {
      if (tokenType === 'hour') {
        this.selectFirstValidHour();
      } else {
        this.getClosestValidItemInCol(tokenType, this[tokenType]);
      }
    },
    selectFirstValidHour: function selectFirstValidHour() {
      var _this15 = this;

      if (!this.validHoursList || !this.validHoursList.length) {
        return;
      }

      var hourChunk = this.tokenChunksPos.find(function (chk) {
        return chk.token === _this15.hourType;
      });
      if (!hourChunk) {
        return;
      }
      var hourToken = hourChunk.token;

      this.setManualHour(this.validHoursList[0]);
      var newChunkPos = this.getChunkPosByToken(hourToken);
      this.debounceSetInputSelection(newChunkPos);
    },
    getClosestValidItemInCol: function getClosestValidItemInCol(column, currentValue) {
      var _this16 = this;

      var direction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'U';

      if (column === 'hour') {
        if (!this.validHoursList || !this.validHoursList.length) {
          if (this.debugMode) {
            this.debugLog('No valid hour values found, please check your "hour-range" config\nhour-range: ' + JSON.stringify(this.hourRange));
          }
          return;
        }
        var currentIndex = this.validHoursList.findIndex(function (item) {
          if (!_this16.baseOn12Hours) {
            return item === currentValue;
          } else {
            var valueKey = '' + currentValue + (_this16.lowerCasedApm(_this16.apm) === 'pm' ? 'p' : 'a');
            return item === valueKey;
          }
        });
        var nextIndex = void 0;
        if (currentIndex === -1) {
          nextIndex = 0;
        } else if (direction === 'D') {
          nextIndex = currentIndex === 0 ? this.validHoursList.length - 1 : currentIndex - 1;
        } else {
          nextIndex = (currentIndex + 1) % this.validHoursList.length;
        }
        var nextItem = this.validHoursList[nextIndex];
        this.setManualHour(nextItem);
      } else {
        var _nextItem = direction === 'D' ? this.prevItem(column + 's', this[column], true) : this.nextItem(column + 's', this[column], true);
        if (_nextItem) {
          this.select(column, _nextItem.getAttribute('data-key'));
        }
      }
    },
    setSanitizedValueToSection: function setSanitizedValueToSection(section, inputValue) {
      if (!section || !this[section + 'Type']) {
        return;
      }
      // NOTE: Disabled values are allowed here, followed by an 'error' event, though
      var sanitizedValue = this.sanitizedValue(this[section + 'Type'], inputValue);
      this[section] = sanitizedValue;
    },
    setManualHour: function setManualHour(nextItem) {
      if (this.is12hRange(nextItem)) {
        var hourT = nextItem.match(/^(\d{1,2})(a|p|A|P)$/);
        var apmValue = hourT[2] === 'a' ? 'AM' : 'PM';
        this.setSanitizedValueToSection('apm', this.apmType === 'a' ? apmValue.toLowerCase() : apmValue);
        this.setSanitizedValueToSection('hour', hourT[1]);
      } else {
        this.setSanitizedValueToSection('hour', nextItem);
      }
    },
    debounceSetInputSelection: function debounceSetInputSelection(_ref) {
      var _this17 = this;

      var _ref$start = _ref.start,
          start = _ref$start === undefined ? 0 : _ref$start,
          _ref$end = _ref.end,
          end = _ref$end === undefined ? 0 : _ref$end;

      this.$nextTick(function () {
        _this17.setInputSelectionRange(start, end);
      });
      window.clearTimeout(this.selectionTimer);
      this.selectionTimer = window.setTimeout(function () {
        window.clearTimeout(_this17.selectionTimer);
        // Double-check selection for 12hr format
        if (_this17.$refs.input && (_this17.$refs.input.selectionStart !== start || _this17.$refs.input.selectionEnd !== end)) {
          _this17.setInputSelectionRange(start, end);
        }
      }, 30);
    },
    setInputSelectionRange: function setInputSelectionRange(start, end) {
      if (this.$refs && this.$refs.input) {
        this.$refs.input.setSelectionRange(start, end);
      }
    },
    getCurrentTokenChunk: function getCurrentTokenChunk() {
      return this.getNearestChunkByPos(this.$refs.input && this.$refs.input.selectionStart || 0);
    },
    getChunkPosByToken: function getChunkPosByToken(token) {
      if (!this.tokenChunksPos || !token) {
        return { start: 0, end: 0 };
      }
      var targetChunk = this.tokenChunksPos.find(function (chk) {
        return chk.token === token;
      });
      return targetChunk ? targetChunk : { start: 0, end: 0 };
    },
    toLateralToken: function toLateralToken(toLeft) {
      var currentChunk = this.getCurrentTokenChunk();
      if (!currentChunk) {
        this.selectFirstValidValue();
        return;
      }
      var currentChunkIndex = this.tokenChunksPos.findIndex(function (chk) {
        return chk.start === currentChunk.start;
      });
      if (!toLeft && currentChunkIndex >= this.tokenChunksPos.length - 1 || toLeft && currentChunkIndex === 0) {
        if (this.debugMode) {
          if (toLeft) {
            this.debugLog('You\'re in the leftmost slot already');
          } else {
            this.debugLog('You\'re in the rightmost slot already');
          }
        }
        return;
      }
      var targetSlotPos = toLeft ? this.tokenChunksPos[currentChunkIndex - 1] : this.tokenChunksPos[currentChunkIndex + 1];
      this.debounceSetInputSelection(targetSlotPos);
    },
    isCustomApmText: function isCustomApmText(inputData) {
      if (!inputData || !inputData.length) {
        return false;
      }
      if (this.amText && this.amText === inputData) {
        return this.apmType === 'A' ? 'AM' : 'am';
      }
      if (this.pmText && this.pmText === inputData) {
        return this.apmType === 'A' ? 'PM' : 'pm';
      }
      return false;
    },
    replaceCustomApmText: function replaceCustomApmText(inputString) {
      if (this.amText && this.amText.length && inputString.includes(this.amText)) {
        return inputString.replace(new RegExp(this.amText, 'g'), this.apmType === 'A' ? 'AM' : 'am');
      } else if (this.pmText && this.pmText.length && inputString.includes(this.pmText)) {
        return inputString.replace(new RegExp(this.pmText, 'g'), this.apmType === 'A' ? 'PM' : 'pm');
      }
      return inputString;
    },


    //
    // Helpers
    //

    is12hRange: function is12hRange(value) {
      return (/^\d{1,2}(a|p|A|P)$/.test(value)
      );
    },
    isNumber: function isNumber(value) {
      return !isNaN(parseFloat(value)) && isFinite(value);
    },
    isBasicType: function isBasicType(type) {
      return CONFIG.BASIC_TYPES.includes(type);
    },
    lowerCasedApm: function lowerCasedApm(apmValue) {
      return (apmValue || '').toLowerCase();
    },
    getTokenRegex: function getTokenRegex(token) {
      switch (token) {
        case 'HH':
          return '([01][0-9]|2[0-3]|H{2})';
        case 'H':
          return '([0-9]{1}|1[0-9]|2[0-3]|H{1})';
        case 'hh':
          return '(0[1-9]|1[0-2]|h{2})';
        case 'h':
          return '([1-9]{1}|1[0-2]|h{1})';
        case 'kk':
          return '(0[1-9]|1[0-9]|2[0-4]|k{2})';
        case 'k':
          return '([1-9]{1}|1[0-9]|2[0-4]|k{1})';
        case 'mm':
          return '([0-5][0-9]|m{2})';
        case 'ss':
          return '([0-5][0-9]|s{2})';
        case 'm':
          return '([0-9]{1}|[1-5][0-9]|m{1})';
        case 's':
          return '([0-9]{1}|[1-5][0-9]|s{1})';
        case 'A':
          return '(AM|PM|A{1})';
        case 'a':
          return '(am|pm|a{1})';
        default:
          return '';
      }
    },
    isEmptyValue: function isEmptyValue(targetToken, testValue) {
      return !testValue || !testValue.length || testValue && testValue === targetToken;
    },
    isValidValue: function isValidValue(targetToken, testValue) {
      if (!targetToken || this.isEmptyValue(targetToken, testValue)) {
        return false;
      }
      var tokenRegexStr = this.getTokenRegex(targetToken);
      if (!tokenRegexStr || !tokenRegexStr.length) {
        return false;
      }
      return new RegExp('^' + tokenRegexStr + '$').test(testValue);
    },
    sanitizedValue: function sanitizedValue(targetToken, inputValue) {
      if (this.isValidValue(targetToken, inputValue)) {
        return inputValue;
      }
      return '';
    },
    getTokenType: function getTokenType(token) {
      var _this18 = this;

      var typesInUse = CONFIG.BASIC_TYPES.filter(function (tokenType) {
        return _this18[tokenType + 'Type'];
      });
      var activeTokens = typesInUse.map(function (tokenType) {
        return _this18[tokenType + 'Type'];
      });
      return typesInUse[activeTokens.indexOf(token)] || '';
    },
    debugLog: function debugLog(logText) {
      var _this19 = this;

      if (!logText || !logText.length) {
        return;
      }
      var identifier = '';
      if (this.id) {
        identifier += '#' + this.id;
      }
      if (this.name) {
        identifier += '[name=' + this.name + ']';
      }
      if (this.inputClass) {
        var inputClasses = [];
        if (typeof this.inputClass === 'string') {
          inputClasses = this.inputClass.split(/\s/g);
        } else if (Array.isArray(this.inputClass)) {
          inputClasses = [].concat([], this.inputClass);
        } else if (_typeof(this.inputClass) === 'object') {
          Object.keys(this.inputClass).forEach(function (clsName) {
            if (_this19.inputClass[clsName]) {
              inputClasses.push(clsName);
            }
          });
        }
        var _iteratorNormalCompletion4 = true;
        var _didIteratorError4 = false;
        var _iteratorError4 = undefined;

        try {
          for (var _iterator4 = inputClasses[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
            var inputClass = _step4.value;

            if (inputClass && inputClass.trim().length) {
              identifier += '.' + inputClass.trim();
            }
          }
        } catch (err) {
          _didIteratorError4 = true;
          _iteratorError4 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion4 && _iterator4.return) {
              _iterator4.return();
            }
          } finally {
            if (_didIteratorError4) {
              throw _iteratorError4;
            }
          }
        }
      }
      var finalLogText = 'DEBUG: ' + logText + (identifier ? '\n\t(' + identifier + ')' : '');
      if (window.console.debug && typeof window.console.debug === 'function') {
        window.console.debug(finalLogText);
      } else {
        window.console.log(finalLogText);
      }
    }
  },

  mounted: function mounted() {
    window.clearTimeout(this.debounceTimer);
    window.clearTimeout(this.selectionTimer);
    window.clearTimeout(this.kbInputTimer);
    this.renderFormat();
  },
  beforeDestroy: function beforeDestroy() {
    window.clearTimeout(this.debounceTimer);
    window.clearTimeout(this.selectionTimer);
    window.clearTimeout(this.kbInputTimer);
  }
});

/***/ }),

/***/ 964:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "span",
    { staticClass: "vue__time-picker time-picker", style: _vm.inputWidthStyle },
    [
      _c("input", {
        ref: "input",
        staticClass: "display-time",
        class: [
          _vm.inputClass,
          {
            "is-empty": _vm.inputIsEmpty,
            invalid: _vm.hasInvalidInput,
            "all-selected": _vm.allValueSelected,
            disabled: _vm.disabled
          }
        ],
        style: _vm.inputWidthStyle,
        attrs: {
          type: "text",
          id: _vm.id,
          name: _vm.name,
          placeholder: _vm.placeholder ? _vm.placeholder : _vm.formatString,
          tabindex: _vm.disabled ? -1 : _vm.tabindex,
          disabled: _vm.disabled,
          readonly: !_vm.manualInput,
          autocomplete: _vm.autocomplete
        },
        domProps: { value: _vm.inputIsEmpty ? null : _vm.customDisplayTime },
        on: {
          focus: _vm.onFocus,
          change: _vm.onChange,
          blur: _vm.debounceBlur,
          mousedown: _vm.onMouseDown,
          keydown: [
            _vm.keyDownHandler,
            function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "esc", 27, $event.key, ["Esc", "Escape"])
              ) {
                return null
              }
              if (
                $event.ctrlKey ||
                $event.shiftKey ||
                $event.altKey ||
                $event.metaKey
              ) {
                return null
              }
              return _vm.escBlur($event)
            }
          ],
          compositionstart: _vm.onCompostionStart,
          compositionend: _vm.onCompostionEnd,
          paste: _vm.pasteHandler
        }
      }),
      _vm._v(" "),
      !_vm.showDropdown && _vm.showClearBtn
        ? _c(
            "span",
            {
              staticClass: "clear-btn",
              attrs: { tabindex: "-1" },
              on: { click: _vm.clearTime }
            },
            [_vm._v("×")]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.showDropdown
        ? _c("div", {
            staticClass: "time-picker-overlay",
            attrs: { tabindex: "-1" },
            on: { click: _vm.toggleDropdown }
          })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.showDropdown && !_vm.opts.hideDropdown,
              expression: "showDropdown && !opts.hideDropdown"
            }
          ],
          staticClass: "dropdown",
          style: _vm.inputWidthStyle,
          attrs: { tabindex: "-1" },
          on: {
            mouseup: _vm.keepFocusing,
            click: function($event) {
              $event.stopPropagation()
            }
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "select-list",
              style: _vm.inputWidthStyle,
              attrs: { tabindex: "-1" }
            },
            [
              !_vm.opts.advancedKeyboard
                ? [
                    _c(
                      "ul",
                      {
                        staticClass: "hours",
                        on: { scroll: _vm.keepFocusing }
                      },
                      [
                        _c("li", {
                          staticClass: "hint",
                          domProps: { textContent: _vm._s(_vm.hourLabelText) }
                        }),
                        _vm._v(" "),
                        _vm._l(_vm.hours, function(hr, hIndex) {
                          return [
                            !_vm.opts.hideDisabledHours ||
                            (_vm.opts.hideDisabledHours &&
                              !_vm.isDisabled("hour", hr))
                              ? _c("li", {
                                  key: hIndex,
                                  class: { active: _vm.hour === hr },
                                  attrs: {
                                    disabled: _vm.isDisabled("hour", hr),
                                    "data-key": hr
                                  },
                                  domProps: { textContent: _vm._s(hr) },
                                  on: {
                                    click: function($event) {
                                      return _vm.select("hour", hr)
                                    }
                                  }
                                })
                              : _vm._e()
                          ]
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass: "minutes",
                        on: { scroll: _vm.keepFocusing }
                      },
                      [
                        _c("li", {
                          staticClass: "hint",
                          domProps: { textContent: _vm._s(_vm.minuteLabelText) }
                        }),
                        _vm._v(" "),
                        _vm._l(_vm.minutes, function(m, mIndex) {
                          return [
                            !_vm.opts.hideDisabledMinutes ||
                            (_vm.opts.hideDisabledMinutes &&
                              !_vm.isDisabled("minute", m))
                              ? _c("li", {
                                  key: mIndex,
                                  class: { active: _vm.minute === m },
                                  attrs: {
                                    disabled: _vm.isDisabled("minute", m),
                                    "data-key": m
                                  },
                                  domProps: { textContent: _vm._s(m) },
                                  on: {
                                    click: function($event) {
                                      return _vm.select("minute", m)
                                    }
                                  }
                                })
                              : _vm._e()
                          ]
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _vm.secondType
                      ? _c(
                          "ul",
                          {
                            staticClass: "seconds",
                            on: { scroll: _vm.keepFocusing }
                          },
                          [
                            _c("li", {
                              staticClass: "hint",
                              domProps: {
                                textContent: _vm._s(_vm.secondLabelText)
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.seconds, function(s, sIndex) {
                              return [
                                !_vm.opts.hideDisabledSeconds ||
                                (_vm.opts.hideDisabledSeconds &&
                                  !_vm.isDisabled("second", s))
                                  ? _c("li", {
                                      key: sIndex,
                                      class: { active: _vm.second === s },
                                      attrs: {
                                        disabled: _vm.isDisabled("second", s),
                                        "data-key": s
                                      },
                                      domProps: { textContent: _vm._s(s) },
                                      on: {
                                        click: function($event) {
                                          return _vm.select("second", s)
                                        }
                                      }
                                    })
                                  : _vm._e()
                              ]
                            })
                          ],
                          2
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.apmType
                      ? _c(
                          "ul",
                          {
                            staticClass: "apms",
                            on: { scroll: _vm.keepFocusing }
                          },
                          [
                            _c("li", {
                              staticClass: "hint",
                              domProps: {
                                textContent: _vm._s(_vm.apmLabelText)
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.apms, function(a, aIndex) {
                              return [
                                !_vm.opts.hideDisabledHours ||
                                (_vm.opts.hideDisabledHours &&
                                  !_vm.isDisabled("apm", a))
                                  ? _c("li", {
                                      key: aIndex,
                                      class: { active: _vm.apm === a },
                                      attrs: {
                                        disabled: _vm.isDisabled("apm", a),
                                        "data-key": a
                                      },
                                      domProps: {
                                        textContent: _vm._s(
                                          _vm.apmDisplayText(a)
                                        )
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.select("apm", a)
                                        }
                                      }
                                    })
                                  : _vm._e()
                              ]
                            })
                          ],
                          2
                        )
                      : _vm._e()
                  ]
                : _vm._e(),
              _vm._v(" "),
              _vm.opts.advancedKeyboard
                ? [
                    _c(
                      "ul",
                      {
                        staticClass: "hours",
                        attrs: { tabindex: "-1" },
                        on: { scroll: _vm.keepFocusing }
                      },
                      [
                        _c("li", {
                          staticClass: "hint",
                          attrs: { tabindex: "-1" },
                          domProps: { textContent: _vm._s(_vm.hourLabelText) }
                        }),
                        _vm._v(" "),
                        _vm._l(_vm.hours, function(hr, hIndex) {
                          return [
                            !_vm.opts.hideDisabledHours ||
                            (_vm.opts.hideDisabledHours &&
                              !_vm.isDisabled("hour", hr))
                              ? _c("li", {
                                  key: hIndex,
                                  class: { active: _vm.hour === hr },
                                  attrs: {
                                    tabindex: _vm.isDisabled("hour", hr)
                                      ? -1
                                      : _vm.tabindex,
                                    "data-key": hr,
                                    disabled: _vm.isDisabled("hour", hr)
                                  },
                                  domProps: { textContent: _vm._s(hr) },
                                  on: {
                                    click: function($event) {
                                      return _vm.select("hour", hr)
                                    },
                                    keydown: [
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "space",
                                            32,
                                            $event.key,
                                            [" ", "Spacebar"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.select("hour", hr)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "enter",
                                            13,
                                            $event.key,
                                            "Enter"
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.select("hour", hr)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "up",
                                            38,
                                            $event.key,
                                            ["Up", "ArrowUp"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.prevItem("hours", hr)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "down",
                                            40,
                                            $event.key,
                                            ["Down", "ArrowDown"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.nextItem("hours", hr)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "left",
                                            37,
                                            $event.key,
                                            ["Left", "ArrowLeft"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          "button" in $event &&
                                          $event.button !== 0
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.toLeftColumn("hours")
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "right",
                                            39,
                                            $event.key,
                                            ["Right", "ArrowRight"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          "button" in $event &&
                                          $event.button !== 2
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.toRightColumn("hours")
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "esc",
                                            27,
                                            $event.key,
                                            ["Esc", "Escape"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          $event.ctrlKey ||
                                          $event.shiftKey ||
                                          $event.altKey ||
                                          $event.metaKey
                                        ) {
                                          return null
                                        }
                                        return _vm.debounceBlur($event)
                                      }
                                    ],
                                    blur: _vm.debounceBlur,
                                    focus: _vm.keepFocusing
                                  }
                                })
                              : _vm._e()
                          ]
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass: "minutes",
                        attrs: { tabindex: "-1" },
                        on: { scroll: _vm.keepFocusing }
                      },
                      [
                        _c("li", {
                          staticClass: "hint",
                          attrs: { tabindex: "-1" },
                          domProps: { textContent: _vm._s(_vm.minuteLabelText) }
                        }),
                        _vm._v(" "),
                        _vm._l(_vm.minutes, function(m, mIndex) {
                          return [
                            !_vm.opts.hideDisabledMinutes ||
                            (_vm.opts.hideDisabledMinutes &&
                              !_vm.isDisabled("minute", m))
                              ? _c("li", {
                                  key: mIndex,
                                  class: { active: _vm.minute === m },
                                  attrs: {
                                    tabindex: _vm.isDisabled("minute", m)
                                      ? -1
                                      : _vm.tabindex,
                                    "data-key": m,
                                    disabled: _vm.isDisabled("minute", m)
                                  },
                                  domProps: { textContent: _vm._s(m) },
                                  on: {
                                    click: function($event) {
                                      return _vm.select("minute", m)
                                    },
                                    keydown: [
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "space",
                                            32,
                                            $event.key,
                                            [" ", "Spacebar"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.select("minute", m)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "enter",
                                            13,
                                            $event.key,
                                            "Enter"
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.select("minute", m)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "up",
                                            38,
                                            $event.key,
                                            ["Up", "ArrowUp"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.prevItem("minutes", m)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "down",
                                            40,
                                            $event.key,
                                            ["Down", "ArrowDown"]
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.nextItem("minutes", m)
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "left",
                                            37,
                                            $event.key,
                                            ["Left", "ArrowLeft"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          "button" in $event &&
                                          $event.button !== 0
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.toLeftColumn("minutes")
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "right",
                                            39,
                                            $event.key,
                                            ["Right", "ArrowRight"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          "button" in $event &&
                                          $event.button !== 2
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.toRightColumn("minutes")
                                      },
                                      function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "esc",
                                            27,
                                            $event.key,
                                            ["Esc", "Escape"]
                                          )
                                        ) {
                                          return null
                                        }
                                        if (
                                          $event.ctrlKey ||
                                          $event.shiftKey ||
                                          $event.altKey ||
                                          $event.metaKey
                                        ) {
                                          return null
                                        }
                                        return _vm.debounceBlur($event)
                                      }
                                    ],
                                    blur: _vm.debounceBlur,
                                    focus: _vm.keepFocusing
                                  }
                                })
                              : _vm._e()
                          ]
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _vm.secondType
                      ? _c(
                          "ul",
                          {
                            staticClass: "seconds",
                            attrs: { tabindex: "-1" },
                            on: { scroll: _vm.keepFocusing }
                          },
                          [
                            _c("li", {
                              staticClass: "hint",
                              attrs: { tabindex: "-1" },
                              domProps: {
                                textContent: _vm._s(_vm.secondLabelText)
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.seconds, function(s, sIndex) {
                              return [
                                !_vm.opts.hideDisabledSeconds ||
                                (_vm.opts.hideDisabledSeconds &&
                                  !_vm.isDisabled("second", s))
                                  ? _c("li", {
                                      key: sIndex,
                                      class: { active: _vm.second === s },
                                      attrs: {
                                        tabindex: _vm.isDisabled("second", s)
                                          ? -1
                                          : _vm.tabindex,
                                        "data-key": s,
                                        disabled: _vm.isDisabled("second", s)
                                      },
                                      domProps: { textContent: _vm._s(s) },
                                      on: {
                                        click: function($event) {
                                          return _vm.select("second", s)
                                        },
                                        keydown: [
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "space",
                                                32,
                                                $event.key,
                                                [" ", "Spacebar"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.select("second", s)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "enter",
                                                13,
                                                $event.key,
                                                "Enter"
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.select("second", s)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "up",
                                                38,
                                                $event.key,
                                                ["Up", "ArrowUp"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.prevItem("seconds", s)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "down",
                                                40,
                                                $event.key,
                                                ["Down", "ArrowDown"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.nextItem("seconds", s)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "left",
                                                37,
                                                $event.key,
                                                ["Left", "ArrowLeft"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              "button" in $event &&
                                              $event.button !== 0
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.toLeftColumn("seconds")
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "right",
                                                39,
                                                $event.key,
                                                ["Right", "ArrowRight"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              "button" in $event &&
                                              $event.button !== 2
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.toRightColumn("seconds")
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "esc",
                                                27,
                                                $event.key,
                                                ["Esc", "Escape"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              $event.ctrlKey ||
                                              $event.shiftKey ||
                                              $event.altKey ||
                                              $event.metaKey
                                            ) {
                                              return null
                                            }
                                            return _vm.debounceBlur($event)
                                          }
                                        ],
                                        blur: _vm.debounceBlur,
                                        focus: _vm.keepFocusing
                                      }
                                    })
                                  : _vm._e()
                              ]
                            })
                          ],
                          2
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.apmType
                      ? _c(
                          "ul",
                          {
                            staticClass: "apms",
                            attrs: { tabindex: "-1" },
                            on: { scroll: _vm.keepFocusing }
                          },
                          [
                            _c("li", {
                              staticClass: "hint",
                              attrs: { tabindex: "-1" },
                              domProps: {
                                textContent: _vm._s(_vm.apmLabelText)
                              }
                            }),
                            _vm._v(" "),
                            _vm._l(_vm.apms, function(a, aIndex) {
                              return [
                                !_vm.opts.hideDisabledHours ||
                                (_vm.opts.hideDisabledHours &&
                                  !_vm.isDisabled("apm", a))
                                  ? _c("li", {
                                      key: aIndex,
                                      class: { active: _vm.apm === a },
                                      attrs: {
                                        tabindex: _vm.isDisabled("apm", a)
                                          ? -1
                                          : _vm.tabindex,
                                        "data-key": a,
                                        disabled: _vm.isDisabled("apm", a)
                                      },
                                      domProps: {
                                        textContent: _vm._s(
                                          _vm.apmDisplayText(a)
                                        )
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.select("apm", a)
                                        },
                                        keydown: [
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "space",
                                                32,
                                                $event.key,
                                                [" ", "Spacebar"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.select("apm", a)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "enter",
                                                13,
                                                $event.key,
                                                "Enter"
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.select("apm", a)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "up",
                                                38,
                                                $event.key,
                                                ["Up", "ArrowUp"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.prevItem("apms", a)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "down",
                                                40,
                                                $event.key,
                                                ["Down", "ArrowDown"]
                                              )
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.nextItem("apms", a)
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "left",
                                                37,
                                                $event.key,
                                                ["Left", "ArrowLeft"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              "button" in $event &&
                                              $event.button !== 0
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.toLeftColumn("apms")
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "right",
                                                39,
                                                $event.key,
                                                ["Right", "ArrowRight"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              "button" in $event &&
                                              $event.button !== 2
                                            ) {
                                              return null
                                            }
                                            $event.preventDefault()
                                            return _vm.toRightColumn("apms")
                                          },
                                          function($event) {
                                            if (
                                              !$event.type.indexOf("key") &&
                                              _vm._k(
                                                $event.keyCode,
                                                "esc",
                                                27,
                                                $event.key,
                                                ["Esc", "Escape"]
                                              )
                                            ) {
                                              return null
                                            }
                                            if (
                                              $event.ctrlKey ||
                                              $event.shiftKey ||
                                              $event.altKey ||
                                              $event.metaKey
                                            ) {
                                              return null
                                            }
                                            return _vm.debounceBlur($event)
                                          }
                                        ],
                                        blur: _vm.debounceBlur,
                                        focus: _vm.keepFocusing
                                      }
                                    })
                                  : _vm._e()
                              ]
                            })
                          ],
                          2
                        )
                      : _vm._e()
                  ]
                : _vm._e()
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c4588f32", module.exports)
  }
}

/***/ }),

/***/ 989:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(990)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(992)
/* template */
var __vue_template__ = __webpack_require__(993)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-42b8bad0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/cloudinaryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42b8bad0", Component.options)
  } else {
    hotAPI.reload("data-v-42b8bad0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 990:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(991);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("26a44260", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42b8bad0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./cloudinaryComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42b8bad0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./cloudinaryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 991:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.progress[data-v-42b8bad0] {\n  height: 20px;\n}\n.logo[data-v-42b8bad0] {\n  width: 50%;\n  height: 120px;\n  padding: 5px;\n}\n.images[data-v-42b8bad0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 15px;\n}\n.oldimg[data-v-42b8bad0] {\n  width: 50%;\n  height: 120px;\n  padding: 5px;\n}\n.oldimg img[data-v-42b8bad0],\n.logo img[data-v-42b8bad0] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n", ""]);

// exports


/***/ }),

/***/ 992:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "CloudinaryUpload",
  props: ["oldimage"],
  data: function data() {
    return {
      filesSelectedLength: 0,
      file: [],
      filetype: "",
      uploadedFile: this.oldimage,
      uploadedFileUrl: "",
      cloudinary: {
        uploadPreset: "knkccgjv",
        apiKey: "634813511968181",
        cloudName: "bizguruh-com"
      },
      progress: 0,
      start: false
    };
  },


  computed: {},
  methods: {
    handleFileChange: function handleFileChange(event) {
      this.file = event.target.files[0];

      this.filesSelectedLength = event.target.files.length;

      this.loadFile();
    },
    loadFile: function loadFile() {
      var _this = this;

      var reader = new FileReader();
      reader.onload = function (event) {
        _this.uploadedFile = event.target.result;
      };
      reader.readAsDataURL(this.file);
    },
    processUpload: function processUpload() {
      var _this2 = this;

      var that = this;
      this.start = true;
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      var cloudName = this.cloudinary.cloudName;
      var upload_preset = this.cloudinary.uploadPreset;
      formData.append("file", this.file);
      formData.append("resource_type", "auto");
      formData.append("upload_preset", upload_preset); // REQUIRED
      xhr.open("POST", "https://api.cloudinary.com/v1_1/" + cloudName + "/upload");
      xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
          that.progress = Math.round(e.loaded / e.total * 100) + "%";
        }
      };
      xhr.upload.onloadstart = function (e) {
        this.progress = "Starting...";
      };
      xhr.upload.onloadend = function (e) {
        this.progress = "Completing..";
      };
      xhr.onload = function (progressEvent) {
        if (xhr.status === 200) {
          // Success! You probably want to save the URL somewhere
          _this2.progress = "Completed";
          setTimeout(function () {
            _this2.start = false;
          }, 1000);
          var response = JSON.parse(xhr.response);
          _this2.uploadedFileUrl = response.secure_url; // https address of uploaded file
          _this2.$emit("getUpload", _this2.uploadedFileUrl);
        } else {
          _this2.start = false;
          alert("Upload failed. Please try again.");
        }
      };
      xhr.send(formData);
    }
  }
});

/***/ }),

/***/ 993:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "form",
      {
        staticClass: "card",
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.processUpload($event)
          }
        }
      },
      [
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              staticClass: "form-control",
              attrs: {
                type: "file",
                id: "file-input",
                "aria-describedby": "helpId",
                placeholder: ""
              },
              on: {
                change: function($event) {
                  return _vm.handleFileChange($event)
                }
              }
            }),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "elevated_btn elevated_btn_sm text-main",
                attrs: { type: "submit", disabled: _vm.filesSelectedLength < 1 }
              },
              [_vm._v("Upload")]
            ),
            _vm._v(" "),
            _vm.start
              ? _c("div", { staticClass: "progress mt-2" }, [
                  _c(
                    "div",
                    {
                      staticClass: "progress-bar progress-bar-striped active",
                      style: { width: _vm.progress },
                      attrs: {
                        role: "progressbar",
                        "aria-valuenow": "0",
                        "aria-valuemin": "0",
                        "aria-valuemax": "100"
                      }
                    },
                    [_vm._v(_vm._s(_vm.progress))]
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "images" }, [
              _c("div", { staticClass: "oldimg" }, [
                _vm.filesSelectedLength >= 1
                  ? _c("small", [_vm._v("Current image")])
                  : _vm._e(),
                _vm._v(" "),
                _vm.oldimage !== ""
                  ? _c("img", { attrs: { src: _vm.oldimage, alt: "image" } })
                  : _vm._e()
              ]),
              _vm._v(" "),
              _vm.filesSelectedLength >= 1
                ? _c("div", { staticClass: "logo" }, [
                    _c("small", [_vm._v("New Image")]),
                    _vm._v(" "),
                    _c("img", {
                      attrs: { src: _vm.uploadedFile, alt: "image" }
                    })
                  ])
                : _vm._e()
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42b8bad0", module.exports)
  }
}

/***/ })

});