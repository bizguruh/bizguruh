webpackJsonp([64],{

/***/ 1965:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1966);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3695fc5a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a6eb31ba\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./formQuestion.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a6eb31ba\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./formQuestion.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1966:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nli[data-v-a6eb31ba] {\n  font-size: 14px;\n}\n.prev-body[data-v-a6eb31ba] {\n  background: #fff;\n  padding: 25px;\n  height: 90%;\n  overflow: scroll;\n  width: 90%;\n}\n.note[data-v-a6eb31ba] {\n  color: blue;\n  line-height: 1.4;\n}\n.prev[data-v-a6eb31ba] {\n  position: absolute;\n  background: rgba(255, 255, 255, 0.8);\n  width: 100%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px;\n  z-index: 1;\n}\n.butt[data-v-a6eb31ba],\n.buttt[data-v-a6eb31ba] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.main_page[data-v-a6eb31ba] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n  position: relative;\n  background: #f7f8fa;\n  padding: 30px;\n}\n.page[data-v-a6eb31ba] {\n  background: #fff;\n  width: 80%;\n  height: 100vh;\n  overflow: scroll;\n  margin: 0 auto;\n  padding: 30px 40px;\n}\n.navi[data-v-a6eb31ba] {\n  padding: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  position: relative;\n}\n.nav-items[data-v-a6eb31ba] {\n  position: relative;\n  text-transform: capitalize;\n  font-size: 12px;\n}\n\n/* .nav-items::after {\n  content: \"\";\n  border-top: 2px solid #ccc;\n  position: absolute;\n  width: 200px;\n  height: 10px;\n  bottom: 0;\n}\n.nav-items:last-child::after {\n  content: \"\";\n  border-top: none;\n} */\n.number[data-v-a6eb31ba] {\n  font-size: 14px;\n}\n.form-control[data-v-a6eb31ba]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-a6eb31ba]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-a6eb31ba]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-a6eb31ba]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-a6eb31ba]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-a6eb31ba]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\ntable[data-v-a6eb31ba] {\n  font-size: 14px;\n}\nh4[data-v-a6eb31ba] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\nh5[data-v-a6eb31ba] {\n  font-size: 14px;\n  margin-bottom: 10px;\n}\n.section_title[data-v-a6eb31ba] {\n  text-transform: uppercase;\n  font-size: 15.5px;\n}\n.section_desc[data-v-a6eb31ba] {\n  font-size: 15.5px;\n}\np[data-v-a6eb31ba] {\n  margin: 0 0 10px;\n  font-size: 15px;\n}\n.mini-label[data-v-a6eb31ba] {\n  font-size: 13px;\n}\nlabel span[data-v-a6eb31ba] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-a6eb31ba] {\n  display: fleX;\n}\n.custom-control-input[data-v-a6eb31ba] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-a6eb31ba] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-a6eb31ba],\ntd[data-v-a6eb31ba] {\n  text-align: center;\n}\ntable[data-v-a6eb31ba] {\n  font-size: 14px;\n}\n@media (max-width: 425px) {\n.page[data-v-a6eb31ba] {\n    width: 100%;\n    padding: 20px 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewForm__ = __webpack_require__(994);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewForm___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewForm__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      section_num: 0,
      user: {},
      template: {},
      record: {},
      preview: false
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        vm.auth = true;
      } else {
        vm.$toasted.error("Log in to access");
        next("/");
      }
    });
  },

  components: {
    Preview: __WEBPACK_IMPORTED_MODULE_0__previewForm___default.a
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    this.user = user;
    this.getQuestion();
  },

  methods: {
    closePreview: function closePreview() {
      this.preview = false;
    },
    openPreview: function openPreview() {
      this.preview = true;
    },
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.template,
        month: this.record.current_month,
        week: this.record.current_week,
        template: this.template.form.legend
      };
      axios.post("/api/save-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.template,
        month: this.record.current_month,
        week: this.record.current_week,
        template: this.template.form.legend
      };
      axios.post("/api/get-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            _this3.template = JSON.parse(response.data.responses);
          }
        }
      });
    },
    continueTo: function continueTo() {
      if (this.section_num < this.template.form.sections.length - 1) {
        this.section_num++;
      }
    },
    previous: function previous() {
      if (this.section_num > 0) {
        this.section_num--;
      }
    },
    submitAnswer: function submitAnswer() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.template,
        month: this.record.current_month,
        week: this.record.current_week,
        template: this.template.form.legend
      };
      axios.post("/api/save-responses", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");
          _this4.preview = true;
          _this4.$router.push("/entrepreneur/bap");
        }
      });
    },
    changeSection: function changeSection(id) {
      this.section_num = id;
    },
    getQuestion: function getQuestion() {
      var _this5 = this;

      var id = this.$route.params.id;
      axios.get("/api/get-form-template/" + id, {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (res) {
        if (res.status = 200) {
          _this5.template = JSON.parse(res.data.template);
          if (_this5.template) {
            _this5.getRecord();
            _this5.getDraft();
          }
        }
      }).catch();
    }
  }
});

/***/ }),

/***/ 1968:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main_page" }, [
    _vm.preview
      ? _c("div", { staticClass: "prev" }, [
          _c(
            "div",
            { staticClass: "prev-body" },
            [
              _c("Preview", {
                attrs: { template: _vm.template },
                on: { close: _vm.closePreview }
              }),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("div", { staticClass: "butt mt-3" }, [
                _c(
                  "button",
                  {
                    staticClass: "elevated_btn elevated_btn_sm text-main mr-3",
                    attrs: { type: "button" },
                    on: { click: _vm.closePreview }
                  },
                  [_vm._v("close")]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass:
                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                    attrs: { type: "button" },
                    on: { click: _vm.submitAnswer }
                  },
                  [_vm._v("Submit")]
                )
              ])
            ],
            1
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.template.form && !_vm.preview
      ? _c(
          "div",
          { staticClass: "page" },
          [
            _c(
              "router-link",
              { staticClass: "back mb-5", attrs: { to: "/entrepreneur/bap" } },
              [
                _c("i", {
                  staticClass: "fa fa-long-arrow-left",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Back\n    ")
              ]
            ),
            _vm._v(" "),
            _c("h3", { staticClass: "text-center mb-3" }, [
              _vm._v(_vm._s(_vm.template.form.legend))
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "navi mb-3" },
              _vm._l(_vm.template.form.sections, function(section, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass: "nav-items",
                    on: {
                      click: function($event) {
                        return _vm.changeSection(index)
                      }
                    }
                  },
                  [_vm._v(_vm._s(section.section.label))]
                )
              }),
              0
            ),
            _vm._v(" "),
            _vm._l(_vm.template.form.sections, function(section, index) {
              return index == _vm.section_num
                ? _c("section", { key: index, staticClass: "mb-3" }, [
                    _c("h4", { staticClass: "mb-3 section_title" }, [
                      _vm._v(_vm._s(section.section.label))
                    ]),
                    _vm._v(" "),
                    _c("p", {
                      staticClass: "section_desc",
                      domProps: {
                        innerHTML: _vm._s(section.section.description)
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "ol",
                      _vm._l(section.section.rows, function(rows, idx) {
                        return _c(
                          "div",
                          { key: idx },
                          _vm._l(rows.row, function(row, id) {
                            return _c(
                              "div",
                              { key: id, staticClass: "mb-3 rows" },
                              [
                                _c("p", [_vm._v(_vm._s(row.description))]),
                                _vm._v(" "),
                                _c("li", [
                                  _c("div", [
                                    row.type == "radio"
                                      ? _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h5", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "small",
                                              { staticClass: "note form-text" },
                                              [_vm._v(_vm._s(row.note))]
                                            ),
                                            _vm._v(" "),
                                            _vm._l(row.values, function(
                                              value,
                                              id
                                            ) {
                                              return _c(
                                                "label",
                                                {
                                                  key: id,
                                                  staticClass:
                                                    "custom-control custom-radio"
                                                },
                                                [
                                                  _c("input", {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value: row.answer,
                                                        expression: "row.answer"
                                                      }
                                                    ],
                                                    staticClass:
                                                      "custom-control-input",
                                                    attrs: { type: "radio" },
                                                    domProps: {
                                                      value: value,
                                                      checked: _vm._q(
                                                        row.answer,
                                                        value
                                                      )
                                                    },
                                                    on: {
                                                      change: function($event) {
                                                        return _vm.$set(
                                                          row,
                                                          "answer",
                                                          value
                                                        )
                                                      }
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass:
                                                        "custom-control-indicator"
                                                    },
                                                    [_vm._v(_vm._s(value))]
                                                  )
                                                ]
                                              )
                                            })
                                          ],
                                          2
                                        )
                                      : row.type == "select"
                                      ? _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h5", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "small",
                                              { staticClass: "note form-text" },
                                              [_vm._v(_vm._s(row.note))]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "select",
                                              {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: row.answer,
                                                    expression: "row.answer"
                                                  }
                                                ],
                                                staticClass: "custom-select",
                                                on: {
                                                  change: function($event) {
                                                    var $$selectedVal = Array.prototype.filter
                                                      .call(
                                                        $event.target.options,
                                                        function(o) {
                                                          return o.selected
                                                        }
                                                      )
                                                      .map(function(o) {
                                                        var val =
                                                          "_value" in o
                                                            ? o._value
                                                            : o.value
                                                        return val
                                                      })
                                                    _vm.$set(
                                                      row,
                                                      "answer",
                                                      $event.target.multiple
                                                        ? $$selectedVal
                                                        : $$selectedVal[0]
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: {
                                                      disabled: "",
                                                      selected: ""
                                                    }
                                                  },
                                                  [_vm._v("Select one")]
                                                ),
                                                _vm._v(" "),
                                                _vm._l(row.values, function(
                                                  value,
                                                  id
                                                ) {
                                                  return _c(
                                                    "option",
                                                    { key: id },
                                                    [_vm._v(_vm._s(value))]
                                                  )
                                                })
                                              ],
                                              2
                                            )
                                          ]
                                        )
                                      : row.type == "checkbox"
                                      ? _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h5", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "small",
                                              { staticClass: "note form-text" },
                                              [_vm._v(_vm._s(row.note))]
                                            ),
                                            _vm._v(" "),
                                            _vm._l(row.values, function(
                                              value,
                                              id
                                            ) {
                                              return _c(
                                                "label",
                                                {
                                                  key: id,
                                                  staticClass:
                                                    "custom-control custom-checkbox"
                                                },
                                                [
                                                  _c("input", {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value: row.answers,
                                                        expression:
                                                          "row.answers"
                                                      }
                                                    ],
                                                    staticClass:
                                                      "custom-control-input",
                                                    attrs: {
                                                      type: "checkbox",
                                                      multiple: row.multiple
                                                    },
                                                    domProps: {
                                                      value: value,
                                                      checked: Array.isArray(
                                                        row.answers
                                                      )
                                                        ? _vm._i(
                                                            row.answers,
                                                            value
                                                          ) > -1
                                                        : row.answers
                                                    },
                                                    on: {
                                                      change: function($event) {
                                                        var $$a = row.answers,
                                                          $$el = $event.target,
                                                          $$c = $$el.checked
                                                            ? true
                                                            : false
                                                        if (
                                                          Array.isArray($$a)
                                                        ) {
                                                          var $$v = value,
                                                            $$i = _vm._i(
                                                              $$a,
                                                              $$v
                                                            )
                                                          if ($$el.checked) {
                                                            $$i < 0 &&
                                                              _vm.$set(
                                                                row,
                                                                "answers",
                                                                $$a.concat([
                                                                  $$v
                                                                ])
                                                              )
                                                          } else {
                                                            $$i > -1 &&
                                                              _vm.$set(
                                                                row,
                                                                "answers",
                                                                $$a
                                                                  .slice(0, $$i)
                                                                  .concat(
                                                                    $$a.slice(
                                                                      $$i + 1
                                                                    )
                                                                  )
                                                              )
                                                          }
                                                        } else {
                                                          _vm.$set(
                                                            row,
                                                            "answers",
                                                            $$c
                                                          )
                                                        }
                                                      }
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass:
                                                        "custom-control-indicator"
                                                    },
                                                    [_vm._v(_vm._s(value))]
                                                  )
                                                ]
                                              )
                                            })
                                          ],
                                          2
                                        )
                                      : row.type == "table"
                                      ? _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h5", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "small",
                                              { staticClass: "note form-text" },
                                              [_vm._v(_vm._s(row.note))]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass: "table-responsive"
                                              },
                                              [
                                                row.table.headers.length
                                                  ? _c(
                                                      "table",
                                                      {
                                                        staticClass:
                                                          "table table-bordered"
                                                      },
                                                      [
                                                        _c("thead", [
                                                          _c(
                                                            "tr",
                                                            [
                                                              _c("th"),
                                                              _vm._v(" "),
                                                              _vm._l(
                                                                row.table
                                                                  .headers,
                                                                function(
                                                                  column,
                                                                  index
                                                                ) {
                                                                  return _c(
                                                                    "th",
                                                                    {
                                                                      key: index
                                                                    },
                                                                    [
                                                                      _vm._v(
                                                                        _vm._s(
                                                                          column
                                                                        )
                                                                      )
                                                                    ]
                                                                  )
                                                                }
                                                              )
                                                            ],
                                                            2
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "tbody",
                                                          _vm._l(
                                                            row.table.items,
                                                            function(
                                                              item,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "tr",
                                                                { key: index },
                                                                [
                                                                  _c("td", [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        row
                                                                          .table
                                                                          .row_titles[
                                                                          index
                                                                        ]
                                                                      )
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _vm._l(
                                                                    row.table
                                                                      .headers,
                                                                    function(
                                                                      column,
                                                                      indexColumn
                                                                    ) {
                                                                      return _c(
                                                                        "td",
                                                                        {
                                                                          key: indexColumn,
                                                                          staticClass:
                                                                            "p-0"
                                                                        },
                                                                        [
                                                                          _c(
                                                                            "input",
                                                                            {
                                                                              directives: [
                                                                                {
                                                                                  name:
                                                                                    "model",
                                                                                  rawName:
                                                                                    "v-model",
                                                                                  value:
                                                                                    item[
                                                                                      column +
                                                                                        index +
                                                                                        indexColumn
                                                                                    ],
                                                                                  expression:
                                                                                    "item[column + index + indexColumn]"
                                                                                }
                                                                              ],
                                                                              staticClass:
                                                                                "form-control border-0",
                                                                              attrs: {
                                                                                type:
                                                                                  "text",
                                                                                placeholder:
                                                                                  "type here"
                                                                              },
                                                                              domProps: {
                                                                                value:
                                                                                  item[
                                                                                    column +
                                                                                      index +
                                                                                      indexColumn
                                                                                  ]
                                                                              },
                                                                              on: {
                                                                                input: function(
                                                                                  $event
                                                                                ) {
                                                                                  if (
                                                                                    $event
                                                                                      .target
                                                                                      .composing
                                                                                  ) {
                                                                                    return
                                                                                  }
                                                                                  _vm.$set(
                                                                                    item,
                                                                                    column +
                                                                                      index +
                                                                                      indexColumn,
                                                                                    $event
                                                                                      .target
                                                                                      .value
                                                                                  )
                                                                                }
                                                                              }
                                                                            }
                                                                          )
                                                                        ]
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                2
                                                              )
                                                            }
                                                          ),
                                                          0
                                                        )
                                                      ]
                                                    )
                                                  : _vm._e()
                                              ]
                                            )
                                          ]
                                        )
                                      : row.type == "table with textarea"
                                      ? _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h4", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c("small", [
                                              _vm._v(_vm._s(row.note))
                                            ]),
                                            _vm._v(" "),
                                            row.table.headers.length
                                              ? _c(
                                                  "table",
                                                  {
                                                    staticClass:
                                                      "table table-bordered"
                                                  },
                                                  [
                                                    _c("thead", [
                                                      _c(
                                                        "tr",
                                                        [
                                                          _c("th"),
                                                          _vm._v(" "),
                                                          _vm._l(
                                                            row.table.headers,
                                                            function(
                                                              column,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "th",
                                                                { key: index },
                                                                [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      column
                                                                    )
                                                                  )
                                                                ]
                                                              )
                                                            }
                                                          )
                                                        ],
                                                        2
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _c(
                                                      "tbody",
                                                      _vm._l(
                                                        row.table.items,
                                                        function(item, index) {
                                                          return _c(
                                                            "tr",
                                                            { key: index },
                                                            [
                                                              _c("td", [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    row.table
                                                                      .row_titles[
                                                                      index
                                                                    ]
                                                                  )
                                                                )
                                                              ]),
                                                              _vm._v(" "),
                                                              _vm._l(
                                                                row.table
                                                                  .headers,
                                                                function(
                                                                  column,
                                                                  indexColumn
                                                                ) {
                                                                  return _c(
                                                                    "td",
                                                                    {
                                                                      key: indexColumn,
                                                                      staticClass:
                                                                        "p-0"
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "textarea",
                                                                        {
                                                                          directives: [
                                                                            {
                                                                              name:
                                                                                "model",
                                                                              rawName:
                                                                                "v-model",
                                                                              value:
                                                                                item[
                                                                                  column +
                                                                                    index +
                                                                                    indexColumn
                                                                                ],
                                                                              expression:
                                                                                "item[column + index + indexColumn]"
                                                                            }
                                                                          ],
                                                                          staticClass:
                                                                            "form-control",
                                                                          attrs: {
                                                                            rows:
                                                                              "5",
                                                                            placeholder:
                                                                              row.placeholder
                                                                          },
                                                                          domProps: {
                                                                            value:
                                                                              item[
                                                                                column +
                                                                                  index +
                                                                                  indexColumn
                                                                              ]
                                                                          },
                                                                          on: {
                                                                            input: function(
                                                                              $event
                                                                            ) {
                                                                              if (
                                                                                $event
                                                                                  .target
                                                                                  .composing
                                                                              ) {
                                                                                return
                                                                              }
                                                                              _vm.$set(
                                                                                item,
                                                                                column +
                                                                                  index +
                                                                                  indexColumn,
                                                                                $event
                                                                                  .target
                                                                                  .value
                                                                              )
                                                                            }
                                                                          }
                                                                        }
                                                                      )
                                                                    ]
                                                                  )
                                                                }
                                                              )
                                                            ],
                                                            2
                                                          )
                                                        }
                                                      ),
                                                      0
                                                    )
                                                  ]
                                                )
                                              : _vm._e()
                                          ]
                                        )
                                      : _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c("h5", [
                                              _vm._v(_vm._s(row.label))
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "small",
                                              { staticClass: "note form-text" },
                                              [_vm._v(_vm._s(row.note))]
                                            ),
                                            _vm._v(" "),
                                            row.type === "checkbox"
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.answer,
                                                      expression: "row.answer"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    placeholder:
                                                      row.placeholder,
                                                    required: row.required,
                                                    type: "checkbox"
                                                  },
                                                  domProps: {
                                                    checked: Array.isArray(
                                                      row.answer
                                                    )
                                                      ? _vm._i(
                                                          row.answer,
                                                          null
                                                        ) > -1
                                                      : row.answer
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      var $$a = row.answer,
                                                        $$el = $event.target,
                                                        $$c = $$el.checked
                                                          ? true
                                                          : false
                                                      if (Array.isArray($$a)) {
                                                        var $$v = null,
                                                          $$i = _vm._i($$a, $$v)
                                                        if ($$el.checked) {
                                                          $$i < 0 &&
                                                            _vm.$set(
                                                              row,
                                                              "answer",
                                                              $$a.concat([$$v])
                                                            )
                                                        } else {
                                                          $$i > -1 &&
                                                            _vm.$set(
                                                              row,
                                                              "answer",
                                                              $$a
                                                                .slice(0, $$i)
                                                                .concat(
                                                                  $$a.slice(
                                                                    $$i + 1
                                                                  )
                                                                )
                                                            )
                                                        }
                                                      } else {
                                                        _vm.$set(
                                                          row,
                                                          "answer",
                                                          $$c
                                                        )
                                                      }
                                                    }
                                                  }
                                                })
                                              : row.type === "radio"
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.answer,
                                                      expression: "row.answer"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    placeholder:
                                                      row.placeholder,
                                                    required: row.required,
                                                    type: "radio"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      row.answer,
                                                      null
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        row,
                                                        "answer",
                                                        null
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.answer,
                                                      expression: "row.answer"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    placeholder:
                                                      row.placeholder,
                                                    required: row.required,
                                                    type: row.type
                                                  },
                                                  domProps: {
                                                    value: row.answer
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        row,
                                                        "answer",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                          ]
                                        )
                                  ])
                                ])
                              ]
                            )
                          }),
                          0
                        )
                      }),
                      0
                    )
                  ])
                : _vm._e()
            }),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("div", { staticClass: "w-100 butt mt-4" }, [
              _c(
                "button",
                {
                  staticClass: "elevated_btn elevated_btn_sm text-main",
                  attrs: { type: "button" },
                  on: { click: _vm.saveDraft }
                },
                [_vm._v("Save to draft")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "buttt" }, [
                _vm.section_num > 0
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-main mr-3",
                        attrs: { type: "button" },
                        on: { click: _vm.previous }
                      },
                      [_vm._v("Previous")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.section_num !== _vm.template.form.sections.length - 1
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-white btn-compliment",
                        attrs: { type: "button" },
                        on: { click: _vm.continueTo }
                      },
                      [_vm._v("Continue")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.section_num == _vm.template.form.sections.length - 1
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-white btn-compliment",
                        attrs: { type: "button" },
                        on: { click: _vm.openPreview }
                      },
                      [_vm._v("Preview")]
                    )
                  : _vm._e()
              ])
            ])
          ],
          2
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a6eb31ba", module.exports)
  }
}

/***/ }),

/***/ 660:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1965)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1967)
/* template */
var __vue_template__ = __webpack_require__(1968)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a6eb31ba"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/formQuestion.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a6eb31ba", Component.options)
  } else {
    hotAPI.reload("data-v-a6eb31ba", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 994:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(995)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(997)
/* template */
var __vue_template__ = __webpack_require__(998)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2fcaa063"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/previewForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2fcaa063", Component.options)
  } else {
    hotAPI.reload("data-v-2fcaa063", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 995:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(996);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("72991d8e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2fcaa063\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2fcaa063\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 996:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.number[data-v-2fcaa063] {\n  font-size: 14px;\n}\n.form-text[data-v-2fcaa063]{\n  color: red;\n}\n.main-preview[data-v-2fcaa063] {\n  width: 80%;\n  margin: 0 auto;\n}\n.form-control[data-v-2fcaa063]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\ntable[data-v-2fcaa063] {\n  font-size: 14px;\n}\nh4[data-v-2fcaa063] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\nh5[data-v-2fcaa063] {\n  font-size: 14px;\n  margin-bottom: 10px;\n}\np[data-v-2fcaa063] {\n  margin: 0 0 10px;\n   font-size: 15px;\n}\n.mini-label[data-v-2fcaa063] {\n  font-size: 13px;\n}\nlabel span[data-v-2fcaa063] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-2fcaa063] {\n  display: fleX;\n}\n.custom-control-input[data-v-2fcaa063] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-2fcaa063] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-2fcaa063],\ntd[data-v-2fcaa063] {\n  text-align: center;\n}\ntable[data-v-2fcaa063] {\n  font-size: 14px;\n}\nli[data-v-2fcaa063] {\n  font-size: 14px;\n}\n@media (max-width: 425px) {\n.main-preview[data-v-2fcaa063] {\n    width: 100%;\n    margin: 0 auto;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 997:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["template"],
  methods: {
    close: function close() {
      this.$emit('close');
    }
  }
});

/***/ }),

/***/ 998:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "p-4 bg-white main-preview" },
    [
      _c("h3", { staticClass: "text-center mb-3" }, [
        _vm._v(_vm._s(_vm.template.form.legend))
      ]),
      _vm._v(" "),
      _c("p", [_vm._v(_vm._s(_vm.template.form.description))]),
      _vm._v(" "),
      _vm._l(_vm.template.form.sections, function(section, index) {
        return _c("section", { key: index, staticClass: "mb-3" }, [
          _c("div", { staticClass: "number" }, [
            _vm._v("Section " + _vm._s(index + 1))
          ]),
          _vm._v(" "),
          _c("h4", { staticClass: "mb-2" }, [
            _vm._v(_vm._s(section.section.label))
          ]),
          _vm._v(" "),
          _c("p", {
            staticClass: "section_desc",
            domProps: { innerHTML: _vm._s(section.section.description) }
          }),
          _vm._v(" "),
          _c(
            "ol",
            _vm._l(section.section.rows, function(rows, idx) {
              return _c(
                "div",
                { key: idx },
                _vm._l(rows.row, function(row, id) {
                  return _c("div", { key: id, staticClass: "mb-3 rows" }, [
                    _c("p", [_vm._v(_vm._s(row.description))]),
                    _vm._v(" "),
                    _c("li", [
                      _c("div", [
                        row.type == "radio"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass: "custom-control custom-radio"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answer,
                                            expression: "row.answer"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: { readonly: "", type: "radio" },
                                        domProps: {
                                          value: value,
                                          checked: _vm._q(row.answer, value)
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              row,
                                              "answer",
                                              value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "select"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: row.answer,
                                      expression: "row.answer"
                                    }
                                  ],
                                  staticClass: "custom-select",
                                  attrs: { readonly: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        row,
                                        "answer",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    { attrs: { disabled: "", selected: "" } },
                                    [_vm._v("Select one")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(row.values, function(value, id) {
                                    return _c("option", { key: id }, [
                                      _vm._v(_vm._s(value))
                                    ])
                                  })
                                ],
                                2
                              )
                            ])
                          : row.type == "checkbox"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass:
                                        "custom-control custom-checkbox"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answers,
                                            expression: "row.answers"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: {
                                          readonly: "",
                                          type: "checkbox",
                                          multiple: row.multiple
                                        },
                                        domProps: {
                                          value: value,
                                          checked: Array.isArray(row.answers)
                                            ? _vm._i(row.answers, value) > -1
                                            : row.answers
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = row.answers,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = value,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(row, "answers", $$c)
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "table"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.table.headers.length
                                ? _c(
                                    "table",
                                    { staticClass: "table table-bordered" },
                                    [
                                      _c("thead", [
                                        _c(
                                          "tr",
                                          [
                                            _c("th"),
                                            _vm._v(" "),
                                            _vm._l(row.table.headers, function(
                                              column,
                                              index
                                            ) {
                                              return _c("th", { key: index }, [
                                                _vm._v(_vm._s(column))
                                              ])
                                            })
                                          ],
                                          2
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        _vm._l(row.table.items, function(
                                          item,
                                          index
                                        ) {
                                          return _c(
                                            "tr",
                                            { key: index },
                                            [
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    row.table.row_titles[index]
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _vm._l(
                                                row.table.headers,
                                                function(column, indexColumn) {
                                                  return _c(
                                                    "td",
                                                    {
                                                      key: indexColumn,
                                                      staticClass: "p-0"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              item[
                                                                column +
                                                                  index +
                                                                  indexColumn
                                                              ],
                                                            expression:
                                                              "item[column + index + indexColumn]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control border-0",
                                                        attrs: {
                                                          readonly: "",
                                                          type: "text",
                                                          placeholder:
                                                            "type here"
                                                        },
                                                        domProps: {
                                                          value:
                                                            item[
                                                              column +
                                                                index +
                                                                indexColumn
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              item,
                                                              column +
                                                                index +
                                                                indexColumn,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                }
                                              )
                                            ],
                                            2
                                          )
                                        }),
                                        0
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : (row.type = "text")
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.type === "checkbox"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "checkbox"
                                    },
                                    domProps: {
                                      checked: Array.isArray(row.answer)
                                        ? _vm._i(row.answer, null) > -1
                                        : row.answer
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = row.answer,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(row, "answer", $$c)
                                        }
                                      }
                                    }
                                  })
                                : row.type === "radio"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "radio"
                                    },
                                    domProps: {
                                      checked: _vm._q(row.answer, null)
                                    },
                                    on: {
                                      change: function($event) {
                                        return _vm.$set(row, "answer", null)
                                      }
                                    }
                                  })
                                : _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: row.type
                                    },
                                    domProps: { value: row.answer },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          row,
                                          "answer",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                            ])
                          : _c("div")
                      ])
                    ])
                  ])
                }),
                0
              )
            }),
            0
          )
        ])
      }),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "butt mt-3 text-center w-100" }, [
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm text-white btn-compliment mx-auto",
            attrs: { type: "button" },
            on: { click: _vm.close }
          },
          [_vm._v("Close")]
        )
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2fcaa063", module.exports)
  }
}

/***/ })

});