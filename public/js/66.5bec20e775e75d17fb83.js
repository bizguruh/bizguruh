webpackJsonp([66],{

/***/ 1490:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1491);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("135d87d8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-075d64f3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userJournalComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-075d64f3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userJournalComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1491:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.rowBanner[data-v-075d64f3]  {\n    padding: 30px 200px;\n    background: #f5f5f5;\n}\n.imgJournal[data-v-075d64f3] {\n    width: 150px;\n}\n.pTitle[data-v-075d64f3] {\n    width: 150px;\n    text-align:center;\n}\n.prodDis[data-v-075d64f3] {\n    float: left;\n    margin: 10px 50px;\n    height: 315px;\n}\n.shipTabSwitch[data-v-075d64f3], .reviewTabSwitch[data-v-075d64f3], .shareTabSwitch[data-v-075d64f3] {\n    background-color: #a3c2dc;\n    border: 1px solid #a3c2dc;\n    color: #ffffff !important;\n    padding: 15px 0;\n    cursor: pointer;\n}\n.priceTabs[data-v-075d64f3] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n}\n.shipTab[data-v-075d64f3], .reviewTab[data-v-075d64f3], .shareTab[data-v-075d64f3] {\n    color: #a4a4a4;\n    border: 1px solid #a4a4a4;\n    padding: 15px 0;\n    cursor: pointer;\n}\n.relatedDiv[data-v-075d64f3] {\n    text-align: center;\n}\n.relatedLeft[data-v-075d64f3] {\n    width: 40%;\n    height: 4px;\n    float: left;\n    background: #c4c4c4;\n}\n.relatedRight[data-v-075d64f3] {\n    width: 40%;\n    height: 4px;\n    float: right;\n    background: #c4c4c4;\n}\n.reviewList[data-v-075d64f3] {\n    margin-bottom: 20px;\n}\n.rateStyle[data-v-075d64f3] {\n    float: left;\n    margin: 2px;\n}\n.userStyle .course-thumbnail img[data-v-075d64f3]\n{\n    -o-object-fit: inherit !important;\n       object-fit: inherit !important;\n    height: 220px !important;\n}\n.entry-title a[data-v-075d64f3] {\n    font-size: 16px;\n}\n.catImage[data-v-075d64f3] {\n    width: 150px;\n}\n.filterTest[data-v-075d64f3] {\n    border-right: 1px solid #bdbfbf;\n    padding-left: 60px;\n}\n.rowCat[data-v-075d64f3] {\n    margin-top: 40px;\n}\n.prodImg[data-v-075d64f3] {\n    text-align: center;\n}\n.prodImg img[data-v-075d64f3] {\n    width: 200px;\n}\n.proReview[data-v-075d64f3] {\n    margin-left: 100px;\n    margin-right: 50px;\n}\n.btn-guruh-cart[data-v-075d64f3] {\n    border: 1px solid #a3c2dc;\n    font-size: 15px;\n    font-weight: bold;\n    color: #a3c2dc;\n    padding: 15px 39%;\n}\n.btn-guruh-online[data-v-075d64f3] {\n    color: #a3c2dc;\n    border: 1px solid #a3c2dc;\n    padding: 20px 42%;\n}\n.addCart[data-v-075d64f3] {\n    margin-bottom: 20px !important;\n}\n#menu4 .sponsorName[data-v-075d64f3] {\n    font-weight: bold;\n    text-transform: uppercase;\n    padding-bottom:4px;\n}\n#menu4 .sponsorDesc[data-v-075d64f3] {\n    padding-top: 0px !important;\n}\n.btn-wishlist[data-v-075d64f3] {\n    background-color: #a3c2dc !important;\n    color: #ffffff;\n    padding: 15px 33%;\n    font-weight: bold;\n}\n.tab-content p[data-v-075d64f3], .shipDetailRow p[data-v-075d64f3] {\n    padding: 20px;\n}\n.abProd[data-v-075d64f3] {\n    background-color: #f2f6fa;\n    padding: 40px;\n    margin: 70px;\n    text-align: center;\n}\ninput[data-v-075d64f3]::-webkit-input-placeholder, textarea[data-v-075d64f3]::-webkit-input-placeholder  {\n    color: #dbdbdb !important;\n    font-weight: bold;\n}\n.detailTabs[data-v-075d64f3]{\n    padding: 0 50px;\n}\n.hardCover[data-v-075d64f3], .softCover[data-v-075d64f3] {\n    border: 2px solid #a3c2dc;\n    padding: 7px;\n}\n.divC[data-v-075d64f3] {\n    font-size: 40px;\n    margin: 17px 0 50px 0;\n}\n.btn-reviews[data-v-075d64f3] {\n    background-color: #a3c2dc !important;\n    padding: 10px 20px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-journal-component",
    components: {
        'app-banner': __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default.a
    },
    data: function data() {
        return {
            rateFa: true,
            review: {
                productId: '',
                title: '',
                description: '',
                rating: 0
            },
            star: 0,
            NotratedOne: true,
            NotratedTwo: true,
            RatedTwo: false,
            RatedOne: false,
            NotratedThree: true,
            RatedThree: false,
            NotratedFour: true,
            RatedFour: false,
            NotratedFive: true,
            RatedFive: false,
            prodd: {},
            shipTab: false,
            reviewTabSwitch: false,
            shareTabSwitch: false,
            reviewTab: true,
            shareTab: true,
            shipTabSwitch: true,
            shipping: true,
            reviews: false,
            share: false,
            allReviews: [],
            relatedProducts: [],
            journals: [],
            epiFade: true,
            epiModal: true,
            journalDetail: {},
            title: '',
            price: '',
            hardrCopy: false,
            hardpriceCont: false,
            softrCopy: false,
            softpriceCont: true,
            readOnline: false,
            readOnlinepriceCont: true,
            email: '',
            user_id: '',
            user: {},
            authenticate: false,
            anonymousCart: [],
            loginCart: [],
            location: '',
            localShippingRates: '',
            localShippingTime: '',
            intlShippingRates: '',
            intlShippingTime: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.email = user.email;
            this.user_id = user.id;
            this.user = user;
            this.authenticate = true;
        }
        axios.get('/api/get/prodtype/Journals').then(function (response) {
            if (response.status === 200) {
                var product = response.data.data;
                product.forEach(function (item) {
                    item.journal.category = item.category.name;
                    item.journal.coverImage = item.coverImage;
                    item.journal.uid = item.id;
                    _this.journals.push(item.journal);
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        showTabs: function showTabs(params) {
            switch (params) {
                case 'shipping':
                    this.shipping = this.shipTabSwitch = this.reviewTab = this.shareTab = true;
                    this.reviews = this.shipTab = this.reviewTabSwitch = this.shareTabSwitch = this.shareTabSwitch = this.share = false;
                    break;
                case 'reviews':
                    this.reviewTabSwitch = this.reviews = this.shipTab = this.shareTab = true;
                    this.shipTabSwitch = this.reviewTab = this.shipping = this.shareTabSwitch = this.share = false;
                    break;
                case 'share':
                    this.shipping = this.reviews = this.reviewTabSwitch = this.shipTab = this.shareTab = this.shipTabSwitch = false;
                    this.share = this.shareTabSwitch = this.reviewTab = this.shipTab = true;
                    break;
                default:
                    this.shipping = false;
                    this.reviews = false;
            }
        },
        openModal: function openModal(id) {
            var _this2 = this;

            this.epiFade = false;
            axios.get('/api/product-detail/' + id).then(function (response) {
                if (response.status === 200) {
                    _this2.journalDetail = response.data.data[0].journal;
                    _this2.journalDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
                    _this2.journalDetail.coverImage = response.data.data[0].coverImage;
                    _this2.journalDetail.softCopyPrice = response.data.data[0].softCopyPrice;
                    _this2.journalDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
                    _this2.journalDetail.uid = response.data.data[0].id;

                    _this2.getProductReviews(_this2.journalDetail.uid);

                    if (_this2.journalDetail.hardCopyPrice !== '' || null) {
                        _this2.title = 'Hard Copy';
                        _this2.price = _this2.journalDetail.hardCopyPrice;
                    } else if (_this2.journalDetail.softCopyPrice !== '' || null) {
                        _this2.title = 'SoftCopy';
                        _this2.price = _this2.softCopyPrice;
                    } else if (_this2.journalDetail.readOnlinePrice !== '' || null) {
                        _this2.title = 'Read Online';
                        _this2.price = _this2.journalDetail.readOnlinePrice;
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        togglePrice: function togglePrice(cat) {
            if (cat === 'hardCopy') {
                this.price = this.journalDetail.hardCopyPrice;
                this.hardpriceCont = this.softrCopy = this.readOnline = false;
                this.hardrCopy = this.softpriceCont = this.readOnlinepriceCont = true;
                this.title = 'Hard Copy';
            } else if (cat === 'softCopy') {
                this.price = this.journalDetail.softCopyPrice;
                this.softpriceCont = this.hardrCopy = this.readOnline = false;
                this.hardpriceCont = this.softrCopy = this.readOnlinepriceCont = true;
                this.title = 'Digital Copy';
            } else if (cat === 'readOnline') {
                this.price = this.journalDetail.readOnlinePrice;
                this.softrCopy = this.hardrCopy = this.readOnlinepriceCont = false;
                this.softpriceCont = this.hardpriceCont = this.readOnline = true;
                this.title = 'Read Online';
            }
        },
        addToCart: function addToCart(id) {
            console.log(id);
            var cart = {
                productId: id,
                price: this.price
            };
            if (this.title === 'Hard Copy') {
                cart.quantity = 1;
                cart.prodType = 'HardCopy';
            } else if (this.title === 'Digital Copy') {
                cart.quantity = 0;
                cart.prodType = 'SoftCopy';
            } else if (this.title === 'Read Online') {
                cart.quantity = 0;
                cart.prodType = 'ReadOnline';
            }

            if (this.authenticate === false) {
                cart.cartNum = 1;
                this.anonymousCart.push(cart);
                if (JSON.parse(localStorage.getItem('userCart')) === null) {
                    localStorage.setItem('userCart', JSON.stringify(this.anonymousCart));
                    var sessionCart = JSON.parse(localStorage.getItem('userCart'));
                    var cartCount = sessionCart.length;
                    this.$emit('getCartCount', cartCount);
                    this.$notify({
                        group: 'cart',
                        title: this.journalDetail.articleTitle,
                        text: 'Successfully Added to Cart!'
                    });
                } else if (JSON.parse(localStorage.getItem('userCart')) !== null) {
                    var _sessionCart = JSON.parse(localStorage.getItem('userCart'));
                    var ss = _sessionCart.length;
                    _sessionCart[ss] = cart;
                    localStorage.setItem('userCart', JSON.stringify(_sessionCart));
                    var a = JSON.parse(localStorage.getItem('userCart'));
                    var aCount = a.length;
                    this.$emit('getCartCount', aCount);
                    this.$notify({
                        group: 'cart',
                        title: this.journalDetail.articleTitle,
                        text: 'Successfully Added to Cart!'
                    });
                }
            } else {

                // cart.id = this.journalDetail.vendor_user_id;
                this.addCart(cart);
            }
        },
        addCart: function addCart(cart) {
            var _this3 = this;

            axios.post('/api/cart', JSON.parse(JSON.stringify(cart)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 201) {
                    _this3.loginCart.push(response.data);
                    var aCount = _this3.loginCart.length;
                    _this3.$emit('getCartCount', aCount);
                    _this3.$notify({
                        group: 'cart',
                        title: _this3.journalDetail.articleTitle,
                        text: 'Successfully Added to Cart!'
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        rateWithStar: function rateWithStar(num) {
            switch (num) {
                case '1':
                    this.NotratedOne = !this.NotratedOne;
                    this.RatedOne = !this.RatedOne;
                    this.review.rating = 1;
                    break;
                case '2':
                    this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
                    this.RatedTwo = this.RatedOne = !this.RatedTwo;
                    this.review.rating = 2;
                    break;
                case '3':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
                    this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
                    this.review.rating = 3;
                    break;
                case '4':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
                    this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
                    this.review.rating = 4;
                    break;
                case '5':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
                    this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
                    this.review.rating = 5;
                    break;
                default:
                    this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
                    this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
            }
        },
        getProductReviews: function getProductReviews(id) {
            var _this4 = this;

            var data = {
                id: id
            };
            axios.post('/api/product/get-reviews', JSON.parse(JSON.stringify(data))).then(function (response) {
                if (response.status === 200) {
                    console.log(response);
                    _this4.allReviews = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        submitReview: function submitReview() {
            var _this5 = this;

            if (this.authenticate === false) {
                this.$toasted.error('You must log in to review a product');
            } else {
                this.review.productId = this.journalDetail.uid;
                axios.post('/api/user/reviews', JSON.parse(JSON.stringify(this.review)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                    console.log(response);
                    if (response.status === 201) {
                        _this5.$toasted.success('Reviews successfully saved');
                        _this5.review.productId = _this5.review.title = _this5.review.description = '';
                        _this5.review.rating = 0;
                        _this5.NotratedOne = _this5.NotratedTwo = _this5.NotratedThree = _this5.NotratedFour = _this5.NotratedFive = true;
                        _this5.RatedOne = _this5.RatedTwo = _this5.RatedThree = _this5.RatedFour = _this5.RatedFive = false;
                    } else {
                        _this5.$toasted.error('You cannot review this product until you purchase it');
                    }
                }).catch(function (error) {
                    var errors = Object.values(error.response.data.errors);
                    errors.forEach(function (item) {
                        _this5.$toasted.error(item[0]);
                    });
                });
            }
        }
    }
});

/***/ }),

/***/ 1493:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "course-Detail" },
    [
      _c("app-banner"),
      _vm._v(" "),
      _c(
        "div",
        {
          class: { modal: _vm.epiModal, fade: _vm.epiFade },
          attrs: {
            id: "exampleModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalLabel",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog product-dialog",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-6 prodImg" }, [
                      _c("img", {
                        attrs: { src: _vm.journalDetail.coverImage }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" }, [
                      _c("div", [
                        _c("h1", [
                          _vm._v(_vm._s(_vm.journalDetail.journalTitle))
                        ]),
                        _vm._v(" "),
                        _vm.journalDetail.storeName !== undefined
                          ? _c("div", [
                              _c("span", [
                                _vm._v(_vm._s(_vm.journalDetail.author) + " ")
                              ]),
                              _c("span", [
                                _vm._v(
                                  "/ " + _vm._s(_vm.journalDetail.storeName)
                                )
                              ])
                            ])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c("div", { staticClass: "priceTabs" }, [
                        _vm.journalDetail.hardCopyPrice > 0
                          ? _c(
                              "div",
                              {
                                staticClass: "hardCover",
                                class: {
                                  hardpriceCont: _vm.hardpriceCont,
                                  hardrCopy: _vm.hardrCopy
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("hardCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.journalDetail.hardCopyPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Hard Copy")])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.journalDetail.softCopyPrice > 0
                          ? _c(
                              "div",
                              {
                                staticClass: "softCover",
                                class: {
                                  softpriceCont: _vm.softpriceCont,
                                  softrCopy: _vm.softrCopy
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("softCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.journalDetail.softCopyPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Digital Copy")])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.journalDetail.readOnlinePrice > 0
                          ? _c(
                              "div",
                              {
                                staticClass: "softCover",
                                class: {
                                  readOnlinepriceCont: _vm.readOnlinepriceCont,
                                  readOnline: _vm.readOnline
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("readOnline")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.journalDetail.readOnlinePrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Read Online")])
                              ]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-md-12 addCart" }, [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-guruh-cart",
                              on: {
                                click: function($event) {
                                  return _vm.addToCart(_vm.journalDetail.uid)
                                }
                              }
                            },
                            [_vm._v("Add Cart")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-12 reviewList" }, [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-wishlist",
                              on: {
                                click: function($event) {
                                  return _vm.addToWishlist()
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fa fa-heart-o" }),
                              _vm._v("Add to Wishlist")
                            ]
                          )
                        ])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm.journalDetail.excerptFile !== ""
                    ? _c("div", { staticClass: "abProd" }, [
                        _c("h1", [_vm._v("Excerpts")]),
                        _vm._v(" "),
                        _c("div", [
                          _c("video", {
                            staticClass: "cld-video-player demo-player",
                            attrs: {
                              src: _vm.journalDetail.excerptFile,
                              width: "520",
                              controls: ""
                            }
                          })
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "detailTabs" }, [
                    _vm._m(2),
                    _vm._v(" "),
                    _c("div", { staticClass: "tab-content" }, [
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane active",
                          attrs: { id: "home" }
                        },
                        [
                          _vm.journalDetail.isbn !== null
                            ? _c("p", [
                                _vm._v(
                                  "ISBN: " + _vm._s(_vm.journalDetail.isbn)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.publisher !== null
                            ? _c("p", [
                                _vm._v(
                                  "Publisher: " +
                                    _vm._s(_vm.journalDetail.publisher)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.publicationDate !== null
                            ? _c("p", [
                                _vm._v(
                                  "Publication Date: " +
                                    _vm._s(_vm.journalDetail.publicationDate)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.pageNo !== null
                            ? _c("p", [
                                _vm._v(
                                  "Page No: " +
                                    _vm._s(_vm.journalDetail.noOfPages)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.edition !== null
                            ? _c("p", [
                                _vm._v(
                                  "Edition: " +
                                    _vm._s(_vm.journalDetail.edition)
                                )
                              ])
                            : _vm._e()
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu1" }
                        },
                        [
                          _c("p", [
                            _vm._v(_vm._s(_vm.journalDetail.aboutThisAuthor))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu2" }
                        },
                        [
                          _c("p", {
                            domProps: {
                              innerHTML: _vm._s(
                                _vm.journalDetail.tableOfContent
                              )
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu3" }
                        },
                        [_c("p", [_vm._v(_vm._s(_vm.journalDetail.excerpt))])]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu4" }
                        },
                        [
                          _c("p", { staticClass: "sponsorName" }, [
                            _vm._v(_vm._s(_vm.journalDetail.sponsorName))
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "sponsorDesc" }, [
                            _vm._v(_vm._s(_vm.journalDetail.aboutSponsor))
                          ])
                        ]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          shipTab: _vm.shipTab,
                          shipTabSwitch: _vm.shipTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("shipping")
                          }
                        }
                      },
                      [_vm._v("Shipping and Delivery")]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          reviewTab: _vm.reviewTab,
                          reviewTabSwitch: _vm.reviewTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("reviews")
                          }
                        }
                      },
                      [_vm._v("Write a Reviews")]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          shareTab: _vm.shareTab,
                          shareTabSwitch: _vm.shareTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("share")
                          }
                        }
                      },
                      [_vm._v("Share this Items")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _vm.shipping
                      ? _c("div", { staticClass: "col-md-12 shipDetailRow" }, [
                          _vm.journalDetail.shippingDetail !== null
                            ? _c("p", [
                                _vm._v("Location: " + _vm._s(_vm.location))
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.shippingDetail !== null
                            ? _c("p", [
                                _vm._v(
                                  "Local Shipping Rate: " +
                                    _vm._s(_vm.localShippingRates)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.shippingDetail !== null
                            ? _c("p", [
                                _vm._v(
                                  "Local Shipping Time: " +
                                    _vm._s(_vm.localShippingTime)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.shippingDetail !== null
                            ? _c("p", [
                                _vm._v(
                                  "Int Shipping Rate: " +
                                    _vm._s(_vm.intlShippingRates)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.journalDetail.shippingDetail !== null
                            ? _c("p", [
                                _vm._v(
                                  "Int Shipping Time: " +
                                    _vm._s(_vm.intlShippingTime)
                                )
                              ])
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.reviews
                      ? _c("div", { staticClass: "col-md-12 " }, [
                          _c("div", { staticClass: "review-div" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.review.title,
                                    expression: "review.title"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Title"
                                },
                                domProps: { value: _vm.review.title },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.review,
                                      "title",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.review.description,
                                    expression: "review.description"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  rows: "5",
                                  placeholder: "Enter Reviews"
                                },
                                domProps: { value: _vm.review.description },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.review,
                                      "description",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", [
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedOne,
                                  "fa-star": _vm.RatedOne
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("1")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedTwo,
                                  "fa-star": _vm.RatedTwo
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("2")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedThree,
                                  "fa-star": _vm.RatedThree
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("3")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedFour,
                                  "fa-star": _vm.RatedFour
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("4")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedFive,
                                  "fa-star": _vm.RatedFive
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("5")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", { staticClass: "ml-lg-5" }, [
                                _vm._v(
                                  _vm._s(_vm.review.rating) +
                                    " " +
                                    _vm._s(
                                      _vm.review.rating > 1 ? "stars" : "star"
                                    )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-reviews mb-4",
                                on: {
                                  click: function($event) {
                                    return _vm.submitReview()
                                  }
                                }
                              },
                              [_vm._v("Submit")]
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.share
                      ? _c("div", { staticClass: "col-md-12" }, [
                          _c("p", [_vm._v("Share across social media")])
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _vm.allReviews.length > 0
                    ? _c("div", { staticClass: "row" }, [
                        _c("h1", [_vm._v("Reviews")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-12" }, [
                          _c(
                            "div",
                            [
                              _vm._l(_vm.allReviews, function(Review, index) {
                                return _c(
                                  "div",
                                  { staticClass: "mb-5" },
                                  [
                                    _c("div", { staticClass: "avatarD" }, [
                                      _c(
                                        "span",
                                        { staticClass: "reviewAvatar" },
                                        [
                                          _vm._v(
                                            _vm._s(Review.user.name.charAt(0))
                                          )
                                        ]
                                      ),
                                      _c(
                                        "span",
                                        { staticClass: "avatartitle" },
                                        [_vm._v(_vm._s(Review.title))]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "avatarDescription" },
                                      [_vm._v(_vm._s(Review.description))]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(Review.rating, function(star) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [_c("i", { staticClass: "fa fa-star" })]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _vm._l(5 - Review.rating, function(star) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-star-o"
                                          })
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              _vm._v(" "),
                              _c("hr")
                            ],
                            2
                          )
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._m(3),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-md-12" },
                      _vm._l(_vm.relatedProducts, function(product, index) {
                        return _c("div", { staticClass: "prodDis" }, [
                          _c(
                            "a",
                            {
                              attrs: {
                                href: "#",
                                "data-toggle": "modal",
                                "data-target": "#exampleModal"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.openModal(product.id)
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "catImage",
                                attrs: { src: product.coverImage }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "pTitle" }, [
                            _vm._v(_vm._s(product.title))
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "catName" }, [
                            _vm._v(_vm._s(product.vendor.storeName))
                          ]),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" })
                        ])
                      }),
                      0
                    )
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _vm._m(4),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row" },
        _vm._l(_vm.journals, function(journal, index) {
          return _vm.journals.length > 0
            ? _c("div", { staticClass: "prodDis" }, [
                _c("div", { staticClass: "vid center" }, [
                  _c(
                    "a",
                    {
                      attrs: {
                        href: "#",
                        "data-toggle": "modal",
                        "data-target": "#exampleModal"
                      },
                      on: {
                        click: function($event) {
                          return _vm.openModal(journal.uid)
                        }
                      }
                    },
                    [
                      _c("img", {
                        staticClass: "imgJournal",
                        attrs: { src: journal.coverImage }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      attrs: {
                        href: "#",
                        "data-toggle": "modal",
                        "data-target": "#exampleModal"
                      },
                      on: {
                        click: function($event) {
                          return _vm.openModal(journal.uid)
                        }
                      }
                    },
                    [
                      _c("p", { staticClass: "pTitle" }, [
                        _vm._v(_vm._s(journal.journalTitle))
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("i", { staticClass: "fa fa-star-o" }),
                  _vm._v(" "),
                  _c("i", { staticClass: "fa fa-star-o" }),
                  _vm._v(" "),
                  _c("i", { staticClass: "fa fa-star-o" }),
                  _vm._v(" "),
                  _c("i", { staticClass: "fa fa-star-o" }),
                  _vm._v(" "),
                  _c("i", { staticClass: "fa fa-star-o" })
                ])
              ])
            : _vm._e()
        }),
        0
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("span", { staticClass: "proReview" }, [_vm._v("No Customer Review")]),
      _c("span", { staticClass: "proEdi" }, [_vm._v("Editorial Review")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "nav nav-tabs" }, [
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#home" } }, [
          _vm._v("PRODUCT DETAILS")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu1" } }, [
          _vm._v("ABOUT THE AUTHOR")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu2" } }, [
          _vm._v("TABLE OF CONTENT")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu3" } }, [
          _vm._v("READ AN EXCERPT")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", HREF: "#menu4" } }, [
          _vm._v("ABOUT THE SPONSOR")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "relatedDiv" }, [
      _c("div", { staticClass: "relatedLeft" }),
      _vm._v("RELATED ITEMS"),
      _c("div", { staticClass: "relatedRight" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row rowBanner" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "search", placeholder: "Search" }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "select",
          { staticClass: "form-control", attrs: { type: "search" } },
          [_c("option", [_vm._v("Video")])]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "text", placeholder: "Tags" }
        })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-075d64f3", module.exports)
  }
}

/***/ }),

/***/ 574:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1490)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1492)
/* template */
var __vue_template__ = __webpack_require__(1493)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-075d64f3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userJournalComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-075d64f3", Component.options)
  } else {
    hotAPI.reload("data-v-075d64f3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 745:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(746)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(748)
/* template */
var __vue_template__ = __webpack_require__(749)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f443e1b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryVideoBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f443e1b", Component.options)
  } else {
    hotAPI.reload("data-v-0f443e1b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(747);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cd849544", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vidBanner[data-v-0f443e1b]{\n     position: relative;\n}\nimg[data-v-0f443e1b]{\n    margin-top: 40px;\n    width: 100%;\n    /* max-height: 300px; */\n}\n.text[data-v-0f443e1b]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size: 60px;\n    color: #ffffff;\n}\n@media(max-width: 475px){\n.vidBanner[data-v-0f443e1b]{\n     display: none;\n}\n.text[data-v-0f443e1b]{\n            font-size:30px;\n            top: 110px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-video-banner-component"
});

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vidBanner" }, [
      _c("img", { attrs: { src: "/images/vid.png" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Videos")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f443e1b", module.exports)
  }
}

/***/ })

});