webpackJsonp([142],{

/***/ 1658:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1659);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("62d24030", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-068ff4f9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-068ff4f9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1659:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.containers[data-v-068ff4f9] {\n  padding: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  min-height: 100%;\n  width: 100%;\n  height:100vh;\n  background: #f7f8fa;\n}\n.main[data-v-068ff4f9] {\n  position: relative;\n  background: white;\n  padding: 30px 50px;\n  border-radius: 4px;\n  font-size: 16px;\n  width: 70%;\n\n  z-index: 1;\n}\ninput[type=\"radio\"][data-v-068ff4f9], input[type=\"checkbox\"][data-v-068ff4f9] {\n    margin: 4px 6px 0;\n    line-height: normal;\n}\n.form-check[data-v-068ff4f9]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n}\n.form-check-input[data-v-068ff4f9]{\n  position: relative;\n}\n.form-control[data-v-068ff4f9]{\n    border-top: unset;\n    border-left: unset;\n    border-right: unset;\n    /* background-color:lightsteelblue; */\n}\n.form-control[data-v-068ff4f9]::-webkit-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n color:rgba(0, 0, 0, .44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-068ff4f9]::-moz-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n color:rgba(0, 0, 0, .44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-068ff4f9]::-ms-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n color:rgba(0, 0, 0, .44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-068ff4f9]::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n color:rgba(0, 0, 0, .44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-068ff4f9]:-ms-input-placeholder { /* Internet Explorer 10-11 */\n color:rgba(0, 0, 0, .44) !important;\n}\n.form-control[data-v-068ff4f9]::-ms-input-placeholder { /* Microsoft Edge */\n color:rgba(0, 0, 0, .44) !important;\n}\nul[data-v-068ff4f9], ol[data-v-068ff4f9]{\n    text-decoration:none !important;\n}\n.mainTemplate[data-v-068ff4f9] {\n  background: white;\n  padding: 20px 30px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  width: 90%;\n  min-height: 500px;\n  z-index: 1;\n}\n.start[data-v-068ff4f9] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width: 100%;\n}\n.count[data-v-068ff4f9] {\n  font-size: 9em;\n}\n.card-columns .card[data-v-068ff4f9] {\n    background: #f2f5fe;\n}\n.under[data-v-068ff4f9] {\n  width: 30px;\n  margin-top: 5px;\n  margin-right: auto;\n  margin-left: 0;\n  border: 0;\n  border-top: 2px solid #eee;\n}\n.return[data-v-068ff4f9] {\n  position: absolute;\n  top: 20px;\n  right: 20px;\n  cursor: pointer;\n}\n.contents[data-v-068ff4f9] {\n  height: 80%;\n}\ninput[type=\"text\"][data-v-068ff4f9]:focus {\n  background-color: rgba(255, 255, 255, 0.7);\n  color: rgba(0, 0, 0, 0.64);\n}\ninput[type=\"text\"][data-v-068ff4f9] {\n  background-color: #122048;\n  color: rgba(255, 255, 255, 0.7);\n}\n.btn[data-v-068ff4f9] {\n  text-transform: capitalize;\n}\n.overlay[data-v-068ff4f9] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(111, 148, 250, 0.1);\n}\n.scores[data-v-068ff4f9] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\nul[data-v-068ff4f9],\nol[data-v-068ff4f9] {\n  list-style: none;\n}\n.prev[data-v-068ff4f9] {\n  background-color: rgb(171, 171, 190) !important;\n  border: none !important;\n}\n.next[data-v-068ff4f9] {\n  background-color: #a4c2db !important;\n  border: none !important;\n  height: 40px;\n}\n.submit[data-v-068ff4f9] {\n  background-color: #5b84a7 !important;\n  border: none !important;\n}\n.question[data-v-068ff4f9] {\n  padding: 2px;\n}\n.options[data-v-068ff4f9] {\n  padding: 10px 20px;\n}\n.navigation[data-v-068ff4f9] {\n  width: 100%;\n  text-align: right;\n  margin-top: 40px;\n}\nlabel[data-v-068ff4f9] {\n  text-transform: capitalize;\n}\n.category[data-v-068ff4f9] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  min-height: 400px;\n}\n@media (max-width: 425px) {\n.main[data-v-068ff4f9] {\n    width: 100%;\n}\n.mainTemplate[data-v-068ff4f9] {\n    width: 100%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1660:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "checklist",
  data: function data() {
    return {
      id: null,
      result: [],
      quest: [],
      models: [],
      answers: [],
      modelAnswer: [],
      answer: "",
      checkAnswer: [],
      number: 0,
      questNumber: 0,
      totalQuest: 0,
      disable: true,
      questionAnswers: [],
      score: 0,
      totalScore: 0,
      showScores: false,
      showQuestions: true,
      showSubmit: false,
      showTemplate: false,
      start: false,
      count: 1,
      startCount: false,
      categoryBegin: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      this.id = user.id;
    }
    axios.get("/api/questions/" + this.$route.params.id).then(function (response) {
      _this.result = response.data;
      _this.models = JSON.parse(_this.result.questions);
    });
  },

  watch: {
    answer: "ableNext",
    checkAnswer: "ableNext",
    answers: "disableNext"
  },
  methods: {
    storeAnswer: function storeAnswer() {
      if (this.result.name === 'qa') {
        var data = {

          user_id: this.id,
          title: this.result.title,
          name: this.result.name,
          question_id: this.result.id,
          answers: this.totalScore
        };
        axios.post('/api/guides/history', JSON.parse(JSON.stringify(data))).then(function (response) {}).catch(function (e) {
          console.log(e);
        });
      } else {
        var _data = {
          user_id: this.id,
          title: this.result.title,
          name: this.result.name,
          question_id: this.result.id,
          answers: this.modelAnswer
        };
        axios.post('/api/guides/history', JSON.parse(JSON.stringify(_data))).then(function (response) {}).catch(function (e) {
          console.log(e);
        });
      }
    },
    startNow: function startNow() {
      this.startCount = true;

      this.start = true;
    },
    submit: function submit() {
      if (this.result.name === "template") {
        this.showTemplate = true;
      } else {
        var totalMark = this.totalQuest * 5;

        this.totalScore = parseInt(this.score / totalMark * 100) + "%";
        this.showQuestions = false;
      }
      this.showSubmit = false;
      this.showScores = true;
      this.storeAnswer();
    },
    processAnswer: function processAnswer() {
      if (this.models[this.number].questions[this.questNumber].answer == this.answer) {
        this.score += 5;
      } else {
        this.score = this.score;
      }
    },
    processMc: function processMc() {
      var _this2 = this;

      var mark = 0;
      this.models[this.number].questions[this.questNumber].answers.forEach(function (answer) {
        _this2.checkAnswer.forEach(function (checked) {
          if (answer == checked) {
            mark += 1;
            _this2.score += mark / _this2.models[_this2.number].questions[_this2.questNumber].answers.length * 5;
          }
        });
      });
    },
    ableNext: function ableNext() {
      if (this.answer !== "" || this.checkAnswer.length > 0) {
        this.disable = false;
      } else {
        this.disable = true;
      }
    },
    disableNext: function disableNext() {
      this.disable = true;
    },
    next: function next() {
      this.number++;
    },
    prev: function prev() {
      this.number--;
    },
    nextQuestion: function nextQuestion(type) {
      this.totalQuest++;
      if (this.number < this.models.length) {
        if (this.questNumber < this.models[this.number].questions.length - 1) {
          if (type === "mc") {
            this.processMc();
            this.answers.push(this.checkAnswer);
            this.checkAnswer = [];
            this.questNumber++;
          } else {
            this.processAnswer();
            this.answers.push(this.answer);

            this.answer = "";

            this.questNumber++;
          }
        } else {
          if (type === "mc") {
            this.processMc();
            this.answers.push(this.checkAnswer);
            this.checkAnswer = [];
          } else {
            this.processAnswer();
            this.answers.push(this.answer);
            this.answer = "";
          }
          if (this.number === this.models.length - 1) {
            if (type === "mc") {
              this.processMc();
              // this.answers.push(this.checkAnswer);
              this.checkAnswer = [];
            } else {
              this.processAnswer();
              // this.answers.push(this.answer);
              this.answer = "";
            }
            this.modelAnswer.push(this.answers);
            this.categoryBegin = false;
            this.showSubmit = true;
            this.start = true;
            this.showQuestions = false;
          } else {
            this.categoryBegin = true;
          }
        }
      } else {
        if (type === "mc") {
          this.processMc();
          this.answers.push(this.checkAnswer);
          this.checkAnswer = [];
        } else {
          this.processAnswer();
          this.answers.push(this.answer);
          this.answer = "";
        }
        this.modelAnswer.push(this.answers);
        this.categoryBegin = false;
        this.showSubmit = true;
        this.start = true;
        this.showQuestions = false;
      }
    },
    prevQuestion: function prevQuestion() {
      this.answers.pop();
      this.questNumber--;
    },
    beginCategory: function beginCategory() {
      this.number++;
      this.questNumber = 0;
      this.categoryBegin = false;
      this.modelAnswer.push(this.answers);
      this.answer = "";
      this.answers = [];
    },
    returnNow: function returnNow() {
      this.$router.push({
        name: "Guides"
      });
    }
  }
});

/***/ }),

/***/ 1661:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "containers" }, [
    _c(
      "div",
      {
        staticClass: "main shadow-lg",
        class: { mainTemplate: _vm.showTemplate }
      },
      [
        _c("div", { staticClass: "return" }, [
          _c("span", { on: { click: _vm.returnNow } }, [
            _vm._v("\n        Return\n        "),
            _c("i", {
              staticClass: "fa fa-undo",
              attrs: { "aria-hidden": "true" }
            })
          ])
        ]),
        _vm._v(" "),
        !_vm.start
          ? _c("div", { staticClass: "start" }, [
              _c("div", [
                !_vm.startCount
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-white btn-compliment submit m-4",
                        attrs: { type: "button" },
                        on: { click: _vm.startNow }
                      },
                      [_vm._v("Click to begin")]
                    )
                  : _vm._e()
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.showQuestions && _vm.start && !_vm.categoryBegin
          ? _c(
              "div",
              { staticClass: "mt-4 contents animated fadeInRight faster" },
              [
                _c(
                  "h2",
                  {
                    staticClass: "mb-4",
                    staticStyle: { "text-align": "center" }
                  },
                  [_vm._v(_vm._s(_vm.result.title))]
                ),
                _vm._v(" "),
                _c("h6", { staticClass: "mb-2 text-center" }, [
                  _vm._v(
                    "Category " +
                      _vm._s(_vm.number + 1) +
                      " / " +
                      _vm._s(_vm.models.length)
                  )
                ]),
                _vm._v(" "),
                _c("h4", [_vm._v(_vm._s(_vm.models[_vm.number].category))]),
                _vm._v(" "),
                _c("span", { staticClass: "under" }),
                _vm._v(" "),
                _c("div", { staticClass: " p-3" }, [
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "tf"
                    ? _c("div", { staticClass: "form-check " }, [
                        _c("div", { staticClass: "d-flex question" }, [
                          _c("span", { staticClass: "bold pr-2" }, [
                            _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                          ]),
                          _vm._v(" "),
                          _c("p", {}, [
                            _vm._v(
                              _vm._s(
                                _vm.models[_vm.number].questions[
                                  _vm.questNumber
                                ].title
                              )
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "label",
                          { staticClass: "form-check-label question" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.answer,
                                  expression: "answer"
                                }
                              ],
                              staticClass: "form-check-input",
                              attrs: {
                                type: "radio",
                                name: "true",
                                id: "true",
                                value: "true"
                              },
                              domProps: { checked: _vm._q(_vm.answer, "true") },
                              on: {
                                change: function($event) {
                                  _vm.answer = "true"
                                }
                              }
                            }),
                            _vm._v(" True\n          ")
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "label",
                          { staticClass: "form-check-label question" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.answer,
                                  expression: "answer"
                                }
                              ],
                              staticClass: "form-check-input",
                              attrs: {
                                type: "radio",
                                name: "false",
                                id: "false",
                                value: "false"
                              },
                              domProps: {
                                checked: _vm._q(_vm.answer, "false")
                              },
                              on: {
                                change: function($event) {
                                  _vm.answer = "false"
                                }
                              }
                            }),
                            _vm._v(" False\n          ")
                          ]
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "mc"
                    ? _c(
                        "div",
                        { staticClass: "question" },
                        [
                          _c("div", { staticClass: "d-flex " }, [
                            _c("span", { staticClass: "bold pr-2" }, [
                              _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                            ]),
                            _vm._v(" "),
                            _c("p", {}, [
                              _vm._v(
                                _vm._s(
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].title
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._l(
                            _vm.models[_vm.number].questions[_vm.questNumber]
                              .options,
                            function(check, index) {
                              return _c(
                                "div",
                                {
                                  key: index,
                                  staticClass: "form-check question"
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.checkAnswer,
                                        expression: "checkAnswer"
                                      }
                                    ],
                                    staticClass: "form-check-input",
                                    attrs: {
                                      type: "checkbox",
                                      name: "checkBox",
                                      id: check
                                    },
                                    domProps: {
                                      value: check,
                                      checked: Array.isArray(_vm.checkAnswer)
                                        ? _vm._i(_vm.checkAnswer, check) > -1
                                        : _vm.checkAnswer
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = _vm.checkAnswer,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = check,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              (_vm.checkAnswer = $$a.concat([
                                                $$v
                                              ]))
                                          } else {
                                            $$i > -1 &&
                                              (_vm.checkAnswer = $$a
                                                .slice(0, $$i)
                                                .concat($$a.slice($$i + 1)))
                                          }
                                        } else {
                                          _vm.checkAnswer = $$c
                                        }
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    {
                                      staticClass: "form-check-label",
                                      attrs: { for: check }
                                    },
                                    [_vm._v(_vm._s(check))]
                                  )
                                ]
                              )
                            }
                          )
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "radio"
                    ? _c(
                        "div",
                        { staticClass: "question" },
                        [
                          _c("div", { staticClass: "d-flex " }, [
                            _c("span", { staticClass: "bold pr-2" }, [
                              _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                            ]),
                            _vm._v(" "),
                            _c("p", {}, [
                              _vm._v(
                                _vm._s(
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].title
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._l(
                            _vm.models[_vm.number].questions[_vm.questNumber]
                              .options,
                            function(check, index) {
                              return _c(
                                "div",
                                {
                                  key: index,
                                  staticClass: "form-check question"
                                },
                                [
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].type === "checkbox"
                                    ? _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type: "checkbox"
                                        },
                                        domProps: {
                                          value: check,
                                          checked: Array.isArray(_vm.answer)
                                            ? _vm._i(_vm.answer, check) > -1
                                            : _vm.answer
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = _vm.answer,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = check,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  (_vm.answer = $$a.concat([
                                                    $$v
                                                  ]))
                                              } else {
                                                $$i > -1 &&
                                                  (_vm.answer = $$a
                                                    .slice(0, $$i)
                                                    .concat($$a.slice($$i + 1)))
                                              }
                                            } else {
                                              _vm.answer = $$c
                                            }
                                          }
                                        }
                                      })
                                    : _vm.models[_vm.number].questions[
                                        _vm.questNumber
                                      ].type === "radio"
                                    ? _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type: "radio"
                                        },
                                        domProps: {
                                          value: check,
                                          checked: _vm._q(_vm.answer, check)
                                        },
                                        on: {
                                          change: function($event) {
                                            _vm.answer = check
                                          }
                                        }
                                      })
                                    : _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type:
                                            _vm.models[_vm.number].questions[
                                              _vm.questNumber
                                            ].type
                                        },
                                        domProps: {
                                          value: check,
                                          value: _vm.answer
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.answer = $event.target.value
                                          }
                                        }
                                      }),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    {
                                      staticClass: "form-check-label",
                                      attrs: { for: check }
                                    },
                                    [_vm._v(_vm._s(check))]
                                  )
                                ]
                              )
                            }
                          )
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "text"
                    ? _c("div", { staticClass: "question" }, [
                        _c("div", { staticClass: "d-flex " }, [
                          _c("span", { staticClass: "bold pr-2" }, [
                            _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                          ]),
                          _vm._v(" "),
                          _c("p", {}, [
                            _vm._v(
                              _vm._s(
                                _vm.models[_vm.number].questions[
                                  _vm.questNumber
                                ].title
                              )
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group question" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.answer,
                                expression: "answer"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "text",
                              id: "textT",
                              placeholder: "Enter your answer"
                            },
                            domProps: { value: _vm.answer },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.answer = $event.target.value
                              }
                            }
                          })
                        ])
                      ])
                    : _vm._e()
                ])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("div", [
          _vm.showScores || _vm.showSubmit
            ? _c("div", { staticClass: "mt-4 contents scores" }, [
                _c(
                  "h2",
                  {
                    staticClass: "mb-4",
                    staticStyle: { "text-align": "center" }
                  },
                  [_vm._v(_vm._s(_vm.result.title))]
                ),
                _vm._v(" "),
                _vm.showScores
                  ? _c("div", { staticClass: "animated slideInUp faster" }, [
                      _vm.result.name === "template"
                        ? _c("div", [
                            _c(
                              "div",
                              { staticClass: "card-columns" },
                              _vm._l(_vm.models, function(model, index) {
                                return _c(
                                  "div",
                                  { key: index, staticClass: "card" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "card-header bg-primary text-white text-center"
                                      },
                                      [
                                        _c("h4", [
                                          _vm._v(_vm._s(model.category))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.modelAnswer[index], function(
                                      result,
                                      idx
                                    ) {
                                      return _c(
                                        "div",
                                        { key: idx, staticClass: "card-body" },
                                        [
                                          _c(
                                            "h5",
                                            { staticClass: "card-title" },
                                            [
                                              _vm._v(
                                                "Q" +
                                                  _vm._s(idx + 1) +
                                                  ". " +
                                                  _vm._s(
                                                    model.questions[idx].title
                                                  )
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          typeof result == "object"
                                            ? _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _c(
                                                    "small",
                                                    {
                                                      staticClass: "text-faded"
                                                    },
                                                    [_vm._v("Your reply :")]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "ul",
                                                    {
                                                      staticClass:
                                                        "text-capitalize"
                                                    },
                                                    _vm._l(result, function(
                                                      results,
                                                      idx
                                                    ) {
                                                      return _c(
                                                        "li",
                                                        { key: idx },
                                                        [
                                                          _vm._v(
                                                            " " +
                                                              _vm._s(
                                                                results.toLowerCase()
                                                              )
                                                          )
                                                        ]
                                                      )
                                                    }),
                                                    0
                                                  )
                                                ]
                                              )
                                            : _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _c(
                                                    "small",
                                                    {
                                                      staticClass: "text-faded"
                                                    },
                                                    [_vm._v("Your reply :")]
                                                  ),
                                                  _vm._v(
                                                    "   " +
                                                      _vm._s(
                                                        result
                                                          .toLowerCase()
                                                          .replace(/''/g, " ")
                                                      )
                                                  )
                                                ]
                                              )
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ])
                        : _vm.result.name === "bg"
                        ? _c("div", [
                            _c(
                              "div",
                              { staticClass: "card-columns" },
                              _vm._l(_vm.models, function(model, index) {
                                return _c(
                                  "div",
                                  { key: index, staticClass: "card" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "card-header bg-primary text-white text-center"
                                      },
                                      [
                                        _c("h4", [
                                          _vm._v(_vm._s(model.category))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.modelAnswer[index], function(
                                      result,
                                      idx
                                    ) {
                                      return _c(
                                        "div",
                                        { key: idx, staticClass: "card-body" },
                                        [
                                          _c(
                                            "h5",
                                            { staticClass: "card-title" },
                                            [
                                              _vm._v(
                                                "Q" +
                                                  _vm._s(idx + 1) +
                                                  ". " +
                                                  _vm._s(
                                                    model.questions[idx].title
                                                  )
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          typeof result == "object "
                                            ? _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _c(
                                                    "small",
                                                    {
                                                      staticClass: "text-faded"
                                                    },
                                                    [_vm._v("Your reply :")]
                                                  ),
                                                  _vm._v(" "),
                                                  _vm._l(result, function(
                                                    results,
                                                    idx
                                                  ) {
                                                    return _c(
                                                      "span",
                                                      {
                                                        key: idx,
                                                        staticClass:
                                                          "text-capitalize"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            results
                                                              .toLowerCase()
                                                              .replace(
                                                                /''/g,
                                                                "-"
                                                              )
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  })
                                                ],
                                                2
                                              )
                                            : _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _c(
                                                    "small",
                                                    {
                                                      staticClass: "text-faded"
                                                    },
                                                    [_vm._v("Your reply :")]
                                                  ),
                                                  _vm._v(
                                                    "  " +
                                                      _vm._s(
                                                        result.toLowerCase()
                                                      )
                                                  )
                                                ]
                                              )
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ])
                        : _c("div", [
                            _c("p", [
                              _vm._v(
                                "Congratulations you just completed this Checklist, you scored"
                              )
                            ]),
                            _vm._v(" "),
                            _c("h1", { staticClass: "text-center" }, [
                              _vm._v(_vm._s(_vm.totalScore))
                            ])
                          ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.showSubmit
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-white btn-compliment submit m-4",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.submit()
                          }
                        }
                      },
                      [_vm._v("Submit")]
                    )
                  : _vm._e()
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _vm.categoryBegin
          ? _c("div", { staticClass: "category contents" }, [
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm text-white btn-compliment next m-4",
                  attrs: { type: "button" },
                  on: { click: _vm.beginCategory }
                },
                [_vm._v("Begin Next Category")]
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.showQuestions && _vm.start && !_vm.categoryBegin
          ? _c("div", { staticClass: "navigation questionNav" }, [
              _vm.questNumber !== 0
                ? _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn elevated_btn_sm text-white btn-compliment prev m-4",
                      attrs: { type: "button" },
                      on: { click: _vm.prevQuestion }
                    },
                    [_vm._v("Prev question")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm text-white btn-compliment next m-4",
                  attrs: { type: "button", disabled: _vm.disable },
                  on: {
                    click: function($event) {
                      return _vm.nextQuestion(
                        _vm.models[_vm.number].questions[_vm.questNumber].type
                      )
                    }
                  }
                },
                [_vm._v("Next question")]
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.showQuestions &&
        _vm.start &&
        _vm.questNumber === _vm.models[_vm.number].questions.length &&
        !_vm.categoryBegin
          ? _c("div", { staticClass: "navigation" }, [
              _vm.number !== 0
                ? _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn elevated_btn_sm text-white btn-compliment prev m-4",
                      attrs: { type: "button" },
                      on: { click: _vm.prev }
                    },
                    [_vm._v("Prev Category")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm text-white btn-compliment next m-4",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.next()
                    }
                  }
                },
                [_vm._v("Next Category")]
              )
            ])
          : _vm._e()
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-068ff4f9", module.exports)
  }
}

/***/ }),

/***/ 612:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1658)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1660)
/* template */
var __vue_template__ = __webpack_require__(1661)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-068ff4f9"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/checklistComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-068ff4f9", Component.options)
  } else {
    hotAPI.reload("data-v-068ff4f9", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});