webpackJsonp([99],{

/***/ 1143:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1144);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7095143a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1d48292e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editProductShippingComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1d48292e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editProductShippingComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1144:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n#localShippingRates[data-v-1d48292e], #localShippingTimes[data-v-1d48292e], #intShippingRates[data-v-1d48292e], #intShippingTimes[data-v-1d48292e], #yourLocation[data-v-1d48292e] {\n    width: 50%;\n}\n#addressLineOne[data-v-1d48292e], #localShippingRates[data-v-1d48292e], #intShippingRates[data-v-1d48292e], #deliveryInfo[data-v-1d48292e] {\n    margin-top: 150px;\n}\n.shippingDetails[data-v-1d48292e] {\n    padding: 10px;\n    background: #ffffff;\n}\n.shippingDetails div[data-v-1d48292e] {\n    margin-bottom: 50px;\n}\n.biz-submit[data-v-1d48292e] {\n    padding: 20px 40px;\n    font-size: 20px;\n}\n.butt[data-v-1d48292e] {\n    text-align: center;\n    margin-top: 40px;\n}\ninput[data-v-1d48292e], textarea[data-v-1d48292e] {\n    border-top: 0 solid green !important;\n    border-left: 0 solid green !important;\n    border-right: 0 solid green !important;\n}\ninput[data-v-1d48292e], textarea[data-v-1d48292e] {\n    border-bottom: 1px solid #d2d6de;\n}\ninput[data-v-1d48292e]::-webkit-input-placeholder, textarea[data-v-1d48292e]::-webkit-input-placeholder  {\n    color: #dbdbdb !important;\n    font-weight: bold;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "edit-product-shipping-component",
    data: function data() {
        return {
            shippingDetails: {
                id: this.$route.params.id,
                location: '',
                addressLineOne: '',
                addressLineTwo: '',
                state: '',
                localShippingRates: '',
                localShippingTime: '',
                intShippingRates: '',
                intShippingTime: '',
                deliveryInformation: '',
                returnPolicy: '',
                extraComment: ''
            },
            token: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authVendor')) {
            var authVendor = JSON.parse(localStorage.getItem('authVendor'));
            this.token = authVendor.access_token;
            axios.get('api/product/' + this.shippingDetails.id + '/edit', { headers: { "Authorization": 'Bearer ' + authVendor.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this.shippingDetails = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        addProduct: function addProduct() {
            var _this2 = this;

            axios.post('api/add/shipping/detail', JSON.parse(JSON.stringify(this.shippingDetails)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this2.$toasted.success("Product successfully updated");
                    _this2.$router.push('/vendor/all');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1146:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "form-grids row widget-shadow" }, [
      _c("h2", [_vm._v("Shipping Detail")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 shippingDetails" }, [
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.location,
                expression: "shippingDetails.location"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "yourLocation",
              placeholder: "Select Your Location"
            },
            domProps: { value: _vm.shippingDetails.location },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "location", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.addressLineOne,
                expression: "shippingDetails.addressLineOne"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "addressLineOne",
              placeholder: "Address Line 1"
            },
            domProps: { value: _vm.shippingDetails.addressLineOne },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "addressLineOne",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.addressLineTwo,
                expression: "shippingDetails.addressLineTwo"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "addressLineTwo",
              placeholder: "Address Line 2"
            },
            domProps: { value: _vm.shippingDetails.addressLineTwo },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "addressLineTwo",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.state,
                expression: "shippingDetails.state"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", id: "state", placeholder: "State" },
            domProps: { value: _vm.shippingDetails.state },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "state", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.localShippingRates,
                expression: "shippingDetails.localShippingRates"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "number",
              id: "localShippingRates",
              placeholder: "Enter Local Shipping Rates"
            },
            domProps: { value: _vm.shippingDetails.localShippingRates },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "localShippingRates",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.localShippingTime,
                expression: "shippingDetails.localShippingTime"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "localShippingTimes",
              placeholder: "Enter Local Shipping Times"
            },
            domProps: { value: _vm.shippingDetails.localShippingTime },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "localShippingTime",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.intShippingRates,
                expression: "shippingDetails.intShippingRates"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "intShippingRates",
              placeholder: "Enter International Shipping Rates"
            },
            domProps: { value: _vm.shippingDetails.intShippingRates },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "intShippingRates",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.intShippingTime,
                expression: "shippingDetails.intShippingTime"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "intShippingTimes",
              placeholder: "Enter International Shipping Times"
            },
            domProps: { value: _vm.shippingDetails.intShippingTime },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "intShippingTime",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.deliveryInformation,
                expression: "shippingDetails.deliveryInformation"
              }
            ],
            staticClass: "form-control",
            attrs: {
              rows: "10",
              id: "deliveryInfo",
              placeholder: "Delivery Information (Max 500 words)"
            },
            domProps: { value: _vm.shippingDetails.deliveryInformation },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "deliveryInformation",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.returnPolicy,
                expression: "shippingDetails.returnPolicy"
              }
            ],
            staticClass: "form-control",
            attrs: { rows: "10", placeholder: "Return Policy (Max 500 words)" },
            domProps: { value: _vm.shippingDetails.returnPolicy },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "returnPolicy",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.extraComment,
                expression: "shippingDetails.extraComment"
              }
            ],
            staticClass: "form-control",
            attrs: {
              rows: "10",
              placeholder: "Extra Comments (Max 500 words)"
            },
            domProps: { value: _vm.shippingDetails.extraComment },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "extraComment",
                  $event.target.value
                )
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 butt" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-submit biz-submit",
            on: {
              click: function($event) {
                return _vm.addProduct()
              }
            }
          },
          [_vm._v("Submit")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1d48292e", module.exports)
  }
}

/***/ }),

/***/ 503:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1143)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1145)
/* template */
var __vue_template__ = __webpack_require__(1146)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1d48292e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/editProductShippingComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1d48292e", Component.options)
  } else {
    hotAPI.reload("data-v-1d48292e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});