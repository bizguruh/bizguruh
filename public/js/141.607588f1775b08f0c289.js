webpackJsonp([141],{

/***/ 1947:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1948);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("50c97433", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0ef70d68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./afterRegister.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0ef70d68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./afterRegister.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1948:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-0ef70d68]{\n    height:100vh;\n      background: #f7f8fa;\n      position:relative;\n}\n.skip[data-v-0ef70d68]{\n    position: absolute;\n    width:200px;\n    left:50%;\n    margin-left: -100px !important;\n    top:60px;\n}\n.boxes[data-v-0ef70d68]{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    height: 100%;\n}\n.box_1[data-v-0ef70d68],.box_2[data-v-0ef70d68]{\n    width: 400px;\n    height: 400px;\n    background: #fff;\n    display:-webkit-box;\n    display:-ms-flexbox;\n    display:flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n  -webkit-box-shadow: 0 0.175rem 0.24rem rgba(0, 0, 0, 0.30) !important;\n          box-shadow: 0 0.175rem 0.24rem rgba(0, 0, 0, 0.30) !important;\n  cursor: pointer;\n}\n.box_1[data-v-0ef70d68]:hover,\n.box_2[data-v-0ef70d68]:hover {\n  -webkit-box-shadow: 0 0.4rem 0.5rem rgba(0, 0, 0, 0.30) !important;\n          box-shadow: 0 0.4rem 0.5rem rgba(0, 0, 0, 0.30) !important;\n}\n.box_1:hover .fas[data-v-0ef70d68],\n.box_2:hover .fas[data-v-0ef70d68],\n.box_1:hover h3[data-v-0ef70d68],\n.box_2:hover h3[data-v-0ef70d68] {\n  -webkit-transform: scale(1.05);\n          transform: scale(1.05);\n}\n@media(max-width:768px){\n.box_1[data-v-0ef70d68],.box_2[data-v-0ef70d68]{\n    width: 250px;\n    height: 250px;\n}\n}\n@media(max-width:425px){\n.box_1[data-v-0ef70d68],.box_2[data-v-0ef70d68]{\n    width: 150px;\n    height: 150px;\n}\nh3[data-v-0ef70d68]{\n    font-size: 16px;\n}\n}\n@media(max-width:375px){\n.box_1[data-v-0ef70d68],.box_2[data-v-0ef70d68]{\n    width: 100px;\n    height: 100px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1949:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ 1950:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main-page" },
    [
      _c(
        "router-link",
        {
          staticClass: "mx-auto text-center text-main skip",
          attrs: { to: "/entrepreneur/explore" }
        },
        [_c("h4", { staticClass: "pt-5 " }, [_vm._v("Skip for later")])]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "boxes" },
        [
          _c("router-link", { attrs: { to: "/entrepreneur/preferences" } }, [
            _c("div", { staticClass: "box_1" }, [
              _c("i", {
                staticClass: "fas fa-cogs mb-4 fa-5x text-main",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" "),
              _c("h3", { staticClass: "josefin text-main" }, [
                _vm._v(" Update Preference")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("router-link", { attrs: { to: "/entrepreneur/update-profile" } }, [
            _c("div", { staticClass: "box_2" }, [
              _c("i", {
                staticClass: "fas fa-user-edit fa-5x text-main mb-4 "
              }),
              _vm._v(" "),
              _c("h3", { staticClass: "josefin text-main" }, [
                _vm._v(" Update Profile")
              ])
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0ef70d68", module.exports)
  }
}

/***/ }),

/***/ 658:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1947)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1949)
/* template */
var __vue_template__ = __webpack_require__(1950)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0ef70d68"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/afterRegister.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0ef70d68", Component.options)
  } else {
    hotAPI.reload("data-v-0ef70d68", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});