webpackJsonp([168],{

/***/ 1253:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1254);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("50d01c56", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-464b3d14\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-464b3d14\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1254:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nth[data-v-464b3d14] {\n    background-color: #f3f3f3;\n    color: #000000;\n    font-weight: bold;\n}\nth[data-v-464b3d14], td[data-v-464b3d14] {\n    padding: 30px !important;\n}\ntd[data-v-464b3d14] {\n     color: #aaaaaa;\n}\n.content-wrapper[data-v-464b3d14] {\n    height: 100vh;\n}\n.deleteProductBtn[data-v-464b3d14] {\n    text-align: right;\n}\n.main-page[data-v-464b3d14] {\n    padding: 40px 20px;\n    max-height: 100vh;\n    height: 100vh;\n    overflow: scroll;\n}\n.btn-below[data-v-464b3d14] {\n    padding-bottom: 10px;\n}\n.btn-custom-market input[data-v-464b3d14] {\n    position: absolute !important;\n    clip: rect(0, 0, 0, 0);\n    height: 1px;\n    width: 1px;\n    border: 0;\n    overflow: hidden;\n}\n.btn-custom-market label[data-v-464b3d14] {\n    float: left;\n    display: inline-block;\n    width: auto;\n    background-color: #e4e4e4;\n    color: rgba(0, 0, 0, 0.6);\n    font-size: 14px;\n    font-weight: normal;\n    text-align: center;\n    text-shadow: none;\n    padding: 6px 14px;\n    border: 1px solid rgba(0, 0, 0, 0.2);\n    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    -webkit-transition: all 0.1s ease-in-out;\n    transition:         all 0.1s ease-in-out;\n}\n.btn-custom-market[data-v-464b3d14] {\n    overflow: hidden;\n}\n.btn-custom-market label[data-v-464b3d14]:hover {\n    cursor: pointer;\n}\n.btn-custom-market input[data-v-464b3d14]:checked {\n    background-color: #A5DC86;\n    -webkit-box-shadow: none;\n    box-shadow: none;\n}\n.btn-custom-market label[data-v-464b3d14]:first-of-type {\n    border-radius: 4px 0 0 4px;\n}\n.btn-custom-market label[data-v-464b3d14]:last-of-type {\n    border-radius: 0 4px 4px 0;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-product-component",
    data: function data() {
        return {
            token: '',
            products: [],
            overallCheck: false,
            verified: 'All',
            sponsor: '',
            isActive: true
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.token = admin.access_token;
        console.log(this.products);

        if (admin != null) {
            axios.get('/api/admin/all/product', { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
                var emptyArray = [];
                console.log(response);
                response.data.data.forEach(function (item) {
                    switch (item.prodType) {
                        case 'Books':
                            _this.$set(item.books, 'verify', item.verify);
                            _this.$set(item.books, 'draftValue', item.draftType);
                            _this.$set(item.books, 'editFlag', item.editFlag);
                            _this.$set(item.books, 'uniId', item.id);
                            emptyArray.push(item.books);
                            break;
                        case 'White Paper':
                            _this.$set(item.whitePaper, 'verify', item.verify);
                            _this.$set(item.whitePaper, 'draftValue', item.draftType);
                            _this.$set(item.whitePaper, 'editFlag', item.editFlag);
                            _this.$set(item.whitePaper, 'uniId', item.id);
                            emptyArray.push(item.whitePaper);
                            break;
                        case 'Market Reports':
                            _this.$set(item.marketReport, 'verify', item.verify);
                            _this.$set(item.marketReport, 'draftValue', item.draftType);
                            _this.$set(item.marketReport, 'editFlag', item.editFlag);
                            _this.$set(item.marketReport, 'uniId', item.id);
                            emptyArray.push(item.marketReport);
                            break;
                        case 'Market Research':
                            _this.$set(item.marketResearch, 'verify', item.verify);
                            _this.$set(item.marketResearch, 'draftValue', item.draftType);
                            _this.$set(item.marketResearch, 'editFlag', item.editFlag);
                            _this.$set(item.marketResearch, 'uniId', item.id);
                            emptyArray.push(item.marketResearch);
                            break;
                        case 'Webinar':
                            _this.$set(item.webinar, 'verify', item.verify);
                            _this.$set(item.webinar, 'draftValue', item.draftType);
                            _this.$set(item.webinar, 'editFlag', item.editFlag);
                            _this.$set(item.webinar, 'uniId', item.id);
                            emptyArray.push(item.webinar);
                            break;
                        case 'Podcast':
                            _this.$set(item.webinar, 'verify', item.verify);
                            _this.$set(item.webinar, 'draftValue', item.draftType);
                            _this.$set(item.webinar, 'editFlag', item.editFlag);
                            _this.$set(item.webinar, 'uniId', item.id);
                            emptyArray.push(item.webinar);
                            break;
                        case 'Videos':
                            _this.$set(item.webinar, 'verify', item.verify);
                            _this.$set(item.webinar, 'draftValue', item.draftType);
                            _this.$set(item.webinar, 'uniId', item.id);
                            emptyArray.push(item.webinar);
                            break;
                        case 'Journals':
                            _this.$set(item.journal, 'verify', item.verify);
                            _this.$set(item.journal, 'draftValue', item.draftType);
                            _this.$set(item.journal, 'editFlag', item.editFlag);
                            _this.$set(item.journal, 'uniId', item.id);
                            emptyArray.push(item.journal);
                            break;
                        case 'Courses':
                            _this.$set(item.courses, 'verify', item.verify);
                            _this.$set(item.courses, 'draftValue', item.draftType);
                            _this.$set(item.courses, 'editFlag', item.editFlag);
                            _this.$set(item.courses, 'uniId', item.id);
                            emptyArray.push(item.courses);
                            break;
                        case 'Events':
                            console.log('vvvvv');
                            break;
                        default:
                            return false;
                    }
                });
                _this.products = emptyArray;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        overallCheckBox: function overallCheckBox() {
            var _this2 = this;

            this.showData.forEach(function (item) {
                if (!('itemClick' in item)) {
                    _this2.$set(item, 'itemClick', true);
                } else if (_this2.overallCheck) {
                    _this2.$set(item, 'itemClick', true);
                } else {
                    _this2.$set(item, 'itemClick', false);
                }
            });
        },

        // admin/delete/products
        deleteItem: function deleteItem() {
            var _this3 = this;

            var deletedArray = [];
            this.showData.forEach(function (item) {
                if (item.itemClick) {
                    console.log(item);
                    deletedArray.push(item.uniId);
                }
            });
            if (deletedArray.length === 0) {
                this.$toasted.error('You must select at least one product to delete');
            } else {
                console.log(deletedArray);
                axios.post('/api/admin/delete/products', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        console.log(response);
                        for (var i = 0; i < deletedArray.length; i++) {
                            console.log(deletedArray[i]);
                            _this3.showData.splice(i, 1);
                        }
                        _this3.$toasted.success('All selected items deleted');
                    } else {
                        _this3.$toasted.error('Unable to delete');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    },
    computed: {
        showData: function showData() {
            var verify = this.verified;
            var sponsor = this.sponsor;
            if (verify === 'All') {
                return this.products;
            } else if (verify === "1" || verify === "0") {
                return this.products.filter(function (value) {
                    return value.verify === parseInt(verify);
                });
            } else if (verify === 'PN') {
                return this.products.filter(function (value) {
                    return value.editFlag === 'Y';
                });
            } else {
                return this.products.filter(function (value) {
                    return value.sponsor === verify;
                });
            }
        }
    }
});

/***/ }),

/***/ 1256:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "btn-below" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-8" }, [
            _c("div", { staticClass: "btn-custom-market" }, [
              _c("label", { class: { active: _vm.isActive } }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "All" },
                  domProps: { checked: _vm._q(_vm.verified, "All") },
                  on: {
                    change: function($event) {
                      _vm.verified = "All"
                    }
                  }
                }),
                _vm._v("All")
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "1" },
                  domProps: { checked: _vm._q(_vm.verified, "1") },
                  on: {
                    change: function($event) {
                      _vm.verified = "1"
                    }
                  }
                }),
                _vm._v("Verified")
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "0" },
                  domProps: { checked: _vm._q(_vm.verified, "0") },
                  on: {
                    change: function($event) {
                      _vm.verified = "0"
                    }
                  }
                }),
                _vm._v("Unverified")
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "Y" },
                  domProps: { checked: _vm._q(_vm.verified, "Y") },
                  on: {
                    change: function($event) {
                      _vm.verified = "Y"
                    }
                  }
                }),
                _vm._v("Sponsor")
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "N" },
                  domProps: { checked: _vm._q(_vm.verified, "N") },
                  on: {
                    change: function($event) {
                      _vm.verified = "N"
                    }
                  }
                }),
                _vm._v("Unsponsor")
              ]),
              _vm._v(" "),
              _c("label", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.verified,
                      expression: "verified"
                    }
                  ],
                  attrs: { type: "radio", value: "PN" },
                  domProps: { checked: _vm._q(_vm.verified, "PN") },
                  on: {
                    change: function($event) {
                      _vm.verified = "PN"
                    }
                  }
                }),
                _vm._v("Pending Update")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
            _c(
              "button",
              { staticClass: "btn btn-danger", on: { click: _vm.deleteItem } },
              [_vm._v("Delete")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("table", { staticClass: "table" }, [
        _c("thead", [
          _c("tr", [
            _c("th", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.overallCheck,
                    expression: "overallCheck"
                  }
                ],
                attrs: { type: "checkbox" },
                domProps: {
                  checked: Array.isArray(_vm.overallCheck)
                    ? _vm._i(_vm.overallCheck, null) > -1
                    : _vm.overallCheck
                },
                on: {
                  change: [
                    function($event) {
                      var $$a = _vm.overallCheck,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && (_vm.overallCheck = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.overallCheck = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.overallCheck = $$c
                      }
                    },
                    _vm.overallCheckBox
                  ]
                }
              })
            ]),
            _vm._v(" "),
            _c("th", [_vm._v("Identification")]),
            _vm._v(" "),
            _c("th", [_vm._v("Details")]),
            _vm._v(" "),
            _c("th", [_vm._v("Status")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.showData, function(product, index) {
            return _c("tr", [
              _c("td", [
                _c("input", {
                  attrs: { type: "checkbox" },
                  domProps: { checked: product.itemClick },
                  on: {
                    change: function($event) {
                      product.itemClick = !product.itemClick
                      _vm.overallCheck = false
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              product.articleTitle
                ? _c("td", [_vm._v(_vm._s(product.articleTitle))])
                : _c("td", [_vm._v(_vm._s(product.title))]),
              _vm._v(" "),
              product.verify === 1
                ? _c("td", [_vm._v("Active")])
                : _c("td", [_vm._v("Pending")]),
              _vm._v(" "),
              _c(
                "td",
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "adminShowProduct",
                          params: { id: product.uniId }
                        }
                      }
                    },
                    [_c("i", { staticClass: "fa fa-arrow-right" })]
                  )
                ],
                1
              )
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-464b3d14", module.exports)
  }
}

/***/ }),

/***/ 523:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1253)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1255)
/* template */
var __vue_template__ = __webpack_require__(1256)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-464b3d14"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-464b3d14", Component.options)
  } else {
    hotAPI.reload("data-v-464b3d14", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});