webpackJsonp([81,187],{

/***/ 1605:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1606);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("155588ee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e7d1e6ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./communityComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e7d1e6ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./communityComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1606:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-e7d1e6ca] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n    margin: 10px 10px 22px;\n    padding:5px 15px;\n    background:#f7f7f7;\n}\nul > li[data-v-e7d1e6ca] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nul > li[data-v-e7d1e6ca]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-e7d1e6ca] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.main_content[data-v-e7d1e6ca] {\n\tpadding-top: 80px;\n\tpadding-bottom: 80px;\n\ttext-align: center;\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n    background: hsl(0, 0%, 100%);\n}\n.btn-primary[data-v-e7d1e6ca] {\n\tbackground: #5c7991 !important;\n\tborder: #5c7991 !important;\n\ttext-transform: capitalize;\n}\n.headers[data-v-e7d1e6ca] {\n\tfont-weight: bold;\n\tfont-size: calc(\n\t\t18px + (34 - 18) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: calc(1.3em + (1.6 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n\tmargin-bottom: 20px;\n}\n.subHeaders[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t16px + (28 - 16) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: calc(1.3em + (1.6 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n\tmargin-bottom: 20px;\n\tfont-weight: bold;\n}\n.miniHeaders[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: calc(1.3em + (1.6 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n\tmargin-bottom: 10px;\n\tfont-weight: bold;\n}\n.beg[data-v-e7d1e6ca] {\n\tpadding: 5px;\n\tbackground: #fff;\n}\n.rbgist[data-v-e7d1e6ca] {\n\tpadding: 5px;\n\tbackground: #fff;\n    font-size: 16px;\n    text-align:left;\n    line-height: 1.6;\n}\n.yinsights[data-v-e7d1e6ca] {\n\tpadding: 5px;\n\tbackground: #fff;\n}\n.one[data-v-e7d1e6ca],\n.two[data-v-e7d1e6ca],\n.three[data-v-e7d1e6ca] {\n\tfont-size: 40px;\n}\n.first_text[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: 1.6;\n}\n.second_text[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: 1.6;\n\tmargin-bottom: 10px;\n}\n.third_text[data-v-e7d1e6ca] {\n    text-align: left;\n    line-height: 1.6;\n}\n.fourth_text[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n    ) !important;\n    line-height: 1.6;\n}\n.sixth_text[data-v-e7d1e6ca] {\n\tfont-size: calc(\n\t\t15px + (17 - 15) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: 1.6;\n}\n.instruct[data-v-e7d1e6ca] {\n\tmargin: 20px 0;\n\tfont-size: 16px !important;\n}\n.instruct_main[data-v-e7d1e6ca] {\n\tmargin: 20px 0;\n\tfont-size: 18px !important;\n}\n.bgLightBlue[data-v-e7d1e6ca] {\n\tbackground-color: #e8edf1;\n}\n.final_button[data-v-e7d1e6ca] {\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n.work_container[data-v-e7d1e6ca] {\n\tposition: relative;\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\twidth: 165px;\n\theight: 165px;\n\tborder-radius: 50%;\n\tborder-left: 1px solid #a4c2db;\n\tborder-right: 1px solid #a4c2db;\n\toverflow: hidden;\n\tmargin-bottom: 20px;\n\tmargin-right: auto;\n\t/* margin-left:auto; */\n}\n.work_container img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 100%;\n\tbackground: #a4c2db;\n}\n.love_img[data-v-e7d1e6ca] {\n\tborder-top: 2px  rgb(113, 148, 177) dotted;\n\tborder-bottom: 2px rgb(113, 148, 177) dotted;\n\tmax-width: 50px;\n    max-height: 50px;\n    \twidth: 100%;\n\theight: 100%;\n    padding:3px;\n\tborder-radius: 50%;\n\toverflow: hidden;\n}\n.love_img img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 100%;\n    background: #a4c2db;\n    border-radius: 50%;\n}\n.love_name[data-v-e7d1e6ca] {\n\tpadding: 25px 0 0 10px;\n    font-weight: bold;\n    font-size:12px;\n    text-align: left;\n}\n.love_text[data-v-e7d1e6ca] {\n    text-align: left;\n    line-height: 1.5;\n}\n.love_content[data-v-e7d1e6ca] {\n\twidth: 100%;\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n}\n.mobile[data-v-e7d1e6ca] {\n\tdisplay: none;\n\twidth: 100%;\n}\n.desktop[data-v-e7d1e6ca] {\n\twidth: 100%;\n}\n.extra_content[data-v-e7d1e6ca] {\n\twidth: 70%;\n\tpadding: 30px;\n\tmargin: 0 auto;\n    text-align: left;\n    line-height: 1.6;\n}\n.extra_text[data-v-e7d1e6ca] {\n\ttext-align: left;\n\tmargin-bottom: 10px;\n}\n.first_content[data-v-e7d1e6ca] {\n\ttext-align: left;\n\tposition: relative;\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.first_image[data-v-e7d1e6ca] {\n\twidth: 50%;\n\tposition: absolute;\n\t/* border: 1px solid blue; */\n\tright: 0;\n\theight: 550px;\n\tbackground-position: center !important;\n\tbackground-origin: border-box !important;\n\tbackground-size: cover !important;\n\tz-index: 70 !important;\n}\n.first_image img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 100%;\n}\n.blank_content[data-v-e7d1e6ca] {\n\twidth: 70%;\n\tposition: relative !important;\n\tpadding-top: 60px !important;\n\tpadding-bottom: 60px !important;\n\tmargin-left: auto !important;\n\tmargin-right: auto !important;\n}\n.publish_content[data-v-e7d1e6ca] {\n\twidth: 60%;\n\tposition: relative;\n\t/* border: 1px solid red; */\n\theight: 400px;\n\tbackground: #e8f3ec;\n\t/* margin: 0 60px; */\n\tmargin-top: 8px !important;\n\tpadding-bottom: 100px !important;\n\tpadding: 50px !important;\n\t-webkit-box-sizing: border-box !important;\n\t        box-sizing: border-box !important;\n\tz-index: 100 !important;\n}\n.line_content[data-v-e7d1e6ca] {\n\tborder-left: 1px solid black;\n\tright: 438px !important;\n\tbottom: -203px !important;\n\theight: 204px;\n\tposition: absolute;\n}\n.second_content[data-v-e7d1e6ca] {\n\tposition: relative;\n\ttext-align: left;\n\twidth: 75%;\n\n\tmargin-top: 140px;\n\tpadding-top: 60px !important;\n\tpadding-bottom: 30px !important;\n\tmargin-left: auto !important;\n\tmargin-right: auto !important;\n\tmargin-bottom: 5px !important;\n\t-webkit-box-sizing: border-box;\n\t        box-sizing: border-box;\n\tdisplay: inline-block;\n\t-webkit-box-align: end;\n\t    -ms-flex-align: end;\n\t        align-items: flex-end;\n\tfont-size: calc(14px + (16 - 14) * ((100vw - 300px) / (1600 - 300)));\n\tline-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n\tvertical-align: top;\n}\n.believe_image[data-v-e7d1e6ca] {\n}\n.believe_content[data-v-e7d1e6ca] {\n\twidth: 50%;\n\tfloat: right;\n\tpadding: 0 40px;\n}\n.believe_image[data-v-e7d1e6ca] {\n\tfloat: left;\n\ttext-align: right;\n\twidth: 50%;\n\theight: 400px;\n\t/* border-radius: 50%; */\n\tbackground-position: center !important;\n\tbackground-origin: border-box !important;\n\tbackground-size: cover !important;\n\toverflow: hidden;\n}\n.believe_image img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 300px;\n\tborder-radius: 50%;\n}\n.cont[data-v-e7d1e6ca]{\n    width:100%;\n}\n.third_content[data-v-e7d1e6ca],\n.fourth_content[data-v-e7d1e6ca],\n.fifth_content[data-v-e7d1e6ca],\n.sixth_content[data-v-e7d1e6ca],\n.seventh_content[data-v-e7d1e6ca],\n.eight_content[data-v-e7d1e6ca],\n.ninth_content[data-v-e7d1e6ca] {\n\tposition: relative;\n\tfont-size: calc(\n\t\t14px + (16 - 14) * ((100vw - 300px) / (1600 - 300))\n\t) !important;\n\tline-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.third_content[data-v-e7d1e6ca] {\n\tpadding: 60px;\n\t/* border: 1px solid blue; */\n\tbackground: #bcc8d2;\n}\n.third_content .line_content[data-v-e7d1e6ca] {\n\tleft: 271px !important;\n}\n.works_content[data-v-e7d1e6ca] {\n\t/* border: 1px solid blue; */\n\n\theight:auto;\n\tmargin-bottom: 30px;\n\ttext-align: left;\n}\n.fourth_content[data-v-e7d1e6ca] {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\ttext-align: left;\n\tpadding: 60px;\n\t/* border: 1px solid yellow; */\n\tmargin-top: 140px;\n}\n.rewarded_content[data-v-e7d1e6ca],\n.rewarded_image[data-v-e7d1e6ca] {\n\twidth: 50%;\n\theight: 450px;\n}\n.rewarded_content[data-v-e7d1e6ca] {\n\tpadding:150px 60px;\n}\n.rewarded_image[data-v-e7d1e6ca] {\n\t/* border-radius: 50%; */\n\n\tbackground-position: center !important;\n\tbackground-origin: border-box !important;\n\tbackground-size: cover !important;\n\toverflow: hidden;\n}\n.rewarded_image img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 100%;\n\tborder-radius: 10px;\n}\n.fifth_content[data-v-e7d1e6ca] {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t/* border: 1px solid brown; */\n\tmargin-top: 100px;\n}\n.love_writing[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 200PX;\n    padding:20px 30px;\n   -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n           box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n\t/* border: 1px solid brown; */\n}\n.sixth_content[data-v-e7d1e6ca] {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t/* border: 1px solid firebrick; */\n\tpadding: 60px 50px;\n\theight: 450px;\n\tposition: relative;\n}\n.ideas_content[data-v-e7d1e6ca] {\n\tposition: absolute;\n\twidth: 50%;\n\t/* border: 1px solid firebrick; */\n\theight: auto;\n\tright: 0;\n\tpadding: 60px;\n\ttext-align: left;\n}\n.ideas_content_icons[data-v-e7d1e6ca] {\n\tposition: absolute;\n\tpadding: 20px;\n\tbackground: #a4c2db;\n\twidth: 50%;\n\t/* border: 1px solid firebrick; */\n\theight: auto;\n\tleft: 0;\n}\n.ideas_single[data-v-e7d1e6ca] {\n\tmargin-bottom: 20px;\n\ttext-align: left;\n}\n.ideas_container[data-v-e7d1e6ca] {\n\tpadding: 10px;\n\ttext-align: right;\n\tfont-weight: bolder;\n}\n.sixth_content .line_contentt[data-v-e7d1e6ca] {\n\tborder-left: 1px solid black;\n\tright: 471px !important;\n\tbottom: -155px !important;\n\theight: 310px;\n\tposition: absolute;\n}\n.seventh_content[data-v-e7d1e6ca] {\n\ttext-align: left;\n\t/* border: 1px solid rebeccapurple; */\n\twidth: 70%;\n\tmargin-top: 140px;\n\tpadding-top: 50px;\n\tpadding-bottom: 70px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n.seventh_text[data-v-e7d1e6ca]{\n    line-height: 1.6;\n}\n.crave_smart[data-v-e7d1e6ca],\n.crave_image[data-v-e7d1e6ca] {\n\twidth: 50%;\n}\n.crave_image[data-v-e7d1e6ca] {\n\t/* border-radius: 50%; */\n\tpadding: 0 20px;\n\tbackground-position: center !important;\n\tbackground-origin: border-box !important;\n\tbackground-size: cover !important;\n\toverflow: hidden;\n}\n.crave_image img[data-v-e7d1e6ca] {\n\twidth: 100%;\n\theight: 100%;\n\tborder-radius: 10px;\n}\n.eight_content[data-v-e7d1e6ca] {\n\tposition: relative;\n\twidth: 70%;\n\tbackground: hsl(0, 8%, 98%);\n\tpadding: 20px;\n\tpadding-top: 60px;\n\tpadding-bottom: 100px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n.recent_content[data-v-e7d1e6ca] {\n\n\theight: auto;\n    margin-bottom: 20px;\n    text-align:left;\n}\n.ninth_content[data-v-e7d1e6ca] {\n\tbackground: #a4c2db;\n\tpadding: 80px;\n\tpadding-top: 20px;\n\tpadding-bottom: 100px;\n\tmargin-left: auto;\n\tmargin-right: auto;\n}\n.worthy_content[data-v-e7d1e6ca] {\n\twidth: 100%;\n}\n@media (max-width: 767px) {\n.main_content[data-v-e7d1e6ca] {\n\t\tpadding-top: 47px;\n}\n.desktop[data-v-e7d1e6ca] {\n\t\tdisplay: none;\n}\n.mobile[data-v-e7d1e6ca] {\n\t\tdisplay: block;\n}\n.line_content[data-v-e7d1e6ca] {\n\t\tdisplay: none;\n}\n.blank_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\tposition: relative !important;\n\t\tpadding-top: 0 !important;\n\t\tpadding-bottom: 60px !important;\n\t\tmargin-left: auto !important;\n\t\tmargin-right: auto !important;\n}\n.publish_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\tposition: relative;\n\t\t/* border: 1px solid red; */\n\t\theight: 400px;\n\t\tbackground: #e8f3ec;\n\t\t/* margin: 0 60px; */\n\t\tmargin-top: 0 !important;\n\n\t\tpadding: 30px !important;\n\t\tpadding-bottom: 100px !important;\n\t\t-webkit-box-sizing: border-box !important;\n\t\t        box-sizing: border-box !important;\n\t\tz-index: 100 !important;\n}\n.first_image[data-v-e7d1e6ca] {\n\t\twidth: 40%;\n\t\tz-index: 100 !important;\n\t\theight: 230px;\n\t\tbottom: 0 !important;\n}\n.second_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\t/* border: 1px solid green; */\n\t\tmargin-top: 40px;\n\t\tpadding-top: 0 !important;\n\t\tpadding-bottom: 10px !important;\n\t\tmargin-left: auto !important;\n\t\tmargin-right: auto !important;\n\t\tmargin-bottom: 5px !important;\n\t\t-webkit-box-sizing: border-box !important;\n\t\t        box-sizing: border-box !important;\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t\t-webkit-box-align: end;\n\t\t    -ms-flex-align: end;\n\t\t        align-items: flex-end;\n\t\tvertical-align: top;\n}\n.believe_image[data-v-e7d1e6ca],\n\t.believe_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n}\n.believe_image[data-v-e7d1e6ca] {\n\t\ttext-align: center !important;\n\t\theight: auto;\n}\n.believe_image img[data-v-e7d1e6ca] {\n\t\twidth: 200px;\n\t\theight: 100%;\n\t\tpadding: 20px;\n}\n.extra_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n}\n.third_content[data-v-e7d1e6ca] {\n\t\tpadding: 10px;\n\t\t/* border: 1px solid blue; */\n}\n.works_content[data-v-e7d1e6ca] {\n\t\t/* border: 1px solid blue; */\n\t\theight: auto;\n\t\tpadding: 10px;\n}\n.fourth_content[data-v-e7d1e6ca] {\n\t\tdisplay: block;\n\t\tpadding: 10px;\n\t\t/* border: 1px solid yellow; */\n\t\tmargin-top: 40px;\n}\n.rewarded_content[data-v-e7d1e6ca],\n\t.rewarded_image[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\theight: auto;\n\t\tpadding: 20px;\n}\n.fifth_content[data-v-e7d1e6ca] {\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t\t/* border: 1px solid brown; */\n\t\tmargin-top: 50px;\n}\n.love_writing[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\theight: auto;\n\t\t/* border: 1px solid brown; */\n}\n.sixth_content[data-v-e7d1e6ca] {\n\t\tdisplay: -webkit-box;\n\t\tdisplay: -ms-flexbox;\n\t\tdisplay: flex;\n\t\t/* border: 1px solid firebrick; */\n\t\tpadding: 0;\n\t\theight: auto;\n}\n.ideas_content[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\t/* border: 1px solid firebrick; */\n\t\theight: auto;\n\t\tposition: relative;\n}\n.ideas_content_icons[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n\t\t/* border: 1px solid firebrick; */\n\t\theight: auto;\n\t\tposition: relative;\n}\n.seventh_content[data-v-e7d1e6ca] {\n\t\t/* border: 1px solid rebeccapurple; */\n\t\twidth: 100%;\n\t\tmargin-top: 10px;\n\t\tpadding: 20px;\n\t\tpadding-top: 20px;\n\t\tpadding-bottom: 20px;\n\t\tmargin-left: auto;\n\t\tmargin-right: auto;\n}\n.crave_smart[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n}\n.crave_image[data-v-e7d1e6ca] {\n\t\twidth: 100%;\n}\n.eight_content[data-v-e7d1e6ca] {\n\t\t/* border: 1px solid darkblue; */\n\t\twidth: 100%;\n\t\tmargin-top: 50px;\n\t\tpadding: 20px;\n\t\tpadding-top: 50px;\n\t\tpadding-bottom: 50px;\n\t\tmargin-left: auto;\n        margin-right: auto;\n}\n.recent_content[data-v-e7d1e6ca] {\n\t\t/* border: 1px solid saddlebrown; */\n\t\theight: 150px;\n}\n.ninth_content[data-v-e7d1e6ca] {\n\t\t/* border: 1px solid gold; */\n\t\tpadding: 20px;\n\t\tpadding-top: 20px;\n\t\tpadding-bottom: 20px;\n\t\tmargin-left: auto;\n\t\tmargin-right: auto;\n}\n@media(max-width:768px){\n.work_container[data-v-e7d1e6ca]{\n            width:100px;\n            height:100px;\n}\n.works_content[data-v-e7d1e6ca]{\n            padding:10px 0;\n}\n.sixth_content .line_contentt[data-v-e7d1e6ca]{\n            display:none;\n}\n.eight_content[data-v-e7d1e6ca]{\n            width:90%;\n}\n}\n@media (max-width: 425px) {\n.main_content[data-v-e7d1e6ca] {\n\t\t\tpadding-top: 26px;\n}\n.VueCarousel-pagination[data-v-e7d1e6ca]{\n            background: #ced8e0 !important;\n}\n.love_writing[data-v-e7d1e6ca]{\n            -webkit-box-shadow: none;\n                    box-shadow: none;\n}\n.first_image[data-v-e7d1e6ca] {\n\t\t\twidth: 70%;\n\t\t\tz-index: 100 !important;\n\t\t\theight: 200px;\n\t\t\tbottom: 0 !important;\n}\n.publish_content[data-v-e7d1e6ca] {\n\t\t\twidth: 100%;\n\t\t\tposition: relative;\n\t\t\t/* border: 1px solid red; */\n\t\t\theight: 400px;\n\t\t\tbackground: #e8f3ec;\n\t\t\t/* margin: 0 60px; */\n\t\t\tmargin-top: 0 !important;\n\n\t\t\tpadding: 10px !important;\n\t\t\tpadding-bottom: 100px !important;\n\t\t\t-webkit-box-sizing: border-box !important;\n\t\t\t        box-sizing: border-box !important;\n\t\t\tz-index: 100 !important;\n}\n.work_container[data-v-e7d1e6ca] {\n\t\t\twidth: 120px;\n\t\t\theight: 120px;\n}\n.third_content[data-v-e7d1e6ca]{\n            padding: 10px 50px;\n}\n\n\t\t/* .believe_image img {\n\twidth: 200px;\n\theight: 200px;\n\tpadding:20px;\n} */\n}\n@media (max-width: 375px) {\n.first_image[data-v-e7d1e6ca] {\n\t\t\twidth: 80%;\n\t\t\tz-index: 100 !important;\n\t\t\theight: 180px;\n\t\t\tbottom: 0 !important;\n}\n.work_container[data-v-e7d1e6ca] {\n\t\t\twidth: 165px;\n\t\t\theight: 165px;\n}\n.love_writing[data-v-e7d1e6ca] {\n\t\t\tpadding: 20px 60px;\n}\n}\n@media (max-width: 320px) {\n.main_content[data-v-e7d1e6ca] {\n\t\t\tpadding-top: 31px;\n}\n.work_container[data-v-e7d1e6ca] {\n\t\t\twidth: 150px;\n\t\t\theight: 150px;\n}\n.first_image[data-v-e7d1e6ca] {\n\t\t\theight: 150px;\n}\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1607:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_carousel__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_carousel___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_carousel__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "community-component",
  data: function data() {
    return { breadcrumbList: [] };
  },

  components: {
    Carousel: __WEBPACK_IMPORTED_MODULE_0_vue_carousel__["Carousel"],
    Slide: __WEBPACK_IMPORTED_MODULE_0_vue_carousel__["Slide"]
  },
  mounted: function mounted() {
    this.updateList();
  },

  watch: {
    '$route': function $route() {
      this.updateList();
    }
  },
  methods: {
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    }
  }
});

/***/ }),

/***/ 1608:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main_content" }, [
    _c(
      "ul",
      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
        return _c(
          "li",
          {
            key: index,
            class: { linked: !!breadcrumb.link },
            on: {
              click: function($event) {
                return _vm.routeTo(index)
              }
            }
          },
          [
            _vm._v(
              "\n               " + _vm._s(breadcrumb.name) + "\n             "
            )
          ]
        )
      }),
      0
    ),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("div", { staticClass: "row extra_content" }, [
      _c(
        "div",
        { staticClass: "instructor_content" },
        [
          _c(
            "h2",
            { staticClass: "subHeaders", staticStyle: { width: "100%" } },
            [_vm._v("\n\t\t\t\t\tBIZGURUH PARTNER FORUM (BPF)\n\t\t\t\t")]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "extra_text" }, [
            _vm._v(
              "\n\t\t\t\t\tBPF is an open forum of stakeholders of the informal sector\n\t\t\t\t\tthat includes; regulatory institutions, academic\n\t\t\t\t\tinstitutions, business proprietors and consumers. We\n\t\t\t\t\tfacilitate connection, education, shared insight and\n\t\t\t\t\tcollaboration amongst stakeholders, with the aim of\n\t\t\t\t\tenhancing the ecosystem. Stakeholders are broadly\n\t\t\t\t\tcategorized: Sponsor, Expert, Instructor, Contributor and\n\t\t\t\t\tCommunity Member. BPF membership is open to all with a\n\t\t\t\t\tgenuine desire to see the African informal sector thrive. By\n\t\t\t\t\tbecoming a BPF Member, you are guaranteed: ⁃ A platform for\n\t\t\t\t\tbroader impact leading to a positive change in Africa’s\n\t\t\t\t\tsocioeconomic narrative, through her enterprises ⁃ Technical\n\t\t\t\t\tsupport empowering you to maximize your knowledge ⁃ Get\n\t\t\t\t\tpaid- just for sharing ⁃ Brand exposure to a wider network ⁃\n\t\t\t\t\tCost savings on market research and enhanced marketing ⁃\n\t\t\t\t\tFront row pass at the forum of the new elite business\n\t\t\t\t\teducators.\n                    "
            )
          ]),
          _vm._v(" "),
          _c("router-link", { attrs: { to: "/vendor/auth" } }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary btn-sm ",
                attrs: { type: "button" }
              },
              [_vm._v("Partner With Us")]
            )
          ])
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row third_content" }, [
      _c("h2", { staticClass: "subHeaders", staticStyle: { width: "100%" } }, [
        _vm._v("How it works")
      ]),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "mobile" },
        [
          _c(
            "carousel",
            {
              attrs: {
                perPageCustom: [
                  [320, 1],
                  [375, 1],
                  [425, 1],
                  [768, 2]
                ],
                navigationEnabled: false,
                paginationEnabled: true,
                loop: true
              }
            },
            [
              _c("slide", {}, [
                _c("div", { staticClass: " works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/share.jpg", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v("Share something good")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "third_text" }, [
                    _vm._v(
                      "Share your experiences as an African\n\t\t\t\t\tentrepreneur and thoughts about something that matters to\n\t\t\t\t\tyou as a business owner or expert, in video, audio or text\n\t\t\t\t\tformat. It can be short or long, serious or funny, reported\n\t\t\t\t\tor opinionated - it’s the quality of your perspective that\n\t\t\t\t\tcounts."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/work.jpg", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v(" Put your story to work")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "third_text" }, [
                    _vm._v(
                      " If you’d like to earn money through\n\t\t\t\t\tthe BizGister Program, make sure that you indicate this in\n\t\t\t\t\tthe available fields on your upload form when you publish."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/referral.png", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v("Get recommended to readers")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "third_text" }, [
                    _vm._v(
                      " Our editorial team reviews a ton\n\t\t\t\t\tof iBizGists every day. When a story meets our editorial\n\t\t\t\t\tstandards and endorsed by our subject matter experts, it\n\t\t\t\t\twill be recommended to readers interested in relevant topics\n\t\t\t\t\tacross BizGuruh.com, our app, and email digests."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/makemoney.jpg", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v("\tEarn for your work")
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "If you’re in the BizGister Program,\n\t\t\t\t\tyou’ll earn money when subscribing members read, bookmark or\n\t\t\t\t\tarchive your work on their BizLibrary"
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/earn.jpg", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v("Get paid monthly ")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "third_text" }, [
                    _vm._v(
                      "At the end of every calendar month, we’ll\n\t\t\t\t\tdeposit what you’ve earned into your bank account."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: " works_content " }, [
                  _c("div", { staticClass: "work_container" }, [
                    _c("img", {
                      attrs: { src: "/images/expand.png", alt: "believe" }
                    })
                  ]),
                  _vm._v(" "),
                  _c("h5", { staticClass: "miniHeaders" }, [
                    _vm._v("Expand your BizTribe")
                  ]),
                  _vm._v(" "),
                  _c("p", { staticClass: "third_text" }, [
                    _vm._v(
                      " Your story doesn’t end here. Keep\n\t\t\t\t\tsharing your business experiences and perspective to build\n\t\t\t\t\tand broaden your BizTribe. We’ll help spread the word too."
                    )
                  ])
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "line_content" })
    ]),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c("div", { staticClass: "row extra_content bgLightBlue" }, [
      _c("div", { staticClass: "instructor_content" }, [
        _c(
          "h2",
          { staticClass: "subHeaders", staticStyle: { width: "100%" } },
          [_vm._v("\n\t\t\t\t\tBECOME A BIZGURUH INSTRUCTOR\n\t\t\t\t")]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "extra_text" }),
        _vm._m(4),
        _vm._v(" "),
        _vm._m(5),
        _vm._v(" "),
        _c("h4", { staticClass: "instruct_main" }, [
          _vm._v("Planning your course")
        ]),
        _vm._v(" "),
        _vm._m(6),
        _vm._v(" "),
        _vm._m(7),
        _vm._v(" "),
        _vm._m(8),
        _vm._v(" "),
        _vm._m(9),
        _vm._v(" "),
        _c("p"),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("router-link", { attrs: { to: "/vendor/auth" } }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-lg ",
                  attrs: { type: "button" }
                },
                [_vm._v("Partner With Us")]
              )
            ])
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row fifth_content" }, [
      _c("h2", { staticClass: "subHeaders", staticStyle: { width: "100%" } }, [
        _vm._v("\n\t\t\t\tWhy people love our BizTribe\n\t\t\t")
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "cont" },
        [
          _c(
            "carousel",
            {
              attrs: {
                perPageCustom: [
                  [320, 1],
                  [375, 1],
                  [425, 1],
                  [768, 3]
                ],
                navigationEnabled: false,
                paginationEnabled: true,
                loop: true
              }
            },
            [
              _c("slide", {}, [
                _c("div", { staticClass: "love_writing" }, [
                  _c("div", { staticClass: "d-flex  mb-2" }, [
                    _c("div", { staticClass: "love_img" }, [
                      _c("img", {
                        attrs: {
                          src: "/images/profile.png",
                          alt: "",
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "love_name" }, [
                      _vm._v("Oladunni Omotola")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", {}, [
                    _vm._v(
                      " I am here because @freesiafoodies asked us to, but seriously I must say it is worth it."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "love_writing" }, [
                  _c("div", { staticClass: "d-flex  mb-2" }, [
                    _c("div", { staticClass: "love_img" }, [
                      _c("img", {
                        attrs: {
                          src: "/images/profile.png",
                          alt: "",
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "love_name" }, [
                      _vm._v("Tinuke Adegboye")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "love_text" }, [
                    _vm._v(
                      " I heard about your page from Nwanyiakamu and I followed you. Your content is very educative and qualitative always. Keep it up and thank you."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "love_writing" }, [
                  _c("div", { staticClass: "d-flex  mb-2" }, [
                    _c("div", { staticClass: "love_img" }, [
                      _c("img", {
                        attrs: {
                          src: "/images/profile.png",
                          alt: "",
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "love_name" }, [
                      _vm._v("@theefosa")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "love_text" }, [
                    _vm._v(
                      "   I find your insights very inspiring, I stopped dreaming and started doing."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "love_writing" }, [
                  _c("div", { staticClass: "d-flex  mb-2" }, [
                    _c("div", { staticClass: "love_img" }, [
                      _c("img", {
                        attrs: {
                          src: "/images/profile.png",
                          alt: "",
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "love_name" }, [_vm._v("@oilbyo")])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "love_text" }, [
                    _vm._v(
                      " The post on registering ones business was particularly beneficial. I’m currently saving towards it and the post on affirmations daily no matter how business is going."
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("slide", {}, [
                _c("div", { staticClass: "love_writing" }, [
                  _c("div", { staticClass: "d-flex  mb-2" }, [
                    _c("div", { staticClass: "love_img" }, [
                      _c("img", {
                        attrs: {
                          src: "/images/profile.png",
                          alt: "",
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "love_name" }, [
                      _vm._v("@thattokelady")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "love_text" }, [
                    _vm._v(
                      " I am applying your insights in the growth of my business. Keep sharing them.\n"
                    )
                  ])
                ])
              ])
            ],
            1
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _vm._m(10),
    _vm._v(" "),
    _vm._m(11),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "row eight_content" }, [
      _c(
        "h4",
        {
          staticClass: "rbgist mb-4",
          staticStyle: { width: "100%", "text-align": "left" }
        },
        [
          _vm._v(
            "\n\t\t\tDid you know? We provide answers to your most pressing business-related questions. Here are some of the questions from our tribe members:\n\t\t\t"
          )
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "desktop col-lg-12 col-md-12 col-sm-12 " }, [
        _vm._m(12),
        _vm._v(" "),
        _vm._m(13),
        _vm._v(" "),
        _vm._m(14),
        _vm._v(" "),
        _vm._m(15),
        _vm._v(" "),
        _vm._m(16),
        _vm._v(" "),
        _vm._m(17),
        _vm._v(" "),
        _vm._m(18),
        _vm._v(" "),
        _c(
          "p",
          { staticClass: " col-lg-12 col-md-12 col-sm-12 mt-4" },
          [
            _c("span", { staticStyle: { "padding-right": "5px" } }, [
              _vm._v("Go On— Ask! ")
            ]),
            _vm._v(" "),
            _c("router-link", { attrs: { to: "/ask-bizguruh" } }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-lg ",
                  attrs: { type: "button" }
                },
                [_vm._v("Ask Bizguruh")]
              )
            ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "mobile" },
        [
          _c(
            "carousel",
            {
              attrs: {
                perPageCustom: [
                  [320, 1],
                  [375, 1],
                  [425, 2],
                  [768, 2]
                ],
                navigationEnabled: false,
                paginationEnabled: true,
                loop: true
              }
            },
            [
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\tI would like to know how I would leverage social media to generate more customers for my online business— "
                ),
                _c("strong", [_vm._v("Osas")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\tHow can I improve brand visibility and product sales? — "
                ),
                _c("strong", [_vm._v("Adedunni")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\tI would like to know how to leverage my social media to work with brands and get more partnerships— "
                ),
                _c("strong", [_vm._v("Toke")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\t\tWhat do you do when you need money for personal runs and its only your business account that can save you. Do you borrow from the business account and return or let it slide?— "
                ),
                _c("strong", [_vm._v("@mzpotable")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\tHow do I determine how much to pay myself monthly as a business owner?— "
                ),
                _c("strong", [_vm._v("Adaobi")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\tI’m interested in building a career in business strategy and marketing. How do I go about this?— "
                ),
                _c("strong", [_vm._v("Benedict")])
              ]),
              _vm._v(" "),
              _c("slide", { staticStyle: { "line-height": "1.6" } }, [
                _vm._v(
                  "\n\t\t\t\t\t\t\tHow do I manage my emotions about my business competitor. My stomach knots when I see another company entering the market and doing what I’m about to do.— "
                ),
                _c("strong", [_vm._v("Ugochi")])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "p",
            { staticClass: "mt-4" },
            [
              _c("span", { staticStyle: { "padding-right": "5px" } }, [
                _vm._v("Go On— Ask! ")
              ]),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "/ask-bizguruh" } }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-lg ",
                    attrs: { type: "button" }
                  },
                  [_vm._v("Ask Bizguruh")]
                )
              ])
            ],
            1
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row ninth_content" }, [
      _c(
        "h2",
        { staticClass: "yinsights subHeaders", staticStyle: { width: "100%" } },
        [_vm._v("\n\t\t\t\tYOUR INSIGHT IS DEFINITELY WORTHY\n\t\t\t")]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "worthy_content" }, [
        _c("div", { staticClass: "final_button" }, [
          _c(
            "div",
            { staticClass: " col-lg-12 col-md-12 col-sm-12 col-xs-12" },
            [
              _vm._m(19),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-lg-6 col-md-6 col-sm-6 col-xs-12 p-2 " },
                [
                  _c("router-link", { attrs: { to: "/vendor/auth" } }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary btn-lg   btn-block",
                        attrs: { type: "button" }
                      },
                      [_vm._v("Become a BizInstructor")]
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "col-lg-6 col-md-6 col-sm-6 col-xs-12 p-2" },
                [
                  _c("router-link", { attrs: { to: "/auth" } }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary btn-lg  btn-block",
                        attrs: { type: "button" }
                      },
                      [_vm._v("  Join\n\t\t\t\t\tOur BizTribe")]
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-2 p-2"
                },
                [
                  _c("router-link", { attrs: { to: "/explore" } }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary btn-lg  btn-block",
                        attrs: { type: "button" }
                      },
                      [_vm._v("Explore BizGuruh")]
                    )
                  ])
                ],
                1
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row first_content" }, [
      _c("div", { staticClass: " blank_content" }, [
        _c("div", { staticClass: "publish_content" }, [
          _c("h2", { staticClass: "headers", staticStyle: { width: "100%" } }, [
            _vm._v(
              "\n\t\t\t\t\t\tBPF: A Place To Share Your African Business Insight\n\t\t\t\t\t"
            )
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "first_text" }, [
            _vm._v(
              "\n\t\t\t\t\t\tWe’ve created a home for all stakeholders of Africa’s\n\t\t\t\t\t\tinformal sector to converge. Industry leaders, experts\n\t\t\t\t\t\tand emerging entrepreneurs can now put out their\n\t\t\t\t\t\tbusiness insight and enrich each other’s perspective.\n\t\t\t\t\t\tFor us, it’s really about connecting, learning and\n\t\t\t\t\t\tcollaborating on changing Africa’s socioeconomic\n\t\t\t\t\t\tnarrative through her enterprises.\n\t\t\t\t\t"
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: " first_image" }, [
        _c("img", { attrs: { src: "/images/first.jpg", alt: "first" } })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "line_content" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row second_content" }, [
      _c("div", { staticClass: " believe_content" }, [
        _c(
          "h2",
          { staticClass: "subHeaders", staticStyle: { width: "100%" } },
          [
            _vm._v(
              "\n\t\t\t\t\tWe believe we all offer valuable insight\n\t\t\t\t"
            )
          ]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "second_text" }, [
          _vm._v(
            "\n\t\t\t\t\tWe believe we all have valuable insight to offer- old and\n\t\t\t\t\tnew, established and emerging. So anyone who publishes here\n\t\t\t\t\tcan also join the BizGister Forum and earn money for sharing\n\t\t\t\t\ttheir business insights and expertise with the world.\n\t\t\t\t\tInsights are rewarded by readers who believe in the quality\n\t\t\t\t\tof the idea, not just the attention they attract for\n\t\t\t\t\tadvertisers. So you can focus on what matters— putting only\n\t\t\t\t\tyour best insight out there.\n                    "
          )
        ]),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "btn btn-primary btn-sm ", attrs: { type: "button" } },
          [_vm._v("Join the BizGister\n\t\t\t\t\tForum ")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: " believe_image" }, [
        _c("img", { attrs: { src: "/images/insights.jpg", alt: "believe" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "desktop", staticStyle: { width: "100%" } },
      [
        _c(
          "div",
          {
            staticClass: " works_content col-lg-4 col-md-4 col-sm-4 col-xs-2 "
          },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", { attrs: { src: "/images/share.jpg", alt: "believe" } })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("Share something good")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                "Share your experiences as an African\n\t\t\t\t\tentrepreneur and thoughts about something that matters to\n\t\t\t\t\tyou as a business owner or expert, in video, audio or text\n\t\t\t\t\tformat. It can be short or long, serious or funny, reported\n\t\t\t\t\tor opinionated - it’s the quality of your perspective that\n\t\t\t\t\tcounts."
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "works_content col-lg-4 col-md-4 col-sm-4  col-xs-2" },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", { attrs: { src: "/images/work.jpg", alt: "believe" } })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("Put your story to work")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                " If you’d like to earn money through\n\t\t\t\t\tthe BizGister Program, make sure that you indicate this in\n\t\t\t\t\tthe available fields on your upload form when you publish."
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "works_content col-lg-4 col-md-4 col-sm-4  col-xs-2" },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", {
                attrs: { src: "/images/referral.png", alt: "believe" }
              })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("Get recommended to readers")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                " Our editorial team reviews a ton\n\t\t\t\t\tof iBizGists every day. When a story meets our editorial\n\t\t\t\t\tstandards and endorsed by our subject matter experts, it\n\t\t\t\t\twill be recommended to readers interested in relevant topics\n\t\t\t\t\tacross BizGuruh.com, our app, and email digests."
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "works_content col-lg-4 col-md-4 col-sm-4  col-xs-2" },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", {
                attrs: { src: "/images/makemoney.jpg", alt: "believe" }
              })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("\tEarn for your work")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                "If you’re in the BizGister Program,\n\t\t\t\t\tyou’ll earn money when subscribing members read, bookmark or\n\t\t\t\t\tarchive your work on their BizLibrary"
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "works_content col-lg-4 col-md-4 col-sm-4  col-xs-2" },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", { attrs: { src: "/images/earn.jpg", alt: "believe" } })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("Get paid monthly ")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                "At the end of every calendar month, we’ll\n\t\t\t\t\tdeposit what you’ve earned into your bank account."
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: " works_content col-lg-4 col-md-4 col-sm-4  col-xs-2"
          },
          [
            _c("div", { staticClass: "work_container" }, [
              _c("img", {
                attrs: { src: "/images/expand.png", alt: "believe" }
              })
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("Expand your BizTribe")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "third_text" }, [
              _vm._v(
                " Your story doesn’t end here. Keep\n\t\t\t\t\tsharing your business experiences and perspective to build\n\t\t\t\t\tand broaden your BizTribe. We’ll help spread the word too."
              )
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row fourth_content" }, [
      _c("div", { staticClass: "rewarded_content" }, [
        _c(
          "h2",
          { staticClass: "subHeaders", staticStyle: { width: "100%" } },
          [
            _vm._v(
              "\n\t\t\t\t\tYou’re rewarded by our BizTribe, not advertisers\n\t\t\t\t"
            )
          ]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "fourth_text" }, [
          _vm._v(
            "In\tthe\tBizGist\tProgram,\tcontributors\tare\tpaid\tbased\ton\thow\tdeeply\tothers\tengage\twith\ttheir\tperspective,\tnot\tbased\ton\tclicks\tor\tattention.\tEvery\tmonth,\twe\tdistribute\teach\tmember’s\tsubscription\tfee\tin\tproportion\tto\tthe\tcontributors\tand\tstories\tshe\tengaged\twith\tthat\tmonth"
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "rewarded_image" }, [
        _c("img", { attrs: { src: "/images/tribe.jpg", alt: "readers" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [
        _vm._v("Make a global impact")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Turn what you know into an opportunity.\n\t\t\t\t\tCreate online courses and earn money by teaching people\n\t\t\t\t\taround the world how to do business in Africa."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [_vm._v(" HOW IT WORKS")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Plan your course ==> Record and collate your resources ==>\n\t\t\t\t\tBuild your student-community"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [_vm._v(" Get organized")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "  Start with your passion and knowledge. Then choose\n\t\t\t\t\ta topic and plan your lectures in Google Docs, Microsoft\n\t\t\t\t\tExcel, or your favorite notebook. You get to teach the way\n\t\t\t\t\tyou want — even create courses in multiple languages and\n\t\t\t\t\tinspire more students."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [_vm._v("How we help you")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " BizGuruh offers free courses on how to build your own course, complete with\n\t\t\t\t\tworksheets and real-world examples. Plus, our instructor\n\t\t\t\t\tdashboard and curriculum pages help keep you on track."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [_vm._v("WE ARE HERE TO HELP")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " Our BizInstructor Support Team is here for\n                       you 24/7 to help you through your course creation needs.\n\t\t\t\t\tContact us directly or join BizInstructor Facebook Forum and\n\t\t\t\t\tget peer-to-peer support from our engaged instructor\n\t\t\t\t\tcommunity. This Facebook group is always on, always there,\n\t\t\t\t\tand always helpful."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "instruct" }, [
      _c("h5", { staticClass: "miniHeaders" }, [
        _vm._v("Become an instructor today")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " Let’s build\n\t\t\t\t\tAfrica’s largest online business learning marketplace.\n                    "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row sixth_content" }, [
      _c("div", { staticClass: "ideas_content" }, [
        _c(
          "h2",
          { staticClass: "subHeaders", staticStyle: { width: "100%" } },
          [_vm._v("\n\t\t\t\t\tWe love quality BizGist\n\t\t\t\t")]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "sixth_text" }, [
          _vm._v(
            "\n\t\t\t\t\tInsights published on BizGuruh should foster thinking that\n\t\t\t\t\teducates, inspires, and moves understanding forward. We’ve\n\t\t\t\t\tcreated some best practices to ensure everyone’s on the same\n\t\t\t\t\tpage.\n                    "
          )
        ]),
        _c("p", [_c("strong", [_vm._v(" Full editorial guidelines")])]),
        _vm._v(" "),
        _c("p")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ideas_content_icons" }, [
        _c("h2", { staticClass: "beg mb-2", staticStyle: { width: "100%" } }, [
          _vm._v("BIZGURUH EDITORIAL GUIDELINES")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex  ideas_single" }, [
          _c(
            "div",
            { staticClass: "col-lg-3 col-md-3 col-xs-3 ideas_container one" },
            [_vm._v("1")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-9 col-md-9 col-xs-9" }, [
            _c("h5", { staticClass: "miniHeaders" }, [_vm._v("MAKE IT YOURS")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "All writing and images must be your own, or\n\t\t\t\t\tmust be used with permission or citation."
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex  ideas_single" }, [
          _c(
            "div",
            { staticClass: "col-lg-3 col-md- col-xs-3 ideas_container two" },
            [_vm._v("2")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-9 col-md-9 col-xs-9" }, [
            _c("h5", { staticClass: "miniHeaders" }, [
              _vm._v("KEEP IT AD-FREE")
            ]),
            _vm._v(
              "\n                    BizGuruh doesn’t accept advertising. Please\n\t\t\t\t\tdon’t market yourself or other products, feature\n\t\t\t\t\tadvertisements, or include requests for bookmarks,\n\t\t\t\t\tdonations, or email signups.\n\t\t\t\t"
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex  ideas_single" }, [
          _c(
            "div",
            {
              staticClass: "ideas_container  col-lg-3 col-md-3 col-xs-3 three"
            },
            [_vm._v("3")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: " col-lg-9 col-md-9 col-xs-9" }, [
            _c("h5", { staticClass: "miniHeaders" }, [_vm._v("RAISE THE BAR")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Our BizTribe seek valuable and practical\n\t\t\t\t\tinsight. So we recommend publishing only your best business\n\t\t\t\t\texperiences, analysis and ideas."
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "line_contentt" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row seventh_content" }, [
      _c("div", { staticClass: "crave_smart" }, [
        _c(
          "h2",
          { staticClass: "subHeaders", staticStyle: { width: "100%" } },
          [
            _vm._v(
              "\n\t\t\t\t\tOur BizTribe crave smart, insightful thinking from diverse\n\t\t\t\t\tvoices, across subject matters and industries\n\t\t\t\t"
            )
          ]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "seventh_text mb-2" }, [
          _vm._v(
            "\n\t\t\t\t\tIndustry experts and emerging entrepreneurs add color and\n\t\t\t\t\tflair by contributing based on what they know and what they\n\t\t\t\t\tlove. No two insights are exactly alike. So go on and make\n\t\t\t\t\tyour mark!\n\t\t\t\t"
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "crave_image" }, [
        _c("img", { attrs: { src: "/images/thinking.jpg", alt: "thinking" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: " recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\tI would like to know how I would leverage social media to generate more customers for my online business "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Osas")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "recent_content col-lg-3 col-md- col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tI would like to know how to leverage my social media to work with brands and get more partnerships "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Toke")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tHow can I improve brand visibility and product sales? "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Adedunni")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tWhat do you do when you need money for personal runs and its only your business account that can save you. Do you borrow from the business account and return or let it slide? "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("@mzpotable")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: " recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tHow do I determine how much to pay myself monthly as a business owner? "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Adaobi")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: " recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tI’m interested in building a career in business strategy and marketing. How do I go about this? "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Benedict")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: " recent_content col-lg-3 col-md-3 col-sm-3" },
      [
        _vm._v(
          "\n\t\t\t\t\tHow do I manage my emotions about my business competitor. My stomach knots when I see another company entering the market and doing what I’m about to do. "
        ),
        _c("br"),
        _vm._v(" — "),
        _c("strong", [_vm._v("Ugochi")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "col-lg-6 col-md-6 col-sm-6 col-xs-12 p-2 " },
      [
        _c(
          "button",
          {
            staticClass: "btn btn-primary btn-lg btn-block  ",
            attrs: { type: "button" }
          },
          [_vm._v(" Become a BizGister")]
        )
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e7d1e6ca", module.exports)
  }
}

/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

/*!
 * vue-carousel v0.18.0-alpha
 * (c) 2019 todd.beauchamp@ssense.com
 * https://github.com/ssense/vue-carousel#readme
 */
!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.VueCarousel=e():t.VueCarousel=e()}(window,function(){return function(t){var e={};function n(r){if(e[r])return e[r].exports;var i=e[r]={i:r,l:!1,exports:{}};return t[r].call(i.exports,i,i.exports,n),i.l=!0,i.exports}return n.m=t,n.c=e,n.d=function(t,e,r){n.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:r})},n.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},n.t=function(t,e){if(1&e&&(t=n(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var i in t)n.d(r,i,function(e){return t[e]}.bind(null,i));return r},n.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return n.d(e,"a",e),e},n.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},n.p="",n(n.s=53)}([function(t,e,n){var r=n(30)("wks"),i=n(15),o=n(3).Symbol,a="function"==typeof o;(t.exports=function(t){return r[t]||(r[t]=a&&o[t]||(a?o:i)("Symbol."+t))}).store=r},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e,n){var r=n(3),i=n(11),o=n(6),a=n(10),s=n(29),u=function(t,e,n){var c,l,f,d,h=t&u.F,p=t&u.G,g=t&u.S,v=t&u.P,y=t&u.B,m=p?r:g?r[e]||(r[e]={}):(r[e]||{}).prototype,b=p?i:i[e]||(i[e]={}),x=b.prototype||(b.prototype={});for(c in p&&(n=e),n)f=((l=!h&&m&&void 0!==m[c])?m:n)[c],d=y&&l?s(f,r):v&&"function"==typeof f?s(Function.call,f):f,m&&a(m,c,f,t&u.U),b[c]!=f&&o(b,c,d),v&&x[c]!=f&&(x[c]=f)};r.core=i,u.F=1,u.G=2,u.S=4,u.P=8,u.B=16,u.W=32,u.U=64,u.R=128,t.exports=u},function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e,n){var r=n(7),i=n(40),o=n(20),a=Object.defineProperty;e.f=n(5)?Object.defineProperty:function(t,e,n){if(r(t),e=o(e,!0),r(n),i)try{return a(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e,n){t.exports=!n(1)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e,n){var r=n(4),i=n(14);t.exports=n(5)?function(t,e,n){return r.f(t,e,i(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e,n){var r=n(8);t.exports=function(t){if(!r(t))throw TypeError(t+" is not an object!");return t}},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var r=n(3),i=n(6),o=n(9),a=n(15)("src"),s=Function.toString,u=(""+s).split("toString");n(11).inspectSource=function(t){return s.call(t)},(t.exports=function(t,e,n,s){var c="function"==typeof n;c&&(o(n,"name")||i(n,"name",e)),t[e]!==n&&(c&&(o(n,a)||i(n,a,t[e]?""+t[e]:u.join(String(e)))),t===r?t[e]=n:s?t[e]?t[e]=n:i(t,e,n):(delete t[e],i(t,e,n)))})(Function.prototype,"toString",function(){return"function"==typeof this&&this[a]||s.call(this)})},function(t,e){var n=t.exports={version:"2.5.7"};"number"==typeof __e&&(__e=n)},function(t,e,n){var r=n(47),i=n(17);t.exports=function(t){return r(i(t))}},function(t,e,n){var r=n(48),i=n(33);t.exports=Object.keys||function(t){return r(t,i)}},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e){var n=0,r=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+r).toString(36))}},function(t,e,n){var r=n(17);t.exports=function(t){return Object(r(t))}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e){t.exports={}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e,n){var r=n(8);t.exports=function(t,e){if(!r(t))return t;var n,i;if(e&&"function"==typeof(n=t.toString)&&!r(i=n.call(t)))return i;if("function"==typeof(n=t.valueOf)&&!r(i=n.call(t)))return i;if(!e&&"function"==typeof(n=t.toString)&&!r(i=n.call(t)))return i;throw TypeError("Can't convert object to primitive value")}},function(t,e){t.exports=!1},function(t,e){e.f={}.propertyIsEnumerable},function(t,e){t.exports=function(t){var e=[];return e.toString=function(){return this.map(function(e){var n=function(t,e){var n=t[1]||"",r=t[3];if(!r)return n;if(e&&"function"==typeof btoa){var i=function(t){return"/*# sourceMappingURL=data:application/json;charset=utf-8;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(t))))+" */"}(r),o=r.sources.map(function(t){return"/*# sourceURL="+r.sourceRoot+t+" */"});return[n].concat(o).concat([i]).join("\n")}return[n].join("\n")}(e,t);return e[2]?"@media "+e[2]+"{"+n+"}":n}).join("")},e.i=function(t,n){"string"==typeof t&&(t=[[null,t,""]]);for(var r={},i=0;i<this.length;i++){var o=this[i][0];"number"==typeof o&&(r[o]=!0)}for(i=0;i<t.length;i++){var a=t[i];"number"==typeof a[0]&&r[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),e.push(a))}},e}},function(t,e,n){"use strict";function r(t,e){for(var n=[],r={},i=0;i<e.length;i++){var o=e[i],a=o[0],s={id:t+":"+i,css:o[1],media:o[2],sourceMap:o[3]};r[a]?r[a].parts.push(s):n.push(r[a]={id:a,parts:[s]})}return n}n.r(e),n.d(e,"default",function(){return p});var i="undefined"!=typeof document;if("undefined"!=typeof DEBUG&&DEBUG&&!i)throw new Error("vue-style-loader cannot be used in a non-browser environment. Use { target: 'node' } in your Webpack config to indicate a server-rendering environment.");var o={},a=i&&(document.head||document.getElementsByTagName("head")[0]),s=null,u=0,c=!1,l=function(){},f=null,d="data-vue-ssr-id",h="undefined"!=typeof navigator&&/msie [6-9]\b/.test(navigator.userAgent.toLowerCase());function p(t,e,n,i){c=n,f=i||{};var a=r(t,e);return g(a),function(e){for(var n=[],i=0;i<a.length;i++){var s=a[i];(u=o[s.id]).refs--,n.push(u)}for(e?g(a=r(t,e)):a=[],i=0;i<n.length;i++){var u;if(0===(u=n[i]).refs){for(var c=0;c<u.parts.length;c++)u.parts[c]();delete o[u.id]}}}}function g(t){for(var e=0;e<t.length;e++){var n=t[e],r=o[n.id];if(r){r.refs++;for(var i=0;i<r.parts.length;i++)r.parts[i](n.parts[i]);for(;i<n.parts.length;i++)r.parts.push(y(n.parts[i]));r.parts.length>n.parts.length&&(r.parts.length=n.parts.length)}else{var a=[];for(i=0;i<n.parts.length;i++)a.push(y(n.parts[i]));o[n.id]={id:n.id,refs:1,parts:a}}}}function v(){var t=document.createElement("style");return t.type="text/css",a.appendChild(t),t}function y(t){var e,n,r=document.querySelector("style["+d+'~="'+t.id+'"]');if(r){if(c)return l;r.parentNode.removeChild(r)}if(h){var i=u++;r=s||(s=v()),e=b.bind(null,r,i,!1),n=b.bind(null,r,i,!0)}else r=v(),e=function(t,e){var n=e.css,r=e.media,i=e.sourceMap;if(r&&t.setAttribute("media",r),f.ssrId&&t.setAttribute(d,e.id),i&&(n+="\n/*# sourceURL="+i.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(i))))+" */"),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}.bind(null,r),n=function(){r.parentNode.removeChild(r)};return e(t),function(r){if(r){if(r.css===t.css&&r.media===t.media&&r.sourceMap===t.sourceMap)return;e(t=r)}else n()}}var m=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}();function b(t,e,n,r){var i=n?"":r.css;if(t.styleSheet)t.styleSheet.cssText=m(e,i);else{var o=document.createTextNode(i),a=t.childNodes;a[e]&&t.removeChild(a[e]),a.length?t.insertBefore(o,a[e]):t.appendChild(o)}}},function(t,e,n){var r=n(95);"string"==typeof r&&(r=[[t.i,r,""]]),r.locals&&(t.exports=r.locals),(0,n(24).default)("1c9d4ce3",r,!1,{})},function(t,e,n){var r=n(98);"string"==typeof r&&(r=[[t.i,r,""]]),r.locals&&(t.exports=r.locals),(0,n(24).default)("6a175419",r,!1,{})},function(t,e,n){var r=n(100);"string"==typeof r&&(r=[[t.i,r,""]]),r.locals&&(t.exports=r.locals),(0,n(24).default)("07c48036",r,!1,{})},function(t,e,n){var r=n(102);"string"==typeof r&&(r=[[t.i,r,""]]),r.locals&&(t.exports=r.locals),(0,n(24).default)("6eff00d0",r,!1,{})},function(t,e,n){var r=n(39);t.exports=function(t,e,n){if(r(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,r){return t.call(e,n,r)};case 3:return function(n,r,i){return t.call(e,n,r,i)}}return function(){return t.apply(e,arguments)}}},function(t,e,n){var r=n(11),i=n(3),o=i["__core-js_shared__"]||(i["__core-js_shared__"]={});(t.exports=function(t,e){return o[t]||(o[t]=void 0!==e?e:{})})("versions",[]).push({version:r.version,mode:n(21)?"pure":"global",copyright:"© 2018 Denis Pushkarev (zloirock.ru)"})},function(t,e,n){var r=n(7),i=n(67),o=n(33),a=n(32)("IE_PROTO"),s=function(){},u=function(){var t,e=n(41)("iframe"),r=o.length;for(e.style.display="none",n(69).appendChild(e),e.src="javascript:",(t=e.contentWindow.document).open(),t.write("<script>document.F=Object<\/script>"),t.close(),u=t.F;r--;)delete u.prototype[o[r]];return u()};t.exports=Object.create||function(t,e){var n;return null!==t?(s.prototype=r(t),n=new s,s.prototype=null,n[a]=t):n=u(),void 0===e?n:i(n,e)}},function(t,e,n){var r=n(30)("keys"),i=n(15);t.exports=function(t){return r[t]||(r[t]=i(t))}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e,n){var r=n(4).f,i=n(9),o=n(0)("toStringTag");t.exports=function(t,e,n){t&&!i(t=n?t:t.prototype,o)&&r(t,o,{configurable:!0,value:e})}},function(t,e){e.f=Object.getOwnPropertySymbols},function(t,e,n){var r=n(48),i=n(33).concat("length","prototype");e.f=Object.getOwnPropertyNames||function(t){return r(t,i)}},function(t,e,n){var r=n(22),i=n(14),o=n(12),a=n(20),s=n(9),u=n(40),c=Object.getOwnPropertyDescriptor;e.f=n(5)?c:function(t,e){if(t=o(t),e=a(e,!0),u)try{return c(t,e)}catch(t){}if(s(t,e))return i(!r.f.call(t,e),t[e])}},function(t,e,n){"use strict";var r=n(3),i=n(9),o=n(19),a=n(85),s=n(20),u=n(1),c=n(36).f,l=n(37).f,f=n(4).f,d=n(87).trim,h=r.Number,p=h,g=h.prototype,v="Number"==o(n(31)(g)),y="trim"in String.prototype,m=function(t){var e=s(t,!1);if("string"==typeof e&&e.length>2){var n,r,i,o=(e=y?e.trim():d(e,3)).charCodeAt(0);if(43===o||45===o){if(88===(n=e.charCodeAt(2))||120===n)return NaN}else if(48===o){switch(e.charCodeAt(1)){case 66:case 98:r=2,i=49;break;case 79:case 111:r=8,i=55;break;default:return+e}for(var a,u=e.slice(2),c=0,l=u.length;c<l;c++)if((a=u.charCodeAt(c))<48||a>i)return NaN;return parseInt(u,r)}}return+e};if(!h(" 0o1")||!h("0b1")||h("+0x1")){h=function(t){var e=arguments.length<1?0:t,n=this;return n instanceof h&&(v?u(function(){g.valueOf.call(n)}):"Number"!=o(n))?a(new p(m(e)),n,h):m(e)};for(var b,x=n(5)?c(p):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),P=0;x.length>P;P++)i(p,b=x[P])&&!i(h,b)&&f(h,b,l(p,b));h.prototype=g,g.constructor=h,n(10)(r,"Number",h)}},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e,n){t.exports=!n(5)&&!n(1)(function(){return 7!=Object.defineProperty(n(41)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){var r=n(8),i=n(3).document,o=r(i)&&r(i.createElement);t.exports=function(t){return o?i.createElement(t):{}}},function(t,e,n){var r=n(43),i=Math.min;t.exports=function(t){return t>0?i(r(t),9007199254740991):0}},function(t,e){var n=Math.ceil,r=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?r:n)(t)}},function(t,e,n){"use strict";var r=n(7);t.exports=function(){var t=r(this),e="";return t.global&&(e+="g"),t.ignoreCase&&(e+="i"),t.multiline&&(e+="m"),t.unicode&&(e+="u"),t.sticky&&(e+="y"),e}},function(t,e,n){"use strict";var r=n(46),i=n(64),o=n(18),a=n(12);t.exports=n(65)(Array,"Array",function(t,e){this._t=a(t),this._i=0,this._k=e},function(){var t=this._t,e=this._k,n=this._i++;return!t||n>=t.length?(this._t=void 0,i(1)):i(0,"keys"==e?n:"values"==e?t[n]:[n,t[n]])},"values"),o.Arguments=o.Array,r("keys"),r("values"),r("entries")},function(t,e,n){var r=n(0)("unscopables"),i=Array.prototype;void 0==i[r]&&n(6)(i,r,{}),t.exports=function(t){i[r][t]=!0}},function(t,e,n){var r=n(19);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==r(t)?t.split(""):Object(t)}},function(t,e,n){var r=n(9),i=n(12),o=n(49)(!1),a=n(32)("IE_PROTO");t.exports=function(t,e){var n,s=i(t),u=0,c=[];for(n in s)n!=a&&r(s,n)&&c.push(n);for(;e.length>u;)r(s,n=e[u++])&&(~o(c,n)||c.push(n));return c}},function(t,e,n){var r=n(12),i=n(42),o=n(68);t.exports=function(t){return function(e,n,a){var s,u=r(e),c=i(u.length),l=o(a,c);if(t&&n!=n){for(;c>l;)if((s=u[l++])!=s)return!0}else for(;c>l;l++)if((t||l in u)&&u[l]===n)return t||l||0;return!t&&-1}}},function(t,e,n){var r=n(3),i=n(11),o=n(21),a=n(51),s=n(4).f;t.exports=function(t){var e=i.Symbol||(i.Symbol=o?{}:r.Symbol||{});"_"==t.charAt(0)||t in e||s(e,t,{value:a.f(t)})}},function(t,e,n){e.f=n(0)},function(t,e,n){var r=n(2);r(r.S+r.F,"Object",{assign:n(96)})},function(t,e,n){t.exports=n(103)},function(t,e,n){"use strict";var r=n(29),i=n(2),o=n(16),a=n(55),s=n(56),u=n(42),c=n(57),l=n(58);i(i.S+i.F*!n(60)(function(t){Array.from(t)}),"Array",{from:function(t){var e,n,i,f,d=o(t),h="function"==typeof this?this:Array,p=arguments.length,g=p>1?arguments[1]:void 0,v=void 0!==g,y=0,m=l(d);if(v&&(g=r(g,p>2?arguments[2]:void 0,2)),void 0==m||h==Array&&s(m))for(n=new h(e=u(d.length));e>y;y++)c(n,y,v?g(d[y],y):d[y]);else for(f=m.call(d),n=new h;!(i=f.next()).done;y++)c(n,y,v?a(f,g,[i.value,y],!0):i.value);return n.length=y,n}})},function(t,e,n){var r=n(7);t.exports=function(t,e,n,i){try{return i?e(r(n)[0],n[1]):e(n)}catch(e){var o=t.return;throw void 0!==o&&r(o.call(t)),e}}},function(t,e,n){var r=n(18),i=n(0)("iterator"),o=Array.prototype;t.exports=function(t){return void 0!==t&&(r.Array===t||o[i]===t)}},function(t,e,n){"use strict";var r=n(4),i=n(14);t.exports=function(t,e,n){e in t?r.f(t,e,i(0,n)):t[e]=n}},function(t,e,n){var r=n(59),i=n(0)("iterator"),o=n(18);t.exports=n(11).getIteratorMethod=function(t){if(void 0!=t)return t[i]||t["@@iterator"]||o[r(t)]}},function(t,e,n){var r=n(19),i=n(0)("toStringTag"),o="Arguments"==r(function(){return arguments}());t.exports=function(t){var e,n,a;return void 0===t?"Undefined":null===t?"Null":"string"==typeof(n=function(t,e){try{return t[e]}catch(t){}}(e=Object(t),i))?n:o?r(e):"Object"==(a=r(e))&&"function"==typeof e.callee?"Arguments":a}},function(t,e,n){var r=n(0)("iterator"),i=!1;try{var o=[7][r]();o.return=function(){i=!0},Array.from(o,function(){throw 2})}catch(t){}t.exports=function(t,e){if(!e&&!i)return!1;var n=!1;try{var o=[7],a=o[r]();a.next=function(){return{done:n=!0}},o[r]=function(){return a},t(o)}catch(t){}return n}},function(t,e,n){"use strict";n(62);var r=n(7),i=n(44),o=n(5),a=/./.toString,s=function(t){n(10)(RegExp.prototype,"toString",t,!0)};n(1)(function(){return"/a/b"!=a.call({source:"a",flags:"b"})})?s(function(){var t=r(this);return"/".concat(t.source,"/","flags"in t?t.flags:!o&&t instanceof RegExp?i.call(t):void 0)}):"toString"!=a.name&&s(function(){return a.call(this)})},function(t,e,n){n(5)&&"g"!=/./g.flags&&n(4).f(RegExp.prototype,"flags",{configurable:!0,get:n(44)})},function(t,e,n){for(var r=n(45),i=n(13),o=n(10),a=n(3),s=n(6),u=n(18),c=n(0),l=c("iterator"),f=c("toStringTag"),d=u.Array,h={CSSRuleList:!0,CSSStyleDeclaration:!1,CSSValueList:!1,ClientRectList:!1,DOMRectList:!1,DOMStringList:!1,DOMTokenList:!0,DataTransferItemList:!1,FileList:!1,HTMLAllCollection:!1,HTMLCollection:!1,HTMLFormElement:!1,HTMLSelectElement:!1,MediaList:!0,MimeTypeArray:!1,NamedNodeMap:!1,NodeList:!0,PaintRequestList:!1,Plugin:!1,PluginArray:!1,SVGLengthList:!1,SVGNumberList:!1,SVGPathSegList:!1,SVGPointList:!1,SVGStringList:!1,SVGTransformList:!1,SourceBufferList:!1,StyleSheetList:!0,TextTrackCueList:!1,TextTrackList:!1,TouchList:!1},p=i(h),g=0;g<p.length;g++){var v,y=p[g],m=h[y],b=a[y],x=b&&b.prototype;if(x&&(x[l]||s(x,l,d),x[f]||s(x,f,y),u[y]=d,m))for(v in r)x[v]||o(x,v,r[v],!0)}},function(t,e){t.exports=function(t,e){return{value:e,done:!!t}}},function(t,e,n){"use strict";var r=n(21),i=n(2),o=n(10),a=n(6),s=n(18),u=n(66),c=n(34),l=n(70),f=n(0)("iterator"),d=!([].keys&&"next"in[].keys()),h=function(){return this};t.exports=function(t,e,n,p,g,v,y){u(n,e,p);var m,b,x,P=function(t){if(!d&&t in O)return O[t];switch(t){case"keys":case"values":return function(){return new n(this,t)}}return function(){return new n(this,t)}},S=e+" Iterator",C="values"==g,w=!1,O=t.prototype,T=O[f]||O["@@iterator"]||g&&O[g],_=T||P(g),E=g?C?P("entries"):_:void 0,j="Array"==e&&O.entries||T;if(j&&(x=l(j.call(new t)))!==Object.prototype&&x.next&&(c(x,S,!0),r||"function"==typeof x[f]||a(x,f,h)),C&&T&&"values"!==T.name&&(w=!0,_=function(){return T.call(this)}),r&&!y||!d&&!w&&O[f]||a(O,f,_),s[e]=_,s[S]=h,g)if(m={values:C?_:P("values"),keys:v?_:P("keys"),entries:E},y)for(b in m)b in O||o(O,b,m[b]);else i(i.P+i.F*(d||w),e,m);return m}},function(t,e,n){"use strict";var r=n(31),i=n(14),o=n(34),a={};n(6)(a,n(0)("iterator"),function(){return this}),t.exports=function(t,e,n){t.prototype=r(a,{next:i(1,n)}),o(t,e+" Iterator")}},function(t,e,n){var r=n(4),i=n(7),o=n(13);t.exports=n(5)?Object.defineProperties:function(t,e){i(t);for(var n,a=o(e),s=a.length,u=0;s>u;)r.f(t,n=a[u++],e[n]);return t}},function(t,e,n){var r=n(43),i=Math.max,o=Math.min;t.exports=function(t,e){return(t=r(t))<0?i(t+e,0):o(t,e)}},function(t,e,n){var r=n(3).document;t.exports=r&&r.documentElement},function(t,e,n){var r=n(9),i=n(16),o=n(32)("IE_PROTO"),a=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=i(t),r(t,o)?t[o]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?a:null}},function(t,e,n){var r=n(16),i=n(13);n(72)("keys",function(){return function(t){return i(r(t))}})},function(t,e,n){var r=n(2),i=n(11),o=n(1);t.exports=function(t,e){var n=(i.Object||{})[t]||Object[t],a={};a[t]=e(n),r(r.S+r.F*o(function(){n(1)}),"Object",a)}},function(t,e,n){n(50)("asyncIterator")},function(t,e,n){"use strict";var r=n(3),i=n(9),o=n(5),a=n(2),s=n(10),u=n(75).KEY,c=n(1),l=n(30),f=n(34),d=n(15),h=n(0),p=n(51),g=n(50),v=n(76),y=n(77),m=n(7),b=n(8),x=n(12),P=n(20),S=n(14),C=n(31),w=n(78),O=n(37),T=n(4),_=n(13),E=O.f,j=T.f,A=w.f,M=r.Symbol,N=r.JSON,k=N&&N.stringify,L=h("_hidden"),V=h("toPrimitive"),$={}.propertyIsEnumerable,I=l("symbol-registry"),D=l("symbols"),W=l("op-symbols"),F=Object.prototype,R="function"==typeof M,B=r.QObject,H=!B||!B.prototype||!B.prototype.findChild,z=o&&c(function(){return 7!=C(j({},"a",{get:function(){return j(this,"a",{value:7}).a}})).a})?function(t,e,n){var r=E(F,e);r&&delete F[e],j(t,e,n),r&&t!==F&&j(F,e,r)}:j,U=function(t){var e=D[t]=C(M.prototype);return e._k=t,e},X=R&&"symbol"==typeof M.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof M},G=function(t,e,n){return t===F&&G(W,e,n),m(t),e=P(e,!0),m(n),i(D,e)?(n.enumerable?(i(t,L)&&t[L][e]&&(t[L][e]=!1),n=C(n,{enumerable:S(0,!1)})):(i(t,L)||j(t,L,S(1,{})),t[L][e]=!0),z(t,e,n)):j(t,e,n)},Y=function(t,e){m(t);for(var n,r=v(e=x(e)),i=0,o=r.length;o>i;)G(t,n=r[i++],e[n]);return t},q=function(t){var e=$.call(this,t=P(t,!0));return!(this===F&&i(D,t)&&!i(W,t))&&(!(e||!i(this,t)||!i(D,t)||i(this,L)&&this[L][t])||e)},J=function(t,e){if(t=x(t),e=P(e,!0),t!==F||!i(D,e)||i(W,e)){var n=E(t,e);return!n||!i(D,e)||i(t,L)&&t[L][e]||(n.enumerable=!0),n}},K=function(t){for(var e,n=A(x(t)),r=[],o=0;n.length>o;)i(D,e=n[o++])||e==L||e==u||r.push(e);return r},Q=function(t){for(var e,n=t===F,r=A(n?W:x(t)),o=[],a=0;r.length>a;)!i(D,e=r[a++])||n&&!i(F,e)||o.push(D[e]);return o};R||(s((M=function(){if(this instanceof M)throw TypeError("Symbol is not a constructor!");var t=d(arguments.length>0?arguments[0]:void 0),e=function(n){this===F&&e.call(W,n),i(this,L)&&i(this[L],t)&&(this[L][t]=!1),z(this,t,S(1,n))};return o&&H&&z(F,t,{configurable:!0,set:e}),U(t)}).prototype,"toString",function(){return this._k}),O.f=J,T.f=G,n(36).f=w.f=K,n(22).f=q,n(35).f=Q,o&&!n(21)&&s(F,"propertyIsEnumerable",q,!0),p.f=function(t){return U(h(t))}),a(a.G+a.W+a.F*!R,{Symbol:M});for(var Z="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),tt=0;Z.length>tt;)h(Z[tt++]);for(var et=_(h.store),nt=0;et.length>nt;)g(et[nt++]);a(a.S+a.F*!R,"Symbol",{for:function(t){return i(I,t+="")?I[t]:I[t]=M(t)},keyFor:function(t){if(!X(t))throw TypeError(t+" is not a symbol!");for(var e in I)if(I[e]===t)return e},useSetter:function(){H=!0},useSimple:function(){H=!1}}),a(a.S+a.F*!R,"Object",{create:function(t,e){return void 0===e?C(t):Y(C(t),e)},defineProperty:G,defineProperties:Y,getOwnPropertyDescriptor:J,getOwnPropertyNames:K,getOwnPropertySymbols:Q}),N&&a(a.S+a.F*(!R||c(function(){var t=M();return"[null]"!=k([t])||"{}"!=k({a:t})||"{}"!=k(Object(t))})),"JSON",{stringify:function(t){for(var e,n,r=[t],i=1;arguments.length>i;)r.push(arguments[i++]);if(n=e=r[1],(b(e)||void 0!==t)&&!X(t))return y(e)||(e=function(t,e){if("function"==typeof n&&(e=n.call(this,t,e)),!X(e))return e}),r[1]=e,k.apply(N,r)}}),M.prototype[V]||n(6)(M.prototype,V,M.prototype.valueOf),f(M,"Symbol"),f(Math,"Math",!0),f(r.JSON,"JSON",!0)},function(t,e,n){var r=n(15)("meta"),i=n(8),o=n(9),a=n(4).f,s=0,u=Object.isExtensible||function(){return!0},c=!n(1)(function(){return u(Object.preventExtensions({}))}),l=function(t){a(t,r,{value:{i:"O"+ ++s,w:{}}})},f=t.exports={KEY:r,NEED:!1,fastKey:function(t,e){if(!i(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!o(t,r)){if(!u(t))return"F";if(!e)return"E";l(t)}return t[r].i},getWeak:function(t,e){if(!o(t,r)){if(!u(t))return!0;if(!e)return!1;l(t)}return t[r].w},onFreeze:function(t){return c&&f.NEED&&u(t)&&!o(t,r)&&l(t),t}}},function(t,e,n){var r=n(13),i=n(35),o=n(22);t.exports=function(t){var e=r(t),n=i.f;if(n)for(var a,s=n(t),u=o.f,c=0;s.length>c;)u.call(t,a=s[c++])&&e.push(a);return e}},function(t,e,n){var r=n(19);t.exports=Array.isArray||function(t){return"Array"==r(t)}},function(t,e,n){var r=n(12),i=n(36).f,o={}.toString,a="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[];t.exports.f=function(t){return a&&"[object Window]"==o.call(t)?function(t){try{return i(t)}catch(t){return a.slice()}}(t):i(r(t))}},function(t,e,n){var r=n(2);r(r.S,"Math",{sign:n(80)})},function(t,e){t.exports=Math.sign||function(t){return 0==(t=+t)||t!=t?t:t<0?-1:1}},function(t,e,n){n(82)("match",1,function(t,e,n){return[function(n){"use strict";var r=t(this),i=void 0==n?void 0:n[e];return void 0!==i?i.call(n,r):new RegExp(n)[e](String(r))},n]})},function(t,e,n){"use strict";var r=n(6),i=n(10),o=n(1),a=n(17),s=n(0);t.exports=function(t,e,n){var u=s(t),c=n(a,u,""[t]),l=c[0],f=c[1];o(function(){var e={};return e[u]=function(){return 7},7!=""[t](e)})&&(i(String.prototype,t,l),r(RegExp.prototype,u,2==e?function(t,e){return f.call(t,this,e)}:function(t){return f.call(t,this)}))}},function(t,e,n){"use strict";var r=n(2),i=n(39),o=n(16),a=n(1),s=[].sort,u=[1,2,3];r(r.P+r.F*(a(function(){u.sort(void 0)})||!a(function(){u.sort(null)})||!n(84)(s)),"Array",{sort:function(t){return void 0===t?s.call(o(this)):s.call(o(this),i(t))}})},function(t,e,n){"use strict";var r=n(1);t.exports=function(t,e){return!!t&&r(function(){e?t.call(null,function(){},1):t.call(null)})}},function(t,e,n){var r=n(8),i=n(86).set;t.exports=function(t,e,n){var o,a=e.constructor;return a!==n&&"function"==typeof a&&(o=a.prototype)!==n.prototype&&r(o)&&i&&i(t,o),t}},function(t,e,n){var r=n(8),i=n(7),o=function(t,e){if(i(t),!r(e)&&null!==e)throw TypeError(e+": can't set as prototype!")};t.exports={set:Object.setPrototypeOf||("__proto__"in{}?function(t,e,r){try{(r=n(29)(Function.call,n(37).f(Object.prototype,"__proto__").set,2))(t,[]),e=!(t instanceof Array)}catch(t){e=!0}return function(t,n){return o(t,n),e?t.__proto__=n:r(t,n),t}}({},!1):void 0),check:o}},function(t,e,n){var r=n(2),i=n(17),o=n(1),a=n(88),s="["+a+"]",u=RegExp("^"+s+s+"*"),c=RegExp(s+s+"*$"),l=function(t,e,n){var i={},s=o(function(){return!!a[t]()||"​"!="​"[t]()}),u=i[t]=s?e(f):a[t];n&&(i[n]=u),r(r.P+r.F*s,"String",i)},f=l.trim=function(t,e){return t=String(i(t)),1&e&&(t=t.replace(u,"")),2&e&&(t=t.replace(c,"")),t};t.exports=l},function(t,e){t.exports="\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff"},function(t,e,n){"use strict";var r=n(2),i=n(49)(!0);r(r.P,"Array",{includes:function(t){return i(this,t,arguments.length>1?arguments[1]:void 0)}}),n(46)("includes")},function(t,e,n){"use strict";var r=n(2),i=n(91);r(r.P+r.F*n(93)("includes"),"String",{includes:function(t){return!!~i(this,t,"includes").indexOf(t,arguments.length>1?arguments[1]:void 0)}})},function(t,e,n){var r=n(92),i=n(17);t.exports=function(t,e,n){if(r(e))throw TypeError("String#"+n+" doesn't accept regex!");return String(i(t))}},function(t,e,n){var r=n(8),i=n(19),o=n(0)("match");t.exports=function(t){var e;return r(t)&&(void 0!==(e=t[o])?!!e:"RegExp"==i(t))}},function(t,e,n){var r=n(0)("match");t.exports=function(t){var e=/./;try{"/./"[t](e)}catch(n){try{return e[r]=!1,!"/./"[t](e)}catch(t){}}return!0}},function(t,e,n){"use strict";var r=n(25);n.n(r).a},function(t,e,n){(t.exports=n(23)(!1)).push([t.i,'\n.VueCarousel-navigation-button[data-v-453ad8cd] {\n  position: absolute;\n  top: 50%;\n  box-sizing: border-box;\n  color: #000;\n  text-decoration: none;\n  appearance: none;\n  border: none;\n  background-color: transparent;\n  padding: 0;\n  cursor: pointer;\n  outline: none;\n}\n.VueCarousel-navigation-button[data-v-453ad8cd]:focus {\n  outline: 1px solid lightblue;\n}\n.VueCarousel-navigation-next[data-v-453ad8cd] {\n  right: 0;\n  transform: translateY(-50%) translateX(100%);\n  font-family: "system";\n}\n.VueCarousel-navigation-prev[data-v-453ad8cd] {\n  left: 0;\n  transform: translateY(-50%) translateX(-100%);\n  font-family: "system";\n}\n.VueCarousel-navigation--disabled[data-v-453ad8cd] {\n  opacity: 0.5;\n  cursor: default;\n}\n\n/* Define the "system" font family */\n@font-face {\n  font-family: system;\n  font-style: normal;\n  font-weight: 300;\n  src: local(".SFNSText-Light"), local(".HelveticaNeueDeskInterface-Light"),\n    local(".LucidaGrandeUI"), local("Ubuntu Light"), local("Segoe UI Symbol"),\n    local("Roboto-Light"), local("DroidSans"), local("Tahoma");\n}\n',""])},function(t,e,n){"use strict";var r=n(13),i=n(35),o=n(22),a=n(16),s=n(47),u=Object.assign;t.exports=!u||n(1)(function(){var t={},e={},n=Symbol(),r="abcdefghijklmnopqrst";return t[n]=7,r.split("").forEach(function(t){e[t]=t}),7!=u({},t)[n]||Object.keys(u({},e)).join("")!=r})?function(t,e){for(var n=a(t),u=arguments.length,c=1,l=i.f,f=o.f;u>c;)for(var d,h=s(arguments[c++]),p=l?r(h).concat(l(h)):r(h),g=p.length,v=0;g>v;)f.call(h,d=p[v++])&&(n[d]=h[d]);return n}:u},function(t,e,n){"use strict";var r=n(26);n.n(r).a},function(t,e,n){(t.exports=n(23)(!1)).push([t.i,"\n.VueCarousel-pagination[data-v-438fd353] {\n  width: 100%;\n  text-align: center;\n}\n.VueCarousel-pagination--top-overlay[data-v-438fd353] {\n  position: absolute;\n  top: 0;\n}\n.VueCarousel-pagination--bottom-overlay[data-v-438fd353] {\n  position: absolute;\n  bottom: 0;\n}\n.VueCarousel-dot-container[data-v-438fd353] {\n  display: inline-block;\n  margin: 0 auto;\n  padding: 0;\n}\n.VueCarousel-dot[data-v-438fd353] {\n  display: inline-block;\n  cursor: pointer;\n  appearance: none;\n  border: none;\n  background-clip: content-box;\n  box-sizing: content-box;\n  padding: 0;\n  border-radius: 100%;\n  outline: none;\n}\n.VueCarousel-dot[data-v-438fd353]:focus {\n  outline: 1px solid lightblue;\n}\n",""])},function(t,e,n){"use strict";var r=n(27);n.n(r).a},function(t,e,n){(t.exports=n(23)(!1)).push([t.i,"\n.VueCarousel-slide {\n  flex-basis: inherit;\n  flex-grow: 0;\n  flex-shrink: 0;\n  user-select: none;\n  backface-visibility: hidden;\n  -webkit-touch-callout: none;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\n  outline: none;\n}\n.VueCarousel-slide-adjustableHeight {\n  display: table;\n  flex-basis: auto;\n  width: 100%;\n}\n",""])},function(t,e,n){"use strict";var r=n(28);n.n(r).a},function(t,e,n){(t.exports=n(23)(!1)).push([t.i,"\n.VueCarousel {\n  display: flex;\n  flex-direction: column;\n  position: relative;\n}\n.VueCarousel--reverse {\n  flex-direction: column-reverse;\n}\n.VueCarousel-wrapper {\n  width: 100%;\n  position: relative;\n  overflow: hidden;\n}\n.VueCarousel-inner {\n  display: flex;\n  flex-direction: row;\n  backface-visibility: hidden;\n}\n.VueCarousel-inner--center {\n  justify-content: center;\n}\n",""])},function(t,e,n){"use strict";n.r(e);var r=function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"VueCarousel",class:{"VueCarousel--reverse":"top"===t.paginationPosition}},[n("div",{ref:"VueCarousel-wrapper",staticClass:"VueCarousel-wrapper"},[n("div",{ref:"VueCarousel-inner",class:["VueCarousel-inner",{"VueCarousel-inner--center":t.isCenterModeEnabled}],style:{transform:"translate("+t.currentOffset+"px, 0)",transition:t.dragging?"none":t.transitionStyle,"ms-flex-preferred-size":t.slideWidth+"px","webkit-flex-basis":t.slideWidth+"px","flex-basis":t.slideWidth+"px",visibility:t.slideWidth?"visible":"hidden",height:""+t.currentHeight,"padding-left":t.padding+"px","padding-right":t.padding+"px"}},[t._t("default")],2)]),t._v(" "),t.navigationEnabled?t._t("navigation",[t.isNavigationRequired?n("navigation",{attrs:{clickTargetSize:t.navigationClickTargetSize,nextLabel:t.navigationNextLabel,prevLabel:t.navigationPrevLabel},on:{navigationclick:t.handleNavigation}}):t._e()]):t._e(),t._v(" "),t.paginationEnabled?t._t("pagination",[n("pagination",{on:{paginationclick:function(e){t.goToPage(e,"pagination")}}})]):t._e()],2)};r._withStripped=!0,n(54),n(61),n(63),n(45),n(71),n(73),n(74),n(79),n(81),n(83),n(38),n(89),n(90);var i={props:{autoplay:{type:Boolean,default:!1},autoplayTimeout:{type:Number,default:2e3},autoplayHoverPause:{type:Boolean,default:!0},autoplayDirection:{type:String,default:"forward"}},data:function(){return{autoplayInterval:null}},destroyed:function(){this.$isServer||(this.$el.removeEventListener("mouseenter",this.pauseAutoplay),this.$el.removeEventListener("mouseleave",this.startAutoplay))},methods:{pauseAutoplay:function(){this.autoplayInterval&&(this.autoplayInterval=clearInterval(this.autoplayInterval))},startAutoplay:function(){this.autoplay&&(this.autoplayInterval=setInterval(this.autoplayAdvancePage,this.autoplayTimeout))},restartAutoplay:function(){this.pauseAutoplay(),this.startAutoplay()},autoplayAdvancePage:function(){this.advancePage(this.autoplayDirection)}},mounted:function(){!this.$isServer&&this.autoplayHoverPause&&(this.$el.addEventListener("mouseenter",this.pauseAutoplay),this.$el.addEventListener("mouseleave",this.startAutoplay)),this.startAutoplay()}},o=function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"VueCarousel-navigation"},[n("button",{staticClass:"VueCarousel-navigation-button VueCarousel-navigation-prev",class:{"VueCarousel-navigation--disabled":!t.canAdvanceBackward},style:"padding: "+t.clickTargetSize+"px; margin-right: -"+t.clickTargetSize+"px;",attrs:{type:"button","aria-label":"Previous page",tabindex:t.canAdvanceBackward?0:-1},domProps:{innerHTML:t._s(t.prevLabel)},on:{click:function(e){e.preventDefault(),t.triggerPageAdvance("backward")}}}),t._v(" "),n("button",{staticClass:"VueCarousel-navigation-button VueCarousel-navigation-next",class:{"VueCarousel-navigation--disabled":!t.canAdvanceForward},style:"padding: "+t.clickTargetSize+"px; margin-left: -"+t.clickTargetSize+"px;",attrs:{type:"button","aria-label":"Next page",tabindex:t.canAdvanceForward?0:-1},domProps:{innerHTML:t._s(t.nextLabel)},on:{click:function(e){e.preventDefault(),t.triggerPageAdvance("forward")}}})])};o._withStripped=!0;var a={name:"navigation",inject:["carousel"],props:{clickTargetSize:{type:Number,default:8},nextLabel:{type:String,default:"&#9654"},prevLabel:{type:String,default:"&#9664"}},computed:{canAdvanceForward:function(){return this.carousel.canAdvanceForward||!1},canAdvanceBackward:function(){return this.carousel.canAdvanceBackward||!1}},methods:{triggerPageAdvance:function(t){this.$emit("navigationclick",t)}}};function s(t,e,n,r,i,o,a,s){var u,c="function"==typeof t?t.options:t;if(e&&(c.render=e,c.staticRenderFns=n,c._compiled=!0),r&&(c.functional=!0),o&&(c._scopeId="data-v-"+o),a?(u=function(t){(t=t||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext)||"undefined"==typeof __VUE_SSR_CONTEXT__||(t=__VUE_SSR_CONTEXT__),i&&i.call(this,t),t&&t._registeredComponents&&t._registeredComponents.add(a)},c._ssrRegister=u):i&&(u=s?function(){i.call(this,this.$root.$options.shadowRoot)}:i),u)if(c.functional){c._injectStyles=u;var l=c.render;c.render=function(t,e){return u.call(e),l(t,e)}}else{var f=c.beforeCreate;c.beforeCreate=f?[].concat(f,u):[u]}return{exports:t,options:c}}n(94);var u=s(a,o,[],!1,null,"453ad8cd",null);u.options.__file="src/Navigation.vue";var c=u.exports,l=function(){var t,e=this,n=e.$createElement,r=e._self._c||n;return r("div",{directives:[{name:"show",rawName:"v-show",value:e.carousel.pageCount>1,expression:"carousel.pageCount > 1"}],staticClass:"VueCarousel-pagination",class:(t={},t["VueCarousel-pagination--"+e.paginationPositionModifierName]=e.paginationPositionModifierName,t)},[r("div",{staticClass:"VueCarousel-dot-container",style:e.dotContainerStyle,attrs:{role:"tablist"}},e._l(e.paginationCount,function(t,n){return r("button",{key:t+"_"+n,staticClass:"VueCarousel-dot",class:{"VueCarousel-dot--active":e.isCurrentDot(n)},style:e.dotStyle(n),attrs:{"aria-hidden":"false",role:"tab",title:e.getDotTitle(n),value:e.getDotTitle(n),"aria-label":e.getDotTitle(n),"aria-selected":e.isCurrentDot(n)?"true":"false"},on:{click:function(t){e.goToPage(n)}}})}))])};l._withStripped=!0,n(52);var f={name:"pagination",inject:["carousel"],computed:{paginationPositionModifierName:function(){var t=this.carousel.paginationPosition;if(!(t.indexOf("overlay")<0))return t},paginationPropertyBasedOnPosition:function(){return this.carousel.paginationPosition.indexOf("top")>=0?"bottom":"top"},paginationCount:function(){return this.carousel&&this.carousel.scrollPerPage?this.carousel.pageCount:this.carousel.slideCount||0},dotContainerStyle:function(){var t=this.carousel;if(-1===t.maxPaginationDotCount)return{"margin-top":"".concat(2*t.paginationPadding,"px")};var e=2*t.paginationPadding,n=t.maxPaginationDotCount*(t.paginationSize+e);return{"margin-top":"".concat(2*t.paginationPadding,"px"),overflow:"hidden",width:"".concat(n,"px"),margin:"0 auto","white-space":"nowrap"}}},methods:{goToPage:function(t){this.$emit("paginationclick",t)},isCurrentDot:function(t){return t===this.carousel.currentPage},getDotTitle:function(t){return this.carousel.$children[t].title?this.carousel.$children[t].title:"Item ".concat(t)},dotStyle:function(t){var e=this.carousel,n={};if(n["margin-".concat(this.paginationPropertyBasedOnPosition)]="".concat(2*e.paginationPadding,"px"),Object.assign(n,{padding:"".concat(e.paginationPadding,"px"),width:"".concat(e.paginationSize,"px"),height:"".concat(e.paginationSize,"px"),"background-color":"".concat(this.isCurrentDot(t)?e.paginationActiveColor:e.paginationColor)}),-1===e.maxPaginationDotCount)return n;var r=e.paginationSize+2*e.paginationPadding,i=e.pageCount-e.maxPaginationDotCount,o=0-r*(e.currentPage>i?i:e.currentPage<=e.maxPaginationDotCount/2?0:e.currentPage-Math.ceil(e.maxPaginationDotCount/2)+1);return Object.assign(n,{"-webkit-transform":"translate3d(".concat(o,"px,0,0)"),transform:"translate3d(".concat(o,"px,0,0)"),"-webkit-transition":"-webkit-transform ".concat(e.speed/1e3,"s"),transition:"transform ".concat(e.speed/1e3,"s")})}}},d=(n(97),s(f,l,[],!1,null,"438fd353",null));d.options.__file="src/Pagination.vue";var h=d.exports,p=function(){var t=this.$createElement;return(this._self._c||t)("div",{staticClass:"VueCarousel-slide",class:{"VueCarousel-slide-active":this.isActive,"VueCarousel-slide-center":this.isCenter,"VueCarousel-slide-adjustableHeight":this.isAdjustableHeight},attrs:{tabindex:"-1","aria-hidden":!this.isActive,role:"tabpanel"}},[this._t("default")],2)};p._withStripped=!0;var g={name:"slide",props:["title"],data:function(){return{width:null}},inject:["carousel"],mounted:function(){this.$isServer||this.$el.addEventListener("dragstart",function(t){return t.preventDefault()}),this.$el.addEventListener(this.carousel.isTouch?"touchend":"mouseup",this.onTouchEnd)},computed:{activeSlides:function(){for(var t=this.carousel,e=t.currentPage,n=t.breakpointSlidesPerPage,r=[],i=t.$children.filter(function(t){return t.$el&&t.$el.className.indexOf("VueCarousel-slide")>=0}).map(function(t){return t._uid}),o=0;o<n;){var a=i[e*n+o];r.push(a),o++}return r},isActive:function(){return this.activeSlides.indexOf(this._uid)>=0},isCenter:function(){var t=this.carousel.breakpointSlidesPerPage;return!(t%2==0||!this.isActive)&&this.activeSlides.indexOf(this._uid)===Math.floor(t/2)},isAdjustableHeight:function(){return this.carousel.adjustableHeight}},methods:{onTouchEnd:function(t){var e=this.carousel.isTouch&&t.changedTouches&&t.changedTouches.length>0?t.changedTouches[0].clientX:t.clientX,n=this.carousel.dragStartX-e;(0===this.carousel.minSwipeDistance||Math.abs(n)<this.carousel.minSwipeDistance)&&(this.$emit("slideclick",Object.assign({},t.currentTarget.dataset)),this.$emit("slide-click",Object.assign({},t.currentTarget.dataset)))}}},v=(n(99),s(g,p,[],!1,null,null,null));v.options.__file="src/Slide.vue";var y=v.exports;function m(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function b(t){return(b="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}var x={onwebkittransitionend:"webkitTransitionEnd",onmoztransitionend:"transitionend",onotransitionend:"oTransitionEnd otransitionend",ontransitionend:"transitionend"},P=function(){for(var t in x)if(t in window)return x[t]},S={name:"carousel",beforeUpdate:function(){this.computeCarouselWidth()},components:{Navigation:c,Pagination:h,Slide:y},data:function(){return{browserWidth:null,carouselWidth:0,currentPage:0,dragging:!1,dragMomentum:0,dragOffset:0,dragStartY:0,dragStartX:0,isTouch:"undefined"!=typeof window&&"ontouchstart"in window,offset:0,refreshRate:16,slideCount:0,transitionstart:"transitionstart",transitionend:"transitionend",currentHeight:"auto"}},mixins:[i],provide:function(){return{carousel:this}},props:{adjustableHeight:{type:Boolean,default:!1},adjustableHeightEasing:{type:String},centerMode:{type:Boolean,default:!1},easing:{type:String,validator:function(t){return-1!==["ease","linear","ease-in","ease-out","ease-in-out"].indexOf(t)||t.includes("cubic-bezier")},default:"ease"},loop:{type:Boolean,default:!1},minSwipeDistance:{type:Number,default:8},mouseDrag:{type:Boolean,default:!0},touchDrag:{type:Boolean,default:!0},navigateTo:{type:[Number,Array],default:0},navigationClickTargetSize:{type:Number,default:8},navigationEnabled:{type:Boolean,default:!1},navigationNextLabel:{type:String,default:"&#9654"},navigationPrevLabel:{type:String,default:"&#9664"},paginationActiveColor:{type:String,default:"#000000"},paginationColor:{type:String,default:"#efefef"},paginationEnabled:{type:Boolean,default:!0},paginationPadding:{type:Number,default:10},paginationPosition:{type:String,default:"bottom"},paginationSize:{type:Number,default:10},perPage:{type:Number,default:2},perPageCustom:{type:Array},resistanceCoef:{type:Number,default:20},scrollPerPage:{type:Boolean,default:!0},spacePadding:{type:Number,default:0},spacePaddingMaxOffsetFactor:{type:Number,default:0},speed:{type:Number,default:500},tagName:{type:String,default:"slide"},value:{type:Number},maxPaginationDotCount:{type:Number,default:-1},rtl:{type:Boolean,default:!1}},watch:{value:function(t){t!==this.currentPage&&(this.goToPage(t),this.render())},navigateTo:{immediate:!0,handler:function(t){var e=this;"object"===b(t)?(0==t[1]&&(this.dragging=!0,setTimeout(function(){e.dragging=!1},this.refreshRate)),this.$nextTick(function(){e.goToPage(t[0])})):this.$nextTick(function(){e.goToPage(t)})}},currentPage:function(t){this.$emit("pageChange",t),this.$emit("page-change",t),this.$emit("input",t)},autoplay:function(t){!1===t?this.pauseAutoplay():this.restartAutoplay()}},computed:{breakpointSlidesPerPage:function(){if(!this.perPageCustom)return this.perPage;var t=this.perPageCustom,e=this.browserWidth,n=t.sort(function(t,e){return t[0]>e[0]?-1:1}).filter(function(t){return e>=t[0]});return n[0]&&n[0][1]||this.perPage},canAdvanceForward:function(){return this.loop||this.offset<this.maxOffset},canAdvanceBackward:function(){return this.loop||this.currentPage>0},currentPerPage:function(){return!this.perPageCustom||this.$isServer?this.perPage:this.breakpointSlidesPerPage},currentOffset:function(){return this.isCenterModeEnabled?0:this.rtl?1*(this.offset-this.dragOffset):-1*(this.offset+this.dragOffset)},isHidden:function(){return this.carouselWidth<=0},maxOffset:function(){return Math.max(this.slideWidth*(this.slideCount-this.currentPerPage)-this.spacePadding*this.spacePaddingMaxOffsetFactor,0)},pageCount:function(){return this.scrollPerPage?Math.ceil(this.slideCount/this.currentPerPage):this.slideCount-this.currentPerPage+1},slideWidth:function(){return(this.carouselWidth-2*this.spacePadding)/this.currentPerPage},isNavigationRequired:function(){return this.slideCount>this.currentPerPage},isCenterModeEnabled:function(){return this.centerMode&&!this.isNavigationRequired},transitionStyle:function(){var t="".concat(this.speed/1e3,"s"),e="".concat(t," ").concat(this.easing," transform");return this.adjustableHeight?"".concat(e,", height ").concat(t," ").concat(this.adjustableHeightEasing||this.easing):e},padding:function(){var t=this.spacePadding;return t>0&&t}},methods:{getNextPage:function(){return this.currentPage<this.pageCount-1?this.currentPage+1:this.loop?0:this.currentPage},getPreviousPage:function(){return this.currentPage>0?this.currentPage-1:this.loop?this.pageCount-1:this.currentPage},advancePage:function(t){t&&"backward"===t&&this.canAdvanceBackward?this.goToPage(this.getPreviousPage(),"navigation"):(!t||t&&"backward"!==t)&&this.canAdvanceForward&&this.goToPage(this.getNextPage(),"navigation")},goToLastSlide:function(){var t=this;this.dragging=!0,setTimeout(function(){t.dragging=!1},this.refreshRate),this.$nextTick(function(){t.goToPage(t.pageCount)})},attachMutationObserver:function(){var t=this,e=window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver;if(e){var n={attributes:!0,data:!0};if(this.adjustableHeight&&(n=function(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},r=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(r=r.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),r.forEach(function(e){m(t,e,n[e])})}return t}({},n,{childList:!0,subtree:!0,characterData:!0})),this.mutationObserver=new e(function(){t.$nextTick(function(){t.computeCarouselWidth(),t.computeCarouselHeight()})}),this.$parent.$el)for(var r=this.$el.getElementsByClassName("VueCarousel-inner"),i=0;i<r.length;i++)this.mutationObserver.observe(r[i],n)}},handleNavigation:function(t){this.advancePage(t),this.pauseAutoplay(),this.$emit("navigation-click",t)},detachMutationObserver:function(){this.mutationObserver&&this.mutationObserver.disconnect()},getBrowserWidth:function(){return this.browserWidth=window.innerWidth,this.browserWidth},getCarouselWidth:function(){for(var t=this.$el.getElementsByClassName("VueCarousel-inner"),e=0;e<t.length;e++)t[e].clientWidth>0&&(this.carouselWidth=t[e].clientWidth||0);return this.carouselWidth},getCarouselHeight:function(){var t=this;if(!this.adjustableHeight)return"auto";var e=this.currentPerPage*(this.currentPage+1)-1,n=function(t){return function(t){if(Array.isArray(t)){for(var e=0,n=new Array(t.length);e<t.length;e++)n[e]=t[e];return n}}(t)||function(t){if(Symbol.iterator in Object(t)||"[object Arguments]"===Object.prototype.toString.call(t))return Array.from(t)}(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance")}()}(Array(this.currentPerPage)).map(function(n,r){return t.getSlide(e+r)}).reduce(function(t,e){return Math.max(t,e&&e.$el.clientHeight||0)},0);return this.currentHeight=0===n?"auto":"".concat(n,"px"),this.currentHeight},getSlideCount:function(){var t=this;this.slideCount=this.$slots&&this.$slots.default&&this.$slots.default.filter(function(e){return e.tag&&null!==e.tag.match("^vue-component-\\d+-".concat(t.tagName,"$"))}).length||0},getSlide:function(t){var e=this;return this.$children.filter(function(t){return null!==t.$vnode.tag.match("^vue-component-\\d+-".concat(e.tagName,"$"))})[t]},goToPage:function(t,e){t>=0&&t<=this.pageCount&&(this.offset=this.scrollPerPage?Math.min(this.slideWidth*this.currentPerPage*t,this.maxOffset):this.slideWidth*t,this.autoplay&&!this.autoplayHoverPause&&this.restartAutoplay(),this.currentPage=t,"pagination"===e&&(this.pauseAutoplay(),this.$emit("pagination-click",t)))},onStart:function(t){2!=t.button&&(document.addEventListener(this.isTouch?"touchend":"mouseup",this.onEnd,!0),document.addEventListener(this.isTouch?"touchmove":"mousemove",this.onDrag,!0),this.startTime=t.timeStamp,this.dragging=!0,this.dragStartX=this.isTouch?t.touches[0].clientX:t.clientX,this.dragStartY=this.isTouch?t.touches[0].clientY:t.clientY)},onEnd:function(t){this.autoplay&&!this.autoplayHoverPause&&this.restartAutoplay(),this.pauseAutoplay();var e=this.isTouch?t.changedTouches[0].clientX:t.clientX,n=this.dragStartX-e;if(this.dragMomentum=n/(t.timeStamp-this.startTime),0!==this.minSwipeDistance&&Math.abs(n)>=this.minSwipeDistance){var r=this.scrollPerPage?this.slideWidth*this.currentPerPage:this.slideWidth;this.dragOffset=this.dragOffset+Math.sign(n)*(r/2)}this.rtl?this.offset-=this.dragOffset:this.offset+=this.dragOffset,this.dragOffset=0,this.dragging=!1,this.render(),document.removeEventListener(this.isTouch?"touchend":"mouseup",this.onEnd,!0),document.removeEventListener(this.isTouch?"touchmove":"mousemove",this.onDrag,!0)},onDrag:function(t){var e=this.isTouch?t.touches[0].clientX:t.clientX,n=this.isTouch?t.touches[0].clientY:t.clientY,r=this.dragStartX-e,i=this.dragStartY-n;if(!(this.isTouch&&Math.abs(r)<Math.abs(i))){t.stopImmediatePropagation(),this.dragOffset=r;var o=this.offset+this.dragOffset;this.rtl?0==this.offset&&this.dragOffset>0?this.dragOffset=Math.sqrt(this.resistanceCoef*this.dragOffset):this.offset==this.maxOffset&&this.dragOffset<0&&(this.dragOffset=-Math.sqrt(-this.resistanceCoef*this.dragOffset)):o<0?this.dragOffset=-Math.sqrt(-this.resistanceCoef*this.dragOffset):o>this.maxOffset&&(this.dragOffset=Math.sqrt(this.resistanceCoef*this.dragOffset))}},onResize:function(){var t=this;this.computeCarouselWidth(),this.computeCarouselHeight(),this.dragging=!0,this.render(),setTimeout(function(){t.dragging=!1},this.refreshRate)},render:function(){this.rtl?this.offset-=Math.max(1-this.currentPerPage,Math.min(Math.round(this.dragMomentum),this.currentPerPage-1))*this.slideWidth:this.offset+=Math.max(1-this.currentPerPage,Math.min(Math.round(this.dragMomentum),this.currentPerPage-1))*this.slideWidth;var t=this.scrollPerPage?this.slideWidth*this.currentPerPage:this.slideWidth,e=t*Math.floor(this.slideCount/(this.currentPerPage-1)),n=e+this.slideWidth*(this.slideCount%this.currentPerPage);this.offset>(e+n)/2?this.offset=n:this.offset=t*Math.round(this.offset/t),this.offset=Math.max(0,Math.min(this.offset,this.maxOffset)),this.currentPage=this.scrollPerPage?Math.round(this.offset/this.slideWidth/this.currentPerPage):Math.round(this.offset/this.slideWidth)},computeCarouselWidth:function(){this.getSlideCount(),this.getBrowserWidth(),this.getCarouselWidth(),this.setCurrentPageInBounds()},computeCarouselHeight:function(){this.getCarouselHeight()},setCurrentPageInBounds:function(){if(!this.canAdvanceForward&&this.scrollPerPage){var t=this.pageCount-1;this.currentPage=t>=0?t:0,this.offset=Math.max(0,Math.min(this.offset,this.maxOffset))}},handleTransitionStart:function(){this.$emit("transitionStart"),this.$emit("transition-start")},handleTransitionEnd:function(){this.$emit("transitionEnd"),this.$emit("transition-end")}},mounted:function(){window.addEventListener("resize",function(t,e,n){var r;return function(){var i=n&&!r;clearTimeout(r),r=setTimeout(function(){r=null,n||t.apply(void 0)},e),i&&t.apply(void 0)}}(this.onResize,this.refreshRate)),(this.isTouch&&this.touchDrag||this.mouseDrag)&&this.$refs["VueCarousel-wrapper"].addEventListener(this.isTouch?"touchstart":"mousedown",this.onStart),this.attachMutationObserver(),this.computeCarouselWidth(),this.computeCarouselHeight(),this.transitionstart=P(),this.$refs["VueCarousel-inner"].addEventListener(this.transitionstart,this.handleTransitionStart),this.transitionend=P(),this.$refs["VueCarousel-inner"].addEventListener(this.transitionend,this.handleTransitionEnd),this.$emit("mounted"),"backward"===this.autoplayDirection&&this.goToLastSlide()},beforeDestroy:function(){this.detachMutationObserver(),window.removeEventListener("resize",this.getBrowserWidth),this.$refs["VueCarousel-inner"].removeEventListener(this.transitionstart,this.handleTransitionStart),this.$refs["VueCarousel-inner"].removeEventListener(this.transitionend,this.handleTransitionEnd),this.$refs["VueCarousel-wrapper"].removeEventListener(this.isTouch?"touchstart":"mousedown",this.onStart)}},C=(n(101),s(S,r,[],!1,null,null,null));C.options.__file="src/Carousel.vue";var w=C.exports;n.d(e,"Carousel",function(){return w}),n.d(e,"Slide",function(){return y}),e.default={install:function(t){t.component("carousel",w),t.component("slide",y)}}}])});

/***/ }),

/***/ 599:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1605)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1607)
/* template */
var __vue_template__ = __webpack_require__(1608)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e7d1e6ca"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/communityComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e7d1e6ca", Component.options)
  } else {
    hotAPI.reload("data-v-e7d1e6ca", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});