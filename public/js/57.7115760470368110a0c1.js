webpackJsonp([57],{

/***/ 1091:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1092);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("10783800", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7288f46a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorStatisticPageComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7288f46a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorStatisticPageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1092:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nimg[data-v-7288f46a]{\n  -o-object-fit:cover;\n     object-fit:cover;\n}\n.main-page[data-v-7288f46a] {\n  margin-top: -5px;\n  position: relative;\n min-height: 100vh;\n}\n.campaign-overlay[data-v-7288f46a]{\n  position:absolute;\n  top: 0;\n  left:0;\n  bottom:0;\n  right:0;\n  background-color:rgba(0,0,0,.8);\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-pack:center;\n      -ms-flex-pack:center;\n          justify-content:center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index:99;\n}\n.closeCamp[data-v-7288f46a]{\n  position:absolute;\n  top:-15px;\n  right:-15px;\n  cursor:pointer;\n}\n.w-70[data-v-7288f46a]{\n  width:70% !important;\n}\n.campaign[data-v-7288f46a]{\n  position:relative;\n  background-color:#e1e1e1;\n}\n.form[data-v-7288f46a]{\n  background-color:#f5f5f5;\n}\nul li[data-v-7288f46a] {\n  list-style: none;\n}\n.small-text[data-v-7288f46a] {\n  font-size: 16px;\n}\nimg[data-v-7288f46a] {\n  width: 100%;\n  height: 100%;\n}\n.h-100[data-v-7288f46a]{\n  min-height:100px;\n}\nsmall[data-v-7288f46a] {\n  font-size: 12px;\n}\nlabel[data-v-7288f46a] {\n  font-weight: normal;\n}\n.form-check-input[data-v-7288f46a] {\n  position: absolute;\n  margin-top: 0.3rem;\n  margin-left: -1.25rem;\n}\n.head_flex[data-v-7288f46a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  text-align:center;\n}\n.info_icon[data-v-7288f46a] {\n  font-size: 0.5em;\n}\n.top-nav[data-v-7288f46a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  background-color:  hsl(207, 43%, 20%) ;\n  position: -webkit-sticky;\n  position: sticky;\n}\n.select_nav[data-v-7288f46a] {\n  padding: 5px 10px;\n  width: 33.3%;\n  font-weight: bold;\n  color: rgba(255, 255, 255, 0.64);\n  cursor:pointer;\n}\n.select_nav[data-v-7288f46a]:hover {\n  border-bottom: 1px solid #ccc;\n  font-weight: bold;\n  color: white;\n}\n.selected_nav[data-v-7288f46a] {\n  border-bottom: 1px solid #333;\n  color: white !important;\n}\n.main_content[data-v-7288f46a] {\n  max-height: 90vh;\n  overflow: scroll;\n}\n\n/* Schedule styles  */\n.schedule_1[data-v-7288f46a] {\n  width: 70%;\n  min-height: 60vh;\n}\n.schedule_2[data-v-7288f46a] {\n  width: 30%;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  height: -webkit-max-content;\n  height: -moz-max-content;\n  height: max-content;\n  padding-left:15px;\n  margin: 0;\n}\n.schedule_box[data-v-7288f46a] {\n  position: relative;\n  background: white;\n  min-height: 200px;\n  padding: 15px;\n  margin: 0 0 15px 0;\n}\n.schedule_create[data-v-7288f46a] {\n  position: absolute;\n  bottom: 15px;\n  right: 15px;\n  cursor: pointer;\n   color:hsl(207, 43%, 20%)\n}\n.schedule_create[data-v-7288f46a]:hover {\n  text-decoration: underline;\n}\n.schedule_show_all[data-v-7288f46a] {\n  position: absolute;\n  bottom: 15px;\n  left: 15px;\n  cursor: pointer;\n  color:hsl(207, 43%, 20%)\n}\n.top_box[data-v-7288f46a]{\n  padding:15px;\n  background:white;\n}\n.myContents[data-v-7288f46a] {\n  padding: 40px;\n  margin: 15px;\n  width: 100%;\n  display: grid;\n  grid-column-gap: 10px;\n  grid-row-gap: 10px;\n  grid-template-columns: auto auto auto auto;\n}\n.myContent[data-v-7288f46a] {\n  position: relative;\n  width: 160px;\n  height: 170px;\n  border: 1px solid;\n}\n.flip-card[data-v-7288f46a] {\n  background-color: transparent;\n  border: 1px solid #f1f1f1;\n  -webkit-perspective: 1000px;\n          perspective: 1000px; /* Remove this if you don't want the 3D effect */\n}\n\n/* This container is needed to position the front and back side */\n.flip-card-inner[data-v-7288f46a] {\n  position: relative;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n  -webkit-transition: -webkit-transform 0.8s;\n  transition: -webkit-transform 0.8s;\n  transition: transform 0.8s;\n  transition: transform 0.8s, -webkit-transform 0.8s;\n  -webkit-transform-style: preserve-3d;\n          transform-style: preserve-3d;\n}\n\n/* Do an horizontal flip when you move the mouse over the flip box container */\n.flip-card:hover .flip-card-inner[data-v-7288f46a] {\n  -webkit-transform: rotateY(180deg);\n          transform: rotateY(180deg);\n}\n\n/* Position the front and back side */\n.flip-card-front[data-v-7288f46a],\n.flip-card-back[data-v-7288f46a] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n}\n.flip-card-front img[data-v-7288f46a]{\n  height:70%;\n}\n/* Style the front side (fallback if image is missing) */\n.flip-card-front[data-v-7288f46a] {\n  background-color: #f7f8fa;\n  color: black;\n}\n\n/* Style the back side */\n.flip-card-back[data-v-7288f46a] {\n  background-color: rgba(0, 0, 0, 0.84);\n  color: white;\n  -webkit-transform: rotateY(180deg);\n          transform: rotateY(180deg);\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  text-transform: capitalize;\n  padding: 10px;\n}\n.small[data-v-7288f46a]{\n  font-size: 11px;\n  line-height: 1.4;\n  padding:10px 0;\n}\n.tiny-text[data-v-7288f46a] {\n  font-size: 12px;\n  line-height:normal;\n}\n.topContent[data-v-7288f46a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.topImage[data-v-7288f46a] {\n  width: 150px;\n  height: 160px;\n  margin:0 auto;\n}\n.topText[data-v-7288f46a] {\n  padding: 15px;\n}\n\n/* activity styles  */\n.activity_box[data-v-7288f46a] {\n  position: relative;\n  margin: 0 0 15px 0;\n  min-height: 200px;\n  padding: 15px;\n  background: white;\n}\n\n/* audience styles  */\n.audience_box[data-v-7288f46a] {\n  position: relative;\n  min-height: 200px;\n  padding: 15px;\n  margin: 0 0 15px 0;\n  background: white;\n}\n.mobile[data-v-7288f46a] {\n  display: none !important;\n}\n@media (max-width: 768px) {\n.btn[data-v-7288f46a] {\n    text-transform: capitalize !important;\n    padding: 6px 12px !important;\n    font-size: 13px;\n    color: white !important;\n}\n.schedule_1[data-v-7288f46a] {\n    width: 100%;\n}\n.schedule_2[data-v-7288f46a] {\n    width: 100%;\n}\n.myContents[data-v-7288f46a] {\n    grid-template-columns: auto auto auto;\n    padding:15px;\n}\n.myContent[data-v-7288f46a] {\n    width: 100px;\n    height: 85px;\n}\n.tiny-text[data-v-7288f46a] {\n    font-size: 11px;\n}\n.topImage[data-v-7288f46a] {\n    width: 100px;\n    height: 90px;\n}\n.display-5[data-v-7288f46a] {\n    font-size: 17px;\n}\nh4[data-v-7288f46a] {\n    font-size: 18px;\n}\n.desktop[data-v-7288f46a] {\n    display: none !important;\n}\n.mobile[data-v-7288f46a] {\n    display: block !important;\n}\n.select_nav[data-v-7288f46a]{\n    font-size:14px;\n}\n.schedule_box[data-v-7288f46a]{\n    margin-right:0;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1093:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
var _props$name$data$befo;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = (_props$name$data$befo = {
  props: ['vendorSub'],
  name: "vendorStatisticComponent",
  data: function data() {
    return {
      first: true,
      second: false,
      third: false,
      location_names: [],
      location_values: [],
      gender_values: [0, 0],
      gender_names: ["Male", "Female"],
      age_values: [],
      age_range: ["0 - 17", "18 - 25", "26 - 41", "42 - 59", "60 - abv"],
      followNo: 0,
      allContents: [],
      activity: {},
      genders: [],
      male: [],
      female: [],
      locations: [],
      realLocations: [],
      topContentArr: [],
      topContent: {},
      topIndustryArr: [],
      topIndustry: {},
      likes: [],
      ages: [],
      day: "Monday",
      response: {
        first: "",
        second: ""
      },
      submitContenDone: false,
      daydone: false,
      uploadDay: "",
      interactions: 0,
      myShares: 0,
      myLik: 0,
      myView: 0,
      reach: [],
      weeklyReach: [],
      weeklyActivity: [],
      weeklyViews: 0,
      weeklyVisits: 0,
      totalViews: 0,
      totalVisits: 0,
      weeklyFollow: 0,
      openCampaign: false,
      showChecklist: false,
      campaign: {
        subject: '',
        from_name: '',
        message: ''
      },
      isActive: true
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.getFollowers();
      vm.getVendorProducts();
      vm.getActivity();
      vm.getGender();
      vm.getTops();
      vm.getLocation();
      vm.getAges();
      vm.getDayForUpload();
      vm.getInteractions();
    });
  }
}, _defineProperty(_props$name$data$befo, "beforeRouteEnter", function beforeRouteEnter(to, from, next) {
  next(function (vm) {
    if (vm.$props.vendorSub > 2) {
      next();
    } else {
      next('/vendor/dashboard');
    }
  });
}), _defineProperty(_props$name$data$befo, "mounted", function mounted() {
  // this.getFollowers();
  // this.getVendorProducts();
  // this.getActivity();
  // this.getGender();
  // this.getTops();
  // this.getLocation();
  // this.getAges()
  // this.getDayForUpload()
  if (this.$moment().format('dddd') === "Friday" || this.$moment().format('dddd') === "Saturday") {

    this.showChecklist = true;
  }
}), _defineProperty(_props$name$data$befo, "computed", {
  contents: function contents() {
    return this.allContents.filter(function (item) {
      if (item.verify == 1) {
        return item;
      }
    });
  }
}), _defineProperty(_props$name$data$befo, "components", {
  "app-editor": __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]

}), _defineProperty(_props$name$data$befo, "methods", {
  upload_handler: function upload_handler(blobInfo, success, failure) {
    var xhr, formData;
    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open("POST", "/api/image-upload");
    xhr.onload = function () {
      var json;

      if (xhr.status != 200) {
        failure("HTTP Error: " + xhr.status);
        return;
      }
      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location != "string") {
        failure("Invalid JSON: " + xhr.responseText);
        return;
      }
      success(json.location);
    };
    formData = new FormData();
    formData.append("file", blobInfo.blob());
    xhr.send(formData);
  },
  loadFile: function loadFile(cb, mt) {
    if (mt.filetype === "image") {
      var up = this.$refs.upload;

      up.onchange = function () {
        var file = this.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
          var id = "blobid" + new Date().getTime();
          var blobCache = tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(",")[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);

          /* call the callback and populate the Title field with the file name */
          cb(blobInfo.blobUri(), { title: file.name });
        };
        reader.readAsDataURL(file);
      };

      up.click();
    }
  },
  makeList: function makeList() {
    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    var data = {
      name: vendor.storeName,
      company: vendor.storeName
    };
    axios.post("/api/create-list/", data).then().catch(function (error) {});
  },
  sendCamp: function sendCamp() {

    axios.post("/api/sendCampaign/", this.campaign).then().catch(function (error) {});
  },
  openCamp: function openCamp() {
    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    this.campaign.from_name = vendor.storeName;
    this.openCampaign = !this.openCampaign;
  },
  getInteractions: function getInteractions() {
    var _this = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-interactions/" + vendor.id).then(function (res) {
      if (res.status === 200) {

        if (res.data.shares) {
          _this.myshares = res.data.shares;
        }

        if (res.data.likes) {
          _this.myLik = res.data.likes;
        }

        if (res.data.views) {
          _this.myView = res.data.views;
        }

        _this.interactions = Math.round(res.data.shares + res.data.views + res.data.likes);
      }
    });
  },
  editDay: function editDay() {
    this.daydone = !this.daydone;
  },
  submitDay: function submitDay() {
    var _this2 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    var data = {
      vendor_id: vendor.id,
      day: this.day
    };
    var conf = confirm("Are you sure?");

    if (conf) {
      axios.post("/api/upload-day", data).then(function (response) {
        if (response.status === 200) {
          _this2.getDayForUpload();
          _this2.$toasted.success(" successful");

          _this2.daydone = true;
        }
      });
    }
  },
  getDayForUpload: function getDayForUpload() {
    var _this3 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-day/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data !== null) {
          _this3.uploadDay = response.data.day;
          _this3.daydone = true;
        } else {
          _this3.daydone = false;
        }
      }
    });
  },
  submitCheck: function submitCheck() {
    var _this4 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    var data = {
      vendor_id: vendor.id,
      response: this.response
    };
    var conf = confirm("Are you sure?");

    if (conf) {
      axios.post("/api/add-checklist-vendor", data).then(function (response) {
        if (response.status === 200) {
          _this4.$toasted.success("submission successful");
          _this4.submitContenDone = true;
        }
      });
    }
  },
  compare: function compare(b, a) {
    if (a.count > b.count) {
      return 1;
    }
    if (a.count < b.count) {
      return -1;
    }
    return 0;
  },
  getGender: function getGender() {
    var _this5 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-gender/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data) {
          _this5.genders = response.data;

          _this5.genders.forEach(function (item) {
            if (item.male === 1) {
              _this5.male.push(item);
            }
            if (item.female === 1) {
              _this5.female.push(item);
            }
          });

          var total = Math.round(_this5.male.length) + Math.round(_this5.female.length);
          _this5.gender_values = [Math.round(Math.round(_this5.male.length) / total * 100), Math.round(Math.round(_this5.female.length) / total * 100)];
        }
      }
    });
  },
  getLocation: function getLocation() {
    var _this6 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    var name = [];
    var value = [];
    axios.get("/api/get-location/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data.length > 0) {

          var _name = [];
          var _value = [];

          var allLocations = response.data.map(function (item) {
            return item.locale;
          });

          var locale = allLocations.reduce(function (b, c) {
            return (b[b.findIndex(function (d) {
              return d.el === c;
            })] || b[b.push({ el: c, count: 0 }) - 1]).count++, b;
          }, []);
          _this6.realLocations = locale.sort(_this6.compare);
          _this6.realLocations.forEach(function (item) {
            _name.push(item.el);

            _value.push(item.count);
          });
          _this6.location_names = Array.from(_name);
          _this6.location_values = Array.from(_value);
        }
      }
    });
  },
  getTops: function getTops() {
    var _this7 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-top-content/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data.length > 0) {
          _this7.topContentArr = response.data.sort(_this7.compare);
          axios.get("/api/vendor-product/" + _this7.topContentArr[0].content_id, {
            headers: { Authorization: "Bearer " + vendor.access_token }
          }).then(function (response) {
            if (response.status === 200) {
              var emptyObject = {};

              switch (response.data.data.prodType) {
                case "Books":
                  _this7.$set(response.data.data.books, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.books, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.books, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.books, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.books, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.books, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.books, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.books, "audioPrice", null);
                  _this7.$set(response.data.data.books, "videoPrice", null);
                  _this7.$set(response.data.data.books, "webinarPrice", null);
                  _this7.$set(response.data.data.books, "streamPrice", null);
                  _this7.$set(response.data.data.books, "subscribePrice", null);
                  _this7.$set(response.data.data.books, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.books;

                  break;
                case "White Paper":
                  _this7.$set(response.data.data.whitePaper, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.whitePaper, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.whitePaper, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.whitePaper, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.whitePaper, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.whitePaper, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.whitePaper, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.whitePaper, "audioPrice", null);
                  _this7.$set(response.data.data.whitePaper, "videoPrice", null);
                  _this7.$set(response.data.data.whitePaper, "webinarPrice", null);
                  _this7.$set(response.data.data.whitePaper, "streamPrice", null);
                  _this7.$set(response.data.data.whitePaper, "subscribePrice", null);
                  _this7.$set(response.data.data.whitePaper, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.whitePaper;
                  break;
                case "Market Reports":
                  _this7.$set(response.data.data.marketReport, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.marketReport, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.marketReport, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.marketReport, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.marketReport, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.marketReport, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.marketReport, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.marketReport, "audioPrice", null);
                  _this7.$set(response.data.data.marketReport, "videoPrice", null);
                  _this7.$set(response.data.data.marketReport, "webinarPrice", null);
                  _this7.$set(response.data.data.marketReport, "streamPrice", null);
                  _this7.$set(response.data.data.marketReport, "subscribePrice", null);
                  _this7.$set(response.data.data.marketReport, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.marketReport;
                  break;
                case "Market Research":
                  _this7.$set(response.data.data.marketResearch, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.marketResearch, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.marketResearch, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.marketResearch, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.marketResearch, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.marketResearch, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.marketResearch, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.marketResearch, "audioPrice", null);
                  _this7.$set(response.data.data.marketResearch, "videoPrice", null);
                  _this7.$set(response.data.data.marketResearch, "webinarPrice", null);
                  _this7.$set(response.data.data.marketResearch, "streamPrice", null);
                  _this7.$set(response.data.data.marketResearch, "subscribePrice", null);
                  _this7.$set(response.data.data.marketResearch, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.marketResearch;
                  break;
                case "Webinar":
                  _this7.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.webinar, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.webinar, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.webinar, "readOnlinePrice", null);
                  _this7.$set(response.data.data.webinar, "hardCopyPrice", null);
                  _this7.$set(response.data.data.webinar, "softCopyPrice", null);
                  _this7.$set(response.data.data.webinar, "audioPrice", null);
                  _this7.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                  _this7.$set(response.data.data.webinar, "videoPrice", null);
                  _this7.$set(response.data.data.webinar, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.webinar;
                  break;
                case "Podcast":
                  _this7.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.webinar, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.webinar, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.webinar, "readOnlinePrice", response.data.data.readOnlinePrice);
                  _this7.$set(response.data.data.webinar, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.webinar, "softCopyPrice", null);
                  _this7.$set(response.data.data.webinar, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
                  _this7.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                  _this7.$set(response.data.data.webinar, "streamPrice", response.data.data.streamPrice ? response.data.data.streamPrice : null);
                  _this7.$set(response.data.data.webinar, "subscribePrice", response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
                  _this7.$set(response.data.data.webinar, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
                  _this7.$set(response.data.data.webinar, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.webinar;
                  break;
                case "Videos":
                  _this7.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.webinar, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.webinar, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.webinar, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.webinar, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.webinar, "softCopyPrice", null);
                  _this7.$set(response.data.data.webinar, "audioPrice", null);
                  _this7.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                  _this7.$set(response.data.data.webinar, "streamPrice", response.data.data.streamPrice ? response.data.data.streamPrice : null);
                  _this7.$set(response.data.data.webinar, "subscribePrice", response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
                  _this7.$set(response.data.data.webinar, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
                  _this7.$set(response.data.data.webinar, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.webinar;
                  break;
                case "Journals":
                  _this7.$set(response.data.data.journal, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.journal, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.journal, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.journal, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.journal, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.journal, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.journal, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.journal, "audioPrice", null);
                  _this7.$set(response.data.data.journal, "videoPrice", null);
                  _this7.$set(response.data.data.journal, "webinarPrice", null);
                  _this7.$set(response.data.data.journal, "streamPrice", null);
                  _this7.$set(response.data.data.journal, "subscribePrice", null);
                  _this7.$set(response.data.data.journal, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.journal;
                  break;
                case "Courses":
                  _this7.$set(response.data.data.courses, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.courses, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.courses, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.courses, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.courses, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.courses, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.courses, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.courses, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
                  _this7.$set(response.data.data.courses, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
                  _this7.$set(response.data.data.courses, "webinarPrice", null);
                  _this7.$set(response.data.data.courses, "streamPrice", null);
                  _this7.$set(response.data.data.courses, "subscribePrice", null);
                  _this7.$set(response.data.data.courses, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.courses;
                  break;
                case "Articles":
                  _this7.$set(response.data.data.articles, "vendor", response.data.data.vendor);
                  _this7.$set(response.data.data.articles, "prodType", response.data.data.prodType);
                  _this7.$set(response.data.data.articles, "uniId", response.data.data.id);
                  _this7.$set(response.data.data.articles, "coverImage", response.data.data.coverImage);
                  _this7.$set(response.data.data.articles, "verify", response.data.data.verify);
                  _this7.$set(response.data.data.articles, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                  _this7.$set(response.data.data.articles, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                  _this7.$set(response.data.data.articles, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                  _this7.$set(response.data.data.articles, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
                  _this7.$set(response.data.data.articles, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
                  _this7.$set(response.data.data.articles, "webinarPrice", null);
                  _this7.$set(response.data.data.articles, "streamPrice", null);
                  _this7.$set(response.data.data.articles, "subscribePrice", null);
                  _this7.$set(response.data.data.articles, "industry", response.data.data.industry.name);
                  emptyObject = response.data.data.articles;
                  break;
                default:

                  return false;
              }
              _this7.topContent = emptyObject;
            }
            _this7.isActive = false;
          });
        }
      }
    });
    axios.get("/api/get-top-industry/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data.length > 0) {
          _this7.topIndustryArr = response.data.sort(_this7.compare);
        }
      }
    });
  },
  getLikes: function getLikes() {
    var _this8 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-likes/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        _this8.likes = response.data;
      }
    });
  },
  getAges: function getAges() {
    var _this9 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));

    axios.get("/api/get-ages/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        if (response.data) {
          _this9.ages = response.data;
          var first = [];
          var second = [];
          var third = [];
          var fourth = [];
          var fifth = [];

          _this9.ages.forEach(function (item) {
            if (item.first === 1) {
              first.push(item);
            }
            if (item.second === 1) {
              second.push(item);
            }
            if (item.third === 1) {
              third.push(item);
            }
            if (item.fourth === 1) {
              fourth.push(item);
            }
            if (item.fifth === 1) {
              fifth.push(item);
            }
          });

          _this9.age_values = [[first.length], [second.length], [third.length], [fourth.length], [fifth.length]];
        }
      }
    });
  },
  getActivity: function getActivity() {
    var _this10 = this;

    var reducer = function reducer(accumulator, currentValue) {
      return accumulator + currentValue;
    };

    var currentWeek = this.$moment().week();
    var currentMonth = this.$moment().month();
    var visits = [];
    var views = [];
    var viewTotal = [];
    var visitTotal = [];

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/get-activity/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        _this10.activity = response.data;
        _this10.activity.forEach(function (item) {

          visitTotal.push(item.profile_visits);
          viewTotal.push(item.content_views);
          if (_this10.$moment(item.created_at).week() === currentWeek) {
            _this10.weeklyActivity.push(item);
          }
        });

        _this10.weeklyActivity.forEach(function (item) {
          visits.push(item.profile_visits);
          views.push(item.content_views);
        });
        _this10.weeklyViews = views.reduce(reducer, 0);
        _this10.weeklyVisits = visits.reduce(reducer, 0);
        _this10.totalViews = viewTotal.reduce(reducer, 0);
        _this10.totalVisits = visitTotal.reduce(reducer, 0);
      }
    });

    axios.get("/api/get-reach/" + vendor.id).then(function (response) {
      _this10.reach = response.data;
      _this10.reach.forEach(function (item) {
        if (_this10.$moment(item.created_at).week() === currentWeek) {
          _this10.weeklyReach.push(item);
        }
      });
    });
  },
  getVendorProducts: function getVendorProducts() {
    var _this11 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    if (vendor != null) {
      var vendorid = vendor.id;
      this.token = vendor.access_token;
      axios.get("/api/vendorproduct/" + vendorid, {
        headers: { Authorization: "Bearer " + vendor.access_token }
      }).then(function (response) {

        if (response.data.data.length > 0) {
          var emptyArray = [];

          response.data.data.forEach(function (item) {
            switch (item.prodType) {
              case "Books":
                _this11.$set(item.books, "verify", item.verify);
                _this11.$set(item.books, "uniId", item.id);
                _this11.$set(item.books, "prodType", item.prodType);
                _this11.$set(item.books, "created_at", item.myDate);
                _this11.$set(item.books, "coverImage", item.coverImage);
                emptyArray.push(item.books);
                break;
              case "White Paper":
                _this11.$set(item.whitePaper, "verify", item.verify);
                _this11.$set(item.whitePaper, "uniId", item.id);
                _this11.$set(item.whitePaper, "prodType", item.prodType);
                _this11.$set(item.whitePaper, "created_at", item.myDate);
                _this11.$set(item.whitePaper, "coverImage", item.coverImage);
                emptyArray.push(item.whitePaper);
                break;
              case "Market Reports":
                _this11.$set(item.marketReport, "verify", item.verify);
                _this11.$set(item.marketReport, "uniId", item.id);
                _this11.$set(item.marketReport, "prodType", item.prodType);
                _this11.$set(item.marketReport, "created_at", item.myDate);
                _this11.$set(item.marketReport, "coverImage", item.coverImage);
                emptyArray.push(item.marketReport);
                break;
              case "Market Research":
                _this11.$set(item.marketResearch, "verify", item.verify);
                _this11.$set(item.marketResearch, "uniId", item.id);
                _this11.$set(item.marketResearch, "prodType", item.prodType);
                _this11.$set(item.marketResearch, "created_at", item.myDate);
                _this11.$set(item.marketResearch, "coverImage", item.coverImage);
                emptyArray.push(item.marketResearch);
                break;
              case "Webinar":
                _this11.$set(item.webinar, "verify", item.verify);
                _this11.$set(item.webinar, "uniId", item.id);
                _this11.$set(item.webinar, "prodType", item.prodType);
                _this11.$set(item.webinar, "created_at", item.myDate);
                _this11.$set(item.webinar, "coverImage", item.coverImage);
                emptyArray.push(item.webinar);
                break;
              case "Podcast":
                _this11.$set(item.webinar, "verify", item.verify);
                _this11.$set(item.webinar, "uniId", item.id);
                _this11.$set(item.webinar, "prodType", item.prodType);
                _this11.$set(item.webinar, "created_at", item.myDate);
                _this11.$set(item.webinar, "coverImage", item.coverImage);
                emptyArray.push(item.webinar);
                break;
              case "Videos":
                _this11.$set(item.webinar, "verify", item.verify);
                _this11.$set(item.webinar, "uniId", item.id);
                _this11.$set(item.webinar, "prodType", item.prodType);
                _this11.$set(item.webinar, "created_at", item.myDate);
                _this11.$set(item.webinar, "created_at", item.coverImage);
                emptyArray.push(item.webinar);
                break;
              case "Journals":
                _this11.$set(item.journal, "verify", item.verify);
                _this11.$set(item.journal, "uniId", item.id);
                _this11.$set(item.journal, "prodType", item.prodType);
                _this11.$set(item.journal, "created_at", item.myDate);
                _this11.$set(item.journal, "coverImage", item.coverImage);
                emptyArray.push(item.journal);
                break;
              case "Articles":
                _this11.$set(item.articles, "verify", item.verify);
                _this11.$set(item.articles, "uniId", item.id);
                _this11.$set(item.articles, "prodType", item.prodType);
                _this11.$set(item.articles, "created_at", item.myDate);
                _this11.$set(item.articles, "coverImage", item.coverImage);
                emptyArray.push(item.articles);
                break;
              case "Courses":
                _this11.$set(item.courses, "verify", item.verify);
                _this11.$set(item.courses, "uniId", item.id);
                _this11.$set(item.courses, "prodType", item.prodType);
                _this11.$set(item.courses, "created_at", item.myDate);
                _this11.$set(item.courses, "coverImage", item.coverImage);
                emptyArray.push(item.courses);
                break;
              default:
                return false;
            }
          });

          _this11.allContents = emptyArray;
        } else {
          _this11.contents = [];
        }
        //  this.products = response.data.data;
      }).catch(function (error) {
        console.log(error);
      });
    }
  },
  getFollowers: function getFollowers() {
    var _this12 = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    axios.get("/api/getfollowers/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        _this12.followNo = response.data;
      }
    });
    axios.get("/api/getfollowers-weekly/" + vendor.id).then(function (response) {
      if (response.status === 200) {
        _this12.weeklyFollow = response.data;
      }
    });
  },

  dataFormat: function dataFormat(a, b) {
    if (b) return b + "%";
    return a;
  },
  switchTab: function switchTab(tab) {
    switch (tab) {
      case "schedule":
        this.first = true;
        this.second = this.third = false;
        break;
      case "activity":
        this.second = true;
        this.first = this.third = false;

        break;

      case "audience":
        this.third = true;
        this.second = this.first = false;

        break;

      default:
        break;
    }
  }
}), _props$name$data$befo);

/***/ }),

/***/ 1094:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", [
          _vm.openCampaign
            ? _c("div", { staticClass: "campaign-overlay" }, [
                _c("div", { staticClass: "campaign w-70 rounded p-5" }, [
                  _c("i", {
                    staticClass:
                      "fa fa-minus-circle fa-2x closeCamp bg-white rounded",
                    attrs: { "aria-hidden": "true" },
                    on: { click: _vm.openCamp }
                  }),
                  _vm._v(" "),
                  _c(
                    "form",
                    {
                      staticClass: "form p-3 rounded",
                      attrs: { action: "" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.sendCamp($event)
                        }
                      }
                    },
                    [
                      _c("legend", { staticClass: "text-center" }, [
                        _vm._v("Send Campaign")
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "form-group d-flex align-items-start" },
                        [
                          _c(
                            "label",
                            { staticClass: "w-25", attrs: { for: "" } },
                            [_vm._v("Subject")]
                          ),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.campaign.subject,
                                expression: "campaign.subject"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              "aria-describedby": "helpId",
                              placeholder: "Your Subject",
                              required: ""
                            },
                            domProps: { value: _vm.campaign.subject },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.campaign,
                                  "subject",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "form-group d-flex align-items-start" },
                        [
                          _c(
                            "label",
                            { staticClass: "w-25", attrs: { for: "" } },
                            [_vm._v("From")]
                          ),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.campaign.from_name,
                                expression: "campaign.from_name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              "aria-describedby": "helpId",
                              placeholder: "From",
                              readonly: "",
                              required: ""
                            },
                            domProps: { value: _vm.campaign.from_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.campaign,
                                  "from_name",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "form-group d-flex align-items-start" },
                        [
                          _c(
                            "label",
                            { staticClass: "w-25", attrs: { for: "" } },
                            [_vm._v("Message")]
                          ),
                          _vm._v(" "),
                          _c("app-editor", {
                            staticClass: "form-control",
                            attrs: {
                              init: {
                                plugins:
                                  "advlist autolink lists link image imagetools charmap print preview anchor textcolor insertdatetime media table paste code help wordcount",

                                toolbar:
                                  "undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat |image help",
                                image_title: true,

                                height: 200,
                                file_picker_types: "image",
                                automatic_uploads: false,
                                relative_urls: false,
                                convert_urls: false,
                                images_upload_handler: function(
                                  blobInfo,
                                  success,
                                  failure
                                ) {
                                  _vm.upload_handler(blobInfo, success, failure)
                                },
                                file_picker_callback: function(
                                  callback,
                                  value,
                                  meta
                                ) {
                                  _vm.loadFile(callback, value)
                                }
                              }
                            },
                            model: {
                              value: _vm.campaign.message,
                              callback: function($$v) {
                                _vm.$set(_vm.campaign, "message", $$v)
                              },
                              expression: "campaign.message"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: " btn-primary rounded ml-auto",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Send Campaign")]
                      )
                    ]
                  )
                ])
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticStyle: { positon: "relative" } }, [
            _c("div", { staticClass: "top-nav" }, [
              _c(
                "div",
                {
                  staticClass: "select_nav text-center",
                  class: { selected_nav: _vm.first },
                  on: {
                    click: function($event) {
                      return _vm.switchTab("schedule")
                    }
                  }
                },
                [_vm._v("Content")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "select_nav text-center",
                  class: { selected_nav: _vm.second },
                  on: {
                    click: function($event) {
                      return _vm.switchTab("activity")
                    }
                  }
                },
                [_vm._v("Activity")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "select_nav text-center",
                  class: { selected_nav: _vm.third },
                  on: {
                    click: function($event) {
                      return _vm.switchTab("audience")
                    }
                  }
                },
                [_vm._v("Audience")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "main_content" }, [
              _vm.first
                ? _c("div", { staticClass: "w-100 py-3" }, [
                    _c(
                      "div",
                      {
                        staticClass:
                          "row flex-column flex-sm-row flex-md-row flex-lg-row justify-content-between"
                      },
                      [
                        _c("div", { staticClass: "schedule_1" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "schedule_box rounded shadow-sm border-bottom"
                            },
                            [
                              _vm._m(1),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "h-100 d-flex justify-content-center align-items-center flex-column"
                                },
                                [
                                  _c("div", { staticClass: "text-center" }, [
                                    _c("p", { staticClass: "text-muted" }, [
                                      _vm._v(
                                        _vm._s(_vm.contents.length) +
                                          " active contents"
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _vm.contents.length > 0
                                    ? _c(
                                        "div",
                                        { staticClass: "myContents" },
                                        _vm._l(_vm.contents, function(
                                          content,
                                          idx
                                        ) {
                                          return _c(
                                            "div",
                                            {
                                              key: idx,
                                              staticClass: "flip-card myContent"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass: "flip-card-inner"
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "flip-card-front"
                                                    },
                                                    [
                                                      _c("img", {
                                                        staticClass:
                                                          "shadow-sm rounded",
                                                        attrs: {
                                                          src:
                                                            content.coverImage,
                                                          alt: "image"
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "p",
                                                        {
                                                          staticClass:
                                                            "small toCaps"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              content.title.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "flip-card-back"
                                                    },
                                                    [
                                                      _c(
                                                        "h4",
                                                        {
                                                          staticClass:
                                                            "tiny-text"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              content.prodType
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "p",
                                                        {
                                                          staticClass:
                                                            "tiny-text toCaps"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              content.title.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        }),
                                        0
                                      )
                                    : _vm._e()
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                { attrs: { to: "/vendor/add/category" } },
                                [
                                  _c("h5", { staticClass: "schedule_create" }, [
                                    _vm._v("Create content")
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                { attrs: { to: "/vendor/all" } },
                                [
                                  _c(
                                    "h5",
                                    { staticClass: "schedule_show_all" },
                                    [_vm._v("Show all")]
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "schedule_box rounded shadow-sm" },
                            [
                              _vm._m(2),
                              _vm._v(" "),
                              _vm._m(3),
                              _vm._v(" "),
                              _c(
                                "h5",
                                {
                                  staticClass: "schedule_create",
                                  on: { click: _vm.openCamp }
                                },
                                [_vm._v("create campaign")]
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "schedule_2 rounded" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "top_box rounded shadow-sm border-bottom"
                            },
                            [
                              _vm._m(4),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "h-100 d-flex flex-direction justify-content-center align-items-center flex-column py-3"
                                },
                                [
                                  _vm.topContentArr.length > 0
                                    ? _c(
                                        "div",
                                        {
                                          staticClass: "topContent text-center"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "topImage" },
                                            [
                                              _c("img", {
                                                staticClass: " rounded",
                                                attrs: {
                                                  src:
                                                    _vm.topContent.coverImage,
                                                  alt: "image"
                                                }
                                              })
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "topText" },
                                            [
                                              _c("h4", [
                                                _vm._v(
                                                  "\n                       " +
                                                    _vm._s(
                                                      _vm.topContent.title
                                                    ) +
                                                    "\n                      "
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "p",
                                                { staticClass: "toCaps" },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      staticClass: "text-muted"
                                                    },
                                                    [_vm._v("Industry :")]
                                                  ),
                                                  _vm._v(
                                                    "\n                        " +
                                                      _vm._s(
                                                        _vm.topContent.industry
                                                      ) +
                                                      "\n                      "
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("p", [
                                                _c(
                                                  "span",
                                                  { staticClass: "text-muted" },
                                                  [_vm._v("Views :")]
                                                ),
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.topContentArr[0].count
                                                    ) +
                                                    "\n                      "
                                                )
                                              ])
                                            ]
                                          )
                                        ]
                                      )
                                    : _c("h4", { staticClass: "text-muted" }, [
                                        _vm._v("No Content Yet")
                                      ])
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _vm.showChecklist && !_vm.submitContenDone
                            ? _c(
                                "div",
                                {
                                  staticClass: "p-2 mt-3 animated",
                                  class: { fadeOut: _vm.submitContenDone }
                                },
                                [
                                  _vm._m(5),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "p-2 text-left bg-white rounded",
                                      staticStyle: { "font-size": "14px" }
                                    },
                                    [
                                      _c("div", { staticClass: "form-group" }, [
                                        _vm._v(
                                          "\n                    I have uploaded my insight for the week\n                    "
                                        ),
                                        _c(
                                          "div",
                                          { staticClass: "form-check" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass: "form-check-label"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.response.first,
                                                      expression:
                                                        "response.first"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "form-check-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value:
                                                      " I have uploaded my insight for the week - yes",
                                                    "aria-label":
                                                      "  I have uploaded my insight for the week"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      _vm.response.first,
                                                      " I have uploaded my insight for the week - yes"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        _vm.response,
                                                        "first",
                                                        " I have uploaded my insight for the week - yes"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(
                                                  "\n                        Yes\n                      "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "label",
                                              {
                                                staticClass: "form-check-label"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.response.first,
                                                      expression:
                                                        "response.first"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "form-check-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value:
                                                      " I have uploaded my insight for the week - no",
                                                    "aria-label":
                                                      "  I have uploaded my insight for the week"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      _vm.response.first,
                                                      " I have uploaded my insight for the week - no"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        _vm.response,
                                                        "first",
                                                        " I have uploaded my insight for the week - no"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(
                                                  "\n                        No\n                      "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "form-group" }, [
                                        _vm._v(
                                          "\n                    I have shared at least one post about BizGuruh with my community\n                    "
                                        ),
                                        _c(
                                          "div",
                                          { staticClass: "form-check" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass: "form-check-label"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value:
                                                        _vm.response.second,
                                                      expression:
                                                        "response.second"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "form-check-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value:
                                                      " I have shared at least one post about BizGuruh with my community - Yes",
                                                    "aria-label":
                                                      " I have shared at least one post about BizGuruh with my community "
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      _vm.response.second,
                                                      " I have shared at least one post about BizGuruh with my community - Yes"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        _vm.response,
                                                        "second",
                                                        " I have shared at least one post about BizGuruh with my community - Yes"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(
                                                  "\n                        Yes\n                      "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "label",
                                              {
                                                staticClass: "form-check-label"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value:
                                                        _vm.response.second,
                                                      expression:
                                                        "response.second"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "form-check-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value:
                                                      " I have shared at least one post about BizGuruh with my community - No",
                                                    "aria-label": " "
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      _vm.response.second,
                                                      " I have shared at least one post about BizGuruh with my community - No"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        _vm.response,
                                                        "second",
                                                        " I have shared at least one post about BizGuruh with my community - No"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(
                                                  "\n                        No\n                      "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", {}, [
                                        _c(
                                          "button",
                                          {
                                            staticClass:
                                              "btn-primary btn-sm ml-auto",
                                            attrs: { type: "button" },
                                            on: { click: _vm.submitCheck }
                                          },
                                          [_vm._v("Submit")]
                                        )
                                      ])
                                    ]
                                  )
                                ]
                              )
                            : _vm._e()
                        ])
                      ]
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.second
                ? _c("div", { staticClass: "w-100 py-3" }, [
                    _c("div", { staticClass: "row flex-column" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "activity_box rounded shadow-sm border-bottom"
                        },
                        [
                          _vm._m(6),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.totalVisits) +
                                    "\n                  times profile was visited\n                "
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.weeklyVisits) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("times profile was visited this week")
                                ])
                              ])
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "activity_box rounded shadow-sm border-bottom"
                        },
                        [
                          _vm._m(7),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  _vm._s(_vm.reach.length) + " total reaches"
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.weeklyReach.length) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("reaches this week")
                                ])
                              ])
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "activity_box rounded shadow-sm border-bottom"
                        },
                        [
                          _vm._m(8),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.totalViews) +
                                    "\n                 times content was viewed\n                "
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.weeklyViews) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("times content was viewed this week")
                                ])
                              ])
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "activity_box rounded shadow-sm" },
                        [
                          _vm._m(9),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c("p", { staticClass: "display-5" }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.interactions) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("interactions on your content")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("p", {}, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.myView) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("Views")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("p", {}, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.myLik) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("Likes")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("p", {}, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(_vm.myShares) +
                                    "\n                  "
                                ),
                                _c("small", { staticClass: "text-muted" }, [
                                  _vm._v("Shares")
                                ])
                              ])
                            ]
                          )
                        ]
                      )
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.third
                ? _c("div", { staticClass: "w-100 py-3" }, [
                    _c("div", { staticClass: "row flex-column" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(10),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c("p", { staticClass: "display-5 mb-2" }, [
                                _vm._v(_vm._s(_vm.followNo) + " Follower(s)")
                              ]),
                              _vm._v(" "),
                              _c(
                                "p",
                                { staticClass: "display-5 mb-2 text-muted" },
                                [
                                  _vm._v(
                                    _vm._s(_vm.weeklyFollow) +
                                      " new Follower(s) this week"
                                  )
                                ]
                              )
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(11),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column desktop"
                            },
                            [
                              _c(
                                "graph-bar",
                                {
                                  attrs: {
                                    width: 600,
                                    height: 250,
                                    "axis-min": 0,
                                    "axis-max": 50,
                                    labels: ["Age ranges"],
                                    values: _vm.age_values
                                  }
                                },
                                [
                                  _c("note", { attrs: { text: "Age Chart" } }),
                                  _vm._v(" "),
                                  _c("tooltip", {
                                    attrs: {
                                      names: _vm.age_range,
                                      position: "left"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("legends", {
                                    attrs: {
                                      names: _vm.age_range,
                                      filter: true
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column mobile"
                            },
                            [
                              _c(
                                "graph-bar",
                                {
                                  attrs: {
                                    width: 330,
                                    height: 270,
                                    "axis-min": 0,
                                    "axis-max": 50,
                                    labels: ["Age ranges"],
                                    values: _vm.age_values
                                  }
                                },
                                [
                                  _c("note", { attrs: { text: "Age Chart" } }),
                                  _vm._v(" "),
                                  _c("tooltip", {
                                    attrs: {
                                      names: _vm.age_range,
                                      position: "left"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("legends", {
                                    attrs: {
                                      names: _vm.age_range,
                                      filter: true
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(12),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column flex-lg-row flex-md-row"
                            },
                            [
                              _vm.genders === null
                                ? _c("h4", { staticClass: "text-muted" }, [
                                    _vm._v("No Content Yet")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _c("div", { staticClass: "p-4" }, [
                                _c("p", [
                                  _vm._v("Male(s) : " + _vm._s(_vm.male.length))
                                ]),
                                _vm._v(" "),
                                _c("p", [
                                  _vm._v(
                                    "Female(s) : " + _vm._s(_vm.female.length)
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "graph-pie",
                                {
                                  attrs: {
                                    width: 300,
                                    height: 300,
                                    "padding-top": 60,
                                    "padding-bottom": 60,
                                    "padding-left": 60,
                                    "padding-right": 60,
                                    values: _vm.gender_values,
                                    names: _vm.gender_names,
                                    "active-index": [0, 2],
                                    "active-event": "click",
                                    "show-text-type": "outside",
                                    "data-format": _vm.dataFormat
                                  }
                                },
                                [
                                  _c("legends", {
                                    attrs: { names: _vm.gender_names }
                                  }),
                                  _vm._v(" "),
                                  _c("tooltip", {
                                    attrs: { names: _vm.gender_names }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(13),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "h-100 d-flex justify-content-center align-items-center flex-column p-3 w-100"
                                },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "display-5 toCaps" },
                                    [
                                      _vm._m(14),
                                      _vm._v(
                                        "\n                    " +
                                          _vm._s(_vm.topContent.industry) +
                                          "\n                  "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _vm.topContentArr.length > 0
                                    ? _c("div", { staticClass: "topContent" }, [
                                        _c("div", { staticClass: "topText" }, [
                                          _c("p", [
                                            _c(
                                              "span",
                                              { staticClass: "text-muted" },
                                              [_vm._v("Title :")]
                                            ),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(_vm.topContent.title)
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("p", [
                                            _c(
                                              "span",
                                              { staticClass: "text-muted" },
                                              [_vm._v("Views :")]
                                            ),
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.topContentArr[0].count
                                                ) +
                                                "\n                      "
                                            )
                                          ])
                                        ])
                                      ])
                                    : _c("h4", { staticClass: "text-muted" }, [
                                        _vm._v("No Content Yet")
                                      ])
                                ]
                              )
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(15),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "h-100 d-flex justify-content-center align-items-center flex-column p-3 w-100"
                                },
                                [
                                  _vm.topContentArr.length > 0
                                    ? _c("div", { staticClass: "topContent" }, [
                                        _c("div", { staticClass: "topImage" }, [
                                          _c("img", {
                                            staticClass: "shadow-sm rounded",
                                            attrs: {
                                              src: _vm.topContent.coverImage,
                                              alt: "image"
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "topText" }, [
                                          _c(
                                            "p",
                                            { staticClass: "display-5" },
                                            [
                                              _c(
                                                "span",
                                                { staticClass: "text-muted" },
                                                [_vm._v("Title :")]
                                              ),
                                              _vm._v(" "),
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(_vm.topContent.title)
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("p", [
                                            _c(
                                              "span",
                                              { staticClass: "text-muted" },
                                              [_vm._v("Category :")]
                                            ),
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.topContent.prodType
                                                ) +
                                                "\n                      "
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("p", [
                                            _c(
                                              "span",
                                              { staticClass: "text-muted" },
                                              [_vm._v("Views :")]
                                            ),
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.topContentArr[0].count
                                                ) +
                                                "\n                      "
                                            )
                                          ])
                                        ])
                                      ])
                                    : _c("h4", { staticClass: "text-muted" }, [
                                        _vm._v("No Content Yet")
                                      ])
                                ]
                              )
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "audience_box shadow-sm rounded border-bottom"
                        },
                        [
                          _vm._m(16),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "h-100 d-flex justify-content-center align-items-center flex-column"
                            },
                            [
                              _vm.locations.length < 0
                                ? _c("h4", { staticClass: "text-muted" }, [
                                    _vm._v("No Content Yet")
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "w-100 d-flex justify-content-around align-items-center desktop"
                                },
                                [
                                  _c("div", [
                                    _c(
                                      "ul",
                                      [
                                        _vm._m(17),
                                        _vm._v(" "),
                                        _vm._l(_vm.realLocations, function(
                                          location,
                                          idx
                                        ) {
                                          return _c(
                                            "li",
                                            { key: idx, staticClass: "toCaps" },
                                            [
                                              _vm._v(
                                                _vm._s(idx + 1) +
                                                  ". " +
                                                  _vm._s(location.el)
                                              )
                                            ]
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "graph-bar",
                                    {
                                      attrs: {
                                        width: 400,
                                        height: 300,
                                        "axis-min": 0,
                                        "axis-max": 50,
                                        labels: _vm.location_names,
                                        values: _vm.location_values
                                      }
                                    },
                                    [
                                      _c("tooltip", {
                                        attrs: {
                                          names: _vm.location_names,
                                          position: "left"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "w-100 d-flex justify-content-around align-items-center mobile"
                                },
                                [
                                  _c("div", [
                                    _c(
                                      "ul",
                                      [
                                        _vm._m(18),
                                        _vm._v(" "),
                                        _vm._l(_vm.realLocations, function(
                                          location,
                                          idx
                                        ) {
                                          return _c(
                                            "li",
                                            { key: idx, staticClass: "toCaps" },
                                            [
                                              _vm._v(
                                                _vm._s(idx + 1) +
                                                  ". " +
                                                  _vm._s(location.el)
                                              )
                                            ]
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "graph-bar",
                                    {
                                      attrs: {
                                        width: 330,
                                        height: 270,
                                        "axis-min": 0,
                                        "axis-max": 50,
                                        labels: _vm.location_names,
                                        values: _vm.location_values
                                      }
                                    },
                                    [
                                      _c("tooltip", {
                                        attrs: {
                                          names: _vm.location_names,
                                          position: "left"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        ]
                      )
                    ])
                  ])
                : _vm._e()
            ])
          ])
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", { staticClass: "mb-1" }, [_vm._v("Overview")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: ""
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Campaigns")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: ""
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "h-100 d-flex justify-content-center align-items-center flex-column"
      },
      [
        _c("i", { staticClass: "fas fa-chart-line fa-6x text-muted" }),
        _vm._v(" "),
        _c("h4", { staticClass: "text-muted" }, [_vm._v("No Campaign Yet")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex justify-content-center" }, [
      _c("h4", [_vm._v("Top content this week")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: ""
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("b", [
      _c("small", { staticClass: "mb-2 text-muted" }, [
        _vm._v(
          "Checklists— to monitor adherence to partner commitment (Available fridays & saturdays)"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Profile Visits")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "  number of times the profile was viewed"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Reach")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "   — unique accounts that have seen your posts"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Impression")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "   — Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Interactions")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "— Number of times your posts have been seen,liked and shared"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("No. of followers")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Age range")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Gender")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Top Industry")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("small", [_vm._v("Most viewed :")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Top content")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "head_flex" }, [
      _c("h4", [_vm._v("Top Locations")]),
      _vm._v(" "),
      _c(
        "span",
        {
          staticClass: "ml-2 fa-stack info fa-1x info_icon",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title: "— Number of times your posts have been seen"
          }
        },
        [
          _c("i", { staticClass: "far fa-circle text-black fa-stack-2x" }),
          _vm._v(" "),
          _c("i", { staticClass: "fas fa-info text-black fa-stack-1x" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [_c("h4", [_vm._v("Location Ranking")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [_c("h4", [_vm._v("Location Ranking")])])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7288f46a", module.exports)
  }
}

/***/ }),

/***/ 490:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1091)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1093)
/* template */
var __vue_template__ = __webpack_require__(1094)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7288f46a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorStatisticPageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7288f46a", Component.options)
  } else {
    hotAPI.reload("data-v-7288f46a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ })

});