webpackJsonp([106],{

/***/ 1444:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1445);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("81895f8c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-02337fa7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userSuccessComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-02337fa7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userSuccessComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1445:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n/* .course-Detail {\n        margin: 200px 50px 0 50px !important;\n    }*/\n.fa-2x[data-v-02337fa7] {\n  font-size: 1em !important;\n}\n.fa-inverse[data-v-02337fa7] {\n  color: green;\n}\n.fa-check[data-v-02337fa7] {\n  color: white;\n}\n.successRedirect[data-v-02337fa7] {\n  width: 80%;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 100px;\n  text-align: center;\n  font-weight: bold;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  height: 80vh;\n}\n.alert-secondary[data-v-02337fa7] {\n  background: #a3c2dc;\n}\na[data-v-02337fa7] {\n  color: #ffffff !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 1446:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-success-component",
  data: function data() {
    return {
      success: true,
      stop: null
    };
  },

  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.verify();
    });
  },
  mounted: function mounted() {},

  methods: {
    verify: function verify() {
      var _this = this;

      this.stop = false;
      var type = localStorage.getItem("type");
      var user;
      if (type == "accounting") {
        user = JSON.parse(localStorage.getItem("accountUser"));
      } else if (type == "entrepreneur") {
        user = JSON.parse(localStorage.getItem("authUser"));
      } else if (type == "expert") {
        user = JSON.parse(localStorage.getItem("authVendor"));
      }

      if (user != null) {
        if (this.$route.query.trxref !== null) {
          localStorage.removeItem("userCart");
        }
        axios.get("api/user/verify-order/" + this.$route.query.trxref + "/" + type + "/" + user.id).then(function (response) {
          if (response.status === 200) {
            _this.stop = true;
            _this.$emit("clearCartCount", 0);
            localStorage.getItem("type");
            _this.success = true;
            //   if (localStorage.getItem('reloaded')) {
            // The page was just reloaded. Clear the value from local storage
            // so that it will reload the next time this page is visited.
            //     localStorage.removeItem('reloaded');
            // } else {
            // Set a flag so that we know not to reload the page twice.
            //     localStorage.setItem('reloaded', '1');
            //     location.reload();
            // }

            _this.$emit("clearCartCount", 0);
            _this.$emit("getCart");
          }
        }).catch(function (error) {
          console.log(error);
          _this.success = false;
        });
      }
    }
  }
});

/***/ }),

/***/ 1447:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "course-Detail" }, [
    _vm.success
      ? _c("div", { staticClass: "successRedirect" }, [
          !_vm.stop
            ? _c("div", { staticClass: "text-center mb-4" }, [
                _c("h2", { staticClass: "mb-3" }, [
                  _vm._v("Verifying payment")
                ]),
                _vm._v(" "),
                _c("i", { staticClass: "fas fa-spinner spinner fa-spin fa-5x" })
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.stop
            ? _c("div", { staticClass: "mb-4 text-center" }, [
                _c("h2", { staticClass: " mb-3" }, [
                  _vm._v("Payment Successful")
                ]),
                _vm._v(" "),
                _vm._m(0)
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.stop
            ? _c(
                "div",
                { staticClass: "alert alert-success" },
                [
                  _c("router-link", { attrs: { to: "/" } }, [
                    _vm._v("Visit Home")
                  ])
                ],
                1
              )
            : _vm._e()
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.success
      ? _c("div", { staticClass: "successRedirect" }, [_vm._m(1)])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("span", { staticClass: "fa-stack fa-5x mb-4 animated fadeIn slow" }, [
        _c("i", {
          staticClass: "fas fa-circle fa-inverse fa-stack-2x",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" "),
        _c("i", { staticClass: "fas fa-check fa-stack-1x" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-4 text-center" }, [
      _c("h2", { staticClass: " mb-3" }, [_vm._v("Payment failed")]),
      _vm._v(" "),
      _c("div", [
        _c(
          "span",
          { staticClass: "fa-stack fa-5x mb-4 animated fadeIn slow" },
          [
            _c("i", {
              staticClass: "fas fa-circle text-darek fa-stack-2x",
              attrs: { "aria-hidden": "true" }
            }),
            _vm._v(" "),
            _c("i", { staticClass: "fas fa-minus fa-stack-1x" })
          ]
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-02337fa7", module.exports)
  }
}

/***/ }),

/***/ 565:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1444)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1446)
/* template */
var __vue_template__ = __webpack_require__(1447)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-02337fa7"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userSuccessComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-02337fa7", Component.options)
  } else {
    hotAPI.reload("data-v-02337fa7", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});