webpackJsonp([156],{

/***/ 1368:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1369);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("035ceb4f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45da1375\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editChecklist.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45da1375\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editChecklist.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1369:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-45da1375] {\n  padding: 80px 15px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  min-height: 100vh;\n  width: 100%;\n}\n.content-wrapper[data-v-45da1375] {\n  height: 100vh;\n}\n.main-page[data-v-45da1375] {\n  padding: 40px 20px;\n  max-height: 100vh;\n  height: 100vh;\n  padding: 20px 30px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  overflow: scroll;\n}\n.main[data-v-45da1375] {\n  background: white;\n  padding: 20px 30px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  width: 80%;\n  min-height: 500px;\n}\nul[data-v-45da1375],\nol[data-v-45da1375] {\n  list-style: none;\n}\n.labs[data-v-45da1375] {\n  background: white;\n  padding: 1px 10px;\n  margin: 8px 0;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  border-bottom-right-radius: 30px;\n  border-top-right-radius: 30px;\n}\n.form-check-input[data-v-45da1375] {\n  position: absolute;\n  margin-top: 0.3rem;\n  margin-left: -1.25rem;\n}\n.btn-primary[data-v-45da1375] {\n  background-color: rgb(171, 171, 190) !important;\n  border: none !important;\n}\n.sub[data-v-45da1375] {\n  margin: 20px;\n  background-color: rgba(53, 113, 141, 0.6) !important;\n}\n.form-control[data-v-45da1375] {\n  background: rgba(255, 255, 255, 0.5);\n}\n.questionGroup[data-v-45da1375] {\n  padding: 10px;\n  border-bottom: 1px solid #ccc;\n  margin-bottom: 15px;\n}\n.submit[data-v-45da1375] {\n  width: 100%;\n  text-align: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 1370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "edit-checklist-page",
  data: function data() {
    return {
      id: this.$route.params.id,
      title: "",
      premise: "",
      type: "",
      catalog: "",

      models: [{
        category: "",
        questions: [{
          title: "",
          type: "tf",
          option: "",
          options: [],
          answers: [],
          answer: ""
        }]
      }],
      answers: []
    };
  },

  created: function created() {
    var _this = this;

    axios.get("/api/questions/" + this.id).then(function (response) {
      _this.title = response.data.title;
      _this.type = response.data.name;
      _this.catalog = response.data.catalog;
      _this.premise = response.data.premise;
      _this.models = JSON.parse(response.data.questions);
    });
  },

  methods: {
    addOption: function addOption(options, option) {
      options.push(option);
      option = " ";

      this.$toasted.success("Added");
    },
    removeOption: function removeOption(options, index) {
      options.splice(index);
      this.$toasted.success("Removed");
    },
    create: function create(i) {
      this.models[i].questions.push({
        title: "",
        type: "tf",
        option: "",
        options: [],
        answers: [],
        answer: ""
      });
      this.$toasted.success("Question added");
    },
    addCategory: function addCategory() {
      this.models.push({
        category: "",
        questions: [{
          title: "",
          type: "tf",
          option: "",
          options: [],
          answers: [],
          answer: ""
        }]
      });
    },
    submit: function submit() {
      var _this2 = this;

      var data = {
        title: this.title.toLowerCase(),
        models: this.models,
        name: this.type
      };
      axios.put("/api/questions/" + this.id, JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          _this2.$router.go(-1);
          _this2.$toasted.success("Question updated");
        }
      });
    }
  }
});

/***/ }),

/***/ 1371:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content_wrapper" }, [
    _c(
      "form",
      {
        staticClass: "main-page",
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.submit($event)
          }
        }
      },
      [
        _c("div", { staticClass: "form-group" }, [
          _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
            _vm._v("Questionaire Catalog")
          ]),
          _vm._v(" "),
          _c(
            "select",
            {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.catalog,
                  expression: "catalog"
                }
              ],
              staticClass: "form-control",
              attrs: { name: "", id: "" },
              on: {
                change: function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.catalog = $event.target.multiple
                    ? $$selectedVal
                    : $$selectedVal[0]
                }
              }
            },
            [
              _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "qg" } }, [
                _vm._v("Questionaire/Guides")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "diagnostics" } }, [
                _vm._v("Business Diagnostics")
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
            _vm._v("Questionaire Title")
          ]),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.title,
                expression: "title"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              name: "",
              id: "title",
              "aria-describedby": "helpId",
              placeholder: "",
              minlength: "10",
              required: ""
            },
            domProps: { value: _vm.title },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.title = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _vm.catalog === "diagnostics"
          ? _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                  _vm._v("Premise")
                ]),
                _vm._v(" "),
                _c("app-editor", {
                  staticClass: "form-control",
                  attrs: {
                    apiKey: "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                    init: { plugins: "wordcount, lists, advlist" }
                  },
                  model: {
                    value: _vm.premise,
                    callback: function($$v) {
                      _vm.premise = $$v
                    },
                    expression: "premise"
                  }
                })
              ],
              1
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.catalog === "diagnostics"
          ? _c("div", { staticClass: "form-group" }, [
              _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                _vm._v("Questionaire Type")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "", id: "" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.type = $event.target.multiple
                        ? $$selectedVal
                        : $$selectedVal[0]
                    }
                  }
                },
                [
                  _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "brain-gym" } }, [
                    _vm._v("Brain-gym")
                  ])
                ]
              )
            ])
          : _c("div", { staticClass: "form-group" }, [
              _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                _vm._v("Questionaire Type")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "", id: "" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.type = $event.target.multiple
                        ? $$selectedVal
                        : $$selectedVal[0]
                    }
                  }
                },
                [
                  _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "qa" } }, [
                    _vm._v("Checklist")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "template" } }, [
                    _vm._v("Business Template")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "bg" } }, [
                    _vm._v("Business Guide")
                  ])
                ]
              )
            ]),
        _vm._v(" "),
        _vm._l(_vm.models, function(model, index) {
          return _c(
            "div",
            { key: index, staticClass: "questionGroup" },
            [
              _c("div", { staticClass: "form-group" }, [
                _c("h5", { staticClass: "mb-2", attrs: { for: "title" } }, [
                  _vm._v("Question Category " + _vm._s(index + 1))
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: model.category,
                      expression: "model.category"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    name: "",
                    id: "title",
                    "aria-describedby": "helpId",
                    placeholder: "Enter category",
                    required: ""
                  },
                  domProps: { value: model.category },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(model, "category", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _vm._l(model.questions, function(question, index) {
                return _c("div", { key: index }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("h5", { staticClass: "mb-2", attrs: { for: "title" } }, [
                      _vm._v("Question " + _vm._s(index + 1))
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: question.title,
                          expression: "question.title"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        name: "",
                        id: "title",
                        "aria-describedby": "helpId",
                        placeholder: "Enter question",
                        required: ""
                      },
                      domProps: { value: question.title },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(question, "title", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { staticClass: "mb-2", attrs: { for: "" } }, [
                      _vm._v("Type")
                    ]),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: question.type,
                            expression: "question.type"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { name: "", id: "", required: "" },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              question,
                              "type",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "tf" } }, [
                          _vm._v("True/False")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "mc" } }, [
                          _vm._v("Multiple Optons")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "radio" } }, [
                          _vm._v("Single Options")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "text" } }, [
                          _vm._v("text")
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  question.type === "mc" || question.type === "radio"
                    ? _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c(
                            "label",
                            { staticClass: "mb-2", attrs: { for: "option" } },
                            [_vm._v("Options")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "d-flex" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: question.option,
                                  expression: "question.option"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                name: "",
                                id: "option",
                                "aria-describedby": "helpId",
                                placeholder: ""
                              },
                              domProps: { value: question.option },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    question,
                                    "option",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.addOption(
                                      question.options,
                                      question.option
                                    )
                                  }
                                }
                              },
                              [_vm._v("Add")]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(question.options, function(quest, index) {
                            return _c("div", { key: index }, [
                              _c("ul", [
                                _c("li", [
                                  _vm._v(
                                    "\n                " +
                                      _vm._s(quest) +
                                      "\n                "
                                  ),
                                  _c("i", {
                                    staticClass:
                                      "fas fa-trash fa-inverse fa-1x ml-2",
                                    staticStyle: { color: "red" },
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeOption(
                                          question.options,
                                          index
                                        )
                                      }
                                    }
                                  })
                                ])
                              ])
                            ])
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c("div")
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.type === "qa"
                    ? _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c(
                            "label",
                            { staticClass: "mb-2", attrs: { for: "title" } },
                            [_vm._v("Answer(s)")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "d-flex" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: question.answer,
                                  expression: "question.answer"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                name: "",
                                id: "title",
                                "aria-describedby": "helpId",
                                placeholder: ""
                              },
                              domProps: { value: question.answer },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    question,
                                    "answer",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.addOption(
                                      question.answers,
                                      question.answer
                                    )
                                  }
                                }
                              },
                              [_vm._v("Add")]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(question.answers, function(quest, index) {
                            return _c("div", { key: index }, [
                              _c("ul", [
                                _c("li", [
                                  _vm._v(
                                    "\n                " +
                                      _vm._s(quest) +
                                      "\n                "
                                  ),
                                  _c("i", {
                                    staticClass:
                                      "fas fa-trash fa-inverse fa-1x ml-2",
                                    staticStyle: { color: "red" },
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeOption(
                                          question.answers,
                                          index
                                        )
                                      }
                                    }
                                  })
                                ])
                              ])
                            ])
                          }),
                          _vm._v(" "),
                          _c("br")
                        ],
                        2
                      )
                    : _vm._e()
                ])
              }),
              _vm._v(" "),
              _c("div", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary sub",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.create(index)
                      }
                    }
                  },
                  [_vm._v("Add question")]
                )
              ])
            ],
            2
          )
        }),
        _vm._v(" "),
        _c("div", [
          _c(
            "button",
            {
              staticClass: "btn btn-primary sub",
              attrs: { type: "button" },
              on: { click: _vm.addCategory }
            },
            [_vm._v("Add Category")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "submit" }, [
          _c(
            "button",
            { staticClass: "btn btn-primary", on: { click: _vm.submit } },
            [_vm._v("Update")]
          )
        ])
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-45da1375", module.exports)
  }
}

/***/ }),

/***/ 547:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1368)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1370)
/* template */
var __vue_template__ = __webpack_require__(1371)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-45da1375"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/editChecklist.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45da1375", Component.options)
  } else {
    hotAPI.reload("data-v-45da1375", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});