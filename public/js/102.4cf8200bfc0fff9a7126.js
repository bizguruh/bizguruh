webpackJsonp([102],{

/***/ 1131:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1132);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0e3e309c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ad50d08\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addCategoriesVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ad50d08\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addCategoriesVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1132:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.firstBar[data-v-6ad50d08] {\n  border: 3px solid black;\n  height: auto;\n  min-height: 100px;\n  overflow: scroll;\n  background-color: #fff;\n}\n.main-page[data-v-6ad50d08] {\n  background: #f7f8fa;\n  padding: 40px 15px;\n  position:relative;\n}\n#page-wrapper[data-v-6ad50d08] {\n  height: 100vh;\n}\n.widget-shadow[data-v-6ad50d08] {\n  height: 85vh;\n}\n.category[data-v-6ad50d08] {\n  width: 100%;\n  padding: 8px;\n  border-bottom: 1px solid black;\n  cursor: pointer;\n  text-transform: capitalize;\n}\n.hover[data-v-6ad50d08],\n.clickColor[data-v-6ad50d08] {\n  background: #222d32;\n  color: #ffffff;\n}\n.cat-header[data-v-6ad50d08] {\n  margin: 19px 0;\n  font-size: 17px;\n  font-weight: bold;\n}\n", ""]);

// exports


/***/ }),

/***/ 1133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["vendorSub"],
  name: "add-categories-vendor-component",
  data: function data() {
    return {
      showUpgrade: false,
      categories: [],
      insights: [],
      industries: [],
      token: "",
      catd: true,
      category_name: "",
      subcategory_name: "",
      subcategoryBrand_name: "",
      subcategoriesBrand: [],
      subcategories: [],
      cat: {
        category_id: "",
        sub_category_id: "",
        sub_category_brand_id: "",
        vendor_user_id: "",
        productCategory: "ES",
        insight_id: 1,
        industry_id: 1
      },
      btn: true,
      topicId: [],
      subcat: "",
      user: {},
      vendorInfo: {},
      vendorArea: null,
      vendorTopic: []
    };
  },
  mounted: function mounted() {
    var _this = this;

    this.getVendor();
    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    this.user = vendor;
    this.cat.vendor_user_id = vendor.id;
    if (vendor != null) {
      axios.get("/api/get/all/category", {
        headers: { Authorization: "Bearer " + vendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          _this.categories = response.data;
          _this.token = vendor.access_token;
          _this.categories.forEach(function (item) {
            _this.$set(item, "hover", false);
          });
          _this.categories.forEach(function (item) {
            _this.$set(item, "click", false);
          });
          _this.getVendorInsight();

          _this.getVendorIndustry();
        }
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    redirectUpgrade: function redirectUpgrade() {
      this.showUpgrade = false;
      this.$router.push('/vendor/home');
    },
    getVendor: function getVendor() {
      var _this2 = this;

      var authVendor = JSON.parse(localStorage.getItem("authVendor"));

      axios.get("/api/vendor/" + authVendor.id, {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (res) {
        _this2.vendorInfo = res.data.vendor[0];
        _this2.vendorArea = res.data.vendor[0].subjectMatter;
      });
    },
    getVendorInsight: function getVendorInsight() {
      var _this3 = this;

      axios.get("/api/vendor-insights").then(function (response) {
        if (response.status === 200) {
          _this3.insights = response.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getVendorIndustry: function getVendorIndustry() {
      var _this4 = this;

      axios.get("/api/vendor-industry").then(function (response) {
        if (response.status === 200) {
          _this4.industries = response.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getSubCategory: function getSubCategory(subcat, category, category_name) {
      var _this5 = this;

      if (this.$props.vendorSub > 1) {
        this.categories.forEach(function (val) {
          val.click = false;
        });
        axios.get("/api/get/all/subcategory/" + subcat, {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 200) {
            _this5.cat.category_id = subcat;
            _this5.cat.sub_category_id = "";
            _this5.subcategories = response.data;
            _this5.subcategories.forEach(function (item) {
              if (item.id == _this5.vendorArea) {
                _this5.$set(item, "hover", true);
                _this5.getSubCategoryId(item.id, item, item.name);
              } else {
                _this5.$set(item, "hover", false);
              }
            });
            category.click = true;
            _this5.category_name = category_name;
          }
        }).catch(function (error) {
          console.log(error);
        });
      } else {

        if (category_name.toLowerCase() == "articles") {

          this.categories.forEach(function (val) {
            val.click = false;
          });
          axios.get("/api/get/all/subcategory/" + subcat, {
            headers: { Authorization: "Bearer " + this.token }
          }).then(function (response) {
            if (response.status === 200) {
              _this5.cat.category_id = subcat;
              _this5.cat.sub_category_id = "";
              _this5.subcategories = response.data;
              _this5.subcategories.forEach(function (item) {
                if (item.id == _this5.vendorArea) {
                  _this5.$set(item, "hover", true);
                  _this5.getSubCategoryId(item.id, item, item.name);
                } else {
                  _this5.$set(item, "hover", false);
                }
              });
              category.click = true;
              _this5.category_name = category_name;
            }
          }).catch(function (error) {
            console.log(error);
          });
        } else {
          category.click = false;
          category.hover = false;
          this.showUpgrade = true;
        }
      }
    },
    getSubCategoryId: function getSubCategoryId(subcatid, subcategory, subcategory_name) {
      var _this6 = this;

      this.subcat = subcategory_name;
      axios.get("/api/get/all/subcategorybrand/" + subcatid, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        if (response.status === 200) {
          _this6.subcategoriesBrand = response.data;
          _this6.subcategoriesBrand.forEach(function (item) {
            _this6.$set(item, "hover", false);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
      this.subcategories.forEach(function (val) {
        val.click = false;
      });
      this.cat.sub_category_id = subcatid;
      subcategory.click = true;
      this.subcategory_name = subcategory_name;
      this.btn = false;
    },
    getSubCategoryBrandId: function getSubCategoryBrandId(subbrandId, subcategoryBrand, subcategoryBrand_name, index) {
      if (this.topicId.includes(subbrandId)) {
        var a = this.topicId.indexOf(subbrandId);
        console.log("a", a);
        if (a > -1) {
          this.topicId.splice(a, 1);
          this.cat.sub_category_brand_id = JSON.stringify(this.topicId);
          subcategoryBrand.click = false;
        }
      } else {
        this.topicId.push(subbrandId);
        this.cat.sub_category_brand_id = JSON.stringify(this.topicId);
        subcategoryBrand.click = true;
      }
    },
    addCategory: function addCategory() {
      var _this7 = this;

      axios.post("/api/vendor-product", JSON.parse(JSON.stringify(this.cat)), {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        if (response.status === 201) {
          var id = response.data.data.id;
          _this7.$toasted.success("Category successfully added");
          _this7.$router.push("/vendor/add/product/" + id + "/" + _this7.category_name.replace(/ /g, "-"));
        }
      }).catch(function (error) {
        for (var key in error.response.data.errors) {
          _this7.$toasted.error(error.response.data.errors[key][0]);
        }
      });
    }
  }
});

/***/ }),

/***/ 1134:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _vm.showUpgrade
      ? _c("div", { staticClass: "upgradeContainer animated fadeIn" }, [
          _c("div", { staticClass: "upgrade" }, [
            _c("img", {
              staticClass: "sad",
              attrs: { src: "/images/sad.svg", alt: "" }
            }),
            _vm._v(" "),
            _c("p", { staticClass: "upgradeText" }, [
              _vm._v(
                "Oops, looks like you need to upgrade to create other media forms. It’ll only take a sec though. "
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "upgradeButtons" }, [
              _c(
                "div",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                  on: { click: _vm.redirectUpgrade }
                },
                [_vm._v("Yes please!")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "elevated_btn elevated_btn_sm text-dark",
                  on: { click: _vm.closeUpgrade }
                },
                [_vm._v("I'll keep exploring")]
              )
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("h3", { staticClass: "mb-4 josefin text-center" }, [
      _vm._v(
        "Dear " +
          _vm._s(_vm.user.storeName) +
          ", Welcome back to your Bizguruh Dashboard!"
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row widget-shadow" }, [
      _c("div", { staticClass: "col-md-4" }, [
        _c("h5", { staticClass: "mb-3" }, [
          _vm._v("What genre of insight do you wish to share today?")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.categories, function(category, index) {
            return _c(
              "div",
              {
                key: index,
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hover: category.hover,
                  clickColor: category.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategory(
                      category.id,
                      category,
                      category.name
                    )
                  },
                  mouseenter: function($event) {
                    category.hover = true
                  },
                  mouseleave: function($event) {
                    category.hover = false
                  }
                }
              },
              [_vm._v(_vm._s(category.name))]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4" }, [
        _c("h5", { staticClass: "mb-3" }, [_vm._v("Knowledge Area")]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.subcategories, function(subcategory, index) {
            return _c(
              "div",
              {
                key: index,
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hover: subcategory.hover,
                  clickColor: subcategory.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategoryId(
                      subcategory.id,
                      subcategory,
                      subcategory.name
                    )
                  },
                  mouseenter: function($event) {
                    subcategory.hover = true
                  },
                  mouseleave: function($event) {
                    subcategory.hover = false
                  }
                }
              },
              [_vm._v(_vm._s(subcategory.name))]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4" }, [
        _c("h5", { staticClass: "mb-3" }, [
          _vm._v(
            "What will you be teaching us about " +
              _vm._s(_vm.subcat.toLowerCase()) +
              " today?"
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.subcategoriesBrand, function(subcategoryBrand, index) {
            return _c(
              "div",
              {
                key: index,
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hover: subcategoryBrand.hover,
                  clickColor: subcategoryBrand.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategoryBrandId(
                      subcategoryBrand.id,
                      subcategoryBrand,
                      subcategoryBrand.name,
                      index
                    )
                  },
                  mouseenter: function($event) {
                    subcategoryBrand.hover = true
                  },
                  mouseleave: function($event) {
                    subcategoryBrand.hover = false
                  }
                }
              },
              [_vm._v(_vm._s(subcategoryBrand.name))]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 text-center mt-2 mb-3" }, [
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm text-white btn-compliment mx-auto",
            attrs: { disabled: _vm.btn },
            on: {
              click: function($event) {
                return _vm.addCategory()
              }
            }
          },
          [
            _vm._v("\n        Proceed\n        "),
            _c("i", {
              staticClass: "fa fa-arrow-right pl-2",
              attrs: { "aria-hidden": "true" }
            })
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6ad50d08", module.exports)
  }
}

/***/ }),

/***/ 500:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1131)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1133)
/* template */
var __vue_template__ = __webpack_require__(1134)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6ad50d08"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/addCategoriesVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6ad50d08", Component.options)
  } else {
    hotAPI.reload("data-v-6ad50d08", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});