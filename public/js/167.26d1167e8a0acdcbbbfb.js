webpackJsonp([167],{

/***/ 1360:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1361);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("c34ce7e0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42641048\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminProductTypeComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42641048\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminProductTypeComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1361:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.rowB[data-v-42641048] {\n    padding: 30px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-product-type-component",
    data: function data() {
        return {
            token: '',
            subjectMatters: [],
            insights: [],
            topics: [],
            industries: [],
            admin: {},
            fade: true,
            modal: true,
            overallCheck: false,
            insightCheck: false,
            conceptCheck: false,
            industryCheck: false,
            type: '',
            category: {
                id: '',
                name: '',
                description: '',
                adminCategory: ''
            },
            CategoryText: 'Create Category',
            catId: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.admin = admin;

        axios.get('/api/admin/all-subject-matter', { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
            console.log(response);
            if (response.status === 200) {
                _this.subjectMatters = response.data.data;
                _this.getAllInsight();
                _this.getAllTopics();
                _this.getAllIndustry();
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        getAllInsight: function getAllInsight() {
            var _this2 = this;

            axios.get('/api/insights', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this2.insights = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAllTopics: function getAllTopics() {
            var _this3 = this;

            axios.get('/api/admin/all-topics', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.topics = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAllIndustry: function getAllIndustry() {
            var _this4 = this;

            axios.get('/api/industry', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.industries = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        showModal: function showModal(params, id) {
            var _this5 = this;

            this.fade = false;
            if (params === 'subjectMatter') {
                this.type = 'subjectMatter';
            } else if (params === 'insight') {
                this.type = 'insight';
            } else if (params === 'concept') {
                this.type = 'concept';
            } else if (params === 'industry') {
                this.type = 'industry';
            } else if (params === 'industryEdit') {
                this.type = 'industryEdit';

                axios.get('/api/industry/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    console.log(response.data);
                    _this5.category.id = response.data.id;
                    _this5.category.name = response.data.name;
                    _this5.category.description = response.data.description;
                    _this5.category.adminCategory = response.data.adminCategory;
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this5.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (params === 'conceptEdit') {
                this.type = 'conceptEdit';
                axios.get('/api/subcategorybrand/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this5.category.id = response.data.id;
                        _this5.category.name = response.data.name;
                        _this5.category.description = response.data.description;
                        _this5.category.adminCategory = response.data.adminCategory;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else if (params === 'subjectMatterEdit') {
                this.type = 'subjectMatterEdit';
                this.catId = id;
                axios.get('/api/subcategory/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    console.log(response.data);
                    _this5.category.id = response.data.id;
                    _this5.category.name = response.data.name;
                    _this5.category.description = response.data.description;
                    _this5.category.adminCategory = response.data.adminCategory;
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this5.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (params === 'insightEdit') {
                console.log(id);
                this.type = 'insightEdit';
                this.catId = id;
                axios.get('/api/insights/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    _this5.category.id = response.data.id;
                    _this5.category.name = response.data.name;
                    _this5.category.description = response.data.description;
                    _this5.category.adminCategory = response.data.adminCategory;
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this5.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            }
        },
        createNewCategory: function createNewCategory(params) {
            var _this6 = this;

            if (this.type === 'subjectMatter') {
                axios.post('/api/subcategory', JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    console.log(response);
                    if (response.status === 201) {
                        _this6.$toasted.success('Subject matter successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'insight') {
                axios.post('/api/insights', JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this6.$toasted.success('Insight successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'concept') {
                axios.post('/api/subcategorybrand', JSON.parse(JSON.stringify(this.brand)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this6.$toasted.success('Subcategory successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'industry') {
                axios.post('/api/industry', JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this6.$toasted.success('Insight successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'subjectMatterEdit') {
                axios.put('/api/subcategory/' + params, JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this6.$toasted.success('Subcategory successfully edited');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'conceptEdit') {
                axios.put('/api/subcategorybrand/' + params, JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this6.$toasted.success('Subcategory successfully edited');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'insightEdit') {
                console.log(params);
                axios.put('/api/insights/' + params, JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this6.$toasted.success('Subcategory successfully edited');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (this.type === 'industryEdit') {
                axios.put('/api/industry/' + params, JSON.parse(JSON.stringify(this.category)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this6.$toasted.success('Subcategory successfully edited');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this6.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            }
        },
        deleteCategory: function deleteCategory(id, cat, params) {
            var _this7 = this;

            console.log(id, cat, params);
            if (params === 'subjectMatter') {
                console.log('sm');
                axios.delete('/api/subcategory/' + id, { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this7.$toasted.success("Deleted Successfully");
                        window.location.reload();
                    } else {
                        _this7.$toasted.error("Unable to delete item");
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this7.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (params === 'insight') {
                axios.delete('/api/insights/' + id, { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this7.$toasted.success("Deleted Successfully");
                        window.location.reload();
                    } else {
                        _this7.$toasted.error("Unable to delete item");
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this7.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (params === 'concept') {
                axios.delete('/api/subcategorybrand/' + id, { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this7.$toasted.success("Deleted Successfully");
                        window.location.reload();
                    } else {
                        _this7.$toasted.error("Unable to delete item");
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this7.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else if (params === 'industry') {
                axios.delete('/api/industry/' + id, { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this7.$toasted.success("Deleted Successfully");
                        window.location.reload();
                    } else {
                        _this7.$toasted.error("Unable to delete item");
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this7.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            }
        },
        deleteItem: function deleteItem(params) {
            var _this8 = this;

            if (params === 'subjectMatter') {
                var deletedArray = [];
                this.subjectMatters.forEach(function (item) {
                    if (item.itemClick) {
                        deletedArray.push(item.id);
                    }
                });
                if (deletedArray.length === 0) {
                    this.$toasted.error('You must select at least one subject matter to delete');
                } else {
                    console.log(deletedArray);
                    axios.post('/api/admin/subject-matter-delete', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                        if (response.status === 200) {
                            console.log(response);
                            for (var i = 0; i < deletedArray.length; i++) {
                                window.location.reload();
                            }
                            _this8.$toasted.success('All selected items deleted');
                        } else {
                            _this8.$toasted.error('Unable to delete');
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            } else if (params === 'insight') {
                var _deletedArray = [];
                this.insights.forEach(function (item) {
                    if (item.itemClick) {
                        _deletedArray.push(item.id);
                    }
                });

                if (_deletedArray.length === 0) {
                    this.$toasted.error('You must select at least one subject matter to delete');
                } else {
                    console.log(_deletedArray);
                    axios.post('/api/admin/delete-insights', JSON.parse(JSON.stringify(_deletedArray)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                        if (response.status === 200) {
                            console.log(response);
                            for (var i = 0; i < _deletedArray.length; i++) {
                                window.location.reload();
                            }
                            _this8.$toasted.success('All selected items deleted');
                        } else {
                            _this8.$toasted.error('Unable to delete');
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            } else if (params === 'concept') {
                var _deletedArray2 = [];
                this.topics.forEach(function (item) {
                    if (item.itemClick) {
                        _deletedArray2.push(item.id);
                    }
                });

                if (_deletedArray2.length === 0) {
                    this.$toasted.error('You must select at least one subject matter to delete');
                } else {
                    console.log(_deletedArray2);
                    axios.post('/api/admin/delete-concepts', JSON.parse(JSON.stringify(_deletedArray2)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                        if (response.status === 200) {
                            console.log(response);
                            for (var i = 0; i < _deletedArray2.length; i++) {
                                console.log(_deletedArray2[i]);
                                _this8.topics.splice(i, 1);
                            }
                            _this8.$toasted.success('All selected items deleted');
                        } else {
                            _this8.$toasted.error('Unable to delete');
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            } else if (params === 'industry') {
                var _deletedArray3 = [];
                this.industries.forEach(function (item) {
                    if (item.itemClick) {
                        _deletedArray3.push(item.id);
                    }
                });

                if (_deletedArray3.length === 0) {
                    this.$toasted.error('You must select at least one subject matter to delete');
                } else {
                    console.log(_deletedArray3);
                    axios.post('/api/admin/delete-industry', JSON.parse(JSON.stringify(_deletedArray3)), { headers: { "Authorization": 'Bearer ' + this.admin.access_token } }).then(function (response) {
                        if (response.status === 200) {
                            console.log(response);
                            for (var i = 0; i < _deletedArray3.length; i++) {
                                console.log(_deletedArray3[i]);
                                _this8.industries.splice(i, 1);
                            }
                            _this8.$toasted.success('All selected items deleted');
                        } else {
                            _this8.$toasted.error('Unable to delete');
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
            }
        },
        overallCheckBox: function overallCheckBox(params) {
            var _this9 = this;

            if (params === 'subjectMatter') {
                console.log(params);
                this.subjectMatters.forEach(function (item) {
                    if (!('itemClick' in item)) {
                        _this9.$set(item, 'itemClick', true);
                    } else if (_this9.overallCheck) {
                        _this9.$set(item, 'itemClick', true);
                    } else {
                        _this9.$set(item, 'itemClick', false);
                    }
                });
            } else if (params === 'insight') {
                this.insights.forEach(function (item) {
                    if (!('itemClick' in item)) {
                        _this9.$set(item, 'itemClick', true);
                    } else if (_this9.insightCheck) {
                        _this9.$set(item, 'itemClick', true);
                    } else {
                        _this9.$set(item, 'itemClick', false);
                    }
                });
            } else if (params === 'concept') {
                this.topics.forEach(function (item) {
                    if (!('itemClick' in item)) {
                        _this9.$set(item, 'itemClick', true);
                    } else if (_this9.conceptCheck) {
                        _this9.$set(item, 'itemClick', true);
                    } else {
                        _this9.$set(item, 'itemClick', false);
                    }
                });
            } else if (params === 'industry') {
                this.industries.forEach(function (item) {
                    if (!('itemClick' in item)) {
                        _this9.$set(item, 'itemClick', true);
                    } else if (_this9.industryCheck) {
                        _this9.$set(item, 'itemClick', true);
                    } else {
                        _this9.$set(item, 'itemClick', false);
                    }
                });
            }
        },
        deleteAllItem: function deleteAllItem() {}
    }
});

/***/ }),

/***/ 1363:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c(
      "div",
      {
        class: { modal: _vm.modal, fade: _vm.fade },
        attrs: {
          id: "myModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "myModalLabel"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "category" } }, [
                    _vm._v("Category:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.category.name,
                        expression: "category.name"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "category" },
                    domProps: { value: _vm.category.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.category, "name", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "categorydescription" } }, [
                    _vm._v("Description:")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.category.description,
                        expression: "category.description"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { id: "categorydescription" },
                    domProps: { value: _vm.category.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.category,
                          "description",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "adminCategory" } }, [
                    _vm._v("Admins")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.category.adminCategory,
                          expression: "category.adminCategory"
                        }
                      ],
                      attrs: { id: "adminCategory" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.category,
                            "adminCategory",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "AO" } }, [
                        _vm._v("Admin only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "UO" } }, [
                        _vm._v("User only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "AU" } }, [
                        _vm._v("Admin and User")
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-footer" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-default",
                      attrs: { type: "button", "data-dismiss": "modal" }
                    },
                    [_vm._v("Close")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.createNewCategory(_vm.category.id)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.CategoryText))]
                  )
                ])
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", [
      _c("div", { staticClass: "rowB" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { "data-toggle": "modal", "data-target": "#myModal" },
                on: {
                  click: function($event) {
                    return _vm.showModal("subjectMatter", null)
                  }
                }
              },
              [_vm._v("Create Subject Matter")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-danger",
                on: {
                  click: function($event) {
                    return _vm.deleteItem("subjectMatter")
                  }
                }
              },
              [_vm._v("Delete")]
            )
          ])
        ]),
        _vm._v(" "),
        _c("table", [
          _c("thead", [
            _c("tr", [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.overallCheck,
                      expression: "overallCheck"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.overallCheck)
                      ? _vm._i(_vm.overallCheck, null) > -1
                      : _vm.overallCheck
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$a = _vm.overallCheck,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.overallCheck = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.overallCheck = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.overallCheck = $$c
                        }
                      },
                      function($event) {
                        return _vm.overallCheckBox("subjectMatter")
                      }
                    ]
                  }
                })
              ]),
              _vm._v(" "),
              _c("th", [_vm._v("Name")]),
              _vm._v(" "),
              _c("th", [_vm._v("Description")]),
              _vm._v(" "),
              _c("th")
            ])
          ]),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.subjectMatters, function(subjectMatter, index) {
              return _vm.subjectMatters.length > 0
                ? _c("tr", [
                    _c("td", [
                      _c("input", {
                        attrs: { type: "checkbox" },
                        domProps: { checked: subjectMatter.itemClick },
                        on: {
                          change: function($event) {
                            subjectMatter.itemClick = !subjectMatter.itemClick
                            _vm.overallCheck = false
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(subjectMatter.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(subjectMatter.description))]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-primary",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#myModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.showModal(
                                "subjectMatterEdit",
                                subjectMatter.id
                              )
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          on: {
                            click: function($event) {
                              return _vm.deleteCategory(
                                subjectMatter.id,
                                index,
                                "subjectMatter"
                              )
                            }
                          }
                        },
                        [_vm._v("Delete")]
                      )
                    ])
                  ])
                : _vm._e()
            }),
            0
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", [
      _c("div", { staticClass: "rowB" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { "data-toggle": "modal", "data-target": "#myModal" },
                on: {
                  click: function($event) {
                    return _vm.showModal("insight", null)
                  }
                }
              },
              [_vm._v("Create Insight")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-danger",
                on: {
                  click: function($event) {
                    return _vm.deleteItem("insight")
                  }
                }
              },
              [_vm._v("Delete")]
            )
          ])
        ]),
        _vm._v(" "),
        _c("table", [
          _c("thead", [
            _c("tr", [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.insightCheck,
                      expression: "insightCheck"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.insightCheck)
                      ? _vm._i(_vm.insightCheck, null) > -1
                      : _vm.insightCheck
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$a = _vm.insightCheck,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.insightCheck = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.insightCheck = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.insightCheck = $$c
                        }
                      },
                      function($event) {
                        return _vm.overallCheckBox("insight")
                      }
                    ]
                  }
                })
              ]),
              _vm._v(" "),
              _c("th", [_vm._v("Name")]),
              _vm._v(" "),
              _c("th", [_vm._v("Description")]),
              _vm._v(" "),
              _c("th")
            ])
          ]),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.insights, function(insight, index) {
              return _vm.insights.length > 0
                ? _c("tr", [
                    _c("td", [
                      _c("input", {
                        attrs: { type: "checkbox" },
                        domProps: { checked: insight.itemClick },
                        on: {
                          change: function($event) {
                            insight.itemClick = !insight.itemClick
                            _vm.overallCheck = false
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(insight.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(insight.description))]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-primary",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#myModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.showModal("insightEdit", insight.id)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          on: {
                            click: function($event) {
                              return _vm.deleteCategory(
                                insight.id,
                                index,
                                "insight"
                              )
                            }
                          }
                        },
                        [_vm._v("Delete")]
                      )
                    ])
                  ])
                : _vm._e()
            }),
            0
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", [
      _c("div", { staticClass: "rowB" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { "data-toggle": "modal", "data-target": "#myModal" },
                on: {
                  click: function($event) {
                    return _vm.showModal("concept", null)
                  }
                }
              },
              [_vm._v(" Create Concept")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-danger",
                on: {
                  click: function($event) {
                    return _vm.deleteItem("concept")
                  }
                }
              },
              [_vm._v("Delete")]
            )
          ])
        ]),
        _vm._v(" "),
        _c("table", [
          _c("thead", [
            _c("tr", [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.conceptCheck,
                      expression: "conceptCheck"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.conceptCheck)
                      ? _vm._i(_vm.conceptCheck, null) > -1
                      : _vm.conceptCheck
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$a = _vm.conceptCheck,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.conceptCheck = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.conceptCheck = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.conceptCheck = $$c
                        }
                      },
                      function($event) {
                        return _vm.overallCheckBox("concept")
                      }
                    ]
                  }
                })
              ]),
              _vm._v(" "),
              _c("th", [_vm._v("Name")]),
              _vm._v(" "),
              _c("th", [_vm._v("Description")]),
              _vm._v(" "),
              _c("th")
            ])
          ]),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.topics, function(topic, index) {
              return _vm.topics.length > 0
                ? _c("tr", [
                    _c("td", [
                      _c("input", {
                        attrs: { type: "checkbox" },
                        domProps: { checked: topic.itemClick },
                        on: {
                          change: function($event) {
                            topic.itemClick = !topic.itemClick
                            _vm.overallCheck = false
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(topic.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(topic.description))]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-primary",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#myModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.showModal("conceptEdit", topic.id)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          on: {
                            click: function($event) {
                              return _vm.deleteCategory(
                                topic.id,
                                index,
                                "concept"
                              )
                            }
                          }
                        },
                        [_vm._v("Delete")]
                      )
                    ])
                  ])
                : _vm._e()
            }),
            0
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", [
      _c("div", { staticClass: "rowB" }, [
        _c("div", { staticClass: "col-md-4" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-primary",
              attrs: { "data-toggle": "modal", "data-target": "#myModal" },
              on: {
                click: function($event) {
                  return _vm.showModal("industry", null)
                }
              }
            },
            [_vm._v(" Create Industry")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-danger",
              on: {
                click: function($event) {
                  return _vm.deleteItem("industry")
                }
              }
            },
            [_vm._v("Delete")]
          )
        ]),
        _vm._v(" "),
        _c("table", [
          _c("thead", [
            _c("tr", [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.industryCheck,
                      expression: "industryCheck"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.industryCheck)
                      ? _vm._i(_vm.industryCheck, null) > -1
                      : _vm.industryCheck
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$a = _vm.industryCheck,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.industryCheck = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.industryCheck = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.industryCheck = $$c
                        }
                      },
                      function($event) {
                        return _vm.overallCheckBox("industry")
                      }
                    ]
                  }
                })
              ]),
              _vm._v(" "),
              _c("th", [_vm._v("Name")]),
              _vm._v(" "),
              _c("th", [_vm._v("Description")]),
              _vm._v(" "),
              _c("th")
            ])
          ]),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.industries, function(industry, index) {
              return _vm.industries.length > 0
                ? _c("tr", [
                    _c("td", [
                      _c("input", {
                        attrs: { type: "checkbox" },
                        domProps: { checked: industry.itemClick },
                        on: {
                          change: function($event) {
                            industry.itemClick = !industry.itemClick
                            _vm.overallCheck = false
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(industry.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(industry.description))]),
                    _vm._v(" "),
                    _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-primary",
                          attrs: {
                            "data-toggle": "modal",
                            "data-target": "#myModal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.showModal("industryEdit", industry.id)
                            }
                          }
                        },
                        [_vm._v("Edit")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-sm btn-danger",
                          on: {
                            click: function($event) {
                              return _vm.deleteCategory(
                                industry.id,
                                index,
                                "industry"
                              )
                            }
                          }
                        },
                        [_vm._v("Delete")]
                      )
                    ])
                  ])
                : _vm._e()
            }),
            0
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c("h4", { staticClass: "modal-title", attrs: { id: "myModalLabel" } }, [
        _vm._v("Create Category")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", { attrs: { for: "upload" } }, [_vm._v("Upload Image:")]),
      _vm._v(" "),
      _c("input", { attrs: { type: "file", id: "upload" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42641048", module.exports)
  }
}

/***/ }),

/***/ 545:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1360)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1362)
/* template */
var __vue_template__ = __webpack_require__(1363)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-42641048"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminProductTypeComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42641048", Component.options)
  } else {
    hotAPI.reload("data-v-42641048", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});