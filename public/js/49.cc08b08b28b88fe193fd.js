webpackJsonp([49],{

/***/ 1540:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1541);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("e9ea7788", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ead8d78\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./videoComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6ead8d78\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./videoComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1541:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container-fluid[data-v-6ead8d78] {\n  margin-top: 0;\n  min-height: 100vh;\n  padding-left: 20px;\n  padding-right: 20px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: #f7f8fa;\n}\n.vidP[data-v-6ead8d78]{\n  padding:10px;\n}\n.df[data-v-6ead8d78]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-pack:justify;\n      -ms-flex-pack:justify;\n          justify-content:space-between;\n  -webkit-box-align:center;\n      -ms-flex-align:center;\n          align-items:center;\n  width:100%;\n}\n.sad[data-v-6ead8d78]{\n  width:50px;\n  margin: 0 auto;\n}\n.main-page[data-v-6ead8d78] {\n  width: 70%;\n  background-color: white;\n  padding-bottom: 100px;\n}\n.side-page[data-v-6ead8d78] {\n  width: 35%;\n  background-color: #f7f8fa;\n  padding: 20px;\n  overflow-y: scroll;\n\n  height: 100vh;\n}\n.videoContainer[data-v-6ead8d78] {\n  width: 100%;\n}\nvideo[data-v-6ead8d78] {\n  width: 100%;\n  background-color: black;\n}\n.about[data-v-6ead8d78] {\n  margin-top: 10px;\n}\n.videoTitle[data-v-6ead8d78] {\n  font-size: 28px;\n  font-family: \"Josefin Sans\", sans-serif;\n  text-transform: capitalize;\n  padding: 10px;\n}\n.mvc[data-v-6ead8d78] {\n  position: relative;\n  padding: 15px 50px 15px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  border-bottom: 1px solid #f7f8fa;\n}\n.mvc_2[data-v-6ead8d78] {\n  width: 30%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.icon_social[data-v-6ead8d78] {\n  margin-right: 10px;\n  font-size: 12px !important;\n}\n.share[data-v-6ead8d78] {\n  position: relative;\n  font-size: 12px;\n  font-weight: bold;\n  cursor: pointer;\n}\n.shareBtn[data-v-6ead8d78] {\n  position: absolute;\n  top: -50px;\n  background: #f7f8fa;\n  padding: 10px;\n  width: auto;\n  right: 5px;\n}\n.desc-container[data-v-6ead8d78] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 10px;\n}\n.description[data-v-6ead8d78] {\n  padding: 0;\n  width: 65%;\n  margin-top: 20px;\n}\n.buttons[data-v-6ead8d78] {\n  width: 35%;\n  padding: 15px;\n}\n.fa[data-v-6ead8d78],\n.fas[data-v-6ead8d78] {\n  font-size: 18px;\n}\n.review-container[data-v-6ead8d78] {\n  min-height: 250px;\n}\n.episodeDescription[data-v-6ead8d78] {\n  display: block;\n  padding: 15px;\n  background-color: #fff;\n  border: 1px solid #e8e9eb;\n  border-top: none;\n  font-size: 14px;\n  color: #686f7a;\n  letter-spacing: 0.3px;\n  line-height: 1.33;\n  font-weight: normal;\n}\n.reviewAvatar[data-v-6ead8d78] {\n  text-transform: capitalize;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff;\n  padding: 7px 10px;\n  border-radius: 50%;\n}\nimg[data-v-6ead8d78] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.reviewList[data-v-6ead8d78] {\n  margin-bottom: 20px;\n}\n.proReview[data-v-6ead8d78],\n.proEdi[data-v-6ead8d78] {\n  font-size: 13px;\n}\n.reviews[data-v-6ead8d78] {\n  font-weight: 500;\n}\n.floatUserRating[data-v-6ead8d78] {\n  margin-right: 104px;\n  margin-left: 0px;\n}\n.floatReview[data-v-6ead8d78] {\n  float: left;\n}\n.reviewList[data-v-6ead8d78] {\n  margin-bottom: 20px;\n}\n.rateStyle[data-v-6ead8d78] {\n  float: left;\n  margin: 2px;\n}\n.videoguest[data-v-6ead8d78] {\n  font-size: 11px;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n.sponsor[data-v-6ead8d78] {\n  font-size: 11px;\n}\n.like[data-v-6ead8d78],\n.dislike[data-v-6ead8d78] {\n  position: relative;\n  font-weight: bold;\n  font-size: 14px;\n  cursor: pointer;\n}\n.hoverLike[data-v-6ead8d78],\n.hoverDislike[data-v-6ead8d78] {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.5);\n  color: white;\n  padding: 3px 10px;\n  bottom: -35px;\n  left: -50%;\n  width: 100px;\n  font-size: 12px;\n  margin-left: 50px;\n  visibility: hidden;\n}\n.like:hover .hoverLike[data-v-6ead8d78] {\n  visibility: visible;\n}\n.dislike:hover .hoverDislike[data-v-6ead8d78] {\n  visibility: visible;\n}\n.review-div[data-v-6ead8d78] {\n  width: 70%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.tabre[data-v-6ead8d78] {\n  overflow: hidden;\n  margin-top: 10px;\n}\n.dateCreated[data-v-6ead8d78] {\n  font-size: 13px;\n  color: #adacac;\n  font-weight: 400;\n}\n.hostName[data-v-6ead8d78] {\n  color: hsl(207, 43%, 20%);\n  font-weight: bold;\n}\n.hostName[data-v-6ead8d78]:hover {\n  text-decoration: underline;\n}\n.creation[data-v-6ead8d78] {\n  font-size: 14px;\n}\n.avatarD[data-v-6ead8d78] {\n  margin-right: 20px;\n  margin-top: 10px;\n}\n.reviewerName[data-v-6ead8d78] {\n  text-transform: capitalize;\n  margin-top: -6px;\n}\n.reviewdiv[data-v-6ead8d78] {\n  border-top: 1px solid #f7f8fa;\n  border-bottom: 1px solid #f7f8fa;\n  margin: 20px 0;\n  padding-bottom: 10px;\n  font-size: 14px;\n}\n.epiPad[data-v-6ead8d78] {\n  min-height: 200px;\n}\n.reviewAvatar[data-v-6ead8d78] {\n  text-transform: capitalize;\n  background: hsl(207, 43%, 20%);\n  color: #ffffff;\n  padding: 11px 15px;\n  font-size: 15px;\n  border-radius: 50%;\n}\n.review-div[data-v-6ead8d78] {\n  width: 70%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.floatRT[data-v-6ead8d78] {\n  overflow: hidden;\n}\n.episodeTitles[data-v-6ead8d78] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-bottom: 1px solid #f7f8fa;\n  cursor: pointer;\n  height: auto;\n  margin-top: 3px;\n  padding: 15px;\n}\n.divC[data-v-6ead8d78] {\n  font-size: 40px;\n  margin: 17px 0 50px 0;\n}\n.epiPrice[data-v-6ead8d78] {\n  float: right;\n  margin: 0 5px;\n}\n.epiPriceDiv[data-v-6ead8d78] {\n  width: 40%;\n  margin-left: auto;\n}\n.buy[data-v-6ead8d78] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.pr[data-v-6ead8d78] {\n  font-size: 14px;\n}\n.episodeDescription div[data-v-6ead8d78]:nth-child(1) {\n  margin-bottom: 10px;\n}\n.shipTabSwitch[data-v-6ead8d78],\n.reviewTabSwitch[data-v-6ead8d78],\n.shareTabSwitch[data-v-6ead8d78],\n.episodeTabSwitch[data-v-6ead8d78],\n.aboutWriterTabSwitch[data-v-6ead8d78] {\n  background-color: hsl(207, 43%, 20%);\n  border: 1px solid hsl(207, 43%, 20%);\n  color: #ffffff !important;\n  width: 30%;\n  height: 30px;\n  margin: 20px 0;\n  padding: 5px;\n  cursor: pointer;\n  line-height: 1.2;\n}\n.shipTab[data-v-6ead8d78],\n.reviewTab[data-v-6ead8d78],\n.shareTab[data-v-6ead8d78],\n.episodeTab[data-v-6ead8d78],\n.aboutWriterTab[data-v-6ead8d78] {\n  color: #a4a4a4;\n  border: 1px solid #a4a4a4;\n  width: 30%;\n  margin: 20px 0;\n  padding: 5px;\n  cursor: pointer;\n  line-height: 1.2;\n  height: 30px;\n}\n.Videorow[data-v-6ead8d78] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n}\n.mobile[data-v-6ead8d78] {\n  display: none;\n}\n.cartAddText[data-v-6ead8d78] {\n  visibility: hidden;\n  width: 150px;\n  background-color: hsl(207, 43%, 20%);\n  font-size: 12px;\n  color: #fff;\n  text-align: center;\n  padding: 2px;\n  border-radius: 5px;\n  position: absolute;\n  z-index: 1;\n  bottom: 125%;\n  left: 50%;\n  margin-left: -75px;\n  opacity: 0;\n  -webkit-transition: opacity 0.3s;\n  transition: opacity 0.3s;\n}\n.cartAddText[data-v-6ead8d78]::after {\n  content: \"\";\n  position: absolute;\n  top: 100%;\n  left: 50%;\n  margin-left: -5px;\n  border-width: 5px;\n  border-style: solid;\n  border-color: hsl(207, 43%, 20%) transparent transparent transparent;\n}\n.elevated_btn:hover .cartAddText[data-v-6ead8d78] {\n  visibility: visible;\n  opacity: 1;\n}\n.related-container[data-v-6ead8d78] {\n  width: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: auto;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  border-bottom: 1px solid #dedfe1;\n  -webkit-transition: all 0.9s;\n  transition: all 0.9s;\n}\n.related-container:hover .vid-container img[data-v-6ead8d78] {\n  -webkit-transform: scale(1.03);\n          transform: scale(1.03);\n}\n.vid-container[data-v-6ead8d78] {\n  width: 50%;\n  height: 100%;\n  position: relative;\n  overflow: hidden;\n  cursor: pointer;\n}\n.about-related[data-v-6ead8d78] {\n  width: 50%;\n  height: 100%;\n  padding-left: 10px;\n}\n.related_title[data-v-6ead8d78] {\n  font-weight: bold;\n  font-size: 15px;\n  line-height: 1.4;\n  cursor: pointer;\n}\n.related_host[data-v-6ead8d78] {\n  font-size: 14px;\n  cursor: pointer;\n}\n.related_creation[data-v-6ead8d78] {\n  font-size: 14px;\n}\n.playCont[data-v-6ead8d78] {\n  background: rgba(255, 255, 255, 0.8);\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-top: -25px;\n  margin-left: -25px;\n  width: 50px;\n  height: 50px;\n  padding: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.playIcon[data-v-6ead8d78] {\n  font-size: 24px;\n  margin-left: 3px;\n}\n.related-container:hover .vid-container .playCont .playIcon[data-v-6ead8d78] {\n  -webkit-transform: rotate(360deg);\n          transform: rotate(360deg);\n}\n@media (max-width: 768px) {\n.container-fluid[data-v-6ead8d78] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 0;\n}\n.main-page[data-v-6ead8d78] {\n    width: 100%;\n    padding-bottom: 0;\n}\n.side-page[data-v-6ead8d78] {\n    width: 100%;\n}\n.videoTitle[data-v-6ead8d78] {\n    font-size: 20.5px;\n}\n.mvc[data-v-6ead8d78] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 0 10px;\n}\n.mvc_2[data-v-6ead8d78] {\n    width: 100%;\n    padding: 10px 0;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n}\n.dislike[data-v-6ead8d78],\n  .like[data-v-6ead8d78],\n  .share[data-v-6ead8d78] {\n    display: grid;\n    text-align: center;\n}\n.desc-container[data-v-6ead8d78] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.description[data-v-6ead8d78] {\n    width: 100%;\n    margin-top: 0;\n}\n.buttons[data-v-6ead8d78] {\n    width: 100%;\n    padding: 15px 0;\n}\n.episodeTitles[data-v-6ead8d78] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 15px 10px;\n}\n.episode-text[data-v-6ead8d78] {\n    font-size: 15px;\n}\n.epiPad[data-v-6ead8d78] {\n    padding: 0 7.5px;\n    min-height: auto;\n}\n.dii[data-v-6ead8d78] {\n    margin-bottom: 20px;\n    width:100%;\n}\n.epiPriceDiv[data-v-6ead8d78] {\n    width: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.episodeDescription[data-v-6ead8d78] {\n    font-size: 16px;\n}\n.shipTabSwitch[data-v-6ead8d78],\n  .reviewTabSwitch[data-v-6ead8d78],\n  .shareTabSwitch[data-v-6ead8d78],\n  .episodeTabSwitch[data-v-6ead8d78],\n  .aboutWriterTabSwitch[data-v-6ead8d78] {\n    font-size: 12px;\n}\n.shipTab[data-v-6ead8d78],\n  .reviewTab[data-v-6ead8d78],\n  .shareTab[data-v-6ead8d78],\n  .episodeTab[data-v-6ead8d78],\n  .aboutWriterTab[data-v-6ead8d78] {\n    font-size: 12px;\n}\n.Videorow[data-v-6ead8d78] {\n    padding: 0;\n}\n.elevated_btn[data-v-6ead8d78] {\n    width: 100%;\n    margin-top: 16px;\n}\n.elevated_btn_sm[data-v-6ead8d78] {\n    width: 50%;\n    padding: 14px 10px 11px;\n    font-size: 11px;\n}\n.icon_social[data-v-6ead8d78] {\n    position: absolute;\n    left: 30px;\n    top: 50%;\n    margin-top: -6px;\n}\n.related_title[data-v-6ead8d78] {\n    font-size: 14px;\n}\n}\n@media (max-width: 375px) {\n.elevated_btn_sm[data-v-6ead8d78] {\n    font-size: 9px;\n}\n.shipTabSwitch[data-v-6ead8d78],\n  .reviewTabSwitch[data-v-6ead8d78],\n  .shareTabSwitch[data-v-6ead8d78],\n  .episodeTabSwitch[data-v-6ead8d78],\n  .aboutWriterTabSwitch[data-v-6ead8d78] {\n    font-size: 10px;\n}\n.shipTab[data-v-6ead8d78],\n  .reviewTab[data-v-6ead8d78],\n  .shareTab[data-v-6ead8d78],\n  .episodeTab[data-v-6ead8d78],\n  .aboutWriterTab[data-v-6ead8d78] {\n    font-size: 10px;\n}\n.episode-text[data-v-6ead8d78] {\n    font-size: 14px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-video-component",
  data: function data() {
    var _ref;

    return _ref = {
      id: 0,
      related: "default",
      url: "https://bizguruh.com/videos",
      videos: [],
      videoTag: [],
      epiFade: true,
      searchItem: "",
      epiModal: true,
      videoDetail: {},
      breadcrumbList: [],
      cld: "",
      player: "",
      relatedVideos: [],
      anonymousCart: [],
      relatedPlaylist: "",
      authenticate: false,
      price: "",
      title: "",
      isActive: false,
      shipTab: true,
      reviewTabSwitch: false,
      shareTabSwitch: false,
      episodeTabSwitch: true,
      reviewTab: true,
      shareTab: true,
      aboutWriterTabSwitch: false,
      aboutWriterTab: true,
      episodeTab: true,
      shipTabSwitch: false,
      episode: true,
      shipping: false,
      reviews: false,
      share: false,
      aboutWriter: false,
      allReviews: [],
      inCart: false,
      review: {
        productId: "",
        title: "",
        description: "",
        rating: 0
      },
      star: 0,
      NotratedOne: true,
      NotratedTwo: true,
      RatedTwo: false,
      RatedOne: false,
      NotratedThree: true,
      RatedThree: false,
      NotratedFour: true,
      RatedFour: false,
      NotratedFive: true,
      RatedFive: false,
      rateFa: true,
      episodeArray: [],
      user: {}
    }, _defineProperty(_ref, "share", false), _defineProperty(_ref, "email", ""), _defineProperty(_ref, "user_id", ""), _defineProperty(_ref, "loginCart", []), _defineProperty(_ref, "open", false), _defineProperty(_ref, "hardCopy", false), _defineProperty(_ref, "vendorInfo", []), _defineProperty(_ref, "hardpriceCont", false), _defineProperty(_ref, "softCopy", false), _defineProperty(_ref, "softpriceCont", true), _defineProperty(_ref, "readOnline", false), _defineProperty(_ref, "readOnlinepriceCont", true), _defineProperty(_ref, "purchaseCheck", false), _defineProperty(_ref, "startDate", null), _defineProperty(_ref, "episodeadded", null), _defineProperty(_ref, "startDateParsed", null), _defineProperty(_ref, "currentDate", null), _defineProperty(_ref, "endDate", null), _defineProperty(_ref, "freeTrial", true), _defineProperty(_ref, "checkLibrary", []), _defineProperty(_ref, "inLibrary", false), _defineProperty(_ref, "usersub", null), _defineProperty(_ref, "showUpgrade", false), _defineProperty(_ref, "added", false), _defineProperty(_ref, "wishes", 0), _defineProperty(_ref, "swiperOption", {
      slidesPerView: 3,
      spaceBetween: 30,

      breakpoints: {
        320: {
          slidesPerView: 3,
          spaceBetween: 15
        },

        425: {
          slidesPerView: 3,
          spaceBetween: 15
        },
        640: {
          slidesPerView: 4,
          spaceBetween: 30
        }
      },
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    }), _defineProperty(_ref, "isLiked", false), _defineProperty(_ref, "isUnliked", false), _defineProperty(_ref, "allLikes", []), _defineProperty(_ref, "allUnlikes", []), _defineProperty(_ref, "update", 0), _ref;
  },
  components: {
    "app-videoBanner": __WEBPACK_IMPORTED_MODULE_2__components_categoryVideoBannerComponent___default.a,
    SocialSharing: __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default.a
  },

  created: function created() {
    var _this = this;

    this.vendorInfo;

    this.updateList();
    this.isActive = true;
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user != null) {
      axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_1__config__["b" /* getCustomerHeader */])() }).then(function (response) {
        var userDate = response.data.created_at;
        _this.startDate = new Date(userDate);

        _this.startDateParsed = Date.parse(new Date(userDate));
        _this.currentDate = Date.parse(new Date());

        _this.endDate = Date.parse(new Date(_this.startDate.setDate(_this.startDate.getDate(userDate) + 30)));

        if (_this.currentDate >= _this.endDate) {
          _this.freeTrial = false;
        } else {}
      });

      this.token = user.access_token;
      axios.get("/api/user/orders", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        response.data.data.forEach(function (element) {
          element.orderDetail.forEach(function (item) {
            if (item.type === "VIDEOS") {
              _this.checkLibrary.push(item);
            }
          });
        });
      });
      axios.post("/api/user/subscription-plan/" + user.id).then(function (response) {
        _this.usersub = Number(response.data[0].level);
      });
    }

    this.id = this.$route.params.id;
  },


  watch: {
    $route: function $route() {
      this.updateList();
    },

    update: "updateThis"
  },
  computed: {
    relatedVids: function relatedVids() {
      if (this.related === "default") {
        return this.relatedVideos;
      }
      if (this.related === "latest") {
        return this.relatedVideos.reverse();
      }
      if (this.related === "popular") {
        return this.relatedVideos.reverse();
      }
    },
    myLikes: function myLikes() {
      return this.allLikes.length;
    },
    myUnLikes: function myUnLikes() {
      return this.allUnlikes.length;
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      this.email = user.email;
      this.user_id = user.id;
      this.user = user;
      this.authenticate = true;
    }

    axios.get("/api/admin/product/bizguruh/Videos").then(function (response) {
      if (response.status === 200) {
        _this2.isActive = false;
        var product = response.data.data;
        product.forEach(function (item, index) {
          item.webinar.category = item.category.name;
          item.webinar.subLevel = item.subscriptionLevel;

          item.webinar.uid = item.id;
          _this2.videos.push(item.webinar);
        });
      }
    }).catch(function (error) {});

    var id = this.id;

    axios.get("/api/product-detail/" + id).then(function (response) {
      if (response.status === 200) {
        _this2.videoDetail = response.data.data[0].webinar;
        _this2.videoDetail.subscribePrice = response.data.data[0].subscribePrice;
        _this2.videoDetail.streamPrice = response.data.data[0].streamPrice;
        _this2.videoDetail.subLevel = response.data.data[0].subscriptionLevel;
        _this2.videoDetail.uid = response.data.data[0].id;
        _this2.videoDetail.videoPrice = response.data.data[0].videoPrice;
        if (response.data.data[0].industry != null) {
          _this2.videoDetail.industry = response.data.data[0].industry.name;
        }
        _this2.getMediaDetail(_this2.videoDetail.id);
        _this2.getProductReviews(_this2.videoDetail.uid);
        _this2.player.forEach(function (item) {
          item.source(_this2.videoDetail.excerptsPublicId);
        });
        _this2.getRelatedVideoProduct(id);
        axios.get("/api/getaVendorDetail/" + _this2.videoDetail.uid).then(function (response) {
          _this2.vendorInfo = response.data;
          if (user !== null) {
            _this2.$emit("activity", _this2.vendorInfo.id, 0, 1);
            _this2.$emit("top-content", _this2.vendorInfo.id, _this2.videoDetail.uid, 1);
            if (_this2.videoDetail.industry != null) {
              _this2.$emit("top-industry", _this2.vendorInfo.id, _this2.videoDetail.industry, 1);
            }

            _this2.$emit("reach", user.id, _this2.videoDetail.product_id, _this2.vendorInfo.id);
            _this2.$emit("interaction", _this2.vendorInfo.id, "views");
            _this2.$emit("add-location", _this2.vendorInfo.id, user.location);
            _this2.$emit("add-gender", _this2.vendorInfo.id, user.gender);
            _this2.getLike(_this2.videoDetail.product_id);
            _this2.getUnLike(_this2.videoDetail.product_id);
            _this2.$emit("add-age", _this2.vendorInfo.id, user.age);
          }
        });
        if (!isNaN(parseInt(_this2.videoDetail.videoPrice))) {
          _this2.title = "Video";
          _this2.hardpriceCont = false;
          _this2.hardCopy = true;
          _this2.price = _this2.videoDetail.videoPrice;
        } else if (!isNaN(parseInt(_this2.videoDetail.subscribePrice))) {
          _this2.title = "Subscription";
          _this2.softpriceCont = false;
          _this2.softCopy = true;
          _this2.price = _this2.videoDetail.subscribePrice;
        } else if (!isNaN(parseInt(_this2.videoDetail.streamPrice))) {
          _this2.title = "Streaming";
          _this2.readOnlinepriceCont = false;
          _this2.readOnline = true;
          _this2.price = _this2.videoDetail.streamPrice;
        }

        if (_this2.videoDetail.subscribePrice !== "" || _this2.videoDetail.streamPrice !== "" || _this2.videoDetail.videoPrice !== "") {
          _this2.purchaseCheck = true;
        } else {
          _this2.purchaseCheck = false;
        }
        _this2.checkInLibrary(_this2.videoDetail.uid);
        if (_this2.user !== null) {
          _this2.wishlist(_this2.videoDetail.uid);
          axios.post("/api/cart/check/" + _this2.user.id + "/" + _this2.videoDetail.uid).then(function (response) {
            if (response.data === true) {
              _this2.inCart = true;
            } else {
              _this2.inCart = false;
            }
          });
        }
      }
    }).catch(function (error) {
      console.log(error);
    });
    if (this.videoDetail.streamPrice > 0) {
      this.togglePrice("streamPrice");
    }
  },


  methods: {
    updateThis: function updateThis() {
      this.getLike(this.videoDetail.product_id);
      this.getUnLike(this.videoDetail.product_id);
    },
    checkLike: function checkLike(user, product) {
      var _this3 = this;

      axios.get("/api/get-likes/" + user + "/" + product).then(function (response) {
        if (response.status === 200) {
          if (response.data.like == 1 && response.data.unlike == 0) {
            _this3.isLiked = true;
            _this3.isUnliked = false;
          }
          if (response.data.unlike == 1 && response.data.like == 0) {
            _this3.isLiked = false;
            _this3.isUnliked = true;
          }
          if (response.data.unlike == 0 && response.data.like == 0) {
            _this3.isLiked = false;
            _this3.isUnliked = false;
          }
        }
      });
    },
    getLike: function getLike(product) {
      var _this4 = this;

      axios.get("/api/get-all-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this4.allLikes = response.data;
        }
      });
    },
    getUnLike: function getUnLike(product) {
      var _this5 = this;

      axios.get("/api/get-un-likes/" + product).then(function (response) {
        if (response.status === 200) {
          _this5.allUnlikes = response.data;
        }
      });
    },
    like: function like(user, product, vendor, type) {
      if (!this.isLiked) {
        this.update++;
        this.isLiked = true;
        this.isUnliked = false;
        this.$emit("likes", user, product, vendor, type);
        this.$emit("interaction", vendor, "likes");
      }
    },
    unlike: function unlike(user, product, vendor, type) {
      if (!this.isUnliked) {
        this.update++;
        this.isLiked = false;
        this.isUnliked = true;
        this.$emit("likes", user, product, vendor, type);
      }
    },
    isEmpty: function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) return false;
      }
      return true;
    },
    shareNow: function shareNow() {
      this.share = !this.share;
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    searchDataItem: function searchDataItem() {
      this.$emit("sendSearchData", this.searchItem);
    },
    wishlist: function wishlist(product) {
      var _this6 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      axios.get("/api/wish-list/" + product, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status = 200) {
          if (response.data === true) {
            _this6.added = true;
          } else {
            _this6.added = false;
          }
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    addToWishlist: function addToWishlist() {
      var _this7 = this;

      if (this.authenticate === false) {
        this.$toasted.error("Login to add to wishlist");
      } else {
        var data = {
          userId: this.user_id,
          productId: this.videoDetail.uid,
          vendorId: 1
        };
        if (this.added) {
          this.$toasted.error("Already added to wishlist");
        } else {
          axios.post("/api/wishlist", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this7.added = true;
              _this7.$toasted.success("Successfully added to wishlist");
            } else {
              _this7.$toasted.error("Already added to wishlist");
            }
          }).catch(function (error) {
            console.log(error);
          });
        }
      }
    },
    AddVideoToLibrary: function AddVideoToLibrary(id, name) {
      var _this8 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Subscription Video",
        name: name
      };

      if (this.freeTrial === true || this.usersub > 0) {
        if (this.usersub >= this.videoDetail.subLevel) {
          axios.post("/api/user/sendvideotolibrary", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this8.$toasted.success("Successfully added to BizLibrary");
              _this8.inLibrary = true;
            } else if (response.data == "Order Already Added") {
              _this8.$toasted.error("You have already added this product before");
            } else if (response.data.status === 99 || response.data === " " || response.data.data === "error") {
              _this8.showUpgrade = true;
            } else {
              _this8.showUpgrade = true;
            }
          }).catch(function (error) {
            if (error.response.data.message === "Unauthenticated.") {
              _this8.$router.push({
                name: "auth",
                params: { name: "login" },
                query: { redirect: _this8.$route.fullPath }
              });
            } else {
              _this8.$toasted.error(error.response.data.message);
            }
          });
        } else {
          this.showUpgrade = true;
        }
      } else {
        this.showUpgrade = true;
      }
    },
    getProductReviews: function getProductReviews(id) {
      var _this9 = this;

      var data = {
        id: id
      };
      axios.post("/api/product/get-reviews", JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          _this9.allReviews = response.data.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    watchEpisodeNow: function watchEpisodeNow(e, vid, id, price) {
      var _this10 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Stream Subscription Episode Video",
        type: "EV",
        vid: vid
      };
      if (this.freeTrial === true || this.usersub >= this.videoDetail.subLevel) {
        axios.post("/api/user/watchnowepisodemedia", JSON.parse(JSON.stringify(data)), { headers: { Authorization: "Bearer " + this.user.access_token } }).then(function (response) {
          localStorage.removeItem("mediaStream");

          if (response.status === 200) {
            if (response.data.status == 'failed') {
              _this10.showUpgrade = true;
            } else {
              localStorage.setItem("mediaStream", JSON.stringify(data));
              var routeData = _this10.$router.resolve({
                name: "WatchMedia",
                params: {
                  id: _this10.videoDetail.uid,
                  prodtype: "Videos",
                  gettype: "insight-streaming-video",
                  type: "AV",
                  vid: "videos"
                }
              });

              window.open(routeData.href);
            }
          }
        }).catch(function (error) {
          if (error.response.data.message === "Unauthenticated.") {
            _this10.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: _this10.$router.fullPath }
            });
          } else {
            _this10.$toasted.error(error.response.data.message);
            console.log(error.response.data.message);
          }
        });
      } else {
        this.showUpgrade = true;
      }
    },
    sendEpisodeToLibrary: function sendEpisodeToLibrary(vid, id, name) {
      var _this11 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Subscription Episode Video",
        name: name,
        vid: vid
      };

      if (this.freeTrial === true || this.usersub >= this.videoDetail.subLevel) {
        axios.post("/api/user/sendvideoepisodetolibrary", JSON.parse(JSON.stringify(data)), {
          headers: {
            Authorization: "Bearer " + this.user.access_token
          }
        }).then(function (response) {
          if (response.status === 201) {
            _this11.$toasted.success("Successfully added to BizLibrary");
            _this11.checkEpisodeInLibrary(vid);
            // this.episodeadded = true
          } else if (response.data === "you have added this product already") {
            _this11.$toasted.error("You have already added this product before");
          } else if (response.data.status === 99 || response.data === " " || response.data.data === "error") {
            _this11.showUpgrade = true;
          } else {
            _this11.showUpgrade = true;
          }
        }).catch(function (error) {
          console.log(error);
          if (error.response.data.message === "Unauthenticated.") {
            _this11.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: _this11.$router.fullPath }
            });
          } else {
            _this11.$toasted.error(error.response.data.message);
            console.log(error.response.data.message);
          }
        });
      } else {
        this.showUpgrade = true;
      }
    },
    showTabs: function showTabs(params) {
      switch (params) {
        case "shipping":
          this.shipping = this.shipTabSwitch = this.reviewTab = this.shareTab = this.aboutWriterTab = true;
          this.reviews = this.aboutWriter = this.episode = this.shipTab = this.reviewTabSwitch = this.shareTabSwitch = this.shareTabSwitch = this.share = this.episodeTabSwitch = this.aboutWriter = this.aboutWriterTabSwitch = false;
          break;
        case "reviews":
          this.reviewTabSwitch = this.reviews = this.shipTab = this.shareTab = this.aboutWriterTab = true;
          this.shipTabSwitch = this.aboutWriter = this.episode = this.reviewTab = this.shipping = this.shareTabSwitch = this.share = this.episodeTabSwitch = this.aboutWriterTabSwitch = false;
          break;
        case "share":
          this.shipping = this.aboutWriter = this.episode = this.reviews = this.reviewTabSwitch = this.shipTab = this.shareTab = this.shipTabSwitch = this.episodeTabSwitch = this.aboutWriterTabSwitch = false;
          this.share = this.shareTabSwitch = this.reviewTab = this.shipTab = this.aboutWriterTab = true;
          break;
        case "episode":
          this.shipping = this.aboutWriter = this.share = this.reviews = this.reviewTabSwitch = this.shipTabSwitch = this.shareTabSwitch = false;
          this.episode = this.episodeTabSwitch = this.reviewTab = this.shipTab = this.shareTab = true;
          break;
        case "aboutWriter":
          this.aboutWriter = this.aboutWriterTabSwitch = this.shareTab = this.shipTab = this.reviewTab = true;
          this.aboutWriterTab = this.shareTabSwitch = this.shipTabSwitch = this.reviewTabSwitch = this.episodeTabSwitch = this.shipping = this.share = this.reviews = this.episode = false;

          break;
        default:
          this.shipping = false;
          this.reviews = false;
      }
    },
    getRelatedVideoProduct: function getRelatedVideoProduct(id) {
      var _this12 = this;

      axios.get("/api/get/related/Videos/" + id).then(function (response) {
        if (response.status === 200) {
          var relatedProduct = response.data.data;
          relatedProduct.forEach(function (item, index) {
            item.webinar.category = item.category.name;
            item.webinar.uid = item.id;
            item.webinar.cover = item.coverImage;
            _this12.relatedVideos.push(item.webinar);
            // this.relatedPlaylist[index].source(item.webinar.excerptsPublicId);
          });

          _this12.relatedPlaylist.forEach(function (item, index) {
            item.source(_this12.relatedVideos[index].excerptsPublicId);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    togglePrice: function togglePrice(price) {
      if (price === "streamPrice") {
        this.price = this.videoDetail.streamPrice;
        this.softpriceCont = this.hardCopy = this.readOnline = false;
        this.softCopy = this.hardpriceCont = this.readOnlinepriceCont = true;
        this.title = "Streaming";
      } else if (price === "subscribePrice") {
        this.price = this.videoDetail.subscribePrice;
        this.readOnlinepriceCont = this.hardCopy = this.softCopy = false;
        this.readOnline = this.softpriceCont = this.hardpriceCont = true;
        this.title = "Subscription";
      }
    },
    addEpisodeToCart: function addEpisodeToCart(e, vid, id, price) {
      e.preventDefault();

      var cart = {
        productId: id,
        mediaId: vid,
        price: price,
        prodType: "VideoEpisode",
        quantity: 0,
        vidEpisode: true
      };

      if (this.authenticate === false) {
        cart.cartNum = 1;
        this.anonymousCart.push(cart);
        if (JSON.parse(localStorage.getItem("userCart")) === null) {
          localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
          var sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var cartCount = sessionCart.length;
          this.$emit("getCartCount", cartCount);
        } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
          var _sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var ss = _sessionCart.length;
          _sessionCart[ss] = cart;
          localStorage.setItem("userCart", JSON.stringify(_sessionCart));
          var a = JSON.parse(localStorage.getItem("userCart"));
          var aCount = a.length;
          this.$emit("getCartCount", aCount);
        }
      } else {
        this.addCart(cart);
      }
    },
    rateWithStar: function rateWithStar(num) {
      switch (num) {
        case "1":
          this.NotratedOne = !this.NotratedOne;
          this.RatedOne = !this.RatedOne;
          this.review.rating = 1;
          break;
        case "2":
          this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
          this.RatedTwo = this.RatedOne = !this.RatedTwo;
          this.review.rating = 2;
          break;
        case "3":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
          this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
          this.review.rating = 3;
          break;
        case "4":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
          this.review.rating = 4;
          break;
        case "5":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
          this.review.rating = 5;
          break;
        default:
          this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
          this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
      }
    },
    submitReview: function submitReview() {
      var _this13 = this;

      if (this.authenticate === false) {
        this.$toasted.error("You must log in to review a product");
      } else {
        this.review.productId = this.videoDetail.uid;
        axios.post("/api/user/reviews", JSON.parse(JSON.stringify(this.review)), {
          headers: { Authorization: "Bearer " + this.user.access_token }
        }).then(function (response) {
          if (response.status === 201) {
            _this13.$toasted.success("Reviews successfully saved");
            _this13.review.productId = _this13.review.title = _this13.review.description = "";
            _this13.review.rating = 0;
            _this13.NotratedOne = _this13.NotratedTwo = _this13.NotratedThree = _this13.NotratedFour = _this13.NotratedFive = true;
            _this13.RatedOne = _this13.RatedTwo = _this13.RatedThree = _this13.RatedFour = _this13.RatedFive = false;
          } else {
            _this13.$toasted.error("You cannot review this product until you purchase it");
          }
        }).catch(function (error) {
          var errors = Object.values(error.response.data.errors);
          errors.forEach(function (item) {
            _this13.$toasted.error(item[0]);
          });
        });
      }
    },
    checkInLibrary: function checkInLibrary(id) {
      var _this14 = this;

      this.checkLibrary.forEach(function (item) {
        if (Number(id) === Number(item.productId)) {
          _this14.inLibrary = true;
        } else {
          _this14.inLibrary = false;
        }
      });
    },
    checkEpisodeInLibrary: function checkEpisodeInLibrary(id) {
      var _this15 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/user/orders", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        response.data.data.forEach(function (element) {
          element.orderDetail.forEach(function (item) {
            if (item.type === "VIDEOS") {
              _this15.checkLibrary.push(item);
            }
          });
        });
        _this15.checkLibrary.forEach(function (element) {
          if (Number(element.itemPurchase) === Number(id)) {
            _this15.episodeadded = Number(id);
          }
        });
      });
    },
    getMediaDetail: function getMediaDetail(id) {
      var _this16 = this;

      axios.get("/api/get/getvideodetail/" + id).then(function (response) {
        if (response.status === 200) {
          _this16.episodeArray = response.data;
          _this16.episodeArray.forEach(function (item) {
            _this16.checkLibrary.forEach(function (element) {
              if (Number(element.itemPurchase) === Number(item.id)) {
                _this16.episodeadded = Number(item.id);
              }
            });
          });
          //  this.checkInLibrary(this.episodeArray.id, this.episodeArray.name)
          localStorage.setItem("episodeCount", JSON.stringify(_this16.episodeArray.length));
          _this16.episodeArray.forEach(function (item) {
            _this16.$set(item, "showHide", false);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    addToCart: function addToCart(id) {
      if (this.price === "") {
        this.$toasted.error("Please Select a price");
      } else {
        var cart = {
          productId: id,
          price: this.price
        };
        if (this.title === "Video") {
          cart.quantity = 0;
          cart.prodType = "Video";
        } else if (this.title === "Streaming") {
          cart.quantity = 0;
          cart.prodType = "Streaming";
        } else if (this.title === "Subscription") {
          cart.quantity = 0;
          cart.prodType = "Subscription";
        }

        if (this.authenticate === false) {
          cart.cartNum = 1;
          this.anonymousCart.push(cart);
          if (JSON.parse(localStorage.getItem("userCart")) === null) {
            localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
            var sessionCart = JSON.parse(localStorage.getItem("userCart"));
            var cartCount = sessionCart.length;
            this.$emit("getCartCount", cartCount);
            this.$notify({
              group: "cart",
              title: this.videoDetail.title,
              text: "Successfully Added to Cart!"
            });
          } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
            var _sessionCart2 = JSON.parse(localStorage.getItem("userCart"));
            var ss = _sessionCart2.length;
            _sessionCart2[ss] = cart;
            localStorage.setItem("userCart", JSON.stringify(_sessionCart2));
            var a = JSON.parse(localStorage.getItem("userCart"));
            var aCount = a.length;
            this.$emit("getCartCount", aCount);
            this.$notify({
              group: "cart",
              title: this.videoDetail.title,
              text: "Successfully Added to Cart!"
            });
          }
        } else {
          this.addCart(cart);
        }
      }
    },
    addCart: function addCart(cart) {
      var _this17 = this;

      if (this.inCart === true) {
        this.$toasted.error("Already in cart");
      } else {
        axios.post("/api/cart", JSON.parse(JSON.stringify(cart)), {
          headers: { Authorization: "Bearer " + this.user.access_token }
        }).then(function (response) {
          if (response.status === 201) {
            _this17.$emit('plusCart');
            _this17.inCart = true;
            _this17.loginCart.push(response.data);
            var aCount = _this17.loginCart.length;
            _this17.$emit("getCartCount", aCount);
            _this17.$notify({
              group: "cart",
              title: _this17.videoDetail.title,
              text: "Successfully Added to Cart!"
            });
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    },
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    redirectUpgrade: function redirectUpgrade() {
      this.showUpgrade = false;
      var routeData = this.$router.resolve({
        name: "SubscriptionProfile",
        params: {
          level: this.videoDetail.subLevel
        }
      });

      window.open(routeData.href, "_blank");
    },
    gotoRelated: function gotoRelated(related_id) {
      var routeData = this.$router.resolve({
        name: "Video",
        params: { id: related_id }
      });

      window.open(routeData.href, "_self");
    }
  }
});

/***/ }),

/***/ 1543:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm.showUpgrade
      ? _c("div", { staticClass: "upgradeContainer animated fadeIn" }, [
          _c("div", { staticClass: "upgrade" }, [
            _c("img", {
              staticClass: "sad",
              attrs: { src: "/images/sad.svg", alt: "" }
            }),
            _vm._v(" "),
            _c("p", { staticClass: "upgradeText" }, [
              _vm._v(
                "Oops, looks like you need to upgrade to access this. It’ll only take a sec though. 😁"
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "upgradeButtons" }, [
              _c(
                "div",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                  on: { click: _vm.redirectUpgrade }
                },
                [_vm._v("Yes please!")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "elevated_btn elevated_btn_sm text-dark",
                  on: { click: _vm.closeUpgrade }
                },
                [_vm._v("I'll keep exploring")]
              )
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "videoContainer" }, [
        _c("video", {
          attrs: { src: _vm.videoDetail.excerpts, controls: "", autoplay: "" }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "about" }, [
        _vm.videoDetail !== null && _vm.videoDetail !== undefined
          ? _c("p", { staticClass: "videoTitle" }, [
              _vm._v(_vm._s(_vm.videoDetail.title))
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.videoDetail.videoPrice
          ? _c("p", { staticClass: "vidP" }, [
              _vm._v("₦" + _vm._s(_vm.videoDetail.videoPrice) + ".00")
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "mvc" }, [
          _c("p", { staticClass: "creation" }, [
            _vm._v(
              _vm._s(
                _vm._f("moment")(
                  _vm.videoDetail.created_at,
                  "dddd, MMMM Do YYYY"
                )
              )
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "mvc_2" }, [
            _vm.share
              ? _c(
                  "div",
                  { staticClass: "shareBtn" },
                  [
                    _c("social-sharing", {
                      attrs: {
                        url: _vm.url,
                        title: _vm.videoDetail.title,
                        description: _vm.videoDetail.description,
                        quote: _vm.videoDetail.title,
                        hashtags: "bizguruh,videos"
                      },
                      inlineTemplate: {
                        render: function() {
                          var _vm = this
                          var _h = _vm.$createElement
                          var _c = _vm._self._c || _h
                          return _c(
                            "div",
                            { staticClass: "d-flex" },
                            [
                              _c(
                                "network",
                                { attrs: { network: "facebook" } },
                                [
                                  _c("i", {
                                    staticClass:
                                      "fa fa-facebook ml-2 fb cpointer"
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c("network", { attrs: { network: "twitter" } }, [
                                _c("i", {
                                  staticClass:
                                    "fa fa-twitter ml-2 tweet cpointer"
                                })
                              ]),
                              _vm._v(" "),
                              _c(
                                "network",
                                { attrs: { network: "googleplus" } },
                                [
                                  _c("i", {
                                    staticClass:
                                      "fa fa-google-plus ml-2 google cpointer"
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "network",
                                { attrs: { network: "linkedin" } },
                                [
                                  _c("i", {
                                    staticClass:
                                      "fa fa-linkedin ml-2 linkedin cpointer"
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "network",
                                { attrs: { network: "whatsapp" } },
                                [
                                  _c("i", {
                                    staticClass:
                                      "fa fa-whatsapp ml-2 whatsapp cpointer"
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        },
                        staticRenderFns: []
                      }
                    })
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "like mr-3",
                on: {
                  click: function($event) {
                    return _vm.like(
                      _vm.user.id,
                      _vm.videoDetail.product_id,
                      _vm.vendorInfo.id,
                      "like"
                    )
                  }
                }
              },
              [
                _c("p", { staticClass: "hoverLike" }, [_vm._v("I like this")]),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fas fa-thumbs-up text-main mr-1",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v("\n            " + _vm._s(_vm.myLikes) + "\n          ")
              ]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "dislike",
                on: {
                  click: function($event) {
                    return _vm.unlike(
                      _vm.user.id,
                      _vm.videoDetail.product_id,
                      _vm.vendorInfo.id,
                      "unlike"
                    )
                  }
                }
              },
              [
                _c("i", { staticClass: "fas fa-thumbs-down text-main mr-1" }),
                _vm._v(
                  "\n            " + _vm._s(_vm.myUnLikes) + "\n            "
                ),
                _c("p", { staticClass: "hoverDislike" }, [
                  _vm._v("I dislike this")
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "share", on: { click: _vm.shareNow } }, [
              _c("i", {
                staticClass: "fa fa-share text-main mr-1",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" SHARE\n          ")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "desc-container" }, [
          _c("div", { staticClass: "description" }, [
            _c("p", { staticClass: "desc" }, [
              _vm._v(_vm._s(_vm.videoDetail.description))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mbc" }, [
              _c(
                "small",
                { staticClass: "hostBy" },
                [
                  _vm._v("\n              By:\n              "),
                  !_vm.isEmpty(_vm.vendorInfo)
                    ? _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "PartnerProfile",
                              params: { username: _vm.vendorInfo.username }
                            }
                          }
                        },
                        [
                          _c("span", { staticClass: "hostName" }, [
                            _vm._v(_vm._s(_vm.vendorInfo.storeName))
                          ])
                        ]
                      )
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "buttons" }, [
            !_vm.purchaseCheck
              ? _c("div", [
                  !_vm.inLibrary
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "elevated_btn text-white btn-compliment w-100",
                          attrs: { id: "send" },
                          on: {
                            click: function($event) {
                              return _vm.AddVideoToLibrary(
                                _vm.videoDetail.uid,
                                _vm.videoDetail.title
                              )
                            }
                          }
                        },
                        [_vm._v("Add to BizLibrary")]
                      )
                    : _c(
                        "div",
                        {
                          staticClass:
                            "elevated_btn text-white btn-compliment w-100",
                          attrs: { id: "send" }
                        },
                        [_vm._v("Added to BizLibrary")]
                      )
                ])
              : _c("div", [
                  !_vm.inLibrary
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "elevated_btn text-white btn-compliment w-100",
                          attrs: { id: "addCart" },
                          on: {
                            click: function($event) {
                              return _vm.addToCart(_vm.videoDetail.uid)
                            }
                          }
                        },
                        [_vm._v("Add to Cart")]
                      )
                    : _c(
                        "div",
                        {
                          staticClass:
                            "elevated_btn text-white btn-compliment w-100",
                          attrs: { id: "addCart" }
                        },
                        [_vm._v("Purchased")]
                      )
                ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "review-container" }, [
          _c(
            "div",
            { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12 Videorow" },
            [
              _c(
                "div",
                {
                  staticClass: "text-center",
                  class: {
                    episodeTab: _vm.episodeTab,
                    episodeTabSwitch: _vm.episodeTabSwitch
                  },
                  on: {
                    click: function($event) {
                      return _vm.showTabs("episode")
                    }
                  }
                },
                [_vm._v("Episode Info")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "text-center",
                  class: {
                    reviewTab: _vm.reviewTab,
                    reviewTabSwitch: _vm.reviewTabSwitch
                  },
                  on: {
                    click: function($event) {
                      return _vm.showTabs("reviews")
                    }
                  }
                },
                [_vm._v("Write Review")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "text-center",
                  class: {
                    aboutWriterTab: _vm.aboutWriterTab,
                    aboutWriterTabSwitch: _vm.aboutWriterTabSwitch
                  },
                  on: {
                    click: function($event) {
                      return _vm.showTabs("aboutWriter")
                    }
                  }
                },
                [_vm._v("About Expert")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "text-center",
                  class: {
                    shipTab: _vm.shipTab,
                    shipTabSwitch: _vm.shipTabSwitch
                  },
                  on: {
                    click: function($event) {
                      return _vm.showTabs("shipping")
                    }
                  }
                },
                [_vm._v("Sponsor Info")]
              )
            ]
          ),
          _vm._v(" "),
          _vm.episode
            ? _c(
                "div",
                {
                  staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12 epiPad"
                },
                [
                  _c(
                    "div",
                    _vm._l(_vm.episodeArray, function(episodeArrays, index) {
                      return _vm.episodeArray.length > 0
                        ? _c("div", [
                            _c("div", { staticClass: "border" }, [
                              _c("div", { staticClass: "episodeTitles" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass: "dii",
                                    on: {
                                      click: function($event) {
                                        episodeArrays.showHide
                                          ? (episodeArrays.showHide = false)
                                          : (episodeArrays.showHide = true)
                                      }
                                    }
                                  },
                                  [
                                    _c("div", { staticClass: "episode-text" }, [
                                      !episodeArrays.showHide
                                        ? _c("i", {
                                            staticClass:
                                              "fa fa-caret-down mr-2",
                                            attrs: { "aria-hidden": "true" }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      episodeArrays.showHide
                                        ? _c("i", {
                                            staticClass: "fa fa-caret-up mr-2",
                                            attrs: { "aria-hidden": "true" }
                                          })
                                        : _vm._e(),
                                      _vm._v(
                                        "\n                      " +
                                          _vm._s(episodeArrays.title) +
                                          "\n                    "
                                      )
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                episodeArrays.price === ""
                                  ? _c("div", { staticClass: "epiPriceDiv" }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "epiPrice cartAdd elevated_btn elevated_btn_sm text-main",
                                          on: {
                                            click: function($event) {
                                              return _vm.watchEpisodeNow(
                                                $event,
                                                episodeArrays.id,
                                                _vm.videoDetail.uid
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass:
                                              "fas fa-tv text-main icon_social"
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            { staticClass: "desktop" },
                                            [_vm._v("Watch")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            { staticClass: "cartAddText" },
                                            [_vm._v("watch")]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _vm.inLibrary ||
                                      _vm.episodeadded ===
                                        Number(episodeArrays.id)
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "epiPrice cartAdd elevated_btn elevated_btn_sm btn-compliment text-white"
                                            },
                                            [
                                              _c(
                                                "span",
                                                { staticClass: "desktop" },
                                                [_vm._v("Added to library")]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "cartAddText" },
                                                [_vm._v("Added to library")]
                                              )
                                            ]
                                          )
                                        : _c(
                                            "div",
                                            {
                                              staticClass:
                                                "epiPrice cartAdds elevated_btn elevated_btn_sm btn-compliment text-white",
                                              on: {
                                                click: function($event) {
                                                  return _vm.sendEpisodeToLibrary(
                                                    episodeArrays.id,
                                                    _vm.videoDetail.uid,
                                                    episodeArrays.title
                                                  )
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "span",
                                                { staticClass: "desktop" },
                                                [_vm._v("Add to library")]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "cartAddText" },
                                                [_vm._v("Add to library")]
                                              )
                                            ]
                                          )
                                    ])
                                  : _c(
                                      "div",
                                      {
                                        staticClass:
                                          "col-lg-5 col-md-4 col-sm-4 col-xs-4 epiPriceDiv p-0"
                                      },
                                      [
                                        _vm.inLibrary
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "epiPrice cartAdd elevated_btn elevated_btn_sm btn-compliment text-white"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  { staticClass: "desktop" },
                                                  [_vm._v("Purchased")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass: "cartAddText"
                                                  },
                                                  [_vm._v("Purchased")]
                                                )
                                              ]
                                            )
                                          : _c("div", { staticClass: "df" }, [
                                              _c("div", { staticClass: "pr" }, [
                                                _vm._v(
                                                  "₦" +
                                                    _vm._s(episodeArrays.price)
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "m-0 elevated_btn elevated_btn_sm btn-compliment text-white",
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.addEpisodeToCart(
                                                        $event,
                                                        episodeArrays.id,
                                                        _vm.videoDetail.uid,
                                                        episodeArrays.price
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                        Buy\n                        "
                                                  ),
                                                  _c("i", {
                                                    staticClass:
                                                      "fa fa-cart-plus icon_social",
                                                    attrs: {
                                                      "aria-hidden": "true"
                                                    }
                                                  })
                                                ]
                                              )
                                            ])
                                      ]
                                    )
                              ]),
                              _vm._v(" "),
                              episodeArrays.showHide
                                ? _c(
                                    "div",
                                    { staticClass: "episodeDescription" },
                                    [
                                      _c("div", [
                                        _vm._v(_vm._s(episodeArrays.overview))
                                      ]),
                                      _vm._v(" "),
                                      _vm.vendorInfo !== null
                                        ? _c(
                                            "div",
                                            { staticClass: "videoguest" },
                                            [
                                              !_vm.isEmpty(_vm.vendorInfo)
                                                ? _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name:
                                                            "PartnerProfile",
                                                          params: {
                                                            username:
                                                              _vm.vendorInfo
                                                                .username
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "hostName"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.vendorInfo
                                                                .storeName
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                : _vm._e()
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                : _vm._e()
                            ])
                          ])
                        : _vm._e()
                    }),
                    0
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.shipping
            ? _c(
                "div",
                {
                  staticClass:
                    "col-lg-12 col-md-12 col-sm-12 col-xs-12 shipDetailRow"
                },
                [
                  _vm.videoDetail.sponsorName !== ""
                    ? _c("p", { staticClass: "videoguest" }, [
                        _vm._v(_vm._s(_vm.videoDetail.sponsorName))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.videoDetail.aboutSponsor !== ""
                    ? _c("div", { staticClass: "sponsor" }, [
                        _vm._v(_vm._s(_vm.videoDetail.aboutSponsor))
                      ])
                    : _vm._e()
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.reviews
            ? _c(
                "div",
                { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
                [
                  _c("div", { staticClass: "review-div" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.review.description,
                            expression: "review.description"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { rows: "5", placeholder: "Enter Reviews" },
                        domProps: { value: _vm.review.description },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.review,
                              "description",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", [
                      _c("i", {
                        class: {
                          fa: _vm.rateFa,
                          "fa-star-o": _vm.NotratedOne,
                          "fa-star": _vm.RatedOne
                        },
                        on: {
                          click: function($event) {
                            return _vm.rateWithStar("1")
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        class: {
                          fa: _vm.rateFa,
                          "fa-star-o": _vm.NotratedTwo,
                          "fa-star": _vm.RatedTwo
                        },
                        on: {
                          click: function($event) {
                            return _vm.rateWithStar("2")
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        class: {
                          fa: _vm.rateFa,
                          "fa-star-o": _vm.NotratedThree,
                          "fa-star": _vm.RatedThree
                        },
                        on: {
                          click: function($event) {
                            return _vm.rateWithStar("3")
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        class: {
                          fa: _vm.rateFa,
                          "fa-star-o": _vm.NotratedFour,
                          "fa-star": _vm.RatedFour
                        },
                        on: {
                          click: function($event) {
                            return _vm.rateWithStar("4")
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        class: {
                          fa: _vm.rateFa,
                          "fa-star-o": _vm.NotratedFive,
                          "fa-star": _vm.RatedFive
                        },
                        on: {
                          click: function($event) {
                            return _vm.rateWithStar("5")
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", { staticClass: "ml-lg-5" }, [
                        _vm._v(
                          _vm._s(_vm.review.rating) +
                            " " +
                            _vm._s(_vm.review.rating > 1 ? "stars" : "star")
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-reviews mb-4",
                        on: {
                          click: function($event) {
                            return _vm.submitReview()
                          }
                        }
                      },
                      [_vm._v("Submit")]
                    )
                  ])
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.aboutWriter
            ? _c(
                "div",
                { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
                [
                  _c("div", { staticClass: "aboutWriter" }, [
                    _c("p", [_vm._v(_vm._s(_vm.vendorInfo.bio))])
                  ])
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.allReviews.length > 0
            ? _c(
                "div",
                { staticClass: "row", attrs: { id: "reviewed" } },
                [
                  _c("div", { staticClass: "reviews" }, [_vm._v("Reviews")]),
                  _vm._v(" "),
                  _vm._l(_vm.allReviews, function(Review, index) {
                    return _c(
                      "div",
                      {
                        staticClass:
                          "col-lg-12 col-md-12 col-sm-12 col-xs-12 reviewdiv"
                      },
                      [
                        _c("div", { staticClass: "tabre" }, [
                          _c("div", { staticClass: "mb-5 disRfb" }, [
                            _c("div", { staticClass: "floatReview avatarD" }, [
                              _c("span", { staticClass: "reviewAvatar" }, [
                                _vm._v(_vm._s(Review.user.name.charAt(0)))
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "floatReview floatUserRating" },
                              [
                                _c("div", { staticClass: "dateCreated" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("moment")(
                                        Review.created_at.date,
                                        "from"
                                      )
                                    )
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "reviewerName" }, [
                                  _vm._v(
                                    _vm._s(Review.user.name) +
                                      " " +
                                      _vm._s(Review.user.lastName)
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "floatReview" }, [
                              _c(
                                "div",
                                { staticClass: "floatRT" },
                                [
                                  _vm._l(Review.rating, function(star) {
                                    return _c(
                                      "div",
                                      { staticClass: "rateStyle" },
                                      [_c("i", { staticClass: "fa fa-star" })]
                                    )
                                  }),
                                  _vm._v(" "),
                                  _vm._l(5 - Review.rating, function(star) {
                                    return _c(
                                      "div",
                                      { staticClass: "rateStyle" },
                                      [_c("i", { staticClass: "fa fa-star-o" })]
                                    )
                                  })
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "avatarDescription" }, [
                                _vm._v(_vm._s(Review.description))
                              ])
                            ])
                          ])
                        ])
                      ]
                    )
                  })
                ],
                2
              )
            : _vm._e()
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "side-page" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "" } }, [_vm._v("Related videos")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.related,
                expression: "related"
              }
            ],
            staticClass: "form-control",
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.related = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0]
              }
            }
          },
          [
            _c("option", { attrs: { value: "default" } }, [_vm._v("Related")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "latest" } }, [_vm._v("Latest")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "popular" } }, [_vm._v("Popular")])
          ]
        )
      ]),
      _vm._v(" "),
      _vm.relatedVids.length
        ? _c(
            "div",
            _vm._l(_vm.relatedVids, function(relatedVideo) {
              return _c(
                "div",
                { key: relatedVideo.id, staticClass: "related-container" },
                [
                  _c("div", { staticClass: "vid-container" }, [
                    _c("img", {
                      attrs: { src: relatedVideo.cover, alt: "related" },
                      on: {
                        click: function($event) {
                          return _vm.gotoRelated(relatedVideo.uid)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "playCont rounded-circle",
                        on: {
                          click: function($event) {
                            return _vm.gotoRelated(relatedVideo.uid)
                          }
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "fa fa-play playIcon",
                          attrs: { "aria-hidden": "true" }
                        })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "about-related" }, [
                    _c(
                      "div",
                      {
                        staticClass: "related_title",
                        on: {
                          click: function($event) {
                            return _vm.gotoRelated(relatedVideo.uid)
                          }
                        }
                      },
                      [_vm._v(_vm._s(relatedVideo.title))]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "related_host" }, [
                      _vm._v(_vm._s(relatedVideo.host))
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "related_creation" }, [
                      _vm._v(
                        _vm._s(
                          _vm._f("moment")(
                            relatedVideo.created_at,
                            "from",
                            "now"
                          )
                        )
                      )
                    ])
                  ])
                ]
              )
            }),
            0
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6ead8d78", module.exports)
  }
}

/***/ }),

/***/ 584:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1540)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1542)
/* template */
var __vue_template__ = __webpack_require__(1543)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6ead8d78"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/videoComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6ead8d78", Component.options)
  } else {
    hotAPI.reload("data-v-6ead8d78", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 745:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(746)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(748)
/* template */
var __vue_template__ = __webpack_require__(749)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f443e1b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryVideoBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f443e1b", Component.options)
  } else {
    hotAPI.reload("data-v-0f443e1b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(747);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cd849544", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vidBanner[data-v-0f443e1b]{\n     position: relative;\n}\nimg[data-v-0f443e1b]{\n    margin-top: 40px;\n    width: 100%;\n    /* max-height: 300px; */\n}\n.text[data-v-0f443e1b]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size: 60px;\n    color: #ffffff;\n}\n@media(max-width: 475px){\n.vidBanner[data-v-0f443e1b]{\n     display: none;\n}\n.text[data-v-0f443e1b]{\n            font-size:30px;\n            top: 110px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-video-banner-component"
});

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vidBanner" }, [
      _c("img", { attrs: { src: "/images/vid.png" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Videos")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f443e1b", module.exports)
  }
}

/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * vue-social-sharing v2.4.7 
 * (c) 2019 nicolasbeauvais
 * Released under the MIT License.
 */


function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Vue = _interopDefault(__webpack_require__(40));

var SocialSharingNetwork = {
  functional: true,

  props: {
    network: {
      type: String,
      default: ''
    }
  },

  render: function (createElement, context) {
    var network = context.parent._data.baseNetworks[context.props.network];

    if (!network) {
      return console.warn(("Network " + (context.props.network) + " does not exist"));
    }

    return createElement(context.parent.networkTag, {
      staticClass: context.data.staticClass || null,
      staticStyle: context.data.staticStyle || null,
      class: context.data.class || null,
      style: context.data.style || null,
      attrs: {
        id: context.data.attrs.id || null,
        tabindex: context.data.attrs.tabindex || 0,
        'data-link': network.type === 'popup'
          ? '#share-' + context.props.network
          : context.parent.createSharingUrl(context.props.network),
        'data-action': network.type === 'popup' ? null : network.action
      },
      on: {
        click: network.type === 'popup' ? function () {
          context.parent.share(context.props.network);
        } : function () {
          context.parent.touch(context.props.network);
        }
      }
    }, context.children);
  }
};

var email = {"sharer":"mailto:?subject=@title&body=@url%0D%0A%0D%0A@description","type":"direct"};
var facebook = {"sharer":"https://www.facebook.com/sharer/sharer.php?u=@url&title=@title&description=@description&quote=@quote&hashtag=@hashtags","type":"popup"};
var googleplus = {"sharer":"https://plus.google.com/share?url=@url","type":"popup"};
var line = {"sharer":"http://line.me/R/msg/text/?@description%0D%0A@url","type":"popup"};
var linkedin = {"sharer":"https://www.linkedin.com/shareArticle?mini=true&url=@url&title=@title&summary=@description","type":"popup"};
var odnoklassniki = {"sharer":"https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=@url&st.comments=@description","type":"popup"};
var pinterest = {"sharer":"https://pinterest.com/pin/create/button/?url=@url&media=@media&description=@title","type":"popup"};
var reddit = {"sharer":"https://www.reddit.com/submit?url=@url&title=@title","type":"popup"};
var skype = {"sharer":"https://web.skype.com/share?url=@description%0D%0A@url","type":"popup"};
var telegram = {"sharer":"https://t.me/share/url?url=@url&text=@description","type":"popup"};
var twitter = {"sharer":"https://twitter.com/intent/tweet?text=@title&url=@url&hashtags=@hashtags@twitteruser","type":"popup"};
var viber = {"sharer":"viber://forward?text=@url @description","type":"direct"};
var vk = {"sharer":"https://vk.com/share.php?url=@url&title=@title&description=@description&image=@media&noparse=true","type":"popup"};
var weibo = {"sharer":"http://service.weibo.com/share/share.php?url=@url&title=@title","type":"popup"};
var whatsapp = {"sharer":"https://api.whatsapp.com/send?text=@description%0D%0A@url","type":"popup","action":"share/whatsapp/share"};
var sms = {"sharer":"sms:?body=@url%20@description","type":"direct"};
var sms_ios = {"sharer":"sms:;body=@url%20@description","type":"direct"};
var BaseNetworks = {
	email: email,
	facebook: facebook,
	googleplus: googleplus,
	line: line,
	linkedin: linkedin,
	odnoklassniki: odnoklassniki,
	pinterest: pinterest,
	reddit: reddit,
	skype: skype,
	telegram: telegram,
	twitter: twitter,
	viber: viber,
	vk: vk,
	weibo: weibo,
	whatsapp: whatsapp,
	sms: sms,
	sms_ios: sms_ios
};

var inBrowser = typeof window !== 'undefined';
var $window = inBrowser ? window : null;

var SocialSharing = {
  props: {
    /**
     * URL to share.
     * @var string
     */
    url: {
      type: String,
      default: inBrowser ? window.location.href : ''
    },

    /**
     * Sharing title, if available by network.
     * @var string
     */
    title: {
      type: String,
      default: ''
    },

    /**
     * Sharing description, if available by network.
     * @var string
     */
    description: {
      type: String,
      default: ''
    },

    /**
     * Facebook quote
     * @var string
     */
    quote: {
      type: String,
      default: ''
    },

    /**
     * Twitter hashtags
     * @var string
     */
    hashtags: {
      type: String,
      default: ''
    },

    /**
     * Twitter user.
     * @var string
     */
    twitterUser: {
      type: String,
      default: ''
    },

    /**
     * Flag that indicates if counts should be retrieved.
     * - NOT WORKING IN CURRENT VERSION
     * @var mixed
     */
    withCounts: {
      type: [String, Boolean],
      default: false
    },

    /**
     * Google plus key.
     * @var string
     */
    googleKey: {
      type: String,
      default: undefined
    },

    /**
     * Pinterest Media URL.
     * Specifies the image/media to be used.
     */
    media: {
      type: String,
      default: ''
    },

    /**
     * Network sub component tag.
     * Default to span tag
     */
    networkTag: {
      type: String,
      default: 'span'
    },

    /**
     * Additional or overridden networks.
     * Default to BaseNetworks
     */
    networks: {
      type: Object,
      default: function () {
        return {};
      }
    }
  },

  data: function data () {
    return {
      /**
       * Available sharing networks.
       * @param object
       */
      baseNetworks: BaseNetworks,

      /**
       * Popup settings.
       * @param object
       */
      popup: {
        status: false,
        resizable: true,
        toolbar: false,
        menubar: false,
        scrollbars: false,
        location: false,
        directories: false,
        width: 626,
        height: 436,
        top: 0,
        left: 0,
        window: undefined,
        interval: null
      }
    };
  },

  methods: {
    /**
     * Returns generated sharer url.
     *
     * @param network Social network key.
     */
    createSharingUrl: function createSharingUrl (network) {
      var ua = navigator.userAgent.toLowerCase();

      /**
       * On IOS, SMS sharing link need a special formating
       * Source: https://weblog.west-wind.com/posts/2013/Oct/09/Prefilling-an-SMS-on-Mobile-Devices-with-the-sms-Uri-Scheme#Body-only
        */
      if (network === 'sms' && (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1)) {
        network += '_ios';
      }

      var url = this.baseNetworks[network].sharer;

      /**
       * On IOS, Twitter sharing shouldn't include a hashtag parameter if the hashtag value is empty
       * Source: https://github.com/nicolasbeauvais/vue-social-sharing/issues/143
        */
      if (network === 'twitter' && this.hashtags.length === 0) {
        url = url.replace('&hashtags=@hashtags', '');
      }

      return url
        .replace(/@url/g, encodeURIComponent(this.url))
        .replace(/@title/g, encodeURIComponent(this.title))
        .replace(/@description/g, encodeURIComponent(this.description))
        .replace(/@quote/g, encodeURIComponent(this.quote))
        .replace(/@hashtags/g, this.generateHashtags(network, this.hashtags))
        .replace(/@media/g, this.media)
        .replace(/@twitteruser/g, this.twitterUser ? '&via=' + this.twitterUser : '');
    },
    /**
     * Encode hashtags for the specified social network.
     *
     * @param  network Social network key
     * @param  hashtags All hashtags specified
     */
    generateHashtags: function generateHashtags (network, hashtags) {
      if (network === 'facebook' && hashtags.length > 0) {
        return '%23' + hashtags.split(',')[0];
      }

      return hashtags;
    },
    /**
     * Shares URL in specified network.
     *
     * @param network Social network key.
     */
    share: function share (network) {
      this.openSharer(network, this.createSharingUrl(network));

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Touches network and emits click event.
     *
     * @param network Social network key.
     */
    touch: function touch (network) {
      window.open(this.createSharingUrl(network), '_self');

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Opens sharer popup.
     *
     * @param network Social network key
     * @param url Url to share.
     */
    openSharer: function openSharer (network, url) {
      var this$1 = this;

      // If a popup window already exist it will be replaced, trigger a close event.
      var popupWindow = null;
      if (popupWindow && this.popup.interval) {
        clearInterval(this.popup.interval);

        popupWindow.close();// Force close (for Facebook)

        this.$root.$emit('social_shares_change', network, this.url);
        this.$emit('change', network, this.url);
      }

      popupWindow = window.open(
        url,
        'sharer',
        'status=' + (this.popup.status ? 'yes' : 'no') +
        ',height=' + this.popup.height +
        ',width=' + this.popup.width +
        ',resizable=' + (this.popup.resizable ? 'yes' : 'no') +
        ',left=' + this.popup.left +
        ',top=' + this.popup.top +
        ',screenX=' + this.popup.left +
        ',screenY=' + this.popup.top +
        ',toolbar=' + (this.popup.toolbar ? 'yes' : 'no') +
        ',menubar=' + (this.popup.menubar ? 'yes' : 'no') +
        ',scrollbars=' + (this.popup.scrollbars ? 'yes' : 'no') +
        ',location=' + (this.popup.location ? 'yes' : 'no') +
        ',directories=' + (this.popup.directories ? 'yes' : 'no')
      );

      popupWindow.focus();

      // Create an interval to detect popup closing event
      this.popup.interval = setInterval(function () {
        if (!popupWindow || popupWindow.closed) {
          clearInterval(this$1.popup.interval);

          popupWindow = undefined;

          this$1.$root.$emit('social_shares_close', network, this$1.url);
          this$1.$emit('close', network, this$1.url);
        }
      }, 500);
    }
  },

  /**
   * Merge base networks list with user's list
   */
  beforeMount: function beforeMount () {
    this.baseNetworks = Vue.util.extend(this.baseNetworks, this.networks);
  },

  /**
   * Sets popup default dimensions.
   */
  mounted: function mounted () {
    if (!inBrowser) {
      return;
    }

    /**
     * Center the popup on dual screens
     * http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen/32261263
     */
    var dualScreenLeft = $window.screenLeft !== undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop !== undefined ? $window.screenTop : screen.top;

    var width = $window.innerWidth ? $window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width);
    var height = $window.innerHeight ? $window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height);

    this.popup.left = ((width / 2) - (this.popup.width / 2)) + dualScreenLeft;
    this.popup.top = ((height / 2) - (this.popup.height / 2)) + dualScreenTop;
  },

  /**
   * Set component aliases for buttons and links.
   */
  components: {
    'network': SocialSharingNetwork
  }
};

SocialSharing.version = '2.4.7';

SocialSharing.install = function (Vue) {
  Vue.component('social-sharing', SocialSharing);
};

if (typeof window !== 'undefined') {
  window.SocialSharing = SocialSharing;
}

module.exports = SocialSharing;

/***/ })

});