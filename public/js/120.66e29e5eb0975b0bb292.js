webpackJsonp([120],{

/***/ 1654:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1655);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("5c5327db", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-905242ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./specificIndustryComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-905242ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./specificIndustryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1655:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Lato:700);", ""]);

// module
exports.push([module.i, "\n.noResult[data-v-905242ca]{\n    font-family: fantasy;\n    text-align: center;\n    margin: auto;\n}\n.videoView[data-v-905242ca]{\n    position:relative;\n}\n.videoView img[data-v-905242ca]{\n   width:100%;\n   height:100%;\n   -o-object-fit: cover;\n      object-fit: cover;\n}\n.playCont[data-v-905242ca]{\n    position:absolute;\n    width:90px;\n    height:90px;\n      background: #ffffff;\n       border-radius: 50%;\n       overflow:hidden;\n     left:50%;\n    top:50%;\n     opacity:.8;\n     -webkit-transform: translate(-50%, -50%);\n             transform: translate(-50%, -50%);\n}\n.playIcon[data-v-905242ca]{\n    position:absolute;\n    left:50%;\n    top:50%;\n    font-size: 40px;\n    opacity:.8;\n     -webkit-transform: translate(-50%, -50%);\n             transform: translate(-50%, -50%);\n     color:#000000;\n}\n.shadowA[data-v-905242ca]{\n    /* position:absolute; */\n    top: 0;\n    left: 0;\n\tbackground:rgba(255, 255, 255, 0.1);\n\tcolor: #f1f1f1;\n\twidth: 100%;\n\theight: 100%;\n\ttext-align: center;\n    border-radius: 5px;\n}\n.ribbon[data-v-905242ca] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  position: absolute;\n  /* z-index: 70; */\n}\n.ribbon[data-v-905242ca]::before,\n.ribbon[data-v-905242ca]::after {\n  position: absolute;\n  z-index: -1;\n  content: '';\n  display: block;\n  border: 5px solid #2980b9;\n}\n.ribbon span[data-v-905242ca] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 8px 0;\n  background-color: #3498db;\n  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.1);\n          box-shadow: 0 5px 10px rgba(0,0,0,.1);\n  color: #fff;\n  font: 700 18px/1 'Lato', sans-serif;\n  text-shadow: 0 1px 1px rgba(0,0,0,.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n.ribbon-free span[data-v-905242ca] {\n  position: absolute;\n  display: block;\n  width: 225px;\n  padding: 8px 0;\n  background-color: #404346;\n  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.1);\n          box-shadow: 0 5px 10px rgba(0,0,0,.1);\n  color: #fff;\n  font: 700 18px/1 'Lato', sans-serif;\n  text-shadow: 0 1px 1px rgba(0,0,0,.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n\n/* top left*/\n.ribbon-top-left[data-v-905242ca] {\n top: -15px;\nright: -30px;\n}\n.ribbon-top-left[data-v-905242ca]::before,\n.ribbon-top-left[data-v-905242ca]::after {\n  border-top-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-top-left[data-v-905242ca]::before {\n  top: 0;\n  right: 0;\n}\n.ribbon-top-left[data-v-905242ca]::after {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-top-left span[data-v-905242ca] {\n  right: -25px;\n  top: 30px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n/* top right*/\n.ribbon-top-right[data-v-905242ca] {\n  top: 7px;\n  right: 7px;\n}\n.ribbon-top-right[data-v-905242ca]::before,\n.ribbon-top-right[data-v-905242ca]::after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-top-right[data-v-905242ca]::before {\n  top: 0;\n  left: 0;\n}\n.ribbon-top-right[data-v-905242ca]::after {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-top-right span[data-v-905242ca] {\n    left: -7px;\n    top: 22px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n/* bottom left*/\n.ribbon-bottom-left[data-v-905242ca] {\n  bottom: -10px;\n  left: -10px;\n}\n.ribbon-bottom-left[data-v-905242ca]::before,\n.ribbon-bottom-left[data-v-905242ca]::after {\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-905242ca]::before {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-bottom-left[data-v-905242ca]::after {\n  top: 0;\n  left: 0;\n}\n.ribbon-bottom-left span[data-v-905242ca] {\n  right: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(225deg);\n          transform: rotate(225deg);\n}\n\n/* bottom right*/\n.ribbon-bottom-right[data-v-905242ca] {\n  bottom: -10px;\n  right: -10px;\n}\n.ribbon-bottom-right[data-v-905242ca]::before,\n.ribbon-bottom-right[data-v-905242ca]::after {\n  border-bottom-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-905242ca]::before {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-bottom-right[data-v-905242ca]::after {\n  top: 0;\n  right: 0;\n}\n.ribbon-bottom-right span[data-v-905242ca] {\n  left: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(-225deg);\n          transform: rotate(-225deg);\n}\n.cover[data-v-905242ca]{\n    font-size:9px;\n}\n.coursePrice[data-v-905242ca]{\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n    margin-top:0;\n}\n.container[data-v-905242ca]{\n    padding-top:80px ;\n    padding-bottom: 100px;\n}\n.mobile[data-v-905242ca]{\n    display: none;\n}\na[data-v-905242ca]{\n    color: #a3c2dc !important;\n}\n.seeAll[data-v-905242ca]{\n    margin-left: auto;\n}\n.courseTit[data-v-905242ca] {\n    display: -webkit-box!important;\n\ttext-align: left;\n\ttext-transform: capitalize;\n\tcolor: rgba(0, 0, 0, 0.84);\n    font-size: 16px;\n    max-height:40px;\n    -webkit-line-clamp: 2;\n    -moz-line-clamp: 2;\n    -ms-line-clamp: 2;\n    -o-line-clamp:2 ;\n    line-clamp: 2;\n    -webkit-box-orient: vertical;\n    -ms-box-orient: vertical;\n    -o-box-orient: vertical;\n    box-orient: vertical;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: normal;\n    word-break:break-word;\n    margin-top:5px;\n    margin-bottom: 5px;\n}\n.courseOver[data-v-905242ca] {\n\tfont-size: 16px;\n\tcolor: rgba(0, 0, 0, 0.64);\n    text-align: left;\n\ttext-transform: capitalize;\n    font-size: 16px;\n    max-height:20px;\n    -webkit-line-clamp: 1;\n    -moz-line-clamp: 1;\n    -ms-line-clamp: 1;\n    -o-line-clamp:1;\n    line-clamp: 1;\n    -webkit-box-orient: vertical;\n    -ms-box-orient: vertical;\n    -o-box-orient: vertical;\n    box-orient: vertical;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: normal;\n    word-break:break-word;\n}\n.courseLevel[data-v-905242ca] {\n\tfont-size: 12px;\n\tcolor: rgba(0, 0, 0, 0.54);\n\ttext-transform: capitalize;\n}\n.courseDiv[data-v-905242ca] {\n\tdisplay: block;\n\tpadding: 15px 10px 0 15px;\n\tfont-size: small;\n\tcolor: #383838;\n\tfont-weight: 500;\n}\n.price[data-v-905242ca]{\n    line-height: 1.2;\n    color:rgba(0, 0, 0, 0.70);\n    text-align:center;\n    font-size: 18px;\n}\n.vidText[data-v-905242ca]{\n    color:#000000;\n}\n.primary[data-v-905242ca]{\n    background-color:#a3c2dc !important;\n}\n.books[data-v-905242ca]{\n\n    padding:8px;\n}\n.bookContent[data-v-905242ca]{\n\n    height: 350px;\n    -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n            box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n    margin-bottom:20px;\n    border-radius:5px;\n}\n.bookImage[data-v-905242ca]{\n    width: 100%;\n    height: 50%;\n}\n.bookImage img[data-v-905242ca]{\n    width: 100%;\n    height: 100%;\n}\n.article[data-v-905242ca]{\n    padding: 8px;\n     -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n             box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n         margin: 5px 0;\n}\n.mrBody[data-v-905242ca]{\n    overflow: hidden;\n    height: 150px;\n}\n.mresearch[data-v-905242ca]{\n    width: 100%;\n    padding:5px;\n}\n.mrContent[data-v-905242ca]{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    height: 200px;\n     -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n             box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n}\n.mrImage[data-v-905242ca]{\n    height: 100%;\n    width:30%;\n}\n.mrImage img[data-v-905242ca]{\n    width: 100%;\n    height:100%;\n}\n.aboutMr[data-v-905242ca]{\n    width: 70%;\n    padding:5px 10px;\n    display:-webkit-box;\n    display:-ms-flexbox;\n    display:flex;\n\t-ms-flex-line-pack:center;\n\t    align-content:center;\n}\n.mrText[data-v-905242ca]{\n      height:98px;\n     color: rgba(0,0,0,.64)!important;\n        fill: rgba(0,0,0,.64)!important;\n         display: -webkit-box!important;\n        text-align: left;\n        font-size: 15px;\n        -webkit-line-clamp: 4;\n        -moz-line-clamp: 4;\n        -ms-line-clamp: 4;\n        -o-line-clamp:4;\n        line-clamp: 4;\n        -webkit-box-orient: vertical;\n        -ms-box-orient: vertical;\n        -o-box-orient: vertical;\n        box-orient: vertical;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: normal;\n        word-break:break-word;\n        margin-bottom: 10px;\n}\n.mrButton[data-v-905242ca]{\n    position: absolute;\n    top: 22%;\n    right: -10px;\n    color:rgba(0,0,0,.1)\n}\n.mrButton .fa-angle-right[data-v-905242ca]{\n    font-size:80px;\n}\n.mrHeader[data-v-905242ca]{\n    font-weight: bold;\n    line-height:1.2;\n    margin-bottom:10px;\n}\n.video[data-v-905242ca]{\n\n    padding:8px;\n    width: 100%;\n}\n.videoContent[data-v-905242ca]{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    height:200px;\n      -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n              box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n}\n.videoView[data-v-905242ca]{\n    height:100%;\n    width:40%;\n    background: #000000;\n}\n.videoView img[data-v-905242ca]{\n    width:100%;\n    height: 100%;\n}\n.aboutVideo[data-v-905242ca]{\n   width: 60%;\n    height: 100%;\n    padding: 10px;\n    overflow: hidden;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.vidText[data-v-905242ca]{\n   height: 90px;\n      color: rgba(0,0,0,.64)!important;\n        fill: rgba(0,0,0,.64)!important;\n         display: -webkit-box!important;\n        text-align: left;\n        font-size: 15px;\n        -webkit-line-clamp: 5;\n        -moz-line-clamp: 5;\n        -ms-line-clamp: 5;\n        -o-line-clamp:5;\n        line-clamp: 5;\n        -webkit-box-orient: vertical;\n        -ms-box-orient: vertical;\n        -o-box-orient: vertical;\n        box-orient: vertical;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: normal;\n        word-break:break-word;\n        margin-bottom: 10px;\n        line-height: normal;\n}\n.vidButton[data-v-905242ca]{\n   text-align: center;\n    width: 100%;\n}\n.podcast[data-v-905242ca]{\n    padding:8px;\n    width: 100%;\n}\n.podcastContent[data-v-905242ca]{\n\n    height:200px;\n    -webkit-box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n            box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);\n}\n.podcastView[data-v-905242ca]{\n    height:100%;\n    width:60%;\n}\n.podcastView img[data-v-905242ca]{\n    width:100%;\n    height: 100%;\n}\n.author[data-v-905242ca]{\n    color:#a3c2dc;\n    margin: 0 auto;\n    padding-top: 30px;\n    text-align: center;\n}\n.articleDesc[data-v-905242ca] {\n       height: 40px;\n\tcolor: rgba(0,0,0,.64)!important;\n        fill: rgba(0,0,0,.64)!important;\n         display: -webkit-box!important;\n        text-align: left;\n        font-size: 15px;\n        -webkit-line-clamp: 2;\n        -moz-line-clamp: 2;\n        -ms-line-clamp: 2;\n        -o-line-clamp:2;\n        line-clamp: 2;\n        -webkit-box-orient: vertical;\n        -ms-box-orient: vertical;\n        -o-box-orient: vertical;\n        box-orient: vertical;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: normal;\n        word-break:break-word;\n        margin-bottom: 10px;\n}\n@media only screen and (min-width: 960px)\n{\n.container[data-v-905242ca] {\n    max-width: 1100px;\n}\n}\n@media only screen and (max-width: 768px)\n{\n.bookContent[data-v-905242ca]{\n     height: 350px;\n}\n.mrContent[data-v-905242ca]{\n     height:150px;\n     color: rgba(0,0,0,.84) !important;\n}\n.author[data-v-905242ca]{\n     padding-top:10px;\n}\n}\n@media only screen and (max-width: 425px)\n{\n.ribbon[data-v-905242ca] {\n  width: 100px;\n  height: 100px;\n  overflow: hidden;\n  position: absolute;\n  /* z-index: 70; */\n}\n.ribbon[data-v-905242ca]::before,\n.ribbon[data-v-905242ca]::after {\n  position: absolute;\n  z-index: -1;\n  content: '';\n  display: block;\n  border: 5px solid #2980b9;\n}\n.ribbon span[data-v-905242ca] {\n  position: absolute;\n  display: block;\n  width: 190px;\n  padding: 2px 0;\n  background-color: #3498db;\n  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.1);\n          box-shadow: 0 5px 10px rgba(0,0,0,.1);\n  color: #fff;\n  font: 600 14px/1 'Lato', sans-serif;\n  text-shadow: 0 1px 1px rgba(0,0,0,.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n.ribbon-free span[data-v-905242ca] {\n  position: absolute;\n  display: block;\n  width: 190px;\n  padding: 2px 0;\n  background-color: #404346;\n  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.1);\n          box-shadow: 0 5px 10px rgba(0,0,0,.1);\n  color: #fff;\n  font: 600 14px/1 'Lato', sans-serif;\n  text-shadow: 0 1px 1px rgba(0,0,0,.2);\n  text-transform: capitalize;\n  text-align: center;\n}\n/* top left*/\n.ribbon-top-left[data-v-905242ca] {\n  top: -10px;\n  left: -10px;\n}\n.ribbon-top-left[data-v-905242ca]::before,\n.ribbon-top-left[data-v-905242ca]::after {\n  border-top-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-top-left[data-v-905242ca]::before {\n  top: 0;\n  right: 0;\n}\n.ribbon-top-left[data-v-905242ca]::after {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-top-left span[data-v-905242ca] {\n  right: -25px;\n  top: 30px;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n}\n\n/* top right*/\n.ribbon-top-right[data-v-905242ca] {\n top: 2px;\n    right: 2px;\n}\n.ribbon-top-right[data-v-905242ca]::before,\n.ribbon-top-right[data-v-905242ca]::after {\n  border-top-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-top-right[data-v-905242ca]::before {\n  top: 0;\n  left: 0;\n}\n.ribbon-top-right[data-v-905242ca]::after {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-top-right span[data-v-905242ca] {\n  left: -25px;\n  top: 30px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n\n/* bottom left*/\n.ribbon-bottom-left[data-v-905242ca] {\n  bottom: -10px;\n  left: -10px;\n}\n.ribbon-bottom-left[data-v-905242ca]::before,\n.ribbon-bottom-left[data-v-905242ca]::after {\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.ribbon-bottom-left[data-v-905242ca]::before {\n  bottom: 0;\n  right: 0;\n}\n.ribbon-bottom-left[data-v-905242ca]::after {\n  top: 0;\n  left: 0;\n}\n.ribbon-bottom-left span[data-v-905242ca] {\n  right: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(225deg);\n          transform: rotate(225deg);\n}\n\n/* bottom right*/\n.ribbon-bottom-right[data-v-905242ca] {\n  bottom: -10px;\n  right: -10px;\n}\n.ribbon-bottom-right[data-v-905242ca]::before,\n.ribbon-bottom-right[data-v-905242ca]::after {\n  border-bottom-color: transparent;\n  border-right-color: transparent;\n}\n.ribbon-bottom-right[data-v-905242ca]::before {\n  bottom: 0;\n  left: 0;\n}\n.ribbon-bottom-right[data-v-905242ca]::after {\n  top: 0;\n  right: 0;\n}\n.ribbon-bottom-right span[data-v-905242ca] {\n  left: -25px;\n  bottom: 30px;\n  -webkit-transform: rotate(-225deg);\n          transform: rotate(-225deg);\n}\n.container[data-v-905242ca]{\n        padding-left:0;\n        padding-right:0;\n}\n.books[data-v-905242ca]{\n        padding:2px;\n}\n.bookCover[data-v-905242ca]{\n        padding-left:5px;\n        padding-right: 5px;\n}\n.bookContent[data-v-905242ca]{\n        height:270px;\n        border-radius:5px;\n        overflow: hidden;\n}\n.courseDiv[data-v-905242ca]{\n        padding:3px;\n}\n.courseTit[data-v-905242ca]{\n        font-size:14px;\n        max-height:35px;\n        margin-bottom: 2px;\n        line-height: 1.2;\n}\n.courseOver[data-v-905242ca]{\n        font-size: 13px;\n        margin-bottom: 0px;\n}\n.courseLevel[data-v-905242ca]{\n        font-size:11px;\n        line-height:1.3;\n}\n.price[data-v-905242ca]{\n        font-size:12px;\n        font-weight:bold;\n        padding:5px 10px;\n}\n.mrContent[data-v-905242ca]{\n     height:150px;\n}\n.mobile[data-v-905242ca]{\n    display: block;\n}\n.desktop[data-v-905242ca]{\n    display: none;\n}\n /* .mrText{\n     height:82%;\n } */\n.videoContent[data-v-905242ca]{\n     height:150px;\n}\n\n /* .vidText{\n     height: 82%;\n } */\n.v-btn--small[data-v-905242ca] {\n    font-size: 9px;\n    height: 20px;\n    padding: 0 8px;\n}\n}\n@media(max-width:320px){\n.price[data-v-905242ca]{\n        font-size:11px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1656:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            id: '',
            name: '',
            industries: [],
            courses: []

        };
    },
    created: function created() {
        this.id = this.$route.params.id;

        this.name = this.$route.params.name.replace(/-/g, ' ').toUpperCase();
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/api/industries/' + this.id).then(function (res) {
            _this.industries = res.data.data;

            res.data.data.forEach(function (ele) {
                if (ele.prodType === 'Courses') {
                    console.log("Data: mounted ->  this", ele);
                }
            });
        });
    }
});

/***/ }),

/***/ 1657:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row" }, [
      _vm.name === "BOOKS"
        ? _c(
            "div",
            {
              staticClass:
                " col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 bookCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Books")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(book) {
                return _c("div", { key: book.id }, [
                  book.prodType === "Books"
                    ? _c("div", [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-2 col-md-2 col-sm-2 col-xs-4 books"
                          },
                          [
                            _c("div", { staticClass: "bookContent" }, [
                              _c("div", { staticClass: "bookImage" }, [
                                _c("img", {
                                  attrs: { src: book.coverImage, alt: "" }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "aboutBook" }, [
                                _c("p", { staticClass: "author" }, [
                                  _vm._v(_vm._s(book.books.author))
                                ])
                              ])
                            ])
                          ]
                        )
                      ])
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.name === "COURSES"
        ? _c(
            "div",
            {
              staticClass:
                " col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 bookCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Courses")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(course) {
                return _c("div", [
                  course.prodType === "Courses"
                    ? _c(
                        "div",
                        [
                          _c(
                            "router-link",
                            {
                              attrs: {
                                to: {
                                  name: "CourseFullPage",
                                  params: {
                                    id: course.courses.product_id,
                                    name: course.courses.title
                                      .replace(/ /g, "-")
                                      .toLowerCase()
                                  }
                                }
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "col-lg-3 col-md-3 col-sm-3 col-xs-4 books"
                                },
                                [
                                  _c("div", { staticClass: "bookContent" }, [
                                    _c("div", { staticClass: "bookImage" }, [
                                      course.prodCategoryType === "BP" ||
                                      course.prodCategoryType === "EP" ||
                                      course.prodCategoryType === "VP"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "ribbon ribbon-top-right"
                                            },
                                            [_c("span", [_vm._v("Paid")])]
                                          )
                                        : _c(
                                            "div",
                                            {
                                              staticClass:
                                                "ribbon ribbon-free ribbon-top-right"
                                            },
                                            [_c("span", [_vm._v("free")])]
                                          ),
                                      _vm._v(" "),
                                      _c("img", {
                                        attrs: {
                                          src: course.coverImage,
                                          alt: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "courseDiv" }, [
                                      _c("p", { staticClass: "courseTit" }, [
                                        _vm._v(_vm._s(course.courses.title))
                                      ]),
                                      _vm._v(" "),
                                      _c("p", { staticClass: "courseOver" }, [
                                        _vm._v(_vm._s(course.courses.overview))
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "courseLevel" },
                                        [
                                          _vm._v(
                                            "\n                                    " +
                                              _vm._s(course.vendor.storeName) +
                                              "\n                                     "
                                          ),
                                          _c("br"),
                                          _vm._v(
                                            "\n\t\t\t\t\t\t\t\t\tLevel : " +
                                              _vm._s(course.courses.level) +
                                              "\n\n\n\n\t\t\t\t\t\t\t\t"
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "row coursePrice" },
                                        [
                                          course.hardCopyPrice
                                            ? _c(
                                                "div",
                                                { staticClass: " price" },
                                                [
                                                  _vm._v(
                                                    "\n\t\t\t\t\t\t\t\t\t₦" +
                                                      _vm._s(
                                                        course.hardCopyPrice
                                                      ) +
                                                      ".00\n                                    "
                                                  ),
                                                  _c(
                                                    "div",
                                                    { staticClass: "cover" },
                                                    [_vm._v("Hard Cover")]
                                                  )
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          course.videoPrice
                                            ? _c(
                                                "div",
                                                { staticClass: " price" },
                                                [
                                                  _vm._v(
                                                    "\n\t\t\t\t\t\t\t\t\t₦" +
                                                      _vm._s(
                                                        course.videoPrice
                                                      ) +
                                                      ".00\n\t\t\t\t\t\t\t\t\t"
                                                  ),
                                                  _c(
                                                    "div",
                                                    { staticClass: "cover" },
                                                    [_vm._v("Video Copy")]
                                                  )
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          course.softCopyPrice
                                            ? _c(
                                                "div",
                                                { staticClass: " price" },
                                                [
                                                  _vm._v(
                                                    "\n                                   \n\t\t\t\t\t\t\t\t\t₦" +
                                                      _vm._s(
                                                        course.softCopyPrice
                                                      ) +
                                                      ".00\n                                     "
                                                  ),
                                                  _c(
                                                    "div",
                                                    { staticClass: "cover" },
                                                    [_vm._v("Digital Cover")]
                                                  )
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          course.readOnlinePrice
                                            ? _c(
                                                "div",
                                                { staticClass: " price" },
                                                [
                                                  _vm._v(
                                                    "\n\t\t\t\t\t\t\t\t\t₦" +
                                                      _vm._s(
                                                        course.readOnlinePrice
                                                      ) +
                                                      ".00\n\t\t\t\t\t\t\t\t\t"
                                                  ),
                                                  _c(
                                                    "div",
                                                    { staticClass: "cover" },
                                                    [_vm._v("Read Online")]
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      )
                                    ])
                                  ])
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.name === "ARTICLES"
        ? _c(
            "div",
            {
              staticClass:
                "col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 articleCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Articles")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(article) {
                return _c("div", { key: article.id }, [
                  article.prodType === "Articles"
                    ? _c(
                        "div",
                        [
                          _c(
                            "router-link",
                            {
                              attrs: {
                                to: {
                                  name: "ArticleSinglePage",
                                  params: {
                                    id: article.articles.product_id,
                                    name: article.articles.title
                                      .replace(/ /g, "-")
                                      .toLowerCase()
                                  }
                                }
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "col-lg-4 col-md-4 col-sm-4 col-xs-12 article"
                                },
                                [
                                  _c("h5", [
                                    _vm._v(_vm._s(article.articles.title))
                                  ]),
                                  _vm._v(" "),
                                  _c("p", {
                                    staticClass: "articleDesc",
                                    domProps: {
                                      innerHTML: _vm._s(
                                        article.articles.description
                                      )
                                    }
                                  })
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.name === "MARKET RESEARCH"
        ? _c(
            "div",
            {
              staticClass:
                "col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 mresearchCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Market Research")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(research) {
                return _c("div", { key: research.id }, [
                  research.prodType === "Market Research"
                    ? _c("div", [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 mresearch mb-4 desktop"
                          },
                          [
                            _c("div", { staticClass: "mrContent" }, [
                              _c(
                                "div",
                                { staticClass: "mrImage" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ProductPaperDetail",
                                          params: {
                                            id:
                                              research.marketResearch
                                                .product_id,
                                            name: research.marketResearch.title
                                              .replace(/ /g, "-")
                                              .toLowerCase(),
                                            type: "subscribe"
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("img", {
                                        attrs: { src: research.coverImage }
                                      })
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "aboutMr" }, [
                                _c("div", [
                                  _c("div", { staticClass: "mrHeader" }, [
                                    _vm._v(
                                      " " +
                                        _vm._s(research.marketResearch.title)
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("small", [
                                    _vm._v(
                                      _vm._s(research.marketResearch.created_at)
                                    )
                                  ]),
                                  _vm._v(" "),
                                  research.marketResearch.executiveSummary
                                    ? _c("div", [
                                        _vm._v(
                                          _vm._s(
                                            research.marketResearch
                                              .executiveSummary
                                          )
                                        )
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  research.marketResearch.journalTitle
                                    ? _c("div", [
                                        _c("small", [
                                          _vm._v(
                                            _vm._s(
                                              research.marketResearch
                                                .journalTitle
                                            )
                                          )
                                        ])
                                      ])
                                    : _c("div", { staticClass: "mrText" }, [
                                        _vm._v(
                                          _vm._s(
                                            research.marketResearch.excerpt
                                          )
                                        )
                                      ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "mrButton" })
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 mresearch mb-4 mobile"
                          },
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "ProductPaperDetail",
                                    params: {
                                      id: research.marketResearch.product_id,
                                      name: research.marketResearch.title
                                        .replace(/ /g, "-")
                                        .toLowerCase(),
                                      type: "subscribe"
                                    }
                                  }
                                }
                              },
                              [
                                _c("div", { staticClass: "mrContent" }, [
                                  _c("div", { staticClass: "mrImage" }, [
                                    _c("img", {
                                      attrs: { src: research.coverImage }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "aboutMr" }, [
                                    _c("div", [
                                      _c("div", { staticClass: "mrHeader" }, [
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              research.marketResearch.title
                                            )
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("small", [
                                        _vm._v(
                                          _vm._s(
                                            research.marketResearch.created_at
                                          )
                                        )
                                      ]),
                                      _vm._v(" "),
                                      research.marketResearch.executiveSummary
                                        ? _c("div", [
                                            _vm._v(
                                              _vm._s(
                                                research.marketResearch
                                                  .executiveSummary
                                              )
                                            )
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      research.marketResearch.journalTitle
                                        ? _c("div", [
                                            _c("small", [
                                              _vm._v(
                                                _vm._s(
                                                  research.marketResearch
                                                    .journalTitle
                                                )
                                              )
                                            ])
                                          ])
                                        : _c("div", { staticClass: "mrText" }, [
                                            _vm._v(
                                              _vm._s(
                                                research.marketResearch.excerpt
                                              )
                                            )
                                          ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mrButton" }, [
                                      _c("i", {
                                        staticClass: "fas fa-angle-right"
                                      })
                                    ])
                                  ])
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.name === "VIDEOS"
        ? _c(
            "div",
            {
              staticClass:
                "col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 videoCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Videos")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(video) {
                return _c("div", { key: video.id }, [
                  video.prodType === "Videos"
                    ? _c("div", [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 video desktop"
                          },
                          [
                            _c("div", { staticClass: "videoContent" }, [
                              _c(
                                "div",
                                { staticClass: "videoView" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "SingleVideoPage",
                                          params: {
                                            id: video.webinar.product_id
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "playCont" }, [
                                        _c("i", {
                                          staticClass: "fa fa-play playIcon",
                                          attrs: { "aria-hidden": "true" }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("img", {
                                        attrs: {
                                          src: video.coverImage,
                                          alt: ""
                                        }
                                      })
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "aboutVideo" }, [
                                _c("div", [
                                  _c("div", { staticClass: "mrHeader" }, [
                                    _vm._v(_vm._s(video.webinar.title))
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "vidText" }, [
                                    _vm._v(
                                      " " + _vm._s(video.webinar.description)
                                    )
                                  ])
                                ])
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 video mobile"
                          },
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "SingleVideoPage",
                                    params: { id: video.webinar.product_id }
                                  }
                                }
                              },
                              [
                                _c("div", { staticClass: "videoContent" }, [
                                  _c("div", { staticClass: "videoView" }, [
                                    _c("div", { staticClass: "playCont" }, [
                                      _c("i", {
                                        staticClass: "fa fa-play playIcon",
                                        attrs: { "aria-hidden": "true" }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("img", {
                                      attrs: { src: video.coverImage, alt: "" }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "aboutVideo" }, [
                                    _c("div", [
                                      _c("div", { staticClass: "mrHeader" }, [
                                        _vm._v(_vm._s(video.webinar.title))
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "vidText" }, [
                                        _vm._v(
                                          " " +
                                            _vm._s(video.webinar.description)
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mrButton" }, [
                                      _c("i", {
                                        staticClass: "fas fa-angle-right"
                                      })
                                    ])
                                  ])
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.name === "PODCAST"
        ? _c(
            "div",
            {
              staticClass:
                "col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-4 videoCover"
            },
            [
              _c("h2", { staticClass: "mb-4" }, [_vm._v("Podcast")]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._l(_vm.industries, function(podcast) {
                return _c("div", { key: podcast.id }, [
                  podcast.prodType === "Podcast"
                    ? _c("div", [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 video desktop"
                          },
                          [
                            _c("div", { staticClass: "videoContent" }, [
                              _c(
                                "div",
                                { staticClass: "videoView" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "SinglePodcastPage",
                                          params: {
                                            id: podcast.webinar.product_id
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "playCont" }, [
                                        _c("i", {
                                          staticClass:
                                            "fa fa-microphone playIcon",
                                          attrs: { "aria-hidden": "true" }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("img", {
                                        attrs: {
                                          src: podcast.coverImage,
                                          alt: ""
                                        }
                                      })
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "aboutVideo" }, [
                                _c("div", [
                                  _c("div", { staticClass: "mrHeader" }, [
                                    _vm._v(_vm._s(podcast.webinar.title))
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "vidText" }, [
                                    _vm._v(
                                      " " + _vm._s(podcast.webinar.description)
                                    )
                                  ])
                                ])
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-lg-6 col-md-6 col-sm-6 col-xs-12 video mobile"
                          },
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "SinglePodcastPage",
                                    params: { id: podcast.webinar.product_id }
                                  }
                                }
                              },
                              [
                                _c("div", { staticClass: "videoContent" }, [
                                  _c("div", { staticClass: "videoView" }, [
                                    _c("div", { staticClass: "playCont" }, [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-microphone playIcon",
                                        attrs: { "aria-hidden": "true" }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("img", {
                                      attrs: {
                                        src: podcast.coverImage,
                                        alt: ""
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "aboutVideo" }, [
                                    _c("div", [
                                      _c("div", { staticClass: "mrHeader" }, [
                                        _vm._v(_vm._s(podcast.webinar.title))
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "vidText" }, [
                                        _vm._v(
                                          " " +
                                            _vm._s(podcast.webinar.description)
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mrButton" }, [
                                      _c("i", {
                                        staticClass: "fas fa-angle-right"
                                      })
                                    ])
                                  ])
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    : _vm._e()
                ])
              })
            ],
            2
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-905242ca", module.exports)
  }
}

/***/ }),

/***/ 611:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1654)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1656)
/* template */
var __vue_template__ = __webpack_require__(1657)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-905242ca"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/specificIndustryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-905242ca", Component.options)
  } else {
    hotAPI.reload("data-v-905242ca", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});