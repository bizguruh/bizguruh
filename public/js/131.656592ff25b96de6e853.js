webpackJsonp([131],{

/***/ 1684:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1685);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("89eb8152", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323664cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resetPasswordComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-323664cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resetPasswordComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1685:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container-fluid[data-v-323664cc]{\n    height:100vh;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n     background: #d1e8fd;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.firstStage[data-v-323664cc],.secondStage[data-v-323664cc]{\n    width:60%;\n}\n.btn-primary[data-v-323664cc]{\n    background-color:#fff !important;\n    color:#333;\n    border-color:  white;\n    text-transform:capitalize;\n    padding:5px 30px 6px\n}\n.form-control[data-v-323664cc] {\n  /* border-top: unset;\n  border-left: unset;\n  border-right: unset; */\n  /* background-color:lightsteelblue; */\n  border-radius: 3px;\n}\n.form-control[data-v-323664cc]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-323664cc]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-323664cc]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-323664cc]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-323664cc]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-323664cc]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'reset-pasword-component',
    data: function data() {
        return {
            firstStage: true,
            secondStage: false,
            email: '',
            newPassword: '',
            confirmPassword: '',
            show: false

        };
    },
    beforeRouteEnter: function beforeRouteEnter(to, from, next) {
        if (to.query.id) {
            next(function (vm) {
                vm.secondStage = true;
                vm.firstStage = false;
            });
        } else {
            next();
        }
    },
    mounted: function mounted() {},

    methods: {
        sendLink: function sendLink() {
            var _this = this;

            this.show = true;
            axios.get('/api/sendResetLink/' + this.email).then(function (response) {

                if (_this.email !== '') {

                    if (response.status === 200) {
                        _this.show = false;
                        _this.$toasted.success('Password reset link sent, check your email');
                    }
                } else {
                    _this.$toasted.error('Incorrect email');
                    _this.show = false;
                }
            });
        },
        changePassword: function changePassword() {
            var _this2 = this;

            this.show = true;
            if (this.newPassword.length >= 7) {
                if (this.newPassword === this.confirmPassword) {

                    var data = {
                        id: this.$route.query.id,
                        newPassword: this.newPassword
                    };
                    axios.post('/api/changePassword', data).then(function (response) {
                        if (response.status === 200) {
                            if (response.data.message === 'success') {
                                _this2.show = false;
                                _this2.$toasted.success('Password reset successful');
                                _this2.$router.push({ name: 'auth', params: { name: 'login' } });
                            }
                            if (response.data.message === 'vendor password changed') {
                                _this2.show = false;
                                _this2.$toasted.success('Password reset successful');
                                _this2.$router.push('/vendor/auth');
                            }
                        }
                        if (response.data === 'error') {
                            _this2.show = false;
                            _this2.$toasted.error('Try using a new password');
                        }
                    });
                } else {
                    this.$toasted.error('Password do not match');
                    this.show = false;
                }
            } else {
                this.$toasted.error('Password too short');
                this.show = false;
            }
        }
    }

});

/***/ }),

/***/ 1687:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm.firstStage
      ? _c("div", { staticClass: "firstStage" }, [
          _c(
            "h5",
            { staticClass: "mb-3", staticStyle: { "text-align": "center" } },
            [_vm._v("Reset Password")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.email,
                  expression: "email"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "email",
                name: "",
                id: "email",
                "aria-describedby": "helpId",
                placeholder: " Enter email address"
              },
              domProps: { value: _vm.email },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.email = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "form-group",
              staticStyle: { "text-align": "center" }
            },
            [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary shadow-sm rounded",
                  attrs: { type: "button" },
                  on: { click: _vm.sendLink }
                },
                [
                  _vm._v("Submit   "),
                  _vm.show
                    ? _c("span", {
                        staticClass: "spinner-border spinner-border-sm",
                        attrs: { role: "status" }
                      })
                    : _vm._e()
                ]
              )
            ]
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.secondStage
      ? _c("div", { staticClass: "secondStage" }, [
          _c(
            "h5",
            { staticClass: "mb-3", staticStyle: { "text-align": "center" } },
            [_vm._v("Reset Password")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "newPassword" } }, [
              _vm._v("New Password")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.newPassword,
                  expression: "newPassword"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "password",
                name: "",
                id: "newPassword",
                "aria-describedby": "helpId",
                placeholder: "Enter new password",
                minlength: "6"
              },
              domProps: { value: _vm.newPassword },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.newPassword = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "confirmPassword" } }, [
              _vm._v("Confirm Password")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.confirmPassword,
                  expression: "confirmPassword"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "password",
                name: "",
                id: "confirmPassword",
                "aria-describedby": "helpId",
                placeholder: "confirm password",
                minlength: "6"
              },
              domProps: { value: _vm.confirmPassword },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.confirmPassword = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "form-group",
              staticStyle: { "text-align": "center" }
            },
            [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary shadow-sm rounded",
                  attrs: { type: "button" },
                  on: { click: _vm.changePassword }
                },
                [
                  _vm._v("Reset   "),
                  _vm.show
                    ? _c("span", {
                        staticClass: "spinner-border spinner-border-sm",
                        attrs: { role: "status" }
                      })
                    : _vm._e()
                ]
              )
            ]
          )
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-323664cc", module.exports)
  }
}

/***/ }),

/***/ 619:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1684)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1686)
/* template */
var __vue_template__ = __webpack_require__(1687)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-323664cc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/resetPasswordComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-323664cc", Component.options)
  } else {
    hotAPI.reload("data-v-323664cc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});