webpackJsonp([60],{

/***/ 1372:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1373);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("1a849890", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-13296268\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistquestionComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-13296268\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistquestionComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1373:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-13296268] {\n  padding: 80px 15px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  min-height: 100vh;\n  width: 100%;\n}\n.content-wrapper[data-v-13296268] {\n  height: 100vh;\n}\n.main-page[data-v-13296268] {\n  padding: 40px 20px;\n  max-height: 100vh;\n  height: 100vh;\n  padding: 20px 30px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  overflow: scroll;\n}\n.main[data-v-13296268] {\n  background: white;\n  padding: 20px 30px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  width: 80%;\n  min-height: 500px;\n}\nul[data-v-13296268],\nol[data-v-13296268] {\n  list-style: none;\n}\n.form-check-input[data-v-13296268] {\n  position: absolute;\n  margin-top: 0.3rem;\n  margin-left: -1.25rem;\n}\n.btn-primary[data-v-13296268] {\n  background-color: rgb(164, 194, 220) !important;\n  border: none !important;\n  text-transform: capitalize;\n}\n.sub[data-v-13296268]{\n  margin: 20px;\n  background-color: rgba(0, 0, 0, 0.6) !important;\n}\n.form-control[data-v-13296268] {\n  background: rgba(255, 255, 255, 0.5);\n}\n.questionGroup[data-v-13296268] {\n  padding: 10px;\n  border-bottom: 1px solid #ccc;\n  margin-bottom: 15px;\n  background:white;\n}\n.submit[data-v-13296268] {\n  width: 100%;\n  text-align: right;\n}\n.fa-1x[data-v-13296268] {\n  font-size: 0.7em;\n}\n", ""]);

// exports


/***/ }),

/***/ 1374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "checklistQuestions",
  data: function data() {
    return {
      title: "",
      premise: '',
      type: "",
      catalog: '',

      models: [{
        category: "",
        questions: [{
          title: "",
          type: "tf",
          option: "",
          options: [],
          answers: [],
          answer: ""

        }]
      }]
    };
  },

  components: {
    "app-editor": __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]

  },

  methods: {
    addOption: function addOption(options, option) {
      options.push(option);
      option = " ";

      this.$toasted.success("Added");
    },
    removeOption: function removeOption(options, index) {
      options.splice(index);
      this.$toasted.success("Removed");
    },
    create: function create(i) {

      this.models[i].questions.push({
        title: "",
        type: "tf",
        option: "",
        options: [],
        answers: [],
        answer: ""

      });
      this.$toasted.success("Question added");
    },
    addCategory: function addCategory() {
      this.models.push({
        category: "",
        questions: [{
          title: "",
          type: "tf",
          option: "",
          options: [],
          answers: [],
          answer: ""

        }]
      });
    },
    submit: function submit() {
      var _this = this;

      var data = {
        title: this.title.toLowerCase(),
        catalog: this.catalog,
        models: this.models,
        premise: this.premise,
        name: this.type
      };
      axios.post("/api/questions", JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          // if (response.data === 'saved') {
          _this.$router.push({ name: "checklistPage" });
          _this.$toasted.success("Question saved");
          // }
        }
      });
    }
  }
});

/***/ }),

/***/ 1375:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content_wrapper" }, [
    _c(
      "form",
      {
        staticClass: "main-page",
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.submit($event)
          }
        }
      },
      [
        _c("div", { staticClass: "form-group" }, [
          _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
            _vm._v("Questionaire Catalog")
          ]),
          _vm._v(" "),
          _c(
            "select",
            {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.catalog,
                  expression: "catalog"
                }
              ],
              staticClass: "form-control",
              attrs: { name: "", id: "" },
              on: {
                change: function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.catalog = $event.target.multiple
                    ? $$selectedVal
                    : $$selectedVal[0]
                }
              }
            },
            [
              _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
              _vm._v(" "),
              _c("option", { attrs: { value: "qg" } }, [
                _vm._v("Questionaire/Guides")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "diagnostics" } }, [
                _vm._v("Business Diagnostics")
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
            _vm._v("Questionaire Title")
          ]),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.title,
                expression: "title"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              name: "",
              id: "title",
              "aria-describedby": "helpId",
              placeholder: "",
              minlength: "10"
            },
            domProps: { value: _vm.title },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.title = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _vm.catalog === "diagnostics"
          ? _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                  _vm._v("Premise ")
                ]),
                _vm._v(" "),
                _c("app-editor", {
                  staticClass: "form-control",
                  attrs: {
                    apiKey: "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                    init: { plugins: "wordcount, lists, advlist" }
                  },
                  model: {
                    value: _vm.premise,
                    callback: function($$v) {
                      _vm.premise = $$v
                    },
                    expression: "premise"
                  }
                })
              ],
              1
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.catalog === "diagnostics"
          ? _c("div", { staticClass: "form-group" }, [
              _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                _vm._v("Questionaire Type")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "", id: "" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.type = $event.target.multiple
                        ? $$selectedVal
                        : $$selectedVal[0]
                    }
                  }
                },
                [
                  _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "brain-gym" } }, [
                    _vm._v("Brain-gym")
                  ])
                ]
              )
            ])
          : _c("div", { staticClass: "form-group" }, [
              _c("label", { staticClass: "mb-2", attrs: { for: "title" } }, [
                _vm._v("Questionaire Type")
              ]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.type,
                      expression: "type"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { name: "", id: "" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.type = $event.target.multiple
                        ? $$selectedVal
                        : $$selectedVal[0]
                    }
                  }
                },
                [
                  _c("option", { attrs: { value: "" } }, [_vm._v("Select")]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "qa" } }, [
                    _vm._v("Checklist")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "template" } }, [
                    _vm._v("Business Template")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "bg" } }, [
                    _vm._v("Business Guide")
                  ])
                ]
              )
            ]),
        _vm._v(" "),
        _vm._l(_vm.models, function(model, index) {
          return _c(
            "div",
            { key: index, staticClass: "questionGroup" },
            [
              _c("div", { staticClass: "form-group" }, [
                _c("h5", { staticClass: "mb-2", attrs: { for: "title" } }, [
                  _vm._v("Question Category " + _vm._s(index + 1))
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: model.category,
                      expression: "model.category"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    name: "",
                    id: "title",
                    "aria-describedby": "helpId",
                    placeholder: "Enter category"
                  },
                  domProps: { value: model.category },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(model, "category", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _vm._l(model.questions, function(question, index) {
                return _c("div", { key: index }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("h5", { staticClass: "mb-2", attrs: { for: "title" } }, [
                      _vm._v("Question " + _vm._s(index + 1))
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: question.title,
                          expression: "question.title"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        name: "",
                        id: "title",
                        "aria-describedby": "helpId",
                        placeholder: "Enter question"
                      },
                      domProps: { value: question.title },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(question, "title", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { staticClass: "mb-2", attrs: { for: "" } }, [
                      _vm._v("Type")
                    ]),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: question.type,
                            expression: "question.type"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { name: "", id: "" },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              question,
                              "type",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "tf" } }, [
                          _vm._v("True/False")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "mc" } }, [
                          _vm._v("Multiple Optons")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "radio" } }, [
                          _vm._v("Single Options")
                        ]),
                        _vm._v(" "),
                        _c("option", { attrs: { value: "text" } }, [
                          _vm._v("text")
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  question.type === "mc" || question.type === "radio"
                    ? _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c(
                            "label",
                            { staticClass: "mb-2", attrs: { for: "option" } },
                            [_vm._v("Options")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "d-flex" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: question.option,
                                  expression: "question.option"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                name: "",
                                id: "option",
                                "aria-describedby": "helpId",
                                placeholder: ""
                              },
                              domProps: { value: question.option },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    question,
                                    "option",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.addOption(
                                      question.options,
                                      question.option
                                    )
                                  }
                                }
                              },
                              [_vm._v("Add")]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(question.options, function(quest, index) {
                            return _c("div", { key: index }, [
                              _c("ul", [
                                _c("li", [
                                  _vm._v(
                                    "\n                  " +
                                      _vm._s(quest) +
                                      "\n                  "
                                  ),
                                  _c("i", {
                                    staticClass:
                                      "fas fa-trash fa-inverse fa-1x ml-2",
                                    staticStyle: { color: "red" },
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeOption(
                                          question.options,
                                          index
                                        )
                                      }
                                    }
                                  })
                                ])
                              ])
                            ])
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c("div")
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.type === "qa" || _vm.type === "brain-gym"
                    ? _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c(
                            "label",
                            { staticClass: "mb-2", attrs: { for: "title" } },
                            [_vm._v("Answer(s)")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "d-flex" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: question.answer,
                                  expression: "question.answer"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                name: "",
                                id: "title",
                                "aria-describedby": "helpId",
                                placeholder: ""
                              },
                              domProps: { value: question.answer },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    question,
                                    "answer",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.addOption(
                                      question.answers,
                                      question.answer
                                    )
                                  }
                                }
                              },
                              [_vm._v("Add")]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(question.answers, function(quest, index) {
                            return _c("div", { key: index }, [
                              _c("ul", [
                                _c("li", [
                                  _vm._v(
                                    "\n                  " +
                                      _vm._s(quest) +
                                      "\n                  "
                                  ),
                                  _c("i", {
                                    staticClass:
                                      "fas fa-trash fa-inverse fa-1x ml-2",
                                    staticStyle: { color: "red" },
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.removeOption(
                                          question.answers,
                                          index
                                        )
                                      }
                                    }
                                  })
                                ])
                              ])
                            ])
                          }),
                          _vm._v(" "),
                          _c("br")
                        ],
                        2
                      )
                    : _vm._e()
                ])
              }),
              _vm._v(" "),
              _c("div", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary sub",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.create(index)
                      }
                    }
                  },
                  [_vm._v("Add question")]
                )
              ])
            ],
            2
          )
        }),
        _vm._v(" "),
        _c("div", [
          _c(
            "button",
            {
              staticClass: "btn btn-primary sub",
              attrs: { type: "button" },
              on: { click: _vm.addCategory }
            },
            [_vm._v("Add Category")]
          )
        ]),
        _vm._v(" "),
        _vm._m(0)
      ],
      2
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submit" }, [
      _c(
        "button",
        { staticClass: "btn btn-primary ", attrs: { type: "submit" } },
        [_vm._v("Submit question(s)")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-13296268", module.exports)
  }
}

/***/ }),

/***/ 548:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1372)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1374)
/* template */
var __vue_template__ = __webpack_require__(1375)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-13296268"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/checklistquestionComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-13296268", Component.options)
  } else {
    hotAPI.reload("data-v-13296268", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ })

});