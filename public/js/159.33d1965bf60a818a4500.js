webpackJsonp([159],{

/***/ 1245:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1246);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("a02c41c8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-914464c6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-914464c6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1246:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-914464c6] {\n    height: 100vh;\n}\n.container[data-v-914464c6] {\n    width: 100%;\n}\nth[data-v-914464c6] {\n    background-color: #f3f3f3;\n    color: #000000;\n    font-weight: bold;\n}\nth[data-v-914464c6], td[data-v-914464c6] {\n    padding: 30px !important;\n}\ntd[data-v-914464c6] {\n    color: #aaaaaa;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-vendor-component",
    data: function data() {
        return {
            token: '',
            vendors: [],
            overallCheck: false
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.token = admin.access_token;

        if (admin != null) {
            axios.get('/api/admin/all/vendors', { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
                console.log(response);
                _this.vendors = response.data.data;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        overallCheckBox: function overallCheckBox() {
            var _this2 = this;

            this.vendors.forEach(function (item) {
                if (!('itemClick' in item)) {
                    _this2.$set(item, 'itemClick', true);
                } else if (_this2.overallCheck) {
                    _this2.$set(item, 'itemClick', true);
                } else {
                    _this2.$set(item, 'itemClick', false);
                }
            });
        },

        // admin/delete/products
        deleteItem: function deleteItem() {
            var _this3 = this;

            var deletedArray = [];
            this.vendors.forEach(function (item) {
                if (item.itemClick) {
                    console.log(item);
                    deletedArray.push(item.id);
                }
            });
            if (deletedArray.length === 0) {
                this.$toasted.error('You must select at least one product to delete');
            } else {
                console.log(deletedArray);
                axios.post('/api/admin/delete/products', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        console.log(response);
                        for (var i = 0; i < deletedArray.length; i++) {
                            console.log(deletedArray[i]);
                            _this3.showData.splice(i, 1);
                        }
                        _this3.$toasted.success('All selected items deleted');
                    } else {
                        _this3.$toasted.error('Unable to delete');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }
});

/***/ }),

/***/ 1248:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-4 deleteProductBtn" }, [
          _c(
            "button",
            { staticClass: "btn btn-danger", on: { click: _vm.deleteItem } },
            [_vm._v("Delete")]
          )
        ])
      ]),
      _vm._v(" "),
      _c("table", [
        _c("thead", [
          _c("tr", [
            _c("th", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.overallCheck,
                    expression: "overallCheck"
                  }
                ],
                attrs: { type: "checkbox" },
                domProps: {
                  checked: Array.isArray(_vm.overallCheck)
                    ? _vm._i(_vm.overallCheck, null) > -1
                    : _vm.overallCheck
                },
                on: {
                  change: [
                    function($event) {
                      var $$a = _vm.overallCheck,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && (_vm.overallCheck = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.overallCheck = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.overallCheck = $$c
                      }
                    },
                    _vm.overallCheckBox
                  ]
                }
              })
            ]),
            _vm._v(" "),
            _c("th", [_vm._v("Identification")]),
            _vm._v(" "),
            _c("th", [_vm._v("Details")]),
            _vm._v(" "),
            _c("th", [_vm._v("Status")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.vendors, function(vendor, index) {
            return _c("tr", [
              _c("td", [
                _c("input", {
                  attrs: { type: "checkbox" },
                  domProps: { checked: vendor.itemClick },
                  on: {
                    change: function($event) {
                      vendor.itemClick = !vendor.itemClick
                      _vm.overallCheck = false
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(vendor.storeName))]),
              _vm._v(" "),
              vendor.verified === 1
                ? _c("td", [_vm._v("Active")])
                : _c("td", [_vm._v("Pending")]),
              _vm._v(" "),
              _c(
                "td",
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "adminVendorDetail",
                          params: { id: vendor.id }
                        }
                      }
                    },
                    [_c("i", { staticClass: "fa fa-arrow-right" })]
                  )
                ],
                1
              )
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-914464c6", module.exports)
  }
}

/***/ }),

/***/ 521:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1245)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1247)
/* template */
var __vue_template__ = __webpack_require__(1248)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-914464c6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-914464c6", Component.options)
  } else {
    hotAPI.reload("data-v-914464c6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});