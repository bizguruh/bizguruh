webpackJsonp([74],{

/***/ 1688:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1689);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3de88467", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cd90e5a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bapComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cd90e5a8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bapComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1689:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bapPage[data-v-cd90e5a8] {\n  width: 100%;\n  height: 100vh;\n  background-color: #f7f8fa;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.mobileNav[data-v-cd90e5a8]{\n  height: 6% !important;\n}\n.side[data-v-cd90e5a8] {\n  width: 20%;\n  height: 100%;\n}\n.page[data-v-cd90e5a8] {\n  width: 80%;\n  height: 100%;\n  overflow: scroll;\n  padding: 15px;\n  padding-bottom: 60px;\n  position: relative;\n}\n@media (max-width: 768px) {\n.side[data-v-cd90e5a8] {\n    position: fixed;\n    z-index: 999;\n    width: 100%;\n    bottom:0;\n}\n.page[data-v-cd90e5a8] {\n    width: 100%;\n    padding: 0\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sidebar__ = __webpack_require__(1691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__sidebar__);
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: false,
      currentSub: 0,
      showMenu: true
    };
  },

  components: {
    Sidebar: __WEBPACK_IMPORTED_MODULE_0__sidebar___default.a
  },
  watch: {
    $route: 'handleMobileMenu'
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.getUserSub();
    });
  },
  mounted: function mounted() {
    if (window.innerWidth < 768) {
      this.showMenu = false;
    }
  },

  methods: {
    handleMobileMenu: function handleMobileMenu() {
      if (window.innerWidth < 768) {
        this.showMenu = !this.showMenu;
      }
    },
    getUserSub: function getUserSub() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      if (user !== null) {
        axios.get("/api/user-sub/" + user.id).then(function (response) {
          if (response.status == 200) {
            _this.currentSub = Math.round(response.data);
            if (_this.currentSub < 3) {
              _this.$router.go(-1);
            }
            if (_this.currentSub > 2) {
              _this.show = true;
            }
          }
        });
      }
    }
  }
});

/***/ }),

/***/ 1691:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1692)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1694)
/* template */
var __vue_template__ = __webpack_require__(1695)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c0090b68"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/sidebar.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c0090b68", Component.options)
  } else {
    hotAPI.reload("data-v-c0090b68", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1692:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1693);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("d273e29c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c0090b68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sidebar.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c0090b68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sidebar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1693:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.side_bar[data-v-c0090b68]{\n    background:#a4c2db;\n    height: 100%;\n    position:relative\n}\nli[data-v-c0090b68]{\n    padding: 20px 15px;\n    border-bottom: 1px solid white;\n    font-size: 15px;\n    font-weight: bold;\n    color: hsl(207,46%,20%);\n    cursor: pointer;\n    border-right: 4px solid transparent;\n    display:-webkit-box;\n    display:-ms-flexbox;\n    display:flex;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n}\nli[data-v-c0090b68]:hover{\nbackground-color: hsl(207,46%,90%);\nborder-color: #333;\n}\n.router-link-exact-active [data-v-c0090b68],.router-link-active[data-v-c0090b68]{\n background-color: hsl(207,46%,90%);\n border-color: #333;\n}\n.profile_info[data-v-c0090b68]{\n    padding: 30px 15px 20px;\n    border-bottom: 1px solid white;\n}\n.img[data-v-c0090b68]{\n    width: 60px;\n    height: 60px;\n    overflow: hidden;\n    border-radius: 50%;\n    margin-bottom: 16px;\n}\n.img img[data-v-c0090b68]{\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n}\n.name[data-v-c0090b68]{\n    font-size: 16px;\n    text-align: left;\n    text-transform: capitalize;\n}\n.fa-times-circle[data-v-c0090b68]{\n    position:absolute;\n    top:20px;\n    right:20px;\n    display:none;\n}\n@media(max-width:768px){\n.fa-times-circle[data-v-c0090b68]{\n        display: block;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['showMenu'],
    data: function data() {
        return {

            user: {}
        };
    },
    mounted: function mounted() {
        this.user = JSON.parse(localStorage.getItem('authUser'));
    },

    methods: {
        handleMobileMenu: function handleMobileMenu() {
            this.$emit('handleMobileMenu');
        }
    }
});

/***/ }),

/***/ 1695:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "side_bar" }, [
    _vm.showMenu
      ? _c("i", {
          staticClass: "fas fa-times-circle fa-2x text-main",
          attrs: { "aria-hidden": "true" },
          on: { click: _vm.handleMobileMenu }
        })
      : _vm._e(),
    _vm._v(" "),
    _vm.showMenu
      ? _c("div", { staticClass: "profile_info" }, [
          _c("div", { staticClass: "img" }, [
            _vm.user.logo
              ? _c("img", { attrs: { src: _vm.user.logo, alt: "" } })
              : _c("i", {
                  staticClass: "fas fa-user-circle fa-4x",
                  attrs: { "aria-hidden": "true" }
                })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "name" }, [_vm._v(_vm._s(_vm.user.name))])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.showMenu
      ? _c("nav", [
          _c(
            "ul",
            [
              _c("router-link", { attrs: { to: "/entrepreneur/bap" } }, [
                _c("li", [
                  _c("img", {
                    staticClass: "mr-3",
                    attrs: { src: "/images/graph.svg", width: "20", alt: "" }
                  }),
                  _vm._v(" Dashboard")
                ])
              ]),
              _vm._v(" "),
              _c(
                "router-link",
                { attrs: { to: "/entrepreneur/bap/templates" } },
                [
                  _c("li", [
                    _c("img", {
                      staticClass: "mr-3",
                      attrs: { src: "/images/text.svg", width: "20", alt: "" }
                    }),
                    _vm._v("  Work-sheets")
                  ])
                ]
              ),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "/entrepreneur/bap/guides" } }, [
                _c("li", [
                  _c("img", {
                    staticClass: "mr-3",
                    attrs: { src: "/images/package.svg", width: "20", alt: "" }
                  }),
                  _vm._v("  Resources")
                ])
              ]),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "/entrepreneur/bap/review" } }, [
                _c("li", [
                  _c("img", {
                    staticClass: "mr-3",
                    attrs: { src: "/images/comment.svg", width: "20", alt: "" }
                  }),
                  _vm._v("  Review")
                ])
              ])
            ],
            1
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.showMenu
      ? _c(
          "div",
          {
            staticClass: "text-center p-2",
            on: { click: _vm.handleMobileMenu }
          },
          [_vm._m(0)]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", [
      _c("i", { staticClass: "fas fa-sort-amount-up" }),
      _vm._v("  Menu")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c0090b68", module.exports)
  }
}

/***/ }),

/***/ 1696:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.show
    ? _c("div", { staticClass: "bapPage" }, [
        _c(
          "div",
          { staticClass: "side", class: { mobileNav: !_vm.showMenu } },
          [
            _c("Sidebar", {
              attrs: { showMenu: _vm.showMenu },
              on: { handleMobileMenu: _vm.handleMobileMenu }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "page" },
          [_c("router-view", { attrs: { currentSub: _vm.currentSub } })],
          1
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-cd90e5a8", module.exports)
  }
}

/***/ }),

/***/ 620:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1688)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1690)
/* template */
var __vue_template__ = __webpack_require__(1696)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-cd90e5a8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/bapComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cd90e5a8", Component.options)
  } else {
    hotAPI.reload("data-v-cd90e5a8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});