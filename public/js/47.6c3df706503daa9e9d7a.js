webpackJsonp([47],{

/***/ 1625:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1626);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("27a07262", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-07e86a6f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userUpdateProfileComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-07e86a6f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userUpdateProfileComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1626:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.rule[data-v-07e86a6f] {\n  border-bottom: 2px solid #ccc;\n  width: 10%;\n  margin-bottom: 16px;\n}\n.mobile[data-v-07e86a6f] {\n  display: none;\n}\n.butt[data-v-07e86a6f] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\nlegend[data-v-07e86a6f] {\n  display: block !important;\n  width: 100%;\n  padding: 0;\n  margin-bottom: 20px;\n  font-size: 21px;\n  line-height: inherit;\n  color: #333;\n  border: 0;\n  border-bottom: 1px solid #e5e5e5;\n}\n.display-6[data-v-07e86a6f] {\n  font-size: 3.6rem;\n  font-weight: 700;\n  line-height: 1.2;\n}\n.main-page[data-v-07e86a6f] {\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 0px;\n}\n.form-control-file[data-v-07e86a6f] {\n  font-size: 14px;\n}\n.btn-block[data-v-07e86a6f] {\n  font-size: 14px;\n}\nlabel[data-v-07e86a6f] {\n  font-size: 14px;\n}\n.form[data-v-07e86a6f] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 100%;\n  background: #fff;\n  height: 100%;\n\n  padding: 40px;\n}\n.rightTab[data-v-07e86a6f],\n.leftTab[data-v-07e86a6f] {\n  width: 50%;\n  padding:0 40px;\n  background: #f7f8fa;\n  border-radius: 5px;\n}\n.form-control[data-v-07e86a6f] {\n  /* border-top: unset;\n  border-left: unset;\n  border-right: unset; */\n  /* background-color:lightsteelblue; */\n  border-radius: 5px;\n}\n.form-control[data-v-07e86a6f]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-07e86a6f]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-07e86a6f]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-07e86a6f]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-07e86a6f]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-07e86a6f]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.logo[data-v-07e86a6f] {\n  width: 100px;\n  height: auto;\n}\n.main-form[data-v-07e86a6f] {\n  font-size: 16px;\n  width: 80%;\n  padding: 20px;\n  background-color: #f7f8fa;\n  max-height: 700px;\n  overflow-y: scroll;\n}\n.image[data-v-07e86a6f] {\n  position: relative;\n  width: 60%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n}\n.overlay[data-v-07e86a6f] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  /* background:linear-gradient(90deg, #36d1dc 0%,#5b86e5 100% ); */\n  opacity: 0.5;\n  top: 0;\n  bottom: 0;\n}\n.overlay-button[data-v-07e86a6f] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nimg[data-v-07e86a6f] {\n  height: 100%;\n  width: 100%;\n}\n.input-group-text[data-v-07e86a6f] {\n  font-size: 13px;\n}\n.input-group[data-v-07e86a6f] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n  flex-wrap: wrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n@media only screen and (max-width: 768px) {\n.image[data-v-07e86a6f] {\n    display: none !important;\n}\n.form[data-v-07e86a6f] {\n    width: 100%;\n    display: block;\n    padding: 0;\n}\n.mobile[data-v-07e86a6f] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.main-form[data-v-07e86a6f] {\n    font-size: 16px;\n    width: 100%;\n    padding: 10px;\n    background-color: #f7f8fa;\n    max-height: 100vh;\n    overflow-y: scroll;\n}\n.butt[data-v-07e86a6f] {\n    margin-bottom: 40px;\n}\n.mob-form[data-v-07e86a6f] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.leftTab[data-v-07e86a6f],\n  .rightTab[data-v-07e86a6f] {\n    width: 100%;\n    padding: 10px;\n}\nlegend[data-v-07e86a6f] {\n    margin-top: 70px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1627:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__states__ = __webpack_require__(1628);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__phoneCodes__ = __webpack_require__(714);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__js_config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cloudinaryComponent__ = __webpack_require__(989);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cloudinaryComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__cloudinaryComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "update-user-profile",
  data: function data() {
    return {
      oldimage: '',
      industry: [],
      myStates: [],
      myDials: [],
      user: {
        bio: "",
        age: null,
        location: "",
        gender: "",
        first_name: "",
        last_name: "",
        phone_code: "+234",
        phone_no: null,
        business_type: "",
        logo: "",
        address: "",
        industry: ""
      },
      logoImage: "",
      userData: [],
      show: false,
      cloudinary: {
        uploadPreset: "knkccgjv",
        apiKey: "634813511968181",
        cloudName: "bizguruh-com"
      }
    };
  },

  components: {
    "cl-upload": __WEBPACK_IMPORTED_MODULE_3__cloudinaryComponent___default.a
  },
  mounted: function mounted() {
    this.myStates = Object(__WEBPACK_IMPORTED_MODULE_0__states__["a" /* states */])();
    this.myDials = Object(__WEBPACK_IMPORTED_MODULE_1__phoneCodes__["a" /* dialCodes */])();
    this.getUser();
    this.getIndustry();
  },

  methods: {
    getIndustry: function getIndustry() {
      var _this = this;

      axios.get("/api/all-industries").then(function (response) {
        _this.industry = response.data;
      });
    },
    cancel: function cancel() {
      var cancel = confirm("Are you sure you want to leave this page?");
      if (cancel) {
        this.$router.push("/explore");
      }
    },
    getUpload: function getUpload(value) {
      this.user.logo = value;
    },
    update: function update() {
      var _this2 = this;

      this.show = true;
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        this.user.id = user.id;

        axios.post("/api/update-profile", this.user).then(function (response) {
          if (response.status === 200) {
            _this2.$toasted.success("Update successful, login to continue");

            _this2.logout();
          }
        });
      } else {
        this.show = false;
        this.$toasted.error("Login to update profile");
      }
    },
    logout: function logout() {
      window.Engagespot = {}, q = function q(e) {
        return function () {
          (window.engageq = window.engageq || []).push({
            f: e,
            a: arguments
          });
        };
      }, f = ["captureEvent", "subscribe", "init", "showPrompt", "identifyUser", "clearUser"];
      for (k in f) {
        Engagespot[f[k]] = q(f[k]);
      }var s = document.createElement("script");
      s.type = "text/javascript", s.async = !0, s.src = "https://cdn.engagespot.co/EngagespotSDK.2.0.js";
      var x = document.getElementsByTagName("script")[0];
      x.parentNode.insertBefore(s, x);

      Engagespot.init("cywCppTlinzpmjVHxFcYoiuuKlnefc");
      Engagespot.clearUser();

      localStorage.removeItem("authUser");
      localStorage.removeItem("login_token");

      this.$router.push("auth/login");
    },
    getUser: function getUser() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_2__js_config__["b" /* getCustomerHeader */])() }).then(function (response) {
          if (response.status === 200) {
            _this3.userData = response.data;
            _this3.user.age = response.data.age;
            _this3.user.first_name = response.data.name;
            _this3.user.last_name = response.data.lastName;

            if (response.data.location) {
              _this3.user.location = response.data.location;
            }

            if (response.data.gender) {
              _this3.user.gender = response.data.gender;
            }

            _this3.user.bio = response.data.bio;
            _this3.user.business_type = response.data.business_type;
            _this3.user.business_name = response.data.business_name;
            _this3.user.phone_no = response.data.phoneNo;
            _this3.oldimage = response.data.logo;
            // this.logoImage = response.data.logo;
            _this3.user.address = response.data.address;
            _this3.user.industry = response.data.type;
          }
        });
      }
    }
  }
});

/***/ }),

/***/ 1628:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return states; });

var states = function states() {
  var state = ["Abia", "Adamawa", "Anambra", "Akwa Ibom", "Bauchi", "Bayelsa", "Benue", "Borno", "Cross River", "Delta", "Ebonyi", "Enugu", "Edo", "Ekiti", "FCT - Abuja", "Gombe", "Imo", "Jigawa", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi", "Kwara", "Lagos", "Nasarawa", "Niger", "Ogun", "Ondo", "Osun", "Oyo", "Plateau", "Rivers", "Sokoto", "Taraba", "Yobe", "Zamfara"];
  return state;
};

/***/ }),

/***/ 1629:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "form" }, [
      _c(
        "form",
        {
          staticClass: "main-form",
          attrs: { action: "" },
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.update($event)
            }
          }
        },
        [
          _c("legend", { staticClass: "text-center" }, [
            _vm._v("Update Profile")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "d-flex w-100 mob-form" }, [
            _c("div", { staticClass: "leftTab" }, [
              _c("h6", { staticClass: "mb-1 text-muted" }, [
                _vm._v("Personal Information")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "rule" }),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("First Name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.first_name,
                      expression: "user.first_name"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    "aria-describedby": "helpId",
                    placeholder: "Full name",
                    required: ""
                  },
                  domProps: { value: _vm.user.first_name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "first_name", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Location")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.location,
                        expression: "user.location"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { required: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "location",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "", disabled: "" } }, [
                      _vm._v("Select State")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.myStates, function(state, idx) {
                      return _c("option", { key: idx }, [_vm._v(_vm._s(state))])
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Age")]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group" }, [
                  _c("div", { staticClass: "input-group-prepend" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.user.age,
                          expression: "user.age"
                        }
                      ],
                      staticClass: "form-control input-group-text",
                      attrs: {
                        required: "",
                        type: "number",
                        "aria-describedby": "helpId",
                        placeholder: "Enter age"
                      },
                      domProps: { value: _vm.user.age },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.user, "age", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _vm._m(0)
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Phone number")]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group" }, [
                  _c(
                    "div",
                    {
                      staticClass: "input-group-prepend",
                      staticStyle: { width: "30%" }
                    },
                    [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.user.phone_code,
                              expression: "user.phone_code"
                            }
                          ],
                          staticClass: "input-group-text form-control",
                          attrs: { required: "" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.user,
                                "phone_code",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.myDials, function(dial, idx) {
                          return _c(
                            "option",
                            { key: idx, domProps: { value: dial.dial_code } },
                            [_vm._v(_vm._s(dial.dial_code + " - " + dial.name))]
                          )
                        }),
                        0
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "input-group-append",
                      staticStyle: { width: "70%" }
                    },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.user.phone_no,
                            expression: "user.phone_no"
                          }
                        ],
                        staticClass: "form-control input-group-text text-left",
                        attrs: {
                          required: "",
                          type: "number",
                          "aria-describedby": "helpId",
                          placeholder: "Enter phone no",
                          minlength: "6",
                          maxlength: "10"
                        },
                        domProps: { value: _vm.user.phone_no },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.user, "phone_no", $event.target.value)
                          }
                        }
                      })
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Gender")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.gender,
                        expression: "user.gender"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { required: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "gender",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "" } }, [
                      _vm._v("Select gender")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "male" } }, [
                      _vm._v("Male")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "female" } }, [
                      _vm._v("Female")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.address,
                      expression: "user.address"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    "aria-describedby": "helpId",
                    placeholder: "Enter your address"
                  },
                  domProps: { value: _vm.user.address },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "address", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "rightTab" }, [
              _c("h6", { staticClass: "mb-1 text-muted" }, [
                _vm._v("Business Information")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "rule" }),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Business name")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.business_name,
                      expression: "user.business_name"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    "aria-describedby": "helpId",
                    placeholder: "Business name",
                    required: ""
                  },
                  domProps: { value: _vm.user.business_name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "business_name", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("Industry")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.industry,
                        expression: "user.industry"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { required: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "industry",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "", disabled: "" } }, [
                      _vm._v("Select Industry")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.industry, function(item, idx) {
                      return _c(
                        "option",
                        { key: idx, domProps: { value: item.id } },
                        [_vm._v(_vm._s(item.name))]
                      )
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("Business Activity")
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.business_type,
                        expression: "user.business_type"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { required: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "business_type",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "", disabled: "" } }, [
                      _vm._v("Select Activity")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "retailer" } }, [
                      _vm._v("Retailer")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "manufacturer" } }, [
                      _vm._v("Manufacturer")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("What do you do?")
                ]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.bio,
                      expression: "user.bio"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    name: "",
                    id: "",
                    rows: "3",
                    placeholder: "Tell us what your business is about"
                  },
                  domProps: { value: _vm.user.bio },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "bio", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", { attrs: { for: "" } }, [
                    _vm._v("Business logo")
                  ]),
                  _vm._v(" "),
                  _c("cl-upload", {
                    attrs: { oldimage: _vm.oldimage },
                    on: { getUpload: _vm.getUpload }
                  })
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "butt" }, [
            _c(
              "button",
              {
                staticClass:
                  "btn-secondary elevated_btn elevated_btn_sm text-main mx-5",
                attrs: { type: "submit" },
                on: { click: _vm.cancel }
              },
              [_vm._v("Cancel")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass:
                  "btn-primary elevated_btn elevated_btn_sm text-white btn-compliment mx-5",
                attrs: { type: "submit" }
              },
              [_vm._v("Update")]
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        { staticClass: "form-control input-group-text bg-gray-light" },
        [_vm._v("years")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-07e86a6f", module.exports)
  }
}

/***/ }),

/***/ 604:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1625)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1627)
/* template */
var __vue_template__ = __webpack_require__(1629)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-07e86a6f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userUpdateProfileComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-07e86a6f", Component.options)
  } else {
    hotAPI.reload("data-v-07e86a6f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dialCodes; });
var dialCodes = function dialCodes() {
    var dials = [{ name: "Afghanistan", dial_code: "+93", code: "AF" }, { name: "Albania", dial_code: "+355", code: "AL" }, { name: "Algeria", dial_code: "+213", code: "DZ" }, { name: "AmericanSamoa", dial_code: "+1 684", code: "AS" }, { name: "Andorra", dial_code: "+376", code: "AD" }, { name: "Angola", dial_code: "+244", code: "AO" }, { name: "Anguilla", dial_code: "+1 264", code: "AI" }, { name: "Antarctica", dial_code: "+672", code: "AQ" }, { name: "Antigua and Barbuda", dial_code: "+1268", code: "AG" }, { name: "Argentina", dial_code: "+54", code: "AR" }, { name: "Armenia", dial_code: "+374", code: "AM" }, { name: "Aruba", dial_code: "+297", code: "AW" }, { name: "Australia", dial_code: "+61", code: "AU" }, { name: "Austria", dial_code: "+43", code: "AT" }, { name: "Azerbaijan", dial_code: "+994", code: "AZ" }, { name: "Bahamas", dial_code: "+1 242", code: "BS" }, { name: "Bahrain", dial_code: "+973", code: "BH" }, { name: "Bangladesh", dial_code: "+880", code: "BD" }, { name: "Barbados", dial_code: "+1 246", code: "BB" }, { name: "Belarus", dial_code: "+375", code: "BY" }, { name: "Belgium", dial_code: "+32", code: "BE" }, { name: "Belize", dial_code: "+501", code: "BZ" }, { name: "Benin", dial_code: "+229", code: "BJ" }, { name: "Bermuda", dial_code: "+1 441", code: "BM" }, { name: "Bhutan", dial_code: "+975", code: "BT" }, { name: "Bolivia, Plurinational State of", dial_code: "+591", code: "BO" }, { name: "Bosnia and Herzegovina", dial_code: "+387", code: "BA" }, { name: "Botswana", dial_code: "+267", code: "BW" }, { name: "Brazil", dial_code: "+55", code: "BR" }, { name: "British Indian Ocean Territory", dial_code: "+246", code: "IO" }, { name: "Brunei Darussalam", dial_code: "+673", code: "BN" }, { name: "Bulgaria", dial_code: "+359", code: "BG" }, { name: "Burkina Faso", dial_code: "+226", code: "BF" }, { name: "Burundi", dial_code: "+257", code: "BI" }, { name: "Cambodia", dial_code: "+855", code: "KH" }, { name: "Cameroon", dial_code: "+237", code: "CM" }, { name: "Canada", dial_code: "+1", code: "CA" }, { name: "Cape Verde", dial_code: "+238", code: "CV" }, { name: "Cayman Islands", dial_code: "+ 345", code: "KY" }, { name: "Central African Republic", dial_code: "+236", code: "CF" }, { name: "Chad", dial_code: "+235", code: "TD" }, { name: "Chile", dial_code: "+56", code: "CL" }, { name: "China", dial_code: "+86", code: "CN" }, { name: "Christmas Island", dial_code: "+61", code: "CX" }, { name: "Cocos (Keeling) Islands", dial_code: "+61", code: "CC" }, { name: "Colombia", dial_code: "+57", code: "CO" }, { name: "Comoros", dial_code: "+269", code: "KM" }, { name: "Congo", dial_code: "+242", code: "CG" }, {
        name: "Congo, The Democratic Republic of the",
        dial_code: "+243",
        code: "CD"
    }, { name: "Cook Islands", dial_code: "+682", code: "CK" }, { name: "Costa Rica", dial_code: "+506", code: "CR" }, { name: "Cote d'Ivoire", dial_code: "+225", code: "CI" }, { name: "Croatia", dial_code: "+385", code: "HR" }, { name: "Cuba", dial_code: "+53", code: "CU" }, { name: "Cyprus", dial_code: "+357", code: "CY" }, { name: "Czech Republic", dial_code: "+420", code: "CZ" }, { name: "Denmark", dial_code: "+45", code: "DK" }, { name: "Djibouti", dial_code: "+253", code: "DJ" }, { name: "Dominica", dial_code: "+1 767", code: "DM" }, { name: "Dominican Republic", dial_code: "+1 849", code: "DO" }, { name: "Ecuador", dial_code: "+593", code: "EC" }, { name: "Egypt", dial_code: "+20", code: "EG" }, { name: "El Salvador", dial_code: "+503", code: "SV" }, { name: "Equatorial Guinea", dial_code: "+240", code: "GQ" }, { name: "Eritrea", dial_code: "+291", code: "ER" }, { name: "Estonia", dial_code: "+372", code: "EE" }, { name: "Ethiopia", dial_code: "+251", code: "ET" }, { name: "Falkland Islands (Malvinas)", dial_code: "+500", code: "FK" }, { name: "Faroe Islands", dial_code: "+298", code: "FO" }, { name: "Fiji", dial_code: "+679", code: "FJ" }, { name: "Finland", dial_code: "+358", code: "FI" }, { name: "France", dial_code: "+33", code: "FR" }, { name: "French Guiana", dial_code: "+594", code: "GF" }, { name: "French Polynesia", dial_code: "+689", code: "PF" }, { name: "Gabon", dial_code: "+241", code: "GA" }, { name: "Gambia", dial_code: "+220", code: "GM" }, { name: "Georgia", dial_code: "+995", code: "GE" }, { name: "Germany", dial_code: "+49", code: "DE" }, { name: "Ghana", dial_code: "+233", code: "GH" }, { name: "Gibraltar", dial_code: "+350", code: "GI" }, { name: "Greece", dial_code: "+30", code: "GR" }, { name: "Greenland", dial_code: "+299", code: "GL" }, { name: "Grenada", dial_code: "+1 473", code: "GD" }, { name: "Guadeloupe", dial_code: "+590", code: "GP" }, { name: "Guam", dial_code: "+1 671", code: "GU" }, { name: "Guatemala", dial_code: "+502", code: "GT" }, { name: "Guernsey", dial_code: "+44", code: "GG" }, { name: "Guinea", dial_code: "+224", code: "GN" }, { name: "Guinea-Bissau", dial_code: "+245", code: "GW" }, { name: "Guyana", dial_code: "+595", code: "GY" }, { name: "Haiti", dial_code: "+509", code: "HT" }, { name: "Holy See (Vatican City State)", dial_code: "+379", code: "VA" }, { name: "Honduras", dial_code: "+504", code: "HN" }, { name: "Hong Kong", dial_code: "+852", code: "HK" }, { name: "Hungary", dial_code: "+36", code: "HU" }, { name: "Iceland", dial_code: "+354", code: "IS" }, { name: "India", dial_code: "+91", code: "IN" }, { name: "Indonesia", dial_code: "+62", code: "ID" }, { name: "Iran, Islamic Republic of", dial_code: "+98", code: "IR" }, { name: "Iraq", dial_code: "+964", code: "IQ" }, { name: "Ireland", dial_code: "+353", code: "IE" }, { name: "Isle of Man", dial_code: "+44", code: "IM" }, { name: "Israel", dial_code: "+972", code: "IL" }, { name: "Italy", dial_code: "+39", code: "IT" }, { name: "Jamaica", dial_code: "+1 876", code: "JM" }, { name: "Japan", dial_code: "+81", code: "JP" }, { name: "Jersey", dial_code: "+44", code: "JE" }, { name: "Jordan", dial_code: "+962", code: "JO" }, { name: "Kazakhstan", dial_code: "+7 7", code: "KZ" }, { name: "Kenya", dial_code: "+254", code: "KE" }, { name: "Kiribati", dial_code: "+686", code: "KI" }, {
        name: "Korea, Democratic People's Republic of",
        dial_code: "+850",
        code: "KP"
    }, { name: "Korea, Republic of", dial_code: "+82", code: "KR" }, { name: "Kuwait", dial_code: "+965", code: "KW" }, { name: "Kyrgyzstan", dial_code: "+996", code: "KG" }, { name: "Lao People's Democratic Republic", dial_code: "+856", code: "LA" }, { name: "Latvia", dial_code: "+371", code: "LV" }, { name: "Lebanon", dial_code: "+961", code: "LB" }, { name: "Lesotho", dial_code: "+266", code: "LS" }, { name: "Liberia", dial_code: "+231", code: "LR" }, { name: "Libyan Arab Jamahiriya", dial_code: "+218", code: "LY" }, { name: "Liechtenstein", dial_code: "+423", code: "LI" }, { name: "Lithuania", dial_code: "+370", code: "LT" }, { name: "Luxembourg", dial_code: "+352", code: "LU" }, { name: "Macao", dial_code: "+853", code: "MO" }, {
        name: "Macedonia, The Former Yugoslav Republic of",
        dial_code: "+389",
        code: "MK"
    }, { name: "Madagascar", dial_code: "+261", code: "MG" }, { name: "Malawi", dial_code: "+265", code: "MW" }, { name: "Malaysia", dial_code: "+60", code: "MY" }, { name: "Maldives", dial_code: "+960", code: "MV" }, { name: "Mali", dial_code: "+223", code: "ML" }, { name: "Malta", dial_code: "+356", code: "MT" }, { name: "Marshall Islands", dial_code: "+692", code: "MH" }, { name: "Martinique", dial_code: "+596", code: "MQ" }, { name: "Mauritania", dial_code: "+222", code: "MR" }, { name: "Mauritius", dial_code: "+230", code: "MU" }, { name: "Mayotte", dial_code: "+262", code: "YT" }, { name: "Mexico", dial_code: "+52", code: "MX" }, { name: "Micronesia, Federated States of", dial_code: "+691", code: "FM" }, { name: "Moldova, Republic of", dial_code: "+373", code: "MD" }, { name: "Monaco", dial_code: "+377", code: "MC" }, { name: "Mongolia", dial_code: "+976", code: "MN" }, { name: "Montenegro", dial_code: "+382", code: "ME" }, { name: "Montserrat", dial_code: "+1664", code: "MS" }, { name: "Morocco", dial_code: "+212", code: "MA" }, { name: "Mozambique", dial_code: "+258", code: "MZ" }, { name: "Myanmar", dial_code: "+95", code: "MM" }, { name: "Namibia", dial_code: "+264", code: "NA" }, { name: "Nauru", dial_code: "+674", code: "NR" }, { name: "Nepal", dial_code: "+977", code: "NP" }, { name: "Netherlands", dial_code: "+31", code: "NL" }, { name: "Netherlands Antilles", dial_code: "+599", code: "AN" }, { name: "New Caledonia", dial_code: "+687", code: "NC" }, { name: "New Zealand", dial_code: "+64", code: "NZ" }, { name: "Nicaragua", dial_code: "+505", code: "NI" }, { name: "Niger", dial_code: "+227", code: "NE" }, { name: "Nigeria", dial_code: "+234", code: "NG" }, { name: "Niue", dial_code: "+683", code: "NU" }, { name: "Norfolk Island", dial_code: "+672", code: "NF" }, { name: "Northern Mariana Islands", dial_code: "+1 670", code: "MP" }, { name: "Norway", dial_code: "+47", code: "NO" }, { name: "Oman", dial_code: "+968", code: "OM" }, { name: "Pakistan", dial_code: "+92", code: "PK" }, { name: "Palau", dial_code: "+680", code: "PW" }, { name: "Palestinian Territory, Occupied", dial_code: "+970", code: "PS" }, { name: "Panama", dial_code: "+507", code: "PA" }, { name: "Papua New Guinea", dial_code: "+675", code: "PG" }, { name: "Paraguay", dial_code: "+595", code: "PY" }, { name: "Peru", dial_code: "+51", code: "PE" }, { name: "Philippines", dial_code: "+63", code: "PH" }, { name: "Pitcairn", dial_code: "+872", code: "PN" }, { name: "Poland", dial_code: "+48", code: "PL" }, { name: "Portugal", dial_code: "+351", code: "PT" }, { name: "Puerto Rico", dial_code: "+1 939", code: "PR" }, { name: "Qatar", dial_code: "+974", code: "QA" }, { name: "Romania", dial_code: "+40", code: "RO" }, { name: "Russia", dial_code: "+7", code: "RU" }, { name: "Rwanda", dial_code: "+250", code: "RW" }, { name: "Réunion", dial_code: "+262", code: "RE" }, { name: "Saint Barthélemy", dial_code: "+590", code: "BL" }, {
        name: "Saint Helena, Ascension and Tristan Da Cunha",
        dial_code: "+290",
        code: "SH"
    }, { name: "Saint Kitts and Nevis", dial_code: "+1 869", code: "KN" }, { name: "Saint Lucia", dial_code: "+1 758", code: "LC" }, { name: "Saint Martin", dial_code: "+590", code: "MF" }, { name: "Saint Pierre and Miquelon", dial_code: "+508", code: "PM" }, {
        name: "Saint Vincent and the Grenadines",
        dial_code: "+1 784",
        code: "VC"
    }, { name: "Samoa", dial_code: "+685", code: "WS" }, { name: "San Marino", dial_code: "+378", code: "SM" }, { name: "Sao Tome and Principe", dial_code: "+239", code: "ST" }, { name: "Saudi Arabia", dial_code: "+966", code: "SA" }, { name: "Senegal", dial_code: "+221", code: "SN" }, { name: "Serbia", dial_code: "+381", code: "RS" }, { name: "Seychelles", dial_code: "+248", code: "SC" }, { name: "Sierra Leone", dial_code: "+232", code: "SL" }, { name: "Singapore", dial_code: "+65", code: "SG" }, { name: "Slovakia", dial_code: "+421", code: "SK" }, { name: "Slovenia", dial_code: "+386", code: "SI" }, { name: "Solomon Islands", dial_code: "+677", code: "SB" }, { name: "Somalia", dial_code: "+252", code: "SO" }, { name: "South Africa", dial_code: "+27", code: "ZA" }, {
        name: "South Georgia and the South Sandwich Islands",
        dial_code: "+500",
        code: "GS"
    }, { name: "Spain", dial_code: "+34", code: "ES" }, { name: "Sri Lanka", dial_code: "+94", code: "LK" }, { name: "Sudan", dial_code: "+249", code: "SD" }, { name: "Suriname", dial_code: "+597", code: "SR" }, { name: "Svalbard and Jan Mayen", dial_code: "+47", code: "SJ" }, { name: "Swaziland", dial_code: "+268", code: "SZ" }, { name: "Sweden", dial_code: "+46", code: "SE" }, { name: "Switzerland", dial_code: "+41", code: "CH" }, { name: "Syrian Arab Republic", dial_code: "+963", code: "SY" }, { name: "Taiwan, Province of China", dial_code: "+886", code: "TW" }, { name: "Tajikistan", dial_code: "+992", code: "TJ" }, { name: "Tanzania, United Republic of", dial_code: "+255", code: "TZ" }, { name: "Thailand", dial_code: "+66", code: "TH" }, { name: "Timor-Leste", dial_code: "+670", code: "TL" }, { name: "Togo", dial_code: "+228", code: "TG" }, { name: "Tokelau", dial_code: "+690", code: "TK" }, { name: "Tonga", dial_code: "+676", code: "TO" }, { name: "Trinidad and Tobago", dial_code: "+1 868", code: "TT" }, { name: "Tunisia", dial_code: "+216", code: "TN" }, { name: "Turkey", dial_code: "+90", code: "TR" }, { name: "Turkmenistan", dial_code: "+993", code: "TM" }, { name: "Turks and Caicos Islands", dial_code: "+1 649", code: "TC" }, { name: "Tuvalu", dial_code: "+688", code: "TV" }, { name: "Uganda", dial_code: "+256", code: "UG" }, { name: "Ukraine", dial_code: "+380", code: "UA" }, { name: "United Arab Emirates", dial_code: "+971", code: "AE" }, { name: "United Kingdom", dial_code: "+44", code: "GB" }, { name: "United States", dial_code: "+1", code: "US" }, { name: "Uruguay", dial_code: "+598", code: "UY" }, { name: "Uzbekistan", dial_code: "+998", code: "UZ" }, { name: "Vanuatu", dial_code: "+678", code: "VU" }, { name: "Venezuela, Bolivarian Republic of", dial_code: "+58", code: "VE" }, { name: "Viet Nam", dial_code: "+84", code: "VN" }, { name: "Virgin Islands, British", dial_code: "+1 284", code: "VG" }, { name: "Virgin Islands, U.S.", dial_code: "+1 340", code: "VI" }, { name: "Wallis and Futuna", dial_code: "+681", code: "WF" }, { name: "Yemen", dial_code: "+967", code: "YE" }, { name: "Zambia", dial_code: "+260", code: "ZM" }, { name: "Zimbabwe", dial_code: "+263", code: "ZW" }, { name: "Åland Islands", dial_code: "+358", code: "AX" }];
    return dials;
};

/***/ }),

/***/ 989:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(990)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(992)
/* template */
var __vue_template__ = __webpack_require__(993)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-42b8bad0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/cloudinaryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42b8bad0", Component.options)
  } else {
    hotAPI.reload("data-v-42b8bad0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 990:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(991);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("26a44260", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42b8bad0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./cloudinaryComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42b8bad0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./cloudinaryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 991:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.progress[data-v-42b8bad0] {\n  height: 20px;\n}\n.logo[data-v-42b8bad0] {\n  width: 50%;\n  height: 120px;\n  padding: 5px;\n}\n.images[data-v-42b8bad0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-top: 15px;\n}\n.oldimg[data-v-42b8bad0] {\n  width: 50%;\n  height: 120px;\n  padding: 5px;\n}\n.oldimg img[data-v-42b8bad0],\n.logo img[data-v-42b8bad0] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n", ""]);

// exports


/***/ }),

/***/ 992:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "CloudinaryUpload",
  props: ["oldimage"],
  data: function data() {
    return {
      filesSelectedLength: 0,
      file: [],
      filetype: "",
      uploadedFile: this.oldimage,
      uploadedFileUrl: "",
      cloudinary: {
        uploadPreset: "knkccgjv",
        apiKey: "634813511968181",
        cloudName: "bizguruh-com"
      },
      progress: 0,
      start: false
    };
  },


  computed: {},
  methods: {
    handleFileChange: function handleFileChange(event) {
      this.file = event.target.files[0];

      this.filesSelectedLength = event.target.files.length;

      this.loadFile();
    },
    loadFile: function loadFile() {
      var _this = this;

      var reader = new FileReader();
      reader.onload = function (event) {
        _this.uploadedFile = event.target.result;
      };
      reader.readAsDataURL(this.file);
    },
    processUpload: function processUpload() {
      var _this2 = this;

      var that = this;
      this.start = true;
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      var cloudName = this.cloudinary.cloudName;
      var upload_preset = this.cloudinary.uploadPreset;
      formData.append("file", this.file);
      formData.append("resource_type", "auto");
      formData.append("upload_preset", upload_preset); // REQUIRED
      xhr.open("POST", "https://api.cloudinary.com/v1_1/" + cloudName + "/upload");
      xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
          that.progress = Math.round(e.loaded / e.total * 100) + "%";
        }
      };
      xhr.upload.onloadstart = function (e) {
        this.progress = "Starting...";
      };
      xhr.upload.onloadend = function (e) {
        this.progress = "Completing..";
      };
      xhr.onload = function (progressEvent) {
        if (xhr.status === 200) {
          // Success! You probably want to save the URL somewhere
          _this2.progress = "Completed";
          setTimeout(function () {
            _this2.start = false;
          }, 1000);
          var response = JSON.parse(xhr.response);
          _this2.uploadedFileUrl = response.secure_url; // https address of uploaded file
          _this2.$emit("getUpload", _this2.uploadedFileUrl);
        } else {
          _this2.start = false;
          alert("Upload failed. Please try again.");
        }
      };
      xhr.send(formData);
    }
  }
});

/***/ }),

/***/ 993:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "form",
      {
        staticClass: "card",
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.processUpload($event)
          }
        }
      },
      [
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              staticClass: "form-control",
              attrs: {
                type: "file",
                id: "file-input",
                "aria-describedby": "helpId",
                placeholder: ""
              },
              on: {
                change: function($event) {
                  return _vm.handleFileChange($event)
                }
              }
            }),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "elevated_btn elevated_btn_sm text-main",
                attrs: { type: "submit", disabled: _vm.filesSelectedLength < 1 }
              },
              [_vm._v("Upload")]
            ),
            _vm._v(" "),
            _vm.start
              ? _c("div", { staticClass: "progress mt-2" }, [
                  _c(
                    "div",
                    {
                      staticClass: "progress-bar progress-bar-striped active",
                      style: { width: _vm.progress },
                      attrs: {
                        role: "progressbar",
                        "aria-valuenow": "0",
                        "aria-valuemin": "0",
                        "aria-valuemax": "100"
                      }
                    },
                    [_vm._v(_vm._s(_vm.progress))]
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "images" }, [
              _c("div", { staticClass: "oldimg" }, [
                _vm.filesSelectedLength >= 1
                  ? _c("small", [_vm._v("Current image")])
                  : _vm._e(),
                _vm._v(" "),
                _vm.oldimage !== ""
                  ? _c("img", { attrs: { src: _vm.oldimage, alt: "image" } })
                  : _vm._e()
              ]),
              _vm._v(" "),
              _vm.filesSelectedLength >= 1
                ? _c("div", { staticClass: "logo" }, [
                    _c("small", [_vm._v("New Image")]),
                    _vm._v(" "),
                    _c("img", {
                      attrs: { src: _vm.uploadedFile, alt: "image" }
                    })
                  ])
                : _vm._e()
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42b8bad0", module.exports)
  }
}

/***/ })

});