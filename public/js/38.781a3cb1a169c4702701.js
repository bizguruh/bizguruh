webpackJsonp([38],{

/***/ 1477:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1478);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("192edd4e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c8a97606\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userWhitePaperComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c8a97606\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userWhitePaperComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1478:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.catBg[data-v-c8a97606] {\n    background: linear-gradient(110deg,#e9eeef,#fff 33%);\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1479:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent__ = __webpack_require__(955);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userPaperContentComponent__ = __webpack_require__(1480);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userPaperContentComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_userPaperContentComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-white-paper-component",
    data: function data() {
        return {
            cartNo: '',
            cartId: ''
        };
    },
    components: {
        'app-side-category': __WEBPACK_IMPORTED_MODULE_0__components_userCourseCategoriesComponent___default.a,
        'app-paper-content': __WEBPACK_IMPORTED_MODULE_1__components_userPaperContentComponent___default.a
    },
    mounted: function mounted() {},

    methods: {
        addToCart: function addToCart() {
            this.$emit('update-cart', this.cartNo);
        }
    }
});

/***/ }),

/***/ 1480:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1481)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1483)
/* template */
var __vue_template__ = __webpack_require__(1484)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35475b6e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userPaperContentComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35475b6e", Component.options)
  } else {
    hotAPI.reload("data-v-35475b6e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1481:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1482);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("ff6d968c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35475b6e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPaperContentComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35475b6e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPaperContentComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1482:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.formatName[data-v-35475b6e] {\n    font-weight: bold;\n    font-size: 12px;\n}\n.formatStyle[data-v-35475b6e] {\n    float: left;\n    margin-right: 10px;\n}\n.vieww[data-v-35475b6e] {\n    margin-bottom: 10px;\n}\n.productsPagePrice[data-v-35475b6e] {\n    border: 1px solid #a3c2dc;\n    padding: 10px;\n    cursor: pointer;\n}\n.productsPrice[data-v-35475b6e] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n.datePaper[data-v-35475b6e] {\n    color: #cccccc;\n}\n.paperTitle[data-v-35475b6e] {\n    text-transform: capitalize;\n    font-weight: bold;\n}\n.viewDetail[data-v-35475b6e] {\n    padding: 3px;\n    border: 2px solid #a3c2dc;\n    text-align: center;\n    cursor: pointer;\n}\n.viewDetailCart[data-v-35475b6e] {\n    background-color: #a3c2dc !important;\n    color: #ffffff;\n    padding: 3px;\n    text-align: center;\n    cursor: pointer;\n    border: 2px solid #a3c2dc;\n}\n.imgPaper[data-v-35475b6e] {\n    width: 150px;\n}\n.paperDiv[data-v-35475b6e] {\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    margin: 30px\n}\n.whitePP[data-v-35475b6e] {\n    height: 100px;\n    width: 600px;\n    white-space: initial;\n    overflow: hidden;\n    text-overflow: ellipsis;\n}\n.modal-mask[data-v-35475b6e] {\n    position: fixed;\n    z-index: 9998;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, .5);\n    display: table;\n    -webkit-transition: opacity .3s ease;\n    transition: opacity .3s ease;\n}\n.modal-wrapper[data-v-35475b6e] {\n    display: table-cell;\n    vertical-align: middle;\n}\n.modal-container[data-v-35475b6e] {\n    width: 300px;\n    margin: 0px auto;\n    padding: 20px 30px;\n    background-color: #fff;\n    border-radius: 2px;\n    -webkit-box-shadow: 0 2px 8px rgba(0, 0, 0, .33);\n            box-shadow: 0 2px 8px rgba(0, 0, 0, .33);\n    -webkit-transition: all .3s ease;\n    transition: all .3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal-header h3[data-v-35475b6e] {\n    margin-top: 0;\n    color: #42b983;\n}\n.modal-body[data-v-35475b6e] {\n    margin: 20px 0;\n}\n.modal-default-button[data-v-35475b6e] {\n    float: right;\n}\n\n/*\n * The following styles are auto-applied to elements with\n * transition=\"modal\" when their visibility is toggled\n * by Vue.js.\n *\n * You can easily play with the modal transition by editing\n * these styles.\n */\n.modal-enter[data-v-35475b6e] {\n    opacity: 0;\n}\n.modal-leave-active[data-v-35475b6e] {\n    opacity: 0;\n}\n.modal-enter .modal-container[data-v-35475b6e],\n.modal-leave-active .modal-container[data-v-35475b6e] {\n    -webkit-transform: scale(1.1);\n    transform: scale(1.1);\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-paper-content-component",
    data: function data() {
        return {
            whitePapers: [],
            name: this.$route.params.name,
            price: '',
            title: '',
            priceT: '',
            authenticate: false,
            email: '',
            user_id: '',
            user: {},
            showModal: false,
            anonymousCart: [],
            epiModal: true,
            epiFade: true,
            loginCart: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.email = user.email;
            this.user_id = user.id;
            this.user = user;
            this.authenticate = true;
        }

        var name = this.name.replace('-', ' ').toLowerCase().split(' ').map(function (s) {
            return s.charAt(0).toUpperCase() + s.substring(1);
        }).join(' ');

        axios.get('/api/get/prodtype/' + name).then(function (response) {

            if (response.status === 200) {
                var product = response.data.data;
                product.forEach(function (item) {
                    if (item.prodType === 'White Paper') {
                        item.whitePaper.category = item.category.name;
                        item.whitePaper.hardCopyPrice = item.hardCopyPrice;
                        item.whitePaper.uid = item.id;
                        item.whitePaper.price = '';
                        item.whitePaper.ptitle = '';
                        item.whitePaper.priceT = '';
                        item.whitePaper.softCopyPrice = item.softCopyPrice;
                        item.whitePaper.readOnlinePrice = item.readOnlinePrice;
                        item.whitePaper.coverImage = item.coverImage;
                        item.whitePaper.uid = item.id;
                        _this.whitePapers.push(item.whitePaper);
                    } else if (item.prodType === 'Market Reports') {
                        item.marketReport.category = item.category.name;
                        item.marketReport.hardCopyPrice = item.hardCopyPrice;
                        item.marketReport.uid = item.id;
                        item.marketReport.softCopyPrice = item.softCopyPrice;
                        item.marketReport.price = '';
                        item.marketReport.priceT = '';
                        item.marketReport.ptitle = '';
                        item.marketReport.readOnlinePrice = item.readOnlinePrice;
                        item.marketReport.coverImage = item.coverImage;
                        item.marketReport.uid = item.id;
                        _this.whitePapers.push(item.marketReport);
                    } else if (item.prodType === 'Market Research') {
                        item.marketResearch.category = item.category.name;
                        item.marketResearch.coverImage = item.coverImage;
                        item.marketResearch.hardCopyPrice = item.hardCopyPrice;
                        item.marketResearch.uid = item.id;
                        item.marketResearch.priceT = '';
                        item.marketResearch.price = '';
                        item.marketResearch.ptitle = '';
                        item.marketResearch.softCopyPrice = item.softCopyPrice;
                        item.marketResearch.readOnlinePrice = item.readOnlinePrice;
                        item.marketResearch.uid = item.id;
                        _this.whitePapers.push(item.marketResearch);
                    }
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        togglePrice: function togglePrice(params, index) {
            if (params === 'hardCopy') {

                this.whitePapers[index].price = this.whitePapers[index].hardCopyPrice;
                this.whitePapers[index].ptitle = 'HardCopy';
            } else if (params === 'softCopy') {

                this.whitePapers[index].price = this.whitePapers[index].softCopyPrice;

                this.whitePapers[index].ptitle = 'SoftCopy';
            } else if (params === 'readOnline') {

                this.whitePapers[index].price = this.whitePapers[index].readOnlinePrice;

                this.whitePapers[index].ptitle = 'ReadOnline';
            }
        },
        addToCart: function addToCart(id, index, title) {
            if (this.whitePapers[index].priceT === '') {
                this.$toasted.error('Please Select Product Format');
            } else {
                var cart = {
                    productId: id,
                    price: this.whitePapers[index].priceT
                };

                if (this.whitePapers[index].priceT === this.whitePapers[index].hardCopyPrice) {
                    cart.quantity = 1;
                    cart.prodType = 'HardCopy';
                } else if (this.whitePapers[index].priceT === this.whitePapers[index].softCopyPrice) {
                    cart.quantity = 0;
                    cart.prodType = 'SoftCopy';
                } else if (this.whitePapers[index].priceT === this.whitePapers[index].readOnlinePrice) {
                    cart.quantity = 0;
                    cart.prodType = 'ReadOnline';
                }

                if (this.authenticate === false) {
                    cart.cartNum = 1;
                    this.anonymousCart.push(cart);
                    if (JSON.parse(localStorage.getItem('userCart')) === null) {
                        localStorage.setItem('userCart', JSON.stringify(this.anonymousCart));
                        var sessionCart = JSON.parse(localStorage.getItem('userCart'));
                        var cartCount = sessionCart.length;
                        this.$emit('getCartCountT', cartCount);
                        this.$emit('getCartCount', cartCount);
                        this.$notify({
                            group: 'cart',
                            title: title,
                            text: 'Successfully Added to Cart!'
                        });
                        this.epiFade = false;
                    } else if (JSON.parse(localStorage.getItem('userCart')) !== null) {
                        var _sessionCart = JSON.parse(localStorage.getItem('userCart'));
                        var ss = _sessionCart.length;
                        _sessionCart[ss] = cart;
                        localStorage.setItem('userCart', JSON.stringify(_sessionCart));
                        var a = JSON.parse(localStorage.getItem('userCart'));
                        var aCount = a.length;
                        this.$emit('getCartCountT', aCount);
                        this.$emit('getCartCount', aCount);
                        this.$notify({
                            group: 'cart',
                            title: title,
                            text: 'Successfully Added to Cart!'
                        });
                    }
                    this.epiFade = false;
                } else {
                    this.addCart(cart, title);
                }
            }
        },
        addCart: function addCart(cart, title) {
            var _this2 = this;

            axios.post('/api/cart', JSON.parse(JSON.stringify(cart)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 201) {
                    _this2.loginCart.push(response.data);
                    var aCount = _this2.loginCart.length;
                    _this2.$emit('getCartCountT', aCount);
                    _this2.$emit('getCartCount', aCount);
                    _this2.$notify({
                        group: 'cart',
                        title: title,
                        text: 'Successfully Added to Cart!'
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1484:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {},
    _vm._l(_vm.whitePapers, function(whitePaper, index) {
      return _vm.whitePapers.length > 0
        ? _c("div", { staticClass: "paperDiv" }, [
            _c("div", {}, [
              _c("img", {
                staticClass: "imgPaper",
                attrs: { src: whitePaper.coverImage, width: "150" }
              })
            ]),
            _vm._v(" "),
            _c("div", {}, [
              _c("div", { staticClass: "paperTitle" }, [
                _vm._v(_vm._s(whitePaper.title))
              ]),
              _vm._v(" "),
              _c("small", { staticClass: "datePaper" }, [
                _vm._v(
                  _vm._s(_vm._f("moment")(whitePaper.created_at, "MMM YYYY"))
                )
              ]),
              _vm._v(" "),
              whitePaper.executiveSummary
                ? _c("div", { staticClass: "whitePP" }, [
                    _vm._v(_vm._s(whitePaper.executiveSummary))
                  ])
                : _c("div", { staticClass: "whitePP" }, [
                    _vm._v(_vm._s(whitePaper.excerpt))
                  ])
            ]),
            _vm._v(" "),
            _c("div", [
              _c("div", { staticClass: "vieww" }, [
                _c(
                  "div",
                  {
                    staticClass: "viewDetailCart",
                    on: {
                      click: function($event) {
                        return _vm.addToCart(
                          whitePaper.uid,
                          index,
                          whitePaper.title
                        )
                      }
                    }
                  },
                  [_vm._v("Add to Cart")]
                )
              ]),
              _vm._v(" "),
              _c("div", {}, [
                _c(
                  "div",
                  { staticClass: "viewDetail" },
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "ProductPaperDetail",
                            params: {
                              id: whitePaper.uid,
                              name: whitePaper.title
                                .replace(/\s+/g, "-")
                                .toLowerCase(),
                              type: "product"
                            }
                          }
                        }
                      },
                      [_vm._v("View Detail")]
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              whitePaper.hardCopyPrice !== ""
                ? _c("div", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: whitePaper.priceT,
                          expression: "whitePaper.priceT"
                        }
                      ],
                      staticClass: "formatStyle",
                      attrs: { type: "radio", name: "format" },
                      domProps: {
                        value: whitePaper.hardCopyPrice,
                        checked: _vm._q(
                          whitePaper.priceT,
                          whitePaper.hardCopyPrice
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            whitePaper,
                            "priceT",
                            whitePaper.hardCopyPrice
                          )
                        }
                      }
                    }),
                    _c("span", { staticClass: "formatName" }, [
                      _vm._v("Hard Copy")
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              whitePaper.softCopyPrice !== ""
                ? _c("div", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: whitePaper.priceT,
                          expression: "whitePaper.priceT"
                        }
                      ],
                      staticClass: "formatStyle",
                      attrs: { type: "radio", name: "format" },
                      domProps: {
                        value: whitePaper.softCopyPrice,
                        checked: _vm._q(
                          whitePaper.priceT,
                          whitePaper.softCopyPrice
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            whitePaper,
                            "priceT",
                            whitePaper.softCopyPrice
                          )
                        }
                      }
                    }),
                    _c("span", { staticClass: "formatName" }, [
                      _vm._v("Digital")
                    ])
                  ])
                : _vm._e(),
              _vm._v(" "),
              whitePaper.readOnlinePrice !== ""
                ? _c("div", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: whitePaper.priceT,
                          expression: "whitePaper.priceT"
                        }
                      ],
                      staticClass: "formatStyle",
                      attrs: { type: "radio", name: "format" },
                      domProps: {
                        value: whitePaper.readOnlinePrice,
                        checked: _vm._q(
                          whitePaper.priceT,
                          whitePaper.readOnlinePrice
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            whitePaper,
                            "priceT",
                            whitePaper.readOnlinePrice
                          )
                        }
                      }
                    }),
                    _c("span", { staticClass: "formatName" }, [
                      _vm._v("Read Online")
                    ])
                  ])
                : _vm._e()
            ])
          ])
        : _vm._e()
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35475b6e", module.exports)
  }
}

/***/ }),

/***/ 1485:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "course-Detail" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-md-2 catBg" },
        [_c("app-side-category")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-10" },
        [
          _c("app-paper-content", {
            on: {
              getCartCount: function($event) {
                return _vm.addToCart()
              },
              getCartCountT: function($event) {
                _vm.cartNo = $event
              }
            }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c8a97606", module.exports)
  }
}

/***/ }),

/***/ 572:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1477)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1479)
/* template */
var __vue_template__ = __webpack_require__(1485)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c8a97606"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userWhitePaperComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c8a97606", Component.options)
  } else {
    hotAPI.reload("data-v-c8a97606", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 955:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(956)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(958)
/* template */
var __vue_template__ = __webpack_require__(959)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1468ed38"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userCourseCategoriesComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1468ed38", Component.options)
  } else {
    hotAPI.reload("data-v-1468ed38", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 956:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(957);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3a2541c4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1468ed38\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCourseCategoriesComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1468ed38\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCourseCategoriesComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 957:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.catName[data-v-1468ed38] {\n    text-transform: capitalize;\n}\n.sidebarcat[data-v-1468ed38] {\n    margin-top: 30px;\n}\n.envCat[data-v-1468ed38] {\n    margin: 30px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-course-categories",
    data: function data() {
        return {
            categories: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/api/product-all-category').then(function (response) {
            if (response.status === 200) {
                _this.categories = response.data.data;
            }
        }).catch(function (error) {
            console.log(error);
        });
    }
});

/***/ }),

/***/ 959:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sidebarcat" }, [
    _c(
      "ul",
      [
        _c("li", { staticClass: "envCat" }, [_vm._v("All Courses")]),
        _vm._v(" "),
        _vm._l(_vm.categories, function(category, index) {
          return _vm.categories.length > 0
            ? _c("li", { staticClass: "envCat" }, [
                _c("div", { staticClass: "catName" }, [
                  _vm._v(_vm._s(category.name))
                ]),
                _vm._v(" "),
                _c("small")
              ])
            : _vm._e()
        })
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1468ed38", module.exports)
  }
}

/***/ })

});