webpackJsonp([100],{

/***/ 1135:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1136);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cde67dba", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5ff8d2db\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editCategoriesVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5ff8d2db\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editCategoriesVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1136:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.firstBar[data-v-5ff8d2db] {\n    border: 4px solid black;\n    height: 55vh;\n    overflow: scroll;\n}\n.hightlightbg[data-v-5ff8d2db] {\n    background: #222d32;\n    color: #ffffff;\n}\n.category[data-v-5ff8d2db] {\n    width: 100%;\n    padding: 8px;\n    border-bottom: 1px solid black;\n    cursor: pointer;\n}\n#page-wrapper[data-v-5ff8d2db] {\n    height: 100vh;\n}\n.widget-shadow[data-v-5ff8d2db] {\n    height: 85vh;\n}\n.hover[data-v-5ff8d2db], .clickColor[data-v-5ff8d2db] {\n    background: #222d32;\n    color: #ffffff;\n}\n.cat-header[data-v-5ff8d2db] {\n    margin: 19px 0;\n    font-size: 17px;\n    font-weight: bold;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "edit-categories-vendor-component",
    data: function data() {
        return {
            id: this.$route.params.id,
            token: '',
            cat_id: '',
            catd: true,
            sub_id: '',

            vendor_user_id: '',
            category: '',
            category_name: '',
            subcategory_name: '',
            subcategory: '',
            categories: [],
            subcategories: [],
            subcategoriesBrand: [],
            cat: {
                category_id: '',
                sub_category_id: '',
                sub_category_brand_id: [],
                vendor_user_id: ''
            },
            btn: false,
            topicId: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authVendor')) {
            var authVendor = JSON.parse(localStorage.getItem('authVendor'));
            this.token = authVendor.access_token;
            this.cat.vendor_user_id = authVendor.id;
            axios.get('/api/vendor-product/' + this.id + '/edit', { headers: { "Authorization": 'Bearer ' + authVendor.access_token } }).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    _this.cat.category_id = response.data.data.category_id;
                    _this.cat.sub_category_id = response.data.data.sub_category_id;
                    if (response.data.data.sub_category_brand_id) {
                        var firstArray = response.data.data.sub_category_brand_id;
                        var secondArray = firstArray.replace(']', '');
                        var thirdArray = secondArray.replace('[', '');
                        var fourthArray = thirdArray.split(',');
                        var realArray = [];
                        fourthArray.forEach(function (item) {
                            realArray.push(JSON.parse(item));
                        });

                        _this.cat.sub_category_brand_id = realArray;
                        _this.topicId = realArray;
                    }

                    _this.category = response.data.data.category_id;
                    _this.subcategory = response.data.data.sub_category_id;
                    _this.getAllCategory();
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        getAllCategory: function getAllCategory() {
            var _this2 = this;

            axios.get('/api/get/all/category', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this2.categories = response.data;
                    _this2.categories.forEach(function (item) {
                        _this2.$set(item, 'hover', false);
                    });
                    _this2.getSubCategory(_this2.cat.category_id);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getSubCategory: function getSubCategory(subcat, category, category_name) {
            var _this3 = this;

            if (category !== undefined) {
                this.subcategoriesBrand = [];
                this.cat.sub_category_brand_id = '';
                this.cat.sub_category_id = '';
                this.categories.forEach(function (val) {
                    val.click = false;
                });
                console.log(subcat);
                console.log('this is a subcat');

                axios.get('/api/get/all/subcategory/' + subcat, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this3.cat.category_id = subcat;
                        _this3.subcategories = response.data;
                        _this3.subcategories.forEach(function (item) {
                            _this3.$set(item, 'hover', false);
                        });
                        // category.click = true;
                        _this3.category_name = category_name;
                        _this3.btn = true;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                this.categories.forEach(function (val) {
                    val.click = false;
                });
                axios.get('/api/get/all/subcategory/' + subcat, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this3.cat.category_id = subcat;
                        _this3.subcategories = response.data;
                        _this3.subcategories.forEach(function (item) {
                            _this3.$set(item, 'hover', false);
                        });
                        // category.click = true;
                        _this3.getSubCategoryBrand(_this3.cat.sub_category_id);
                        _this3.category_name = category_name;
                        _this3.btn = true;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        getSubCategoryBrand: function getSubCategoryBrand(subcatBrand) {
            var _this4 = this;

            axios.get('/api/get/subcategorybrand/' + subcatBrand, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                var a = [];
                if (response.status === 200) {
                    _this4.subcategoriesBrand = response.data;
                    _this4.subcategoriesBrand.forEach(function (item) {

                        _this4.$set(item, 'hover', false);

                        _this4.cat.sub_category_brand_id.forEach(function (items) {

                            if (parseInt(items) === parseInt(item.id)) {
                                _this4.$set(item, 'clickF', true);
                            } else {
                                console.log('false');
                            }
                        });
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getSubCategoryId: function getSubCategoryId(subcatid, subcategory, subcategory_name) {
            var _this5 = this;

            axios.get('/api/get/all/subcategorybrand/' + subcatid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this5.subcategoriesBrand = response.data;
                    _this5.subcategoriesBrand.forEach(function (item) {
                        _this5.$set(item, 'hover', false);
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
            this.subcategories.forEach(function (val) {
                val.click = false;
            });
            this.cat.sub_category_id = subcatid;
            this.subcategory_name = subcategory_name;
            subcategory.click = true;
            this.btn = false;
        },
        getSubCategoryBrandById: function getSubCategoryBrandById(subrandId, subcategoryBrand, subcategoryBrand_name, index) {

            if (this.topicId.includes(subrandId)) {
                var a = this.topicId.indexOf(subrandId);
                if (a > -1) {
                    console.log('test');
                    this.topicId.splice(a, 1);
                    this.cat.sub_category_brand_id = this.topicId;
                    subcategoryBrand.clickF = false;
                }
            } else {
                this.topicId.push(subrandId);
                this.cat.sub_category_brand_id = this.topicId;
                subcategoryBrand.clickF = true;
            }

            /*
            this.subcategoriesBrand.forEach((item) => {
                item.click = false;
            });
            this.cat.sub_category_brand_id = subrandId;
            subcategoryBrand.click = true;*/
        },
        editCategory: function editCategory() {
            var _this6 = this;

            var cat = {
                category_id: this.cat.category_id,
                sub_category_id: this.cat.sub_category_id,
                sub_category_brand_id: JSON.stringify(this.cat.sub_category_brand_id),
                vendor_user_id: this.cat.vendor_user_id
                // JSON.stringify(this.cat.sub_category_brand_id);

            };axios.put('/api/vendor-product/' + this.id, JSON.parse(JSON.stringify(cat)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    console.log("TCL: editCategory -> response", response);

                    var id = response.data.data.id;
                    _this6.$toasted.success('Category successfully updated ');
                    _this6.$router.push('/vendor/' + _this6.id + '/edit');
                }
            }).catch(function (error) {
                for (var key in error.response.data.data.errors) {
                    _this6.$toasted.error(error.response.data.errors[key][0]);
                }
            });
        }
    }
});

/***/ }),

/***/ 1138:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("h5", [_vm._v("Edit Product")]),
    _vm._v(" "),
    _c("div", { staticClass: "row widget-shadow" }, [
      _c("div", { staticClass: "col-md-4" }, [
        _c("h3", { staticClass: "cat-header" }, [
          _vm._v("Select Menu Category")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.categories, function(category, index) {
            return _c(
              "div",
              {
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hightlightbg: category.id === _vm.cat.category_id,
                  hover: category.hover,
                  clickColor: category.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategory(
                      category.id,
                      category,
                      category.name
                    )
                  },
                  mouseenter: function($event) {
                    category.hover = true
                  },
                  mouseleave: function($event) {
                    category.hover = false
                  }
                }
              },
              [
                _vm._v(
                  "\n                        " +
                    _vm._s(category.name) +
                    "\n                "
                )
              ]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4" }, [
        _c("h3", { staticClass: "cat-header" }, [
          _vm._v("Select Subject Matter")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.subcategories, function(subcategory, index) {
            return _c(
              "div",
              {
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hightlightbg: subcategory.id === _vm.cat.sub_category_id,
                  hover: subcategory.hover,
                  clickColor: subcategory.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategoryId(
                      subcategory.id,
                      subcategory,
                      subcategory.name
                    )
                  },
                  mouseenter: function($event) {
                    subcategory.hover = true
                  },
                  mouseleave: function($event) {
                    subcategory.hover = false
                  }
                }
              },
              [
                _vm._v(
                  "\n                        " +
                    _vm._s(subcategory.name) +
                    "\n                "
                )
              ]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4" }, [
        _c("h3", { staticClass: "cat-header" }, [_vm._v("Select Topic")]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "firstBar" },
          _vm._l(_vm.subcategoriesBrand, function(subcategoryBrand, index) {
            return _c(
              "div",
              {
                staticClass: "row m-0",
                class: {
                  category: "catd",
                  hightlightbg: subcategoryBrand.clickF === true,
                  hover: subcategoryBrand.hover,
                  clickColor: subcategoryBrand.click
                },
                on: {
                  click: function($event) {
                    return _vm.getSubCategoryBrandById(
                      subcategoryBrand.id,
                      subcategoryBrand,
                      subcategoryBrand.name,
                      index
                    )
                  },
                  mouseenter: function($event) {
                    subcategoryBrand.hover = true
                  },
                  mouseleave: function($event) {
                    subcategoryBrand.hover = false
                  }
                }
              },
              [
                _vm._v(
                  "\n                    " +
                    _vm._s(subcategoryBrand.name) +
                    _vm._s(subcategoryBrand.id) +
                    "\n                "
                )
              ]
            )
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 text-center" }, [
        _c("h3", [_vm._v(_vm._s(_vm.category_name))]),
        _vm._v(" "),
        _c("p", [_vm._v(_vm._s(_vm.subcategory_name))]),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            on: {
              click: function($event) {
                return _vm.editCategory()
              }
            }
          },
          [_vm._v("Edit Category")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5ff8d2db", module.exports)
  }
}

/***/ }),

/***/ 501:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1135)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1137)
/* template */
var __vue_template__ = __webpack_require__(1138)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5ff8d2db"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/editCategoriesVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5ff8d2db", Component.options)
  } else {
    hotAPI.reload("data-v-5ff8d2db", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});