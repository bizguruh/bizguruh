webpackJsonp([125],{

/***/ 1836:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1837);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("71bec86d", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6352c762\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidesHistoryComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6352c762\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidesHistoryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1837:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-6352c762] {\n  margin-top: 20px;\n  position: relative;\n  min-height: 100vh;\n}\nul[data-v-6352c762], ol[data-v-6352c762]{\n    text-decoration:none !important;\n}\n.toCaps[data-v-6352c762]:hover{\n    cursor:pointer;\n    text-decoration: underline;\n}\ntd[data-v-6352c762]{\n    font-size:16px;\n}\n.mode[data-v-6352c762]{\n      margin-top: 50px;\n       display:-webkit-box;\n       display:-ms-flexbox;\n       display:flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    background-color: rgba(0, 0, 0, .8);\n    padding:15px;\n}\n.modeBody[data-v-6352c762]{\n     display:-webkit-box;\n     display:-ms-flexbox;\n     display:flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.close[data-v-6352c762]{\n    position: absolute;\n    top:20px;\n    right:20px;\n}\n.congrats[data-v-6352c762]{\n    font-size:16px;\n}\n@media (max-width: 425px){\n.container[data-v-6352c762]{\n        min-height: 130vh;\n}\n.mode[data-v-6352c762]{\n        overflow-y: scroll;\n}\nh2[data-v-6352c762]{\n      font-size: 16px;\n}\ntd[data-v-6352c762]{\n      font-size:14px;\n}\n.card-body[data-v-6352c762]{\n      padding:.6rem;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "guides-history-component",

  data: function data() {
    return {
      id: null,
      histories: [],
      open: false,
      questions: [],
      modelAnswer: [],
      totalScore: 0,
      history: {}
    };
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      this.id = user.id;
    }
    this.getHistory();
  },

  methods: {
    getHistory: function getHistory() {
      var _this = this;

      axios.get("/api/history/" + this.id).then(function (response) {
        _this.histories = response.data;
      });
    },
    getCurrentHistory: function getCurrentHistory(id) {
      var _this2 = this;

      var data = {
        id: this.id,
        question_id: id
      };
      axios.get("/api/questions/" + id).then(function (response) {
        if (response.status === 200) {
          _this2.questions = JSON.parse(response.data.questions);
          _this2.openModal();
        }
      });

      axios.post('/api/single/history', data).then(function (response) {

        _this2.history = response.data;
        if (_this2.history.name === 'qa') {
          _this2.totalScore = JSON.parse(_this2.history.answers);
        } else {
          _this2.modelAnswer = JSON.parse(_this2.history.answers);
        }
      });
    },
    openModal: function openModal() {
      this.open = true;
    },
    closeModal: function closeModal() {
      this.open = false;
    }
  }
});

/***/ }),

/***/ 1839:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container bg-white rounded shadow-sm" }, [
    _vm.open
      ? _c("div", { staticClass: "mode animated fadeIn" }, [
          _c("div", { staticClass: "close", on: { click: _vm.closeModal } }, [
            _c("i", {
              staticClass: "fa fa-times fa-2x fa-inverse",
              attrs: { "aria-hidden": "true" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modeBody bg-white p-2 rounded" }, [
            _vm.history.name === "template"
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "card-columns" },
                    _vm._l(_vm.questions, function(model, index) {
                      return _c(
                        "div",
                        { key: index, staticClass: "card" },
                        [
                          _c(
                            "div",
                            {
                              staticClass:
                                "card-header bg-primary text-white text-center"
                            },
                            [_c("h4", [_vm._v(_vm._s(model.category))])]
                          ),
                          _vm._v(" "),
                          _vm._l(_vm.modelAnswer[index], function(result, idx) {
                            return _c(
                              "div",
                              { key: idx, staticClass: "card-body" },
                              [
                                _c("h5", { staticClass: "card-title" }, [
                                  _vm._v(
                                    "Q" +
                                      _vm._s(idx + 1) +
                                      ". " +
                                      _vm._s(model.questions[idx].title)
                                  )
                                ]),
                                _vm._v(" "),
                                typeof result == "object"
                                  ? _c(
                                      "p",
                                      {
                                        staticClass: "card-text text-capitalize"
                                      },
                                      [
                                        _c(
                                          "ul",
                                          { staticClass: "text-capitalize" },
                                          _vm._l(result, function(
                                            results,
                                            idx
                                          ) {
                                            return _c("li", { key: idx }, [
                                              _vm._v(
                                                _vm._s(results.toLowerCase())
                                              )
                                            ])
                                          }),
                                          0
                                        )
                                      ]
                                    )
                                  : _c(
                                      "p",
                                      {
                                        staticClass:
                                          "card-text text-capitalize pl-3"
                                      },
                                      [
                                        _vm._v(
                                          " " +
                                            _vm._s(
                                              result
                                                .toLowerCase()
                                                .replace(/''/g, " ")
                                            )
                                        )
                                      ]
                                    )
                              ]
                            )
                          })
                        ],
                        2
                      )
                    }),
                    0
                  )
                ])
              : _vm.history.name === "bg"
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "card-columns" },
                    _vm._l(_vm.questions, function(model, index) {
                      return _c(
                        "div",
                        { key: index, staticClass: "card" },
                        [
                          _c(
                            "div",
                            {
                              staticClass:
                                "card-header bg-primary text-white text-center"
                            },
                            [_c("h4", [_vm._v(_vm._s(model.category))])]
                          ),
                          _vm._v(" "),
                          _vm._l(_vm.modelAnswer[index], function(result, idx) {
                            return _c(
                              "div",
                              { key: idx, staticClass: "card-body" },
                              [
                                typeof result == "object "
                                  ? _c(
                                      "p",
                                      {
                                        staticClass: "card-text text-capitalize"
                                      },
                                      [
                                        _vm._v(
                                          "\n                   Response :  "
                                        ),
                                        _vm._l(result, function(results, idx) {
                                          return _c(
                                            "span",
                                            {
                                              key: idx,
                                              staticClass: "text-capitalize"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  results
                                                    .toLowerCase()
                                                    .replace(/''/g, "-")
                                                )
                                              )
                                            ]
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  : _c(
                                      "p",
                                      {
                                        staticClass:
                                          "card-text text-capitalize  pl-3"
                                      },
                                      [
                                        _vm._v(
                                          "Response : " +
                                            _vm._s(result.toLowerCase())
                                        )
                                      ]
                                    )
                              ]
                            )
                          })
                        ],
                        2
                      )
                    }),
                    0
                  )
                ])
              : _c("div", [
                  _c("p", { staticClass: "congrats" }, [
                    _vm._v(
                      "Congratulations you just completed this Q&A section, you scored"
                    )
                  ]),
                  _vm._v(" "),
                  _c("h1", { staticClass: "text-center" }, [
                    _vm._v(_vm._s(_vm.totalScore))
                  ])
                ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("h2", [_vm._v("Checklist & Guide History")]),
    _vm._v(" "),
    _c("table", { staticClass: "table" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.histories, function(item, idx) {
          return _c("tr", { key: idx }, [
            _c(
              "td",
              {
                staticClass: "toCaps",
                attrs: { scope: "row" },
                on: {
                  click: function($event) {
                    return _vm.getCurrentHistory(item.question_id)
                  }
                }
              },
              [_vm._v(_vm._s(item.title.toLowerCase()))]
            ),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(item.created_at))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(item.updated_at))])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", [_vm._v("Date Created ")]),
        _vm._v(" "),
        _c("th", [_vm._v("Date Updated")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6352c762", module.exports)
  }
}

/***/ }),

/***/ 641:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1836)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1838)
/* template */
var __vue_template__ = __webpack_require__(1839)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6352c762"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/guidesHistoryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6352c762", Component.options)
  } else {
    hotAPI.reload("data-v-6352c762", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});