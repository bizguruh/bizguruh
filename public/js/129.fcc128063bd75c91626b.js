webpackJsonp([129],{

/***/ 1630:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1631);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("ed0347fa", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b31cece\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./termsAndConditionComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0b31cece\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./termsAndConditionComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1631:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bread ul[data-v-0b31cece] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n   margin: 10px 0px 30px;\n    padding:5px 15px;\n    background:#fff;\n}\n.bread ul > li[data-v-0b31cece] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bread ul > li[data-v-0b31cece]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-0b31cece] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.container[data-v-0b31cece]{\n    padding-top:80px;\n    padding-bottom: 100px;\n    line-height:1.8;\n    font-size:16px;\n}\ndiv ol[data-v-0b31cece]{\n    list-style: lower-roman;\n}\nh3[data-v-0b31cece]{\n    margin:5px 0;\n}\nul[data-v-0b31cece]{\n    list-style: circle;\n}\n.boldText[data-v-0b31cece]{\n    font-weight:bold;\n    font-size:16px;\n    margin-bottom:2px;\n}\n.boldText[data-v-0b31cece]{\n    color:rgba(0, 0, 0,0.64)\n}\n.uline[data-v-0b31cece]{\n    text-decoration:underline;\n}\n.tCenter[data-v-0b31cece]{\n    margin: 10px auto;\n  text-align: center;\n}\n.pryColor[data-v-0b31cece]{\n    color: #a3c2dc;\n    font-weight: bold;\n}\n@media(max-width:425px){\nh2[data-v-0b31cece]{\n        font-size: 18px;\n}\nh3[data-v-0b31cece]{\n        font-size: 16px;\n}\n.boldText[data-v-0b31cece]{\n        font-size:16px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1632:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "terms-and-condition-component",
    data: function data() {
        return {
            breadcrumbList: []
        };
    },
    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        }
    }
});

/***/ }),

/***/ 1633:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "bread" }, [
      _c(
        "ul",
        _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
          return _c(
            "li",
            {
              key: index,
              class: { linked: !!breadcrumb.link },
              on: {
                click: function($event) {
                  return _vm.routeTo(index)
                }
              }
            },
            [
              _vm._v(
                "\n               " +
                  _vm._s(breadcrumb.name) +
                  "\n             "
              )
            ]
          )
        }),
        0
      )
    ]),
    _vm._v(" "),
    _vm._m(0)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", {}, [
      _c("h2", { staticClass: "tCenter uline mb-4" }, [
        _vm._v(" TERMS AND CONDITIONS FOR BIZGURUH APP")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          ' Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the https://www.bizguruh.com (the "Service") operated by BizGuruh ("us", "we", or "our").'
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "boldText" }, [
        _vm._v(
          "By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.\n"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You agree that you will use the BizGuruh App in compliance with these Agreements, and all applicable domestic and international laws, rules and regulations, including copyright laws."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "As a condition of your use of the BizGuruh App, you will not use the BizGuruh App in any manner intended to damage, disable, overburden, or impair any server or the network(s) connected to any server or to interfere with any other party's use and enjoyment of the App. You may not attempt to gain unauthorized access to the BizGuruh App, other accounts, computer systems, or networks connected to any server through hacking, password mining, or any other means. You may not obtain or attempt to obtain any materials or information stored on the BizGuruh App, its servers, or associated computers through any means not intentionally made available through the App. If you are a registered user, you will not share your password or let anyone else access or compromise your account."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You agree not to misrepresent or attempt to misrepresent your identity while using the App (although you are welcome and encouraged to use an anonymous username in the forums and to act in a manner that keeps your identity concealed)."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("1.\tSTRICTLY PROHIBITED ACTIVITIES")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Users are restrained from participating in the following activities on the App:"
        )
      ]),
      _vm._v(" "),
      _c("ul", { staticClass: "mb-4" }, [
        _c("li", [
          _vm._v("content that defames, harasses or threatens others;")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "content that discusses illegal activities with the intent to commit them;"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "content that infringes another's intellectual property, including, but not limited to, copyrights or trademarks;"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "Profane, pornographic, obscene, indecent or unlawful content;"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("Advertising or any form of commercial solicitation;")
        ]),
        _vm._v(" "),
        _c("li", [_vm._v("Content related to partisan political activities;")]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "Sending Viruses, trojan horses, worms, time bombs, corrupted files, malware, spyware, or any other similar software that may damage the operation of another's computer or property; and"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "Content that contains intentionally inaccurate information or that is posted with the intent of misleading others (this list, collectively, “Strictly Prohibited Items”).\n    "
          )
        ])
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You may not submit, post, publish, share, or otherwise distribute any of the above Strictly Prohibited Items on or via the BizGuruh App."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("2.\tUSER ACCOUNTS")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "In order to create a User Account, you must provide your full name, an email address, your country or region of residence, a public username, and a user password. You agree that you will never divulge or share access or access information for your User account with any third party for any reason. In setting up your user account, you may be prompted to enter additional optional information (e.g., your year of birth). You represent that all information provided by you is accurate and current. You agree to maintain and update your information to keep it accurate and current."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We care about the confidentiality and security of your personal information.\n    "
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("3.\tCOPYRIGHT PROTECTION")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "The content on the BizGuruh App is protected by copyright laws. Unless otherwise expressly stated on the BizGuruh App, the texts, work sheets, video, images, and other instructional materials provided with the courses offered on the App are for your personal use in connection with those courses only."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "By using the BizGuruh App, you agree to abide by all such rules and conditions."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "All rights in the App and its content, if not expressly granted, are reserved."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("4.\tTRADEMARKS")]),
      _vm._v(" "),
      _c("ol", [
        _c("li", [
          _c("p", { staticClass: "boldText" }, [
            _vm._v(
              "Use of and Other Member Names, Trademarks, and Service Marks"
            )
          ]),
          _vm._v(
            "\n The names, logos, and seals of the Members are Trademarks owned by the respective Member. You may not use any of these Trademarks, or any variations thereof, without the owner's prior written consent. You may not use any of these Trademarks, or any variations thereof, for promotional purposes, or in any way that deliberately or inadvertently claims, suggests or, in the owner’s sole judgment, gives the appearance or impression of a relationship with or endorsement by the owner.\nNothing contained on the BizGuruh App should be construed as granting, by   implication, estoppel, or otherwise, any license or right to use any Trademark displayed on the App without the written permission of the owner of the applicable Trademark.\nCopyright owners who believe their material has been infringed on the App should the administrator.\n    "
          )
        ])
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("5.\tINDEMNIFICATION")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You agree to defend, hold harmless, and indemnify BizGuruh and the BizGuruh Participants, and their respective subsidiaries, affiliates, officers, students, fellows, governing board members, agents and employees from and against any third-party claims, actions, or demands arising out of, resulting from or in any way related to your use of the BizGuruh App, including any liability or expense arising from any and all claims, losses, damages (actual and consequential), suits, judgments, litigation costs, and lawyers' fees, of every kind and nature. In such a case, BizGuruh or the applicable BizGuruh Participant will provide you with written notice of such claim, suit, or action."
        )
      ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("h3", [_vm._v("6.\tADDITIONAL TERMS")]),
      _vm._v(" "),
      _c("ol", [
        _c("li", { staticClass: "mb-2" }, [
          _c("p", { staticClass: "boldText" }, [
            _vm._v("Termination Rights; Discontinuation of Courses and Content")
          ]),
          _vm._v(
            "\nYou agree that, in its sole discretion, BizGuruh may terminate your use of the BizGuruh App or your participation in it, for any reason or no reason, upon notice to you. It is BizGuruh's policy to terminate in appropriate circumstances the accounts of users of the BizGuruh App who are repeat copyright infringers.  and the Participants reserve the right at any time in their sole discretion to cancel, delay, reschedule or alter the format of any course offered through BizGuruh, or to cease providing any part or all of the BizGuruh App content or related services, and you agree that neither BizGuruh nor any of the BizGuruh Participants will have any liability to you for such an action. If you no longer desire to participate in the BizGuruh App, you may terminate your participation at any time. The rights granted to you hereunder will terminate upon any termination of your right to use the BizGuruh App, but the other provisions of the Agreements will survive any such termination."
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "mb-2" }, [
          _c("p", { staticClass: "boldText" }, [_vm._v("Entire Agreement")]),
          _vm._v(
            "\nThe Agreements constitute the entire agreement between you and with respect to your use of the BizGuruh App, superseding any prior agreements between you and regarding your use of the BizGuruh App."
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "mb-2" }, [
          _c("p", { staticClass: "boldText" }, [
            _vm._v("Waiver and Severability")
          ]),
          _vm._v(
            "\nThe failure of BizGuruh to exercise or enforce any right or provision of the Agreement shall not constitute a waiver of such right or provision. If any provision of the Agreement is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision and the other provisions of the Agreement shall remain in full force and effect."
          )
        ]),
        _vm._v(" "),
        _c("li", { staticClass: "mb-2" }, [
          _c("p", { staticClass: "boldText" }, [
            _vm._v("Choice of Law/Forum Selection")
          ]),
          _vm._v(
            "\nYou agree that the Agreement and any claim or dispute arising out of or relating to the Agreement or any content or service obtained from or through the BizGuruh App will be governed by the laws of the Federal Republic of Nigeria. You agree that all such claims and disputes will be heard and resolved exclusively in the federal or state courts located in and serving Lagos, Nigeria. You consent to the personal jurisdiction of those courts over you for this purpose, and you waive and agree not to assert any objection to such proceedings in those courts (including any defense or objection of lack of proper jurisdiction or venue or inconvenience of forum)."
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0b31cece", module.exports)
  }
}

/***/ }),

/***/ 605:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1630)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1632)
/* template */
var __vue_template__ = __webpack_require__(1633)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0b31cece"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/termsAndConditionComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0b31cece", Component.options)
  } else {
    hotAPI.reload("data-v-0b31cece", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});