webpackJsonp([169],{

/***/ 1261:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1262);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("f4d415d0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-16ddbd77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminOrderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-16ddbd77\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminOrderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1262:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-16ddbd77] {\n    height: 100vh;\n}\n.container[data-v-16ddbd77] {\n    width: 100%;\n}\n.content-wrapper[data-v-16ddbd77] {\n    padding:30px 20px;\n}\nth[data-v-16ddbd77] {\n    background-color: #f3f3f3;\n    color: #000000;\n    font-weight: bold;\n}\nth[data-v-16ddbd77], td[data-v-16ddbd77] {\n    padding: 30px !important;\n}\ntd[data-v-16ddbd77] {\n    color: #aaaaaa;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-order-component",
    data: function data() {
        return {
            token: '',
            orders: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.token = admin.access_token;
        if (admin != null) {
            axios.get(' /api/order', { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this.orders = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    }
});

/***/ }),

/***/ 1264:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wrapper skin-blue" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "main-page" }, [
        _c("table", { staticClass: "table" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.orders, function(order, index) {
              return _vm.orders.length > 0
                ? _c("tr", [
                    _c("td", [_vm._v(_vm._s(index + 1))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(order.orderNum))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(order.referenceNo))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(order.paymentType))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(order.totalAmount))]),
                    _vm._v(" "),
                    _vm._m(1, true)
                  ])
                : _vm._e()
            }),
            0
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("th", [_vm._v("SN")]),
      _vm._v(" "),
      _c("th", [_vm._v("Order Num")]),
      _vm._v(" "),
      _c("th", [_vm._v("Reference No")]),
      _vm._v(" "),
      _c("th", [_vm._v("Payment Method")]),
      _vm._v(" "),
      _c("th", [_vm._v("Total Amount")]),
      _vm._v(" "),
      _c("th")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [_c("i", { staticClass: "fa fa-arrow-right" })])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-16ddbd77", module.exports)
  }
}

/***/ }),

/***/ 525:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1261)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1263)
/* template */
var __vue_template__ = __webpack_require__(1264)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-16ddbd77"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminOrderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-16ddbd77", Component.options)
  } else {
    hotAPI.reload("data-v-16ddbd77", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});