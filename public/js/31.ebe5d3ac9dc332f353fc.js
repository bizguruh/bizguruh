webpackJsonp([31],{

/***/ 1277:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1278);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("21b875c0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68802e68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminMainComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-68802e68\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminMainComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1278:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.wrapper[data-v-68802e68] {\n  background-color: #e3ecf4;\n}\n.content-wrapper[data-v-68802e68] {\n        height: 100vh;\n        width: 100%;\n        margin-left: 0 !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 1279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__adminSidebarComponent__ = __webpack_require__(1280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__adminSidebarComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__adminSidebarComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__adminMainHeaderComponent__ = __webpack_require__(1285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__adminMainHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__adminMainHeaderComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__adminFooterComponent__ = __webpack_require__(1290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__adminFooterComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__adminFooterComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "admin-main-component",
  components: {
    "app-header": __WEBPACK_IMPORTED_MODULE_1__adminMainHeaderComponent___default.a,
    "app-sidebar": __WEBPACK_IMPORTED_MODULE_0__adminSidebarComponent___default.a,
    "app-footer": __WEBPACK_IMPORTED_MODULE_2__adminFooterComponent___default.a
  }
});

/***/ }),

/***/ 1280:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1281)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1283)
/* template */
var __vue_template__ = __webpack_require__(1284)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-01e1bc29"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminSidebarComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-01e1bc29", Component.options)
  } else {
    hotAPI.reload("data-v-01e1bc29", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1281:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1282);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7dbdfb86", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-01e1bc29\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSidebarComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-01e1bc29\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSidebarComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1282:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-sidebar[data-v-01e1bc29] {\n  width:30%;\n  max-height: 100vh;\n  overflow-y: scroll;\n  background-color: rgba(0,0,0 .74);\n}\n.sidebar ul li a[data-v-01e1bc29] {\n    font-size: .8em;\n}\n", ""]);

// exports


/***/ }),

/***/ 1283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-sidebar-component",
    data: function data() {
        return {
            webAdmin: false,
            storeAdmin: false
        };
    },
    mounted: function mounted() {
        if (localStorage.getItem('authAdmin')) {
            var admin = JSON.parse(localStorage.getItem('authAdmin'));
            console.log(admin);
            if (admin.role == 0) {
                this.webAdmin = this.storeAdmin = true;
            } else if (admin.role == 0 || admin.role == 1) {
                this.webAdmin = true;
            } else if (admin.role == 0 || admin.role == 2) {
                this.storeAdmin = true;
            }
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        logout: function logout() {
            localStorage.removeItem('authAdmin');
            this.$router.push('/admin/auth/manage');
        }
    }
});

/***/ }),

/***/ 1284:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("aside", { staticClass: "main-sidebar" }, [
    _c("section", { staticClass: "sidebar" }, [
      _c(
        "ul",
        { staticClass: "sidebar-menu", attrs: { "data-widget": "tree" } },
        [
          _c(
            "li",
            [
              _c(
                "router-link",
                { attrs: { tag: "a", to: "/admin/dashboard" } },
                [
                  _c("span", [_vm._v("Dashboard")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "pull-right-container" })
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/products" } },
                    [_c("span", [_vm._v("All Products")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "bizguruhProduct" }, tag: "a" } },
                    [_vm._v("Bizguruh Products")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "adminDraftProduct" }, tag: "a" } },
                    [_vm._v("Bizguruh Products(Draft)")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/univerify-products" } },
                    [_c("span", [_vm._v("Products (UnVerified)")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin && _vm.webAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: { name: "adminManage" } } },
                    [_vm._v("Manage Admin")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin && _vm.webAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: { name: "adminQuestionBank" } } },
                    [_vm._v("Upload Question")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: { name: "SubscriptionPage" } } },
                    [_vm._v("Subscription")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: { name: "checklistPage" } } },
                    [_vm._v("Checklist Questions")]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/orders" } },
                    [_c("span", [_vm._v("Order")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin && _vm.webAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/product-type" } },
                    [_c("span", [_vm._v("Product Type")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.webAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/category" } },
                    [_c("span", [_vm._v("Category")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/reviews" } },
                    [_c("span", [_vm._v(" Reviews")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/users" } },
                    [_c("span", [_vm._v("Users")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c(
                    "router-link",
                    { attrs: { tag: "a", to: "/admin/vendors" } },
                    [_c("span", [_vm._v("Vendor")])]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.storeAdmin
            ? _c(
                "li",
                [
                  _c("router-link", { attrs: { tag: "a", to: "/admin/ops" } }, [
                    _c("span", [_vm._v("Ops Users")])
                  ])
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "#" } }, [
              _c(
                "span",
                {
                  on: {
                    click: function($event) {
                      return _vm.logout()
                    }
                  }
                },
                [_vm._v("Logout")]
              )
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "#" } }, [
        _c("span", [_vm._v("Admin Configuration")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "#" } }, [_c("span", [_vm._v("Settings")])])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-01e1bc29", module.exports)
  }
}

/***/ }),

/***/ 1285:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1286)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1288)
/* template */
var __vue_template__ = __webpack_require__(1289)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a2fac402"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminMainHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a2fac402", Component.options)
  } else {
    hotAPI.reload("data-v-a2fac402", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1286:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1287);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7aeae236", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a2fac402\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminMainHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a2fac402\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminMainHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1287:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-header[data-v-a2fac402]{\n  -webkit-box-shadow:  0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow:  0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "adim-main-header-component"
});

/***/ }),

/***/ 1289:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "main-header w-100 bg-white" }, [
      _c("a", { staticClass: "logo", attrs: { href: "index2.html" } }, [
        _c("span", { staticClass: "logo-mini" }, [
          _c("b", [_vm._v("B")]),
          _vm._v("SHOP")
        ]),
        _vm._v(" "),
        _c("span", { staticClass: "logo-lg" }, [
          _c("b", [_vm._v("B")]),
          _vm._v("SHOP")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a2fac402", module.exports)
  }
}

/***/ }),

/***/ 1290:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1291)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1293)
/* template */
var __vue_template__ = __webpack_require__(1294)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-71a01c2a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminFooterComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-71a01c2a", Component.options)
  } else {
    hotAPI.reload("data-v-71a01c2a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1291:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1292);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("570487f7", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71a01c2a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminFooterComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71a01c2a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminFooterComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-footer[data-v-71a01c2a] {\n    background: #fff;\n    padding: 15px;\n    color: #444;\n    border-top: 1px solid #d2d6de;\n    position: absolute;\n    bottom: 0;\n    width: 100%;\n    margin: 0;\n    margin-top: 40px;\n    text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-footer-component"
});

/***/ }),

/***/ 1294:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "main-footer" }, [
      _c("div", { staticClass: "pull-right hidden-xs" }),
      _vm._v(" "),
      _c("strong", [
        _vm._v("Copyright © 2018 "),
        _c("a", { attrs: { href: "https://adminlte.io" } }, [_vm._v("B SHOP")]),
        _vm._v(".")
      ]),
      _vm._v(" All rights\n    reserved.\n")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-71a01c2a", module.exports)
  }
}

/***/ }),

/***/ 1295:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "wrapper skin-blue d-flex flex-column" },
    [
      _c("app-header"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "d-flex" },
        [
          _c("app-sidebar"),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "d-flex flex-column w-100 align-center" },
            [_c("router-view"), _vm._v(" "), _c("app-footer")],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68802e68", module.exports)
  }
}

/***/ }),

/***/ 529:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1277)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1279)
/* template */
var __vue_template__ = __webpack_require__(1295)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-68802e68"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminMainComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68802e68", Component.options)
  } else {
    hotAPI.reload("data-v-68802e68", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});