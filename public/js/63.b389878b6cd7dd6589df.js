webpackJsonp([63],{

/***/ 1078:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1079);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("fa037830", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8559a98e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./mainVendorPageComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8559a98e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./mainVendorPageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1079:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1080:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__vendorDashboardComponent__ = __webpack_require__(1081);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__vendorDashboardComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__vendorDashboardComponent__);
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['inboxCount'],
  name: "main-vendor-page-component",
  components: {
    "app-dashboard": __WEBPACK_IMPORTED_MODULE_0__vendorDashboardComponent___default.a
  }
});

/***/ }),

/***/ 1081:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1082)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1084)
/* template */
var __vue_template__ = __webpack_require__(1085)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-26c475f5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorDashboardComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-26c475f5", Component.options)
  } else {
    hotAPI.reload("data-v-26c475f5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1082:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1083);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("765f4652", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-26c475f5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorDashboardComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-26c475f5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorDashboardComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1083:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-26c475f5] {\n        position: relative;\n            min-height: 100vh;\n}\n    /* .r3_counter_box {\n    display: flex;\n    justify-content: space-between;\n} */\n.new_message[data-v-26c475f5]{\n      font-size:14px;\n}\nsmall[data-v-26c475f5]{\n      font-weight:bold;\n}\nh5[data-v-26c475f5],h4[data-v-26c475f5]{\n      font-weight: normal;\n}\na[data-v-26c475f5]{\n      color: hsl(207, 43%, 20%) !important;\n}\np[data-v-26c475f5]{\n      color:rgba(0, 0, 0, 0.64) !important;\n}\n.row-one[data-v-26c475f5]{\n      display: grid;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: start;\n          -ms-flex-align: start;\n              align-items: flex-start;\n      grid-row-gap: 20px;\n      grid-column-gap: 20px;\n      grid-template-columns: 20% 60% 20%;\n      padding:0 20px;\n}\n.col_3[data-v-26c475f5]{\n      display:-webkit-box;\n      display:-ms-flexbox;\n      display:flex;\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between;\n      -webkit-box-align: start;\n          -ms-flex-align: start;\n              align-items: flex-start;\n      margin-bottom: 20px;\n}\n.row-2[data-v-26c475f5]{\n    display: grid;\n    -webkit-box-pack: normal;\n        -ms-flex-pack: normal;\n            justify-content: normal;\n    -webkit-box-align: start;\n        -ms-flex-align: start;\n            align-items: flex-start;\n  \n    grid-template-rows: 33.3% 33.3% 33.3%;\n    height: 100%;\n}\n.row-3[data-v-26c475f5]{\n    display: grid;\n    -webkit-box-pack: normal;\n        -ms-flex-pack: normal;\n            justify-content: normal;\n    -webkit-box-align: start;\n        -ms-flex-align: start;\n            align-items: flex-start;\n    grid-row-gap: 20px;\n    grid-template-rows:auto;\n}\n.r3_counter_box1 .fa[data-v-26c475f5] {\n    margin-right: 0px;\n    width: 63.4px;\n    height: 63.4px;\n    text-align: center;\n    line-height: 63.4px;\n    -webkit-transition: 0.5s all;\n    transition: 0.5s all;\n    cursor:pointer;\n}\n.schedule_2[data-v-26c475f5] {\n  background-color: white;\n  padding: 15px;\n  margin: 0;\n  text-align: center;\n}\n.content-top-1[data-v-26c475f5] {\n  text-align: center;\n    padding:1.4em .7em;\n    margin-bottom:0;\n}\n.content-top[data-v-26c475f5]{\n  margin-bottom:0;\n}\n.content-top-1:hover .r3_counter_box1 .fa[data-v-26c475f5]{\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1);\n}\n.faded[data-v-26c475f5]{\n  cursor:not-allowed!important;\n  color: rgba(255, 255, 255, 0.4);\n}\n.fa-1x[data-v-26c475f5]{\n  font-size:2em !important;\n}\n.widget[data-v-26c475f5]:hover {\n  background-color: rgba(0, 0, 0, 0.4);\n  cursor: pointer;\n}\n.bg-biz[data-v-26c475f5] {\n  font-weight: bold;\n  font-size: 18px;\n  padding: 20px;\n  background-color: #a4c2db;\n  text-align: center;\n}\n.mobile[data-v-26c475f5] {\n  display: none !important;\n}\n.row-one .content-top-2.card[data-v-26c475f5] {\n    width: 100%;\n    overflow: hidden;\n}\n@media (max-width: 768px) {\n.widget[data-v-26c475f5] {\n    width: 100%;\n}\n.mobile[data-v-26c475f5] {\n    display: block !important;\n}\n.desktop[data-v-26c475f5] {\n    display: none !important;\n}\n.agileinfo-cdr[data-v-26c475f5] {\n    padding: 10px 0;\n}\n.row-one[data-v-26c475f5]{\n      grid-template-columns: 100%;\n      padding: 0;\n}\n.row-1[data-v-26c475f5]{\n    margin-bottom:20px;\n}\n}\n@media (max-width: 425px) {\n#page-wrapper[data-v-26c475f5]{\n    padding: 15px;\n}\n.schedule_2[data-v-26c475f5]{\n    text-align: center;\n}\n.col_3[data-v-26c475f5]{\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.widget[data-v-26c475f5]{\n  margin: 0 0 20px ;\n}\n.content-top-1[data-v-26c475f5]{\n  margin-bottom: 20px;\n}\n.row-one[data-v-26c475f5]{\n  margin-top: 0;\n}\n.row-1[data-v-26c475f5]{\n  margin-bottom: 0;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1084:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "vendorDashboardComponent",
  props: ['inboxCount', 'vendorSub'],
  data: function data() {
    return {
      names: ["sales", "products", "orders"],
      values: [[0, 10], [0, 40], [0, 30]],
      productCount: 0,
      products: [],
      followNo: 0,
      allLikes: [],
      reach: [],
      visits: [],
      content: false,
      revenue: true,
      activity: false,
      weeklyReach: [],
      totalVisits: 0,
      day: "Monday",
      response: {
        first: "",
        second: ""
      },
      submitContenDone: false,
      daydone: false,
      uploadDay: "",
      showChecklist: false,
      isActive: true

    };
  },
  mounted: function mounted() {
    this.getDetails();
    this.getDayForUpload();
  },

  methods: {
    calcRevenue: function calcRevenue(value) {

      return Math.floor(value / 200) * 10;
    },
    editDay: function editDay() {
      this.daydone = !this.daydone;
    },
    submitDay: function submitDay() {
      var _this = this;

      var vendor = JSON.parse(localStorage.getItem("authVendor"));
      var data = {
        vendor_id: vendor.id,
        day: this.day
      };
      var conf = confirm("Are you sure?");

      if (conf) {
        axios.post('/api/upload-day', data).then(function (response) {
          if (response.status === 200) {
            _this.getDayForUpload();
            _this.$toasted.success(" successful");

            _this.daydone = true;
          }
        });
      }
    },
    getDayForUpload: function getDayForUpload() {
      var _this2 = this;

      var vendor = JSON.parse(localStorage.getItem("authVendor"));
      axios.get('/api/get-day/' + vendor.id).then(function (response) {
        if (response.status === 200) {
          if (response.data !== null) {
            _this2.uploadDay = response.data.day;
            _this2.daydone = true;
          } else {
            _this2.daydone = false;
          }
          _this2.isActive = false;
        }
      });
    },
    submitCheck: function submitCheck() {
      var _this3 = this;

      var vendor = JSON.parse(localStorage.getItem("authVendor"));
      var data = {
        vendor_id: vendor.id,
        response: this.response
      };
      var conf = confirm("Are you sure?");

      if (conf) {
        axios.post('/api/add-checklist-vendor', data).then(function (response) {
          if (response.status === 200) {
            _this3.$toasted.success("submission successful");
            _this3.submitContenDone = true;
          }
        });
      }
    },
    showCharts: function showCharts(type) {
      switch (type) {
        case "content":
          this.names = ["Active Product", "Pending Product", "Draft Product"];
          this.values = [[0, this.activeProduct.length], [0, Number(this.productCount) - Number(this.activeProduct.length)], [0, 0]];
          this.content = true;
          this.revenue = this.activity = false;

          break;
        case "revenue":
          this.names = ["Revenue"];
          this.values = [[0, 20, 30, 25, 40]];
          this.revenue = true;
          this.content = this.activity = false;

          break;
        case "activity":
          this.names = ["Profile Visits", "Reach"];
          this.values = [[0, this.visits.length], [0, this.weeklyReach.length]];
          this.activity = true;
          this.content = this.revenue = false;

          break;

        default:
          break;
      }
    },
    getDetails: function getDetails() {
      var _this4 = this;

      var reducer = function reducer(accumulator, currentValue) {
        return accumulator + currentValue;
      };

      var currentWeek = this.$moment().week();
      var currentMonth = this.$moment().month();

      var vendor = JSON.parse(localStorage.getItem("authVendor"));
      if (vendor != null) {
        var vendorid = vendor.id;
        this.token = vendor.access_token;
        axios.get('/api/vendorproduct/' + vendorid, {
          headers: { Authorization: 'Bearer ' + vendor.access_token }
        }).then(function (response) {
          if (response.data.data.length > 0) {
            _this4.products = response.data.data;
            _this4.productCount = _this4.products.length;
          }
        }).catch(function (error) {
          console.log(error);
        });

        axios.get('/api/get-vendor-likes/' + vendor.id).then(function (response) {
          _this4.allLikes = response.data;
        });

        axios.get('/api/getfollowers/' + vendor.id).then(function (response) {
          if (response.status === 200) {
            _this4.followNo = response.data;
          }
        });

        axios.get('/api/get-reach/' + vendor.id).then(function (response) {
          _this4.reach = response.data;

          _this4.reach.forEach(function (item) {
            if (_this4.$moment(item.created_at).week() === currentWeek) {
              _this4.weeklyReach.push(item);
            }
          });
        });
        var weekVisit = [];
        axios.get('/api/get-activity/' + vendor.id).then(function (response) {
          if (response.status === 200) {
            response.data.forEach(function (item) {
              if (_this4.$moment(item.created_at).week() === currentWeek) {
                _this4.visits.push(item);
              }
            });
            _this4.visits.forEach(function (item) {
              weekVisit.push(item.profile_visits);
            });
            _this4.totalVisits = weekVisit.reduce(reducer, 0);
          }
        });
      } else {
        this.$router.push("/vendor/auth");
      }
    }
  },
  computed: {
    myLikes: function myLikes() {
      return this.allLikes.filter(function (item) {
        if (item.like == 1) {
          return item;
        }
      });
    },
    activeProduct: function activeProduct() {
      return this.products.filter(function (item) {
        if (item.verify == 1) {
          return item;
        }
      });
    }
  }
});

/***/ }),

/***/ 1085:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", [
          _c("div", { staticClass: "col_3" }, [
            _c(
              "div",
              {
                staticClass: " widget widget1",
                on: {
                  click: function($event) {
                    return _vm.showCharts("revenue")
                  }
                }
              },
              [
                _c("div", { staticClass: "r3_counter_box" }, [
                  _c("i", {
                    staticClass: "pull-left fa fa-dollar icon-rounded"
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "stats" }, [
                    _c("h5", [
                      _vm._v("\n            Total Revenue \n           ")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "bold" }, [
                      _vm._v(
                        " $" + _vm._s(_vm.calcRevenue(Number(_vm.reach.length)))
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("hr")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: " widget widget1",
                on: {
                  click: function($event) {
                    return _vm.showCharts("content")
                  }
                }
              },
              [
                _c("div", { staticClass: "r3_counter_box" }, [
                  _c("i", {
                    staticClass:
                      "pull-left fa fa-shopping-bag user2 icon-rounded"
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "stats" }, [
                    _c("h5", [_vm._v("\n           Resources\n           ")]),
                    _vm._v(" "),
                    _c("span", {}, [
                      _vm._v(
                        "\n             " +
                          _vm._s(_vm.activeProduct.length) +
                          " Active\n               "
                      )
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "ml-2 " }, [
                      _vm._v(
                        _vm._s(_vm.productCount - _vm.activeProduct.length) +
                          " Pending"
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("hr")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: " widget",
                on: {
                  click: function($event) {
                    return _vm.showCharts("activity")
                  }
                }
              },
              [
                _c("div", { staticClass: "r3_counter_box" }, [
                  _c("i", {
                    staticClass: "pull-left fa fa-calculator user1 icon-rounded"
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "stats" }, [
                    _c("h5", [
                      _vm._v("\n             Activities\n           ")
                    ]),
                    _vm._v(" "),
                    _c(
                      "span",
                      { staticClass: "d-flex justify-content-between" },
                      [
                        _c("span", { staticClass: "mr-2" }, [
                          _vm._v(
                            "\n               " +
                              _vm._s(_vm.totalVisits) +
                              "\n               "
                          ),
                          _c("small", [_vm._v("Profile visit(s)")])
                        ]),
                        _vm._v(" "),
                        _c("span", [
                          _vm._v(
                            "\n               " +
                              _vm._s(_vm.weeklyReach.length) +
                              "\n               "
                          ),
                          _c("small", [_vm._v("Reach this week")])
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("hr")
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row-one widgettable" }, [
            _c("div", { staticClass: "row-2 stat" }, [
              _c("div", { staticClass: "content-top-1" }, [
                _c(
                  "div",
                  { staticClass: "r3_counter_box1" },
                  [
                    _c("router-link", { attrs: { to: "all" } }, [
                      _c("i", {
                        staticClass:
                          " fa fa-shopping-bag user2 icon-rounded  m-2 fa-1x"
                      })
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "stats" }, [
                  _c(
                    "h4",
                    [
                      _c("router-link", { attrs: { to: "all" } }, [
                        _vm._v("View Resources")
                      ])
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "content-top-1" }, [
                _c(
                  "div",
                  { staticClass: "r3_counter_box1" },
                  [
                    _c("router-link", { attrs: { to: "add/category" } }, [
                      _c("i", {
                        staticClass:
                          " fa fa-pencil dollar2 icon-rounded m-2 fa-1x"
                      })
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "stats" }, [
                  _c(
                    "h4",
                    [
                      _c("router-link", { attrs: { to: "add/category" } }, [
                        _vm._v("Create Resource")
                      ])
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "content-top-1" }, [
                _c(
                  "div",
                  { staticClass: "r3_counter_box1" },
                  [
                    _c("router-link", { attrs: { to: "all/draft-product" } }, [
                      _c("i", {
                        staticClass:
                          " fa fa-archive dollar1 icon-rounded m-2  fa-1x"
                      })
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "stats" }, [
                  _c(
                    "h4",
                    [
                      _c(
                        "router-link",
                        { attrs: { to: "all/draft-product" } },
                        [_vm._v("Draft Resources")]
                      )
                    ],
                    1
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row-1 content-top-2 card" }, [
              _c("div", { staticClass: "agileinfo-cdr" }, [
                _c("div", { staticClass: "card-header" }, [
                  _vm.content
                    ? _c("h3", [_vm._v("Weekly Resource Chart")])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.revenue
                    ? _c("h3", [_vm._v("Weekly Revenue Chart")])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.activity
                    ? _c("h3", [_vm._v("Weekly Activity Chart")])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "desktop" },
                  [
                    _c(
                      "graph-line",
                      {
                        attrs: {
                          width: 600,
                          height: 400,
                          shape: "step",
                          "axis-min": 0,
                          "axis-max": 50,
                          "axis-full-mode": true,
                          labels: ["", "Activities"],
                          names: _vm.names,
                          values: _vm.values
                        }
                      },
                      [
                        _c("note", { attrs: { text: "Weekly Activities" } }),
                        _vm._v(" "),
                        _c("tooltip", {
                          attrs: { names: _vm.names, position: "right" }
                        }),
                        _vm._v(" "),
                        _c("legends", { attrs: { names: _vm.names } }),
                        _vm._v(" "),
                        _c("guideline", { attrs: { "tooltip-y": true } })
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "mobile" },
                  [
                    _c(
                      "graph-line",
                      {
                        attrs: {
                          width: 330,
                          height: 270,
                          shape: "step",
                          "axis-min": 0,
                          "axis-max": 50,
                          "axis-full-mode": true,
                          labels: ["", "Activities"],
                          names: _vm.names,
                          values: _vm.values
                        }
                      },
                      [
                        _c("note", { attrs: { text: "Weekly Activities" } }),
                        _vm._v(" "),
                        _c("tooltip", {
                          attrs: { names: _vm.names, position: "right" }
                        }),
                        _vm._v(" "),
                        _c("legends", { attrs: { names: _vm.names } }),
                        _vm._v(" "),
                        _c("guideline", { attrs: { "tooltip-y": true } })
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row-2 stat" }, [
              _c("div", { staticClass: "content-top-1" }, [
                _c("ul", { staticClass: "info" }, [
                  _c("li", { staticClass: "col-md-6" }, [
                    _c("p", [_vm._v(_vm._s(_vm.followNo))]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Followers")])
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "col-md-6" }, [
                    _c("p", [_vm._v(_vm._s(_vm.myLikes.length))]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Likes")])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "clearfix" })
                ]),
                _vm._v(" "),
                _c("hr")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "content-top-1" }, [
                _c("div", { staticClass: "schedule_2 " }, [
                  _c("div", { staticClass: "p-2 py-2 border-bottom" }, [
                    _vm._m(1),
                    _vm._v(" "),
                    !_vm.daydone
                      ? _c(
                          "div",
                          {
                            staticClass: "p-2 bg-white mt-2 rounded animated",
                            class: { fadeOut: _vm.daydone }
                          },
                          [
                            _c("div", { staticClass: "form-group" }, [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.day,
                                      expression: "day"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.day = $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    }
                                  }
                                },
                                [
                                  _c("option", [_vm._v("Sunday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Monday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Tuesday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Wednesday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Thursday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Friday")]),
                                  _vm._v(" "),
                                  _c("option", [_vm._v("Saturday")])
                                ]
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex justify-content-between" },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn-default btn-sm ml-auto",
                                    attrs: { type: "button" },
                                    on: { click: _vm.editDay }
                                  },
                                  [_vm._v("Cancel")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn-primary btn-sm ml-auto",
                                    attrs: { type: "button" },
                                    on: { click: _vm.submitDay }
                                  },
                                  [_vm._v("Set")]
                                )
                              ]
                            )
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.daydone
                      ? _c("div", [
                          _vm._v(
                            "\n                " +
                              _vm._s(_vm.uploadDay) +
                              "\n               "
                          ),
                          _c("i", {
                            staticClass: "fas fa-edit",
                            on: { click: _vm.editDay }
                          })
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _vm.showChecklist && !_vm.submitContenDone
                    ? _c(
                        "div",
                        {
                          staticClass: "p-2 mt-3 animated",
                          class: { fadeOut: _vm.submitContenDone }
                        },
                        [
                          _vm._m(2),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "p-2 text-left bg-white rounded",
                              staticStyle: { "font-size": "14px" }
                            },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _vm._v(
                                  "\n                 I have uploaded my insight for the week\n                 "
                                ),
                                _c("div", { staticClass: "form-check" }, [
                                  _c(
                                    "label",
                                    { staticClass: "form-check-label" },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.response.first,
                                            expression: "response.first"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          type: "radio",
                                          value:
                                            " I have uploaded my insight for the week - yes",
                                          "aria-label":
                                            "  I have uploaded my insight for the week"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.response.first,
                                            " I have uploaded my insight for the week - yes"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.response,
                                              "first",
                                              " I have uploaded my insight for the week - yes"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(
                                        "\n                     Yes\n                   "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    { staticClass: "form-check-label" },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.response.first,
                                            expression: "response.first"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          type: "radio",
                                          value:
                                            " I have uploaded my insight for the week - no",
                                          "aria-label":
                                            "  I have uploaded my insight for the week"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.response.first,
                                            " I have uploaded my insight for the week - no"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.response,
                                              "first",
                                              " I have uploaded my insight for the week - no"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(
                                        "\n                     No\n                   "
                                      )
                                    ]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _vm._v(
                                  "\n                 I have shared at least one post about BizGuruh with my community\n                 "
                                ),
                                _c("div", { staticClass: "form-check" }, [
                                  _c(
                                    "label",
                                    { staticClass: "form-check-label" },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.response.second,
                                            expression: "response.second"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          type: "radio",
                                          value:
                                            " I have shared at least one post about BizGuruh with my community - Yes",
                                          "aria-label":
                                            " I have shared at least one post about BizGuruh with my community "
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.response.second,
                                            " I have shared at least one post about BizGuruh with my community - Yes"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.response,
                                              "second",
                                              " I have shared at least one post about BizGuruh with my community - Yes"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(
                                        "\n                     Yes\n                   "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    { staticClass: "form-check-label" },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.response.second,
                                            expression: "response.second"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          type: "radio",
                                          value:
                                            " I have shared at least one post about BizGuruh with my community - No",
                                          "aria-label": " "
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.response.second,
                                            " I have shared at least one post about BizGuruh with my community - No"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.response,
                                              "second",
                                              " I have shared at least one post about BizGuruh with my community - No"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(
                                        "\n                     No\n                   "
                                      )
                                    ]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", {}, [
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn-primary btn-sm ml-auto",
                                    attrs: { type: "button" },
                                    on: { click: _vm.submitCheck }
                                  },
                                  [_vm._v("Submit")]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "content-top-1" },
                [
                  _vm.vendorSub > 1
                    ? _c("router-link", { attrs: { to: "vendor/messages" } }, [
                        _c("i", { staticClass: "fas fa-inbox fa-3x " })
                      ])
                    : _c("i", { staticClass: "fas fa-inbox fa-3x faded" }),
                  _vm._v(" "),
                  _vm.vendorSub > 1
                    ? _c("div", { staticClass: "stats" }, [
                        _c(
                          "h4",
                          [
                            _c(
                              "router-link",
                              { attrs: { to: "vendor/messages" } },
                              [_vm._v("Messages")]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.inboxCount > 0
                          ? _c(
                              "p",
                              {
                                staticClass:
                                  "animated flash inifinite slower new_message"
                              },
                              [
                                _c("span", { staticClass: "text-red" }, [
                                  _vm._v(_vm._s(_vm.inboxCount))
                                ]),
                                _vm._v(" new group message(s)")
                              ]
                            )
                          : _vm._e()
                      ])
                    : _c("div", { staticClass: "stats" }, [
                        _c(
                          "h4",
                          [
                            _c(
                              "router-link",
                              { staticClass: "faded", attrs: { to: "" } },
                              [_vm._v("Messages")]
                            )
                          ],
                          1
                        )
                      ]),
                  _vm._v(" "),
                  _c("hr")
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _vm._m(4)
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("small", [
      _c(
        "b",
        { staticClass: "text-muted", staticStyle: { "line-height": "1.2" } },
        [_vm._v("Upload day")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("b", [
      _c("small", { staticClass: "mb-2 text-muted" }, [
        _vm._v(
          "Checklists— to monitor adherence to partner commitment (Available fridays & saturdays)"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "charts" }, [
      _c("div", { staticClass: "clearfix" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col_1" }, [
      _c("div", { staticClass: "clearfix" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-26c475f5", module.exports)
  }
}

/***/ }),

/***/ 1086:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("app-dashboard", { attrs: { inboxCount: _vm.inboxCount } })
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8559a98e", module.exports)
  }
}

/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1078)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1080)
/* template */
var __vue_template__ = __webpack_require__(1086)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8559a98e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/mainVendorPageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8559a98e", Component.options)
  } else {
    hotAPI.reload("data-v-8559a98e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});