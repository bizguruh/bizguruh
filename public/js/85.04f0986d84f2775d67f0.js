webpackJsonp([85],{

/***/ 1194:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1195);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("6ce433d1", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37de1712\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsAuthComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37de1712\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsAuthComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1195:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.form-control-feedback[data-v-37de1712] {\n    position: absolute !important;\n    top: 0 !important;\n    right: 0;\n    z-index: 2;\n    display: block !important;\n    width: 34px;\n    height: 34px;\n    line-height: 34px !important;\n    text-align: center;\n    pointer-events: none;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "ops-auth-component",
    data: function data() {
        return {
            user: {
                email: null,
                password: null
            }
        };
    },
    methods: {
        login: function login(user) {
            var _this = this;

            var data = {
                client_id: 2,
                client_secret: 'UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C',
                grant_type: 'password',
                username: user.email,
                password: user.password,
                theNewProvider: 'ops_user'
            };
            var authOps = {};
            console.log(JSON.parse(JSON.stringify(user)));
            axios.post('oauth/token', data).then(function (response) {
                authOps.access_token = response.data.access_token;
                authOps.refresh_token = response.data.refresh_token;
                localStorage.setItem('authOps', JSON.stringify(authOps));
                console.log(response.data.access_token);
                axios.get('api/opsDetails', { headers: Object(__WEBPACK_IMPORTED_MODULE_0__config__["d" /* getOpsHeader */])() }).then(function (res) {
                    if (res.data.verified) {
                        authOps.id = res.data.id;
                        authOps.email = res.data.email;
                        localStorage.setItem('authOps', JSON.stringify(authOps));
                        _this.$router.push('/ops/dashboard');
                        _this.$toasted.success("Successfully login");
                    } else {
                        _this.$toasted.error('Please verified your account');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }).catch(function (error) {
                console.log(error);
                _this.$toasted.error("Invalid user credential");
            });
        }
    }
});

/***/ }),

/***/ 1197:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "loginStyle" }, [
    _c("div", { staticClass: "login-box" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "login-box-body" }, [
        _c("p", { staticClass: "login-box-msg" }, [
          _vm._v("Sign in to start your session")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group has-feedback" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.user.email,
                expression: "user.email"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "email", placeholder: "Email" },
            domProps: { value: _vm.user.email },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.user, "email", $event.target.value)
              }
            }
          }),
          _vm._v(" "),
          _c("i", { staticClass: "fa fa-envelope form-control-feedback" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group has-feedback" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.user.password,
                expression: "user.password"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "password", placeholder: "Password" },
            domProps: { value: _vm.user.password },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.user, "password", $event.target.value)
              }
            }
          }),
          _vm._v(" "),
          _c("i", { staticClass: "fa fa-lock form-control-feedback" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "col-xs-4" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary btn-block btn-flat",
                attrs: { type: "submit" },
                on: {
                  click: function($event) {
                    return _vm.login(_vm.user)
                  }
                }
              },
              [_vm._v("Sign In")]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "login-logo" }, [
      _c("b", [_vm._v("B")]),
      _vm._v("SHOP\n            "),
      _c("small", { staticClass: "text-center" }, [_vm._v("Operations")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xs-8" }, [
      _c("div", { staticClass: "checkbox icheck" }, [
        _c("label", [
          _c("input", { attrs: { type: "checkbox" } }),
          _vm._v(" Remember Me\n                            ")
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-37de1712", module.exports)
  }
}

/***/ }),

/***/ 512:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1194)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1196)
/* template */
var __vue_template__ = __webpack_require__(1197)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-37de1712"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/operation/components/opsAuthComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-37de1712", Component.options)
  } else {
    hotAPI.reload("data-v-37de1712", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ })

});