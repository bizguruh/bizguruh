webpackJsonp([96],{

/***/ 1119:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1120);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("8c5a6058", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-18cab25f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorConfigComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-18cab25f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorConfigComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1120:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.userProfileDiv[data-v-18cab25f]{\n    width: 80%;\n    margin-left: auto;\n    margin-right: auto;\n}\n.imageProfile[data-v-18cab25f] {\n    height: 200px;\n    width: 200px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "vendor-config-component",
    data: function data() {
        return {
            token: '',
            vendor: {
                firstName: '',
                lastName: '',
                email: '',
                address: '',
                storeName: '',
                city: '',
                state: '',
                country: '',
                image: '',
                bio: '',
                vendor_id: '',
                account_no: null,
                bank_name: ''
            }
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authVendor')) {
            var vendorAuth = JSON.parse(localStorage.getItem('authVendor'));
            console.log(vendorAuth.storeName);
            this.token = vendorAuth.access_token;
            this.vendor.storeName = vendorAuth.storeName;
            this.vendor.email = vendorAuth.email;
            this.vendor.vendor_id = vendorAuth.id;
            axios.get('/api/getvendor/' + this.vendor.storeName, { headers: { "Authorization": 'Bearer ' + vendorAuth.access_token } }).then(function (response) {
                if (response.status === 200) {
                    response.data.data.forEach(function (value) {
                        _this.vendor.firstName = value.firstName;
                        _this.vendor.lastName = value.lastName;
                        _this.vendor.address = value.address;
                        _this.vendor.city = value.city;
                        _this.vendor.state = value.state;
                        _this.vendor.image = value.image;
                        _this.vendor.country = value.country;
                        _this.vendor.bank_name = value.bank_name;
                        _this.vendor.account_no = value.account_no;
                        _this.vendor.bio = value.bio;
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        vendorDetail: function vendorDetail() {
            var _this2 = this;

            axios.post('/api/vendordetail', JSON.parse(JSON.stringify(this.vendor)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    _this2.$router.push('/vendor/user/' + _this2.vendor.storeName);
                    _this2.$toasted.success("Profile successfully updated");
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        uploadPic: function uploadPic(e) {
            var _this3 = this;

            var file = e.target.files[0];
            var reader = new FileReader();
            reader.onload = function (event) {
                _this3.vendor.image = event.target.result;
                _this3.$emit('addedProfileImageToHeader', _this3.vendor.image);
            };
            reader.readAsDataURL(file);
        }
    }
});

/***/ }),

/***/ 1122:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "userProfileDiv" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "firstName" } }, [_vm._v("First Name")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.firstName,
              expression: "vendor.firstName"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "firstName" },
          domProps: { value: _vm.vendor.firstName },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "firstName", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "lastName" } }, [_vm._v("Last Name")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.lastName,
              expression: "vendor.lastName"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "lastName" },
          domProps: { value: _vm.vendor.lastName },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "lastName", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "email" } }, [_vm._v("Email")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.email,
              expression: "vendor.email"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "email", id: "email", readonly: "" },
          domProps: { value: _vm.vendor.email },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "email", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "address" } }, [_vm._v("Address")]),
        _vm._v(" "),
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.address,
              expression: "vendor.address"
            }
          ],
          staticClass: "form-control",
          attrs: { id: "address" },
          domProps: { value: _vm.vendor.address },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "address", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "city" } }, [_vm._v("City")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.city,
              expression: "vendor.city"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "city" },
          domProps: { value: _vm.vendor.city },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "city", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "state" } }, [_vm._v("State")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.state,
              expression: "vendor.state"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "state" },
          domProps: { value: _vm.vendor.state },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "state", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "country" } }, [_vm._v("Country")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.country,
              expression: "vendor.country"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "country" },
          domProps: { value: _vm.vendor.country },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "country", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "bank_name" } }, [_vm._v("Bank Name")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.bank_name,
              expression: "vendor.bank_name"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "bank_name" },
          domProps: { value: _vm.vendor.bank_name },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "bank_name", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "account_no" } }, [
          _vm._v("Account Number")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.account_no,
              expression: "vendor.account_no"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "number",
            id: "account_no",
            min: "9",
            maxlength: "10"
          },
          domProps: { value: _vm.vendor.account_no },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "account_no", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-6" }, [
          _c("img", {
            staticClass: "img-thumbnail imageProfile",
            attrs: { src: _vm.vendor.image },
            model: {
              value: _vm.vendor.image,
              callback: function($$v) {
                _vm.$set(_vm.vendor, "image", $$v)
              },
              expression: "vendor.image"
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("label", { attrs: { for: "image" } }, [
            _vm._v("Upload Store Image")
          ]),
          _vm._v(" "),
          _c("input", {
            attrs: { type: "file", id: "image" },
            on: {
              change: function($event) {
                return _vm.uploadPic($event)
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "bio" } }, [_vm._v("Bio")]),
        _vm._v(" "),
        _c("textarea", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.vendor.bio,
              expression: "vendor.bio"
            }
          ],
          staticClass: "form-control",
          attrs: { id: "bio" },
          domProps: { value: _vm.vendor.bio },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.vendor, "bio", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group text-center" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            attrs: { type: "submit" },
            on: {
              click: function($event) {
                return _vm.vendorDetail()
              }
            }
          },
          [_vm._v("Update Profile")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-18cab25f", module.exports)
  }
}

/***/ }),

/***/ 497:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1119)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1121)
/* template */
var __vue_template__ = __webpack_require__(1122)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-18cab25f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorConfigComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-18cab25f", Component.options)
  } else {
    hotAPI.reload("data-v-18cab25f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});