webpackJsonp([137],{

/***/ 1792:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1793);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("89df15d0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-661a8bc0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./groupChat.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-661a8bc0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./groupChat.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1793:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-661a8bc0] {\n  background: white;\n  height: 92vh;\n  padding: 30px;\n}\n.pm[data-v-661a8bc0] {\n  position: relative;\n  border-bottom: 1px solid #ccc;\n  padding-right: 15px;\n  font-size: 16px;\n  cursor: pointer;\n  color: rgba(0, 0, 0, 0.75);\n}\n.hoverText[data-v-661a8bc0] {\n  position: absolute;\n}\n.active[data-v-661a8bc0] {\n  background: white;\n  border-right: 5px solid #ccc;\n  border-bottom: none !important;\n  border-top: none !important;\n}\n.name_user[data-v-661a8bc0] {\n  font-size: 13px;\n}\n.form-group[data-v-661a8bc0] {\n  position: relative;\n  overflow: hidden;\n}\n.bg-ccc[data-v-661a8bc0] {\n  background: #ccc;\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.submit[data-v-661a8bc0] {\n  width: 85%;\n  margin: 0 auto;\n}\n.search_icon[data-v-661a8bc0] {\n  position: absolute;\n  top: 50%;\n  right: 10px;\n  font-size: 16px;\n  margin-top: -8px;\n  color: rgba(0, 0, 0, 0.5);\n}\n.profile_img[data-v-661a8bc0] {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  margin-right: 10px;\n}\n.align_image[data-v-661a8bc0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.chat_header[data-v-661a8bc0] {\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n}\n.message-tab[data-v-661a8bc0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 100%;\n  border: 1px solid #f7f8fa;\n}\n.time[data-v-661a8bc0] {\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 10px;\n}\n.message-body[data-v-661a8bc0] {\n   height: 82%;\n  max-height: 82%;\n  overflow-y: scroll;\n  background: #333;\n}\n.message-body div li[data-v-661a8bc0] {\n  width: 50%;\n  padding: 10px 30px;\n  font-size: 15px;\n}\n.chat-left[data-v-661a8bc0] {\n  background-color: #d1e0ed;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  position: relative;\n  line-height: 1.2;\n}\n.chat-left[data-v-661a8bc0]::before {\n  content: \"\";\n   background-color: #d1e0ed;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(50deg);\n          transform: skew(50deg);\n  position: absolute;\n  left: -2px;\n  top: 0px;\n}\n.chat-right[data-v-661a8bc0] {\n  position: relative;\n  background: #f7f8fa;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: auto;\n  text-align: left;\n  line-height: 1.2;\n}\n.chat-right[data-v-661a8bc0]::before {\n  content: \"\";\n  background: #f7f8fa;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(-50deg);\n          transform: skew(-50deg);\n  position: absolute;\n  right: -2px;\n  top: 0px;\n}\n.form-group[data-v-661a8bc0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul[data-v-661a8bc0],\nol[data-v-661a8bc0],\nli[data-v-661a8bc0] {\n  list-style: none;\n}\n.users[data-v-661a8bc0] {\n  width: 30%;\n  padding: 15px 20px;\n  background: #f7f8fa;\n  height: 100%;\n  overflow: hidden;\n}\n.message[data-v-661a8bc0] {\n  height: 100%;\n  max-height: 100%;\n  width: 100%;\n  position: relative;\n}\n.user_name[data-v-661a8bc0] {\n  height: 100%;\n  max-height: 100%;\n  overflow-y: scroll;\n  font-size: 15px;\n}\n.user_name li[data-v-661a8bc0] {\n  padding: 10px 15px 10px;\n  border-bottom: 1px solid hsl(220, 23%, 90%);\n  cursor: pointer;\n}\n@media (max-width: 768px) {\n.main-page[data-v-661a8bc0] {\n    padding: 0;\n}\n.mobile[data-v-661a8bc0] {\n    display: block;\n}\n.chat_header[data-v-661a8bc0] {\n    font-size: 14px;\n    padding: 20px 10px !important;\n}\n.message-tab[data-v-661a8bc0] {\n    position: relative;\n}\n.message-body div li[data-v-661a8bc0] {\n    width: 70%;\n    padding: 10px;\n    font-size: 14px;\n}\n.submit[data-v-661a8bc0]{\n    width: 97%;\n}\n.pm[data-v-661a8bc0]{\n    font-size: 12px;\n}\n.user_name li[data-v-661a8bc0] {\n    padding: 15px 10px;\n    font-size: 12px;\n}\n.users[data-v-661a8bc0] {\n    width: 60%;\n    position: absolute;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    z-index: 1;\n    padding: 15px 10px;\n    -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n            box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n}\n.profile_img[data-v-661a8bc0] {\n    width: 20px;\n    height: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1794:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      messages: [],
      users: [],
      course_id: null,
      message: "",
      id: null,
      active: 0,
      search: "",
      activeUser: 0,
      course: {},
      hide: false
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.course_id = vm.$route.params.id;
      vm.fetchMessages();
    });
  },
  mounted: function mounted() {
    this.initialLoad();
  },

  watch: {
    course_id: "initialLoad"
  },
  methods: {
    initialLoad: function initialLoad() {
      var _this = this;

      this.course_id = this.$route.params.id;
      var user = JSON.parse(localStorage.getItem("authUser"));
      this.id = user.id;

      if (window.innerWidth < 768) {
        this.hide = true;
      } else {
        this.hide = false;
      }
      Echo.join("group." + this.$route.params.id).listen("GroupMessageSent", function (e) {
        _this.messages.push({
          message: e.message.message,
          user: e.user,
          created_at: e.message.created_at
        });
      }).here(function (user) {
        _this.users = user;
      });
    },
    showUsers: function showUsers() {
      this.hide = !this.hide;
    },
    fetchMessages: function fetchMessages() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/group-messages/" + this.course_id, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this2.messages = response.data;
        });

        axios.get("/api/get-product/" + this.course_id, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (res) {
          _this2.course = res.data.data;
        });
      }
    },
    sendMessage: function sendMessage() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        var text = {};
        text.message = this.message;
        text.user_id = user.id;
        text.created_at = new Date();
        text.user = user;
        this.messages.push(text);

        var data = {
          message: this.message,
          course_id: this.course_id
        };

        axios.post("/api/group-message", data, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this3.message = "";
          if (response.status == 200) {}
        }).catch(function (err) {
          _this3.$toasted.error("Message not sent!!");
        });
      }
    }
  }
});

/***/ }),

/***/ 1795:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "message-tab" }, [
      !_vm.hide
        ? _c(
            "div",
            { staticClass: "users animated slideIn" },
            [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.search,
                      expression: "search"
                    }
                  ],
                  staticClass: "form-control rounded-pill border-0",
                  attrs: {
                    type: "text",
                    "aria-describedby": "helpId",
                    placeholder: "Search"
                  },
                  domProps: { value: _vm.search },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.search = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa fa-search search_icon",
                  attrs: { "aria-hidden": "true" }
                })
              ]),
              _vm._v(" "),
              _vm.course.vendor
                ? _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "PrivateChat",
                          params: { id: _vm.course.vendor.id }
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "py-3 pm" }, [
                        _c("span", { staticClass: "toCaps bold" }, [
                          _vm._v(
                            _vm._s(_vm.course.vendor.username.toLowerCase())
                          )
                        ]),
                        _vm._v(" - Start a chat\n        ")
                      ])
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "ul",
                { staticClass: "user_name" },
                _vm._l(_vm.users, function(user, idx) {
                  return _c(
                    "li",
                    {
                      key: idx,
                      staticClass: "toCaps align_image",
                      on: {
                        click: function($event) {
                          return _vm.getVendor(user.id, idx)
                        }
                      }
                    },
                    [
                      user.logo !== null && user.logo !== undefined
                        ? _c("div", [
                            _c("img", {
                              staticClass: "profile_img",
                              attrs: { src: user.logo, alt: "" }
                            })
                          ])
                        : _c("div", { staticClass: "profile_img" }, [
                            _c("i", {
                              staticClass: "fa fa-user-circle text-main",
                              attrs: { "aria-hidden": "true" }
                            })
                          ]),
                      _vm._v(" "),
                      _c("div", [_vm._v(_vm._s(user.name.toLowerCase()))])
                    ]
                  )
                }),
                0
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "form",
        {
          staticClass: "message",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.sendMessage($event)
            }
          }
        },
        [
          _vm.course.courses
            ? _c(
                "div",
                {
                  staticClass: "toCaps p-3 chat_header align_image text-right"
                },
                [
                  _vm.course.courses.title
                    ? _c("p", { staticClass: "toCaps w-75 text-left" }, [
                        _vm._v(
                          _vm._s(_vm.course.courses.title.toLowerCase()) +
                            " Group Chat"
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("i", {
                    staticClass:
                      "fa fa-ellipsis-v text-main ml-auto pr-3 mobile",
                    attrs: { "aria-hidden": "true" },
                    on: { click: _vm.showUsers }
                  })
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.messages
            ? _c(
                "ul",
                {
                  directives: [
                    { name: "chat-scroll", rawName: "v-chat-scroll" }
                  ],
                  staticClass: "message-body"
                },
                [
                  _c(
                    "div",
                    { staticClass: "p-2 py-3 pb-4" },
                    _vm._l(_vm.messages, function(message, index) {
                      return _c(
                        "li",
                        {
                          key: index,
                          staticClass: "py-1",
                          class: {
                            "text-right": _vm.id == message.user_id,
                            "ml-auto": _vm.id == message.user_id
                          }
                        },
                        [
                          _c(
                            "div",
                            {
                              class: {
                                "chat-right": _vm.id == message.user_id,
                                "chat-left": _vm.id !== message.user_id
                              }
                            },
                            [
                              _c(
                                "p",
                                {
                                  staticClass:
                                    "toCaps text-muted name_user mb-2"
                                },
                                [
                                  _vm._v(
                                    _vm._s(message.user.name.toLowerCase())
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("p", [
                                _vm._v(
                                  "\n                " +
                                    _vm._s(message.message) +
                                    "\n                "
                                ),
                                _c("span", { staticClass: "time" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("moment")(
                                        message.created_at,
                                        "HH:mm"
                                      )
                                    )
                                  )
                                ])
                              ])
                            ]
                          )
                        ]
                      )
                    }),
                    0
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "bg-ccc p-2" }, [
            _c("div", { staticClass: "form-group submit" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.message,
                    expression: "message"
                  }
                ],
                staticClass: "form-control rounded-pill mr-3",
                attrs: {
                  required: "",
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Type your message here .."
                },
                domProps: { value: _vm.message },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.message = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(0)
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass:
          "elevated_btn elevated_btn_sm m-0 btn-compliment text-white shadow-none rounded-pill",
        attrs: { type: "submit" }
      },
      [
        _c("i", {
          staticClass: "fas fa-paper-plane text-white",
          attrs: { "aria-hidden": "true" }
        })
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-661a8bc0", module.exports)
  }
}

/***/ }),

/***/ 631:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1792)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1794)
/* template */
var __vue_template__ = __webpack_require__(1795)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-661a8bc0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/groupChat.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-661a8bc0", Component.options)
  } else {
    hotAPI.reload("data-v-661a8bc0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});