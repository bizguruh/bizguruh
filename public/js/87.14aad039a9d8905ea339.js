webpackJsonp([87],{

/***/ 1095:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1096);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("744c41cc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-44ed9752\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-44ed9752\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1096:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.rowProduct[data-v-44ed9752] {\n  margin-bottom: 50px;\n}\nth[data-v-44ed9752] {\n  background-color: #f3f3f3;\n  padding: 10px !important;\n  color: rgba(0,0,0.74);\n}\ntd[data-v-44ed9752] {\n   color: rgba(0,0,0.64);\n  padding: 10px !important;\n}\n.grids[data-v-44ed9752]{\n  height:90vh;\n}\n.dive[data-v-44ed9752]{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    padding: 0;\n}\n.btn-create[data-v-44ed9752] {\n  background: #333333;\n}\n.btn-create a[data-v-44ed9752] {\n  color: #ffffff;\n}\n.linktoproduct[data-v-44ed9752] {\n  cursor: pointer;\n}\n.loader-display[data-v-44ed9752] {\n  z-index: 999;\n  position: absolute;\n  left: 32vw;\n  top: 27vh;\n}\n.main-page[data-v-44ed9752] {\n  position: relative;\n  min-height: 100vh;\n}\n@media (max-width: 768px) {\ntable[data-v-44ed9752] {\n    font-size: 14px;\n}\nth[data-v-44ed9752] {\n    background-color: #f3f3f3;\n    padding: 5px 8px !important;\n    color: #000000;\n}\ntd[data-v-44ed9752] {\n    color: #a4a4a4 !important;\n    padding: 5px 8px !important;\n}\n.btn[data-v-44ed9752] {\n    text-transform: capitalize !important;\n    padding: 6px 12px;\n    font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1097:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      title: "",
      overallCheck: false,
      checkIn: false,
      loaderShow: true,
      products: [],
      product: {},

      isActive: true,
      initvalue: "",
      loader: "/images/loader.gif",
      token: "",
      update: 0
    };
  },
  mounted: function mounted() {
    this.getProducts();
  },

  watch: {
    update: "getProducts"
  },
  methods: {
    getProducts: function getProducts() {
      var _this = this;

      var vendor = JSON.parse(localStorage.getItem("authVendor"));
      if (vendor != null) {
        var vendorid = vendor.id;
        this.token = vendor.access_token;
        axios.get("/api/vendorproduct/" + vendorid, {
          headers: { Authorization: "Bearer " + vendor.access_token }
        }).then(function (response) {
          if (response.data.data.length > 0) {
            var emptyArray = [];

            response.data.data.forEach(function (item) {
              switch (item.prodType) {
                case "Books":
                  _this.$set(item.books, "verify", item.verify);
                  _this.$set(item.books, "uniId", item.id);
                  _this.$set(item.books, "created_at", item.myDate);
                  _this.$set(item.books, "prodType", item.prodType);
                  emptyArray.push(item.books);
                  break;
                case "White Paper":
                  _this.$set(item.whitePaper, "verify", item.verify);
                  _this.$set(item.whitePaper, "uniId", item.id);
                  _this.$set(item.whitePaper, "created_at", item.myDate);
                  _this.$set(item.whitePaper, "prodType", item.prodType);
                  emptyArray.push(item.whitePaper);
                  break;
                case "Market Reports":
                  _this.$set(item.marketReport, "verify", item.verify);
                  _this.$set(item.marketReport, "uniId", item.id);
                  _this.$set(item.marketReport, "created_at", item.myDate);
                  _this.$set(item.marketReport, "prodType", item.prodType);
                  emptyArray.push(item.marketReport);
                  break;
                case "Market Research":
                  _this.$set(item.marketResearch, "verify", item.verify);
                  _this.$set(item.marketResearch, "uniId", item.id);
                  _this.$set(item.marketResearch, "created_at", item.myDate);
                  _this.$set(item.marketResearch, "prodType", item.prodType);
                  emptyArray.push(item.marketResearch);
                  break;
                case "Webinar":
                  _this.$set(item.webinar, "verify", item.verify);
                  _this.$set(item.webinar, "uniId", item.id);
                  _this.$set(item.webinar, "created_at", item.myDate);
                  _this.$set(item.webinar, "prodType", item.prodType);
                  emptyArray.push(item.webinar);
                  break;
                case "Podcast":
                  _this.$set(item.webinar, "verify", item.verify);
                  _this.$set(item.webinar, "uniId", item.id);
                  _this.$set(item.webinar, "created_at", item.myDate);
                  _this.$set(item.webinar, "prodType", item.prodType);
                  emptyArray.push(item.webinar);
                  break;
                case "Videos":
                  _this.$set(item.webinar, "verify", item.verify);
                  _this.$set(item.webinar, "uniId", item.id);
                  _this.$set(item.webinar, "created_at", item.myDate);
                  _this.$set(item.webinar, "prodType", item.prodType);
                  emptyArray.push(item.webinar);
                  break;
                case "Journals":
                  _this.$set(item.journal, "verify", item.verify);
                  _this.$set(item.journal, "uniId", item.id);
                  _this.$set(item.journal, "created_at", item.myDate);
                  _this.$set(item.journal, "prodType", item.prodType);
                  emptyArray.push(item.journal);
                  break;
                case "Articles":
                  _this.$set(item.articles, "verify", item.verify);
                  _this.$set(item.articles, "uniId", item.id);
                  _this.$set(item.articles, "created_at", item.myDate);
                  _this.$set(item.articles, "prodType", item.prodType);
                  emptyArray.push(item.articles);
                  break;
                case "Courses":
                  _this.$set(item.courses, "verify", item.verify);
                  _this.$set(item.courses, "uniId", item.id);
                  _this.$set(item.courses, "created_at", item.myDate);
                  _this.$set(item.courses, "prodType", item.prodType);
                  emptyArray.push(item.courses);
                  break;
                default:
                  return false;
              }
            });
            _this.products = emptyArray;

            _this.isActive = false;
            _this.loaderShow = false;
          } else {
            _this.products = [];
            _this.isActive = false;
            _this.loaderShow = false;
          }
          //  this.products = response.data.data;
        }).catch(function (error) {
          console.log(error);
        });
      } else {
        this.$router.push("/vendor/auth");
      }
    },
    overallCheckBox: function overallCheckBox() {
      var _this2 = this;

      //  this.checkIn = !this.checkIn;
      this.products.forEach(function (item) {
        if (!("itemClick" in item)) {
          _this2.$set(item, "itemClick", true);
        } else if (_this2.overallCheck) {
          _this2.$set(item, "itemClick", true);
        } else {
          _this2.$set(item, "itemClick", false);
        }
      });
    },
    deleteItem: function deleteItem() {
      var _this3 = this;

      var deletedArray = [];
      this.products.forEach(function (item) {
        if (item.itemClick) {
          deletedArray.push(item.uniId);
        }
      });
      if (deletedArray.length === 0) {
        this.$toasted.error("You must select at least one product to delete");
      } else {
        axios.post("/api/delete/many-products", JSON.parse(JSON.stringify(deletedArray)), { headers: { Authorization: "Bearer " + this.token } }).then(function (response) {
          if (response.status === 200) {
            for (var i = 0; i < deletedArray.length; i++) {
              _this3.products.splice(i, 1);
            }
            _this3.$toasted.success("All selected items deleted");
            _this3.update++;
            _this3.products.forEach(function (item) {
              _this3.$set(item, "itemClick", false);
            });
          } else {
            _this3.$toasted.error("Unable to delete");
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    }
  }
});

/***/ }),

/***/ 1098:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", { staticClass: "h-100" }, [
          _c(
            "div",
            { staticClass: "grids widget-shadow shadow-sm bg-white rounded" },
            [
              _c("div", { staticClass: "row rowProduct" }, [
                _c(
                  "div",
                  { staticClass: "col-md-12 dive" },
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: "/vendor/add/category" } },
                      [
                        _c(
                          "button",
                          {
                            staticClass:
                              "elevated_btn elevated_btn_sm btn-compliment text-white"
                          },
                          [_vm._v("Create New Resource  ")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm bg-red text-white",
                        on: { click: _vm.deleteItem }
                      },
                      [_vm._v("Delete")]
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table table-bordered" }, [
                  _c("thead", { staticClass: "thead-dark" }, [
                    _c("tr", [
                      _c("th", [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.overallCheck,
                              expression: "overallCheck"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.overallCheck)
                              ? _vm._i(_vm.overallCheck, null) > -1
                              : _vm.overallCheck
                          },
                          on: {
                            change: [
                              function($event) {
                                var $$a = _vm.overallCheck,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.overallCheck = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.overallCheck = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.overallCheck = $$c
                                }
                              },
                              _vm.overallCheckBox
                            ]
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("th", [_vm._v("#")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Details")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Category")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Status")]),
                      _vm._v(" "),
                      _c("th", [_vm._v("Created On")]),
                      _vm._v(" "),
                      _c("th")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.products, function(product, index) {
                      return _vm.products.length > 0
                        ? _c("tr", [
                            _c("td", [
                              _c("input", {
                                attrs: { type: "checkbox" },
                                domProps: { checked: product.itemClick },
                                on: {
                                  change: function($event) {
                                    product.itemClick = !product.itemClick
                                    _vm.overallCheck = false
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("td", [_vm._v(_vm._s(index + 1))]),
                            _vm._v(" "),
                            product.articleTitle
                              ? _c("td", { staticClass: "toCaps" }, [
                                  _vm._v(
                                    _vm._s(product.articleTitle.toLowerCase())
                                  )
                                ])
                              : _c("td", { staticClass: "toCaps" }, [
                                  _vm._v(_vm._s(product.title.toLowerCase()))
                                ]),
                            _vm._v(" "),
                            product.prodType == "Market Research"
                              ? _c("td", [_vm._v("Book")])
                              : _c("td", [_vm._v(_vm._s(product.prodType))]),
                            _vm._v(" "),
                            product.verify === 1
                              ? _c("td", [_vm._v("Active")])
                              : _c("td", [_vm._v("Pending")]),
                            _vm._v(" "),
                            _c("td", [
                              _vm._v(
                                _vm._s(
                                  _vm._f("moment")(product.created_at, "L")
                                )
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "linktoproduct",
                                    attrs: {
                                      to: {
                                        name: "showVendorProduct",
                                        params: { id: product.uniId }
                                      }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-arrow-right p-2"
                                    })
                                  ]
                                )
                              ],
                              1
                            )
                          ])
                        : _c("tr", [_vm._v("No Product")])
                    }),
                    0
                  )
                ])
              ])
            ]
          )
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-44ed9752", module.exports)
  }
}

/***/ }),

/***/ 491:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1095)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1097)
/* template */
var __vue_template__ = __webpack_require__(1098)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-44ed9752"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/viewProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-44ed9752", Component.options)
  } else {
    hotAPI.reload("data-v-44ed9752", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});