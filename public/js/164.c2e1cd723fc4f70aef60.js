webpackJsonp([164],{

/***/ 1257:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1258);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("47edc615", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6fa7efda\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminShowProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6fa7efda\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminShowProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1258:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.btn-primary[data-v-6fa7efda] {\n    margin: 30px;\n}\n.detailContainer[data-v-6fa7efda] {\n    border: 1px solid #90b5da;\n    margin: 30px;\n}\n.detailContainer p[data-v-6fa7efda] {\n    border-bottom: 1px solid #90b5da;\n    cursor: pointer;\n}\n.detailContainer p[data-v-6fa7efda]:hover {\n    background-color: #90b5da;\n}\n.main-page[data-v-6fa7efda] {\n    height: 100vh;\n    background: #ffffff;\n    width: 90%;\n    margin-left: auto;\n    margin-right: auto;\n}\n.content-wrapper[data-v-6fa7efda] {\n    padding:30px 0;\n}\n.imgDivDisplay[data-v-6fa7efda] {\nheight: 330px;\n}\n.slide-up-down-enter-active[data-v-6fa7efda] {\n    -webkit-transition: all .3s ease;\n    transition: all .3s ease;\n}\n.slide-up-down-leave-active[data-v-6fa7efda] {\n    -webkit-transition: all .2s ease;\n    transition: all .2s ease;\n}\n.slide-up-down-enter[data-v-6fa7efda] {\n    -webkit-transform: translateY(-50px);\n            transform: translateY(-50px);\n    opacity: 0;\n}\n.slide-up-down-leave-to[data-v-6fa7efda] {\n    -webkit-transform: translateY(-50px);\n            transform: translateY(-50px);\n    opacity: 0;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-show-product-component",
    data: function data() {
        return {
            id: this.$route.params.id,
            isActive: true,
            category: false,
            priceLists: false,
            format: false,
            linkTag: false,
            detail: true,
            prod: {},
            storeName: '',
            products: {},
            featuredProducts: 'Sponsor Product',
            verifyProducts: 'Verify Product',
            imgSrc: '',
            token: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.token = admin.access_token;
        if (admin != null) {
            axios.get('/api/admin/product/' + this.id, { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response.data.data);
                    var emptyObject = {};
                    switch (response.data.data.prodType) {
                        case 'Books':
                            emptyObject = response.data.data.books;
                            break;
                        case 'White Paper':
                            emptyObject = response.data.data.whitePaper;
                            break;
                        case 'Market Reports':
                            emptyObject = response.data.data.marketReport;
                            break;
                        case 'Journals':
                            emptyObject = response.data.data.journal;
                            break;
                        case 'Market Research':
                            emptyObject = response.data.data.marketResearch;
                            break;
                        case 'Webinar':
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Podcast':
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Videos':
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Courses':
                            emptyObject = response.data.data.courses;
                            break;
                        case 'Events':
                            emptyObject = response.data.data.events;
                            break;
                        case 'Articles':
                            emptyObject = response.data.data.articles;
                            break;
                        default:
                            return false;

                    }

                    _this.storeName = response.data.data.vendor.storeName;
                    if (response.data.data.verify) {
                        _this.verifyProducts = 'Unverify Product';
                    } else {
                        _this.verifyProducts = 'Verify Product';
                    }

                    if (response.data.data.sponsor === 'Y') {
                        _this.featuredProducts = 'Unsponsor Product';
                    } else {
                        _this.featuredProducts = 'Sponsor Product';
                    }

                    _this.prod = response.data.data;
                    _this.products = _extends({}, emptyObject, _this.prod);

                    //this.products.prodContent = emptyObject;
                    console.log(_this.products);
                    _this.isActive = false;
                    _this.linkTag = true;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        verifyProduct: function verifyProduct(productid) {
            var _this2 = this;

            axios.get('/api/admin/verify/product/' + productid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                if (response.data.verify) {
                    _this2.$toasted.success("Successfully verified");
                    _this2.verifyProducts = "Unverify Product";
                } else {
                    _this2.$toasted.success("Successfully unverified");
                    _this2.verifyProducts = "Verify Product";
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        featuredProduct: function featuredProduct(productid) {
            var _this3 = this;

            axios.get('/api/admin/featured/product/' + productid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                if (response.data.sponsor == 'Y') {
                    _this3.$toasted.success("Successfully sponsor");
                    _this3.featuredProducts = "Unsponsor product";
                } else {
                    _this3.$toasted.success("Successfully unsponsor");
                    _this3.featuredProducts = "Sponsor product";
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        swipeImage: function swipeImage(index) {
            console.log(index);
            for (var i = 0; i < this.products.productImage.length; i++) {
                this.imgSrc = this.products.productImage[index].image;
                //console.log(this.products.productImage[index].image);
            }
        },
        showAndHide: function showAndHide(params) {
            switch (params) {
                case 'detail':
                    this.detail = !this.detail;
                    break;
                case 'category':
                    this.category = !this.category;
                    break;
                case 'priceLists':
                    this.priceLists = !this.priceLists;
                    break;
                case 'format':
                    this.format = !this.format;
                    break;
                default:
                    this.detail = this.category = this.priceLists = this.format = false;
            }
        },
        deleteProduct: function deleteProduct(id) {
            var _this4 = this;

            confirm('Are you sure');
            axios.delete('api/admin/remove/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.$router.push('/admin/products');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1260:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "skin-blue" }, [
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "main-page" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6 text-center" }, [
            _c("h2", { staticClass: "h1" }, [
              _vm._v(_vm._s(_vm.products.title))
            ]),
            _vm._v(" "),
            _c("div", [
              _vm._v(
                _vm._s(_vm.products.author) +
                  " / " +
                  _vm._s(_vm.storeName) +
                  " "
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 text-center mt-4" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-danger",
                on: {
                  click: function($event) {
                    return _vm.deleteProduct(_vm.products.id)
                  }
                }
              },
              [_vm._v("Delete")]
            ),
            _vm._v(
              "\n                  STATUS: " +
                _vm._s(
                  _vm.products.verify === 1 ? "CONFIRMED" : "NOT CONFIRMED"
                ) +
                "\n                "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6 text-center" }, [
            _c("div", [
              _c("img", {
                staticClass: "imgDivDisplay",
                attrs: { src: _vm.products.coverImage, height: "100" }
              })
            ]),
            _vm._v(" "),
            _c("div", [_vm._v(_vm._s(_vm.products.aboutAuthor))]),
            _vm._v(" "),
            _c("div", [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  on: {
                    click: function($event) {
                      return _vm.featuredProduct(_vm.products.id)
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.featuredProducts))]
              )
            ]),
            _vm._v(" "),
            _c("div", [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  on: {
                    click: function($event) {
                      return _vm.verifyProduct(_vm.products.id)
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.verifyProducts))]
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 text-center" }, [
            _c(
              "div",
              { staticClass: "detailContainer" },
              [
                _c(
                  "p",
                  {
                    on: {
                      click: function($event) {
                        return _vm.showAndHide("detail")
                      }
                    }
                  },
                  [_vm._v("Details")]
                ),
                _vm._v(" "),
                _c("transition", { attrs: { name: "slide-up-down" } }, [
                  _vm.detail
                    ? _c("div", [
                        _c("div", [
                          _vm._v("ISBN-13 : " + _vm._s(_vm.products.fileType))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(
                            "Publisher : " + _vm._s(_vm.products.publisher)
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(
                            "Publication Date : " +
                              _vm._s(_vm.products.publicationDate)
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v("Series : " + _vm._s(_vm.products.series))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v("Pages : " + _vm._s(_vm.products.pageNo))
                        ])
                      ])
                    : _vm._e()
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "detailContainer" },
              [
                _c(
                  "p",
                  {
                    on: {
                      click: function($event) {
                        return _vm.showAndHide("category")
                      }
                    }
                  },
                  [_vm._v("Category")]
                ),
                _vm._v(" "),
                _c("transition", { attrs: { name: "slide-up-down" } }, [
                  _vm.category
                    ? _c("div", [
                        _c("div", [
                          _vm._v(
                            "ISBN-13 : " + _vm._s(_vm.products.category.name)
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(
                            "Publisher : " +
                              _vm._s(_vm.products.subcategory.name)
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(
                            "Publication Date : " +
                              _vm._s(_vm.products.topic.name)
                          )
                        ])
                      ])
                    : _vm._e()
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "detailContainer" },
              [
                _c(
                  "p",
                  {
                    on: {
                      click: function($event) {
                        return _vm.showAndHide("priceLists")
                      }
                    }
                  },
                  [_vm._v("Pricelists")]
                ),
                _vm._v(" "),
                _c("transition", { attrs: { name: "slide-up-down" } }, [
                  _vm.priceLists
                    ? _c("div", [
                        _vm.products.hardCopyPrice !== null
                          ? _c("div", [
                              _vm._v(
                                "Hard Cover Price : ₦" +
                                  _vm._s(_vm.products.hardCopyPrice) +
                                  ".00"
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.softCopyPrice !== null
                          ? _c("div", [
                              _vm._v(
                                "Soft Cover Price : ₦" +
                                  _vm._s(_vm.products.softCopyPrice) +
                                  ".00"
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.audioPrice !== null
                          ? _c("div", [
                              _vm._v(
                                " Audio Price: ₦" +
                                  _vm._s(_vm.products.audioPrice) +
                                  ".00"
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.videoPrice !== null
                          ? _c("div", [
                              _vm._v(
                                "Video Price: ₦" +
                                  _vm._s(_vm.products.videoPrice) +
                                  ".00"
                              )
                            ])
                          : _vm._e()
                      ])
                    : _vm._e()
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "detailContainer" },
              [
                _c(
                  "p",
                  {
                    on: {
                      click: function($event) {
                        return _vm.showAndHide("format")
                      }
                    }
                  },
                  [_vm._v("Formats")]
                ),
                _vm._v(" "),
                _c("transition", { attrs: { name: "slide-up-down" } }, [
                  _vm.format
                    ? _c("div", [
                        _vm.products.hardCopyPrice !== ""
                          ? _c("div", [_vm._v("Hard Copy")])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.softCopyPrice !== ""
                          ? _c("div", [_vm._v("Pdf Format")])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.audioPrice !== ""
                          ? _c("div", [_vm._v("Audio Format")])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.products.videoPrice !== ""
                          ? _c("div", [_vm._v("Video Format")])
                          : _vm._e()
                      ])
                    : _vm._e()
                ])
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6fa7efda", module.exports)
  }
}

/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1257)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1259)
/* template */
var __vue_template__ = __webpack_require__(1260)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6fa7efda"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminShowProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6fa7efda", Component.options)
  } else {
    hotAPI.reload("data-v-6fa7efda", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});