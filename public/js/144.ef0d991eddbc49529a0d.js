webpackJsonp([144],{

/***/ 1697:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1698);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("34075964", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a80474ae\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./templates.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a80474ae\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./templates.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1698:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container-fluid[data-v-a80474ae] {\n  padding: 0;\n  min-height: 100vh;\n  background-color: #f7f8fa;\n}\n.contain-box[data-v-a80474ae] {\n  padding: 10px;\n  width: 100%;\n  height: 100%;\n}\n.module[data-v-a80474ae] {\n  background: white;\n  width: 70%;\n  margin: 0 auto;\n  margin-bottom: 40px;\n  border-radius: 5px;\n  padding: 15px;\n}\n.sub_heading[data-v-a80474ae] {\n  padding: 15px;\n}\n.title[data-v-a80474ae] {\n  padding: 10px 40px;\n}\n.title_name[data-v-a80474ae] {\n  font-size: 20px;\n  color: hsl(207, 46%, 20%);\n  font-family: \"Josefin Sans\";\n}\n.topics[data-v-a80474ae] {\n  padding: 5px 80px;\n  position: relative;\n}\n.topic[data-v-a80474ae] {\n  font-size: 15px;\n  color: tomato;\n}\n.grid_box[data-v-a80474ae] {\n  height: 100px;\n  position: relative;\n  border: 1px solid #f7f8fa;\n  text-align: center;\n  border-radius: 4px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(#414d57),\n    color-stop(#52616e),\n    color-stop(#647786),\n    color-stop(#778c9f),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#778c9f),\n    color-stop(#647786),\n    color-stop(#52616e),\n    to(#414d57)\n  );\n  background-image: linear-gradient(\n    to right top,\n    #414d57,\n    #52616e,\n    #647786,\n    #778c9f,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #778c9f,\n    #647786,\n    #52616e,\n    #414d57\n  );\n  -webkit-transition: all 2s;\n  transition: all 2s;\n}\n.grid_text[data-v-a80474ae] {\n  text-align: center;\n  position: absolute;\n  font-weight: bold;\n  font-size: 15px;\n  color: white;\n  text-transform: capitalize;\n  font-family: \"Josefin Sans\";\n}\n.grid_box[data-v-a80474ae]:hover {\n  background-image: -webkit-gradient(\n    linear,\n    right bottom, left top,\n    from(#a4c2db),\n    color-stop(#90abc1),\n    color-stop(#7d94a7),\n    color-stop(#6a7e8e),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#6a7e8e),\n    color-stop(#7d94a7),\n    color-stop(#90abc1),\n    to(#a4c2db)\n  );\n  background-image: linear-gradient(\n    to left top,\n    #a4c2db,\n    #90abc1,\n    #7d94a7,\n    #6a7e8e,\n    #586876,\n    #586876,\n    #586876,\n    #586876,\n    #6a7e8e,\n    #7d94a7,\n    #90abc1,\n    #a4c2db\n  );\n}\nh3[data-v-a80474ae] {\n  font-size: 18.5px;\n}\n.first[data-v-a80474ae] {\n  width: 100%;\n  margin: 0 auto;\n}\nul[data-v-a80474ae],\nol[data-v-a80474ae] {\n  list-style: none;\n}\n@media only screen and (max-width: 768px) {\n.first[data-v-a80474ae] {\n    width: 90%;\n}\n.container-fluid[data-v-a80474ae] {\n    padding: 20px 20px;\n}\n.bap_box[data-v-a80474ae] {\n    grid-template-columns: auto auto;\n}\n.grid_text[data-v-a80474ae] {\n    font-size: 18px;\n}\n.topics[data-v-a80474ae]{\n    padding:10px ;\n}\n}\n@media only screen and (max-width: 768px) {\n.module[data-v-a80474ae]{\n    width:90%;\n}\n}\n@media only screen and (max-width: 425px) {\n.module[data-v-a80474ae]{\n    width:100%;\n}\n.title[data-v-a80474ae]{\n    padding: 10px 0;\n}\n.topics[data-v-a80474ae]{\n    padding:10px 5px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1699:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      current_month: 0,
      usersub: 0,
      showPage: false,
      templates: [],
      user: {},
      first: false,
      second: false,
      third: false,
      fourth: false,
      fifth: false,
      id: null
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      var name = "My Bright Idea";
      var user = JSON.parse(localStorage.getItem("authUser"));
      vm.getUserSub();
    });
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      this.user = user;
    }
    this.getRecord();
    this.getGroups();
  },

  methods: {
    show: function show(value) {
      switch (value) {
        case "first":
          this.first = !this.first;
          break;
        case "second":
          this.second = !this.second;
          break;
        case "third":
          this.third = !this.third;
          break;
        case "fourth":
          this.fourth = !this.fourth;
          break;
        case "fifth":
          this.fifth = !this.fifth;
          break;

        default:
          break;
      }
    },
    getGroups: function getGroups() {
      var _this = this;

      axios.get("/api/get-form-templates", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (res) {
        if (res.status = 200) {
          var groups = [];
          _this.templates = res.data;
          res.data.forEach(function (item) {
            groups.push(item.group);
          });

          var newSet = new Set(groups);
          _this.templates = Array.from(newSet);
        }
      }).catch();
    },
    getUserSub: function getUserSub() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      if (user !== null) {
        axios.post("/api/user/subscription-plan/" + user.id).then(function (response) {
          if (response.data.length) {
            _this2.usersub = Math.round(response.data[0].level);

            if (Math.round(response.data[0].level) < 3) {
              _this2.$router.push({
                name: "SubscriptionProfile",
                params: {
                  level: 3
                },
                query: {
                  redirect_from: "bap_progress"
                }
              });
            } else {
              _this2.showPage = true;
            }
          } else {
            _this2.$router.push({
              name: "SubscriptionProfile",
              params: {
                level: 3
              },
              query: {
                redirect_from: "bap_progress"
              }
            });
          }
        });
      }
    },
    getRecord: function getRecord() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user != null) {
        axios.get("/api/get-active-record", {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this3.current_month = response.data.current_month;
        });
      }
    }
  }
});

/***/ }),

/***/ 1700:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "modules" }, [
      _c("div", { staticClass: "module shadow-sm" }, [
        _c(
          "div",
          { staticClass: "title" },
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: {
                    name: "Discovery",
                    query: {
                      name: "Self Discovery"
                    }
                  }
                }
              },
              [
                _c("p", { staticClass: "title_name" }, [
                  _c("img", {
                    staticClass: "mr-1",
                    attrs: { src: "/images/next.svg", width: "20", alt: "" }
                  }),
                  _vm._v(" Discovering what is within\n          ")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "topics" }, [
          _c(
            "h5",
            {
              staticClass: "mb-3",
              on: {
                click: function($event) {
                  return _vm.show("first")
                }
              }
            },
            [
              !_vm.first
                ? _c("i", {
                    staticClass: "fas fa-plus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                : _c("i", {
                    staticClass: "fas fa-minus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  }),
              _vm._v("\n          Topics\n        ")
            ]
          ),
          _vm._v(" "),
          _vm.first
            ? _c("ul", { staticClass: "d-flex justify-content-between" }, [
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/self-discovery" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("Getting to know me\n              ")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/idea" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("My Bright Idea\n              ")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/vision" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("My Business Vision\n              ")
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/mission" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("My Business Mission\n              ")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/values" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("My Business Values\n              ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              ])
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "module shadow-sm" }, [
        _c(
          "div",
          { staticClass: "title" },
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: {
                    name: "BusinessEducation",
                    query: {
                      name: "Business Education"
                    }
                  }
                }
              },
              [
                _c("p", { staticClass: "title_name" }, [
                  _c("img", {
                    staticClass: "mr-1",
                    attrs: { src: "/images/next.svg", width: "20", alt: "" }
                  }),
                  _vm._v(" What Kind Of Entrepreneur Am I?\n          ")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "topics" }, [
          _c(
            "h5",
            {
              staticClass: "mb-3",
              on: {
                click: function($event) {
                  return _vm.show("second")
                }
              }
            },
            [
              !_vm.second
                ? _c("i", {
                    staticClass: "fas fa-plus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                : _c("i", {
                    staticClass: "fas fa-minus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  }),
              _vm._v("\n          Topics\n        ")
            ]
          ),
          _vm._v(" "),
          _vm.second
            ? _c(
                "ul",
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/entrepreneur/bap/entrepreneur" } },
                    [
                      _c("li", { staticClass: "topic" }, [
                        _c("img", {
                          staticClass: "mr-1",
                          attrs: {
                            src: "/images/dot-arrow.svg",
                            width: "10",
                            alt: ""
                          }
                        }),
                        _vm._v("\n              Questionnaire\n            ")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    { attrs: { to: "/entrepreneur/bap/entrepreneur" } },
                    [
                      _c("li", { staticClass: "topic" }, [
                        _c("img", {
                          staticClass: "mr-1",
                          attrs: {
                            src: "/images/dot-arrow.svg",
                            width: "10",
                            alt: ""
                          }
                        }),
                        _vm._v("Result & Recommendation\n            ")
                      ])
                    ]
                  )
                ],
                1
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "module shadow-sm" }, [
        _c(
          "div",
          { staticClass: "title" },
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: {
                    name: "BusinessEducation",
                    query: {
                      name: "Business Education"
                    }
                  }
                }
              },
              [
                _c("p", { staticClass: "title_name" }, [
                  _c("img", {
                    staticClass: "mr-1",
                    attrs: { src: "/images/next.svg", width: "20", alt: "" }
                  }),
                  _vm._v(" My Business Concept\n          ")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "topics" }, [
          _c(
            "h5",
            {
              staticClass: "mb-3",
              on: {
                click: function($event) {
                  return _vm.show("third")
                }
              }
            },
            [
              !_vm.third
                ? _c("i", {
                    staticClass: "fas fa-plus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                : _c("i", {
                    staticClass: "fas fa-minus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  }),
              _vm._v("\n          Topics\n        ")
            ]
          ),
          _vm._v(" "),
          _vm.third
            ? _c(
                "ul",
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/entrepreneur/bap/concept" } },
                    [
                      _c("li", { staticClass: "topic" }, [
                        _c("img", {
                          staticClass: "mr-1",
                          attrs: {
                            src: "/images/dot-arrow.svg",
                            width: "10",
                            alt: ""
                          }
                        }),
                        _vm._v(
                          "\n              My Business Overview\n            "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    { attrs: { to: "/entrepreneur/bap/concept" } },
                    [
                      _c("li", { staticClass: "topic" }, [
                        _c("img", {
                          staticClass: "mr-1",
                          attrs: {
                            src: "/images/dot-arrow.svg",
                            width: "10",
                            alt: ""
                          }
                        }),
                        _vm._v(
                          "\n              My Customer Segments\n            "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    { attrs: { to: "/entrepreneur/bap/concept" } },
                    [
                      _c("li", { staticClass: "topic" }, [
                        _c("img", {
                          staticClass: "mr-1",
                          attrs: {
                            src: "/images/dot-arrow.svg",
                            width: "10",
                            alt: ""
                          }
                        }),
                        _vm._v("My Unique Value Proposition\n            ")
                      ])
                    ]
                  )
                ],
                1
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "module shadow-sm" }, [
        _c(
          "div",
          { staticClass: "title" },
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: {
                    name: "BusinessEducation",
                    query: {
                      name: "Business Education"
                    }
                  }
                }
              },
              [
                _c("p", { staticClass: "title_name" }, [
                  _c("img", {
                    staticClass: "mr-1",
                    attrs: { src: "/images/next.svg", width: "20", alt: "" }
                  }),
                  _vm._v("\n            My Marketing Plan\n          ")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "topics" }, [
          _c(
            "h5",
            {
              staticClass: "mb-3",
              on: {
                click: function($event) {
                  return _vm.show("fourth")
                }
              }
            },
            [
              !_vm.fourth
                ? _c("i", {
                    staticClass: "fas fa-plus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                : _c("i", {
                    staticClass: "fas fa-minus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  }),
              _vm._v("\n          Topics\n        ")
            ]
          ),
          _vm._v(" "),
          _vm.fourth
            ? _c("ul", { staticClass: "d-flex justify-content-between" }, [
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/marketing/target" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Target Market Overview\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        attrs: { to: "/entrepreneur/bap/marketing/situation" }
                      },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Situation Analysis (SWOT)\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/marketing/industry" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Industry Analysis (Competitors)\n              "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/marketing/goal" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Marketing Goals\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/marketing/mix" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v("My Marketing Mix\n              ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              ])
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "module shadow-sm" }, [
        _c(
          "div",
          { staticClass: "title" },
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: {
                    name: "BusinessEducation",
                    query: {
                      name: "Business Education"
                    }
                  }
                }
              },
              [
                _c("p", { staticClass: "title_name" }, [
                  _c("img", {
                    staticClass: "mr-1",
                    attrs: { src: "/images/next.svg", width: "20", alt: "" }
                  }),
                  _vm._v("\n            My Sales Plan\n          ")
                ])
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "topics" }, [
          _c(
            "h5",
            {
              staticClass: "mb-3",
              on: {
                click: function($event) {
                  return _vm.show("fifth")
                }
              }
            },
            [
              !_vm.fifth
                ? _c("i", {
                    staticClass: "fas fa-plus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                : _c("i", {
                    staticClass: "fas fa-minus-circle text-main",
                    attrs: { "aria-hidden": "true" }
                  }),
              _vm._v("\n          Topics\n        ")
            ]
          ),
          _vm._v(" "),
          _vm.fifth
            ? _c("ul", { staticClass: "d-flex justify-content-between" }, [
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/sales/strategy" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Business Acquisition Strategy\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/sales/leads" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Lead Generation Overview\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/sales/goals" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Sales Goals\n              "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/sales/funnel" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "\n                My Sales Funnel\n              "
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      { attrs: { to: "/entrepreneur/bap/sales/retention" } },
                      [
                        _c("li", { staticClass: "topic" }, [
                          _c("img", {
                            staticClass: "mr-1",
                            attrs: {
                              src: "/images/dot-arrow.svg",
                              width: "10",
                              alt: ""
                            }
                          }),
                          _vm._v(
                            "My Customer Retention Strategy\n              "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                )
              ])
            : _vm._e()
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a80474ae", module.exports)
  }
}

/***/ }),

/***/ 621:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1697)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1699)
/* template */
var __vue_template__ = __webpack_require__(1700)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a80474ae"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/templates.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a80474ae", Component.options)
  } else {
    hotAPI.reload("data-v-a80474ae", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});