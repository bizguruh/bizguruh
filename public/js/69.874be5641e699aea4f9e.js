webpackJsonp([69],{

/***/ 1502:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1503);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("8d4d70a2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5c9f1ff2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userEventsComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5c9f1ff2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userEventsComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1503:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.eventA[data-v-5c9f1ff2] {\n       font-size: 12px;\n       text-align: left;\n}\n.moS[data-v-5c9f1ff2] {\n       margin-left: 10px;\n}\n.evenT[data-v-5c9f1ff2] {\n       text-align: left;\n}\n.eventDivs[data-v-5c9f1ff2] {\n       overflow: hidden;\n       padding: 6px 10px 14px;\n}\n.monthEvent[data-v-5c9f1ff2] {\n       float: left;\n       color: #999;\n}\n.eventD[data-v-5c9f1ff2] {\n       cursor: pointer;\n}\n.eventMonth[data-v-5c9f1ff2] {\n       color: #a3c2dc;\n       font-weight: 700;\n       font-size: 14px;\n}\n.imgEvent[data-v-5c9f1ff2] {\n    -o-object-fit: cover;\n       object-fit: cover;\n}\n.priceTag[data-v-5c9f1ff2] {\n    position: absolute;\n    top: 12px;\n    background: #ffffff;\n    -webkit-box-shadow: 0 2px 3px #eee;\n            box-shadow: 0 2px 3px #eee;\n    right: -26px;\n    font-size: 14px;\n    color: #a3c2dc;\n    padding: 10px;\n    border-radius: 6px;\n}\n.eventDiv[data-v-5c9f1ff2] {\n       position: relative;\n       text-align: center;\n       border: 1px solid #f3f3f3;\n       border-radius: 4px;\n       font-size: 18px;\n       -webkit-box-shadow: 2px 3px 0px #f3f3f3;\n               box-shadow: 2px 3px 0px #f3f3f3;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_bannerEventComponent__ = __webpack_require__(1505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_bannerEventComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_bannerEventComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-events-component",
    components: {
        'app-banner': __WEBPACK_IMPORTED_MODULE_0__components_bannerEventComponent___default.a
    },
    data: function data() {
        return {
            events: [],
            date: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/api/events-page').then(function (response) {
            if (response.status === 200) {
                response.data.data.forEach(function (item) {
                    _this.$set(item.events, 'price', item.price);
                    _this.$set(item.events, 'uid', item.id);
                    _this.$set(item.events, 'coverImage', item.coverImage);
                    _this.$set(item.events, 'myDate', item.eventSchedule.length > 0 ? item.eventSchedule[0].startDay : null);
                    _this.events.push(item.events);
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
    }
});

/***/ }),

/***/ 1505:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1506)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1508)
/* template */
var __vue_template__ = __webpack_require__(1509)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8c558bd4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/bannerEventComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8c558bd4", Component.options)
  } else {
    hotAPI.reload("data-v-8c558bd4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1506:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1507);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4c0cb212", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8c558bd4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bannerEventComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8c558bd4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./bannerEventComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1507:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.displaySearch[data-v-8c558bd4] {\n    background: #a3c2dc;\n    padding: 40px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "banner-event-component"
});

/***/ }),

/***/ 1509:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bizguruhEvent" }, [
      _c("div", { staticClass: "row displaySearch" }, [
        _c("div", { staticClass: "col-md-3" }, [
          _c("input", { staticClass: "form-control", attrs: { type: "text" } })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("input", { staticClass: "form-control", attrs: { type: "text" } })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("input", { staticClass: "form-control", attrs: { type: "date" } })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("button", { staticClass: "btn" }, [
            _c("i", { staticClass: "fa fa-search" })
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8c558bd4", module.exports)
  }
}

/***/ }),

/***/ 1510:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "course-Detail" },
    [
      _c("app-banner"),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row" },
        _vm._l(_vm.events, function(event, index) {
          return _vm.events.length > 0
            ? _c(
                "div",
                { staticClass: "col-md-4 eventD" },
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        to: {
                          name: "EventSinglePage",
                          params: { id: event.uid, name: event.title }
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "eventDiv" }, [
                        _c("img", {
                          staticClass: "imgEvent",
                          attrs: { src: event.coverImage }
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "priceTag" }, [
                          _vm._v(
                            _vm._s(
                              event.price ? "₦" + event.price + ".00" : "FREE"
                            )
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "eventDivs" }, [
                          _c("div", { staticClass: "monthEvent" }, [
                            event.myDate !== null
                              ? _c("div", { staticClass: "eventMonth" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("moment")(event.myDate, "MMM")
                                    )
                                  )
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            event.myDate !== null
                              ? _c("div", [
                                  _vm._v(
                                    _vm._s(_vm._f("moment")(event.myDate, "D"))
                                  )
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "monthEvent moS" }, [
                            _c("div", { staticClass: "evenT" }, [
                              _vm._v(_vm._s(event.title))
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "eventA" }, [
                              _vm._v(
                                _vm._s(
                                  _vm._f("moment")(event.myDate, "ddd, MMM Do")
                                )
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "eventA" }, [
                              _vm._v(
                                _vm._s(event.address) +
                                  " " +
                                  _vm._s(event.city) +
                                  " " +
                                  _vm._s(event.state) +
                                  " " +
                                  _vm._s(event.country)
                              )
                            ])
                          ])
                        ])
                      ])
                    ]
                  )
                ],
                1
              )
            : _vm._e()
        }),
        0
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5c9f1ff2", module.exports)
  }
}

/***/ }),

/***/ 577:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1502)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1504)
/* template */
var __vue_template__ = __webpack_require__(1510)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5c9f1ff2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userEventsComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5c9f1ff2", Component.options)
  } else {
    hotAPI.reload("data-v-5c9f1ff2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});