webpackJsonp([79],{

/***/ 1937:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1938);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("65dbda57", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7b03de16\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./loginForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7b03de16\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./loginForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1938:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nbody[data-v-7b03de16] {\n  background: #a4c2db;\n}\n.next[data-v-7b03de16] {\n  background: #a4c2db;\n  padding:5px 30px;\n    height:44px;\n}\n.prev[data-v-7b03de16] {\n  background: #333;\n    padding:5px 30px;\n      height:44px;\n}\n.previous[data-v-7b03de16] {\n  background: #333;\n  padding: 5px 30px;\n    height:44px;\n}\n/* .reset {\n  color: #fff !important;\n} */\n.reset[data-v-7b03de16]:hover {\n  text-decoration: underline;\n  cursor: pointer;\n}\n.container[data-v-7b03de16] {\n  padding: 90px 0 30px !important;\n  text-transform: none;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-weight: normal;\n  min-height: 100vh;\n}\n.form-control[data-v-7b03de16] {\n  border-top: unset;\n  border-left: unset;\n  border-right: unset;\n  height:44px;\n  /* background-color:lightsteelblue; */\n}\n.form-control[data-v-7b03de16]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-7b03de16]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-7b03de16]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-7b03de16]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-7b03de16]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-7b03de16]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\nlabel[data-v-7b03de16] {\n  font-weight: normal;\n}\n.input-group[data-v-7b03de16] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n  flex-wrap: wrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n.text_content[data-v-7b03de16] {\n  width: 100%;\n  padding: 15px;\n}\n/* .form-control {\n  background: rgba(255, 255, 255, 0.7);\n} */\n.form-check-label[data-v-7b03de16] {\n  display: -webkit-box !important;\n  display: -ms-flexbox !important;\n  display: flex !important;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n/* .form-check-input{\n  margin-right:0;\n} */\n.input-container[data-v-7b03de16] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n.switch[data-v-7b03de16]:hover {\n  text-decoration: underline;\n}\n.valid[data-v-7b03de16] {\n  position: absolute;\n  right: 4px;\n  top: 35%;\n}\n.btologin[data-v-7b03de16] {\n  margin: 10px 0 0 10px;\n  font-weight: bold;\n}\n.v-progress-circular[data-v-7b03de16] {\n  margin: 0 1rem;\n}\n.fa-red[data-v-7b03de16] {\n  color: red;\n}\n.fa-green[data-v-7b03de16] {\n  color: green;\n}\n.primary[data-v-7b03de16] {\n  color: #a4c2db;\n}\n.center[data-v-7b03de16] {\n  text-align: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.signUp[data-v-7b03de16] {\n  position: relative;\n  background-image: url(/images/login.jpg);\n  padding: 0;\n  background-position: center;\n  background-size: 100% 100%;\n  width: 100%;\n}\n.overlay[data-v-7b03de16] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.signup-head[data-v-7b03de16] {\n  font-size: 24px;\n}\n.head[data-v-7b03de16] {\n  font-size: 24px;\n  font-weight: bolder;\n}\na.login-home.router-link-active[data-v-7b03de16] {\n  display: none;\n  margin-left: -20px;\n  color: #ffffff !important;\n  cursor: pointer;\n  font-size: 16px;\n}\na.signup-home.router-link-active[data-v-7b03de16] {\n  display: none;\n  margin-left: -20px;\n  color: #ffffff !important;\n  cursor: pointer;\n  font-size: 16px;\n}\n.mobile[data-v-7b03de16],\n.mobile-register-text[data-v-7b03de16] {\n  display: none;\n}\n.loginPagge[data-v-7b03de16] {\n  background-color: white;\n  padding: 20px;\n}\n.text-c[data-v-7b03de16] {\n  z-index: 2;\n  color: white;\n}\n.registerPagge[data-v-7b03de16] {\n  background-color: white;\n  padding: 0;\n  font-size: 14px;\n}\n.registerPagge[data-v-7b03de16] {\n  padding: 60px 40px 20px;\n}\n.text_content[data-v-7b03de16] {\n  padding: 0;\n}\nh2[data-v-7b03de16] {\n  font-size: 42px;\n  text-align: center;\n  margin-bottom: 32px;\n}\n.pp[data-v-7b03de16] {\n  text-align: center;\n  font-size: 18px;\n}\n.btn-signup[data-v-7b03de16] {\n  background-color: #a4c2db !important;\n  color: #ffffff;\n  padding: 5px 20px;\n  border-radius: 5px;\n  text-transform: capitalize;\n  font-size: 16px;\n  font-weight: bold;\n  height:44px;\n}\n.btn-logIn[data-v-7b03de16] {\n  color: #fff;\n  background-color: #a4c2db !important;\n\n  text-transform: capitalize;\n  border: 1px solid #fff;\n  position: relative;\n  padding: 5px 40px;\n  font-weight: bold;\n    height:44px;\n}\n.btn-back[data-v-7b03de16] {\n  display: none;\n  color: #000000;\n  font-size: 14px;\n  text-transform: none;\n    height:44px;\n}\n.terms[data-v-7b03de16] {\n  margin-bottom: 16px;\n  font-size: 12px;\n}\n.btn-join[data-v-7b03de16] {\n  font-size: 16px;\n  text-transform: capitalize;\n  padding: 0 4px;\n  background-color: transparent;\n  color: #a4c2db;\n  font-weight: bold;\n    height:44px;\n}\n.btn-join[data-v-7b03de16]:hover {\n  color: #0000;\n}\n.signUp-button[data-v-7b03de16] {\n  margin: 40px 0 0 0;\n  text-align: center;\n    height:44px;\n}\na.home_button[data-v-7b03de16] {\n  color: white;\n  margin: 10px;\n}\n.login_inner[data-v-7b03de16] {\n  margin-top: 10px;\n  height: 100vh;\n}\n@media screen and (max-width: 768px) {\n.terms[data-v-7b03de16] {\n    text-align: center;\n}\n.text_content[data-v-7b03de16] {\n  text-align: center;\n}\n.input-group-text[data-v-7b03de16] {\n    font-size: 0.85rem;\n}\n.btn-logIn[data-v-7b03de16] {\n    color: #fff;\n    background-color: #333 !important;\n    margin-bottom: 16px;\n    margin-right: 0 !important;\n    width:100%\n}\n.next[data-v-7b03de16] {\n    background: #333;\n    color: #fff !important;\n     width:100%\n}\n.prev[data-v-7b03de16] {\n}\n.previous[data-v-7b03de16] {\n    background: #fff;\n    color: #333 !important;\n    margin-right: 0 !important;\n    margin-bottom: 16px;\n}\n.signup-head[data-v-7b03de16] {\n    font-size: 18px;\n}\n.head[data-v-7b03de16] {\n    font-size: 18px;\n}\n.signUp.signUpActive[data-v-7b03de16] {\n    padding-top: 130px;\n    padding-left: 30px;\n    padding-right: 30px;\n}\n.mobile-register-text[data-v-7b03de16] {\n    display: inline-block;\n    margin-top: 40px;\n}\n.mobile[data-v-7b03de16] {\n    display: inline;\n    text-align: center;\n}\n.btn-back[data-v-7b03de16] {\n    display: inline-block;\n}\n.pp[data-v-7b03de16] {\n    font-size: 14px;\n    text-align: start;\n}\na.home_button[data-v-7b03de16] {\n    color: #ffffff;\n    margin-left: 20px;\n    margin-top: 8px;\n}\n.signUpActive[data-v-7b03de16] {\n    display: block;\n    /* margin-top: 30px; */\n}\n.loginActive[data-v-7b03de16] {\n    display: none;\n}\n.h-100[data-v-7b03de16] {\n    min-height: 100% !important;\n}\n.btologin[data-v-7b03de16] {\n    margin: 0;\n}\n.signup-head[data-v-7b03de16] {\n    font-size: 18px;\n}\n.head[data-v-7b03de16] {\n    font-size: 18;\n}\n.terms p[data-v-7b03de16] {\n    font-size: 9px;\n    text-align: center;\n}\n.btn-join[data-v-7b03de16] {\n    font-size: 14px;\n    /* font-weight: bold; */\n    padding: 0;\n    color: #0f283c;\n    height:auto;\n}\n.container[data-v-7b03de16] {\n    padding: 30px 0 0 !important;\n}\n.login_area[data-v-7b03de16] {\n    width: 100%;\n}\n.text-biz[data-v-7b03de16] {\n    color: #0f283c !important;\n    font-weight: bold;\n}\n.loginPagge[data-v-7b03de16] {\n    background: #d1e8fd;\n    background-size: cover;\n    background-position: center center;\n}\n.registerPagge[data-v-7b03de16] {\n    background: #d1e8fd;\n    background-size: cover;\n    background-position: center center;\n    padding: 60px 20px 20px;\n}\n.signUp[data-v-7b03de16] {\n    display: none !important;\n}\n.signUp.signUpActive[data-v-7b03de16] {\n    padding-top: 130px;\n    padding-left: 30px;\n    padding-right: 30px;\n    margin-top: 30px;\n}\na.home_button[data-v-7b03de16] {\n    margin-left: 20px;\n    margin-top: 8px;\n    color: #ffffff;\n}\na.login-home.router-link-active[data-v-7b03de16] {\n    display: block;\n}\na.signup-home.router-link-active[data-v-7b03de16] {\n    display: block;\n}\n.mobile-register-text[data-v-7b03de16] {\n    display: inline-block;\n    margin-top: 20px;\n}\n.btn-back[data-v-7b03de16] {\n    display: inline-block;\n}\n.pp[data-v-7b03de16] {\n    font-size: 14px;\n    line-height: 1.6;\n    text-align: center;\n    font-weight: normal;\n}\n.signUpActive[data-v-7b03de16] {\n    display: block;\n}\n.loginActive[data-v-7b03de16] {\n    display: none;\n}\n.btn-logIn[data-v-7b03de16] {\n    color: #fff;\n    background-color: #333 !important;\n    margin-bottom: 16px;\n    margin-right: 0 !important;\n}\n.fa-home[data-v-7b03de16] {\n    color: #a4c2db;\n}\n.do-column[data-v-7b03de16] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n}\n@media screen and (max-width: 325px) {\n.do-column[data-v-7b03de16] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1939:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__phoneCodes__ = __webpack_require__(714);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-authenticate-component",
  data: function data() {
    return {
      myDials: [],
      errorCheck: false,
      isLoading: false,
      status: "",
      registerToggle: false,
      count: 0,
      vendor_username: null,
      show: false,
      ip: "",
      newUser: false,
      subscribeNews: true,
      user: {
        name: "",
        email: "",
        password: "",
        // password_confirmation:,
        phone_code: "+234",
        phone_no: null,
        specialToken: "",
        business_name: "",
        business_type: "retailer"
      },
      userLogin: {
        email: null,
        password: null
      },
      validToken: null,
      first: true,
      second: false,
      third: false,
      isNumber: false
    };
  },

  created: function created() {
    this.check();
    this.myDials = Object(__WEBPACK_IMPORTED_MODULE_1__phoneCodes__["a" /* dialCodes */])();
  },

  watch: {
    $route: "check"
  },

  mounted: function mounted() {
    var customerUser = JSON.parse(localStorage.getItem("authUser"));
    if (customerUser != null) {
      this.profile = true;
    } else {
      this.profile = false;
      if (this.$route.params.name === "register") {
        this.registerToggle = true;
      } else if (this.$route.params.name === "login") {
        this.registerToggle = false;
      }
    }
    this.getCount();
    if (this.count === 1) {
      this.newUser = true;
    } else {
      this.newUser = false;
    }
  },

  methods: {
    toggleEmail: function toggleEmail() {
      this.isNumber = !this.isNumber;
      if (this.isNumber === true) {
        this.user.email = "abc@email.com";
        this.user.phone_no = null;
        this.userLogin.email = null;
      }
      if (this.isNumber === false) {
        this.user.email = "";
        this.user.phone_no = 8799;
        this.userLogin.email = "";
      }
    },
    changeTab: function changeTab(stage) {
      switch (stage) {
        case "first":
          this.first = true;
          this.second = this.third = false;

          break;

        case "second":
          if (this.user.name !== '' && this.user.business_name !== '') {
            this.second = true;
            this.first = this.third = false;
          } else {
            this.$toasted.error('Fill all fields');
          }

          break;

        case "third":
          if ((this.user.email !== '' || this.user.phone_no !== null) && this.user.password !== '') {
            this.third = true;
            this.second = this.first = false;
          } else {
            this.$toasted.error('Fill all fields');
          }

          break;

        default:
          break;
      }
    },
    subscribe: function subscribe(email, name) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        email: user.email,
        first_name: name,
        last_name: name
      };
      axios.post("/api/subscribe", data).then(function (response) {
        if (response.status === 200) {}
        {}
      });
    },
    generateToken: function generateToken(l) {
      var text = "";
      var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < l; i++) {
        text += char_list.charAt(Math.floor(Math.random() * char_list.length));
      }

      this.userToken = "USR-" + text;
      return "USR-" + text;
    },
    check: function check() {
      if (this.$route.params.name === "login") {
        this.loginToggle = true;
        this.registerToggle = false;
      } else if (this.$route.params.name === "register") {
        this.registerToggle = true;
        this.loginToggle = false;
      } else {}
    },
    checkToken: function checkToken() {
      var _this = this;

      if (this.user.specialToken !== "") {
        this.validToken = "checking";

        axios.post("/api/checkToken/" + this.user.specialToken).then(function (response) {
          if (response.status === 200) {
            if (response.data.message === "invalid") {
              _this.validToken = "invalid";
              return _this.validToken;
            } else if (response.data.message === "valid") {
              _this.validToken = "valid";
              return _this.validToken;
            } else {
              _this.validToken = "limit";
              _this.$toasted.error("Token maximum limit ");

              return _this.validToken;
            }
          }
        });
      } else {
        this.validToken = null;
      }
    },
    toggleSignin: function toggleSignin() {
      this.$router.push({ name: "auth", params: { name: "login" } });
    },
    toggleSignup: function toggleSignup() {
      this.$router.push({ name: "auth", params: { name: "register" } });
    },
    getCount: function getCount() {
      var _this2 = this;

      axios.get("/api/count/" + this.userLogin.email).then(function (response) {
        _this2.count = response.data.count;
      });
    },
    register: function register() {
      var _this3 = this;

      if (this.subscribeNews === false) {
        localStorage.setItem("news", "show");
      }
      this.show = true;

      if (this.user.specialToken === "") {
        axios.post("/api/customer/register", this.user).then(function (response) {
          if (response.status === 200) {
            _this3.show = false;
            _this3.$toasted.success("Registration successful, Please Login to continue");
            axios.post("/api/addcount/" + _this3.user.email).then(function (response) {});
            _this3.$router.push({ name: "LoginForm" });
          }
        }).catch(function (error) {
          for (var key in error.response.data.errors) {
            _this3.$toasted.error(error.response.data.errors[key][0]);
            _this3.show = false;
          }
        });
      } else {
        axios.post("/api/checkToken/" + this.user.specialToken).then(function (response) {
          if (response.status === 200) {
            if (response.data.message === "invalid") {
              _this3.$toasted.error("Invalid token provided");
              _this3.validToken = "invalid";
            } else if (response.data.message === "limit") {
              _this3.$toasted.error("Token maximum limit");
              _this3.validToken = "invalid";
            } else if (response.data.message === "valid") {
              axios.post("/api/customer/register", _this3.user).then(function (response) {
                if (response.status === 200) {
                  if (subscribeNews) {
                    _this3.subscribe(user.email, user.name);
                  }
                  _this3.show = false;
                  _this3.$toasted.success("Registration successful, Please Login to continue");
                  axios.post("/api/addcount/" + _this3.user.email).then(function (response) {
                    if (response.status === 200) {}
                  });
                  _this3.$router.push({ name: "LoginForm" });
                }
              }).catch(function (error) {
                _this3.show = false;
                for (var key in error.response.data.errors) {
                  _this3.$toasted.error(error.response.data.errors[key][0]);
                }
              });
            } else {
              _this3.$toasted.error("Invalid token provided");
              _this3.show = false;
            }
          }
        });
      }
    },
    login: function login(userLogin) {
      var _this4 = this;

      this.isLoading = true;
      var ipDetails = {
        email: this.userLogin.email,
        login_token: this.generateToken(10)
      };
      localStorage.setItem("login_token", ipDetails.login_token);
      axios.post("/api/ip", JSON.parse(JSON.stringify(ipDetails))).then(function (response) {
        if (response.status === 200) {
          if (response.data.status === "login") {}
        }
      });

      var data = {
        client_id: 2,
        client_secret: "UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C",
        grant_type: "password",
        username: userLogin.email,
        password: userLogin.password,
        theNewProvider: "api"
      };
      var authUser = {};
      axios.post("/oauth/token", data).then(function (response) {
        authUser.access_token = response.data.access_token;
        authUser.refresh_token = response.data.refresh_token;
        localStorage.setItem("authUser", JSON.stringify(authUser));
        axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_0__config__["b" /* getCustomerHeader */])() }).then(function (response) {

          if (response.status === 200) {
            authUser.id = response.data.id;
            authUser.email = response.data.email;
            authUser.name = response.data.name;
            authUser.vendor_user_id = response.data.vendor_user_id;
            if (response.data.business_name !== null) {
              authUser.business_name = response.data.business_name;
            }
            if (response.data.business_name !== null) {
              authUser.business_type = response.data.business_type;
            }
            if (response.data.age !== null) {
              authUser.age = response.data.age;
            }
            if (response.data.gender !== null) {
              authUser.gender = response.data.gender;
            }
            if (response.data.location !== null) {
              authUser.location = response.data.location;
            }
            if (response.data.logo !== null) {
              authUser.logo = response.data.logo;
            }
            axios.get("/api/get-all-vendor").then(function (response) {
              if (response.status === 200) {
                response.data.forEach(function (vendor) {
                  if (Number(vendor.id) === Number(authUser.vendor_user_id)) {
                    _this4.vendor_username = vendor.username;
                  }
                });
                authUser.vendor_username = _this4.vendor_username;
                localStorage.setItem("authUser", JSON.stringify(authUser));
                _this4.isLoading = false;
                _this4.status = true;
                _this4.$emit("authenicatedBar", true);

                if (_this4.$route.query.watch === "media") {
                  _this4.$router.go(-1);
                } else if (_this4.$route.query.check === "guides") {
                  _this4.$router.go(-1);
                } else {
                  if (authUser.vendor_user_id === 0) {
                    Engagespot.identifyUser(authUser.id);
                    if (_this4.$route.query.redirect) {
                      _this4.$router.go(-1);
                    } else if (_this4.$route.query.redirectFrom) {
                      _this4.$router.go(-1);
                    } else {
                      _this4.$router.push({ name: "InsightPage" });
                    }
                  } else {
                    Engagespot.identifyUser(authUser.vendor_user_id);
                    _this4.$router.push({
                      name: "PartnersProfile",
                      params: { username: _this4.vendor_username }
                    });
                    _this4.$toasted.success("Successfully login");
                  }
                }
              }
            });
          } else {
            _this4.$toasted.error("Invalid user name and password");
            _this4.errorCheck = true;
          }
        }).catch(function (error) {
          var errors = Object.values(error.response.data.error);
          errors.forEach(function (item) {
            console.log("vvv");
            _this4.$toasted.error(item[0]);
            console.log("fjfjf");
          });
          // this.isLoading = false;
          // this.status = false;
        });
      }).catch(function (error) {
        _this4.errorCheck = true;
        _this4.isLoading = false;
        //this.status = true;
      });
    }
  }
});

/***/ }),

/***/ 1940:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid p-0" }, [
    _c("div", { staticClass: "login_inner" }, [
      _c("div", { staticClass: "d-flex h-100" }, [
        !_vm.newUser
          ? _c(
              "div",
              {
                staticClass:
                  "col-lg-8 col-md-8 col-sm-12 col-xs-12 signUp d-flex align-items-center justify-content-center"
              },
              [
                _c("div", { staticClass: "overlay" }),
                _vm._v(" "),
                _c("div", { staticClass: "text-c" }, [
                  _c("h2", [_vm._v("Welcome to our BizTribe!")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "pp" }, [
                    _vm._v(
                      "Enjoy your 30-day free BizLibrary access, where resources can be archived and accessed at your convenience."
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "signUp-button" }, [
                    !_vm.registerToggle
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-signup shadow-sm rounded",
                            on: { click: _vm.toggleSignup }
                          },
                          [_vm._v("Join Our BizTribe")]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.registerToggle
                      ? _c(
                          "button",
                          {
                            staticClass: "btn btn-signup",
                            on: { click: _vm.toggleSignin }
                          },
                          [_vm._v("Sign In")]
                        )
                      : _vm._e()
                  ])
                ])
              ]
            )
          : _c(
              "div",
              {
                staticClass:
                  "col-lg-8 col-md-8 col-sm-12 col-xs-12 signUp d-flex justify-content-center align-items-center"
              },
              [_vm._m(0)]
            ),
        _vm._v(" "),
        !_vm.registerToggle
          ? _c(
              "div",
              {
                staticClass:
                  "col-lg-4 col-md-4 col-sm-12 col-xs-12 loginPagge d-flex justify-content-center align-items-center"
              },
              [
                _c(
                  "form",
                  {
                    staticClass: "text_content",
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.login(_vm.userLogin)
                      }
                    }
                  },
                  [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("h2", { staticClass: "head" }, [_vm._v("Log in")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("div", { staticClass: "form-group" }, [
                        !_vm.isNumber
                          ? _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.userLogin.email,
                                  expression: "userLogin.email"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                autocorrect: "off",
                                autocapitalize: "off",
                                type: "email",
                                placeholder: " Email address"
                              },
                              domProps: { value: _vm.userLogin.email },
                              on: {
                                change: _vm.getCount,
                                focus: function($event) {
                                  _vm.errorCheck = false
                                },
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.userLogin,
                                    "email",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.isNumber
                          ? _c("div", { staticClass: "input-group" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "input-group-prepend",
                                  staticStyle: { width: "25%" }
                                },
                                [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.user.phone_code,
                                          expression: "user.phone_code"
                                        }
                                      ],
                                      staticClass: "input-group-text",
                                      attrs: { required: "" },
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.user,
                                            "phone_code",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    _vm._l(_vm.myDials, function(dial, idx) {
                                      return _c(
                                        "option",
                                        {
                                          key: idx,
                                          domProps: { value: dial.dial_code }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              dial.dial_code + " - " + dial.name
                                            )
                                          )
                                        ]
                                      )
                                    }),
                                    0
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "input-group-append",
                                  staticStyle: { width: "75%" }
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.userLogin.email,
                                        expression: "userLogin.email"
                                      }
                                    ],
                                    staticClass:
                                      "form-control input-group-text text-left",
                                    attrs: {
                                      required: "",
                                      type: "number",
                                      "aria-describedby": "helpId",
                                      placeholder: "Enter phone no",
                                      minlength: "6",
                                      maxlength: "10"
                                    },
                                    domProps: { value: _vm.userLogin.email },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.userLogin,
                                          "email",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]
                              )
                            ])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.userLogin.password,
                              expression: "userLogin.password"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            id: "password",
                            type: "password",
                            placeholder: "Enter password"
                          },
                          domProps: { value: _vm.userLogin.password },
                          on: {
                            focus: function($event) {
                              _vm.errorCheck = false
                            },
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.userLogin,
                                "password",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", [
                      _vm.errorCheck
                        ? _c("div", { staticClass: "errorCheck" }, [
                            _vm._v("Please enter a valid credential...")
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-logIn",
                          attrs: { type: "submit" }
                        },
                        [
                          _vm._v("\n              Log In\n              "),
                          _vm.isLoading
                            ? _c("span", {
                                staticClass:
                                  "spinner-grow spinner-grow-sm text-light"
                              })
                            : _vm._e()
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-center" }, [
                      _vm.isNumber
                        ? _c(
                            "small",
                            {
                              staticClass: "mx-auto switch",
                              attrs: { type: "button" },
                              on: { click: _vm.toggleEmail }
                            },
                            [_vm._v("Log in with email address")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.isNumber
                        ? _c(
                            "small",
                            {
                              staticClass: "mx-auto switch",
                              attrs: { type: "button" },
                              on: { click: _vm.toggleEmail }
                            },
                            [_vm._v("Log in with phone number")]
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "text-center" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "reset text-muted",
                            attrs: { to: "/reset-password" }
                          },
                          [_c("small", [_vm._v("Forgot password ?")])]
                        )
                      ],
                      1
                    )
                  ]
                )
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.registerToggle
          ? _c(
              "div",
              {
                staticClass:
                  "col-lg-4 col-md-4 col-sm-12 col-xs-12 registerPagge d-flex justify-content-center align-items-center"
              },
              [
                _c(
                  "form",
                  {
                    staticClass: "text_content",
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.register($event)
                      }
                    }
                  },
                  [
                    _c("h2", { staticClass: "head" }, [_vm._v("Sign Up")]),
                    _vm._v(" "),
                    _vm.first
                      ? _c("span", [
                          _c("div", { staticClass: "form-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.name,
                                  expression: "user.name"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text", placeholder: "Full name" },
                              domProps: { value: _vm.user.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.user,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.business_name,
                                  expression: "user.business_name"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                min: "4",
                                placeholder: "Business name"
                              },
                              domProps: { value: _vm.user.business_name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.user,
                                    "business_name",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "mb-3" }, [
                            _c(
                              "button",
                              {
                                staticClass:
                                  "mx-auto px-4 text-white rounded-sm next ",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeTab("second")
                                  }
                                }
                              },
                              [_vm._v("Next")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "text-center" }, [
                            _c("small", { staticClass: "mobile" }, [
                              _vm._v(
                                "\n                Already have an account ?\n                "
                              ),
                              _c(
                                "span",
                                {
                                  staticClass: "btologin",
                                  on: { click: _vm.toggleSignin }
                                },
                                [_vm._v("Sign in")]
                              )
                            ])
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.second
                      ? _c("span", [
                          _c("div", { staticClass: "form-group" }, [
                            !_vm.isNumber
                              ? _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.user.email,
                                      expression: "user.email"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    autocorrect: "off",
                                    autocapitalize: "off",
                                    type: "email",
                                    placeholder: " Email address"
                                  },
                                  domProps: { value: _vm.user.email },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.user,
                                        "email",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.isNumber
                              ? _c("div", { staticClass: "input-group" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "input-group-prepend",
                                      staticStyle: { width: "25%" }
                                    },
                                    [
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.user.phone_code,
                                              expression: "user.phone_code"
                                            }
                                          ],
                                          staticClass: "input-group-text",
                                          attrs: { required: "" },
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.$set(
                                                _vm.user,
                                                "phone_code",
                                                $event.target.multiple
                                                  ? $$selectedVal
                                                  : $$selectedVal[0]
                                              )
                                            }
                                          }
                                        },
                                        _vm._l(_vm.myDials, function(
                                          dial,
                                          idx
                                        ) {
                                          return _c(
                                            "option",
                                            {
                                              key: idx,
                                              domProps: {
                                                value: dial.dial_code
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  dial.dial_code +
                                                    " - " +
                                                    dial.name
                                                )
                                              )
                                            ]
                                          )
                                        }),
                                        0
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass: "input-group-append",
                                      staticStyle: { width: "75%" }
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.user.phone_no,
                                            expression: "user.phone_no"
                                          }
                                        ],
                                        staticClass:
                                          "form-control input-group-text text-left",
                                        attrs: {
                                          required: "",
                                          type: "number",
                                          "aria-describedby": "helpId",
                                          placeholder: "Enter phone no",
                                          minlength: "6",
                                          maxlength: "10"
                                        },
                                        domProps: { value: _vm.user.phone_no },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.user,
                                              "phone_no",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]
                                  )
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.password,
                                  expression: "user.password"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "password",
                                min: "6",
                                placeholder: "Password"
                              },
                              domProps: { value: _vm.user.password },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.user,
                                    "password",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "d-flex" }, [
                            _c(
                              "button",
                              {
                                staticClass:
                                  "btn-block m-2 text-white rounded-sm prev",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeTab("first")
                                  }
                                }
                              },
                              [_vm._v("Previous")]
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass:
                                  "btn-block m-2 text-white rounded-sm next",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeTab("third")
                                  }
                                }
                              },
                              [_vm._v("Next")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "text-center mt-3" }, [
                            _vm.isNumber
                              ? _c(
                                  "small",
                                  {
                                    staticClass: "mx-auto switch",
                                    attrs: { type: "button" },
                                    on: { click: _vm.toggleEmail }
                                  },
                                  [_vm._v("Sign up with email address")]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            !_vm.isNumber
                              ? _c(
                                  "small",
                                  {
                                    staticClass: "mx-auto switch",
                                    attrs: { type: "button" },
                                    on: { click: _vm.toggleEmail }
                                  },
                                  [_vm._v("Sign up with phone number")]
                                )
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "text-center mt-3" }, [
                            _c("small", { staticClass: "mobile" }, [
                              _vm._v(
                                "\n                Already have an account ?\n                "
                              ),
                              _c(
                                "span",
                                {
                                  staticClass: "btologin",
                                  on: { click: _vm.toggleSignin }
                                },
                                [_vm._v("Sign in")]
                              )
                            ])
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.third
                      ? _c("span", [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { staticClass: "mr-3" }, [
                              _vm._v("I am a ?")
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "form-check form-check-inline" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "form-check-label mr-2" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.user.business_type,
                                          expression: "user.business_type"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "radio",
                                        value: "retailer"
                                      },
                                      domProps: {
                                        checked: _vm._q(
                                          _vm.user.business_type,
                                          "retailer"
                                        )
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.$set(
                                            _vm.user,
                                            "business_type",
                                            "retailer"
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" Retailer\n                ")
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  { staticClass: "form-check-label mr-2" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.user.business_type,
                                          expression: "user.business_type"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "radio",
                                        value: "manufacturer"
                                      },
                                      domProps: {
                                        checked: _vm._q(
                                          _vm.user.business_type,
                                          "manufacturer"
                                        )
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.$set(
                                            _vm.user,
                                            "business_type",
                                            "manufacturer"
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" Manufacturer\n                ")
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  { staticClass: "form-check-label" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.user.business_type,
                                          expression: "user.business_type"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: { type: "radio", value: "none" },
                                      domProps: {
                                        checked: _vm._q(
                                          _vm.user.business_type,
                                          "none"
                                        )
                                      },
                                      on: {
                                        change: function($event) {
                                          return _vm.$set(
                                            _vm.user,
                                            "business_type",
                                            "none"
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" None\n                ")
                                  ]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "" } }, [
                              _vm._v(
                                "   Do you wish to subscribe to our newsletters?"
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "form-check form-check-inline" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "form-check-label mr-3" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.subscribeNews,
                                          expression: "subscribeNews"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: { type: "radio", checked: "" },
                                      domProps: {
                                        value: true,
                                        checked: _vm._q(_vm.subscribeNews, true)
                                      },
                                      on: {
                                        change: function($event) {
                                          _vm.subscribeNews = true
                                        }
                                      }
                                    }),
                                    _vm._v(
                                      "\n                  Yes\n                "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  { staticClass: "form-check-label" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.subscribeNews,
                                          expression: "subscribeNews"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: { type: "radio" },
                                      domProps: {
                                        value: false,
                                        checked: _vm._q(
                                          _vm.subscribeNews,
                                          false
                                        )
                                      },
                                      on: {
                                        change: function($event) {
                                          _vm.subscribeNews = false
                                        }
                                      }
                                    }),
                                    _vm._v(
                                      "\n                  No\n                "
                                    )
                                  ]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("div", { staticClass: "input-container" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.user.specialToken,
                                    expression: "user.specialToken"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder:
                                    "Do you have a token? (optional)",
                                  maxlength: "9",
                                  "aria-describedby": "helpId"
                                },
                                domProps: { value: _vm.user.specialToken },
                                on: {
                                  keyup: _vm.checkToken,
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.user,
                                      "specialToken",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "terms text-muted" }, [
                            _c(
                              "small",
                              { staticClass: "text-muted" },
                              [
                                _vm._v(
                                  "\n                By joining now, you agree to our\n                "
                                ),
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "text-biz",
                                    attrs: { to: "/terms" }
                                  },
                                  [_vm._v("Terms & Services")]
                                ),
                                _vm._v(" "),
                                _c("span", [_vm._v("and")]),
                                _vm._v(" "),
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "text-biz",
                                    attrs: { to: "/policies" }
                                  },
                                  [_vm._v("Privacy Policy")]
                                )
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "d-flex do-column justify-content-center"
                            },
                            [
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "text-white rounded-sm mr-3 previous",
                                  attrs: { type: "button" },
                                  on: {
                                    click: function($event) {
                                      return _vm.changeTab("second")
                                    }
                                  }
                                },
                                [_vm._v("Previous")]
                              ),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-logIn mr-2",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                Join Our BizTribe\n                "
                                  ),
                                  _vm.show
                                    ? _c("span", {
                                        staticClass:
                                          "spinner-grow spinner-grow-sm text-light"
                                      })
                                    : _vm._e()
                                ]
                              ),
                              _vm._v(" "),
                              _c("small", { staticClass: "mobile mt-3" }, [
                                _vm._v(
                                  "\n                Already have an account ?\n                "
                                ),
                                _c(
                                  "span",
                                  {
                                    staticClass: "btologin",
                                    on: { click: _vm.toggleSignin }
                                  },
                                  [_vm._v("Sign in")]
                                )
                              ])
                            ]
                          )
                        ])
                      : _vm._e()
                  ]
                )
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("h3", { staticClass: "text-center mb-2" }, [
        _vm._v("Welcome to our BizTribe!")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "pp" }, [
        _vm._v(
          "Build your BizLibrary and enjoy having all your favourite resources in one place."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "pp" }, [_vm._v("30-day Free Access")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mobile-register-text  mb-3 p-3" }, [
      _c("h4", { staticClass: "text-center" }, [
        _vm._v("Welcome to our BizTribe!")
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "pp" }, [
        _vm._v(
          "Enjoy your 30-day free BizLibrary access, where resources can be archived and accessed at your convenience."
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7b03de16", module.exports)
  }
}

/***/ }),

/***/ 655:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1937)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1939)
/* template */
var __vue_template__ = __webpack_require__(1940)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7b03de16"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/loginForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7b03de16", Component.options)
  } else {
    hotAPI.reload("data-v-7b03de16", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return dialCodes; });
var dialCodes = function dialCodes() {
    var dials = [{ name: "Afghanistan", dial_code: "+93", code: "AF" }, { name: "Albania", dial_code: "+355", code: "AL" }, { name: "Algeria", dial_code: "+213", code: "DZ" }, { name: "AmericanSamoa", dial_code: "+1 684", code: "AS" }, { name: "Andorra", dial_code: "+376", code: "AD" }, { name: "Angola", dial_code: "+244", code: "AO" }, { name: "Anguilla", dial_code: "+1 264", code: "AI" }, { name: "Antarctica", dial_code: "+672", code: "AQ" }, { name: "Antigua and Barbuda", dial_code: "+1268", code: "AG" }, { name: "Argentina", dial_code: "+54", code: "AR" }, { name: "Armenia", dial_code: "+374", code: "AM" }, { name: "Aruba", dial_code: "+297", code: "AW" }, { name: "Australia", dial_code: "+61", code: "AU" }, { name: "Austria", dial_code: "+43", code: "AT" }, { name: "Azerbaijan", dial_code: "+994", code: "AZ" }, { name: "Bahamas", dial_code: "+1 242", code: "BS" }, { name: "Bahrain", dial_code: "+973", code: "BH" }, { name: "Bangladesh", dial_code: "+880", code: "BD" }, { name: "Barbados", dial_code: "+1 246", code: "BB" }, { name: "Belarus", dial_code: "+375", code: "BY" }, { name: "Belgium", dial_code: "+32", code: "BE" }, { name: "Belize", dial_code: "+501", code: "BZ" }, { name: "Benin", dial_code: "+229", code: "BJ" }, { name: "Bermuda", dial_code: "+1 441", code: "BM" }, { name: "Bhutan", dial_code: "+975", code: "BT" }, { name: "Bolivia, Plurinational State of", dial_code: "+591", code: "BO" }, { name: "Bosnia and Herzegovina", dial_code: "+387", code: "BA" }, { name: "Botswana", dial_code: "+267", code: "BW" }, { name: "Brazil", dial_code: "+55", code: "BR" }, { name: "British Indian Ocean Territory", dial_code: "+246", code: "IO" }, { name: "Brunei Darussalam", dial_code: "+673", code: "BN" }, { name: "Bulgaria", dial_code: "+359", code: "BG" }, { name: "Burkina Faso", dial_code: "+226", code: "BF" }, { name: "Burundi", dial_code: "+257", code: "BI" }, { name: "Cambodia", dial_code: "+855", code: "KH" }, { name: "Cameroon", dial_code: "+237", code: "CM" }, { name: "Canada", dial_code: "+1", code: "CA" }, { name: "Cape Verde", dial_code: "+238", code: "CV" }, { name: "Cayman Islands", dial_code: "+ 345", code: "KY" }, { name: "Central African Republic", dial_code: "+236", code: "CF" }, { name: "Chad", dial_code: "+235", code: "TD" }, { name: "Chile", dial_code: "+56", code: "CL" }, { name: "China", dial_code: "+86", code: "CN" }, { name: "Christmas Island", dial_code: "+61", code: "CX" }, { name: "Cocos (Keeling) Islands", dial_code: "+61", code: "CC" }, { name: "Colombia", dial_code: "+57", code: "CO" }, { name: "Comoros", dial_code: "+269", code: "KM" }, { name: "Congo", dial_code: "+242", code: "CG" }, {
        name: "Congo, The Democratic Republic of the",
        dial_code: "+243",
        code: "CD"
    }, { name: "Cook Islands", dial_code: "+682", code: "CK" }, { name: "Costa Rica", dial_code: "+506", code: "CR" }, { name: "Cote d'Ivoire", dial_code: "+225", code: "CI" }, { name: "Croatia", dial_code: "+385", code: "HR" }, { name: "Cuba", dial_code: "+53", code: "CU" }, { name: "Cyprus", dial_code: "+357", code: "CY" }, { name: "Czech Republic", dial_code: "+420", code: "CZ" }, { name: "Denmark", dial_code: "+45", code: "DK" }, { name: "Djibouti", dial_code: "+253", code: "DJ" }, { name: "Dominica", dial_code: "+1 767", code: "DM" }, { name: "Dominican Republic", dial_code: "+1 849", code: "DO" }, { name: "Ecuador", dial_code: "+593", code: "EC" }, { name: "Egypt", dial_code: "+20", code: "EG" }, { name: "El Salvador", dial_code: "+503", code: "SV" }, { name: "Equatorial Guinea", dial_code: "+240", code: "GQ" }, { name: "Eritrea", dial_code: "+291", code: "ER" }, { name: "Estonia", dial_code: "+372", code: "EE" }, { name: "Ethiopia", dial_code: "+251", code: "ET" }, { name: "Falkland Islands (Malvinas)", dial_code: "+500", code: "FK" }, { name: "Faroe Islands", dial_code: "+298", code: "FO" }, { name: "Fiji", dial_code: "+679", code: "FJ" }, { name: "Finland", dial_code: "+358", code: "FI" }, { name: "France", dial_code: "+33", code: "FR" }, { name: "French Guiana", dial_code: "+594", code: "GF" }, { name: "French Polynesia", dial_code: "+689", code: "PF" }, { name: "Gabon", dial_code: "+241", code: "GA" }, { name: "Gambia", dial_code: "+220", code: "GM" }, { name: "Georgia", dial_code: "+995", code: "GE" }, { name: "Germany", dial_code: "+49", code: "DE" }, { name: "Ghana", dial_code: "+233", code: "GH" }, { name: "Gibraltar", dial_code: "+350", code: "GI" }, { name: "Greece", dial_code: "+30", code: "GR" }, { name: "Greenland", dial_code: "+299", code: "GL" }, { name: "Grenada", dial_code: "+1 473", code: "GD" }, { name: "Guadeloupe", dial_code: "+590", code: "GP" }, { name: "Guam", dial_code: "+1 671", code: "GU" }, { name: "Guatemala", dial_code: "+502", code: "GT" }, { name: "Guernsey", dial_code: "+44", code: "GG" }, { name: "Guinea", dial_code: "+224", code: "GN" }, { name: "Guinea-Bissau", dial_code: "+245", code: "GW" }, { name: "Guyana", dial_code: "+595", code: "GY" }, { name: "Haiti", dial_code: "+509", code: "HT" }, { name: "Holy See (Vatican City State)", dial_code: "+379", code: "VA" }, { name: "Honduras", dial_code: "+504", code: "HN" }, { name: "Hong Kong", dial_code: "+852", code: "HK" }, { name: "Hungary", dial_code: "+36", code: "HU" }, { name: "Iceland", dial_code: "+354", code: "IS" }, { name: "India", dial_code: "+91", code: "IN" }, { name: "Indonesia", dial_code: "+62", code: "ID" }, { name: "Iran, Islamic Republic of", dial_code: "+98", code: "IR" }, { name: "Iraq", dial_code: "+964", code: "IQ" }, { name: "Ireland", dial_code: "+353", code: "IE" }, { name: "Isle of Man", dial_code: "+44", code: "IM" }, { name: "Israel", dial_code: "+972", code: "IL" }, { name: "Italy", dial_code: "+39", code: "IT" }, { name: "Jamaica", dial_code: "+1 876", code: "JM" }, { name: "Japan", dial_code: "+81", code: "JP" }, { name: "Jersey", dial_code: "+44", code: "JE" }, { name: "Jordan", dial_code: "+962", code: "JO" }, { name: "Kazakhstan", dial_code: "+7 7", code: "KZ" }, { name: "Kenya", dial_code: "+254", code: "KE" }, { name: "Kiribati", dial_code: "+686", code: "KI" }, {
        name: "Korea, Democratic People's Republic of",
        dial_code: "+850",
        code: "KP"
    }, { name: "Korea, Republic of", dial_code: "+82", code: "KR" }, { name: "Kuwait", dial_code: "+965", code: "KW" }, { name: "Kyrgyzstan", dial_code: "+996", code: "KG" }, { name: "Lao People's Democratic Republic", dial_code: "+856", code: "LA" }, { name: "Latvia", dial_code: "+371", code: "LV" }, { name: "Lebanon", dial_code: "+961", code: "LB" }, { name: "Lesotho", dial_code: "+266", code: "LS" }, { name: "Liberia", dial_code: "+231", code: "LR" }, { name: "Libyan Arab Jamahiriya", dial_code: "+218", code: "LY" }, { name: "Liechtenstein", dial_code: "+423", code: "LI" }, { name: "Lithuania", dial_code: "+370", code: "LT" }, { name: "Luxembourg", dial_code: "+352", code: "LU" }, { name: "Macao", dial_code: "+853", code: "MO" }, {
        name: "Macedonia, The Former Yugoslav Republic of",
        dial_code: "+389",
        code: "MK"
    }, { name: "Madagascar", dial_code: "+261", code: "MG" }, { name: "Malawi", dial_code: "+265", code: "MW" }, { name: "Malaysia", dial_code: "+60", code: "MY" }, { name: "Maldives", dial_code: "+960", code: "MV" }, { name: "Mali", dial_code: "+223", code: "ML" }, { name: "Malta", dial_code: "+356", code: "MT" }, { name: "Marshall Islands", dial_code: "+692", code: "MH" }, { name: "Martinique", dial_code: "+596", code: "MQ" }, { name: "Mauritania", dial_code: "+222", code: "MR" }, { name: "Mauritius", dial_code: "+230", code: "MU" }, { name: "Mayotte", dial_code: "+262", code: "YT" }, { name: "Mexico", dial_code: "+52", code: "MX" }, { name: "Micronesia, Federated States of", dial_code: "+691", code: "FM" }, { name: "Moldova, Republic of", dial_code: "+373", code: "MD" }, { name: "Monaco", dial_code: "+377", code: "MC" }, { name: "Mongolia", dial_code: "+976", code: "MN" }, { name: "Montenegro", dial_code: "+382", code: "ME" }, { name: "Montserrat", dial_code: "+1664", code: "MS" }, { name: "Morocco", dial_code: "+212", code: "MA" }, { name: "Mozambique", dial_code: "+258", code: "MZ" }, { name: "Myanmar", dial_code: "+95", code: "MM" }, { name: "Namibia", dial_code: "+264", code: "NA" }, { name: "Nauru", dial_code: "+674", code: "NR" }, { name: "Nepal", dial_code: "+977", code: "NP" }, { name: "Netherlands", dial_code: "+31", code: "NL" }, { name: "Netherlands Antilles", dial_code: "+599", code: "AN" }, { name: "New Caledonia", dial_code: "+687", code: "NC" }, { name: "New Zealand", dial_code: "+64", code: "NZ" }, { name: "Nicaragua", dial_code: "+505", code: "NI" }, { name: "Niger", dial_code: "+227", code: "NE" }, { name: "Nigeria", dial_code: "+234", code: "NG" }, { name: "Niue", dial_code: "+683", code: "NU" }, { name: "Norfolk Island", dial_code: "+672", code: "NF" }, { name: "Northern Mariana Islands", dial_code: "+1 670", code: "MP" }, { name: "Norway", dial_code: "+47", code: "NO" }, { name: "Oman", dial_code: "+968", code: "OM" }, { name: "Pakistan", dial_code: "+92", code: "PK" }, { name: "Palau", dial_code: "+680", code: "PW" }, { name: "Palestinian Territory, Occupied", dial_code: "+970", code: "PS" }, { name: "Panama", dial_code: "+507", code: "PA" }, { name: "Papua New Guinea", dial_code: "+675", code: "PG" }, { name: "Paraguay", dial_code: "+595", code: "PY" }, { name: "Peru", dial_code: "+51", code: "PE" }, { name: "Philippines", dial_code: "+63", code: "PH" }, { name: "Pitcairn", dial_code: "+872", code: "PN" }, { name: "Poland", dial_code: "+48", code: "PL" }, { name: "Portugal", dial_code: "+351", code: "PT" }, { name: "Puerto Rico", dial_code: "+1 939", code: "PR" }, { name: "Qatar", dial_code: "+974", code: "QA" }, { name: "Romania", dial_code: "+40", code: "RO" }, { name: "Russia", dial_code: "+7", code: "RU" }, { name: "Rwanda", dial_code: "+250", code: "RW" }, { name: "Réunion", dial_code: "+262", code: "RE" }, { name: "Saint Barthélemy", dial_code: "+590", code: "BL" }, {
        name: "Saint Helena, Ascension and Tristan Da Cunha",
        dial_code: "+290",
        code: "SH"
    }, { name: "Saint Kitts and Nevis", dial_code: "+1 869", code: "KN" }, { name: "Saint Lucia", dial_code: "+1 758", code: "LC" }, { name: "Saint Martin", dial_code: "+590", code: "MF" }, { name: "Saint Pierre and Miquelon", dial_code: "+508", code: "PM" }, {
        name: "Saint Vincent and the Grenadines",
        dial_code: "+1 784",
        code: "VC"
    }, { name: "Samoa", dial_code: "+685", code: "WS" }, { name: "San Marino", dial_code: "+378", code: "SM" }, { name: "Sao Tome and Principe", dial_code: "+239", code: "ST" }, { name: "Saudi Arabia", dial_code: "+966", code: "SA" }, { name: "Senegal", dial_code: "+221", code: "SN" }, { name: "Serbia", dial_code: "+381", code: "RS" }, { name: "Seychelles", dial_code: "+248", code: "SC" }, { name: "Sierra Leone", dial_code: "+232", code: "SL" }, { name: "Singapore", dial_code: "+65", code: "SG" }, { name: "Slovakia", dial_code: "+421", code: "SK" }, { name: "Slovenia", dial_code: "+386", code: "SI" }, { name: "Solomon Islands", dial_code: "+677", code: "SB" }, { name: "Somalia", dial_code: "+252", code: "SO" }, { name: "South Africa", dial_code: "+27", code: "ZA" }, {
        name: "South Georgia and the South Sandwich Islands",
        dial_code: "+500",
        code: "GS"
    }, { name: "Spain", dial_code: "+34", code: "ES" }, { name: "Sri Lanka", dial_code: "+94", code: "LK" }, { name: "Sudan", dial_code: "+249", code: "SD" }, { name: "Suriname", dial_code: "+597", code: "SR" }, { name: "Svalbard and Jan Mayen", dial_code: "+47", code: "SJ" }, { name: "Swaziland", dial_code: "+268", code: "SZ" }, { name: "Sweden", dial_code: "+46", code: "SE" }, { name: "Switzerland", dial_code: "+41", code: "CH" }, { name: "Syrian Arab Republic", dial_code: "+963", code: "SY" }, { name: "Taiwan, Province of China", dial_code: "+886", code: "TW" }, { name: "Tajikistan", dial_code: "+992", code: "TJ" }, { name: "Tanzania, United Republic of", dial_code: "+255", code: "TZ" }, { name: "Thailand", dial_code: "+66", code: "TH" }, { name: "Timor-Leste", dial_code: "+670", code: "TL" }, { name: "Togo", dial_code: "+228", code: "TG" }, { name: "Tokelau", dial_code: "+690", code: "TK" }, { name: "Tonga", dial_code: "+676", code: "TO" }, { name: "Trinidad and Tobago", dial_code: "+1 868", code: "TT" }, { name: "Tunisia", dial_code: "+216", code: "TN" }, { name: "Turkey", dial_code: "+90", code: "TR" }, { name: "Turkmenistan", dial_code: "+993", code: "TM" }, { name: "Turks and Caicos Islands", dial_code: "+1 649", code: "TC" }, { name: "Tuvalu", dial_code: "+688", code: "TV" }, { name: "Uganda", dial_code: "+256", code: "UG" }, { name: "Ukraine", dial_code: "+380", code: "UA" }, { name: "United Arab Emirates", dial_code: "+971", code: "AE" }, { name: "United Kingdom", dial_code: "+44", code: "GB" }, { name: "United States", dial_code: "+1", code: "US" }, { name: "Uruguay", dial_code: "+598", code: "UY" }, { name: "Uzbekistan", dial_code: "+998", code: "UZ" }, { name: "Vanuatu", dial_code: "+678", code: "VU" }, { name: "Venezuela, Bolivarian Republic of", dial_code: "+58", code: "VE" }, { name: "Viet Nam", dial_code: "+84", code: "VN" }, { name: "Virgin Islands, British", dial_code: "+1 284", code: "VG" }, { name: "Virgin Islands, U.S.", dial_code: "+1 340", code: "VI" }, { name: "Wallis and Futuna", dial_code: "+681", code: "WF" }, { name: "Yemen", dial_code: "+967", code: "YE" }, { name: "Zambia", dial_code: "+260", code: "ZM" }, { name: "Zimbabwe", dial_code: "+263", code: "ZW" }, { name: "Åland Islands", dial_code: "+358", code: "AX" }];
    return dials;
};

/***/ })

});