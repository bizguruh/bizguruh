webpackJsonp([151],{

/***/ 1210:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1211);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cc6903c0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-092e8f41\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsGetVendorDetailComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-092e8f41\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsGetVendorDetailComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1211:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-092e8f41] {\n    height: 100vh;\n    overflow: auto;\n}\n.detail-cat[data-v-092e8f41] {\n    padding:50px;\n    border: 1px dotted black;\n}\n.img-d[data-v-092e8f41] {\n    height: 200px;\n}\n.img-dp[data-v-092e8f41] {\n    text-align: center;\n}\n.userEna[data-v-092e8f41] {\n    margin-top: 10px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "ops-get-vendor-detail-component",
    data: function data() {
        return {
            id: this.$route.params.id,
            vendorDetail: {},
            image: '',
            token: '',
            verified: 1,
            status: 'Disabled Vendor'
        };
    },
    mounted: function mounted() {
        var _this = this;

        var ops = JSON.parse(localStorage.getItem('authOps'));
        this.token = ops.access_token;

        if (ops != null) {
            axios.get('api/vendor/' + this.id, { headers: { "Authorization": 'Bearer ' + ops.access_token } }).then(function (response) {
                _this.vendorDetail = response.data.vendorDetail;
                _this.image = "/images/productImages/" + response.data.vendorDetail.image;
                if (response.data.vendor.verified === 0) {
                    _this.status = 'Enabled Vendor';
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/ops/login');
        }
    },

    methods: {
        disableEnableVendor: function disableEnableVendor() {
            var _this2 = this;

            axios.get('api/vendor/changestatus/' + this.id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.data.status === 0) {
                    _this2.status = 'Disabled Vendor';
                    _this2.$toasted.success("Vendor Enabled");
                } else if (response.data.status === 67) {
                    _this2.status = 'Enabled Vendor';
                    _this2.$toasted.success("Vendor Disabled");
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1213:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-10 img-dp" }, [
          _c("img", { staticClass: "img-d", attrs: { src: _vm.image } }),
          _vm._v(" "),
          _c("div", [
            _c(
              "button",
              {
                staticClass: "btn btn-primary userEna",
                on: {
                  click: function($event) {
                    return _vm.disableEnableVendor()
                  }
                }
              },
              [_vm._v(_vm._s(_vm.status))]
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-2" }, [
          _vm._v(
            "\n               Memeber Since: " +
              _vm._s(
                _vm._f("moment")(
                  _vm.vendorDetail.created_at,
                  "dddd, MMMM Do YYYY"
                )
              ) +
              "\n            "
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-6 detail-cat" }, [
          _vm._v(
            "\n                Name: " +
              _vm._s(_vm.vendorDetail.firstName) +
              " " +
              _vm._s(_vm.vendorDetail.lastName) +
              "\n            "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6 detail-cat" }, [
          _vm._v(
            "\n                Store Name: " +
              _vm._s(_vm.vendorDetail.storeName) +
              "\n            "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 detail-cat" }, [
          _vm._v(
            "\n                Address: " +
              _vm._s(_vm.vendorDetail.address) +
              "\n            "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 detail-cat" }, [
          _vm._v(
            "\n                City: " +
              _vm._s(_vm.vendorDetail.city) +
              "\n            "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4 detail-cat" }, [
          _vm._v(
            "\n                Country: " +
              _vm._s(_vm.vendorDetail.country) +
              "\n            "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6 detail-cat" }, [
          _vm._v(
            "\n                Bio: " +
              _vm._s(_vm.vendorDetail.bio) +
              "\n            "
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-092e8f41", module.exports)
  }
}

/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1210)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1212)
/* template */
var __vue_template__ = __webpack_require__(1213)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-092e8f41"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/operation/components/opsGetVendorDetailComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-092e8f41", Component.options)
  } else {
    hotAPI.reload("data-v-092e8f41", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});