webpackJsonp([107],{

/***/ 1548:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1549);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4bbc8250", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9b25eb7a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userSubscriptionComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-9b25eb7a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userSubscriptionComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1549:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.subDiv[data-v-9b25eb7a] {\n    margin: 20px;\n}\n.subRow div[data-v-9b25eb7a] {\n    margin: 20px;\n}\n.btn-subscribe[data-v-9b25eb7a] {\n    background: #a3c2dc !important;\n    color: #ffffff;\n}\n.subscriptionTable[data-v-9b25eb7a] {\n    margin: 40px;\n    border: 1px solid #e0dddd;\n    padding: 20px 40px;\n    border-radius: 5px;\n    outline: 1px solid #a3c2dc;\n    -webkit-box-shadow: 0 5px 8px 10px #ccc;\n            box-shadow: 0 5px 8px 10px #ccc;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1550:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-subscription-component",
    data: function data() {
        return {
            subscriptions: [],
            token: '',
            email: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user != null) {
            this.token = user.access_token;
        }
        axios.get('/api/subscription-plans').then(function (response) {
            console.log(response);
            if (response.status === 200) {
                //this.subscriptions = response.data;
                if (user != null) {
                    _this.getUserSubscriptionPlan(response.data);
                } else {
                    _this.subscriptions = response.data;
                }
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        subscribe: function subscribe(id, type) {
            var _this2 = this;

            var data = {
                id: id,
                type: type
            };
            axios.post('/api/usersubscription', JSON.parse(JSON.stringify(data)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    window.location.href = response.data;
                }
            }).catch(function (error) {
                console.log(error.response.data.message);
                if (error.response.data.message === 'Unauthenticated.') {
                    _this2.$router.push({ name: 'auth', query: { redirect: 'subscription' } });
                }
            });
        },
        getUserSubscriptionPlan: function getUserSubscriptionPlan(data) {
            var _this3 = this;

            var subscription = [];
            var sub = [];
            axios.get('/api/user/subscription-plans', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    data.forEach(function (item) {
                        response.data.forEach(function (items) {
                            if (item.level === items.level) {
                                item.addCheck = 'check';
                            } else {
                                item.addCheck = 'notCheck';
                            }
                        });
                        subscription.push(item);
                    });
                    _this3.subscriptions = subscription;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1551:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "subDiv" }, [
    _c("h1", [_vm._v("Subscription Plan")]),
    _vm._v(" "),
    _vm.subscriptions.length > 0
      ? _c(
          "div",
          { staticClass: "row subscriptionTable" },
          _vm._l(_vm.subscriptions, function(subscription, index) {
            return _c("div", { staticClass: "col-md-4 center subRow" }, [
              _c("div", [_vm._v(_vm._s(subscription.title))]),
              _vm._v(" "),
              _c("div", [_vm._v("₦" + _vm._s(subscription.price) + ".00")]),
              _vm._v(" "),
              _c("div", [_vm._v(_vm._s(subscription.duration) + " days")]),
              _vm._v(" "),
              _c("div", {
                domProps: { innerHTML: _vm._s(subscription.moreInfo) }
              }),
              _vm._v(" "),
              subscription.addCheck === "check"
                ? _c("div", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-subscribe",
                        attrs: { disabled: "" },
                        on: {
                          click: function($event) {
                            return _vm.subscribe(
                              subscription.id,
                              subscription.level
                            )
                          }
                        }
                      },
                      [_vm._v("Subscribed")]
                    )
                  ])
                : subscription.addCheck === "notCheck"
                ? _c("div", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-subscribe",
                        on: {
                          click: function($event) {
                            return _vm.subscribe(
                              subscription.id,
                              subscription.level
                            )
                          }
                        }
                      },
                      [_vm._v("Upgrade")]
                    )
                  ])
                : _c("div", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-subscribe",
                        on: {
                          click: function($event) {
                            return _vm.subscribe(
                              subscription.id,
                              subscription.level
                            )
                          }
                        }
                      },
                      [_vm._v("Subscribe")]
                    )
                  ])
            ])
          }),
          0
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-9b25eb7a", module.exports)
  }
}

/***/ }),

/***/ 586:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1548)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1550)
/* template */
var __vue_template__ = __webpack_require__(1551)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-9b25eb7a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userSubscriptionComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9b25eb7a", Component.options)
  } else {
    hotAPI.reload("data-v-9b25eb7a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});