webpackJsonp([75],{

/***/ 1000:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-content[data-v-7e23539c] {\n  height: 100vh;\n  width: 100%;\n  padding-top: 75px;\n  padding-bottom: 100px;\n   background:#f7f8fa;\n}\n.duration[data-v-7e23539c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 400px;\n  margin: 24px auto;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.annual[data-v-7e23539c] {\n  padding: 8px 20px;\n  background: hsl(207, 46%, 20%);\n  color: white;\n  margin-left: 15px;\n  border-radius: 5px;\n}\n.annual-faded[data-v-7e23539c] {\n  background: rgb(128, 128, 128, 0.3);\n  color: rgb(255, 255, 255, 0.3);\n}\n.pricing[data-v-7e23539c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  width: 90%;\n  margin: 0 auto;\n}\n.price_box[data-v-7e23539c] {\n  height: auto;\n  width: 31%;\n  background: #f7f8fa;\n  border-radius: 4px;\n  position: relative;\n  text-align: center;\n}\nul[data-v-7e23539c],\nol[data-v-7e23539c],\nli[data-v-7e23539c] {\n  list-style: square;\n  text-align: left;\n}\nul[data-v-7e23539c] {\n  padding: 15px 35px;\n}\nli[data-v-7e23539c]{\n  padding:7px;\n  font-size: 14px;\n}\n.price_name[data-v-7e23539c] {\n  padding: 15px;\n  text-align: center;\n  background: #0a4065;\n  position: relative;\n  overflow: hidden;\n  color: white;\n  font-weight: bold;\n}\n.price_name[data-v-7e23539c]::after {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  bottom: 0;\n  left: 53%;\n}\n.price_name[data-v-7e23539c]::before {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  bottom: 0;\n  left: -53%;\n}\n.price[data-v-7e23539c] {\n  font-weight: bold;\n  font-size: 24px;\n}\n.price small[data-v-7e23539c] {\n  font-size: 14px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.oPlan[data-v-7e23539c] {\n  font-size: 36px;\n  color: white;\n  text-align: center;\n}\n.oPrice[data-v-7e23539c] {\n  font-size: 36px;\n  text-align: center;\n  color: white;\n}\n.price-overlay[data-v-7e23539c] {\n  background: rgba(255, 255, 255, 0.7);\n  position: absolute;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n    top:0;\n  bottom: 0;\n}\n.form[data-v-7e23539c] {\n  width: 30%;\n  height: auto;\n  padding: 15px;\n  position: relative;\n}\n.closePrice[data-v-7e23539c] {\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 33;\n}\n@media (max-width: 768px) {\n.main-content[data-v-7e23539c]{\n    height:auto;\n}\n.pricing[data-v-7e23539c] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.price_box[data-v-7e23539c] {\n    width: 70%;\n    margin-bottom: 40px;\n}\n}\n@media (max-width: 425px) {\n.price_box[data-v-7e23539c] {\n    width: 100%;\n}\n.annual[data-v-7e23539c]{\n    font-size:12px;\n}\n.duration[data-v-7e23539c]{\n    width:auto;\n    font-size:14px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1001:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__toggle__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      authenticate: false,
      showOverlay: false,
      plan: "",
      price: "",
      level: 0,
      duration: "",
      time: false,
      vendorSub: 1
    };
  },

  components: {
    Toggle: __WEBPACK_IMPORTED_MODULE_0__toggle___default.a
  },
  mounted: function mounted() {
    this.getVendorSub();
  },

  watch: {
    duration: "selectPlan"
  },
  methods: {
    getVendorSub: function getVendorSub() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authVendor"));

      if (user !== null) {
        axios.get("/api/vendor-sub/" + user.id).then(function (res) {
          if (res.status == 200) {
            if (res.data.status !== 'failed') {
              _this.vendorSub = res.data;
            }
          }
        });
      }
    },
    handleToggle: function handleToggle(value) {
      this.time = value;

      if (!value) {
        this.duration = "monthly";
      } else {
        this.duration = "yearly";
      }
    },
    subscribe: function subscribe(type, duration) {
      var _this2 = this;

      localStorage.removeItem("type");
      var user = JSON.parse(localStorage.getItem("authVendor"));
      if (user !== null) {
        this.authenticate = true;
      }
      if (this.authenticate) {
        var data = {
          type: type,
          duration: duration,
          level: this.level,
          subType: "expert"
        };

        axios.post("/api/vendorsubscription", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + user.access_token }
        }).then(function (response) {
          if (response.status === 200) {
            localStorage.setItem("type", "expert");
            window.location.href = response.data;
          }
        }).catch(function (error) {
          localStorage.removeItem("type");
          console.log(error.response.data.message);
          if (error.response.data.message === "Unauthenticated.") {
            _this2.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: "user-subscription" }
            });
          }
        });
      } else {
        localStorage.removeItem("type");
        this.$router.replace({
          name: "auth",
          params: { name: "login" },
          query: { redirect: "user-subscription" }
        });
      }
    },
    selectPackage: function selectPackage(value) {
      switch (value) {
        case "contributor":
          this.plan = "contributor";
          this.level = 1;
          this.selectPlan();
          this.handleOverlay();

          break;
        case "educator":
          this.plan = "educator";
          this.level = 2;
          this.selectPlan();
          this.handleOverlay();
          break;
        case "leader":
          this.plan = "leader";
          this.level = 3;
          this.selectPlan();
          this.handleOverlay();
          break;

        default:
          break;
      }
    },
    selectPlan: function selectPlan() {
      if (this.plan == "contributor" && this.duration == "monthly") {
        this.price = 0;
      } else if (this.plan == "contributor" && this.duration == "yearly") {
        this.price = 0;
      }

      if (this.plan == "educator" && this.duration == "monthly") {
        this.price = 5000;
      } else if (this.plan == "educator" && this.duration == "yearly") {
        this.price = 50000;
      }

      if (this.plan == "leader" && this.duration == "monthly") {
        this.price = 10000;
      } else if (this.plan == "leader" && this.duration == "yearly") {
        this.price = 100000;
      }
    },
    handleOverlay: function handleOverlay() {
      this.showOverlay = !this.showOverlay;
      if (!this.showOverlay) {
        this.price = 0;
      }
    }
  }
});

/***/ }),

/***/ 1002:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-content" }, [
    _c("div", { staticClass: "desc_header mb-5" }, [_vm._v("Pricing Plans")]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "duration" },
      [
        _c("span", [_vm._v("Monthly")]),
        _vm._v(" "),
        _c("Toggle", {
          staticClass: "px-2",
          on: { handleToggle: _vm.handleToggle }
        }),
        _vm._v(" "),
        _c("span", [
          _vm._v("\n      Annual\n      "),
          _c(
            "span",
            { staticClass: "annual", class: { "annual-faded": !_vm.time } },
            [_vm._v("+2 Months Free")]
          )
        ])
      ],
      1
    ),
    _vm._v(" "),
    _vm.showOverlay
      ? _c(
          "div",
          {
            staticClass:
              "price-overlay d-flex justify-content-center align-items-center"
          },
          [
            _c("div", { staticClass: "p-3 btn-compliment form" }, [
              _c("i", {
                staticClass: "fas fa-times-circle closePrice text-white p-2",
                attrs: { "aria-hidden": "true" },
                on: { click: _vm.handleOverlay }
              }),
              _vm._v(" "),
              _c(
                "form",
                {
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                    }
                  }
                },
                [
                  _c("legend", [_vm._v("Select Duration")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c(
                      "label",
                      { staticClass: "toCaps oPlan mb-4", attrs: { for: "" } },
                      [_vm._v(_vm._s(_vm.plan) + " plan")]
                    ),
                    _vm._v(" "),
                    _c("p", { staticClass: "text-muted text-center" }, [
                      _vm._v("\n              Duration :\n              "),
                      _c("strong", { staticClass: "toCaps text-white" }, [
                        _vm._v(_vm._s(_vm.duration))
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "oPrice mb-4" }, [
                    _vm._v("₦" + _vm._s(_vm.price) + ".00")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "my-3" }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm mx-auto text-main",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.subscribe(_vm.plan, _vm.duration)
                          }
                        }
                      },
                      [_vm._v("Pay")]
                    )
                  ])
                ]
              )
            ])
          ]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "pricing" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "price_box shadow" }, [
        _c("div", { staticClass: "price_name" }, [_vm._v("EDUCATOR")]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "price my-3" }, [
          !_vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("\n          ₦5000\n          "),
                _c("small", [_vm._v("/month")])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("\n          ₦4,167\n          "),
                _c("small", [_vm._v("/month billed annually")])
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center my-3" }, [
          _vm.vendorSub < 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.selectPackage("educator")
                    }
                  }
                },
                [_vm._v("Select")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.vendorSub == 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Subscribed")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.vendorSub > 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Unavailable")]
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "price_box shadow" }, [
        _c("div", { staticClass: "price_name" }, [_vm._v("COMMUNITY LEADER")]),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _c("div", { staticClass: "price my-3" }, [
          !_vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("\n          ₦10,000\n          "),
                _c("small", [_vm._v("/month")])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("\n          ₦8,333\n          "),
                _c("small", [_vm._v("/month billed annually")])
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center my-3" }, [
          _vm.vendorSub < 3
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.selectPackage("leader")
                    }
                  }
                },
                [_vm._v("Select")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.vendorSub == 3
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Subscribed")]
              )
            : _vm._e()
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "price_box shadow" }, [
      _c("div", { staticClass: "price_name" }, [_vm._v("CONTRIBUTOR")]),
      _vm._v(" "),
      _c("ul", [
        _c("li", [
          _vm._v(
            "Share your expert insights with our BizTribe (You get full credits)"
          )
        ]),
        _vm._v(" "),
        _c("li", [_vm._v("Create Unlimited Articles")]),
        _vm._v(" "),
        _c("li", [_vm._v("Enjoy traffic and exposure from other experts")]),
        _vm._v(" "),
        _c("li", [_vm._v("Build your BizTribe")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "price my-3" }, [_vm._v("FREE")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Custom Profile & Dashboard")]),
      _vm._v(" "),
      _c("li", [_vm._v("Create Unlimited Multimedia Resources")]),
      _vm._v(" "),
      _c("li", [_vm._v("Set your own pricing")]),
      _vm._v(" "),
      _c("li", [_vm._v("Create Paid Courses")]),
      _vm._v(" "),
      _c("li", [_vm._v("Interact with your content subscribers via chat")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Custom Profile & Dashboard")]),
      _vm._v(" "),
      _c("li", [_vm._v("Create Unlimited Resources")]),
      _vm._v(" "),
      _c("li", [
        _vm._v("Invite your BizTribe on other platforms your profile")
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("Set one-time or subscription community access")]),
      _vm._v(" "),
      _c("li", [_vm._v("Sell one-time or subscription access resources")]),
      _vm._v(" "),
      _c("li", [_vm._v("Create Subscriber Groups")]),
      _vm._v(" "),
      _c("li", [
        _vm._v("Subscriber Analytics (Demographics, Activity, Engagement)")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7e23539c", module.exports)
  }
}

/***/ }),

/***/ 479:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(999)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1001)
/* template */
var __vue_template__ = __webpack_require__(1002)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7e23539c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/pricing.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7e23539c", Component.options)
  } else {
    hotAPI.reload("data-v-7e23539c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 870:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(871)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(873)
/* template */
var __vue_template__ = __webpack_require__(874)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4f25ce06"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/toggle.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4f25ce06", Component.options)
  } else {
    hotAPI.reload("data-v-4f25ce06", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 871:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(872);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4407b836", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue", function() {
     var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 872:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.switch[data-v-4f25ce06] {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n\n/* Hide default HTML checkbox */\n.switch input[data-v-4f25ce06] {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n\n/* The slider */\n.slider[data-v-4f25ce06] {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: hsl(207, 46%, 20%);\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\n.slider[data-v-4f25ce06]:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\ninput:checked + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:focus + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:checked + .slider[data-v-4f25ce06]:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n\n/* Rounded sliders */\n.slider.round[data-v-4f25ce06] {\n  border-radius: 34px;\n}\n.slider.round[data-v-4f25ce06]:before {\n  border-radius: 50%;\n}\n@media(max-width:425px){\n.switch[data-v-4f25ce06] {\n\n  width: 50px;\n  height: 24px;\n}\n.slider[data-v-4f25ce06]:before {\n\n  height: 16px;\n  width: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            time: false
        };
    },

    watch: {
        'time': 'change'
    },
    methods: {
        change: function change() {
            this.$emit('handleToggle', this.time);
        }
    }
});

/***/ }),

/***/ 874:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("label", { staticClass: "switch" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.time,
            expression: "time"
          }
        ],
        attrs: { type: "checkbox" },
        domProps: {
          checked: Array.isArray(_vm.time)
            ? _vm._i(_vm.time, null) > -1
            : _vm.time
        },
        on: {
          change: function($event) {
            var $$a = _vm.time,
              $$el = $event.target,
              $$c = $$el.checked ? true : false
            if (Array.isArray($$a)) {
              var $$v = null,
                $$i = _vm._i($$a, $$v)
              if ($$el.checked) {
                $$i < 0 && (_vm.time = $$a.concat([$$v]))
              } else {
                $$i > -1 &&
                  (_vm.time = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
              }
            } else {
              _vm.time = $$c
            }
          }
        }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "slider round" })
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4f25ce06", module.exports)
  }
}

/***/ }),

/***/ 999:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1000);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("bb0d0e6e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7e23539c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./pricing.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7e23539c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./pricing.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});