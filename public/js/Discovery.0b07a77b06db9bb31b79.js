webpackJsonp([65],{

/***/ 1840:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1841);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("49ae5414", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-06a4a12f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./discoveryComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-06a4a12f\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./discoveryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1841:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container-fluid[data-v-06a4a12f] {\n  padding-top: 50px;\n  min-height: 100vh;\n  background-color: #f7f8fa;\n}\n.bap_box[data-v-06a4a12f] {\n  display: grid;\n  padding: 10px;\n  width: 100%;\n  height: 100%;\n  grid-column-gap: 15px;\n  grid-row-gap: 30px;\n}\n.grid_box[data-v-06a4a12f] {\n  height: 100px;\n  position: relative;\n  border: 1px solid #f7f8fa;\n  text-align: center;\n  border-radius: 4px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(#414d57),\n    color-stop(#52616e),\n    color-stop(#647786),\n    color-stop(#778c9f),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#778c9f),\n    color-stop(#647786),\n    color-stop(#52616e),\n    to(#414d57)\n  );\n  background-image: linear-gradient(\n    to right top,\n    #414d57,\n    #52616e,\n    #647786,\n    #778c9f,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #778c9f,\n    #647786,\n    #52616e,\n    #414d57\n  );\n  -webkit-transition: all 2s;\n  transition: all 2s;\n}\n.grid_text[data-v-06a4a12f] {\n  text-align: center;\n  position: absolute;\n  font-weight: bold;\n  font-size: 18px;\n  color: white;\n  text-transform: capitalize;\n  font-family: \"Josefin Sans\";\n}\n.grid_box[data-v-06a4a12f]:hover {\n  background-image: -webkit-gradient(\n    linear,\n    right bottom, left top,\n    from(#a4c2db),\n    color-stop(#90abc1),\n    color-stop(#7d94a7),\n    color-stop(#6a7e8e),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#6a7e8e),\n    color-stop(#7d94a7),\n    color-stop(#90abc1),\n    to(#a4c2db)\n  );\n  background-image: linear-gradient(\n    to left top,\n    #a4c2db,\n    #90abc1,\n    #7d94a7,\n    #6a7e8e,\n    #586876,\n    #586876,\n    #586876,\n    #586876,\n    #6a7e8e,\n    #7d94a7,\n    #90abc1,\n    #a4c2db\n  );\n}\nh3[data-v-06a4a12f] {\n  font-size: 18.5px;\n}\n@media only screen and (min-width: 768px) {\n.container-fluid[data-v-06a4a12f] {\n    padding: 30px 20px;\n}\n.bap_box[data-v-06a4a12f] {\n    grid-template-columns: auto auto auto;\n}\n.grid_text[data-v-06a4a12f]{\n    font-size: 24px;\n}\n}\n@media only screen and (min-width: 1024px) {\n.container-fluid[data-v-06a4a12f] {\n    padding: 30px 40px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__introTemplateOne__ = __webpack_require__(1843);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__introTemplateOne___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__introTemplateOne__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      current_month: 0,
      one: true,
      forms: []
    };
  },

  components: {
    intro: __WEBPACK_IMPORTED_MODULE_0__introTemplateOne___default.a
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {

    next(function (vm) {
      if (from.path == '/entrepreneur/bap/idea' || from.path == '/entrepreneur/bap/concept' || from.path == '/entrepreneur/bap/self-discovery') {
        vm.one = false;
      }
    });
  },
  mounted: function mounted() {
    this.getRecord();
    this.getForms();
  },

  methods: {
    gotoTemplate: function gotoTemplate(id) {
      this.$router.push('/entrepreneur/bap/form-question/' + id);
    },
    getForms: function getForms() {
      var _this = this;

      var name = this.$route.query.name;
      axios.get('/api/get-bap-forms/' + name).then(function (res) {
        if (res.status == 200) {
          _this.forms = res.data;
        }
      }).catch();
    },
    switchTemp: function switchTemp() {
      this.one = !this.one;
      window.scrollTo(0, 0);
    },
    getRecord: function getRecord() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user != null) {
        axios.get('/api/get-active-record', {
          headers: {
            Authorization: 'Bearer ' + user.access_token
          }
        }).then(function (response) {
          _this2.current_month = response.data.current_month;
        });
      }
    }
  }
});

/***/ }),

/***/ 1843:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1844)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1846)
/* template */
var __vue_template__ = __webpack_require__(1847)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-96dc8afc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/introTemplateOne.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-96dc8afc", Component.options)
  } else {
    hotAPI.reload("data-v-96dc8afc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1844:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1845);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("b0d7ca96", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-96dc8afc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./introTemplateOne.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-96dc8afc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./introTemplateOne.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1845:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.headFont[data-v-96dc8afc]{\n  font-family: 'Josefin Sans';\n}\np[data-v-96dc8afc] {\n  margin-bottom: 1rem;\n}\nh1[data-v-96dc8afc],\nh2[data-v-96dc8afc],\nh3[data-v-96dc8afc],\nh4[data-v-96dc8afc],\nh5[data-v-96dc8afc],\nh6[data-v-96dc8afc] {\n  margin-bottom: 0.8rem;\n}\nem[data-v-96dc8afc] {\n  font-style: italic;\n  font-size: 15px;\n}\nh3[data-v-96dc8afc]{\n    font-size: 18px;\n}\n.intro[data-v-96dc8afc] {\n  margin: 0 auto;\n  min-height: 100vh;\n   padding:15px;\n  padding-top: 20px;\n  padding-bottom: 65px;\n  background: white;\n}\n@media only screen and (min-width: 575px) and (max-width: 767px) {\n.intro[data-v-96dc8afc] {\n    width: 95%;\n    padding-top: 40px;\n}\nh3[data-v-96dc8afc]{\n    font-size: 20px;\n}\n}\n@media only screen and (min-width:768px) {\n.intro[data-v-96dc8afc] {\n    width: 90%;\n     padding:25px;\n    padding-top: 50px;\n      padding-bottom: 80px;\n}\nh3[data-v-96dc8afc]{\n    font-size: 24px;\n}\n}\n@media only screen and (min-width:1024px) {\n.intro[data-v-96dc8afc] {\n    width: 70%;\n}\nh3[data-v-96dc8afc]{\n    font-size: 32px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    switchStage: function switchStage() {
      this.$emit("switch");
    }
  }
});

/***/ }),

/***/ 1847:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "intro" }, [
    _c("h3", { staticClass: "headFont" }, [
      _vm._v("DISCOVERING WHAT IS WITHIN")
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Congratulations! You’re mere weeks away from living the life of your dreams— purposeful, impactful and productive. We are so excited that the world is about to hear you loud and clear, through your business."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "\n    Perhaps you have heard by now that running a business is more than a full-time job, and for it to thrive there needs to be a product-market fit, the right branding, strategy, distribution channels etc. but you may not have heard that running a successful business takes self-awareness and round-the-clock physical, emotional and spiritual commitment.\n    Yes, sometimes all a business really needs to be successful is alignment with your inner man and commitment— lots of it, because when you own your own business you are not obligated to achieve your goals, heck, no one even knows them, so your motivation must come from within. Ultimately, it is easier to stay on course when those business goals are in alignment with who you really are.\n  "
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "We believe this, and that is why we start right here— discovering what is within."
      )
    ]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "We are all mostly aware of the things that society wants us to do, or the people our families want us to be. We hear it so much that sometimes, it’s easy to get caught up in projections of who we are; never quite discovering or perhaps losing touch with our core values, needs, and desires."
      )
    ]),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Therefore, we implore you to answer the questions in this exercise very sincerely and with openness to whatever you find. Give thoughtful answers but do not be consumed by trying to find the one that present you the way you want to be seen, because that just defeats the purpose of this. Be honest enough to allow yourself to be surprised by some aspects of who you are. That should be exciting!"
      )
    ]),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "The aim of this phase of our program is to arm you with self-awareness through a journey of self-reflection. Self-awareness will help you look past your superficial reactions to see your underlying needs and emotions, even in running your business. It is our hope that in doing this exercise, you learn to identify your own voice and distinguish it from the voices of people whose approval you may be seeking."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Above all, we hope that empowered with the combination of self-reflection, self-awareness and your own voice, you will be led to tell your own story through your entrepreneurial journey— the products and services that you develop to serve humanity. We cant wait to see it manifest, so let’s get started!"
      )
    ]),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "elevated_btn text-white btn-compliment mx-auto",
        attrs: { type: "button" },
        on: { click: _vm.switchStage }
      },
      [_vm._v("Click here to discover What's within you")]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("strong", [_vm._v("How well do you really know yourself?")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("strong", [
        _vm._v(
          "If you don’t know yourself very well, it’s difficult to choose a life path that makes you feel happy and fulfilled, and while growing a business is awesome, do you know what’s even better? Being at peace with who you really are while doing so."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("strong", [
        _vm._v(
          "Know this— whatever you find out about yourself today isn’t a death sentence; life is dynamic, and you will always have the chance to re-write your story"
        )
      ]),
      _vm._v(" — especially here, just edit! lol.\n  ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("blockquote", [
      _c("em", [
        _vm._v(
          "Instead of just coasting through life and being reactive to the behaviors and opinions of others, I will get to know myself."
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-96dc8afc", module.exports)
  }
}

/***/ }),

/***/ 1848:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container-fluid" },
    [
      _c(
        "router-link",
        { staticClass: "back mb-5", attrs: { to: "/entrepreneur/bap/bap" } },
        [
          _c("i", {
            staticClass: "fa fa-long-arrow-left",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" Back to BAP-6 \n    ")
        ]
      ),
      _vm._v(" "),
      _vm.one
        ? _c("div", {}, [_c("intro", { on: { switch: _vm.switchTemp } })], 1)
        : _c("div", { staticClass: "first" }, [
            _c("h3", { staticClass: "mb-3" }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "bap_box" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "grid_box shadow-sm",
                    attrs: { to: "/entrepreneur/bap/self-discovery" }
                  },
                  [
                    _c("p", { staticClass: "grid_text" }, [
                      _vm._v("Self Discovery")
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "grid_box shadow-sm",
                    attrs: { to: "/entrepreneur/bap/idea" }
                  },
                  [
                    _c("p", { staticClass: "grid_text" }, [
                      _vm._v("My Bright Idea")
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "grid_box shadow-sm",
                    attrs: { to: "/entrepreneur/bap/concept" }
                  },
                  [
                    _c("p", { staticClass: "grid_text" }, [
                      _vm._v("My Business Concept")
                    ])
                  ]
                ),
                _vm._v(" "),
                _vm._l(_vm.forms, function(form, index) {
                  return _vm.forms.length
                    ? _c(
                        "div",
                        {
                          key: index,
                          staticClass: "grid_box shadow-sm",
                          on: {
                            click: function($event) {
                              return _vm.gotoTemplate(form.id)
                            }
                          }
                        },
                        [
                          _c("p", { staticClass: "grid_text" }, [
                            _vm._v(_vm._s(form.form_title.toLowerCase()))
                          ])
                        ]
                      )
                    : _vm._e()
                })
              ],
              2
            )
          ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-06a4a12f", module.exports)
  }
}

/***/ }),

/***/ 642:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1840)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1842)
/* template */
var __vue_template__ = __webpack_require__(1848)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-06a4a12f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/discoveryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-06a4a12f", Component.options)
  } else {
    hotAPI.reload("data-v-06a4a12f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});