webpackJsonp([86],{

/***/ 1249:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1250);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("266f74aa", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3469ad3d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminAuthComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3469ad3d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminAuthComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1250:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.login_area[data-v-3469ad3d] {\n  background-color: #f5f5f5 !important;\n  height: 100vh;\n}\n.signUp[data-v-3469ad3d] {\n  background-color: #a3c2dc !important;\n  width: 60%;\n  height: 100%;\n  padding: 60px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.loginPagge[data-v-3469ad3d] {\n  background-color: #ffffff !important;\n  width: 40%;\n  padding: 20px 60px;\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.registerPagge[data-v-3469ad3d] {\n  padding: 20px 60px;\n  width: 50%;\n  background-color: #a3c2dc !important;\n}\nh2[data-v-3469ad3d] {\n  font-size: 35px;\n  text-align: center;\n  margin-bottom: 40px;\n}\n.pp[data-v-3469ad3d] {\n  text-align: center;\n  font-size: 20px;\n}\n.btn-signup[data-v-3469ad3d] {\n  background-color: #a3c2dc !important;\n  color: #ffffff;\n  padding: 10px 20px;\n  border-radius: 20px;\n  text-transform: capitalize;\n}\n.btn-logIn[data-v-3469ad3d] {\n  color: #a3c2dc;\n  background-color: #ffffff !important;\n}\n.signUp-button[data-v-3469ad3d] {\n  margin: 40px 0 0 0;\n}\n.login_inner[data-v-3469ad3d] {\n  height: 100%;\n}\n@media (max-width: 768px) {\n.signUp[data-v-3469ad3d] {\n    display: none;\n}\n.loginPagge[data-v-3469ad3d] {\n    width: 100%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1251:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "admin-auth-component",
  data: function data() {
    return {
      user: {
        email: null,
        password: null
      }
    };
  },
  mounted: function mounted() {
    var admin = JSON.parse(localStorage.getItem("authAdmin"));
    if (admin !== null) {
      this.$router.push("/admin/dashboard");
    } else {
      this.$router.push("/admin/auth/manage");
    }
  },

  methods: {
    login: function login(user) {
      var _this = this;

      var data = {
        client_id: 2,
        client_secret: "UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C",
        grant_type: "password",
        username: user.email,
        password: user.password,
        scope: "*",
        theNewProvider: "admin_user"
      };
      var authAdmin = {};
      console.log(JSON.parse(JSON.stringify(user)));
      axios.post("/oauth/token", data).then(function (response) {
        authAdmin.access_token = response.data.access_token;
        authAdmin.refresh_token = response.data.access_token;
        localStorage.setItem("authAdmin", JSON.stringify(authAdmin));
        console.log(response.data.access_token);
        axios.get("/api/adminDetails", { headers: Object(__WEBPACK_IMPORTED_MODULE_0__config__["a" /* getAdminHeader */])() }).then(function (res) {
          console.log("Data: login -> res", res);
          if (res.data.verified) {
            authAdmin.id = res.data.id;
            authAdmin.email = res.data.email;
            authAdmin.role = res.data.role;
            localStorage.setItem("authAdmin", JSON.stringify(authAdmin));
            _this.$router.push("/admin/dashboard");
            _this.$toasted.success("Successfully login");
          } else {
            _this.$toasted.error("Please verify your account");
          }
        }).catch(function (error) {
          console.log(error);
        });
      }).catch(function (error) {
        _this.$toasted.error("Invalid user credential");
      });
    }
  }
});

/***/ }),

/***/ 1252:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "login_area" }, [
    _c("div", { staticClass: "login_inner" }, [
      _c("div", { staticClass: "row h-100" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "divRight loginPagge" }, [
          _c("h3", { staticClass: "mb-4" }, [_vm._v("Admin Log in")]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("p", { staticClass: "login-box-msg" }, [
              _vm._v("Sign in to start your session")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group has-feedback" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.user.email,
                    expression: "user.email"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "email", placeholder: "Email" },
                domProps: { value: _vm.user.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.user, "email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-envelope form-control-feedback" })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group has-feedback" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.user.password,
                    expression: "user.password"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "password", placeholder: "Password" },
                domProps: { value: _vm.user.password },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.user, "password", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-lock form-control-feedback" })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "w-100 text-center" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-signup",
                    attrs: { type: "submit" },
                    on: {
                      click: function($event) {
                        return _vm.login(_vm.user)
                      }
                    }
                  },
                  [_vm._v("Sign In")]
                )
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "divLeft signUp" }, [
      _c("i", { staticClass: "fas fa-user-lock fa-10x mb-4" }),
      _vm._v(" "),
      _c("h2", { staticClass: "text-white" }, [_vm._v("Admin Area")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3469ad3d", module.exports)
  }
}

/***/ }),

/***/ 522:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1249)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1251)
/* template */
var __vue_template__ = __webpack_require__(1252)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3469ad3d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminAuthComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3469ad3d", Component.options)
  } else {
    hotAPI.reload("data-v-3469ad3d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ })

});