webpackJsonp([149],{

/***/ 1202:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1203);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("62864b70", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f80c56c4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsShowProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f80c56c4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./opsShowProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1203:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-f80c56c4] {\n   padding:30px 0;\n}\n.imageContainer[data-v-f80c56c4] {\n   height: 100px;\n   width:100px;\n}\n.imgFullSide[data-v-f80c56c4] {\n    float:left;\n    height: 50%;\n    width: 70%;\n}\n.imgThumbnailSide[data-v-f80c56c4] {\n   float: left;\n}\n.main-page[data-v-f80c56c4] {\n   height: 100vh;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "ops-show-product-component",
    data: function data() {
        return {
            id: this.$route.params.id,
            isActive: true,
            linkTag: false,
            products: {},
            featuredProducts: 'Sponsor Product',
            verifyProducts: 'Verify Product',
            imgSrc: '',
            token: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var ops = JSON.parse(localStorage.getItem('authOps'));
        this.token = ops.access_token;
        if (ops != null) {
            axios.get('/api/ops/product/' + this.id, { headers: { "Authorization": 'Bearer ' + ops.access_token } }).then(function (response) {
                if (response.data.data.verify) {
                    _this.verifyProducts = 'Unverify Product';
                } else {
                    _this.verifyProducts = 'Verify Product';
                }

                if (response.data.data.sponsor == 'Y') {
                    _this.featuredProducts = 'Unsponsor Product';
                } else {
                    _this.featuredProducts = 'Sponsor Product';
                }

                _this.products = response.data.data;
                for (var i = 0; i < _this.products.productImage.length; i++) {
                    _this.imgSrc = _this.products.productImage[0].image;
                }
                _this.isActive = false;
                _this.linkTag = true;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/ops/login');
        }
    },

    methods: {
        verifyProduct: function verifyProduct(productid) {
            var _this2 = this;

            axios.get('/api/verify/product/' + productid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                if (response.data.verify) {
                    _this2.$toasted.success("Successfully verified");
                    _this2.verifyProducts = "Unverify Product";
                } else {
                    _this2.$toasted.success("Successfully unverified");
                    _this2.verifyProducts = "Verify Product";
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        featuredProduct: function featuredProduct(productid) {
            var _this3 = this;

            axios.get('/api/featured/product/' + productid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                if (response.data.sponsor == 'Y') {
                    _this3.$toasted.success("Successfully sponsor");
                    _this3.featuredProducts = "Unsponsor product";
                } else {
                    _this3.$toasted.success("Successfully unsponsor");
                    _this3.featuredProducts = "Sponsor product";
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        swipeImage: function swipeImage(index) {
            console.log(index);
            for (var i = 0; i < this.products.productImage.length; i++) {
                this.imgSrc = this.products.productImage[index].image;
                //console.log(this.products.productImage[index].image);
            }
        }
    }
});

/***/ }),

/***/ 1205:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { attrs: { id: "" } }, [
      _c("div", { staticClass: "main-page" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", [
              _c("div", [
                _c("img", {
                  staticClass: "imgFullSide",
                  attrs: { src: _vm.imgSrc }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "imgThumbnailSide" },
                _vm._l(_vm.products.productImage, function(
                  productImages,
                  index
                ) {
                  return _c("div", { staticClass: "imageContainer" }, [
                    _c("img", {
                      attrs: { src: productImages.image },
                      on: {
                        click: function($event) {
                          return _vm.swipeImage(index)
                        }
                      }
                    })
                  ])
                }),
                0
              )
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-md-6" },
            [
              _c("h1", { staticClass: "h1" }, [
                _vm._v(_vm._s(_vm.products.title))
              ]),
              _vm._v(" "),
              _vm._l(_vm.products.productDetail, function(productVariation) {
                return _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("b", [_vm._v("Size: ")]),
                    _vm._v(_vm._s(productVariation.size))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("b", [_vm._v("Original Price: ")]),
                    _vm._v(_vm._s(productVariation.price))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("b", [_vm._v("Sales Price: ")]),
                    _vm._v(_vm._s(productVariation.salePrice))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("b", [_vm._v("Product Quantity:")]),
                    _vm._v(_vm._s(productVariation.quantity))
                  ])
                ])
              }),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-6" }, [
                  _vm._m(0),
                  _vm._v(
                    "\n                 " +
                      _vm._s(_vm.products.material) +
                      "\n              "
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6" }, [
                  _vm._m(1),
                  _vm._v(
                    "\n                 " + _vm._s(_vm.products.description)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-6" }, [
                  _vm._m(2),
                  _vm._v("\n                 " + _vm._s(_vm.products.dimension))
                ])
              ]),
              _vm._v(" "),
              _c("span", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    on: {
                      click: function($event) {
                        return _vm.featuredProduct(_vm.products.id)
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.featuredProducts))]
                )
              ]),
              _vm._v(" "),
              _c("span", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    on: {
                      click: function($event) {
                        return _vm.verifyProduct(_vm.products.id)
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.verifyProducts))]
                )
              ])
            ],
            2
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [_c("b", [_vm._v("Product Material: ")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [_c("b", [_vm._v("Product Description: ")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [_c("b", [_vm._v("Product Dimension: ")])])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f80c56c4", module.exports)
  }
}

/***/ }),

/***/ 514:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1202)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1204)
/* template */
var __vue_template__ = __webpack_require__(1205)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f80c56c4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/operation/components/opsShowProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f80c56c4", Component.options)
  } else {
    hotAPI.reload("data-v-f80c56c4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});