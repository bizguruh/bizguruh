webpackJsonp([162],{

/***/ 1273:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1274);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("bcd5ce78", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-640f9932\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubCategoryComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-640f9932\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubCategoryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1274:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-640f9932] {\n    height: 100vh;\n}\n.container[data-v-640f9932] {\n    width: 100%;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-sub-category-component",
    data: function data() {
        return {
            subcategory: {
                sub_category_id: this.$route.params.id,
                id: '',
                category: '',
                name: '',
                description: '',
                adminCategory: ''
            },
            token: '',
            fade: true,
            modal: true,
            toggleEdit: true,
            subcategories: [],
            CategoryText: 'Create SubCategory'
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));

        this.token = admin.access_token;
        if (admin != null) {
            axios.get('/api/admin/getsubcategorybyid/' + this.subcategory.sub_category_id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this.subcategories = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        createNewSubCategory: function createNewSubCategory() {
            var _this2 = this;

            if (this.toggleEdit) {
                axios.post('/api/subcategory', JSON.parse(JSON.stringify(this.subcategory)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 201) {
                        _this2.$toasted.success('Subcategory successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this2.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else {
                axios.put('/api/subcategory/' + this.subcategory.id, JSON.parse(JSON.stringify(this.subcategory)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this2.$toasted.success('Subcategory successfully edited');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this2.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            }
        },
        showModal: function showModal(subcategory) {
            var _this3 = this;

            this.fade = false;
            console.log(subcategory);
            if (typeof subcategory === 'undefined') {
                this.CategoryText = 'Create SubCategory';
                this.toggleEdit = true;
                this.subcategory.name = this.subcategory.description = '';
                axios.get('/api/admin/getcategorybyid/' + this.subcategory.sub_category_id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    console.log(response);
                    if (response.status === 200) {
                        _this3.subcategory.category = response.data.name;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                this.CategoryText = 'Update Category';
                this.toggleEdit = false;
                axios.get('/api/admin/getcategorybyid/' + this.subcategory.sub_category_id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    console.log(response);
                    if (response.status === 200) {
                        _this3.subcategory.category = response.data.name;
                        axios.get('/api/subcategory/' + subcategory + '/edit', { headers: { "Authorization": 'Bearer ' + _this3.token } }).then(function (response) {
                            console.log(response);
                            if (response.status === 200) {
                                _this3.subcategory.id = response.data.id;
                                _this3.subcategory.name = response.data.name;
                                _this3.subcategory.description = response.data.description;
                                _this3.subcategory.adminCategory = response.data.adminCategory;
                            }
                        }).catch(function (error) {
                            for (var key in error.response.data.errors) {
                                _this3.$toasted.error(error.response.data.errors[key][0]);
                            }
                        });
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this3.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            }
        },
        deleteCategory: function deleteCategory(categoryid, cat) {
            var _this4 = this;

            this.subcategories.splice(cat, 1);
            axios.delete('/api/subcategory/' + categoryid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.$toasted.success("Deleted Successfully");
                } else {
                    _this4.$toasted.error("Unable to delete item");
                }
            }).catch(function (error) {
                for (var key in error.response.data.errors) {
                    _this4.$toasted.error(error.response.data.errors[key][0]);
                }
            });
        }
    }
});

/***/ }),

/***/ 1276:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c(
      "button",
      {
        staticClass: "btn btn-primary",
        attrs: {
          type: "button",
          "data-toggle": "modal",
          "data-target": "#myModal"
        },
        on: {
          click: function($event) {
            return _vm.showModal()
          }
        }
      },
      [_vm._v("\n            Create Sub Category\n        ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        class: { modal: _vm.modal, fade: _vm.fade },
        attrs: {
          id: "myModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "myModalLabel"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "category" } }, [
                    _vm._v("Category:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.subcategory.category,
                        expression: "subcategory.category"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "category", disabled: "" },
                    domProps: { value: _vm.subcategory.category },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.subcategory,
                          "category",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "subcategory" } }, [
                    _vm._v("SubCategory:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.subcategory.name,
                        expression: "subcategory.name"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "subcategory" },
                    domProps: { value: _vm.subcategory.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.subcategory, "name", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "categorydescription" } }, [
                    _vm._v("Description:")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.subcategory.description,
                        expression: "subcategory.description"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { id: "categorydescription" },
                    domProps: { value: _vm.subcategory.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.subcategory,
                          "description",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "adminCategory" } }, [
                    _vm._v("Admins")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.subcategory.adminCategory,
                          expression: "subcategory.adminCategory"
                        }
                      ],
                      attrs: { id: "adminCategory" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.subcategory,
                            "adminCategory",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "AO" } }, [
                        _vm._v("Admin only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "UO" } }, [
                        _vm._v("User only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "AU" } }, [
                        _vm._v("Admin and User")
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-footer" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-default",
                      attrs: { type: "button", "data-dismiss": "modal" }
                    },
                    [_vm._v("Close")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.createNewSubCategory()
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.CategoryText))]
                  )
                ])
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("table", { staticClass: "table" }, [
        _vm._m(1),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.subcategories, function(category, index) {
            return _c("tr", [
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              _c(
                "td",
                [
                  _c(
                    "router-link",
                    {
                      attrs: {
                        tag: "a",
                        to: {
                          name: "adminProductSubCategoryBrand",
                          params: { id: category.id }
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            " +
                          _vm._s(category.name) +
                          "\n                        "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(category.description))]),
              _vm._v(" "),
              _c("td", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-small",
                    attrs: {
                      "data-toggle": "modal",
                      "data-target": "#myModal"
                    },
                    on: {
                      click: function($event) {
                        return _vm.showModal(category.id)
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-pencil" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-danger btn-small",
                    on: {
                      click: function($event) {
                        return _vm.deleteCategory(category.id, index)
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-trash" })]
                )
              ])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c("h4", { staticClass: "modal-title", attrs: { id: "myModalLabel" } }, [
        _vm._v("Create SubCategory")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("SN")]),
        _vm._v(" "),
        _c("th", [_vm._v("SubCategory")]),
        _vm._v(" "),
        _c("th", [_vm._v("Description")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-640f9932", module.exports)
  }
}

/***/ }),

/***/ 528:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1273)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1275)
/* template */
var __vue_template__ = __webpack_require__(1276)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-640f9932"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminSubCategoryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-640f9932", Component.options)
  } else {
    hotAPI.reload("data-v-640f9932", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});