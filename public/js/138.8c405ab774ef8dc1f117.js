webpackJsonp([138],{

/***/ 1815:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1816);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("65799ab0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-583a482b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./diagnosticsComponents.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-583a482b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./diagnosticsComponents.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1816:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.containers[data-v-583a482b] {\n  padding: 0px;\n  min-height: 100vh;\n  width: 100%;\n  background-color: #f7f8fa;\n  background-size: cover;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bgG[data-v-583a482b] {\n  background-color: #f7f8fa;\n}\n.main[data-v-583a482b] {\n  position: relative;\n  background-color: white;\n  padding: 40px 80px;\n  border-radius: 4px;\n  font-size: 16px;\n  width: 70%;\n  margin: 0 auto;\n  z-index: 1;\n}\nul[data-v-583a482b],\nol[data-v-583a482b] {\n  text-decoration: none !important;\n}\n.input-group[data-v-583a482b] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n.mainTemplate[data-v-583a482b] {\n  background: white;\n  padding: 20px 30px;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 2px 1px #ccc;\n          box-shadow: 0 0 2px 1px #ccc;\n  width: 90%;\n  min-height: 500px;\n  z-index: 1;\n}\n.min-768[data-v-583a482b] {\n  min-width: 768px;\n}\n.start[data-v-583a482b] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: 400px;\n  width: 100%;\n}\n.count[data-v-583a482b] {\n  font-size: 9em;\n}\n.return[data-v-583a482b] {\n  position: absolute;\n  top: 20px;\n  right: 20px;\n  cursor: pointer;\n}\n.contents[data-v-583a482b] {\n  height: 100%;\n}\ninput[type=\"text\"][data-v-583a482b]:focus {\n  background-color: rgba(255, 255, 255, 0.7);\n  color: rgba(0, 0, 0, 0.64);\n}\ninput[type=\"text\"][data-v-583a482b] {\n  background-color: #fafafa;\n  color: rgba(0, 0, 0, 0.44);\n}\n.btn[data-v-583a482b] {\n  text-transform: capitalize;\n}\n.overlay[data-v-583a482b] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(111, 148, 250, 0.1);\n}\n.scores[data-v-583a482b] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\nul[data-v-583a482b],\nol[data-v-583a482b] {\n  list-style: none;\n}\n.form-check-input[data-v-583a482b] {\n  position: absolute;\n  margin-top: 0.1rem;\n  margin-left: -1.25rem;\n}\n.prev[data-v-583a482b] {\n  background-color: rgb(171, 171, 190) !important;\n  border: none !important;\n}\n.next[data-v-583a482b] {\n  background-color: #a4c2db !important;\n  border: none !important;\n  height: 40px;\n}\n.submit[data-v-583a482b] {\n  background-color: #5b84a7 !important;\n  border: none !important;\n}\n.question[data-v-583a482b] {\n  padding: 3px 15px;\n}\n.options[data-v-583a482b] {\n  padding: 10px 20px;\n}\n.navigation[data-v-583a482b] {\n  width: 100%;\n  text-align: right;\n  margin-top: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\nlabel[data-v-583a482b] {\n  text-transform: capitalize;\n}\n.category[data-v-583a482b] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  min-height: 400px;\n}\n@media (max-width: 425px) {\n.containers[data-v-583a482b]{\n    padding: 0 ;\n    background-color:white;\n}\n.main[data-v-583a482b] {\n    width: 100%;\n    padding: 0;\n}\n.mainTemplate[data-v-583a482b] {\n    width: 100%;\n}\n.question[data-v-583a482b]{\n    font-size:14px;\n}\n.contents[data-v-583a482b] {\n    height: 100%;\n    width: 100%;\n    margin: 0 auto !important;\n    -webkit-box-shadow: unset;\n            box-shadow: unset;\n}\nh3[data-v-583a482b]{\n    font-size: 18px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1817:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "checklist",
  data: function data() {
    return {
      id: null,
      questId: this.$route.params.id,
      result: [],
      quest: [],
      models: [],
      answers: [],
      modelAnswer: [],
      answer: "",
      checkAnswer: [],
      number: 0,
      questNumber: 0,
      totalQuest: 0,
      disable: true,
      questionAnswers: [],
      score: 0,
      totalScore: 0,
      showScores: false,
      showQuestions: true,
      showSubmit: false,
      showTemplate: false,
      start: false,
      count: 3,
      startCount: false,
      categoryBegin: false,
      response: {
        first: "",
        second: "",
        third: "",
        fourth: "",
        fifth: "",
        durationFirst: "",
        durationSecond: "",
        durationThird: "",
        durationFourth: "",
        durationFifth: ""
      }
    };
  },
  mounted: function mounted() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user !== null) {
      this.id = user.id;
    }
    axios.get("/api/questionsVerified").then(function (response) {
      response.data.forEach(function (item) {
        if (item.catalog === "diagnostics" && Number(item.id) === Number(_this.questId)) {
          _this.result = item;
          _this.models = JSON.parse(item.questions);
        }
      });
    });
  },

  watch: {
    answer: "ableNext",
    checkAnswer: "ableNext",
    answers: "disableNext"
  },
  methods: {
    storeAnswer: function storeAnswer() {
      var _this2 = this;

      if (this.result.name === "qa") {
        var data = {
          user_id: this.id,
          title: "Brain gym",
          name: this.result.name,
          question_id: this.result.id,
          answers: this.answers,
          response: this.response
        };
        axios.post("/api/guides/history", JSON.parse(JSON.stringify(data))).then(function (response) {
          if (response.status === 200) {
            _this2.$toasted.success("success");

            // this.$router.push('/')
          }
        }).catch(function (e) {
          console.log(e);
        });
      }
    },
    startNow: function startNow() {
      var _this3 = this;

      this.startCount = true;
      var time = setInterval(function () {
        _this3.count--;
        if (_this3.count === 0) {
          clearInterval(time);
          _this3.start = true;
        }
      }, 1000);
    },
    submit: function submit() {
      if (this.result.name === "template") {
        this.showTemplate = true;
      } else {
        var totalMark = this.totalQuest * 5;

        this.totalScore = parseInt(this.score / totalMark * 100) + "%";
        this.showQuestions = false;
      }
      this.showSubmit = false;
      this.showScores = true;
    },
    processAnswer: function processAnswer() {
      if (this.models[this.number].questions[this.questNumber].answer == this.answer) {
        this.score += 5;
      } else {
        this.score = this.score;
      }
    },
    processMc: function processMc() {
      var _this4 = this;

      var mark = 0;
      this.models[this.number].questions[this.questNumber].answers.forEach(function (answer) {
        _this4.checkAnswer.forEach(function (checked) {
          if (answer == checked) {
            mark += 1;
            _this4.score += mark / _this4.models[_this4.number].questions[_this4.questNumber].answers.length * 5;
          }
        });
      });
    },
    ableNext: function ableNext() {
      if (this.answer !== "" || this.checkAnswer.length > 0) {
        this.disable = false;
      } else {
        this.disable = true;
      }
    },
    disableNext: function disableNext() {
      this.disable = true;
    },
    next: function next() {
      this.number++;
    },
    prev: function prev() {
      this.number--;
    },
    nextQuestion: function nextQuestion(type) {
      //   if (this.number !== this.models.length - 1) {
      this.totalQuest++;
      if (this.questNumber < this.models[this.number].questions.length - 1) {
        if (type === "mc") {
          this.processMc();
          this.answers.push(this.checkAnswer);
          this.checkAnswer = [];
          this.questNumber++;
        } else {
          this.processAnswer();
          this.answers.push(this.answer);

          this.answer = "";

          this.questNumber++;
        }
      } else {
        if (type === "mc") {
          this.processMc();
          this.answers.push(this.checkAnswer);
          this.checkAnswer = [];
        } else {
          this.processAnswer();
          this.answers.push(this.answer);
          this.answer = "";
        }
        this.showSubmit = true;
        this.showQuestions = false;
      }
      //   } else {
      //     if (type === "mc") {
      //       this.processMc();
      //       this.answers.push(this.checkAnswer);
      //       this.checkAnswer = [];
      //     } else {
      //       this.processAnswer();
      //       this.answers.push(this.answer);
      //       this.answer = "";
      //     }
      //     this.modelAnswer.push(this.answers);
      //     this.categoryBegin = false;
      //     this.showSubmit = true;
      //     this.start = true;
      //     this.showQuestions = false;
      //   }
    },
    prevQuestion: function prevQuestion() {
      this.answers.pop();
      this.questNumber--;
    },
    beginCategory: function beginCategory() {
      this.number++;
      this.questNumber = 0;
      this.categoryBegin = false;
      this.modelAnswer.push(this.answers);
      this.answer = "";
      this.answers = [];
    },
    returnNow: function returnNow() {
      this.$router.push({
        name: "Guides"
      });
    }
  }
});

/***/ }),

/***/ 1818:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "containers" }, [
    _c(
      "div",
      { staticClass: "main", class: { mainTemplate: _vm.showTemplate } },
      [
        _c("div", { staticClass: "return" }, [
          _c("span", { on: { click: _vm.returnNow } }, [
            _vm._v("\n        Return\n        "),
            _c("i", {
              staticClass: "fa fa-undo",
              attrs: { "aria-hidden": "true" }
            })
          ])
        ]),
        _vm._v(" "),
        _vm.showQuestions && !_vm.categoryBegin
          ? _c(
              "div",
              {
                staticClass:
                  "mt-4 contents animated fadeInRight faster border shadow-sm p-3"
              },
              [
                _vm.result.title !== ""
                  ? _c("h3", [_vm._v(_vm._s(_vm.result.title))])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", {
                  domProps: { innerHTML: _vm._s(_vm.result.premise) }
                }),
                _vm._v(" "),
                _vm.models[_vm.number].category !== ""
                  ? _c("p", [_vm._v(_vm._s(_vm.models[_vm.number].category))])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "p-3" }, [
                  _vm.answers.length > 0
                    ? _c(
                        "div",
                        { staticClass: "border p-2 bgG my-3" },
                        _vm._l(_vm.answers, function(answer, idx) {
                          return _c(
                            "p",
                            { key: idx, staticClass: "animated fadeIn slow" },
                            [
                              _vm._v(
                                "\n            (Q" +
                                  _vm._s(idx + 1) +
                                  ".)\n            "
                              ),
                              _vm._l(answer, function(ans, id) {
                                return _c("span", { key: id }, [
                                  _vm._v(_vm._s(ans) + ",")
                                ])
                              })
                            ],
                            2
                          )
                        }),
                        0
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "tf"
                    ? _c("div", { staticClass: "form-check question" }, [
                        _c("div", { staticClass: "d-flex mb-2" }, [
                          _c("span", { staticClass: "bold pr-2" }, [
                            _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                          ]),
                          _vm._v(" "),
                          _c("p", {}, [
                            _vm._v(
                              _vm._s(
                                _vm.models[_vm.number].questions[
                                  _vm.questNumber
                                ].title
                              )
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "label",
                          {
                            staticClass: "form-check-label question",
                            attrs: { for: "true" }
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.answer,
                                  expression: "answer"
                                }
                              ],
                              staticClass: "form-check-input",
                              attrs: {
                                type: "radio",
                                name: "true",
                                id: "true",
                                value: "true"
                              },
                              domProps: { checked: _vm._q(_vm.answer, "true") },
                              on: {
                                change: function($event) {
                                  _vm.answer = "true"
                                }
                              }
                            }),
                            _vm._v(" True\n          ")
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "label",
                          {
                            staticClass: "form-check-label question",
                            attrs: { for: "false" }
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.answer,
                                  expression: "answer"
                                }
                              ],
                              staticClass: "form-check-input",
                              attrs: {
                                type: "radio",
                                name: "false",
                                id: "false",
                                value: "false"
                              },
                              domProps: {
                                checked: _vm._q(_vm.answer, "false")
                              },
                              on: {
                                change: function($event) {
                                  _vm.answer = "false"
                                }
                              }
                            }),
                            _vm._v(" False\n          ")
                          ]
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "mc"
                    ? _c(
                        "div",
                        { staticClass: "question" },
                        [
                          _c("div", { staticClass: "d-flex mb-2" }, [
                            _c("span", { staticClass: "bold pr-2" }, [
                              _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                            ]),
                            _vm._v(" "),
                            _c("p", {}, [
                              _vm._v(
                                _vm._s(
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].title
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._l(
                            _vm.models[_vm.number].questions[_vm.questNumber]
                              .options,
                            function(check, index) {
                              return _c(
                                "div",
                                {
                                  key: index,
                                  staticClass: "form-check question"
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.checkAnswer,
                                        expression: "checkAnswer"
                                      }
                                    ],
                                    staticClass: "form-check-input",
                                    attrs: {
                                      type: "checkbox",
                                      name: "checkBox",
                                      id: check
                                    },
                                    domProps: {
                                      value: check,
                                      checked: Array.isArray(_vm.checkAnswer)
                                        ? _vm._i(_vm.checkAnswer, check) > -1
                                        : _vm.checkAnswer
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = _vm.checkAnswer,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = check,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              (_vm.checkAnswer = $$a.concat([
                                                $$v
                                              ]))
                                          } else {
                                            $$i > -1 &&
                                              (_vm.checkAnswer = $$a
                                                .slice(0, $$i)
                                                .concat($$a.slice($$i + 1)))
                                          }
                                        } else {
                                          _vm.checkAnswer = $$c
                                        }
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    {
                                      staticClass: "form-check-label",
                                      attrs: { for: check }
                                    },
                                    [_vm._v(_vm._s(check))]
                                  )
                                ]
                              )
                            }
                          )
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "radio"
                    ? _c(
                        "div",
                        { staticClass: "question" },
                        [
                          _c("div", { staticClass: "d-flex mb-2" }, [
                            _c("span", { staticClass: "bold pr-2" }, [
                              _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                            ]),
                            _vm._v(" "),
                            _c("p", {}, [
                              _vm._v(
                                _vm._s(
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].title
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._l(
                            _vm.models[_vm.number].questions[_vm.questNumber]
                              .options,
                            function(check, index) {
                              return _c(
                                "div",
                                {
                                  key: index,
                                  staticClass: "form-check question"
                                },
                                [
                                  _vm.models[_vm.number].questions[
                                    _vm.questNumber
                                  ].type === "checkbox"
                                    ? _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type: "checkbox"
                                        },
                                        domProps: {
                                          value: check,
                                          checked: Array.isArray(_vm.answer)
                                            ? _vm._i(_vm.answer, check) > -1
                                            : _vm.answer
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = _vm.answer,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = check,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  (_vm.answer = $$a.concat([
                                                    $$v
                                                  ]))
                                              } else {
                                                $$i > -1 &&
                                                  (_vm.answer = $$a
                                                    .slice(0, $$i)
                                                    .concat($$a.slice($$i + 1)))
                                              }
                                            } else {
                                              _vm.answer = $$c
                                            }
                                          }
                                        }
                                      })
                                    : _vm.models[_vm.number].questions[
                                        _vm.questNumber
                                      ].type === "radio"
                                    ? _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type: "radio"
                                        },
                                        domProps: {
                                          value: check,
                                          checked: _vm._q(_vm.answer, check)
                                        },
                                        on: {
                                          change: function($event) {
                                            _vm.answer = check
                                          }
                                        }
                                      })
                                    : _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.answer,
                                            expression: "answer"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        attrs: {
                                          name: "radioAnswer",
                                          id: check,
                                          type:
                                            _vm.models[_vm.number].questions[
                                              _vm.questNumber
                                            ].type
                                        },
                                        domProps: {
                                          value: check,
                                          value: _vm.answer
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.answer = $event.target.value
                                          }
                                        }
                                      }),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    {
                                      staticClass: "form-check-label",
                                      attrs: { for: check }
                                    },
                                    [_vm._v(_vm._s(check))]
                                  )
                                ]
                              )
                            }
                          )
                        ],
                        2
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.models[_vm.number].questions[_vm.questNumber].type ===
                  "text"
                    ? _c("div", { staticClass: "question" }, [
                        _c("div", { staticClass: "d-flex mb-2" }, [
                          _c("span", { staticClass: "bold pr-2" }, [
                            _vm._v("Q" + _vm._s(_vm.questNumber + 1) + ".")
                          ]),
                          _vm._v(" "),
                          _c("p", {}, [
                            _vm._v(
                              _vm._s(
                                _vm.models[_vm.number].questions[
                                  _vm.questNumber
                                ].title
                              )
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group question" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.answer,
                                expression: "answer"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "text",
                              id: "textT",
                              placeholder: "Enter your answer"
                            },
                            domProps: { value: _vm.answer },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.answer = $event.target.value
                              }
                            }
                          })
                        ])
                      ])
                    : _vm._e()
                ])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("div", [
          _vm.showScores || _vm.showSubmit
            ? _c("div", { staticClass: "mt-4 contents scores" }, [
                _vm.showScores
                  ? _c("div", { staticClass: "animated slideInUp faster" }, [
                      _vm.result.name === "template"
                        ? _c("div", [
                            _c(
                              "div",
                              { staticClass: "card-columns" },
                              _vm._l(_vm.models, function(model, index) {
                                return _c(
                                  "div",
                                  { key: index, staticClass: "card" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "card-header bg-primary text-white text-center"
                                      },
                                      [
                                        _c("h4", [
                                          _vm._v(_vm._s(model.category))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.modelAnswer[index], function(
                                      result,
                                      idx
                                    ) {
                                      return _c(
                                        "div",
                                        { key: idx, staticClass: "card-body" },
                                        [
                                          _c(
                                            "h5",
                                            { staticClass: "card-title" },
                                            [
                                              _vm._v(
                                                "Q" +
                                                  _vm._s(idx + 1) +
                                                  ". " +
                                                  _vm._s(
                                                    model.questions[idx].title
                                                  )
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          typeof result == "object"
                                            ? _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _c(
                                                    "ul",
                                                    {
                                                      staticClass:
                                                        "text-capitalize"
                                                    },
                                                    _vm._l(result, function(
                                                      results,
                                                      idx
                                                    ) {
                                                      return _c(
                                                        "li",
                                                        { key: idx },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              results.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    }),
                                                    0
                                                  )
                                                ]
                                              )
                                            : _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      result
                                                        .toLowerCase()
                                                        .replace(/''/g, " ")
                                                    )
                                                  )
                                                ]
                                              )
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ])
                        : _vm.result.name === "bg"
                        ? _c("div", [
                            _c(
                              "div",
                              { staticClass: "card-columns" },
                              _vm._l(_vm.models, function(model, index) {
                                return _c(
                                  "div",
                                  { key: index, staticClass: "card" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "card-header bg-primary text-white text-center"
                                      },
                                      [
                                        _c("h4", [
                                          _vm._v(_vm._s(model.category))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.modelAnswer[index], function(
                                      result,
                                      idx
                                    ) {
                                      return _c(
                                        "div",
                                        { key: idx, staticClass: "card-body" },
                                        [
                                          typeof result == "object "
                                            ? _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                    Response :\n                    "
                                                  ),
                                                  _vm._l(result, function(
                                                    results,
                                                    idx
                                                  ) {
                                                    return _c(
                                                      "span",
                                                      {
                                                        key: idx,
                                                        staticClass:
                                                          "text-capitalize"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            results
                                                              .toLowerCase()
                                                              .replace(
                                                                /''/g,
                                                                "-"
                                                              )
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  })
                                                ],
                                                2
                                              )
                                            : _c(
                                                "p",
                                                {
                                                  staticClass:
                                                    "card-text text-capitalize"
                                                },
                                                [
                                                  _vm._v(
                                                    "Response : " +
                                                      _vm._s(
                                                        result.toLowerCase()
                                                      )
                                                  )
                                                ]
                                              )
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ])
                        : _c("div", { staticClass: "border p-5" }, [
                            _c("h3", { staticClass: "mb-4" }, [
                              _vm._v(
                                "Congratulations you just completed this Case Study Gym Exercise"
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex justify-content-around" },
                              [
                                _vm.answers.length > 0
                                  ? _c(
                                      "div",
                                      { staticClass: "border p-3 bgG" },
                                      [
                                        _c("h6", [_vm._v("What you chose")]),
                                        _vm._v(" "),
                                        _vm._l(_vm.answers, function(
                                          answer,
                                          idx
                                        ) {
                                          return _c(
                                            "p",
                                            {
                                              key: idx,
                                              staticClass:
                                                "animated fadeIn slow text-white"
                                            },
                                            [
                                              _vm._v(
                                                "\n                  (Q" +
                                                  _vm._s(idx + 1) +
                                                  ".)\n                  "
                                              ),
                                              _vm._l(answer, function(ans, id) {
                                                return _c("span", { key: id }, [
                                                  _vm._v(_vm._s(ans) + ",")
                                                ])
                                              })
                                            ],
                                            2
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.answers.length > 0
                                  ? _c(
                                      "div",
                                      { staticClass: "border p-3 bgG" },
                                      [
                                        _c("h6", [_vm._v("What we expected")]),
                                        _vm._v(" "),
                                        _vm._l(
                                          _vm.models[0].questions,
                                          function(answer, idx) {
                                            return _c(
                                              "p",
                                              {
                                                key: idx,
                                                staticClass:
                                                  "animated fadeIn slow text-white"
                                              },
                                              [
                                                _vm._v(
                                                  "(Ans" +
                                                    _vm._s(idx + 1) +
                                                    "). " +
                                                    _vm._s(answer.answer)
                                                )
                                              ]
                                            )
                                          }
                                        )
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "border p-3 mt-4 min-768" },
                              [
                                _c("p", [
                                  _vm._v(
                                    "How would you apply this new insight to your Business?"
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", [
                                  _c("div", { staticClass: "input-group" }, [
                                    _vm._m(0),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.first,
                                          expression: "response.first"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.response.first },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "first",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.durationFirst,
                                          expression: "response.durationFirst"
                                        }
                                      ],
                                      attrs: {
                                        type: "text",
                                        placeholder: "duration"
                                      },
                                      domProps: {
                                        value: _vm.response.durationFirst
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "durationFirst",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "input-group" }, [
                                    _vm._m(1),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.second,
                                          expression: "response.second"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.response.second },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "second",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.durationSecond,
                                          expression: "response.durationSecond"
                                        }
                                      ],
                                      attrs: {
                                        type: "text",
                                        placeholder: "duration"
                                      },
                                      domProps: {
                                        value: _vm.response.durationSecond
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "durationSecond",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "input-group" }, [
                                    _vm._m(2),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.third,
                                          expression: "response.third"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.response.third },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "third",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.durationThird,
                                          expression: "response.durationThird"
                                        }
                                      ],
                                      attrs: {
                                        type: "text",
                                        placeholder: "duration"
                                      },
                                      domProps: {
                                        value: _vm.response.durationThird
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "durationThird",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "input-group" }, [
                                    _vm._m(3),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.fourth,
                                          expression: "response.fourth"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.response.fourth },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "fourth",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.durationFourth,
                                          expression: "response.durationFourth"
                                        }
                                      ],
                                      attrs: {
                                        type: "text",
                                        placeholder: "duration"
                                      },
                                      domProps: {
                                        value: _vm.response.durationFourth
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "durationFourth",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "input-group" }, [
                                    _vm._m(4),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.fifth,
                                          expression: "response.fifth"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.response.fifth },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "fifth",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.response.durationFifth,
                                          expression: "response.durationFifth"
                                        }
                                      ],
                                      attrs: {
                                        type: "text",
                                        placeholder: "duration"
                                      },
                                      domProps: {
                                        value: _vm.response.durationFifth
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.response,
                                            "durationFifth",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      " elevated_btn elevated_btn_sm text-white btn-compliment my-2 px-5",
                                    attrs: { type: "button" },
                                    on: { click: _vm.storeAnswer }
                                  },
                                  [_vm._v("Send")]
                                )
                              ]
                            )
                          ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.showSubmit
                  ? _c(
                      "div",
                      { staticClass: "border p-3 bgG" },
                      [
                        _c("h6", [_vm._v("What you chose")]),
                        _vm._v(" "),
                        _vm._l(_vm.answers, function(answer, idx) {
                          return _c(
                            "p",
                            {
                              key: idx,
                              staticClass: "animated fadeIn slow text-white"
                            },
                            [
                              _vm._v(
                                "(Q" + _vm._s(idx + 1) + "). " + _vm._s(answer)
                              )
                            ]
                          )
                        })
                      ],
                      2
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.showSubmit
                  ? _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn elevated_btn_sm text-white btn-compliment submit m-2",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            return _vm.submit()
                          }
                        }
                      },
                      [_vm._v("Submit")]
                    )
                  : _vm._e()
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _vm.showQuestions
          ? _c("div", { staticClass: "navigation questionNav" }, [
              _vm.questNumber !== 0
                ? _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn elevated_btn_sm text-white btn-compliment btn-primary  my-4",
                      attrs: { type: "button" },
                      on: { click: _vm.prevQuestion }
                    },
                    [_vm._v("Prev question")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm text-white btn-compliment my-4",
                  attrs: { type: "button", disabled: _vm.disable },
                  on: {
                    click: function($event) {
                      return _vm.nextQuestion(
                        _vm.models[_vm.number].questions[_vm.questNumber].type
                      )
                    }
                  }
                },
                [_vm._v("Next question")]
              )
            ])
          : _vm._e()
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Response 1")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Response 2")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Response 3")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Response 4")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Response 5")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-583a482b", module.exports)
  }
}

/***/ }),

/***/ 637:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1815)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1817)
/* template */
var __vue_template__ = __webpack_require__(1818)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-583a482b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/diagnosticsComponents.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-583a482b", Component.options)
  } else {
    hotAPI.reload("data-v-583a482b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});