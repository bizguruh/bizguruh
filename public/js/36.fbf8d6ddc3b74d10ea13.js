webpackJsonp([36],{

/***/ 1123:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1124);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("6e5dd09c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-781a725e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./settingsVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-781a725e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./settingsVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1124:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sideVendorComponent__ = __webpack_require__(973);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sideVendorComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__sideVendorComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent__ = __webpack_require__(978);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: "settings-vendor-component",
    data: function data() {
        return {
            token: '',
            settings: {
                id: '',
                storeName: '',
                sidebarColor: '',
                sidebarFontSize: '',
                sidebarMargin: '',
                sidebarBackgroundColor: '',
                topMenuColor: '',
                topMenuBackgroundColor: '',
                topMenuFontSize: '',
                topMenuMargin: '',
                overallBackgroundColor: '',
                overallColor: '',
                overFontSize: ''
            }
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authVendor')) {
            var authVendor = JSON.parse(localStorage.getItem('authVendor'));
            this.token = authVendor.access_token;
            this.settings.id = authVendor.id;
            axios.get('/api/get/vendor/' + this.settings.id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                _this.settings = response.data.data;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        vendorSettings: function vendorSettings() {
            console.log(this.settings);
            axios.post('/api/vendor/settings', JSON.parse(JSON.stringify(this.settings)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(response);
                window.location.reload();
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1126:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Change shop Name")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.storeName,
              expression: "settings.storeName"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "changeColorName" },
          domProps: { value: _vm.settings.storeName },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "storeName", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Sidebar color change")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.sidebarColor,
              expression: "settings.sidebarColor"
            }
          ],
          attrs: { type: "color", id: "sideBarColorChange" },
          domProps: { value: _vm.settings.sidebarColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "sidebarColor", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Sidebar Font Size")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.sidebarFontSize,
              expression: "settings.sidebarFontSize"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "sideBarTextColorChange" },
          domProps: { value: _vm.settings.sidebarFontSize },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "sidebarFontSize", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Sidebar Margin")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.sidebarMargin,
              expression: "settings.sidebarMargin"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "sideBarMarginChange" },
          domProps: { value: _vm.settings.sidebarMargin },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "sidebarMargin", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Sidebar Background color change")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.sidebarBackgroundColor,
              expression: "settings.sidebarBackgroundColor"
            }
          ],
          attrs: { type: "color", id: "sideBarBackgroundColorChange" },
          domProps: { value: _vm.settings.sidebarBackgroundColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(
                _vm.settings,
                "sidebarBackgroundColor",
                $event.target.value
              )
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Top Menu color change")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.topMenuColor,
              expression: "settings.topMenuColor"
            }
          ],
          attrs: { type: "color", id: "topMenuColorChange" },
          domProps: { value: _vm.settings.topMenuColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "topMenuColor", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Top Menu Background color change")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.topMenuBackgroundColor,
              expression: "settings.topMenuBackgroundColor"
            }
          ],
          attrs: { type: "color", id: "topMenuBackgroundColor" },
          domProps: { value: _vm.settings.topMenuBackgroundColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(
                _vm.settings,
                "topMenuBackgroundColor",
                $event.target.value
              )
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Top Menu Font Size")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.topMenuFontSize,
              expression: "settings.topMenuFontSize"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "topMenuFontSize" },
          domProps: { value: _vm.settings.topMenuFontSize },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "topMenuFontSize", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Top Menu Margin")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.topMenuMargin,
              expression: "settings.topMenuMargin"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "topMenuMargin" },
          domProps: { value: _vm.settings.topMenuMargin },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "topMenuMargin", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Background color")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.overallBackgroundColor,
              expression: "settings.overallBackgroundColor"
            }
          ],
          attrs: { type: "color", id: "backgroundColorChange" },
          domProps: { value: _vm.settings.overallBackgroundColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(
                _vm.settings,
                "overallBackgroundColor",
                $event.target.value
              )
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Background color")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.overallColor,
              expression: "settings.overallColor"
            }
          ],
          attrs: { type: "color", id: "colorChange" },
          domProps: { value: _vm.settings.overallColor },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "overallColor", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("span", [_vm._v("Font Size")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.settings.overFontSize,
              expression: "settings.overFontSize"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", id: "fontSizeChange" },
          domProps: { value: _vm.settings.overFontSize },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.settings, "overFontSize", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6" }, [
        _c("input", {
          staticClass: "btn btn-primary",
          attrs: {
            type: "button",
            id: "submitVendorSettings",
            value: "submit"
          },
          on: {
            click: function($event) {
              return _vm.vendorSettings()
            }
          }
        })
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-781a725e", module.exports)
  }
}

/***/ }),

/***/ 498:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1123)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1125)
/* template */
var __vue_template__ = __webpack_require__(1126)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-781a725e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/settingsVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-781a725e", Component.options)
  } else {
    hotAPI.reload("data-v-781a725e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 973:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(974)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(976)
/* template */
var __vue_template__ = __webpack_require__(977)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1779c6aa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/sideVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1779c6aa", Component.options)
  } else {
    hotAPI.reload("data-v-1779c6aa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 974:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(975);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("058369d5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 975:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\na[data-v-1779c6aa] {\n  color: #b8c7ce !important;\n}\nspan[data-v-1779c6aa] {\n  font-size: 16px;\n}\n.router-link-exact-active[data-v-1779c6aa]{\nbackground: #1e282c;\nborder-left-color: #3c8dbc;\n}\n.faded[data-v-1779c6aa]{\n  cursor:not-allowed!important;\n  color: rgba(255, 255, 255, 0.4);\n}\n.main-sidebar[data-v-1779c6aa] {\n  \n  background-image: url(\"/images/education.jpg\");\n  background-size: cover;\n  z-index: 0;\n  width: 20%;\n  position: relative;\n  padding-top:0;\n}\n.overlay[data-v-1779c6aa] {\n  background: rgba(29, 53, 73, 0.8);\n  z-index: 1;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.sidebar[data-v-1779c6aa] {\n  padding-top: 30px;\n  z-index: 2;\n  position: relative;\n}\n.main-sidebar-nav[data-v-1779c6aa] {\n  display: block !important;\n  -webkit-transform: translate(-230px, 0);\n  transform: translate(0, 0);\n}\n@media only screen and (max-width: 768px) {\n.main-sidebar[data-v-1779c6aa] {\n    display: none;\n}\n.main-sidebar[data-v-1779c6aa] {\n    width:100%;\n}\nspan[data-v-1779c6aa] {\n    font-size: 16px;\n}\n.sidebar ul li a[data-v-1779c6aa] {\n    padding: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "side-vendor-component",
  props: ["showNav", 'vendorSub'],
  data: function data() {
    return {
      id: "",
      token: "",
      backgroundColor: "",
      fontSize: "",
      color: "",
      sidebar: "",
      webAdmin: false,
      storeAdmin: false,
      authVendor: {},
      username: ""
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      this.username = authVendor.storeName;
      this.authVendor = authVendor;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.sidebarBackgroundColor;
        // this.fontSize = response.data.data.sidebarFontSize;
        // this.color = response.data.data.sidebarColor;
        // this.sidebar = response.data.data.sidebarMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth?type=login");
    }
  },

  methods: {
    shakeButton: function shakeButton() {
      this.$emit('shakeButton');
    },
    logout: function logout() {
      Engagespot.clearUser();
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth?type=login");
    }
  }
});

/***/ }),

/***/ 977:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "aside",
    { staticClass: "main-sidebar", class: { "main-sidebar-nav": _vm.showNav } },
    [
      _c("div", { staticClass: "overlay" }),
      _vm._v(" "),
      _c("section", { staticClass: "sidebar" }, [
        _c(
          "ul",
          { staticClass: "sidebar-menu", attrs: { "data-widget": "tree" } },
          [
            _c("li", { staticClass: "px-2" }, [
              _vm.vendorSub < 3
                ? _c(
                    "button",
                    { staticClass: "elevated_btn elevated_btn_sm text-main" },
                    [_vm._v("Upgrade")]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: "/vendor/dashboard" } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-chart-line pr-3" }),
                      _vm._v(" Dashboard\n          ")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "pull-right-container" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: { name: "viewVendorProduct" } } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-box-open pr-3" }),
                      _vm._v(" Resources\n          ")
                    ])
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "orderVendor" } } },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-bell pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v("Subscribers\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Subscribers\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "messagesVendor" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-inbox pr-3" }),
                          _vm._v(" Inbox\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-inbox pr-3 faded" }),
                        _vm._v(" Inbox\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: "/vendor-form-questions" } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fa fa-wpforms pr-3" }),
                          _vm._v(" Forms\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Forms\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "VendorStatistics" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-chart-bar pr-3" }),
                          _vm._v(" Insights\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded ", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-chart-bar pr-3 faded" }),
                        _vm._v(" Insights\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.authVendor.type === "vendor"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.authVendor.type === "expert"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c(
                  "span",
                  {
                    on: {
                      click: function($event) {
                        return _vm.logout()
                      }
                    }
                  },
                  [
                    _c("i", {
                      staticClass: "fa fa-sign-out pr-3",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" Logout\n          ")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._m(0)
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "p-3" }, [
      _c("span", { attrs: { id: "notification" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1779c6aa", module.exports)
  }
}

/***/ }),

/***/ 978:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(979)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(981)
/* template */
var __vue_template__ = __webpack_require__(982)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-41d38ab3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/stickyHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-41d38ab3", Component.options)
  } else {
    hotAPI.reload("data-v-41d38ab3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 979:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(980);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3f8a0fc8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 980:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.header-section[data-v-41d38ab3] {\n  background: #fff;\n  position: relative;\n  padding: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hamburger[data-v-41d38ab3]{\n  padding:10px;\n}\n.button-blue[data-v-41d38ab3] {\n  font-size: 15px;\n}\n.mobile-nav[data-v-41d38ab3] {\n  display: none;\n}\n.header-right[data-v-41d38ab3] {\n  float: unset;\n  width: auto !important;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.imgDev[data-v-41d38ab3] {\n  width: 25px;\n  height: 25px;\n  border-radius: 50%;\n  margin-top: 0;\n}\n.dropDownVendor[data-v-41d38ab3] {\n  background-color: #ffffff;\n  position: absolute;\n  width: 150px;\n  left: -30px;\n  top: 57px;\n}\n.pointer[data-v-41d38ab3] {\n  font-size: 16px;\n  padding-left: 20px;\n  cursor: pointer;\n  color: hsl(207, 46%, 20%);\n}\n.pointer[data-v-41d38ab3]:hover {\n  color: #333333;\n}\n.nav_item[data-v-41d38ab3] {\n  color: hsl(207, 46%, 20%);\n  font-size: 15px;\n  border-radius: 5px;\n}\n.list[data-v-41d38ab3] {\n  border-bottom: 1px solid #f5f5f5;\n  padding: 5px;\n  position: relative;\n}\n.list[data-v-41d38ab3]::before {\n  position: absolute;\n  content: \"\";\n  top: -6px;\n  right: 20%;\n  width: 15px;\n  height: 15px;\n  background: #fff;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n/* .userAcc {\n  margin: 25px 0 5px;\n} */\nol[data-v-41d38ab3],\nul[data-v-41d38ab3] {\n  list-style: none;\n}\n.accountCaret[data-v-41d38ab3] {\n  margin-left: 0 !important;\n}\n.accAvatar[data-v-41d38ab3] {\n  margin-right: 0;\n}\n.accountProfile[data-v-41d38ab3] {\n  font-size: 16px;\n  padding: 5px 15px;\n  background: #f7f8fa;\n  position: relative;\n}\n.biz[data-v-41d38ab3] {\n  color: #a4c2db !important;\n}\n.guru[data-v-41d38ab3] {\n  color: #333333;\n}\n.fa-bars[data-v-41d38ab3] {\n  color: #a4c2db;\n}\n.businessLogo[data-v-41d38ab3] {\n  font-size: 24px;\n  margin-right: auto;\n}\n.slide-fade-enter-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.3s ease;\n  transition: all 0.3s ease;\n}\n.slide-fade-leave-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.2s ease;\n  transition: all 0.2s ease;\n}\n.slide-fade-enter[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n.slide-fade-leave-to[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n@media only screen and (max-width: 768px) {\n.userAccName[data-v-41d38ab3] {\n    display: none;\n}\n.animated[data-v-41d38ab3] {\n    display: none;\n}\n.dropDownVendor[data-v-41d38ab3] {\n    left: -100px;\n}\n.imgDev[data-v-41d38ab3] {\n    width: 22px;\n    height: 22px;\n    border-radius: 50%;\n    margin-top: 0;\n}\n.businessLogo[data-v-41d38ab3] {\n    font-size: 25px;\n    margin-top: 5px;\n    margin-left: auto;\n    margin-right: auto;\n    margin-bottom: 5px;\n}\n.mobile-nav[data-v-41d38ab3] {\n    display: inline;\n    margin-right: 25px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var user = JSON.parse(localStorage.getItem("authVendor"));
Engagespot.init("cywCppTlinzpmjVHxFcYoiuuKlnefc");
if (user !== null) {
  Engagespot.identifyUser(user.id);
}

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "sticky-header-component",
  props: ["profileImage", "vendorSub", "shake", "myNav"],
  data: function data() {
    return {
      id: "",
      token: "",
      username: "",
      linkTag: false,
      backgroundColor: "",
      fontSize: "",
      color: "",
      topMenu: "",
      showDropDown: false,
      links: [{
        name: "Profile",
        url: "vendorProfile"
      }, {
        name: "Product",
        url: "viewVendorProduct"
      }, {
        name: "Logout",
        url: "logout"
      }]
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.username = authVendor.storeName;
      this.linkTag = true;
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.topMenuBackgroundColor;
        // this.fontSize = response.data.data.topMenuFontSize;
        // this.color = response.data.data.topMenuColor;
        // this.topMenu = response.data.data.topMenuMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    logout: function logout() {
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth");
    },
    openNav: function openNav() {
      this.$emit("mobile-nav");
    }
  }
});

/***/ }),

/***/ 982:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "sticky-header header-section shadow-sm",
      style: { backgroundColor: _vm.backgroundColor }
    },
    [
      _c("div", { staticClass: "mobile-nav" }, [
        _c(
          "span",
          {
            staticClass:
              "d-flex justify-content-start align-items-center mobile-nav",
            on: { click: _vm.openNav }
          },
          [
            _c(
              "button",
              {
                staticClass: "hamburger hamburger--collapse",
                class: { "is-active": _vm.myNav },
                attrs: {
                  tabindex: "0",
                  "aria-label": "Menu",
                  role: "button",
                  "aria-controls": "navigation",
                  type: "button"
                }
              },
              [_vm._m(0)]
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "businessLogo mr-4" },
        [
          _c("router-link", { attrs: { to: "/" } }, [
            _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
            _c("span", { staticClass: "guru" }, [_vm._v("Guruh")])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.$route.meta.vendor
        ? _c(
            "router-link",
            {
              staticClass: "nav_item p-1 border",
              attrs: { to: "/vendor/home" }
            },
            [
              _c("i", {
                staticClass: "fa fa-home",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v("\nMy Home\n   ")
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "header-right ml-auto" }, [
        _vm.vendorSub < 3
          ? _c(
              "button",
              {
                staticClass: "button-blue mr-4 animated",
                class: { shake: _vm.shake }
              },
              [_vm._v("Upgrade")]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("div", { attrs: { id: "notification mr-3" } }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "accountProfile rounded-pill" },
          [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.showDropDown = !_vm.showDropDown
                  }
                }
              },
              [
                _c("span", { staticClass: "userAcc accAvatar" }, [
                  _vm.profileImage !== ""
                    ? _c("span", { staticClass: "pr-2" }, [
                        _c("img", {
                          staticClass: "imgDev",
                          attrs: { src: _vm.profileImage, alt: "avatar" }
                        })
                      ])
                    : _c("i", {
                        staticClass: "fas fa-user-circle-o text-main pr-2"
                      }),
                  _vm._v(" "),
                  _c("span", { staticClass: "userAccName text-main" }, [
                    _vm._v(_vm._s(_vm.username))
                  ])
                ]),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa userAcc accountCaret text-main",
                  class: {
                    "fa-caret-up": _vm.showDropDown,
                    "fa-caret-down": !_vm.showDropDown
                  },
                  attrs: { "aria-hidden": "true" }
                })
              ]
            ),
            _vm._v(" "),
            _c("transition", { attrs: { name: "slide-fade" } }, [
              _vm.showDropDown
                ? _c("div", { staticClass: "dropDownVendor shadow-sm" }, [
                    _c("ul", { staticClass: "menu list pl0 pa0 ma0" }, [
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "viewVendorProduct" } }
                            },
                            [_vm._v("Resources")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: {
                                to: {
                                  name: "vendorProfile",
                                  params: { profile: _vm.username }
                                }
                              }
                            },
                            [_vm._v("Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "vendorConfig" } }
                            },
                            [_vm._v("Update Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("li", { staticClass: "list" }, [
                        _c(
                          "a",
                          {
                            staticClass: "dd-link pointer hover-bg-moon-gray",
                            on: { click: _vm.logout }
                          },
                          [_vm._v("Logout")]
                        )
                      ])
                    ])
                  ])
                : _vm._e()
            ])
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "hamburger-box" }, [
      _c("span", { staticClass: "hamburger-inner" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-41d38ab3", module.exports)
  }
}

/***/ })

});