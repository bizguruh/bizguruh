webpackJsonp([123],{

/***/ 1662:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1663);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0ad25e1f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4dee4550\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./preferenceComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4dee4550\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./preferenceComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1663:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nlabel span[data-v-4dee4550]{\n    color:rgba(0, 0, 0, .84) !important;\n}\n.singlePref[data-v-4dee4550]{\n    margin:10px;\n}\n.container[data-v-4dee4550]{\n    padding-top:60px;\n}\n.text[data-v-4dee4550]{\n    text-transform: inherit;\n    padding: 40px;\n    width: 100%;\n}\n.row[data-v-4dee4550] {\n  padding: 0;\n}\n.ppc[data-v-4dee4550] {\n  border: 1px solid #a3c2dc;\n  margin: 20px;\n  padding: 20px;\n}\n.preferenceP[data-v-4dee4550] {\n  height: 400px;\n  overflow: scroll;\n  padding: 20px 30px;\n  border: 2px solid #a3c2dc;\n  margin: 30px;\n  border-radius: 10px;\n}\n.tpref[data-v-4dee4550],\n.smatter[data-v-4dee4550] {\n  text-transform: capitalize;\n  font-size: calc(12px + (16 - 12) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.matterP[data-v-4dee4550] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.sec_header[data-v-4dee4550] {\n  padding: 1px;\n  margin-bottom: 20px;\n  border: 1px solid #a3c2dc;\n  background: #a3c2dc;\n  border-radius: 80px 80px 80px 80px;\n  text-align: center;\n  color: hsl(207, 45%, 99%);\n  font-size: calc(14px + (16 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.checkboxP[data-v-4dee4550] {\n  margin-right: 30px;\n}\n.btn-p[data-v-4dee4550] {\n  background-color: #a3c2dc !important;\n  color: #ffffff;\n  text-transform: capitalize;\n}\n.btn-pp[data-v-4dee4550] {\n\n  color: #b1c6d6;\n  border:none;\n  text-transform: capitalize;\n}\n.btn-pp[data-v-4dee4550]:hover {\n\n  text-decoration: underline;\n}\n.skip[data-v-4dee4550]{\n      padding:0;\n}\n.password[data-v-4dee4550] {\n  margin-top: 40px;\n  text-align: left;\n}\n@media (max-width: 768px) {\n.preferenceP[data-v-4dee4550] {\n    margin: 30px;\n    height: 300px;\n}\nh5[data-v-4dee4550]{\n    font-size: 18px !important;\n}\n.text[data-v-4dee4550]{\n    font-size: 18px;\n}\n}\n@media (max-width: 425px) {\n.container[data-v-4dee4550] {\n    padding-top: 30px;\n}\n.preferenceP[data-v-4dee4550] {\n    height: 200px;\n    padding: 20px 30px;\n    border: 2px solid #a3c2dc;\n    border-radius: 10px;\n    margin: 20px;\n}\n.pry_header[data-v-4dee4550] {\n    border-bottom: 2px solid #a3c2dc;\n}\n.password[data-v-4dee4550] {\n    margin-top: 40px;\n}\n.mobile[data-v-4dee4550] {\n    height: 200px;\n}\n.text[data-v-4dee4550]{\n      padding: 5px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1664:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-preference-component",
  data: function data() {
    return {
      user: {},
      matterPreferences: [],
      topicPreferences: [],
      item: [],
      matter: [],
      passwordChange: {
        oldPassword: "",
        password: "",
        password_confirmation: ""
      },
      dialog: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      this.user = user;
      axios.get("/api/user/get/subjectmatter", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 200) {

          _this.matterPreferences = response.data;
          _this.getProductTopic();
        }
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.replace({ name: "auth" });
    }
  },

  computed: {
    loopMatter: function loopMatter() {

      return this.matter.filter(function (item) {
        return item;
      });
    },
    filteredTopic: function filteredTopic() {
      var _this2 = this;

      return this.topicPreferences.filter(function (element) {

        if (parseInt(_this2.loopMatter) === parseInt(element.sub_brand_category_id)) {
          return element;
        }
      });
    }
  },
  methods: {
    changePasswordForUser: function changePasswordForUser() {
      var _this3 = this;

      axios.post("/api/user/changePassword", JSON.parse(JSON.stringify(this.passwordChange)), { headers: { Authorization: "Bearer " + this.user.access_token } }).then(function (response) {
        if (response.status === 200) {
          localStorage.removeItem("authUser");
          window.location.reload();
        }
      }).catch(function (error) {
        if (Array.isArray(error.response.data.errors.oldPassword) || Array.isArray(error.response.data.errors.password) || Array.isArray(error.response.data.errors.password_confirmation)) {
          var errors = Object.values(error.response.data.errors);
          errors.forEach(function (item) {
            _this3.$toasted.error(item[0]);
          });
        } else {
          _this3.$toasted.error(error.response.data.errors);
        }
      });
    },
    getProductTopic: function getProductTopic() {
      var _this4 = this;

      axios.get("/api/user/get/producttopic", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 200) {

          _this4.topicPreferences = response.data;
          _this4.getUserPreference();
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getUserPreference: function getUserPreference() {
      var _this5 = this;

      axios.get("/api/userPreferences", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {

        if (response.status === 200) {
          response.data.data.forEach(function (items) {
            _this5.item.push(items);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    submitPreference: function submitPreference() {
      var _this6 = this;

      axios.post("/api/user-preference", JSON.parse(JSON.stringify(this.item)), {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 200) {

          _this6.$toasted.success(" Saved");
          _this6.$router.push(_this6.$route.query.redirect || "/explore");
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    skip: function skip() {
      this.$toasted.success(" Saved");
      this.$router.push(this.$route.query.redirect || "/explore");
    }
  }
});

/***/ }),

/***/ 1665:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row" }, [
      _c("h3", { staticClass: "col-lg-12 col-md-12 col-sm-4 col-xs-12 text" }, [
        _vm._v(
          "Welcome to our Biztribe! For a more personalised experience please tell us what you like."
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-5 col-md-5 col-sm-5 col-xs-12 preferenceP mobile "
        },
        [
          _c("h3", { staticClass: "sec_header" }, [_vm._v("Subject Matters")]),
          _vm._v(" "),
          _vm.matterPreferences.length > 0
            ? _c("div", [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.matter,
                        expression: "matter"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { multiple: "", size: "6" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.matter = $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      }
                    }
                  },
                  _vm._l(_vm.matterPreferences, function(matterPreference) {
                    return _c(
                      "option",
                      {
                        key: matterPreference.id,
                        domProps: { value: matterPreference.id }
                      },
                      [_vm._v(_vm._s(matterPreference.name))]
                    )
                  }),
                  0
                )
              ])
            : _vm._e()
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            " col-lg-5 col-md-5  col-sm-4 col-xs-12 preferenceP mobile"
        },
        [
          _c("h3", { staticClass: "sec_header" }, [_vm._v("Topics")]),
          _vm._v(" "),
          _vm._l(_vm.filteredTopic, function(topicPreference, index) {
            return _vm.filteredTopic.length > 0
              ? _c("div", { staticClass: "singlePref" }, [
                  _c("label", { staticClass: "matterP" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.item,
                          expression: "item"
                        }
                      ],
                      staticClass: "checkboxP",
                      attrs: { type: "checkbox" },
                      domProps: {
                        value: {
                          typeId: topicPreference.id,
                          name: topicPreference.name,
                          type: "PT"
                        },
                        checked: Array.isArray(_vm.item)
                          ? _vm._i(_vm.item, {
                              typeId: topicPreference.id,
                              name: topicPreference.name,
                              type: "PT"
                            }) > -1
                          : _vm.item
                      },
                      on: {
                        change: function($event) {
                          var $$a = _vm.item,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = {
                                typeId: topicPreference.id,
                                name: topicPreference.name,
                                type: "PT"
                              },
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 && (_vm.item = $$a.concat([$$v]))
                            } else {
                              $$i > -1 &&
                                (_vm.item = $$a
                                  .slice(0, $$i)
                                  .concat($$a.slice($$i + 1)))
                            }
                          } else {
                            _vm.item = $$c
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "tpref" }, [
                      _vm._v(_vm._s(topicPreference.name))
                    ])
                  ])
                ])
              : _vm._e()
          })
        ],
        2
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row center" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-p center",
            on: { click: _vm.submitPreference }
          },
          [_vm._v("Submit & Explore")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c(
          "button",
          { staticClass: "btn btn-pp center skip", on: { click: _vm.skip } },
          [_vm._v("Skip for later")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4dee4550", module.exports)
  }
}

/***/ }),

/***/ 613:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1662)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1664)
/* template */
var __vue_template__ = __webpack_require__(1665)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4dee4550"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/preferenceComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4dee4550", Component.options)
  } else {
    hotAPI.reload("data-v-4dee4550", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});