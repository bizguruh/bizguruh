webpackJsonp([179],{

/***/ 1402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__js_config__ = __webpack_require__(669);
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },

  methods: {
    AuthProvider: function AuthProvider(provider) {
      var self = this;

      this.$auth.authenticate(provider).then(function (response) {
        self.SocialLogin(provider, response);
      }).catch(function (err) {
        console.log({ err: err });
      });
    },
    SocialLogin: function SocialLogin(provider, response) {
      var _this = this;

      this.$http.post("/sociallogin/" + provider, response).then(function (response) {
        var data = {
          client_id: 2,
          client_secret: "UhnuaSGeXZw8AGS4F3ksNtndO9asVaR7sO0BSC3C",
          grant_type: "social",
          access_token: response.data.token,
          provider: provider
        };
        var authUser = {};
        _this.$http.post("/oauth/token", data).then(function (response) {
          if (response.status === 200) {
            authUser.access_token = response.data.access_token;
            authUser.refresh_token = response.data.refresh_token;
            localStorage.setItem("authUser", JSON.stringify(authUser));

            axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_0__js_config__["b" /* getCustomerHeader */])() }).then(function (response) {});
          }
        });
      }).catch(function (err) {
        console.log({ err: err });
      });
    }
  }
});

/***/ }),

/***/ 1403:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "mt-5 p-5" }, [
    _c("h5", [_vm._v("Redirect")]),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-default",
        on: {
          click: function($event) {
            return _vm.AuthProvider("facebook")
          }
        }
      },
      [_vm._v("auth Facebook")]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-68a17864", module.exports)
  }
}

/***/ }),

/***/ 553:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1402)
/* template */
var __vue_template__ = __webpack_require__(1403)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/socialAuthenticate.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68a17864", Component.options)
  } else {
    hotAPI.reload("data-v-68a17864", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ })

});