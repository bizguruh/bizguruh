webpackJsonp([22],{

/***/ 1159:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1160);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("027ae354", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3e52971d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorFormComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3e52971d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorFormComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1160:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main_page[data-v-3e52971d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n  position: relative;\n}\n.preview[data-v-3e52971d] {\n  position: absolute;\n  background: rgba(255, 255, 255, 0.8);\n  width: 100%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px;\n  z-index: 1;\n}\n.body[data-v-3e52971d] {\n  background: #fff;\n  padding: 25px;\n  height: 90%;\n  overflow: scroll;\n  width: 90%;\n}\n.gui[data-v-3e52971d] {\n  width: 20%;\n  background: #f7f8fa;\n  height: 100%;\n  padding: 30px;\n  text-align: center;\n}\n.live[data-v-3e52971d] {\n  width: 80%;\n  height: 100%;\n  position: relative;\n}\n", ""]);

// exports


/***/ }),

/***/ 1161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__vendorGui__ = __webpack_require__(1162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__vendorGui___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__vendorGui__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vendorLiveComponent__ = __webpack_require__(1167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vendorLiveComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__vendorLiveComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vendorPreview__ = __webpack_require__(1172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vendorPreview___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__vendorPreview__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      prev: false,
      start: true,
      value: "",
      product_id: 0,
      template: {
        form: {
          legend: "",
          sections: []
        }
      },
      item: {},
      myHead: "",
      group: ""
    };
  },
  mounted: function mounted() {
    if (this.$route.query.id != null) {
      this.product_id = this.$route.query.id;
    }
  },

  components: {
    Gui: __WEBPACK_IMPORTED_MODULE_0__vendorGui___default.a,
    Live: __WEBPACK_IMPORTED_MODULE_1__vendorLiveComponent___default.a,
    Preview: __WEBPACK_IMPORTED_MODULE_2__vendorPreview___default.a
  },
  methods: {
    changeGroup: function changeGroup(value) {
      this.group = value;
    },
    previewNow: function previewNow() {
      this.prev = !this.prev;
    },
    updateRowTitle: function updateRowTitle(value, sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.push(value);
    },
    updateValue: function updateValue(value) {
      this.value = value;
    },
    updateHeader: function updateHeader(value) {
      this.myHead = value;
    },
    addForm: function addForm() {
      this.start = true;
    },
    addSection: function addSection() {
      this.template.form.sections.push({
        section: {
          label: "",
          description: '',
          rows: []
        }
      });
    },
    removeSection: function removeSection() {
      this.template.form.sections.pop();
    },
    removeRow: function removeRow(id) {
      this.template.form.sections[id].section.rows.pop();
    },
    addRow: function addRow(id) {
      this.template.form.sections[id].section.rows.push({
        row: [{
          type: "selected",
          multiple: false,
          placeholder: "input text",
          label: "",
          description: "",
          note: "",
          required: false,
          answer: "",
          answers: [],
          values: [],
          table: {
            items: [],
            headers: [],
            row_titles: []
          }
        }]
      });
    },
    addValue: function addValue(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].values.push(this.value);
    },
    addItem: function addItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.push(this.item);
    },
    removeItem: function removeItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.pop();
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.pop();
    },
    addHeader: function addHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.push(this.myHead);

      this.myHead = "";
    },
    removeHeader: function removeHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.pop();
    },
    createForm: function createForm() {
      var _this = this;

      var data = {
        group: this.group,
        title: this.template.form.legend,
        template: this.template,
        product_id: this.product_id
      };
      var user = JSON.parse(localStorage.getItem("authVendor"));
      axios.post("/api/save-form-vendor", JSON.parse(JSON.stringify(data)), {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (res) {
        if (res.status == 200) {
          _this.$router.push('/vendor-form-questions');
        }
      }).catch();
    }
  }
});

/***/ }),

/***/ 1162:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1163)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1165)
/* template */
var __vue_template__ = __webpack_require__(1166)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-432811bf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorGui.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-432811bf", Component.options)
  } else {
    hotAPI.reload("data-v-432811bf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1163:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1164);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("ef913c9e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-432811bf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorGui.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-432811bf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorGui.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1164:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-432811bf] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n}\n", ""]);

// exports


/***/ }),

/***/ 1165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },


  methods: {
    addSection: function addSection() {
      this.$emit("addSection");
    },
    removeSection: function removeSection() {
      this.$emit("removeSection");
    },
    previewNow: function previewNow() {
      this.$emit("previewNow");
    }
  }
});

/***/ }),

/***/ 1166:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", [
      _c("div", { staticClass: "mb-5" }, [
        _c(
          "button",
          {
            staticClass: "elevated_btn elevated_btn_sm text-main",
            attrs: { type: "button" },
            on: { click: _vm.previewNow }
          },
          [_vm._v("Preview")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "mb-5" }, [
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm btn-compliment text-white",
            attrs: { type: "button" },
            on: { click: _vm.addSection }
          },
          [_vm._v("\n        Add Section\n       \n      ")]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm btn-compliment text-white",
            attrs: { type: "button" },
            on: { click: _vm.removeSection }
          },
          [_vm._v("\n        Remove Section\n       \n      ")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-432811bf", module.exports)
  }
}

/***/ }),

/***/ 1167:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1168)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1170)
/* template */
var __vue_template__ = __webpack_require__(1171)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3161f3f5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorLiveComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3161f3f5", Component.options)
  } else {
    hotAPI.reload("data-v-3161f3f5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1168:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1169);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("9494515e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3161f3f5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorLiveComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3161f3f5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorLiveComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1169:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.sub-page[data-v-3161f3f5] {\n    height: 90vh;\n    padding: 30px 20px;\n    overflow-y: scroll;\n}\np[data-v-3161f3f5]{\n  font-size: 15px;\n}\nsmall[data-v-3161f3f5]{\n  font-size: 12px;\n  font-style: italic;\n  color:red;\n}\n.form-control[data-v-3161f3f5]::-webkit-input-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-3161f3f5]::-moz-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-3161f3f5]::-ms-input-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-3161f3f5]::placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-3161f3f5]:-ms-input-placeholder {\n    /* Internet Explorer 10-11 */\n    color: rgba(0, 0, 0, 0.44) !important;\n    font-size: 12px;\n}\n.form-control[data-v-3161f3f5]::-ms-input-placeholder {\n    /* Microsoft Edge */\n    color: rgba(0, 0, 0, 0.44) !important;\n    font-size: 12px;\n}\ntable[data-v-3161f3f5] {\n  font-size: 14px;\n}\nh4[data-v-3161f3f5] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: normal;\n}\n.form[data-v-3161f3f5] {\n  padding: 30px 15px;\n}\n.fa-plus-circle[data-v-3161f3f5],\n.fa-minus-circle[data-v-3161f3f5] {\n  font-size: 12px;\n  float: right;\n  padding: 10px;\n}\nsection[data-v-3161f3f5] {\n  background: rgb(204, 204, 204, 0.5);\n  padding: 15px;\n}\n.rows[data-v-3161f3f5] {\n  background: #f7f8fa;\n  padding: 10px 15px;\n}\n.configs[data-v-3161f3f5] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 10px;\n  background: white;\n  margin-bottom: 10px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mini-label[data-v-3161f3f5] {\n  font-size: 13px;\n}\nlabel span[data-v-3161f3f5] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-3161f3f5] {\n  display: fleX;\n}\n.custom-control-input[data-v-3161f3f5] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-3161f3f5] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-3161f3f5],td[data-v-3161f3f5]{\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
var _methods;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["template", "start"],
  data: function data() {
    return {
      config: false,
      current: 0,
      groups: [],
      myValue: "",
      newHeader: "",
      product_id: 0,
      new_group: false,
      row_title: "",
      form_group: "selected"
    };
  },

  components: {
    "app-editor": __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]
  },
  mounted: function mounted() {
    this.getGroups();
    this.product_id = this.$route.query.id;
  },

  watch: {
    myValue: "addNewValue",
    newHeader: "addNewHeader",
    form_group: "changeGroup"
  },
  methods: (_methods = {
    groupType: function groupType() {
      this.new_group = !this.new_group;
      if (this.new_group == true) {
        this.form_group = "";
      } else {
        this.form_group = "selected";
      }
    },
    getGroups: function getGroups() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-form-templates", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (res) {
        if (res.status = 200) {
          var groups = [];

          res.data.forEach(function (item) {
            groups.push(item.group);
          });

          var newSet = new Set(groups);
          _this.groups = Array.from(newSet);
        }
      }).catch();
    },
    changeGroup: function changeGroup() {
      this.$emit("changeGroup", this.form_group);
    }
  }, _defineProperty(_methods, "changeGroup", function changeGroup() {
    this.$emit("changeGroup", this.form_group);
  }), _defineProperty(_methods, "createForm", function createForm() {
    this.$emit("createForm");
  }), _defineProperty(_methods, "updateRowTitle", function updateRowTitle(sectionId, rowsId, rowId) {
    this.$emit('updateRowTitle', this.row_title, sectionId, rowsId, rowId);
    this.row_title = '';
  }), _defineProperty(_methods, "addHeader", function addHeader(sectionId, rowsId, rowId) {
    if (this.newHeader != "") {
      this.$emit("addHeader", sectionId, rowsId, rowId);
      this.newHeader = "";
    } else {
      this.$toasted.error('Input header title');
    }
  }), _defineProperty(_methods, "removeHeader", function removeHeader(sectionId, rowsId, rowId) {
    this.$emit("removeHeader", sectionId, rowsId, rowId);
  }), _defineProperty(_methods, "addItem", function addItem(sectionId, rowsId, rowId) {
    if (this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.length) {

      this.$emit("addItem", sectionId, rowsId, rowId);
      this.updateRowTitle(sectionId, rowsId, rowId);
    } else {
      this.$toasted.error('Input header title');
    }
  }), _defineProperty(_methods, "removeItem", function removeItem(sectionId, rowsId, rowId) {
    this.$emit("removeItem", sectionId, rowsId, rowId);
  }), _defineProperty(_methods, "addForm", function addForm() {
    this.$emit("addForm");
  }), _defineProperty(_methods, "addRow", function addRow(id) {
    this.$emit("addRow", id);
  }), _defineProperty(_methods, "removeRow", function removeRow(id) {
    this.$emit("removeRow", id);
  }), _defineProperty(_methods, "addValue", function addValue(sectionId, rowsId, rowId) {
    this.$emit("addValue", sectionId, rowsId, rowId);
    this.myValue = '';
  }), _defineProperty(_methods, "addNewValue", function addNewValue() {
    this.$emit("updateValue", this.myValue);
  }), _defineProperty(_methods, "addNewHeader", function addNewHeader() {
    this.$emit("updateHeader", this.newHeader);
  }), _defineProperty(_methods, "showConfig", function showConfig(id) {
    this.current = id;
  }), _defineProperty(_methods, "closeConfig", function closeConfig() {
    this.current = null;
  }), _methods)
});

/***/ }),

/***/ 1171:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sub-page" }, [
    _c("div", [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm btn-compliment text-white",
          attrs: { type: "button" },
          on: { click: _vm.createForm }
        },
        [_vm._v("Create Form")]
      )
    ]),
    _vm._v(" "),
    _vm.start
      ? _c(
          "div",
          { staticClass: "form" },
          [
            _vm.new_group
              ? _c("div", { staticClass: "form-group mb-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form_group,
                        expression: "form_group"
                      }
                    ],
                    staticClass: "form-control w-50 mb-3",
                    attrs: {
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "Enter form group"
                    },
                    domProps: { value: _vm.form_group },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.form_group = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn elevated_btn_sm btn-compliment text-white mb-3",
                      attrs: { type: "button" },
                      on: { click: _vm.groupType }
                    },
                    [_vm._v("Select group")]
                  )
                ])
              : _c("div", { staticClass: "form-group" }, [
                  _c("label", { staticClass: "mini-label" }, [
                    _vm._v("Form Group")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form_group,
                          expression: "form_group"
                        }
                      ],
                      staticClass: "form-control mb-3 w-50",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.form_group = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c(
                        "option",
                        { attrs: { disabled: "", value: "selected" } },
                        [_vm._v("Select Group")]
                      ),
                      _vm._v(" "),
                      _c("option", [_vm._v("Discovering what is within")]),
                      _vm._v(" "),
                      _c("option", [_vm._v("Business Education")]),
                      _vm._v(" "),
                      _vm._l(_vm.groups, function(item, idx) {
                        return _c("option", { key: idx }, [
                          _vm._v(_vm._s(item))
                        ])
                      }),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass:
                            "elevated_btn elevated_btn_sm btn-compliment text-white",
                          attrs: { type: "button" },
                          on: { click: _vm.groupType }
                        },
                        [_vm._v("Add new group")]
                      )
                    ],
                    2
                  )
                ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.template.form.legend,
                    expression: "template.form.legend"
                  }
                ],
                staticClass: "form-control w-50",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Enter form title"
                },
                domProps: { value: _vm.template.form.legend },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.template.form, "legend", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _vm._l(_vm.template.form.sections, function(section, index) {
              return _c(
                "section",
                { key: index, staticClass: "mb-3" },
                [
                  _c("div", { staticClass: "mb-2" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: section.section.label,
                          expression: "section.section.label"
                        }
                      ],
                      staticClass: "form-control w-25",
                      attrs: {
                        type: "text",
                        placeholder: "Enter section title",
                        required: ""
                      },
                      domProps: { value: section.section.label },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            section.section,
                            "label",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("label", { staticClass: "mini-label" }, [
                        _vm._v("Description")
                      ]),
                      _vm._v(" "),
                      _c("app-editor", {
                        staticClass: "form-control",
                        attrs: {
                          apiKey:
                            "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                          init: { plugins: "wordcount, lists, advlist" }
                        },
                        model: {
                          value: section.section.description,
                          callback: function($$v) {
                            _vm.$set(section.section, "description", $$v)
                          },
                          expression: "section.section.description"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "right mini-label mr-4",
                      on: {
                        click: function($event) {
                          return _vm.addRow(index)
                        }
                      }
                    },
                    [
                      _c(
                        "i",
                        {
                          staticClass: "fa fa-plus-circle text-main",
                          attrs: { "aria-hidden": "true" }
                        },
                        [_vm._v("Add row")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "right mini-label",
                      on: {
                        click: function($event) {
                          return _vm.removeRow(index)
                        }
                      }
                    },
                    [
                      _c(
                        "i",
                        {
                          staticClass: "fa fa-minus-circle text-main",
                          attrs: { "aria-hidden": "true" }
                        },
                        [_vm._v("Remove row")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm._l(section.section.rows, function(rows, idx) {
                    return _c(
                      "div",
                      { key: idx },
                      _vm._l(rows.row, function(row, id) {
                        return _c(
                          "div",
                          { key: id, staticClass: "mb-3 rows" },
                          [
                            _c("span", { staticClass: "text-right" }, [
                              idx !== _vm.current
                                ? _c("i", {
                                    staticClass: "fa fa-plus-circle",
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.showConfig(idx)
                                      }
                                    }
                                  })
                                : _vm._e(),
                              _vm._v(" "),
                              idx == _vm.current
                                ? _c("i", {
                                    staticClass: "fa fa-minus-circle",
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.closeConfig()
                                      }
                                    }
                                  })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            idx == _vm.current
                              ? _c("div", { staticClass: "configs" }, [
                                  _c("div", [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c(
                                        "label",
                                        { staticClass: "mini-label" },
                                        [_vm._v("Select type")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.type,
                                              expression: "row.type"
                                            }
                                          ],
                                          staticClass: "custom-select",
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.$set(
                                                row,
                                                "type",
                                                $event.target.multiple
                                                  ? $$selectedVal
                                                  : $$selectedVal[0]
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "option",
                                            {
                                              attrs: {
                                                disabled: "",
                                                value: "selected"
                                              }
                                            },
                                            [_vm._v("Select one")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "text" } },
                                            [_vm._v("Text")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "textarea" } },
                                            [_vm._v("Textarea")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "number" } },
                                            [_vm._v("Number")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "date" } },
                                            [_vm._v("Date")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "time" } },
                                            [_vm._v("Time")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "select" } },
                                            [_vm._v("Select Option")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "checkbox" } },
                                            [_vm._v("Checkbox")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "radio" } },
                                            [_vm._v("Radio")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "table" } },
                                            [_vm._v("Table")]
                                          )
                                        ]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    row.type == "table"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex p-2 border align-items-start"
                                          },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "form-group mr-3"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.newHeader,
                                                      expression: "newHeader"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder:
                                                      "type header here",
                                                    "aria-describedby": "helpId"
                                                  },
                                                  domProps: {
                                                    value: _vm.newHeader
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.newHeader =
                                                        $event.target.value
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add Table header")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove header")]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "d-flex" },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.row_title,
                                                          expression:
                                                            "row_title"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        placeholder:
                                                          "type row title here..",
                                                        "aria-describedby":
                                                          "helpId"
                                                      },
                                                      domProps: {
                                                        value: _vm.row_title
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.row_title =
                                                            $event.target.value
                                                        }
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add Table row")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove row")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    row.type == "radio" ||
                                    row.type == "checkbox" ||
                                    row.type == "select"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "form-group d-flex border p-2"
                                          },
                                          [
                                            _c("div", { staticClass: "mr-3" }, [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.myValue,
                                                    expression: "myValue"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "text",
                                                  "aria-describedby": "helpId",
                                                  placeholder:
                                                    "Enter value here"
                                                },
                                                domProps: {
                                                  value: _vm.myValue
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.myValue =
                                                      $event.target.value
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                  attrs: { type: "button" },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.addValue(
                                                        index,
                                                        idx,
                                                        id
                                                      )
                                                    }
                                                  }
                                                },
                                                [_vm._v("Add Value")]
                                              )
                                            ]),
                                            _vm._v(" "),
                                            row.type == "checkbox"
                                              ? _c(
                                                  "div",
                                                  { staticClass: "form-group" },
                                                  [
                                                    _c(
                                                      "label",
                                                      {
                                                        staticClass:
                                                          "mini-label",
                                                        attrs: { for: "" }
                                                      },
                                                      [_vm._v("Multiple")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "d-flex align-items-center"
                                                      },
                                                      [
                                                        _c(
                                                          "label",
                                                          {
                                                            staticClass:
                                                              "custom-control custom-radio mini-label mr-3"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    row.multiple,
                                                                  expression:
                                                                    "row.multiple"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "custom-control-input",
                                                              attrs: {
                                                                type: "radio",
                                                                value: "true"
                                                              },
                                                              domProps: {
                                                                checked: _vm._q(
                                                                  row.multiple,
                                                                  "true"
                                                                )
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.$set(
                                                                    row,
                                                                    "multiple",
                                                                    "true"
                                                                  )
                                                                }
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "custom-control-indicator"
                                                              },
                                                              [_vm._v("True")]
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "label",
                                                          {
                                                            staticClass:
                                                              "custom-control custom-radio mini-label"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    row.multiple,
                                                                  expression:
                                                                    "row.multiple"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "custom-control-input",
                                                              attrs: {
                                                                type: "radio",
                                                                value: "false"
                                                              },
                                                              domProps: {
                                                                checked: _vm._q(
                                                                  row.multiple,
                                                                  "false"
                                                                )
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.$set(
                                                                    row,
                                                                    "multiple",
                                                                    "false"
                                                                  )
                                                                }
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "custom-control-indicator"
                                                              },
                                                              [_vm._v("False")]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e()
                                          ]
                                        )
                                      : _vm._e()
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group mb-2" },
                                      [
                                        _c(
                                          "label",
                                          {
                                            staticClass: "mini-label",
                                            attrs: { for: "" }
                                          },
                                          [_vm._v("Description")]
                                        ),
                                        _vm._v(" "),
                                        _c("textarea", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.description,
                                              expression: "row.description"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: { rows: "3" },
                                          domProps: { value: row.description },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "description",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "form-group" }, [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "mini-label",
                                          attrs: { for: "" }
                                        },
                                        [_vm._v("Question title")]
                                      ),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.label,
                                            expression: "row.label"
                                          }
                                        ],
                                        staticClass: "form-control mb-2",
                                        attrs: {
                                          type: "text",
                                          placeholder: "Enter label / question"
                                        },
                                        domProps: { value: row.label },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              row,
                                              "label",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "form-group" }, [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "mini-label",
                                          attrs: { for: "" }
                                        },
                                        [_vm._v("Note")]
                                      ),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.note,
                                            expression: "row.note"
                                          }
                                        ],
                                        staticClass: "form-control mb-2",
                                        attrs: {
                                          type: "text",
                                          placeholder: "Enter label / question"
                                        },
                                        domProps: { value: row.note },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              row,
                                              "note",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "form-group" }, [
                                    _c(
                                      "label",
                                      {
                                        staticClass: "mini-label",
                                        attrs: { for: "" }
                                      },
                                      [_vm._v("Placeholder")]
                                    ),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: row.placeholder,
                                          expression: "row.placeholder"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "text",
                                        placeholder: "Enter placeholder"
                                      },
                                      domProps: { value: row.placeholder },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            row,
                                            "placeholder",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "form-group" }, [
                                    _c(
                                      "label",
                                      {
                                        staticClass: "mini-label",
                                        attrs: { for: "" }
                                      },
                                      [_vm._v("Required")]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass: "d-flex align-items-center"
                                      },
                                      [
                                        _c(
                                          "label",
                                          {
                                            staticClass:
                                              "custom-control custom-radio mini-label mr-3"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.required,
                                                  expression: "row.required"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: {
                                                type: "radio",
                                                value: "true"
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  row.required,
                                                  "true"
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    row,
                                                    "required",
                                                    "true"
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v("True")]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "label",
                                          {
                                            staticClass:
                                              "custom-control custom-radio mini-label"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.required,
                                                  expression: "row.required"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: {
                                                type: "radio",
                                                value: "false"
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  row.required,
                                                  "false"
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    row,
                                                    "required",
                                                    "false"
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v("False")]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("div", [
                              _c("p", [_vm._v(_vm._s(row.description))]),
                              _vm._v(" "),
                              row.type == "radio"
                                ? _c(
                                    "div",
                                    { staticClass: "form-group" },
                                    [
                                      _c("h4", [_vm._v(_vm._s(row.label))]),
                                      _vm._v(" "),
                                      _c("small", [_vm._v(_vm._s(row.note))]),
                                      _vm._v(" "),
                                      _vm._l(row.values, function(value, id) {
                                        return _c(
                                          "label",
                                          {
                                            key: id,
                                            staticClass:
                                              "custom-control custom-radio"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answer,
                                                  expression: "row.answer"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: { type: "radio" },
                                              domProps: {
                                                value: value,
                                                checked: _vm._q(
                                                  row.answer,
                                                  value
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    row,
                                                    "answer",
                                                    value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v(_vm._s(value))]
                                            )
                                          ]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                : row.type == "select"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answer,
                                            expression: "row.answer"
                                          }
                                        ],
                                        staticClass: "custom-select",
                                        on: {
                                          change: function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              row,
                                              "answer",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "option",
                                          {
                                            attrs: {
                                              disabled: "",
                                              selected: ""
                                            }
                                          },
                                          [_vm._v("Select one")]
                                        ),
                                        _vm._v(" "),
                                        _vm._l(row.values, function(value, id) {
                                          return _c("option", { key: id }, [
                                            _vm._v(_vm._s(value))
                                          ])
                                        })
                                      ],
                                      2
                                    )
                                  ])
                                : row.type == "checkbox"
                                ? _c(
                                    "div",
                                    { staticClass: "form-group" },
                                    [
                                      _c("h4", [_vm._v(_vm._s(row.label))]),
                                      _vm._v(" "),
                                      _c("small", [_vm._v(_vm._s(row.note))]),
                                      _vm._v(" "),
                                      _vm._l(row.values, function(value, id) {
                                        return _c(
                                          "label",
                                          {
                                            key: id,
                                            staticClass:
                                              "custom-control custom-checkbox"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answers,
                                                  expression: "row.answers"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: {
                                                type: "checkbox",
                                                multiple: row.multiple
                                              },
                                              domProps: {
                                                value: value,
                                                checked: Array.isArray(
                                                  row.answers
                                                )
                                                  ? _vm._i(row.answers, value) >
                                                    -1
                                                  : row.answers
                                              },
                                              on: {
                                                change: function($event) {
                                                  var $$a = row.answers,
                                                    $$el = $event.target,
                                                    $$c = $$el.checked
                                                      ? true
                                                      : false
                                                  if (Array.isArray($$a)) {
                                                    var $$v = value,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          row,
                                                          "answers",
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          row,
                                                          "answers",
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(
                                                      row,
                                                      "answers",
                                                      $$c
                                                    )
                                                  }
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v(_vm._s(value))]
                                            )
                                          ]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                : row.type == "table"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    row.table.headers.length
                                      ? _c(
                                          "table",
                                          {
                                            staticClass: "table table-bordered"
                                          },
                                          [
                                            _c("thead", [
                                              _c(
                                                "tr",
                                                [
                                                  _c("th"),
                                                  _vm._v(" "),
                                                  _vm._l(
                                                    row.table.headers,
                                                    function(column, index) {
                                                      return _c(
                                                        "th",
                                                        { key: index },
                                                        [_vm._v(_vm._s(column))]
                                                      )
                                                    }
                                                  )
                                                ],
                                                2
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "tbody",
                                              _vm._l(row.table.items, function(
                                                item,
                                                index
                                              ) {
                                                return _c(
                                                  "tr",
                                                  { key: index },
                                                  [
                                                    _c("td", [
                                                      _vm._v(
                                                        _vm._s(
                                                          row.table.row_titles[
                                                            index
                                                          ]
                                                        )
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _vm._l(
                                                      row.table.headers,
                                                      function(
                                                        column,
                                                        indexColumn
                                                      ) {
                                                        return _c(
                                                          "td",
                                                          {
                                                            key: indexColumn,
                                                            staticClass: "p-0"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    item[
                                                                      column +
                                                                        index +
                                                                        indexColumn
                                                                    ],
                                                                  expression:
                                                                    "item[column + index + indexColumn]"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "form-control border-0",
                                                              attrs: {
                                                                type: "text",
                                                                placeholder:
                                                                  "type here"
                                                              },
                                                              domProps: {
                                                                value:
                                                                  item[
                                                                    column +
                                                                      index +
                                                                      indexColumn
                                                                  ]
                                                              },
                                                              on: {
                                                                input: function(
                                                                  $event
                                                                ) {
                                                                  if (
                                                                    $event
                                                                      .target
                                                                      .composing
                                                                  ) {
                                                                    return
                                                                  }
                                                                  _vm.$set(
                                                                    item,
                                                                    column +
                                                                      index +
                                                                      indexColumn,
                                                                    $event
                                                                      .target
                                                                      .value
                                                                  )
                                                                }
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      }
                                                    )
                                                  ],
                                                  2
                                                )
                                              }),
                                              0
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  ])
                                : (row.type = "text")
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    row.type === "checkbox"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: "checkbox"
                                          },
                                          domProps: {
                                            checked: Array.isArray(row.answer)
                                              ? _vm._i(row.answer, null) > -1
                                              : row.answer
                                          },
                                          on: {
                                            change: function($event) {
                                              var $$a = row.answer,
                                                $$el = $event.target,
                                                $$c = $$el.checked
                                                  ? true
                                                  : false
                                              if (Array.isArray($$a)) {
                                                var $$v = null,
                                                  $$i = _vm._i($$a, $$v)
                                                if ($$el.checked) {
                                                  $$i < 0 &&
                                                    _vm.$set(
                                                      row,
                                                      "answer",
                                                      $$a.concat([$$v])
                                                    )
                                                } else {
                                                  $$i > -1 &&
                                                    _vm.$set(
                                                      row,
                                                      "answer",
                                                      $$a
                                                        .slice(0, $$i)
                                                        .concat(
                                                          $$a.slice($$i + 1)
                                                        )
                                                    )
                                                }
                                              } else {
                                                _vm.$set(row, "answer", $$c)
                                              }
                                            }
                                          }
                                        })
                                      : row.type === "radio"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: "radio"
                                          },
                                          domProps: {
                                            checked: _vm._q(row.answer, null)
                                          },
                                          on: {
                                            change: function($event) {
                                              return _vm.$set(
                                                row,
                                                "answer",
                                                null
                                              )
                                            }
                                          }
                                        })
                                      : _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: row.type
                                          },
                                          domProps: { value: row.answer },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                  ])
                                : row.type == "textarea"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("textarea", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: row.answer,
                                          expression: "row.answer"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        rows: "3",
                                        required: row.required
                                      },
                                      domProps: { value: row.answer },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            row,
                                            "answer",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                : _c("div")
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  })
                ],
                2
              )
            })
          ],
          2
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3161f3f5", module.exports)
  }
}

/***/ }),

/***/ 1172:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1173)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1175)
/* template */
var __vue_template__ = __webpack_require__(1176)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-367c8068"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorPreview.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-367c8068", Component.options)
  } else {
    hotAPI.reload("data-v-367c8068", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1173:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1174);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4a471c8b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-367c8068\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorPreview.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-367c8068\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorPreview.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1174:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.number[data-v-367c8068]{\n font-size: 14px;\n}\n.note[data-v-367c8068]{\n  color:red;\n}\n.form-control[data-v-367c8068]::-webkit-input-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-367c8068]::-moz-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-367c8068]::-ms-input-placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-367c8068]::placeholder {\n    /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: rgba(0, 0, 0, 0.44) !important;\n    opacity: 1; /* Firefox */\n    font-size: 12px;\n}\n.form-control[data-v-367c8068]:-ms-input-placeholder {\n    /* Internet Explorer 10-11 */\n    color: rgba(0, 0, 0, 0.44) !important;\n    font-size: 12px;\n}\n.form-control[data-v-367c8068]::-ms-input-placeholder {\n    /* Microsoft Edge */\n    color: rgba(0, 0, 0, 0.44) !important;\n    font-size: 12px;\n}\ntable[data-v-367c8068] {\n  font-size: 14px;\n}\nh4[data-v-367c8068] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\nh5[data-v-367c8068]{\n  font-size: 14px;\n  margin-bottom: 10px;\n}\np[data-v-367c8068] {\n    margin: 0 0 10px;\n}\n.mini-label[data-v-367c8068] {\n  font-size: 13px;\n}\nlabel span[data-v-367c8068] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-367c8068] {\n  display: fleX;\n}\n.custom-control-input[data-v-367c8068] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-367c8068] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-367c8068],td[data-v-367c8068]{\n  text-align: center;\n}\ntable[data-v-367c8068] {\n  font-size: 14px;\n}\nli[data-v-367c8068]{\n  font-size: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['template']

});

/***/ }),

/***/ 1176:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "p-4 bg-white main-preview" },
    [
      _c("h3", { staticClass: "text-center mb-3" }, [
        _vm._v(_vm._s(_vm.template.form.legend))
      ]),
      _vm._v(" "),
      _c("p", [_vm._v(_vm._s(_vm.template.form.description))]),
      _vm._v(" "),
      _vm._l(_vm.template.form.sections, function(section, index) {
        return _c("section", { key: index, staticClass: "mb-3" }, [
          _c("div", { staticClass: "number" }, [
            _vm._v("Section " + _vm._s(index + 1))
          ]),
          _vm._v(" "),
          _c("h4", { staticClass: "mb-2" }, [
            _vm._v(_vm._s(section.section.label))
          ]),
          _vm._v(" "),
          _c("p", {
            staticClass: "section_desc",
            domProps: { innerHTML: _vm._s(section.section.description) }
          }),
          _vm._v(" "),
          _c(
            "ol",
            _vm._l(section.section.rows, function(rows, idx) {
              return _c(
                "div",
                { key: idx },
                _vm._l(rows.row, function(row, id) {
                  return _c("div", { key: id, staticClass: "mb-3 rows" }, [
                    _c("p", [_vm._v(_vm._s(row.description))]),
                    _vm._v(" "),
                    _c("li", [
                      _c("div", [
                        row.type == "radio"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass: "custom-control custom-radio"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answer,
                                            expression: "row.answer"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: { readonly: "", type: "radio" },
                                        domProps: {
                                          value: value,
                                          checked: _vm._q(row.answer, value)
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              row,
                                              "answer",
                                              value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "select"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: row.answer,
                                      expression: "row.answer"
                                    }
                                  ],
                                  staticClass: "custom-select",
                                  attrs: { readonly: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        row,
                                        "answer",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    { attrs: { disabled: "", selected: "" } },
                                    [_vm._v("Select one")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(row.values, function(value, id) {
                                    return _c("option", { key: id }, [
                                      _vm._v(_vm._s(value))
                                    ])
                                  })
                                ],
                                2
                              )
                            ])
                          : row.type == "checkbox"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass:
                                        "custom-control custom-checkbox"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answers,
                                            expression: "row.answers"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: {
                                          readonly: "",
                                          type: "checkbox",
                                          multiple: row.multiple
                                        },
                                        domProps: {
                                          value: value,
                                          checked: Array.isArray(row.answers)
                                            ? _vm._i(row.answers, value) > -1
                                            : row.answers
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = row.answers,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = value,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(row, "answers", $$c)
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "table"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.table.headers.length
                                ? _c(
                                    "table",
                                    { staticClass: "table table-bordered" },
                                    [
                                      _c("thead", [
                                        _c(
                                          "tr",
                                          [
                                            _c("th"),
                                            _vm._v(" "),
                                            _vm._l(row.table.headers, function(
                                              column,
                                              index
                                            ) {
                                              return _c("th", { key: index }, [
                                                _vm._v(_vm._s(column))
                                              ])
                                            })
                                          ],
                                          2
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        _vm._l(row.table.items, function(
                                          item,
                                          index
                                        ) {
                                          return _c(
                                            "tr",
                                            { key: index },
                                            [
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    row.table.row_titles[index]
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _vm._l(
                                                row.table.headers,
                                                function(column, indexColumn) {
                                                  return _c(
                                                    "td",
                                                    {
                                                      key: indexColumn,
                                                      staticClass: "p-0"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              item[
                                                                column +
                                                                  index +
                                                                  indexColumn
                                                              ],
                                                            expression:
                                                              "item[column + index + indexColumn]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control border-0",
                                                        attrs: {
                                                          readonly: "",
                                                          type: "text",
                                                          placeholder:
                                                            "type here"
                                                        },
                                                        domProps: {
                                                          value:
                                                            item[
                                                              column +
                                                                index +
                                                                indexColumn
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              item,
                                                              column +
                                                                index +
                                                                indexColumn,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                }
                                              )
                                            ],
                                            2
                                          )
                                        }),
                                        0
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : (row.type = "text")
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.type === "checkbox"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "checkbox"
                                    },
                                    domProps: {
                                      checked: Array.isArray(row.answer)
                                        ? _vm._i(row.answer, null) > -1
                                        : row.answer
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = row.answer,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(row, "answer", $$c)
                                        }
                                      }
                                    }
                                  })
                                : row.type === "radio"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "radio"
                                    },
                                    domProps: {
                                      checked: _vm._q(row.answer, null)
                                    },
                                    on: {
                                      change: function($event) {
                                        return _vm.$set(row, "answer", null)
                                      }
                                    }
                                  })
                                : _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: row.type
                                    },
                                    domProps: { value: row.answer },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          row,
                                          "answer",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                            ])
                          : _c("div")
                      ])
                    ])
                  ])
                }),
                0
              )
            }),
            0
          )
        ])
      }),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "butt mt-3 text-center w-100" }, [
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm text-white btn-compliment mx-auto",
            attrs: { type: "button" },
            on: { click: _vm.close }
          },
          [_vm._v("Close")]
        )
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-367c8068", module.exports)
  }
}

/***/ }),

/***/ 1177:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main_page" }, [
    _c(
      "div",
      { staticClass: "live" },
      [
        _vm.prev
          ? _c("div", { staticClass: "preview" }, [
              _c(
                "div",
                { staticClass: "body shadow" },
                [_c("Preview", { attrs: { template: _vm.template } })],
                1
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("Live", {
          attrs: { template: _vm.template, start: _vm.start },
          on: {
            changeGroup: _vm.changeGroup,
            addForm: _vm.addForm,
            addRow: _vm.addRow,
            removeRow: _vm.removeRow,
            addValue: _vm.addValue,
            updateValue: _vm.updateValue,
            addItem: _vm.addItem,
            removeItem: _vm.removeItem,
            addHeader: _vm.addHeader,
            removeHeader: _vm.removeHeader,
            updateHeader: _vm.updateHeader,
            updateRowTitle: _vm.updateRowTitle,
            createForm: _vm.createForm
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "gui" },
      [
        _c("Gui", {
          on: {
            addSection: _vm.addSection,
            removeSection: _vm.removeSection,
            previewNow: _vm.previewNow
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3e52971d", module.exports)
  }
}

/***/ }),

/***/ 507:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1159)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1161)
/* template */
var __vue_template__ = __webpack_require__(1177)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3e52971d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorFormComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3e52971d", Component.options)
  } else {
    hotAPI.reload("data-v-3e52971d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ })

});