webpackJsonp([67],{

/***/ 1515:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1516);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("9e107c8a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-30a3c2a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticlesComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-30a3c2a3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticlesComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1516:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-30a3c2a3] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  list-style-type: none;\n  margin: 0;\n  padding: 5px 20px;\n  background: #ffffff;\n}\nul > li[data-v-30a3c2a3] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  float: left;\n  height: 10px;\n  width: auto;\n  font-weight: bold;\n  font-size: 0.8em;\n  cursor: default;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul > li[data-v-30a3c2a3]:not(:last-child)::after {\n  content: \"/\";\n  float: right;\n  font-size: 0.8em;\n  margin: 0 0.5em;\n  cursor: default;\n}\n.linked[data-v-30a3c2a3] {\n  cursor: pointer;\n  font-size: 1em;\n  font-weight: normal;\n}\n.course-Detail[data-v-30a3c2a3] {\n  background: hsl(0, 0%, 99%);\n  padding: 0px 15px;\n}\n.container[data-v-30a3c2a3] {\n  padding-left: 0;\n  padding-right: 0;\n  padding-top: 100px;\n  padding-bottom: 80px;\n}\n.article-banner[data-v-30a3c2a3] {\n  background-image: url(/images/artic.jpg);\n  background-position: center;\n  background-size: 100% 100%;\n  background-repeat: no-repeat;\n  height: 300px;\n  max-height: 300px;\n  position: relative;\n  margin-top: 50px;\n}\n.text[data-v-30a3c2a3] {\n  position: absolute;\n  top: 150px;\n  left: 50px;\n  font-size: 60px;\n  color: #ffffff;\n}\n.ftm div[data-v-30a3c2a3] {\n  padding: 5px 0;\n  font-size: 16px !important;\n  line-height: 1.2;\n  font-weight: 500;\n}\n.ftm[data-v-30a3c2a3] {\n  /* border-bottom: 1px solid rgba(0,0,0,.15)!important; */\n  margin-bottom: 25px !important;\n}\n.headers[data-v-30a3c2a3] {\n  font-size: 16px;\n  color: rgba(0, 0, 0, 0.54);\n}\n.artTil[data-v-30a3c2a3] {\n  font-size: 16px !important;\n  line-height: 24px !important;\n  font-weight: 600;\n  color: rgba(0, 0, 0, 0.84);\n  fill: rgba(0, 0, 0, 0.84);\n  padding: 0;\n}\n.seeFeatured[data-v-30a3c2a3] {\n  font-size: 12px !important;\n  line-height: 20px !important;\n  font-weight: 500;\n  color: #5b84a7;\n  margin-right: 30px;\n  text-align: right;\n}\ndiv.seeFeatured a[data-v-30a3c2a3] {\n  color: #5b84a7 !important;\n}\n.articleMem[data-v-30a3c2a3] {\n  margin: 15px 5px;\n}\n.articleiveer[data-v-30a3c2a3] {\n  width: 90%;\n}\n\n/* .featuredProd {\n        margin-right: 70px;\n    } */\n.indexCount[data-v-30a3c2a3] {\n  font-size: 20px;\n  color: #ccc;\n  margin: 5px 15px;\n}\n.articleRowImage[data-v-30a3c2a3] {\n  width: 100px;\n  height: 100px;\n  margin-left: auto;\n}\n.articleRowImage img[data-v-30a3c2a3] {\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  height: 100px;\n}\n.articleRS[data-v-30a3c2a3] {\n  padding: 20px;\n  margin-bottom: 0;\n}\n.articleRSs[data-v-30a3c2a3] {\n  max-width: 1083px;\n  width: 100%;\n  margin: 0 auto;\n}\n.articleShift[data-v-30a3c2a3] {\n  margin-left: 30px;\n  width: 60%;\n}\n.articleImagebar[data-v-30a3c2a3] {\n  width: 80px;\n  height: 80px;\n  max-width: 100%;\n  max-height: 100%;\n  background-size: contain;\n  background-position: center;\n  margin-left: auto;\n}\n.articleImagebar img[data-v-30a3c2a3] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\np img[data-v-30a3c2a3] {\n  width: unset !important;\n  height: unset;\n}\n.articlebar[data-v-30a3c2a3],\n.numDiver[data-v-30a3c2a3] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 15px;\n}\n.articleAtitlebar[data-v-30a3c2a3] {\n  line-height: 1.2;\n  font-size: 16px;\n  font-weight: 600;\n  color: rgba(0, 0, 0, 0.84) !important;\n}\n.authorDetail[data-v-30a3c2a3] {\n  margin-top: 10px;\n  color: rgba(0, 0, 0, 0.84) !important;\n  fill: rgba(0, 0, 0, 0.84) !important;\n}\n.authorDetail div[data-v-30a3c2a3] {\n  font-size: 13px;\n  font-weight: 400;\n  line-height: 20px !important;\n  text-transform: capitalize;\n}\n.articleDes[data-v-30a3c2a3] {\n  height: 18px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  color: rgba(0, 0, 0, 0.64) !important;\n  fill: rgba(0, 0, 0, 0.64) !important;\n\n  line-height: 20px !important;\n  width: 90%;\n  display: -webkit-box !important;\n  text-align: left;\n  text-transform: capitalize;\n  font-size: 16px;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  word-break: break-word;\n  margin-bottom: 10px;\n}\n.articleDesd[data-v-30a3c2a3] {\n  height: 43px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  font-size: 16px;\n  line-height: 20px !important;\n  height: 43px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  color: rgba(0, 0, 0, 0.64) !important;\n  fill: rgba(0, 0, 0, 0.64) !important;\n\n  line-height: 20px !important;\n  width: 100%;\n  display: -webkit-box !important;\n  text-align: left;\n  text-transform: capitalize;\n  font-size: 16px;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  word-break: break-word;\n  margin-bottom: 10px;\n}\n.articleImage img[data-v-30a3c2a3] {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  height: 200px;\n}\n.articleImage[data-v-30a3c2a3] {\n  height: 150px;\n}\n.articleShow[data-v-30a3c2a3] {\n  padding-top: 50px;\n}\n.articleAtitle[data-v-30a3c2a3] {\n  font-weight: 700;\n  font-size: 16px !important;\n  line-height: 24px !important;\n  text-transform: capitalize;\n  margin-bottom: 4px !important;\n  color: rgba(0, 0, 0, 0.84);\n  margin-top: 10px;\n}\n.coverImage[data-v-30a3c2a3] {\n  width: 150px;\n}\n.articlesRow[data-v-30a3c2a3] {\n  padding: 60px;\n}\n@media (max-width: 768px) {\n.article-banner[data-v-30a3c2a3] {\n    height: 230px;\n}\n.featuredProd[data-v-30a3c2a3] {\n    margin: 0;\n}\n.authorDetail[data-v-30a3c2a3] {\n    font-size: 11px;\n}\n.authorDetail div[data-v-30a3c2a3] {\n    font-size: 11px;\n}\ndiv.seeFeatured[data-v-30a3c2a3] {\n    font-size: 11px !important;\n}\n}\n@media (max-width: 425px) {\n.course-Detail[data-v-30a3c2a3] {padding: 0\n}\nul[data-v-30a3c2a3] {\n    padding: 5px;\n}\n.container[data-v-30a3c2a3] {\n    padding-top: 5px !important;\n}\n.article-banner[data-v-30a3c2a3] {\n    /* display: none; */\n    height: 150px;\n}\n.articleShift[data-v-30a3c2a3] {\n    margin-left: 0;\n}\n.articlebar[data-v-30a3c2a3] {\n    margin-bottom: 15px;\n}\n.articleDesd[data-v-30a3c2a3] {\n    width: 90%;\n}\n.articleMem[data-v-30a3c2a3] {\n    margin: 0;\n    padding: 0;\n}\n.articleiveer[data-v-30a3c2a3] {\n    width: 80%;\n    padding: 10px 2px;\n}\n.articleiveerr[data-v-30a3c2a3] {\n    width: 100%;\n    padding: 10px 2px;\n}\n.pref[data-v-30a3c2a3] {\n    width: 100%;\n}\n.authorDetail[data-v-30a3c2a3] {\n    font-size: 11px;\n}\n.authorDetail div[data-v-30a3c2a3] {\n    font-size: 11px;\n}\n.featuredProd[data-v-30a3c2a3] {\n    margin-right: 0;\n}\n.text[data-v-30a3c2a3] {\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n}\n.articleShow[data-v-30a3c2a3] {\n    margin-left: 0;\n    padding: 0;\n    letter-spacing: -0.004em;\n}\n.authorDetail.writer[data-v-30a3c2a3] {\n    font-size: 8px;\n}\n.authorDetail.date[data-v-30a3c2a3] {\n    font-size: 8px;\n}\n.headers[data-v-30a3c2a3] {\n    font-size: 16px;\n    color: rgba(0, 0, 0, 0.54);\n}\n.articleImage[data-v-30a3c2a3] {\n    height: 150px;\n    max-width: 425px;\n    background-size: contain;\n    background-position: center;\n}\n.articleImage img[data-v-30a3c2a3] {\n    width: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    height: 100%;\n}\ndiv.seeFeatured[data-v-30a3c2a3] {\n    font-size: 11px !important;\n}\n.articleRowImage[data-v-30a3c2a3] {\n    width: 80px;\n    height: 80px;\n    background: #fff;\n    background-size: contain;\n    background-position: center;\n    margin: 0;\n    padding: 0;\n    margin-top: 10px;\n}\n.articleRowImage img[data-v-30a3c2a3] {\n    display: block;\n    width: 100%;\n    height: 100%;\n    padding: 0;\n}\n.articleRS[data-v-30a3c2a3] {\n    padding: 0;\n    margin-top: 25px;\n}\n.ftm[data-v-30a3c2a3] {\n    margin-bottom: 15px !important;\n}\n.indexCount[data-v-30a3c2a3] {\n    font-size: 16px;\n    color: #ccc;\n    margin: 0px 5px 70px 0;\n\n    border-radius: 50%;\n    padding: 1px 2px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-articles-component",
  components: {
    "app-videoBanner": __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default.a
  },
  data: function data() {
    return {
      articles: [],
      authenticate: false,
      user: {},
      preferenceArticles: [],
      popularArticles: [],
      networkArticles: [],
      isActive: true,
      breadcrumbList: [],
      loaderShow: true
    };
  },
  watch: {
    $route: function $route() {
      this.updateList();
    }
  },
  mounted: function mounted() {
    var _this = this;

    this.updateList();

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user != null) {
      this.authenticate = true;
      this.user = user;
    }
    this.getTopicArticle();
    axios.get("/api/articles-page").then(function (response) {
      if (response.status === 200) {
        _this.getPopularArticles();
        _this.isActive = false;
        _this.loaderShow = false;

        response.data.data.forEach(function (item) {
          _this.$set(item.articles, "myid", item.id);
          _this.$set(item.articles, "coverImage", item.coverImage);
          _this.$set(item.articles, "subjectMatter", item.subjectMatter.name);
          _this.articles.push(item.articles);
        });
        if (_this.authenticate) {
          _this.getUserPreferenceArticle();
          _this.getArticleFromNetwork();
        }
      }
    }).catch(function (error) {
      console.log(error);
    });
  },

  methods: {
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    getArticleFromNetwork: function getArticleFromNetwork() {
      var _this2 = this;

      axios.get("/api/articles/network", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {

        if (response.status === 200) {
          response.data.data.forEach(function (item) {
            _this2.$set(item.articles, "myid", item.id);
            _this2.$set(item.articles, "coverImage", item.coverImage);
            _this2.$set(item.articles, "subjectMatter", item.subjectMatter.name);
            _this2.networkArticles.push(item.articles);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getUserPreferenceArticle: function getUserPreferenceArticle() {
      var _this3 = this;

      axios.get("/api/user/preference/articles", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            _this3.$set(item.articlesDisplay, "myid", item.id);
            _this3.$set(item.articlesDisplay, "coverImage", item.coverImage);
            _this3.$set(item.articlesDisplay, "subjectMatter", item.subjectMatter.name);
            _this3.preferenceArticles.push(item.articlesDisplay);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getPopularArticles: function getPopularArticles() {
      var _this4 = this;

      axios.get("/api/get/popular-articles").then(function (response) {
        if (response.status === 200) {

          response.data.data.forEach(function (item) {
            _this4.$set(item.product_article, "myid", item.id);
            _this4.$set(item.product_article, "coverImage", item.coverImage);
            _this4.popularArticles.push(item.product_article);
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getTopicArticle: function getTopicArticle() {
      axios.get("/api/articlesTopic").then(function (response) {}).catch(function (error) {
        console.log(error);
      });
    }
  },
  computed: {
    showFeaturedProduct: function showFeaturedProduct() {
      return this.articles.filter(function (item) {
        return item.featured.match("1");
        //  return
      });
    }
  }
});

/***/ }),

/***/ 1518:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", { staticClass: "course-Detail" }, [
          _c("div", { staticClass: "container  shadow-lg bg-white" }, [
            _c(
              "ul",
              _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
                return _c(
                  "li",
                  {
                    key: index,
                    class: { linked: !!breadcrumb.link },
                    on: {
                      click: function($event) {
                        return _vm.routeTo(index)
                      }
                    }
                  },
                  [_vm._v(_vm._s(breadcrumb.name))]
                )
              }),
              0
            ),
            _vm._v(" "),
            !_vm.loaderShow
              ? _c("div", [
                  _c("div", { staticClass: "row articleRS" }, [
                    _c("div", { staticClass: "col-md-5 col-sm-5 col-xs-12" }, [
                      _vm.articles.length > 0
                        ? _c("div", { staticClass: "mb-4" }, [
                            _c(
                              "div",
                              { staticClass: "articleImage" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: _vm.showFeaturedProduct[0].myid,
                                          name: _vm.showFeaturedProduct[0].title.replace(
                                            /[^a-z0-9]/gi,
                                            ""
                                          )
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c("img", {
                                      attrs: {
                                        src:
                                          _vm.showFeaturedProduct[0].coverImage
                                      }
                                    })
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "articleShow" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: _vm.showFeaturedProduct[0].myid,
                                          name: _vm.showFeaturedProduct[0].title.replace(
                                            /[^a-z0-9]/gi,
                                            ""
                                          )
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "articleAtitle" },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.showFeaturedProduct[0].title
                                          )
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", {
                                      staticClass: "articleDes",
                                      domProps: {
                                        innerHTML: _vm._s(
                                          _vm.showFeaturedProduct[0].description
                                        )
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "authorDetail" }, [
                                  _c("div", { staticClass: "writer" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm.showFeaturedProduct[0].writer
                                      ) +
                                        " in " +
                                        _vm._s(
                                          _vm.showFeaturedProduct[0].subjectMatter.toLowerCase()
                                        )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "date" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("moment")(
                                          _vm.showFeaturedProduct[0].created_at,
                                          "MMM D"
                                        )
                                      )
                                    )
                                  ])
                                ])
                              ],
                              1
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4 col-sm-4 col-xs-12" }, [
                      _vm.articles.length > 1
                        ? _c("div", [
                            _c("div", { staticClass: "articlebar" }, [
                              _c(
                                "div",
                                { staticClass: "articleShift" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[1].myid,
                                            name: _vm.showFeaturedProduct[1].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "articleAtitlebar" },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              _vm.showFeaturedProduct[1].title
                                            )
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "articleDes",
                                        domProps: {
                                          innerHTML: _vm._s(
                                            _vm.showFeaturedProduct[1]
                                              .description
                                          )
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "authorDetail" }, [
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm.showFeaturedProduct[1].writer
                                        ) +
                                          " in " +
                                          _vm._s(
                                            _vm.showFeaturedProduct[1].subjectMatter.toLowerCase()
                                          )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("moment")(
                                            _vm.showFeaturedProduct[1]
                                              .created_at,
                                            "MMM D"
                                          )
                                        )
                                      )
                                    ])
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "articleImagebar" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[1].myid,
                                            name: _vm.showFeaturedProduct[1].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src:
                                            _vm.showFeaturedProduct[1]
                                              .coverImage
                                        }
                                      })
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.articles.length > 2
                        ? _c("div", [
                            _c("div", { staticClass: "articlebar" }, [
                              _c(
                                "div",
                                { staticClass: "articleShift" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[2].myid,
                                            name: _vm.showFeaturedProduct[2].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "articleAtitlebar" },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              _vm.showFeaturedProduct[2].title
                                            )
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "articleDes",
                                        domProps: {
                                          innerHTML: _vm._s(
                                            _vm.showFeaturedProduct[2]
                                              .description
                                          )
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "authorDetail" }, [
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm.showFeaturedProduct[2].writer
                                        ) +
                                          " in " +
                                          _vm._s(
                                            _vm.showFeaturedProduct[2].subjectMatter.toLowerCase()
                                          )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("moment")(
                                            _vm.showFeaturedProduct[2]
                                              .created_at,
                                            "MMM D"
                                          )
                                        )
                                      )
                                    ])
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "articleImagebar" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[2].myid,
                                            name: _vm.showFeaturedProduct[2].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src:
                                            _vm.showFeaturedProduct[2]
                                              .coverImage
                                        }
                                      })
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.articles.length > 3
                        ? _c("div", [
                            _c("div", { staticClass: "articlebar" }, [
                              _c(
                                "div",
                                { staticClass: "articleShift" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[3].myid,
                                            name: _vm.showFeaturedProduct[3].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "articleAtitlebar" },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              _vm.showFeaturedProduct[3].title
                                            )
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", {
                                        staticClass: "articleDes",
                                        domProps: {
                                          innerHTML: _vm._s(
                                            _vm.showFeaturedProduct[3]
                                              .description
                                          )
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "authorDetail" }, [
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm.showFeaturedProduct[3].writer
                                        ) +
                                          " in " +
                                          _vm._s(
                                            _vm.showFeaturedProduct[3].subjectMatter.toLowerCase()
                                          )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("div", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("moment")(
                                            _vm.showFeaturedProduct[3]
                                              .created_at,
                                            "MMM D"
                                          )
                                        )
                                      )
                                    ])
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "articleImagebar" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: _vm.showFeaturedProduct[3].myid,
                                            name: _vm.showFeaturedProduct[3].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src:
                                            _vm.showFeaturedProduct[3]
                                              .coverImage
                                        }
                                      })
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 col-sm-3 col-xs-12" }, [
                      _vm.articles.length > 4
                        ? _c("div", { staticClass: "mb-4" }, [
                            _c(
                              "div",
                              { staticClass: "articleImage" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: _vm.showFeaturedProduct[4].myid,
                                          name: _vm.showFeaturedProduct[4].title.replace(
                                            /[^a-z0-9]/gi,
                                            ""
                                          )
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c("img", {
                                      attrs: {
                                        src:
                                          _vm.showFeaturedProduct[4].coverImage
                                      }
                                    })
                                  ]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "articleShow" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: _vm.showFeaturedProduct[4].myid,
                                          name: _vm.showFeaturedProduct[4].title.replace(
                                            /[^a-z0-9]/gi,
                                            ""
                                          )
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "articleAtitle" },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.showFeaturedProduct[4].title
                                          )
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", {
                                      staticClass: "articleDes",
                                      domProps: {
                                        innerHTML: _vm._s(
                                          _vm.showFeaturedProduct[4].description
                                        )
                                      }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "authorDetail" }, [
                                  _c("div", { staticClass: "writer" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm.showFeaturedProduct[4].writer
                                      ) +
                                        " in " +
                                        _vm._s(
                                          _vm.showFeaturedProduct[4].subjectMatter.toLowerCase()
                                        )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "date" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("moment")(
                                          _vm.showFeaturedProduct[4].created_at,
                                          "MMM D"
                                        )
                                      )
                                    )
                                  ])
                                ])
                              ],
                              1
                            )
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-12 right" }, [
                      _c(
                        "div",
                        { staticClass: "seeFeatured" },
                        [
                          _c(
                            "router-link",
                            {
                              attrs: {
                                to: {
                                  name: "ArticleFullPage",
                                  params: { type: "all", name: "articles" }
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n                See All Featured\n                "
                              ),
                              _c("i", {
                                staticClass: "fa fa-angle-double-right",
                                attrs: { "aria-hidden": "true" }
                              })
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row articleRSs" }, [
                    _c(
                      "div",
                      {
                        staticClass: "featuredProd col-md-7 col-sm-7 col-xs-12"
                      },
                      [
                        _vm.showFeaturedProduct.length > 0
                          ? _c(
                              "div",
                              [
                                _vm._m(1),
                                _vm._v(" "),
                                _vm._l(_vm.showFeaturedProduct, function(
                                  featuredProduct,
                                  index
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-xs-12 col-sm-12 articlebar articleMem"
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xs-10 col-sm-8 articleiveer"
                                        },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "ArticleSinglePage",
                                                  params: {
                                                    id: featuredProduct.myid,
                                                    name: featuredProduct.title.replace(
                                                      /[^a-z0-9]/gi,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "artTil" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      featuredProduct.title
                                                    )
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("div", {
                                                staticClass: "articleDesd",
                                                domProps: {
                                                  innerHTML: _vm._s(
                                                    featuredProduct.description
                                                  )
                                                }
                                              })
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "authorDetail" },
                                            [
                                              _c("div", [
                                                _vm._v(
                                                  _vm._s(
                                                    featuredProduct.writer
                                                  ) +
                                                    ", " +
                                                    _vm._s(
                                                      featuredProduct.subjectMatter.toLowerCase()
                                                    )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("div", [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      featuredProduct.created_at,
                                                      "MMM D"
                                                    )
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xs-2 col-sm-4 articleRowImage"
                                        },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src: featuredProduct.coverImage
                                            }
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "seeFeatured" },
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "ArticleFullPage",
                                            params: {
                                              type: "all",
                                              name: "articles"
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                  More\n                  "
                                        ),
                                        _c("i", {
                                          staticClass:
                                            "fa fa-angle-double-right",
                                          attrs: { "aria-hidden": "true" }
                                        })
                                      ]
                                    )
                                  ],
                                  1
                                )
                              ],
                              2
                            )
                          : _vm._e()
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-4 col-sm-4 col-xs-12" }, [
                      _vm.preferenceArticles.length > 0
                        ? _c(
                            "div",
                            [
                              _vm._m(2),
                              _vm._v(" "),
                              _vm._l(_vm.preferenceArticles, function(
                                preferenceArticle,
                                index
                              ) {
                                return _c(
                                  "div",
                                  { staticClass: "articlebar articleMem" },
                                  [
                                    _c("div", { staticClass: "row" }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-sm-2 col-xs-2 indexCount"
                                        },
                                        [_vm._v(_vm._s("0" + (index + 1)))]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "col-sm-8 col-xs-8" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "ArticleSinglePage",
                                                  params: {
                                                    id: preferenceArticle.myid,
                                                    name: preferenceArticle.title.replace(
                                                      /[^a-z0-9]/gi,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "artTil" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      preferenceArticle.title
                                                    )
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("div", {
                                                staticClass: "articleDesd pref",
                                                domProps: {
                                                  innerHTML: _vm._s(
                                                    preferenceArticle.description
                                                  )
                                                }
                                              })
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "authorDetail" },
                                            [
                                              _c("div", [
                                                _vm._v(
                                                  _vm._s(
                                                    preferenceArticle.writer
                                                  ) +
                                                    ", " +
                                                    _vm._s(
                                                      preferenceArticle.subjectMatter.toLowerCase()
                                                    )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("div", [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      preferenceArticle.created_at,
                                                      "MMM D"
                                                    )
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ])
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.popularArticles.length > 0
                        ? _c(
                            "div",
                            [
                              _vm._m(3),
                              _vm._v(" "),
                              _vm._l(_vm.popularArticles, function(
                                popularArticle,
                                index
                              ) {
                                return _c(
                                  "div",
                                  { staticClass: "articlebar articleMem" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "articleiveerr" },
                                      [
                                        _c("div", { staticClass: "numDiver" }, [
                                          _c(
                                            "div",
                                            { staticClass: "indexCount" },
                                            [_vm._v(_vm._s("0" + (index + 1)))]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            [
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "ArticleSinglePage",
                                                      params: {
                                                        id: popularArticle.myid,
                                                        name: popularArticle.title.replace(
                                                          /[^a-z0-9]/gi,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "artTil" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          popularArticle.title
                                                        )
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("div", {
                                                    staticClass: "articleDesd",
                                                    domProps: {
                                                      innerHTML: _vm._s(
                                                        popularArticle.description
                                                      )
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "authorDetail" },
                                                [
                                                  _c("div", [
                                                    _vm._v(
                                                      _vm._s(
                                                        popularArticle.writer
                                                      ) +
                                                        ", " +
                                                        _vm._s(
                                                          popularArticle.subjectMatter
                                                        )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("div", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          popularArticle.created_at,
                                                          "MMM D"
                                                        )
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        ])
                                      ]
                                    )
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.networkArticles.length > 0
                        ? _c(
                            "div",
                            [
                              _vm._m(4),
                              _vm._v(" "),
                              _vm._l(_vm.networkArticles, function(
                                networkArticle,
                                index
                              ) {
                                return _c(
                                  "div",
                                  { staticClass: "articlebar articleMem" },
                                  [
                                    _c("div", { staticClass: "articleiveer" }, [
                                      _c("div", { staticClass: "numDiver" }, [
                                        _c(
                                          "div",
                                          { staticClass: "indexCount" },
                                          [_vm._v(_vm._s("0" + (index + 1)))]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "ArticleSinglePage",
                                                    params: {
                                                      id: networkArticle.myid,
                                                      name: networkArticle.title
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "artTil" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        networkArticle.title
                                                      )
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c("div", {
                                                  staticClass: "articleDesd",
                                                  domProps: {
                                                    innerHTML: _vm._s(
                                                      networkArticle.description
                                                    )
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "authorDetail" },
                                              [
                                                _c("div", [
                                                  _vm._v(
                                                    _vm._s(
                                                      networkArticle.writer
                                                    ) +
                                                      ", " +
                                                      _vm._s(
                                                        networkArticle.subjectMatter.toLowerCase()
                                                      )
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c("div", [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm._f("moment")(
                                                        networkArticle.created_at,
                                                        "MMM D"
                                                      )
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        : _vm._e()
                    ])
                  ])
                ])
              : _vm._e()
          ])
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "ftm" }, [
      _c("div", { staticClass: "headers" }, [_vm._v("Featured For Members")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "ftm" }, [
      _c("div", { staticClass: "headers" }, [_vm._v("Based on Preference")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "ftm" }, [
      _c("div", { staticClass: "headers" }, [_vm._v("Based on Popularity")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", { staticClass: "ftm" }, [
      _c("div", { staticClass: "headers" }, [_vm._v("From Expert Network")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-30a3c2a3", module.exports)
  }
}

/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1515)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1517)
/* template */
var __vue_template__ = __webpack_require__(1518)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-30a3c2a3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userArticlesComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-30a3c2a3", Component.options)
  } else {
    hotAPI.reload("data-v-30a3c2a3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 745:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(746)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(748)
/* template */
var __vue_template__ = __webpack_require__(749)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f443e1b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryVideoBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f443e1b", Component.options)
  } else {
    hotAPI.reload("data-v-0f443e1b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(747);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cd849544", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vidBanner[data-v-0f443e1b]{\n     position: relative;\n}\nimg[data-v-0f443e1b]{\n    margin-top: 40px;\n    width: 100%;\n    /* max-height: 300px; */\n}\n.text[data-v-0f443e1b]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size: 60px;\n    color: #ffffff;\n}\n@media(max-width: 475px){\n.vidBanner[data-v-0f443e1b]{\n     display: none;\n}\n.text[data-v-0f443e1b]{\n            font-size:30px;\n            top: 110px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-video-banner-component"
});

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vidBanner" }, [
      _c("img", { attrs: { src: "/images/vid.png" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Videos")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f443e1b", module.exports)
  }
}

/***/ })

});