webpackJsonp([147],{

/***/ 1428:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1429);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("29e9eb94", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14b2e634\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./aboutUsComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14b2e634\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./aboutUsComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1429:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-14b2e634] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n    margin: 20px 0px;\n}\nul > li[data-v-14b2e634] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\nul > li[data-v-14b2e634]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-14b2e634] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.about[data-v-14b2e634] {\n       padding: 100px 150px;\n}\n.connect[data-v-14b2e634] {\n        color: tomato;\n        font-size: 20px;\n        margin-top: 40px;\n}\n.biz[data-v-14b2e634] {\n        margin-bottom: 30px;\n}\np[data-v-14b2e634] {\n        margin: 20px 0;\n}\n@media (max-width:1024px) {\n.about[data-v-14b2e634] {\n       padding:100px 60px\n}\n}\n@media (max-width:425px) {\n.about[data-v-14b2e634] {\n       padding:60px 15px\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "about-us-component",
    data: function data() {
        return {
            breadcrumbList: []
        };
    },
    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        }
    }
});

/***/ }),

/***/ 1431:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "about" }, [
    _c(
      "ul",
      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
        return _c(
          "li",
          {
            key: index,
            class: { linked: !!breadcrumb.link },
            on: {
              click: function($event) {
                return _vm.routeTo(index)
              }
            }
          },
          [_vm._v("\n           " + _vm._s(breadcrumb.name) + "\n         ")]
        )
      }),
      0
    ),
    _vm._v(" "),
    _c("h1", { staticClass: "biz" }, [_vm._v("ABOUT BIZGURUH")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "We are an independent provider of business education and strategic market insight for stakeholders of Africa’s informal sector."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        " We create and curate business education resources and strategic market insight for the African market."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Our pilot product is a digital ecosystem that provides business education, strategic market insight and collaboration for stakeholders of Nigeria’s informal sector."
      )
    ]),
    _vm._v(" "),
    _c("p", [_vm._v("Our platform will feature:")]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        " We will continue to develop and curate data, market intelligence and foster collaboration within the African market; while continuously enhancing our distribution channels to reach our audience wherever they are."
      )
    ]),
    _vm._v(" "),
    _c("p", { staticClass: "connect" }, [
      _vm._v("Connect. Learn. Be Empowered")
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("b", [_vm._v("Business education and market insight portal:")]),
      _vm._v(
        " Here, proprietary and curated business education and market research resources retail; books, journal articles, white papers, online courses in text, video and audio format."
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("b", [_vm._v("A business development chatbot- BizGuruh:")]),
      _vm._v(
        " Trained to be interactive and hold human-like conversations. BizGuruh creates and curates answers to questions on key subject matter areas of business development, particularly within the African context.\n        It will be available on our web portal and all major social media platforms such as: Facebook, Twitter, Telegram, Skype etc."
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("b", [_vm._v("An interactive forum;")]),
      _vm._v(
        " categorized by sectors and industries where various industry stakeholders groups can connect, learn and collaborate."
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-14b2e634", module.exports)
  }
}

/***/ }),

/***/ 561:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1428)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1430)
/* template */
var __vue_template__ = __webpack_require__(1431)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-14b2e634"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/aboutUsComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-14b2e634", Component.options)
  } else {
    hotAPI.reload("data-v-14b2e634", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});