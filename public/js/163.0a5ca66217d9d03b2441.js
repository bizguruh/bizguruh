webpackJsonp([163],{

/***/ 1296:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1297);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("460cd7d1", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1af599bc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubCategoryBrandComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1af599bc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubCategoryBrandComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1297:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-1af599bc] {\n    height: 100vh;\n}\n.container[data-v-1af599bc] {\n    width: 100%;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-sub-category-brand-component",
    data: function data() {
        return {
            brandId: this.$route.params.id,
            subCategoryBrand: [],
            fade: true,
            modal: true,
            token: '',
            brand: {
                sub_brand_category_id: this.$route.params.id,
                category: '',
                subcategory: '',
                name: '',
                description: '',
                adminCategory: ''
            },
            toggleEdit: true,
            CategoryText: 'Create SubCategory'
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        console.log(admin);
        this.token = admin.access_token;
        if (admin != null) {
            axios.get('/api/admin/getsubcatbrandbyid/' + this.brandId, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response);
                    _this.subCategoryBrand = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        createNewSubCategorybrand: function createNewSubCategorybrand() {
            var _this2 = this;

            if (this.toggleEdit) {
                axios.post('/api/subcategorybrand', JSON.parse(JSON.stringify(this.brand)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 201) {
                        _this2.$toasted.success('Subcategory successfully created');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    for (var key in error.response.data.errors) {
                        _this2.$toasted.error(error.response.data.errors[key][0]);
                    }
                });
            } else {
                console.log(this.brand);
                axios.put('/api/subcategorybrand/' + this.brand.id, JSON.parse(JSON.stringify(this.brand)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this2.$toasted.success('Subcategory successfully updated');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        showModal: function showModal(subcategory) {
            var _this3 = this;

            this.fade = false;
            console.log(subcategory);
            if (typeof subcategory === 'undefined') {
                this.CategoryText = 'Create SubCategory';
                this.toggleEdit = true;
                this.brand.name = this.brand.description = '';
                axios.get('/api/admin/getsubcategorybrand/' + this.brandId, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        response.data.data.forEach(function (item) {
                            _this3.brand.category = item.category;
                            _this3.brand.subcategory = item.subCategory;
                        });
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                // console.log(subcategory);
                this.CategoryText = 'Update Category';
                this.toggleEdit = false;
                axios.get('/api/admin/getsubcategorybrand/' + this.brandId, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        response.data.data.forEach(function (item) {
                            _this3.brand.category = item.category;
                            _this3.brand.subcategory = item.subCategory;
                        });
                        axios.get('api/get/subcatbrand/' + subcategory + '/edit', { headers: { "Authorization": 'Bearer ' + _this3.token } }).then(function (response) {
                            if (response.status === 200) {
                                console.log(response);
                                _this3.brand.id = response.data.id;
                                _this3.brand.name = response.data.name;
                                _this3.brand.description = response.data.description;
                                _this3.brand.adminCategory = response.data.adminCategory;
                            }
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        deleteSubCategoryBrand: function deleteSubCategoryBrand(subcatbrand, catbrand) {
            var _this4 = this;

            console.log(subcatbrand);
            console.log(catbrand);
            this.subCategoryBrand.splice(catbrand, 1);
            axios.delete('api/subcategorybrand/' + subcatbrand, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.$toasted.success("Deleted Successfully");
                } else {
                    _this4.$toasted.error("Unable to delete item");
                }
            }).catch(function (error) {
                for (var key in error.response.data.errors) {
                    _this4.$toasted.error(error.response.data.errors[key][0]);
                }
            });
        }
    }
});

/***/ }),

/***/ 1299:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c(
      "button",
      {
        staticClass: "btn btn-primary",
        attrs: {
          type: "button",
          "data-toggle": "modal",
          "data-target": "#myModal"
        },
        on: {
          click: function($event) {
            return _vm.showModal()
          }
        }
      },
      [_vm._v("\n        Create Sub Category\n    ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        class: { modal: _vm.modal, fade: _vm.fade },
        attrs: {
          id: "myModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "myModalLabel"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "category" } }, [
                    _vm._v("Category:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.brand.category,
                        expression: "brand.category"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "category", disabled: "" },
                    domProps: { value: _vm.brand.category },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.brand, "category", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "subcategory" } }, [
                    _vm._v("SubCategory:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.brand.subcategory,
                        expression: "brand.subcategory"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "subcategory", disabled: "" },
                    domProps: { value: _vm.brand.subcategory },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.brand, "subcategory", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "subcategory" } }, [
                    _vm._v("SubCategoryBrand:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.brand.name,
                        expression: "brand.name"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "text", id: "subcategorybrand" },
                    domProps: { value: _vm.brand.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.brand, "name", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "categorydescription" } }, [
                    _vm._v("Description:")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.brand.description,
                        expression: "brand.description"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { id: "categorydescription" },
                    domProps: { value: _vm.brand.description },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.brand, "description", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "adminCategory" } }, [
                    _vm._v("Admins")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.brand.adminCategory,
                          expression: "brand.adminCategory"
                        }
                      ],
                      attrs: { id: "adminCategory" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.brand,
                            "adminCategory",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "AO" } }, [
                        _vm._v("Admin only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "UO" } }, [
                        _vm._v("User only")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "AU" } }, [
                        _vm._v("Admin and User")
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "modal-footer" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-default",
                      attrs: { type: "button", "data-dismiss": "modal" }
                    },
                    [_vm._v("Close")]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.createNewSubCategorybrand()
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.CategoryText))]
                  )
                ])
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("table", { staticClass: "table" }, [
        _vm._m(1),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.subCategoryBrand, function(subCategoryBrands, index) {
            return _c("tr", [
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              _c("td", [
                _vm._v(
                  "\n                    " +
                    _vm._s(subCategoryBrands.name) +
                    "\n                "
                )
              ]),
              _vm._v(" "),
              _c("td", [_vm._v("  " + _vm._s(subCategoryBrands.description))]),
              _vm._v(" "),
              _c("td", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-small",
                    attrs: {
                      "data-toggle": "modal",
                      "data-target": "#myModal"
                    },
                    on: {
                      click: function($event) {
                        return _vm.showModal(subCategoryBrands.id)
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-pencil" })]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-danger btn-small",
                    on: {
                      click: function($event) {
                        return _vm.deleteSubCategoryBrand(
                          subCategoryBrands.id,
                          index
                        )
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-trash" })]
                )
              ])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c("h4", { staticClass: "modal-title", attrs: { id: "myModalLabel" } }, [
        _vm._v("Create SubCategoryBrand")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("SN")]),
        _vm._v(" "),
        _c("th", [_vm._v("SubCategoryBrand")]),
        _vm._v(" "),
        _c("th", [_vm._v("Description")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1af599bc", module.exports)
  }
}

/***/ }),

/***/ 530:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1296)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1298)
/* template */
var __vue_template__ = __webpack_require__(1299)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1af599bc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminSubCategoryBrandComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1af599bc", Component.options)
  } else {
    hotAPI.reload("data-v-1af599bc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});