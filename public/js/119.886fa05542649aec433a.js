webpackJsonp([119],{

/***/ 1593:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1594);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("e3918a62", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b7e92876\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticleFullComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b7e92876\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticleFullComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1594:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.row[data-v-b7e92876]{\n    padding: 0;\n}\n.rightR[data-v-b7e92876] {\n        /* max-width: 328px;\n        WIDTH: 100%; */\n}\n.leftR[data-v-b7e92876] {\n        /* max-width: 680px; */\n        font-weight: 400;\n        /* width: 100%;\n        box-sizing: border-box; */\n}\n/*\n    .arl {\n        max-width: 1080px;\n        padding-right: 24px;\n        padding-left: 24px;\n        display: flex;\n        margin-left: auto;\n        margin-right: auto;\n        width: 100%;\n\n    } */\n.articlebar[data-v-b7e92876] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n}\n.articleRowImage[data-v-b7e92876]{\n        width: 100px;\n        height: 100px;\n}\n.articleRowImage img[data-v-b7e92876]{\n        -o-object-fit: cover;\n           object-fit: cover;\n        max-height: 100%;\n        max-width: 100%;\n}\n.articleMem[data-v-b7e92876] {\n        margin: 62px 4px;\n}\n.articleiveer[data-v-b7e92876] {\n        width: 100%;\n}\n.authorDetail[data-v-b7e92876] {\n        margin-top:10px;\n}\n.authorDetail div[data-v-b7e92876] {\n        font-size: 13px;\n        font-weight: 400;\n        line-height: 20px!important;\n}\n.artTil[data-v-b7e92876] {\n        font-size: 19px!important;\n        line-height: 28px!important;\n        font-weight: 600;\n        color:#a3c2dc;\n}\n.articleDesd[data-v-b7e92876] {\n        height: 18px;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        color: rgba(0,0,0,.44)!important;\n        font-size: 14px;\n        line-height: 20px!important;\n}\n@media(max-width:425px){\n.articleMem[data-v-b7e92876] {\n        margin: 20px 4px;\n}\n.artTil[data-v-b7e92876]{\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1595:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-article-full-component",
    data: function data() {
        return {
            type: this.$route.params.type,
            name: this.$route.params.name,
            items: [],
            popularArticles: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        var data = {
            type: this.type,
            name: this.name
        };
        axios.post('/api/getAllArticle', JSON.parse(JSON.stringify(data))).then(function (response) {
            if (response.status === 200) {
                response.data.data.forEach(function (item) {
                    _this.$set(item.articles, 'myid', item.id);
                    _this.$set(item.articles, 'coverImage', item.coverImage);
                    _this.$set(item.articles, 'subjectMatter', item.subjectMatter.name);
                    _this.items.push(item.articles);
                });
                _this.getPopularArticles();
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        getPopularArticles: function getPopularArticles() {
            var _this2 = this;

            axios.get('/api/get/popular-articles').then(function (response) {
                if (response.status === 200) {

                    response.data.data.forEach(function (item) {
                        _this2.$set(item.product_article, 'myid', item.id);
                        _this2.$set(item.product_article, 'coverImage', item.coverImage);
                        /*
                                                   this.$set(item.product_article, 'subjectMatter', item.subjectMatter.name);
                        */
                        _this2.popularArticles.push(item.product_article);
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1596:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row" }, [
      _vm.items.length > 0
        ? _c(
            "div",
            { staticClass: "col-lg-8 col-md-8 col-sm-8 col-xs-12 leftR" },
            _vm._l(_vm.items, function(item, index) {
              return _c("div", { staticClass: "articlebar articleMem " }, [
                _c(
                  "div",
                  {
                    staticClass:
                      "articleiveer col-lg-9 col-md-9 col-sm-9 col-xs-9"
                  },
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "ArticleSinglePage",
                            params: { id: item.myid, name: item.title }
                          }
                        }
                      },
                      [
                        _c("div", { staticClass: "artTil" }, [
                          _vm._v(_vm._s(item.title))
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", {
                      staticClass: "articleDesd",
                      domProps: { innerHTML: _vm._s(item.description) }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "authorDetail" }, [
                      _c("div", [
                        _vm._v(
                          _vm._s(item.writer) +
                            ", " +
                            _vm._s(item.subjectMatter)
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _vm._v(
                          _vm._s(_vm._f("moment")(item.created_at, "MMM D"))
                        )
                      ])
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass:
                      "articleRowImage col-lg-3 col-md-3 col-sm-3 col-xs-3"
                  },
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "ArticleSinglePage",
                            params: { id: item.myid, name: item.title }
                          }
                        }
                      },
                      [_c("img", { attrs: { src: item.coverImage } })]
                    )
                  ],
                  1
                )
              ])
            }),
            0
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-lg-4 col-md-4 col-sm-4 col-xs-12  rightR " },
        [
          _vm.popularArticles.length > 0
            ? _c(
                "div",
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _vm._l(_vm.popularArticles, function(popularArticle, index) {
                    return _c(
                      "div",
                      {
                        staticClass:
                          "col-lg-12 col-md-12 col-sm-12 col-xs-12 articlebar articleMem"
                      },
                      [
                        _c("div", { staticClass: "articleiveer" }, [
                          _c("div", { staticClass: "numDiver" }, [
                            _c("div", { staticClass: "indexCount" }, [
                              _vm._v(_vm._s("0" + (index + 1)))
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: popularArticle.myid,
                                          name: popularArticle.title
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c("div", { staticClass: "artTil" }, [
                                      _vm._v(_vm._s(popularArticle.title))
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "articleDesd",
                                  domProps: {
                                    innerHTML: _vm._s(
                                      popularArticle.description
                                    )
                                  }
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "authorDetail" }, [
                                  _c("div", [
                                    _vm._v(
                                      _vm._s(popularArticle.writer) +
                                        ", " +
                                        _vm._s(popularArticle.subjectMatter)
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("moment")(
                                          popularArticle.created_at,
                                          "MMM D"
                                        )
                                      )
                                    )
                                  ])
                                ])
                              ],
                              1
                            )
                          ])
                        ])
                      ]
                    )
                  })
                ],
                2
              )
            : _vm._e()
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "header",
      { staticClass: "ftm col-lg-12 col-md-12 col-sm-12 col-xs-12" },
      [_c("div", [_vm._v("Based on Popularity")])]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b7e92876", module.exports)
  }
}

/***/ }),

/***/ 596:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1593)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1595)
/* template */
var __vue_template__ = __webpack_require__(1596)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b7e92876"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userArticleFullComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b7e92876", Component.options)
  } else {
    hotAPI.reload("data-v-b7e92876", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});