webpackJsonp([88],{

/***/ 1147:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1148);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("637a35cc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a0410354\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewDraftProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a0410354\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewDraftProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1148:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.rowProduct[data-v-a0410354] {\n    margin-bottom: 50px;\n}\nth[data-v-a0410354] {\n    background-color: #f3f3f3;\n    padding: 40px 10px !important;\n    color: #000000;\n}\ntd[data-v-a0410354] {\n    color: #a4a4a4 !important;\n    padding: 30px 10px !important;\n}\n.dive[data-v-a0410354] {\n    text-align: right;\n}\n.btn-create[data-v-a0410354] {\n    background: #333333;\n}\n.btn-create a[data-v-a0410354] {\n    color: #ffffff;\n}\n.linktoproduct[data-v-a0410354] {\n    cursor: pointer;\n}\n.loader-display[data-v-a0410354] {\n    z-index: 999;\n    position: absolute;\n    left: 32vw;\n    top: 27vh;\n}\n.main-page[data-v-a0410354] {\n    position: relative;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            title: '',
            overallCheck: false,
            checkIn: false,
            loaderShow: true,
            products: [],
            product: {},

            isActive: true,
            initvalue: '',
            loader: '/images/loader.gif',
            token: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var vendor = JSON.parse(localStorage.getItem('authVendor'));
        if (vendor != null) {
            var vendorid = vendor.id;
            this.token = vendor.access_token;
            axios.get('/api/get/all-draft-product', { headers: { "Authorization": 'Bearer ' + vendor.access_token } }).then(function (response) {
                if (response.data.data.length > 0) {
                    var emptyArray = [];
                    response.data.data.forEach(function (item) {
                        switch (item.prodType) {
                            case 'Books':
                                _this.$set(item.books, 'verify', item.verify);
                                _this.$set(item.books, 'uniId', item.id);
                                _this.$set(item.books, 'created_at', item.myDate);
                                emptyArray.push(item.books);
                                break;
                            case 'White Paper':
                                _this.$set(item.whitePaper, 'verify', item.verify);
                                _this.$set(item.whitePaper, 'uniId', item.id);
                                _this.$set(item.whitePaper, 'created_at', item.myDate);
                                emptyArray.push(item.whitePaper);
                                break;
                            case 'Market Reports':
                                _this.$set(item.marketReport, 'verify', item.verify);
                                _this.$set(item.marketReport, 'uniId', item.id);
                                _this.$set(item.marketReport, 'created_at', item.myDate);
                                emptyArray.push(item.marketReport);
                                break;
                            case 'Market Research':
                                _this.$set(item.marketResearch, 'verify', item.verify);
                                _this.$set(item.marketResearch, 'uniId', item.id);
                                _this.$set(item.marketResearch, 'created_at', item.myDate);
                                emptyArray.push(item.marketResearch);
                                break;
                            case 'Webinar':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Podcast':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Videos':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Journals':
                                _this.$set(item.journal, 'verify', item.verify);
                                _this.$set(item.journal, 'uniId', item.id);
                                _this.$set(item.journal, 'created_at', item.myDate);
                                emptyArray.push(item.journal);
                                break;
                            case 'Courses':
                                _this.$set(item.courses, 'verify', item.verify);
                                _this.$set(item.courses, 'uniId', item.id);
                                _this.$set(item.courses, 'created_at', item.myDate);
                                emptyArray.push(item.courses);
                                break;
                            default:
                                return false;
                        }
                    });
                    _this.products = emptyArray;

                    _this.isActive = false;
                    _this.loaderShow = false;
                } else {
                    _this.products = [];

                    _this.isActive = false;
                    _this.loaderShow = false;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        overallCheckBox: function overallCheckBox() {
            var _this2 = this;

            //  this.checkIn = !this.checkIn;
            this.products.forEach(function (item) {
                if (!('itemClick' in item)) {
                    _this2.$set(item, 'itemClick', true);
                } else if (_this2.overallCheck) {
                    _this2.$set(item, 'itemClick', true);
                } else {
                    _this2.$set(item, 'itemClick', false);
                }
            });
        },
        deleteItem: function deleteItem() {
            var _this3 = this;

            var deletedArray = [];
            this.products.forEach(function (item) {
                if (item.itemClick) {

                    deletedArray.push(item.uniId);
                }
            });
            if (deletedArray.length === 0) {
                this.$toasted.error('You must select at least one product to delete');
            } else {

                axios.post('api/delete/many-products', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {

                        for (var i = 0; i < deletedArray.length; i++) {

                            _this3.products.splice(i, 1);
                        }
                        _this3.$toasted.success('All selected items deleted');
                    } else {
                        _this3.$toasted.error('Unable to delete');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }

});

/***/ }),

/***/ 1150:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main-page" },
    [
      _c("vue-loading", {
        attrs: {
          active: _vm.isActive,
          spinner: "bar-fade-scale",
          color: "#FF6700"
        }
      }),
      _vm._v(" "),
      !_vm.loaderShow
        ? _c("div", { staticClass: "grids widget-shadow" }, [
            _c("div", { staticClass: "row rowProduct" }, [
              _c("div", { staticClass: "col-md-12 dive" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-danger",
                    on: { click: _vm.deleteItem }
                  },
                  [_vm._v("\n                    Delete\n                ")]
                )
              ])
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _c("thead", [
                _c("tr", [
                  _c("th", [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.overallCheck,
                          expression: "overallCheck"
                        }
                      ],
                      attrs: { type: "checkbox" },
                      domProps: {
                        checked: Array.isArray(_vm.overallCheck)
                          ? _vm._i(_vm.overallCheck, null) > -1
                          : _vm.overallCheck
                      },
                      on: {
                        change: [
                          function($event) {
                            var $$a = _vm.overallCheck,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = null,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  (_vm.overallCheck = $$a.concat([$$v]))
                              } else {
                                $$i > -1 &&
                                  (_vm.overallCheck = $$a
                                    .slice(0, $$i)
                                    .concat($$a.slice($$i + 1)))
                              }
                            } else {
                              _vm.overallCheck = $$c
                            }
                          },
                          _vm.overallCheckBox
                        ]
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Identification")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Details")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Status")]),
                  _vm._v(" "),
                  _c("th", [_vm._v("Created On")]),
                  _vm._v(" "),
                  _c("th")
                ])
              ]),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.products, function(product, index) {
                  return _vm.products.length > 0
                    ? _c("tr", [
                        _c("td", [
                          _c("input", {
                            attrs: { type: "checkbox" },
                            domProps: { checked: product.itemClick },
                            on: {
                              change: function($event) {
                                product.itemClick = !product.itemClick
                                _vm.overallCheck = false
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("td", [_vm._v(_vm._s(index + 1))]),
                        _vm._v(" "),
                        product.articleTitle
                          ? _c("td", [_vm._v(_vm._s(product.articleTitle))])
                          : _c("td", [_vm._v(_vm._s(product.title))]),
                        _vm._v(" "),
                        product.verify === 1
                          ? _c("td", [_vm._v("Active")])
                          : _c("td", [_vm._v("Pending")]),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(
                            _vm._s(
                              _vm._f("moment")(product.created_at.date, "L")
                            )
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "td",
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "linktoproduct",
                                attrs: {
                                  to: {
                                    name: "showVendorProduct",
                                    params: { id: product.uniId }
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fa fa-arrow-right" })]
                            )
                          ],
                          1
                        )
                      ])
                    : _c("tr", [_vm._v("No Product")])
                }),
                0
              )
            ])
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a0410354", module.exports)
  }
}

/***/ }),

/***/ 504:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1147)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1149)
/* template */
var __vue_template__ = __webpack_require__(1150)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a0410354"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/viewDraftProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a0410354", Component.options)
  } else {
    hotAPI.reload("data-v-a0410354", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});