webpackJsonp([17,77],{

/***/ 1055:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1056);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("50330da2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4019f2d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./pricing.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4019f2d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./pricing.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1056:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-content[data-v-4019f2d2] {\n  height: 100vh;\n  width: 100%;\n  padding-top: 75px;\n  padding-bottom: 100px;\n  background:#f7f8fa;\n}\nh3.form-control[data-v-4019f2d2] {\n  font-size: 24px;\n}\nli[data-v-4019f2d2]{\n  padding:7px;\n}\n.duration[data-v-4019f2d2]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  width:400px;\n  margin:24px auto;\n  -webkit-box-align:center;\n      -ms-flex-align:center;\n          align-items:center;\n}\n.annual[data-v-4019f2d2]{\n  padding:8px 20px;\n  background:hsl(207,46%,20%);\n  color:white;\n  margin-left:15px;\n  border-radius:5px;\n}\n.annual-faded[data-v-4019f2d2]{\n      background: rgb(128, 128, 128,.3);\n    color: rgb(255, 255, 255,.3);\n}\n.pricing[data-v-4019f2d2] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  width: 80%;\n  margin: 0 auto;\n}\n.price_box[data-v-4019f2d2] {\n  height: auto;\n  width: 31%;\n  background: #f7f8fa;\n  border-radius: 4px;\n  position: relative;\n  text-align: center;\n}\nul[data-v-4019f2d2],\nol[data-v-4019f2d2],\nli[data-v-4019f2d2] {\n  list-style: square;\n  text-align: left;\n}\nul[data-v-4019f2d2] {\n  padding: 15px 35px;\n}\n.price_name[data-v-4019f2d2] {\n  padding: 15px;\n  text-align: center;\n  background: #0f1666;\n  color: white;\n  position: relative;\n  overflow: hidden;\n  font-weight: bold;\n}\n.price_name[data-v-4019f2d2]::after {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  bottom: 0;\n  left: 53%;\n}\n.price_name[data-v-4019f2d2]::before {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  bottom: 0;\n  left: -53%;\n}\n.price[data-v-4019f2d2] {\n  font-weight: bold;\n  font-size:24px;\n}\n.price small[data-v-4019f2d2]{\n  font-size:14px;\n  color:rgba(0, 0, 0, 0.64);\n}\n.oPlan[data-v-4019f2d2] {\n  font-size: 36px;\n  color: white;\n  text-align: center;\n}\n.oPrice[data-v-4019f2d2] {\n  font-size: 36px;\n  text-align: center;\n  color: white;\n}\n.price-overlay[data-v-4019f2d2] {\n  background: rgba(255, 255, 255, 0.7);\n  position: absolute;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n  top:0;\n  bottom: 0;\n}\n.form[data-v-4019f2d2] {\n  width: 30%;\n  height: auto;\n  padding: 15px;\n  position: relative;\n}\n.closePrice[data-v-4019f2d2] {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\n@media (max-width: 768px) {\n.main-content[data-v-4019f2d2]{\n    height:auto;\n}\n.pricing[data-v-4019f2d2] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.price_box[data-v-4019f2d2] {\n    width: 70%;\n    margin-bottom: 40px;\n}\n.form[data-v-4019f2d2]{\n    width: 70%;\n}\n}\n@media (max-width: 425px) {\n.price_box[data-v-4019f2d2] {\n    width: 100%;\n}\n.annual[data-v-4019f2d2]{\n    font-size:12px;\n}\n.form[data-v-4019f2d2]{\n    width: 80%;\n}\n.duration[data-v-4019f2d2]{\n    width:auto;\n    font-size:14px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1057:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__toggle__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["accountSub"],
  data: function data() {
    var _ref;

    return _ref = {
      authenticate: false,
      showOverlay: false,
      plan: "",
      price: "",
      duration: "",
      level: 0
    }, _defineProperty(_ref, "authenticate", false), _defineProperty(_ref, "time", false), _defineProperty(_ref, "message", false), _ref;
  },

  components: {
    Toggle: __WEBPACK_IMPORTED_MODULE_0__toggle___default.a
  },
  mounted: function mounted() {
    this.checkMessage();
  },

  watch: {
    duration: "selectPlan",
    $route: 'checkMessage'
  },

  methods: {
    handleToggle: function handleToggle(value) {
      this.time = value;

      if (!value) {
        this.duration = "monthly";
      } else {
        this.duration = "yearly";
      }
    },
    subscribe: function subscribe(type, duration) {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      localStorage.removeItem("type");

      if (user !== null) {
        this.authenticate = true;
      }
      if (this.authenticate) {
        var data = {
          type: type,
          duration: duration,
          level: this.level,
          subType: "accounting"
        };

        axios.post("/api/usersubscription", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + user.access_token }
        }).then(function (response) {
          if (response.status === 200) {
            localStorage.setItem("type", "accounting");
            window.location.href = response.data;
          }
        }).catch(function (error) {
          localStorage.removeItem("type");
          if (error.response.data.message === "Unauthenticated.") {
            _this.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: "user-subscription" }
            });
          }
        });
      } else {
        localStorage.removeItem("type");
        this.$router.replace({
          name: "auth",
          params: { name: "login" },
          query: { redirect: "user-subscription" }
        });
      }
    },
    selectPackage: function selectPackage(value) {
      switch (value) {
        case "starter":
          this.plan = "starter";
          this.level = 1;
          this.selectPlan();
          this.handleOverlay();

          break;
        case "essential":
          this.plan = "essential";
          this.level = 2;
          this.selectPlan();
          this.handleOverlay();
          break;
        case "professional":
          this.plan = "professional";
          this.level = 3;
          this.selectPlan();
          this.handleOverlay();
          break;

        default:
          break;
      }
    },
    selectPlan: function selectPlan() {
      if (this.plan == "starter" && this.duration == "monthly") {
        this.price = 3000;
      } else if (this.plan == "starter" && this.duration == "yearly") {
        this.price = 30000;
      }

      if (this.plan == "essential" && this.duration == "monthly") {
        this.price = 5000;
      } else if (this.plan == "essential" && this.duration == "yearly") {
        this.price = 50000;
      }

      if (this.plan == "professional" && this.duration == "monthly") {
        this.price = 7000;
      } else if (this.plan == "professional" && this.duration == "yearly") {
        this.price = 70000;
      }
    },
    handleOverlay: function handleOverlay() {
      this.showOverlay = !this.showOverlay;
      if (!this.showOverlay) {
        this.price = 0;
      }
      if (this.message) {
        this.message = false;
      }
    },
    checkMessage: function checkMessage() {
      if (this.$route.query.session == "pricing") {
        this.showOverlay = true;
        this.message = true;
      }
    }
  }
});

/***/ }),

/***/ 1058:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-content" }, [
    _c("div", { staticClass: "desc_header mb-5" }, [_vm._v("Pricing Plans")]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "duration" },
      [
        _c("span", [_vm._v("Monthly")]),
        _vm._v(" "),
        _c("Toggle", {
          staticClass: "px-2",
          on: { handleToggle: _vm.handleToggle }
        }),
        _vm._v(" "),
        _c("span", [
          _vm._v(" Annual  "),
          _c(
            "span",
            { staticClass: "annual", class: { "annual-faded": !_vm.time } },
            [_vm._v(" +2 Months Free")]
          )
        ])
      ],
      1
    ),
    _vm._v(" "),
    _vm.showOverlay || _vm.message
      ? _c(
          "div",
          {
            staticClass:
              "price-overlay d-flex justify-content-center align-items-center"
          },
          [
            _c("div", { staticClass: "p-3 btn-compliment form" }, [
              _c("i", {
                staticClass: "fas fa-times-circle closePrice text-white p-2",
                attrs: { "aria-hidden": "true" },
                on: { click: _vm.handleOverlay }
              }),
              _vm._v(" "),
              !_vm.message
                ? _c(
                    "form",
                    {
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                        }
                      }
                    },
                    [
                      _c("legend", [_vm._v("Select Duration")]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c(
                          "label",
                          {
                            staticClass: "toCaps oPlan mb-4",
                            attrs: { for: "" }
                          },
                          [_vm._v(_vm._s(_vm.plan) + " plan")]
                        ),
                        _vm._v(" "),
                        _c("p", { staticClass: "text-muted  text-center" }, [
                          _vm._v("\n              Duration :\n              "),
                          _c("strong", { staticClass: "toCaps text-white" }, [
                            _vm._v(_vm._s(_vm.duration))
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "oPrice mb-4" }, [
                        _vm._v("₦" + _vm._s(_vm.price) + ".00")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "my-3" }, [
                        _c(
                          "button",
                          {
                            staticClass:
                              "elevated_btn elevated_btn_sm mx-auto text-main",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.subscribe(_vm.plan, _vm.duration)
                              }
                            }
                          },
                          [_vm._v("Pay")]
                        )
                      ])
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.message
                ? _c(
                    "p",
                    {
                      staticClass: "form-control text-center",
                      on: { click: _vm.handleOverlay }
                    },
                    [_vm._v("Choose a plan to begin")]
                  )
                : _vm._e()
            ])
          ]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "pricing" }, [
      _c("div", { staticClass: "price_box shadow" }, [
        _c("div", { staticClass: "price_name" }, [_vm._v("STARTER KIT")]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "price my-3" }, [
          !_vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v(" ₦3,000 "),
                _c("small", [_vm._v("/month")])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("₦2,500"),
                _c("small", [_vm._v("/month billed annually")])
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center my-3" }, [
          _vm.accountSub == 1
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Subscribed")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.accountSub < 1
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.selectPackage("starter")
                    }
                  }
                },
                [_vm._v("Select")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.accountSub > 1
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Unavailable")]
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "price_box shadow" }, [
        _c("div", { staticClass: "price_name" }, [_vm._v("ESSENTIAL")]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "price my-3" }, [
          !_vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v(" ₦5,000 "),
                _c("small", [_vm._v("/month")])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("₦4,167 "),
                _c("small", [_vm._v("/month billed annually")])
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center my-3" }, [
          _vm.accountSub < 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.selectPackage("essential")
                    }
                  }
                },
                [_vm._v("Select")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.accountSub == 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Subscribed")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.accountSub > 2
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Unavailable")]
              )
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "price_box shadow" }, [
        _c("div", { staticClass: "price_name" }, [_vm._v("PROFESSIONAL")]),
        _vm._v(" "),
        _vm._m(2),
        _vm._v(" "),
        _c("div", { staticClass: "price my-3" }, [
          !_vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v(" ₦7,000 "),
                _c("small", [_vm._v("/month")])
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.time
            ? _c("span", { staticClass: "myPrice" }, [
                _vm._v("₦5,834 "),
                _c("small", [_vm._v("/month billed annually")])
              ])
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "text-center my-3" }, [
          _vm.accountSub < 3
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.selectPackage("professional")
                    }
                  }
                },
                [_vm._v("Select")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.accountSub == 3
            ? _c(
                "button",
                {
                  staticClass: "button-blue mx-auto",
                  attrs: { type: "button" }
                },
                [_vm._v("Subscribed")]
              )
            : _vm._e()
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Custom Dashboard")]),
      _vm._v(" "),
      _c("li", [_vm._v("Single User")]),
      _vm._v(" "),
      _c("li", [_vm._v("Bookkeeping Tool")]),
      _vm._v(" "),
      _c("li", [_vm._v("Cash flow Report (Expense/Inflow)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Custom Dashboard")]),
      _vm._v(" "),
      _c("li", [_vm._v("Single User")]),
      _vm._v(" "),
      _c("li", [_vm._v("Cash flow Management")]),
      _vm._v(" "),
      _c("li", [_vm._v("Comprehensive Accounting Statement")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Custom Dashboard")]),
      _vm._v(" "),
      _c("li", [_vm._v("Multi-user")]),
      _vm._v(" "),
      _c("li", [_vm._v("Inventory Management Module")]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Billing & Invoicing Module (Sales Order, Invoices, Receipt and more)"
        )
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("Customer Relationship Management Module")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4019f2d2", module.exports)
  }
}

/***/ }),

/***/ 1798:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1799);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0cb003c0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-feb13a44\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-feb13a44\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1799:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bg[data-v-feb13a44] {\n  background-image: url(\"/images/curve1.jpg\");\n  background-size: cover;\n}\n", ""]);

// exports


/***/ }),

/***/ 1800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pricing__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pricing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__pricing__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__banner__ = __webpack_require__(1801);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__banner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__banner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoriesBarComponent__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_categoriesBarComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_categoriesBarComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent__ = __webpack_require__(944);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_userFooterComponent__ = __webpack_require__(939);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_userFooterComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_userFooterComponent__);
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      auth: false,
      accountSub: 0
    };
  },

  components: {
    Pricing: __WEBPACK_IMPORTED_MODULE_0__pricing___default.a,
    Banner: __WEBPACK_IMPORTED_MODULE_1__banner___default.a,
    Cat: __WEBPACK_IMPORTED_MODULE_2__components_categoriesBarComponent___default.a,
    Footer: __WEBPACK_IMPORTED_MODULE_4__components_userFooterComponent___default.a,
    Test: __WEBPACK_IMPORTED_MODULE_3__components_testimonialComponent___default.a
  },

  mounted: function mounted() {

    this.getAccountSub();
  },


  methods: {
    getAccountSub: function getAccountSub() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("accountUser"));
      if (user !== null) {
        axios.get("/api/account-sub/" + user.id).then(function (res) {
          if (res.status == 200) {
            if (res.data.status !== 'failed') {
              _this.accountSub = res.data;
            }
          }
        });
      }
    }
  }
});

/***/ }),

/***/ 1801:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1802)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1804)
/* template */
var __vue_template__ = __webpack_require__(1805)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2cde89d0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/banner.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2cde89d0", Component.options)
  } else {
    hotAPI.reload("data-v-2cde89d0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1802:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1803);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("697d7e6b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2cde89d0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./banner.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2cde89d0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./banner.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1803:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.banner[data-v-2cde89d0] {\n  width: 100%;\n  height: 85vh;\n  position: relative;\n  overflow: hidden;\n  background:#a4c2db;\n}\n.image-box[data-v-2cde89d0]{\n   position: absolute;\n  width: 60%;\n  right: -5%;\n  height: 100%;\n   background-image: url(\"/images/account.jpg\");\n  background-position: left;\n  background-size: cover;\n}\n.slant_1[data-v-2cde89d0] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n-webkit-transform:skewX(-25deg);\n        transform:skewX(-25deg);\n  top: -50%;\n    z-index: 2;\n    border-right:10px solid white;\n}\n.slant_2[data-v-2cde89d0] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n  -webkit-transform:skewX(25deg);\n          transform:skewX(25deg);\n  bottom: -49.7%;\n  z-index: 2;\n  border-right:10px solid white;\n}\n.text_box[data-v-2cde89d0] {\n  position: absolute;\n  width: 45%;\n  left: 0;\n  height: 100%;\n background: #a4c2db;\n  z-index: 3;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-orient:vertical;\n  -webkit-box-direction:normal;\n      -ms-flex-direction:column;\n          flex-direction:column;\n  padding-left:80px;\n}\n.text-container[data-v-2cde89d0] {\n  font-family: \"Josefin Sans\", sans-serif;\n  line-height: 1.4;\n}\n.bannerA[data-v-2cde89d0] {\n  font-size: 38px;\n  line-height: 1.4;\n}\n.bannerB[data-v-2cde89d0] {\n  font-size: 28px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bannerC[data-v-2cde89d0] {\n  font-size: 24px;\n  font-family: \"Open Sans\", sans-serif;\n}\n@media (max-width: 768px) {\n.butt[data-v-2cde89d0]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.but1[data-v-2cde89d0]{\n    margin-bottom: 16px;\n}\n.banner[data-v-2cde89d0]{\n    height: 90vh;\n}\n.image-box[data-v-2cde89d0] {\n    width: 30%;\n}\n.text_box[data-v-2cde89d0] {\n    width: 70%;\n    padding-left: 20px;\n     border-right: 6px solid white;\n}\n.bannerA[data-v-2cde89d0] {\n    font-size: 24px;\n}\n.bannerB[data-v-2cde89d0] {\n    font-size: 20px;\n}\n.bannerC[data-v-2cde89d0] {\n    font-size: 16px;\n}\n}\n@media (max-width: 350px) {\n.image-box[data-v-2cde89d0] {\n    width: 20%;\n}\n.text_box[data-v-2cde89d0] {\n    width: 80%;\n    padding-left: 20px;\n     border-right: 6px solid white;\n}\n.bannerA[data-v-2cde89d0] {\n    font-size: 20px;\n}\n.bannerB[data-v-2cde89d0] {\n    font-size: 18px;\n}\n.bannerC[data-v-2cde89d0] {\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1804:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      auth: false
    };
  },
  mounted: function mounted() {
    var user = localStorage.getItem('accountUser');
    if (user !== null) {
      this.auth = true;
    }
  }
});

/***/ }),

/***/ 1805:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "banner" }, [
    _c("div", { staticClass: "slant_1" }),
    _vm._v(" "),
    _c("div", { staticClass: "slant_2" }),
    _vm._v(" "),
    _c("div", { staticClass: "slant_2" }),
    _vm._v(" "),
    _c("div", { staticClass: "text_box" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "text-container" },
        [
          !_vm.auth
            ? _c(
                "router-link",
                { attrs: { to: "account/auth?type=register" } },
                [
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn btn-white text-main animated fadeIn slow"
                    },
                    [_vm._v("Get Started Now")]
                  )
                ]
              )
            : _c(
                "div",
                { staticClass: "d-flex butt" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "mr-5 but1",
                      attrs: { to: "/account/dashboard" }
                    },
                    [
                      _c(
                        "button",
                        {
                          staticClass:
                            "elevated_btn btn-white text-main animated fadeIn slow"
                        },
                        [_vm._v("Go to dashboard")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    { attrs: { to: "/account/dashboard?start=bookkeeping" } },
                    [
                      _c(
                        "button",
                        {
                          staticClass:
                            "elevated_btn btn-compliment text-white animated fadeIn slow"
                        },
                        [_vm._v("Start Bookkeping")]
                      )
                    ]
                  )
                ],
                1
              )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "image-box" })
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-container" }, [
      _c("p", { staticClass: "bannerA mb-3" }, [
        _vm._v(
          " Ditch the paper work for a powerful, yet approachable business management system that gets you organized, so that you can track your business profitability and growth in real-time."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "bannerB mb-2" }, [
        _vm._v("Want to know what your numbers say about your business???")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2cde89d0", module.exports)
  }
}

/***/ }),

/***/ 1806:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Banner", {
        staticClass: "bg",
        attrs: { accountSub: _vm.accountSub }
      }),
      _vm._v(" "),
      _vm.accountSub < 3
        ? _c("Pricing", { attrs: { accountSub: _vm.accountSub } })
        : _vm._e(),
      _vm._v(" "),
      _c("Cat"),
      _vm._v(" "),
      _c("Test"),
      _vm._v(" "),
      _c("Footer")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-feb13a44", module.exports)
  }
}

/***/ }),

/***/ 483:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1055)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1057)
/* template */
var __vue_template__ = __webpack_require__(1058)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4019f2d2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/pricing.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4019f2d2", Component.options)
  } else {
    hotAPI.reload("data-v-4019f2d2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 633:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1798)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1800)
/* template */
var __vue_template__ = __webpack_require__(1806)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-feb13a44"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/account/index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-feb13a44", Component.options)
  } else {
    hotAPI.reload("data-v-feb13a44", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 870:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(871)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(873)
/* template */
var __vue_template__ = __webpack_require__(874)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4f25ce06"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/toggle.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4f25ce06", Component.options)
  } else {
    hotAPI.reload("data-v-4f25ce06", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 871:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(872);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4407b836", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue", function() {
     var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 872:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.switch[data-v-4f25ce06] {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n\n/* Hide default HTML checkbox */\n.switch input[data-v-4f25ce06] {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n\n/* The slider */\n.slider[data-v-4f25ce06] {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: hsl(207, 46%, 20%);\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\n.slider[data-v-4f25ce06]:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\ninput:checked + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:focus + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:checked + .slider[data-v-4f25ce06]:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n\n/* Rounded sliders */\n.slider.round[data-v-4f25ce06] {\n  border-radius: 34px;\n}\n.slider.round[data-v-4f25ce06]:before {\n  border-radius: 50%;\n}\n@media(max-width:425px){\n.switch[data-v-4f25ce06] {\n\n  width: 50px;\n  height: 24px;\n}\n.slider[data-v-4f25ce06]:before {\n\n  height: 16px;\n  width: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            time: false
        };
    },

    watch: {
        'time': 'change'
    },
    methods: {
        change: function change() {
            this.$emit('handleToggle', this.time);
        }
    }
});

/***/ }),

/***/ 874:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("label", { staticClass: "switch" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.time,
            expression: "time"
          }
        ],
        attrs: { type: "checkbox" },
        domProps: {
          checked: Array.isArray(_vm.time)
            ? _vm._i(_vm.time, null) > -1
            : _vm.time
        },
        on: {
          change: function($event) {
            var $$a = _vm.time,
              $$el = $event.target,
              $$c = $$el.checked ? true : false
            if (Array.isArray($$a)) {
              var $$v = null,
                $$i = _vm._i($$a, $$v)
              if ($$el.checked) {
                $$i < 0 && (_vm.time = $$a.concat([$$v]))
              } else {
                $$i > -1 &&
                  (_vm.time = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
              }
            } else {
              _vm.time = $$c
            }
          }
        }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "slider round" })
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4f25ce06", module.exports)
  }
}

/***/ }),

/***/ 883:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(884)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(886)
/* template */
var __vue_template__ = __webpack_require__(887)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a85176d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoriesBarComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a85176d", Component.options)
  } else {
    hotAPI.reload("data-v-7a85176d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 884:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(885);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("35bd739c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 885:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.overlay-bus[data-v-7a85176d]{\n  position:absolute;\n  width:100%;\n  height:100%;\n  top:0;\n  background:rgba(255, 255, 255,.5);\n  z-index:0;\n}\n.it[data-v-7a85176d]{\n   position:relative;\n}\n.howIt[data-v-7a85176d]{\n  padding:65px 0 100px;\n  z-index:3;\n  position:relative;\n}\n.form[data-v-7a85176d] {\n  width: 65%;\n  margin-right: auto;\n  background: hsl(207, 43%, 20%);\n}\nh3.text-center[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n}\n.bms-heading[data-v-7a85176d] {\n  width: 40%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n}\n.expert-heading strong[data-v-7a85176d] {\n  font-size: 34px;\n  color: #fff;\n}\n.bms[data-v-7a85176d] {\n  padding: 0px;\n  width: 100%;\n  margin: 0 auto;\n  padding-top: 40px;\n  padding-bottom: 5px;\n}\n.bms_em[data-v-7a85176d]{\n  padding:40px;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  background:#f7f8fa;\n}\n.leftSlide p[data-v-7a85176d]{\n  font-size: 38px;\n  line-height:1.3;\n}\n.swiper[data-v-7a85176d] {\n  height: 600px;\n  width: 100%;\n}\n.accounting_slider[data-v-7a85176d] {\n  margin-top: 50px;\n  min-height: 400px;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient:horizontal;\n  -webkit-box-direction:reverse;\n      -ms-flex-direction:row-reverse;\n          flex-direction:row-reverse;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width:80%;\n  margin:0 auto;\n}\n.leftSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  text-align:center;\n}\n.rightSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0;\n}\n.rightSlide p[data-v-7a85176d]{\n  font-size: 32px;\n}\n.rightSlide img[data-v-7a85176d]{\n  width:70%;\n  height:auto;\n}\n.slide_button[data-v-7a85176d] {\n  width: 80%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin:0 auto\n}\n.my-slides[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n  background: white;\n}\n.bms_img[data-v-7a85176d]{\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: left;\n     object-position: left;\n}\n.profit[data-v-7a85176d] {\n  height: 50px;\n  margin: 30px auto 10px;\n}\n.gp[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n  text-align: center;\n}\n.gProfit[data-v-7a85176d] {\n  font-size: 24px;\n  font-weight: bold;\n  color: #fff;\n}\n.w-40[data-v-7a85176d] {\n  width: 40% !important;\n}\n.w-60[data-v-7a85176d] {\n  width: 70% !important;\n}\n.spin[data-v-7a85176d] {\n  right: 10px;\n  top: 43%;\n}\n.boxes[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 80%;\n  min-height: 50vh;\n  margin: 0 auto;\n \n  padding-bottom: 45px;\n  padding-top: 60px;\n}\n.leftBox[data-v-7a85176d] {\n  width: 50%;\n  padding: 40px 0px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.rightBox[data-v-7a85176d] {\n  width: 50%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 60px 0px;\n}\n.generate-text[data-v-7a85176d] {\n  padding: 10px 0;\n  font-size: 32px;\n  line-height: 1.6;\n  color: rgba(55, 58, 60);\n}\n.input-group[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n.input-group-text[data-v-7a85176d] {\n  font-size: 16px;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  right: -12px;\n  top: -17px;\n  cursor: pointer;\n  color: black;\n  padding: 3px 4px !important;\n  border-radius: 50%;\n  z-index: 6;\n  background: white;\n}\n.askBiz[data-v-7a85176d] {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  cursor: pointer;\n  z-index: 100;\n  text-align: center;\n  width: 25%;\n  height: auto;\n}\n.fa-2x[data-v-7a85176d] {\n  font-size: 1.5em;\n}\n.bizzy[data-v-7a85176d] {\n  width: 50px;\n  height: 50px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.goAsk[data-v-7a85176d] {\n  font-size: 18px;\n  font-weight: bold;\n  color: #a4c2db;\n}\n.askTimes[data-v-7a85176d] {\n  position: absolute;\n  top: -18px;\n  right: -18px;\n  cursor: pointer;\n}\n.askClose[data-v-7a85176d] {\n  margin-left: -7%;\n  text-align: right;\n  opacity: 0.4;\n  width: 9% !important;\n  padding: 0 !important;\n}\n.askClose[data-v-7a85176d]:hover {\n  opacity: 1;\n}\n.askIcons[data-v-7a85176d] {\n  -webkit-box-orient: vertical !important;\n  -webkit-box-direction: normal !important;\n      -ms-flex-direction: column !important;\n          flex-direction: column !important;\n  margin: 0 !important;\n  margin-left: auto !important;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  top: 10px;\n  right: 10px;\n  cursor: pointer;\n}\n.news-box[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.news-button[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.newsletter[data-v-7a85176d] {\n  width: 400px;\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: fixed;\n  top: 40%;\n  right: 15px;\n  z-index: 99;\n  overflow: hidden;\n  background: #d1e8fd;\n}\n.newsletter_image[data-v-7a85176d] {\n  position: absolute;\n  right: -26px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: right;\n     object-position: right;\n  height: 100%;\n}\n.btn-red[data-v-7a85176d] {\n  background-color: red !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.btn-success[data-v-7a85176d] {\n  background-color: green !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.form-control[data-v-7a85176d]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d] {\n  border: none;\n  border-bottom: 1px solid #ccc;\n  border-radius: 5px;\n  height: 38px;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n/* .howw {\n  height: 100vh;\n} */\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.separator-danger[data-v-7a85176d] {\n  color: #5b84a7;\n}\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n  margin: 0 auto 20px;\n  max-width: 240px;\n  text-align: center;\n  position: relative;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:before {\n  float: left;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:after {\n  float: right;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.center[data-v-7a85176d] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.column[data-v-7a85176d] {\n  float: left;\n  width: 33.3%;\n  padding: 5px;\n  background: #ffffff;\n}\n.card-header[data-v-7a85176d] {\n  background: hsl(207, 45%, 95%);\n}\n.taskImg[data-v-7a85176d] {\n  color: #5b84a7 !important;\n  text-align: center;\n  font-weight: normal;\n  text-align: center;\n  font-size: calc(14px + (20 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  width: 100%;\n}\n.howw[data-v-7a85176d] {\n  padding-bottom: 60px;\n}\n\n/* .taskImg[data-v-632021da] {\n    color: #ffffff !important;\n    font-weight: 600;\n    text-align: center;\n    font-size: 14px;\n    background: #143048;\n    #6a88a2\n    border-left: 2px solid white;\n    border-right: 2px solid white;\n} */\n.taskImg[data-v-7a85176d]:hover {\n  background: #5b84a7 !important;\n  color: #ffffff !important;\n}\n.taskImg .f-35[data-v-7a85176d] {\n  font-size: 25px;\n}\n/* .taskImg .f-35:hover{\n\n \tcolor: #ffffff !important;\n} */\n\n/* Clear floats after image containers */\n.row[data-v-7a85176d]::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n.courseBody[data-v-7a85176d] {\n  font-size: calc(13px + (18 - 16) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.imgTask[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.text-grey[data-v-7a85176d] {\n  color: #ccc !important;\n}\n.bg-grey[data-v-7a85176d] {\n  background: #ccc !important;\n  border: 1px solid #ccc !important;\n}\n.works[data-v-7a85176d] {\n  text-align: center;\n  padding: 60px 0 0px 0;\n  font-weight: bolder;\n  color: rgba(3, 12, 38, 0.6);\n}\n.oneContainer[data-v-7a85176d],\n.twoContainer[data-v-7a85176d],\n.threeContainer[data-v-7a85176d],\n.fourContainer[data-v-7a85176d],\n.fiveContainer[data-v-7a85176d] {\n  position: relative;\n  height: 100%;\n  border-radius: 10px;\n  width: 130%;\n  /* overflow: hidden; */\n}\n.leftContainer[data-v-7a85176d] {\n  width: 50%;\n  max-height: 675px;\n  margin-right: 20px;\n  /* overflow-y: scroll; */\n}\n.leftContainer[data-v-7a85176d]::-webkit-scrollbar {\n  color: #5b84a7;\n}\n.d[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.oneContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.twoContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.threeContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fourContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fiveContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.howContent[data-v-7a85176d] {\n  background: #ffffff;\n  /* margin-left: 20px; */\n  max-width: 55%;\n  border-radius: 5px;\n\n  overflow: hidden;\n}\n.courseTitles[data-v-7a85176d] {\n  color: rgba(3, 12, 38, 0.74);\n  margin-bottom: 8px;\n  font-weight: bold;\n  font-size: 16px;\n}\n.hws[data-v-7a85176d] {\n  padding: 10px 0;\n  cursor: pointer;\n}\n.howBorderFive[data-v-7a85176d] {\n  border-top: 1px solid #5b84a7;\n}\n.howBorder[data-v-7a85176d],\n.howBorderThird[data-v-7a85176d],\n.howBorderFour[data-v-7a85176d] {\n  border-bottom: 1px solid #5b84a7;\n  border-top: 1px solid #5b84a7;\n}\n.howCount[data-v-7a85176d] {\n  position: relative;\n  width: 16px;\n  height: 16px;\n  border: 1px solid rgb(3, 12, 38, 0.7);\n  border-radius: 60%;\n  background: hsl(225, 86%, 8%);\n  margin-right: 10px;\n  font-size: 12px;\n  margin-top: 3px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.howCon[data-v-7a85176d] {\n  width: 80%;\n}\n.num[data-v-7a85176d] {\n  padding: 6px;\n  color: white;\n}\n.cat-arrow[data-v-7a85176d] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: white;\n  font-size: 34px;\n}\n.brand[data-v-7a85176d] {\n  background: #c3f1d4;\n  margin: 13px;\n  padding: 13px 40px 400px;\n  position: relative;\n}\n.innovation[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7f0fb;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.procurement[data-v-7a85176d] {\n  margin: 13px;\n  background: #ffd3e6;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.timemgt[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7e6f4;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.sales[data-v-7a85176d] {\n  margin: 13px;\n  background: #dbf1fa;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.diver[data-v-7a85176d] {\n  margin: 40px;\n}\n@media (max-width: 1024px) {\n.form[data-v-7a85176d]{\n    width:90%;\n}\n}\n@media (max-width: 768px) {\n}\n@media only screen and (max-width: 768px) {\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:90%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:50%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.boxes[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 14px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 0;\n    min-height: 100px;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 28px;\n    text-align: center;\n}\n.form[data-v-7a85176d] {\n    width: 90%;\n}\n.accounting_slider[data-v-7a85176d]{\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:28px;\n}\n}\n@media only screen and (max-width: 575px) {\n.boxes[data-v-7a85176d] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    padding: 30px 20px;\n}\n.gp[data-v-7a85176d] {\n    font-size: 12px;\n}\n.gProfit[data-v-7a85176d] {\n    font-size: 16px;\n}\n.form[data-v-7a85176d] {\n    margin: 0 auto;\n    width: 90%;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 13px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 18.5px;\n    text-align: center;\n}\n.newsletter[data-v-7a85176d] {\n    width: 300px;\n}\n.taskImg .f-35[data-v-7a85176d] {\n    font-size: 16px;\n}\n.oneContainer[data-v-7a85176d],\n  .twoContainer[data-v-7a85176d],\n  .threeContainer[data-v-7a85176d],\n  .fourContainer[data-v-7a85176d],\n  .fiveContainer[data-v-7a85176d] {\n    border-radius: 0;\n}\n.howItWork[data-v-7a85176d] {\n    display: block !important;\n    margin-right: 0;\n    margin-left: 0;\n}\n.leftContainer[data-v-7a85176d] {\n    padding: 0 35px;\n    width: 100%;\n}\n.howContent[data-v-7a85176d] {\n    width: 100%;\n    max-width: 100%;\n    height: auto;\n}\n.hws[data-v-7a85176d] {\n    margin: 0;\n\n    padding: 20px 0 5px;\n}\n.howCount[data-v-7a85176d] {\n    width: 15px;\n    height: 15px;\n}\n.howCon[data-v-7a85176d] {\n    width: 100%;\n}\n.taskImg[data-v-7a85176d] {\n    color: #5b84a7 !important;\n    font-size: calc(8px + (26 - 8) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.works[data-v-7a85176d] {\n    font-size: calc(20px + (26 - 20) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n    margin: 20px 0 10px 0;\n    padding: 20px 0 0 0;\n}\n.num[data-v-7a85176d] {\n    top: -5px;\n    right: 3px;\n    font-size: 12px;\n}\n.fa-2x[data-v-7a85176d] {\n    font-size: 1em;\n}\n.scrollUp[data-v-7a85176d] {\n    padding: 5px 7px;\n}\n@media (max-width: 425px) {\n.goAsk[data-v-7a85176d] {\n      font-size: 16px !important;\n}\n.askBiz[data-v-7a85176d] {\n      width: 75%;\n}\n.bizzy[data-v-7a85176d] {\n      width: 40px;\n}\n.askClose[data-v-7a85176d] {\n      width: 11% !important;\n}\n.fa-times[data-v-7a85176d],\n    .fa-plus[data-v-7a85176d] {\n      font-size: 0.7em;\n      top: 0;\n      right: -16px;\n}\n.askTimes[data-v-7a85176d] {\n      top: -8px;\n      right: 8px;\n}\n.card-title[data-v-7a85176d]{\n      font-size:13px;\n}\n.card-text[data-v-7a85176d]{\n       font-size:13px;\n}\n.slide_button[data-v-7a85176d]{\n    width:100%\n}\n.leftSlide[data-v-7a85176d]{\n    width:100%;\n    padding:20px 5px\n}\n.mini-content[data-v-7a85176d]{\n    padding-top:10px;\n}\n.mini-content-title[data-v-7a85176d],  .bms-heading strong[data-v-7a85176d]{\n    font-size:16px;\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.mini-content-title[data-v-7a85176d]{\n    width:60%;\n    margin-left:-4.5%;\n}\n.business-words[data-v-7a85176d]{\n    display: grid;\n    grid-template-rows: auto auto;\n    grid-template-columns: repeat(10,145px);\n    grid-column-gap: 20px;\n    grid-row-gap: 10px;\n    margin-bottom: 20px;\n    overflow-x: scroll;\n}\n.single-word[data-v-7a85176d]{\n    padding: 10px 9px;\n    font-size: 12px;\n    text-align: center;\n}\n.bms-heading strong[data-v-7a85176d]{\n    font-size:18px;\n}\n.mini-content[data-v-7a85176d]{\n    padding-left:15px;\n    padding-right:15px;\n    padding-top:30px;\n}\n.mini-content-title[data-v-7a85176d]{\n    font-size:18px;\n    margin-left:-3%;\n      padding: 15px 20px 13px;\n}\n.overviewMain[data-v-7a85176d]{\n    width:80%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:85%;\n    padding: 20px 20px 15px;\n}\n.accounting_slider[data-v-7a85176d]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height:auto;\n    width:100%;\n    padding: 40px 0;\n}\n.rightSlide[data-v-7a85176d],.leftSlide[data-v-7a85176d]{\n    width:80%;\n    margin:0 auto\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.leftSlide[data-v-7a85176d]{\n    padding:20px;\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:24px;\n    text-align:center !important;\n    padding:10px;\n}\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "categories-bar-component",
  data: function data() {
    var _ref;

    return _ref = {
      gp: 0,
      revenue: null,
      cost: null,
      loader: false,
      calculate: "Continue to opex",
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      askOpen: false,
      show: false,
      first_name: "",
      last_name: "",
      email: "",
      one: true,
      two: false,
      three: false,
      four: false,
      five: false,
      howBor: false,
      howBoro: false,
      howBorod: false,
      howBorod4: false,
      howBorod5: false,
      textGreyOne: false,
      textGreyTwo: true,
      textGreyThree: true,
      textGreyFour: true,
      textGreyFive: true
    }, _defineProperty(_ref, "scrollPos", 0), _defineProperty(_ref, "currentHeight", 0), _defineProperty(_ref, "scrollUpButton", false), _defineProperty(_ref, "currencySymbol", "NGN "), _defineProperty(_ref, "position", "prefix"), _ref;
  },

  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      window.addEventListener("scroll", function (e) {
        _this.scrollPos = window.scrollY;
        _this.currentHeight = window.innerHeight;
      });
    }
  },

  computed: {
    currency: function currency() {
      return _defineProperty({}, this.position, this.currencySymbol);
    }
  },
  watch: {
    scrollPos: "scrolling",
    cost: "calcGp"
  },
  methods: {
    goToDemo: function goToDemo() {
      var user = localStorage.getItem('authUser');
      if (user !== null) {
        var demo = localStorage.getItem('demo');

        this.$router.push({
          name: 'AccountingDemo',
          query: {
            redirect_to: 'demo'
          }
        });
      } else {
        this.$router.push({
          name: 'auth',
          params: { name: 'register' },
          query: {
            redirect_from: 'explore'
          }
        });
      }
    },
    scrolling: function scrolling() {
      if (this.scrollPos > window.innerHeight * 0.3) {
        this.scrollUpButton = true;
      } else {
        this.scrollUpButton = false;
      }

      if (this.scrollPos > window.innerHeight * 1.2) {
        this.swing = true;
      }
    },
    calcGp: function calcGp() {
      this.gp = this.revenue - this.cost;
    },
    calc: function calc() {
      var _this2 = this;

      if (this.gp != 0) {
        this.loader = true;
        this.calculate = "processing ...";
        setTimeout(function () {
          _this2.calculate = "calculating ...";
        }, 2000);

        setTimeout(function () {
          _this2.calculate = "calculate";
          _this2.loader = false;
          _this2.$router.push('/account/pricing?session=pricing');
        }, 4000);
      }
    },
    currencyFormat: function currencyFormat(num) {
      if (num !== null) {
        return "NGN " + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      } else {
        return "NGN 0.00";
      }
    },
    openAsk: function openAsk() {
      this.$router.push("/ask-bizguruh");
    },
    askClose: function askClose() {
      this.askOpen = !this.askOpen;
    },
    showNews: function showNews() {
      this.show = false;
    },
    subscribe: function subscribe() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        email: user.email,
        first_name: user.name,
        last_name: user.name
      };
      axios.post("/api/subscribe", data).then(function (response) {
        if (response.status === 200) {
          _this3.$toasted.success("Subscription successful");
          localStorage.removeItem("news");
          _this3.email = _this3.first_name = _this3.last_name = "";
          _this3.show = false;
        }
        {}
      });
    },
    switchTab: function switchTab(params) {
      switch (params) {
        case "one":
          this.one = this.howBor = this.textGreyTwo = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.two = this.three = this.four = this.five = this.howBoro = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyOne = false;
          break;
        case "two":
          this.two = this.howBoro = this.textGreyOne = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.one = this.three = this.four = this.five = this.howBor = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyTwo = false;
          break;
        case "three":
          this.three = this.howBorod = this.textGreyOne = this.textGreyTwo = this.textGreyFour = this.textGreyFive = true;
          this.one = this.two = this.four = this.five = this.howBor = this.howBoro = this.howBorod4 = this.howBorod5 = this.textGreyThree = false;
          break;
        case "four":
          this.four = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.textGreyFive = this.howBorod4 = true;
          this.one = this.two = this.three = this.five = this.howBor = this.howBoro = this.howBorod5 = this.howBorod = this.textGreyFour = false;
          break;
        case "five":
          this.five = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.howBorod5 = this.textGreyFour = true;
          this.one = this.two = this.three = this.four = this.howBor = this.howBoro = this.howBorod4 = this.howBorod = this.textGreyFive = false;
          break;
        default:
          this.one = this.two = this.three = this.four = this.five = false;
      }
    }
  }
});

/***/ }),

/***/ 887:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "it" }, [
    _c("div", { staticClass: "overlay-bus" }),
    _vm._v(" "),
    _c("div", { staticClass: "howIt" }, [
      _c("h3", { staticClass: "mb-5 desc_header josefin " }, [
        _vm._v("Business Management Solutions")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "boxes" }, [
        _c("div", { staticClass: "leftBox" }, [
          _c(
            "form",
            {
              staticClass: "form shadow p-lg-5 p-md-4 p-sm-3 p-4 rounded",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.calc($event)
                }
              }
            },
            [
              _c("h3", { staticClass: "text-center mb-3" }, [
                _vm._v("Account Statement Generator")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.revenue,
                      callback: function($$v) {
                        _vm.revenue = $$v
                      },
                      expression: "revenue"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(1)
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.cost,
                      callback: function($$v) {
                        _vm.cost = $$v
                      },
                      expression: "cost"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(3)
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "profit" }, [
                _c("p", { staticClass: "gp" }, [
                  _c("span", { staticClass: "gProfit" }, [
                    _vm._v(_vm._s(_vm.currencyFormat(this.gp)))
                  ]),
                  _vm._v(" Gross profit\n           ")
                ])
              ]),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "elevated_btn bg-white text-main mx-auto",
                  attrs: { type: "submit" }
                },
                [
                  _vm._v(
                    "\n           " + _vm._s(_vm.calculate) + "\n           "
                  ),
                  _vm.loader
                    ? _c("span", {
                        staticClass:
                          "spinner-grow spinner-grow-sm spin text-grey"
                      })
                    : _vm._e()
                ]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "rightBox" }, [
          _c(
            "p",
            {
              staticClass: "generate-text animated slideInUp",
              class: { "d-none": !_vm.swing }
            },
            [
              _vm._v(
                "\n         Ditch the paperwork, get organized and track your business\n         "
              ),
              _c("strong", [_vm._v("PROFITABILITY")]),
              _vm._v(" and\n         "),
              _c("strong", [_vm._v("GROWTH")]),
              _vm._v(" in\n         "),
              _c("strong", [_vm._v("REAL-TIME")]),
              _vm._v("!\n       ")
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _vm.$route.name !== "IndexPage"
        ? _c("section", {}, [
            _c("div", { staticClass: "bms" }, [
              _c("div", { staticClass: "accounting_slider" }, [
                _c("div", { staticClass: "rightSlide" }, [
                  _vm.swing
                    ? _c("img", {
                        staticClass: "bms_img animated slideInUp",
                        attrs: { src: "/images/word_threed.png", alt: "" }
                      })
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "leftSlide" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "slide_button" },
                    [
                      _c(
                        "button",
                        {
                          staticClass: "elevated_btn btn-compliment text-white",
                          attrs: { type: "button" },
                          on: { click: _vm.goToDemo }
                        },
                        [_vm._v("Free Demo Now")]
                      ),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "SubscriptionProfile",
                              params: {
                                level: 0
                              },
                              query: {
                                name: "subscribe-accounting"
                              }
                            }
                          }
                        },
                        [
                          _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn btn-outline-compliment bg-white",
                              attrs: { type: "button" }
                            },
                            [_vm._v("Subscribe")]
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Revenue")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Your revenue refers to fees earned from providing goods or services. Under the accrual basis of accounting, revenues are recorded at the time of delivering the service or the merchandise, even if cash is not received at the time of delivery. Often the term income is used instead of revenue."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("COGS")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Cost of goods sold (COGS) refers to the direct costs of producing the goods sold by your company. This amount includes the cost of the materials and labor directly used to create the good but excludes indirect expenses, such as distribution costs and sales force costs."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "w-100  mb-4" }, [
      _c("span", { staticClass: "text-main bold" }, [_vm._v("BE EMPOWERED")]),
      _vm._v(" - To make decisions that "),
      _c("span", { staticClass: "text-main bold" }, [_vm._v("GROW")]),
      _vm._v(" your business")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a85176d", module.exports)
  }
}

/***/ }),

/***/ 939:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(940)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(942)
/* template */
var __vue_template__ = __webpack_require__(943)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-61a5ae60"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userFooterComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-61a5ae60", Component.options)
  } else {
    hotAPI.reload("data-v-61a5ae60", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 940:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(941);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("e6cb859c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-61a5ae60\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFooterComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-61a5ae60\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userFooterComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 941:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.footer[data-v-61a5ae60]{\n  padding:65px 0 100px;\n  background: #20262b;\n}\n.side[data-v-61a5ae60]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-orient:vertical;\n  -webkit-box-direction:normal;\n      -ms-flex-direction:column;\n          flex-direction:column;\n  -webkit-box-pack:justify;\n      -ms-flex-pack:justify;\n          justify-content:space-between;\n  -webkit-box-align:start;\n      -ms-flex-align:start;\n          align-items:flex-start\n}\n.display-4[data-v-61a5ae60] {\n    font-size: 3.5rem;\n    font-weight: 300;\n    line-height: 1.2;\n}\n.text-bl[data-v-61a5ae60]{\n   color:#a4c2db;\n}\n.pr-1[data-v-61a5ae60] {\n  padding-right: 1px !important;\n}\n.pl-1[data-v-61a5ae60] {\n  padding-left: 1px !important;\n}\nli[data-v-61a5ae60] {\n    margin-bottom: 14px !important;\n}\n.btn[data-v-61a5ae60] {\n  background-color: #fff !important;\n  color: #333;\n  text-transform: capitalize !important;\n  font-weight: normal;\n}\n.form-control[data-v-61a5ae60]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-61a5ae60] {\n  border: none;\n}\n.container[data-v-61a5ae60] {\n  padding: 0px !important;\n}\n.col-xs-6[data-v-61a5ae60] {\n  width: 50%;\n}\nh2[data-v-61a5ae60] {\n  font-size: 16px !important;\n  margin-bottom:16px;\n}\n.site-footer[data-v-61a5ae60] {\n  font-size: 16px;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n width:80%;\n margin:0 auto;\n}\n.side[data-v-61a5ae60]{\n  width:30%\n}\n.footer-widgets[data-v-61a5ae60]{\n  width:70%;\n}\n.footer-widgets h2[data-v-61a5ae60] {\n  color: hsl(207, 15%, 70%);\n}\n.footer_body[data-v-61a5ae60] {\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  -webkit-box-align:center;\n      -ms-flex-align:center;\n          align-items:center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-bottom:30px;\n}\n.foot[data-v-61a5ae60] {\n  font-size: 16px;\n  line-height: 1.4;\n  text-align:left;\n}\n.col-6[data-v-61a5ae60] {\n  background-color: #ffffff;\n}\n.socials[data-v-61a5ae60] {\n  padding: 10px 0;\n   display: -webkit-box;\n   display: -ms-flexbox;\n   display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.social_icons[data-v-61a5ae60] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 10%;\n  margin: 0 auto;\n  list-style: none;\n}\nul[data-v-61a5ae60],\nol[data-v-61a5ae60] {\n  list-style: none;\n}\nul li a[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%) !important;\nmargin-bottom:14px;\n}\n.text-bg[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%);\n}\na[data-v-61a5ae60] {\ncolor: hsl(207, 15%, 70%);\n}\n.reserve[data-v-61a5ae60]{\ncolor: hsl(207, 15%, 70%);\n}\n.rules[data-v-61a5ae60] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  font-size: 16px;\n}\n.copyright[data-v-61a5ae60] {\n  color: rgba(0, 0, 0, 0.54);\n  font-size: 16px;\n}\n.footer_policy[data-v-61a5ae60] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  border-top: 1px dashed rgba(255, 255, 255, 0.84);\n  padding: 0;\n  font-size: 16px;\n  margin: 0;\n}\n.fa-inverse[data-v-61a5ae60] {\n color: rgba(0, 0, 0, 0.54);\n}\n.guidelines[data-v-61a5ae60]::after {\n  content: \" |\";\n  color: rgba(0, 0, 0, 0.54);\n  height: 10px;\n}\n.terms[data-v-61a5ae60]::after {\n  content: \" |\";\n color: rgba(0, 0, 0, 0.54);\n  height: 10px;\n}\n@media (max-width: 768px) {\n.footer_policy[data-v-61a5ae60] {\n    padding: 10px;\n}\nh2[data-v-61a5ae60] {\n    font-size: 17px !important;\n    margin-bottom:10px;\n}\n.rules[data-v-61a5ae60] {\n    font-size: 16px;\n}\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.foot[data-v-61a5ae60] {\n    margin-bottom:50px;\n}\n.rules[data-v-61a5ae60] {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.footer_policy[data-v-61a5ae60] {\n    text-align: center;\n}\n}\n@media (max-width: 1024px) {\n.site-footer[data-v-61a5ae60]{\n  width:90%;\n     -webkit-box-orient:vertical;\n     -webkit-box-direction:reverse;\n         -ms-flex-direction:column-reverse;\n             flex-direction:column-reverse;\n     -webkit-box-align:center;\n         -ms-flex-align:center;\n             align-items:center;\n}\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align:start;\n        -ms-flex-align:start;\n            align-items:flex-start;\n}\n.display-4[data-v-61a5ae60]{\n  font-size:2.5em;\n}\nul li a[data-v-61a5ae60]{\n  font-size:15px;\n  font-weight:300;\n}\n.footer-widgets[data-v-61a5ae60]{\n  width:100%;\n}\n.side[data-v-61a5ae60]{\n    width:100%;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n}\n}\n@media (max-width: 768px) {\n}\n@media (max-width: 575px) {\n.footer_body[data-v-61a5ae60] {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align:start;\n        -ms-flex-align:start;\n            align-items:flex-start;\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n}\n.rules[data-v-61a5ae60] {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.footer_policy[data-v-61a5ae60] {\n    text-align: center;\n}\nh2[data-v-61a5ae60] {\n    font-size: 16px !important;\n}\n.site-footer[data-v-61a5ae60] {\n    font-size: 12px;\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:reverse;\n        -ms-flex-direction:column-reverse;\n            flex-direction:column-reverse;\n}\n.side[data-v-61a5ae60]{\n    width:100%;\n    -webkit-box-align:center;\n        -ms-flex-align:center;\n            align-items:center;\n}\n.foot[data-v-61a5ae60] {\n    font-size: 16px;\n    line-height: 1;\n}\n.mt-5[data-v-61a5ae60] {\n    margin-top: 0px !important;\n}\n.footer_body[data-v-61a5ae60] {\n    padding: 5px 10px;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.footer_policy[data-v-61a5ae60] {\n    padding: 15px;\n}\n.reserve[data-v-61a5ae60] {\n    text-align: center;\n}\n.copyright[data-v-61a5ae60] {\n    padding: 0;\n}\n.rules[data-v-61a5ae60] {\n    padding: 0;\n    font-size: 12px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 942:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-new-arrival-component",

  data: function data() {
    return {
      year: 0,
      first_name: "",
      last_name: "",
      email: "",
      hide: false
    };
  },

  methods: {
    hideFooter: function hideFooter() {
      if (this.$route.path == '/private-chat') {
        this.hide = true;
      } else {
        this.hide = false;
      }
    }
  },
  mounted: function mounted() {
    this.hideFooter();
  },

  watch: {
    '$route': 'hideFooter'
  },
  computed: {
    getYear: function getYear() {
      var date = new Date();
      return this.year = date.getFullYear();
    }
  }
});

/***/ }),

/***/ 943:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "footer" }, [
    !_vm.hide
      ? _c("footer", { staticClass: "site-footer" }, [
          _c("div", { staticClass: "side" }, [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "copyright" }, [
              _c("p", { staticClass: "reserve" }, [
                _vm._v(
                  "© " + _vm._s(_vm.getYear) + " Bizguruh . All rights reserved"
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "footer-widgets" }, [
            _c("div", { staticClass: "footer_body" }, [
              _c("div", { staticClass: "foot \n          " }, [
                _c("div", { staticClass: "foot-about" }, [
                  _c("h2", [_vm._v("BIZGURUH")]),
                  _vm._v(" "),
                  _c("ul", [
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/guidelines" } }, [
                          _vm._v("Guidelines")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/faqs" } }, [
                          _vm._v("FAQs")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/terms" } }, [
                          _vm._v("T&C's")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/policies" } }, [
                          _vm._v("Privacy")
                        ])
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "foot " }, [
                _c("div", { staticClass: "foot-contact" }, [
                  _c("h2", [_vm._v("BROWSE")]),
                  _vm._v(" "),
                  _c("ul", [
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/explore" } }, [
                          _vm._v("Explore")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/videos" } }, [
                          _vm._v("Videos")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/articles" } }, [
                          _vm._v("Articles")
                        ])
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("router-link", { attrs: { to: "/community" } }, [
                          _vm._v("Community")
                        ])
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _vm._m(3)
            ])
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c("span", { staticClass: "display-4 text-bl" }, [_vm._v("BizGuruh")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "socials" }, [
      _c("i", {
        staticClass: "fa fa-facebook-square text-bg fa-1x mr-3",
        attrs: { "aria-hidden": "true" }
      }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-instagram text-bg fa-1x mr-3" }),
      _vm._v(" "),
      _c("i", {
        staticClass: "fa fa-twitter-square text-bg fa-1x",
        attrs: { "aria-hidden": "true" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "foot " }, [
      _c("div", { staticClass: "quick-links flex flex-wrap" }, [
        _c("h2", { staticClass: "w-100" }, [_vm._v("SOLUTIONS")]),
        _vm._v(" "),
        _c("ul", { staticClass: "w-100" }, [
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/market-research" } }, [
              _vm._v("Market Research")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Market Reports")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Consulting")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "bizguruh.com/explore" } }, [
              _vm._v("Business Research")
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "foot " }, [
      _c("div", { staticClass: "quick-links flex flex-wrap" }, [
        _c("h2", { staticClass: "w-100" }, [_vm._v("CONTACT US")]),
        _vm._v(" "),
        _c("ul", { staticClass: "w-100" }, [
          _c("li", [
            _c("a", { attrs: { href: "http://bizguruh.com/contact" } }, [
              _c("i", {
                staticClass: "fa fa-envelope",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" Mail\n                  ")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href: "https://www.facebook.com/BizGuruh-314446332731552/"
                }
              },
              [
                _c("i", {
                  staticClass: "fa fa-facebook",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Facebook\n                  ")
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c("a", { attrs: { href: "https://twitter.com/BizGuruh" } }, [
              _c("i", {
                staticClass: "fa fa-twitter",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v(" Twitter\n                  ")
            ])
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              { attrs: { href: "https://www.instagram.com/bizguruh/" } },
              [
                _c("i", {
                  staticClass: "fa fa-instagram",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Instagram\n                  ")
              ]
            )
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-61a5ae60", module.exports)
  }
}

/***/ }),

/***/ 944:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(945)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(947)
/* template */
var __vue_template__ = __webpack_require__(948)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a1b2036"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/testimonialComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a1b2036", Component.options)
  } else {
    hotAPI.reload("data-v-7a1b2036", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 945:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(946);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("460f1c1b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 946:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nh3[data-v-7a1b2036] {\n  text-transform: initial;\n  font-size: 34px;\n  color: #333;\n}\n.main[data-v-7a1b2036] {\n background-color: white;\n  padding-top: 65px;\n  padding-bottom: 100px;\n\n  overflow: hidden;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bg-row[data-v-7a1b2036] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.bg1[data-v-7a1b2036] {\n  width: 60%;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  height: 156%;\n  top: -44%;\n  position: absolute;\n  right: -14%;\n}\n.body[data-v-7a1b2036]{\n\n    width: 100%;\n    padding: 15px 15px 45px;\n    border-radius: 5px;\n    z-index: 2;\n}\n.my-content[data-v-7a1b2036] {\n  display: grid;\n  grid-template-columns: auto auto;\n  grid-template-rows: auto auto;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 80%;\n  margin: 0 auto;\n  grid-column-gap: 50px;\n  grid-row-gap: 50px;\n  padding-top:20px\n}\n.testimonial-box[data-v-7a1b2036] {\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background:#f7f8fa;\n  height:200px;\n}\n.testimonial-image[data-v-7a1b2036] {\n  background:rgba(164, 194, 219,.5);\n  width: 40%;\n  height:100%;\n  text-align: center;\n  padding: 15px;\n}\n.testimonial-image img[data-v-7a1b2036] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.testimonial-text[data-v-7a1b2036] {\n  width: 60%;\n  padding: 15px;\n}\n@media (max-width: 768px) {\nh3[data-v-7a1b2036] {\n    font-size: 28px;\n    margin-bottom: 16px;\n}\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n}\n.testimonial-box shadow-sm[data-v-7a1b2036] {\n    padding: 15px;\n    background: white;\n    border-radius: 5px;\n}\nstrong[data-v-7a1b2036] {\n    font-size: 14px;\n}\np[data-v-7a1b2036]{\n  font-size:14px;\n}\n.body[data-v-7a1b2036]{\n  padding:0;\n}\n}\n@media (max-width: 575px) {\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n    grid-template-columns: auto;\n    grid-template-rows: auto;\n}\n.bg1[data-v-7a1b2036] {\n    width: 60%;\n    /* background: #e3ecf4; */\n    -webkit-transform: rotate(-20deg);\n    transform: rotate(-20deg);\n    height: 156%;\n    top: -60%;\n    position: absolute;\n    right: -14%;\n}\n.body[data-v-7a1b2036] {\n    background: transparent;\n    width: 100%;\n    padding: 15px;\n    border-radius: 5px;\n    z-index: 3;\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n}\n.testimonial-text[data-v-7a1b2036]{\n  font-size: 14px;\n}\nstrong[data-v-7a1b2036]{\n  font-size: 14.5px;\n}\n/* .testimonial-image img{\n  width: 60px;\n  height: auto;\n} */\n}\n", ""]);

// exports


/***/ }),

/***/ 947:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "testimonial-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
  },

  watch: {
    scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 3.85) {
        this.swing = true;
      }
    }
  }
});

/***/ }),

/***/ 948:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main" }, [
      _c("div", { staticClass: "body " }, [
        _c("h3", { staticClass: "mb-5 w-100 text-center desc_header" }, [
          _vm._v("What our users think")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "my-content animated fadeIn" }, [
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-three.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          The post on registering ones business was\n          particularly beneficial. I’m currently saving\n          towards it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @oilbyo")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I am here because\n          @freesiafoodies\n          asked us to, but seriously I must say it is worth\n          it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Oladunni Omotola")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I heard about your page from Nwanyiakamu and I\n          followed you. Your content is very educative and\n          qualitative always. Keep it up and thank you.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Tinuke Adegboye")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-one.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "I find your insights very inspiring, I stopped dreaming and started doing."
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @theefosa")])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a1b2036", module.exports)
  }
}

/***/ })

});