webpackJsonp([101],{

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(965)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(967)
/* template */
var __vue_template__ = __webpack_require__(968)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3862c4b7"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/auth/registerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3862c4b7", Component.options)
  } else {
    hotAPI.reload("data-v-3862c4b7", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 965:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(966);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("90ecbbda", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3862c4b7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./registerComponent.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3862c4b7\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./registerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 966:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.form-control[data-v-3862c4b7] {\n  border-top: unset;\n  border-left: unset;\n  border-right: unset;\n  /* background-color:lightsteelblue; */\n}\n.form-control[data-v-3862c4b7]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-3862c4b7]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-3862c4b7]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-3862c4b7]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n}\n.form-control[data-v-3862c4b7]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-3862c4b7]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n}\n.form-control[data-v-3862c4b7] {\n  background: rgba(255, 255, 255, 0.7) !important;\n}\n.btn[data-v-3862c4b7] {\n  background: #a4c2db !important;\n}\n.error[data-v-3862c4b7] {\n  color: white;\n  padding: 0 10px;\n  background: red;\n  position: absolute;\n  top: 21%;\n  font-size: 16px;\n  font-weight: normal;\n  right: 30px;\n}\n.formSi[data-v-3862c4b7] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hide-input[data-v-3862c4b7] {\n  width: 0.1px;\n  height: 0.1px;\n  opacity: 0;\n  overflow: hidden;\n  position: absolute;\n  z-index: -1;\n}\n.profileimageDiv[data-v-3862c4b7] {\n  width: 100px;\n  height: 100px;\n}\n.profileImage[data-v-3862c4b7] {\n  width: 100%;\n  height: 100%;\n}\ninput[type=\"radio\"][data-v-3862c4b7],\ninput[type=\"checkbox\"][data-v-3862c4b7] {\n  margin: 0;\n  line-height: normal;\n}\n.btn[data-v-3862c4b7],\n.btn-primary[data-v-3862c4b7] {\n  background-color: #333333 !important;\n  text-transform: capitalize !important;\n}\nlabel[data-v-3862c4b7] {\n  font-weight: normal;\n}\n", ""]);

// exports


/***/ }),

/***/ 967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "registerComponent",
  data: function data() {
    return {
      user: {
        storeName: "",
        username: "",
        email: "",
        password: "",
        password_confirmation: "",
        bio: "",
        userSwitch: "expert",
        subjmatter: "default",
        topic: [],
        validId: "",
        address: "",
        occupation: "",
        image: ""
      },
      firstForm: true,
      secondForm: false,
      thirdForm: false,
      subjectMatter: [],
      topics: [],
      imagePlaceholder: "/images/addImage.png",
      cloudinary: {
        uploadPreset: "knkccgjv",
        apiKey: "634813511968181",
        cloudName: "bizguruh-com"
      },
      percentage: "",
      password_error: false,
      email_error: false,
      storeName_error: false,
      show: false
    };
  },
  mounted: function mounted() {
    this.subjectMatters();
    this.allTopics();
  },

  watch: {},
  methods: {
    checkBio: function checkBio() {
      if (this.user.bio.length > 250) {
        this.$toasted.error("Bio too long");
      }
    },
    checkEmail: function checkEmail() {
      var _this = this;

      axios.get("/api/check/" + this.user.email).then(function (response) {
        if (response.data === "email_error") {
          _this.email_error = true;
          _this.show = false;
          _this.$toasted.error("Email already exist");

          setTimeout(function () {
            _this.email_error = false;
          }, 2000);
        }
      });
    },
    checkStore: function checkStore() {
      var _this2 = this;

      axios.get("/api/check/" + this.user.storeName_error).then(function (response) {
        if (response.data === "storeName_error") {
          _this2.storeName_error = true;
          _this2.show = false;
          _this2.$toasted.error("Storename already exist");

          setTimeout(function () {
            _this2.storeName_error = false;
          }, 4000);
        }
      });
    },
    register: function register() {
      var _this3 = this;

      JSON.stringify(this.user.topic);
      this.user.mytopic = JSON.stringify(this.user.topic);

      this.checkEmail();
      // this.checkStore();
      this.user.storeName = this.user.username;
      this.user.password_confirmation = this.user.password;
      this.show = true;
      axios.post("/api/vendor/register", JSON.parse(JSON.stringify(this.user))).then(function (response) {
        if (response.status === 200) {
          _this3.show = false;
          _this3.$toasted.info("Registration successful, check email for verification link");

          _this3.$router.push('/vendor/auth?type=login');
        } else {
          _this3.$toasted.error("Something went wrong,please check all fields");
          _this3.show = false;
        }
      }).catch(function (error) {
        console.log(error);
        _this3.show = false;
        _this3.$toasted.error("Something went wrong,please check all fields");
      });
    },
    switchTab: function switchTab(params) {
      switch (params) {
        case "firstForm":

          this.firstForm = true;
          this.secondForm = this.thirdForm = false;

          break;
        case "secondForm":

          this.secondForm = true;
          this.thirdForm = this.firstForm = false;

          break;
        case "thirdForm":
          this.thirdForm = true;
          this.secondForm = this.firstForm = false;
          break;

        default:
          this.firstForm = this.secondForm = this.thirdForm = false;
      }
    },
    subjectMatters: function subjectMatters() {
      var _this4 = this;

      axios.get("/api/subjectmatter").then(function (response) {
        if (response.status === 200) {
          _this4.subjectMatter = response.data.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    allTopics: function allTopics() {
      var _this5 = this;

      axios.get("/api/topics").then(function (response) {
        if (response.status === 200) {
          // console.log(response.data);
          _this5.topics = response.data;
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    getFileName: function getFileName(e, files, name) {
      var that = this;
      var formData = new FormData();
      formData.append("file", files[0]);
      formData.append("upload_preset", this.cloudinary.uploadPreset);
      formData.append("tags", "gs-vue,gs-vue-uploaded");
      var request = new XMLHttpRequest();
      request.upload.onprogress = function (e) {
        if (e.lengthComputable) {
          that.percentage = Math.round(e.loaded / e.total * 100) + "%";
        }
      };

      request.onreadystatechange = function () {
        if (request.readyState === 4) {
          var res = JSON.parse(request.response);

          that.user.image = res.secure_url;
          that.imagePlaceholder = res.url;
          that.percentage = "Completed";
        }
      };

      request.upload.onloadstart = function (e) {
        that.percentage = "Starting...";
      };
      request.upload.onloadend = function (e) {
        that.percentage = "Completing...";
      };

      request.open("POST", this.clUrl);
      request.send(formData);
    }
  },
  computed: {
    filterTopicByMatter: function filterTopicByMatter() {
      var _this6 = this;

      if (this.user.subjmatter !== "") {
        return this.topics.filter(function (item) {
          if (parseInt(item.sub_brand_category_id) === parseInt(_this6.user.subjmatter)) {
            return item;
          }
        });
      }
    },

    clUrl: function clUrl() {
      return "https://api.cloudinary.com/v1_1/" + this.cloudinary.cloudName + "/upload";
    }
  }
});

/***/ }),

/***/ 968:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      staticClass: "registerStyle",
      on: {
        submit: function($event) {
          $event.preventDefault()
          return _vm.register($event)
        }
      }
    },
    [
      _vm.$route.name == "VendorHome"
        ? _c(
            "router-link",
            { staticClass: "back", attrs: { to: "/vendor/home" } },
            [
              _c("i", { staticClass: "fas fa-long-arrow-alt-left" }, [
                _vm._v("Back to Home")
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.firstForm,
              expression: "firstForm"
            }
          ]
        },
        [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "vendorEmailAddress" } }, [
              _vm._v("Email")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.user.email,
                  expression: "user.email"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "email",
                id: "vendorEmailAddress",
                placeholder: "example@emal.com",
                required: ""
              },
              domProps: { value: _vm.user.email },
              on: {
                change: _vm.checkEmail,
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.user, "email", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "vendorPassword" } }, [
              _vm._v("Password")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.user.password,
                  expression: "user.password"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "password",
                id: "vendorPassword",
                placeholder: "********",
                min: "6",
                required: ""
              },
              domProps: { value: _vm.user.password },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.user, "password", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: {
                  type: "button",
                  disabled:
                    _vm.user.userSwitch === "" ||
                    _vm.user.password === "" ||
                    _vm.user.email === ""
                },
                on: {
                  click: function($event) {
                    return _vm.switchTab("secondForm")
                  }
                }
              },
              [_vm._v("Next")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.secondForm,
              expression: "secondForm"
            }
          ]
        },
        [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "username" } }, [_vm._v("Username")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.user.username,
                  expression: "user.username"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "username" },
              domProps: { value: _vm.user.username },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.user, "username", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _vm.user.userSwitch === "expert"
            ? _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Bio")]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.user.bio,
                      expression: "user.bio"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { maxlength: "250", placeholder: "250 characters *" },
                  domProps: { value: _vm.user.bio },
                  on: {
                    change: _vm.checkBio,
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.user, "bio", $event.target.value)
                    }
                  }
                })
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", [
              _vm._v("\n        Profile Pic\n        "),
              _vm.percentage
                ? _c("span", [_vm._v(_vm._s(_vm.percentage))])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "profileimageDiv" }, [
                _c("img", {
                  staticClass: "profileImage",
                  attrs: { src: _vm.imagePlaceholder, height: "50" }
                })
              ]),
              _vm._v(" "),
              _c("input", {
                staticClass: "hide-input",
                attrs: { type: "file" },
                on: {
                  change: function($event) {
                    return _vm.getFileName($event, $event.target.files, "image")
                  }
                }
              })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.switchTab("firstForm")
                  }
                }
              },
              [_vm._v("Previous")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { type: "button", disabled: _vm.user.username === "" },
                on: {
                  click: function($event) {
                    return _vm.switchTab("thirdForm")
                  }
                }
              },
              [_vm._v("Next")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.thirdForm,
              expression: "thirdForm"
            }
          ]
        },
        [
          _c("legend", { staticClass: "mb-4" }, [_vm._v("Area of Expertise")]),
          _vm._v(" "),
          _vm.storeName_error
            ? _c("small", { staticClass: "error" }, [
                _vm._v("Expert name already exist")
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.subjectMatter.length > 0
            ? _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Select Subject Matter")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.subjmatter,
                        expression: "user.subjmatter"
                      }
                    ],
                    staticClass: "form-control",
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "subjmatter",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c(
                      "option",
                      { attrs: { disabled: "", value: "default" } },
                      [_vm._v("Choose")]
                    ),
                    _vm._v(" "),
                    _vm._l(_vm.subjectMatter, function(subjectMatters) {
                      return _c(
                        "option",
                        {
                          key: subjectMatters.id,
                          domProps: { value: subjectMatters.id }
                        },
                        [_vm._v(_vm._s(subjectMatters.name))]
                      )
                    })
                  ],
                  2
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.topics.length > 0
            ? _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Select Topic(s)")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.topic,
                        expression: "user.topic"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { multiple: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.user,
                          "topic",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { disabled: "", value: "" } }, [
                      _vm._v("Choose")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.filterTopicByMatter, function(topic) {
                      return _c(
                        "option",
                        { key: topic.id, domProps: { value: topic.id } },
                        [_vm._v(_vm._s(topic.name))]
                      )
                    })
                  ],
                  2
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.switchTab("secondForm")
                  }
                }
              },
              [_vm._v("Previous")]
            ),
            _vm._v(" "),
            _c(
              "button",
              { staticClass: "btn btn-success", attrs: { type: "submit" } },
              [
                _vm._v("\n        Register\n        "),
                _vm.show
                  ? _c("span", {
                      staticClass: "spinner-border spinner-border-sm",
                      attrs: { role: "status" }
                    })
                  : _vm._e()
              ]
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3862c4b7", module.exports)
  }
}

/***/ })

});