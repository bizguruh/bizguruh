webpackJsonp([134],{

/***/ 1613:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1614);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2ba22fee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2d153976\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./partnerProfileComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2d153976\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./partnerProfileComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1614:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-2d153976] {\n  margin-top: 60px;\n  padding-bottom: 120px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n}\nul[data-v-2d153976],\nol[data-v-2d153976] {\n  list-style: none;\n}\n.title[data-v-2d153976]{\n  width:100%;\n}\na[data-v-2d153976] {\n  color: hsl(207, 43%, 20%) !important;\n}\na[data-v-2d153976]:hover {\n  color: rgba(0, 0, 0, 0.64) !important;\n  text-decoration: underline !important;\n}\nh2[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.84);\n}\nh4[data-v-2d153976] {\n  margin-bottom: 10px;\n  color: rgba(0, 0, 0, 0.6);\n}\n.subject[data-v-2d153976] {\n  position: absolute;\n  bottom: 10px;\n  left: 35px;\n  color: hsl(207, 43%, 20%);\n  font-size: 14px;\n  text-transform: capitalize;\n  line-height: 1.2;\n}\n.subject a[data-v-2d153976] {\n  color: hsl(207, 43%, 20%) !important;\n}\n.miniAbout[data-v-2d153976] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 15px 0;\n}\n.joinDate[data-v-2d153976] {\n  font-size: 11px;\n  color: rgba(0, 0, 0, 0.54);\n}\n.vendor[data-v-2d153976] {\n  font-size: 14px;\n  color: rgba(0, 0, 0, 0.64);\n  padding-bottom: 10px;\n  text-transform: capitalize;\n}\n.imgCover[data-v-2d153976] {\n  width: 40px;\n  height: 40px;\n  overflow: hidden;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n.imgCover img[data-v-2d153976] {\n  height: 100%;\n  width: 100%;\n}\n.partnerName[data-v-2d153976] {\n  text-transform: capitalize;\n}\n.tabs[data-v-2d153976] {\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  border-bottom: 1px solid #f7f8fa;\n}\n.tab[data-v-2d153976] {\n  padding: 2px 30px;\n  color: rgba(0, 0, 0, 0.54);\n  cursor: pointer;\n}\n.tab[data-v-2d153976]:hover {\n  color: rgba(0, 0, 0, 0.74);\n}\n.img_text h3[data-v-2d153976] {\n  margin-bottom: 2px;\n}\n.activeTab[data-v-2d153976] {\n  background: #ffffff;\n  border-bottom: 1px solid #000000;\n  color: rgba(0, 0, 0, 0.84);\n  border-radius: 5px 5px 0 0;\n}\n.spark[data-v-2d153976] {\n  float: right;\n}\n.follow[data-v-2d153976] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.swiper-container[data-v-2d153976] {\n  height: auto !important;\n  margin-left: auto;\n  margin-right: auto;\n}\n/* .swiper-slide {\n\theight: 200px;\n} */\nh3[data-v-2d153976] {\n  margin-bottom: 20px;\n}\n.popular_box[data-v-2d153976] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  background: #f7f8fa;\n  margin-bottom: 40px;\n  padding: 25px;\n  -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);\n  border-radius: 3px;\n}\n.tab-content p[data-v-2d153976] {\n  line-height: 1.2;\n}\n.bio[data-v-2d153976] {\n  height: 130px;\n  overflow: hidden;\n}\n.aboutCover[data-v-2d153976] {\n  width: 90%;\n}\n.img_text[data-v-2d153976] {\n  width: 100%;\n  height: 100%;\n  padding: 20px 10px;\n  font-size: 16px;\n  line-height: 1.6 !important;\n}\n.img_cover[data-v-2d153976] {\n  width: 100%;\n  height: 300px;\n}\n.img_cover img[data-v-2d153976] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.vue-tabs.nav-tabs > li > a[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.btn[data-v-2d153976] {\n  padding: 0 8px !important;\n  color: hsl(207, 43%, 20%) !important;\n  text-transform: capitalize !important;\n  font-weight: normal;\n  border: 1px solid hsl(207, 43%, 20%);;\n  border-radius: 3px;\n  cursor: pointer;\n  margin: auto 10px;\n  padding: 2px 14px;\n  text-transform: capitalize;\n\n  /* height: 35px; */\n}\n.btn[data-v-2d153976]:hover {\n  background: rgba(164, 194, 219, 0.5) !important;\n  color: #fff !important;\n}\n.btn-primary[data-v-2d153976] {\n  padding: 10px 20px !important;\n  color: #fff !important;\n  text-transform: capitalize !important;\n  font-weight: normal;\n  text-decoration: none;\n  border-radius: 3px;\n  background: hsl(207, 43%, 20%) !important;\n  cursor: pointer;\n  /* margin: auto ; */\n  padding: 2px 14px;\n  text-transform: capitalize;\n   border:none;\n}\n.btn-primary[data-v-2d153976]:hover {\n  background: rgba(164, 194, 219, 0.5) !important;\n  color: #fff !important;\n}\n.followingVendor[data-v-2d153976] {\n  color: #ffffff !important;\n  background-color: hsl(207, 43%, 20%) !important;\n  font-weight: normal;\n  border: 1px solid hsl(207, 43%, 20%);\n  border-radius: 3px;\n  cursor: pointer;\n  margin: auto 10px;\n  padding: 2px 4px;\n  text-transform: capitalize;\n  height: 25px;\n}\np[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76);\n}\nsmall[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.54);\n}\nli[data-v-2d153976] {\n  font-size: 12px;\n  color: rgba(0, 0, 0, 0.54);\n  padding: 5px;\n}\n.a[data-v-2d153976] {\n  display: block;\n  max-width: 868px;\n  width: 100%;\n  min-height: 200px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-bottom: 30px;\n\n  font-family: \"medium-content-sans-serif-font\", \"-apple-system\",\n    \"BlinkMacSystemFont\", \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell,\n    \"Open Sans\", \"Helvetica Neue\", sans-serif !important;\n}\n.b[data-v-2d153976] {\n  max-width: 868px;\n  width: 100%;\n  height: auto;\n  /* border: 1px solid black; */\n  margin-right: auto;\n  margin-left: auto;\n}\n.aa[data-v-2d153976] {\n  /* border: 1px red dotted; */\n  width: 70%;\n  height: 130px;\n  line-height: 1.6;\n}\n.ac[data-v-2d153976] {\n  display: block;\n  max-width: 868px;\n  width: 100%;\n  margin-right: auto;\n  margin-left: auto;\n  margin-bottom: 30px;\n}\n.ab[data-v-2d153976] {\n  border-top: 3px #8291c0 dotted;\n  border-bottom: 3px #8291c0 dotted;\n  width: 120px;\n  height: 120px;\n  border-radius: 50%;\n  overflow: hidden;\n  float: right;\n}\n.ab img[data-v-2d153976] {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  vertical-align: middle;\n  padding: 5px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.vue-tabs[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\na[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\na.tabs__link[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.title.title_center[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.primary_header[data-v-2d153976] {\n  width: 100%;\n  margin-bottom: 20px;\n  text-align: left;\n  font-weight: bold;\n}\n.bop[data-v-2d153976] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.boxx[data-v-2d153976] {\n  position: relative;\n  width: 24%;\n  height: 200px;\n  padding: 0 !important;\n  margin-right: 1%;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  -webkit-box-shadow: none !important;\n          box-shadow: none !important;\n  border-radius: 5px;\n}\n.boxx img[data-v-2d153976] {\n  width: 100%;\n  height: 100%;\n  border-radius: 5px;\n}\n.shadow[data-v-2d153976] {\n  position: absolute;\n  top: 0;\n  background: -webkit-gradient(linear, left top, right top, from(rgb(0, 0, 0)), to(rgb(79, 79, 79)));\n  background: linear-gradient(to right, rgb(0, 0, 0), rgb(79, 79, 79));\n  background: -webkit-gradient(\n    linear,\n    left top, right top,\n    from(rgb(0, 0, 0, 0.5)),\n    to(rgb(79, 79, 79, 0.5))\n  );\n  background: linear-gradient(\n    to right,\n    rgb(0, 0, 0, 0.5),\n    rgb(79, 79, 79, 0.5)\n  );\n  color: #f7f8fa;\n  width: 100%;\n  padding: 60px 0;\n  height: 200px;\n  text-align: center;\n  border-radius: 5px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n.text[data-v-2d153976] {\n  font-size: calc(9px + (24 - 9) * ((100vw - 300px) / (1600 - 300)));\n  line-height: 1.6;\n}\n.textContent[data-v-2d153976] {\n  color: rgba(0, 0, 0, 0.64);\n  font-weight: normal;\n  height: 25px;\n  overflow: hidden;\n  line-clamp: 1;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  text-overflow: ellipsis;\n  word-wrap: break-word;\n  white-space: normal;\n}\n.partner_name[data-v-2d153976] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n@media (max-width: 425px) {\n.container[data-v-2d153976] {\n    padding: 20px 10px;\n}\n.btn-primary[data-v-2d153976] {\n    font-size: 12px !important;\n    padding: 5px 10px !important;\n}\nh2[data-v-2d153976] {\n    font-size: 18px;\n}\nh4[data-v-2d153976] {\n    font-size: 16px;\n}\n.primary_header[data-v-2d153976]{\n    font-size: 16px;\n}\n.textContent[data-v-2d153976] {\n    margin-bottom: 20px;\n}\n.popular_box[data-v-2d153976] {\n    padding: 10px;\n}\n.tab[data-v-2d153976] {\n    padding: 5px 10px;\n    font-size: 11px;\n}\n.b[data-v-2d153976] {\n    padding: 0;\n}\nh2[data-v-2d153976] {\n    font-size: 1em;\n}\n.tab-content p[data-v-2d153976] {\n    line-height: 1.2;\n    font-size: 0.7em;\n}\n.img_text[data-v-2d153976] {\n    width: 100%;\n    height: 100%;\n    padding: 6px 5px;\n    font-size: 16px;\n    line-height: 1.2 !important;\n}\n.img_text h3[data-v-2d153976] {\n    font-size: 16px;\n}\n.ab[data-v-2d153976] {\n    border-top: 3px hsl(207, 43%, 20%) dotted;\n    border-bottom: 3px hsl(207, 43%, 20%) dotted;\n    width: 80px;\n    height: 80px;\n    border-radius: 50%;\n    overflow: hidden;\n    float: right;\n}\n.nav[data-v-2d153976] {\n    font-size: 11px;\n}\n.vue-tabs .nav > li span.title[data-v-2d153976] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px;\n}\n.vue-tabs .nav li span.title[data-v-2d153976] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px !important;\n}\n.vue-tabs .nav li span .title[data-v-2d153976] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px !important;\n}\n.a[data-v-2d153976] {\n    color: rgba(0, 0, 0, 0.76) !important;\n    padding: 0;\n    min-height: 300px;\n}\n.row .a[data-v-2d153976] {\n    padding: 0 !important;\n}\n.ab[data-v-2d153976] {\n    float: none;\n    width: 120%px;\n    margin-bottom: 10px;\n}\n.aa[data-v-2d153976] {\n    clear: right;\n    width: 100%;\n}\n.boxx[data-v-2d153976] {\n    width: 24%;\n    height: 120px;\n}\n.shadow[data-v-2d153976] {\n    padding: 30px 0;\n    height: 120px;\n}\n.primary_header[data-v-2d153976] {\n    font-size: 16px !important;\n}\n.img_cover[data-v-2d153976] {\n    width: 100%;\n    height: 120px;\n}\n.textContent[data-v-2d153976] {\n    font-size: 16px;\n    line-height: 1.6;\n}\n.subject[data-v-2d153976] {\n    left: 15px;\n}\n.btn[data-v-2d153976] {\n    height: 15px;\n    font-size: 9px;\n    margin: 1px 5px;\n}\n.followingVendor[data-v-2d153976] {\n    height: 15px;\n    margin: 1px 5px;\n}\n.textContent[data-v-2d153976] {\n    font-size: 14px;\n    line-height: 1.6;\n}\nh3[data-v-2d153976]{\n    font-size: 15px;\n}\n.subject[data-v-2d153976] {\n    left: 15px;\n    font-size: 12px;\n}\np[data-v-2d153976]{\n    font-size: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1615:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Partner-Profile-component",
  data: function data() {
    var _ref;

    return _ref = {
      username: this.$route.params.username,
      join: "",
      article: {},
      user: {},
      token: "",
      nonSubscriber: false,
      authenticate: false,
      readCount: 0,
      writterD: {},
      auth: "",
      followNo: 0,
      update: 0,
      vendor: {}
    }, _defineProperty(_ref, "vendor", {}), _defineProperty(_ref, "userFollowing", {}), _defineProperty(_ref, "following", false), _defineProperty(_ref, "followText", "follow"), _defineProperty(_ref, "vendorProducts", []), _defineProperty(_ref, "industries", []), _defineProperty(_ref, "concepts", []), _defineProperty(_ref, "categories", []), _defineProperty(_ref, "articles", false), _defineProperty(_ref, "courses", false), _defineProperty(_ref, "videos", false), _defineProperty(_ref, "podcast", false), _defineProperty(_ref, "research", false), _defineProperty(_ref, "articlesF", false), _defineProperty(_ref, "coursesF", false), _defineProperty(_ref, "videosF", false), _defineProperty(_ref, "podcastF", false), _defineProperty(_ref, "researchF", false), _defineProperty(_ref, "userPreference", []), _defineProperty(_ref, "vendorTopics", []), _defineProperty(_ref, "swiperOption", {
      slidesPerView: 1,
      slidesPerColumn: 3,

      observer: true,
      observeParents: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }), _defineProperty(_ref, "swiperOptions", {
      slidesPerView: 2,
      slidesPerColumn: 3,

      observer: true,
      observeParents: true,
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 30
        }

      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }), _defineProperty(_ref, "openPref", true), _defineProperty(_ref, "openOthers", false), _ref;
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user === null) {
      next({
        name: "auth",
        params: { name: "login" },
        query: { redirectFrom: to.fullPath }
      });
    } else if (user.vendor_user_id === 0) {
      next("/entrepreneur");
    } else {
      next();
    }
  },


  // created() {
  //   axios.get(`/api/vendor/${this.id}`).then(response => {
  //     if (response.status === 200) {
  //       this.vendor = response.data.vendor[0];

  //       this.vendor = response.data.vendor[0];
  //     }
  //   });
  // },

  mounted: function mounted() {
    var _this = this;

    axios.get("/api/vendor/" + this.username.toLowerCase()).then(function (response) {
      if (response.status === 200) {
        _this.vendor = response.data.vendorU[0];

        var user = JSON.parse(localStorage.getItem("authUser"));
        var data = {
          id: _this.vendor.id,
          prodType: "Articles",
          readCount: _this.readCount,
          industries: [],
          concepts: [],
          categories: []
        };

        if (user != null) {
          _this.user = user;
          _this.auth = true;
          _this.token = user.access_token;

          axios.get("/api/user-following/" + data.id, {
            headers: { Authorization: "Bearer " + _this.token }
          }).then(function (response) {
            if (response.status === 200) {
              if (response.data === true) {
                _this.following = true;
                _this.followText = "following";
              } else {
                _this.following = false;
                _this.followText = "follow";
              }
            }
          });
        }

        axios.get("/api/getfollowers/" + data.id).then(function (response) {
          if (response.status === 200) {
            _this.followNo = response.data;
          }
        });

        axios.get("/api/vendor-products/" + _this.vendor.id).then(function (response) {
          if (response.status === 200) {
            _this.vendorProducts = response.data.data.reverse();

            _this.vendorProducts.forEach(function (item) {
              _this.userPreference.forEach(function (element) {
                if (element.typeId === JSON.parse(item.sub_category_brand_id)[0]) {
                  _this.vendorTopics.push(item);
                  _this.vendorTopics.forEach(function (item) {
                    if (item.prodType === "Articles") {
                      _this.articlesF = true;
                    } else if (item.prodType === "Courses") {
                      _this.coursesF = true;
                    } else if (item.prodType === "Videos") {
                      _this.videosF = true;
                    } else if (item.prodType === "Podcast") {
                      _this.podcastF = true;
                    } else if (item.prodType === "Market Research") {
                      _this.researchF = true;
                    } else {}
                  });
                }
              });

              if (item.prodType === "Articles") {
                _this.articles = true;
              } else if (item.prodType === "Courses") {
                _this.courses = true;
              } else if (item.prodType === "Videos") {
                _this.videos = true;
              } else if (item.prodType === "Podcast") {
                _this.podcast = true;
              } else if (item.prodType === "Market Research") {
                _this.research = true;
              } else {}
            });
          }
        });
      }
    });

    axios.get("/api/products").then(function (res) {
      res.data.data.forEach(function (element) {
        var vendor_user_id = Number(_this.vendor.id);
        if (element.vendor_user_id === vendor_user_id) {}
      });
    });
  },


  methods: {
    joinDate: function joinDate(date) {
      var joinDate = new Date(date);
      var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var month = joinDate.getMonth();
      return mlist[month] + " " + joinDate.getFullYear();
    },
    openPrefTab: function openPrefTab() {
      this.openPref = true;
      this.openOthers = false;
    },
    openOtherTab: function openOtherTab() {
      this.openPref = false;
      this.openOthers = true;
    },
    followToggle: function followToggle() {
      this.following ? this.unFollow() : this.follow(this.vendor.id, this.vendor.storeName);
    },
    follow: function follow(params, name) {
      var _this2 = this;

      var data = {
        vendorName: name,
        vendorId: params
      };
      if (this.auth) {
        axios.post("/api/user-following", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 201) {
            _this2.following = true;
            _this2.followText = "following";
            _this2.$toasted.success("Successfully followed");
            _this2.followNo++;
            _this2.forceRender();
          } else if (response.data === "Already following") {
            _this2.$toasted.error("Already following");
          } else {
            _this2.$toasted.error("Error");
          }
        }).catch(function (error) {
          console.log(error);
        });
      } else {
        this.$toasted.error("You have to log in to follow");
      }
    },
    unFollow: function unFollow() {
      var _this3 = this;

      var vendor = this.vendor.id;
      var unfollow = confirm("unfollow " + this.vendor.storeName.toLowerCase() + "?");
      if (unfollow) {
        axios.delete("/api/user-following/" + vendor, {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 200) {
            _this3.following = false;
            _this3.followText = "follow";
            _this3.$toasted.success("Successfully Unfollowed");
            _this3.followNo--;
            _this3.forceRender();
          } else {
            _this3.$toasted.error("Already Unfollowed");
          }
        });
      }
    }
  },

  components: {}
});

/***/ }),

/***/ 1616:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.vendorProducts
    ? _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row a" }, [
          _vm.vendor.valid_id !== null
            ? _c("div", { staticClass: "ab" }, [
                _c("img", {
                  attrs: {
                    src: _vm.vendor.valid_id,
                    alt: _vm.vendor.storeName,
                    srcset: ""
                  }
                })
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.vendor.valid_id === null
            ? _c("div", { staticClass: "ab" }, [
                _c("img", {
                  attrs: { src: "/images/profile.png", alt: "", srcset: "" }
                })
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "aa" }, [
            _c("div", { staticClass: "partner_name mb-2" }, [
              _vm.vendor.storeName
                ? _c("h2", { staticClass: "partnerName mr-3" }, [
                    _vm._v(_vm._s(_vm.vendor.storeName))
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  key: _vm.update,
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white mt-0",
                  attrs: { type: "button" },
                  on: { click: _vm.followToggle }
                },
                [_vm._v(_vm._s(_vm.followText))]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "bio" }, [
              _vm.vendor.bio !== null
                ? _c("p", [_vm._v(_vm._s(_vm.vendor.bio))])
                : _c("p", [_vm._v(_vm._s(_vm.vendor.bio))])
            ]),
            _vm._v(" "),
            _c("div", [
              _c("small", [
                _vm._v(
                  "Member since " +
                    _vm._s(_vm._f("moment")(_vm.vendor.created_at, "MMMM YYYY"))
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", [
              _c("small", [_vm._v(_vm._s(_vm.followNo) + " Follower(s)")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row b" }, [
          _c("div", { staticClass: "title" }, [
            _vm.vendorProducts.length > 0
              ? _c("h2", { staticClass: "primary_header mt-4" }, [
                  _vm._v("Latest")
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.vendorProducts.length === 0
              ? _c("h2", { staticClass: "primary_header mt-4" }, [
                  _vm._v("No available content yet")
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "mt-4" }, [
              _vm.vendorProducts.length === 0
                ? _c(
                    "a",
                    {
                      staticClass: "btn-primary",
                      attrs: {
                        name: "",
                        id: "",
                        href: "mailto:bizguruh@gmail.com",
                        target: "_top",
                        role: "button"
                      }
                    },
                    [_vm._v("Submit content")]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", [
              _c(
                "div",
                { staticClass: "mb-4" },
                [
                  _c(
                    "swiper",
                    {
                      staticClass: "mb-4",
                      attrs: { options: _vm.swiperOption }
                    },
                    [
                      _vm._l(_vm.vendorProducts, function(latest) {
                        return _c("swiper-slide", { key: latest.id }, [
                          latest.prodType === "Courses"
                            ? _c(
                                "div",
                                { staticClass: "popular_box" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "subject" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: { to: "/entrepreneur/courses" }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.prodType.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      ),
                                      _vm._v("/\n                    "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Industry",
                                              params: {
                                                id: latest.industry.id,
                                                name: latest.industry.name.replace(
                                                  / /g,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.industry.name.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "miniAbout" }, [
                                    _c("div", { staticClass: "imgCover" }, [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.vendor.valid_id,
                                          alt: _vm.vendor.storeName,
                                          srcset: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "aboutCover" }, [
                                      _c(
                                        "div",
                                        { staticClass: "vendor mr-2" },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.vendor.storeName.toLowerCase()
                                              ) +
                                              " in\n                        "
                                          ),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SubjectMatters",
                                                  params: {
                                                    id: latest.subjectMatter.id,
                                                    name: latest.subjectMatter.name
                                                      .toLowerCase()
                                                      .replace(/ /g, "-")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.subjectMatter.name.toLowerCase()
                                                )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "joinDate" }, [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("moment")(
                                              latest.courses.created_at,
                                              "dddd, MMMM Do YYYY"
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "spark" }, [
                                      _c("img", {
                                        attrs: {
                                          src: "/images/spark.png",
                                          alt: "",
                                          height: "30",
                                          width: "35"
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "routerlink",
                                      attrs: {
                                        to: {
                                          name: "CourseFullPage",
                                          params: {
                                            id: latest.courses.product_id,
                                            name: latest.courses.title.replace(
                                              / /g,
                                              "-"
                                            )
                                          }
                                        },
                                        tag: "a"
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "img_cover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: latest.coverImage,
                                            alt: latest.courses.title,
                                            srcset: ""
                                          }
                                        })
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "img_text" }, [
                                    _c("h3", [
                                      _vm._v(_vm._s(latest.courses.title))
                                    ]),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "textContent" }, [
                                      _vm._v(_vm._s(latest.courses.overview))
                                    ])
                                  ])
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          latest.prodType === "Articles"
                            ? _c(
                                "div",
                                { staticClass: "popular_box" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "subject" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: "/entrepreneur/articles"
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.prodType.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      ),
                                      _vm._v("/\n                    "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Industry",
                                              params: {
                                                id: latest.industry.id,
                                                name: latest.industry.name.replace(
                                                  / /g,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.industry.name.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "miniAbout" }, [
                                    _c("div", { staticClass: "imgCover" }, [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.vendor.valid_id,
                                          alt: _vm.vendor.storeName,
                                          srcset: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "aboutCover" }, [
                                      _c(
                                        "div",
                                        { staticClass: "vendor mr-2" },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.vendor.storeName.toLowerCase()
                                              ) +
                                              " in\n                        "
                                          ),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SubjectMatters",
                                                  params: {
                                                    id: latest.subjectMatter.id,
                                                    name: latest.subjectMatter.name
                                                      .toLowerCase()
                                                      .replace(/ /g, "-")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.subjectMatter.name.toLowerCase()
                                                )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "joinDate" }, [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("moment")(
                                              latest.articles.created_at,
                                              "dddd, MMMM Do YYYY"
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "spark" }, [
                                      _c("img", {
                                        attrs: {
                                          src: "/images/spark.png",
                                          alt: "",
                                          height: "30",
                                          width: "35"
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id: latest.articles.product_id,
                                            name: latest.articles.title
                                              .replace(/[^\w\s]/gi, "")
                                              .replace(/ /g, "-")
                                              .replace(/\?/g, "-")
                                              .replace(/\!/g, "")
                                              .replace(/\$/g, "")
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "img_cover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: latest.coverImage,
                                            alt: latest.articles.title,
                                            srcset: ""
                                          }
                                        })
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "img_text" }, [
                                    _c("h3", [
                                      _vm._v(_vm._s(latest.articles.title))
                                    ]),
                                    _vm._v(" "),
                                    _c("p", {
                                      staticClass: "textContent",
                                      domProps: {
                                        innerHTML: _vm._s(
                                          latest.articles.description
                                        )
                                      }
                                    })
                                  ])
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          latest.prodType === "Videos"
                            ? _c(
                                "div",
                                { staticClass: "popular_box" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "subject" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: { to: "/entrepreneur/videos" }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.prodType.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      ),
                                      _vm._v("/\n                    "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Industry",
                                              params: {
                                                id: latest.industry.id,
                                                name: latest.industry.name.replace(
                                                  / /g,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.industry.name.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "miniAbout" }, [
                                    _c("div", { staticClass: "imgCover" }, [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.vendor.valid_id,
                                          alt: _vm.vendor.storeName,
                                          srcset: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "aboutCover" }, [
                                      _c(
                                        "div",
                                        { staticClass: "vendor mr-2" },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.vendor.storeName.toLowerCase()
                                              ) +
                                              " in\n                        "
                                          ),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SubjectMatters",
                                                  params: {
                                                    id: latest.subjectMatter.id,
                                                    name: latest.subjectMatter.name
                                                      .toLowerCase()
                                                      .replace(/ /g, "-")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.subjectMatter.name.toLowerCase()
                                                )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "joinDate" }, [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("moment")(
                                              latest.webinar.created_at,
                                              "dddd, MMMM Do YYYY"
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "spark" }, [
                                      _c("img", {
                                        attrs: {
                                          src: "/images/spark.png",
                                          alt: "",
                                          height: "30",
                                          width: "35"
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "Video",
                                          params: {
                                            id: latest.webinar.product_id
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "img_cover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: latest.coverImage,
                                            alt: latest.webinar.title,
                                            srcset: ""
                                          }
                                        })
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "img_text" }, [
                                    _c("h3", [
                                      _vm._v(_vm._s(latest.webinar.title))
                                    ]),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "textContent" }, [
                                      _vm._v(_vm._s(latest.webinar.description))
                                    ])
                                  ])
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          latest.prodType === "Podcast"
                            ? _c(
                                "div",
                                { staticClass: "popular_box" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "subject" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: { to: "/entrepreneur/podcast" }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.prodType.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      ),
                                      _vm._v("/\n                    "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Industry",
                                              params: {
                                                id: latest.industry.id,
                                                name: latest.industry.name.replace(
                                                  / /g,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.industry.name.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "miniAbout" }, [
                                    _c("div", { staticClass: "imgCover" }, [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.vendor.valid_id,
                                          alt: _vm.vendor.storeName,
                                          srcset: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "aboutCover" }, [
                                      _c(
                                        "div",
                                        { staticClass: "vendor mr-2" },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.vendor.storeName.toLowerCase()
                                              ) +
                                              " in\n                        "
                                          ),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SubjectMatters",
                                                  params: {
                                                    id: latest.subjectMatter.id,
                                                    name: latest.subjectMatter.name
                                                      .toLowerCase()
                                                      .replace(/ /g, "-")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.subjectMatter.name.toLowerCase()
                                                )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "joinDate" }, [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("moment")(
                                              latest.webinarVideo.created_at,
                                              "dddd, MMMM Do YYYY"
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "spark" }, [
                                      _c("img", {
                                        attrs: {
                                          src: "/images/spark.png",
                                          alt: "",
                                          height: "30",
                                          width: "35"
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "SinglePodcastPage",
                                          params: {
                                            id: latest.webinarVideo.product_id
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "img_cover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: latest.coverImage,
                                            alt: "latest.webinarVideo.title",
                                            srcset: ""
                                          }
                                        })
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "img_text" }, [
                                    _c("h3", [
                                      _vm._v(_vm._s(latest.webinarVideo.title))
                                    ]),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "textContent" }, [
                                      _vm._v(
                                        _vm._s(latest.webinarVideo.description)
                                      )
                                    ])
                                  ])
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          latest.prodType === "Market Research"
                            ? _c(
                                "div",
                                { staticClass: "popular_box" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "subject" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "WhitePaperAdmin",
                                              params: {
                                                name: "market-research"
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.prodType.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      ),
                                      _vm._v("/\n                    "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Industry",
                                              params: {
                                                id: latest.industry.id,
                                                name: latest.industry.name.replace(
                                                  / /g,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                latest.industry.name.toLowerCase()
                                              )
                                            )
                                          ])
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "miniAbout" }, [
                                    _c("div", { staticClass: "imgCover" }, [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.vendor.valid_id,
                                          alt: _vm.vendor.storeName,
                                          srcset: ""
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "aboutCover" }, [
                                      _c(
                                        "div",
                                        { staticClass: "vendor mr-2" },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                _vm.vendor.storeName.toLowerCase()
                                              ) +
                                              " in\n                        "
                                          ),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SubjectMatters",
                                                  params: {
                                                    id: latest.subjectMatter.id,
                                                    name: latest.subjectMatter.name
                                                      .toLowerCase()
                                                      .replace(/ /g, "-")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.subjectMatter.name.toLowerCase()
                                                )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "joinDate" }, [
                                        _vm._v(
                                          _vm._s(
                                            _vm._f("moment")(
                                              latest.marketResearch.created_at,
                                              "dddd, MMMM Do YYYY"
                                            )
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "spark" }, [
                                      _c("img", {
                                        attrs: {
                                          src: "/images/spark.png",
                                          alt: "",
                                          height: "30",
                                          width: "35"
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ProductPaperDetail",
                                          params: {
                                            id:
                                              latest.marketResearch.product_id,
                                            name: (latest.marketResearch.title
                                              ? latest.marketResearch.title
                                              : latest.marketResearch
                                                  .articleTitle
                                            )
                                              .replace(/\s+/g, "-")
                                              .toLowerCase(),
                                            type: "subscribe"
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "img_cover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: latest.coverImage,
                                            alt: latest.marketResearch.title,
                                            srcset: ""
                                          }
                                        })
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "img_text" }, [
                                    _c("h3", [
                                      _vm._v(
                                        _vm._s(latest.marketResearch.title)
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("p", { staticClass: "textContent" }, [
                                      _vm._v(
                                        _vm._s(latest.marketResearch.overview)
                                      )
                                    ])
                                  ])
                                ],
                                1
                              )
                            : _vm._e()
                        ])
                      }),
                      _vm._v(" "),
                      _c("div", {
                        staticClass: "swiper-pagination",
                        attrs: { slot: "pagination" },
                        slot: "pagination"
                      })
                    ],
                    2
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _vm.vendorProducts.length > 0
              ? _c("h2", { staticClass: "mb-4 mt-4" }, [_vm._v("Featured")])
              : _vm._e(),
            _vm._v(" "),
            _vm.articles
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "mb-4" },
                    [
                      _c("h4", [_vm._v("Articles")]),
                      _vm._v(" "),
                      _c(
                        "swiper",
                        {
                          staticClass: "mb-4",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.vendorProducts, function(latest) {
                            return _c("swiper-slide", { key: latest.id }, [
                              latest.prodType === "Articles"
                                ? _c(
                                    "div",
                                    { staticClass: "popular_box" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "subject" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: "/entrepreneur/articles"
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.prodType.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v("/\n                    "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "Industry",
                                                  params: {
                                                    id: latest.industry.id,
                                                    name: latest.industry.name.replace(
                                                      / /g,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.industry.name.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "miniAbout" }, [
                                        _c("div", { staticClass: "imgCover" }, [
                                          _c("img", {
                                            attrs: {
                                              src: _vm.vendor.valid_id,
                                              alt: _vm.vendor.storeName,
                                              srcset: ""
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "aboutCover" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "vendor mr-2" },
                                              [
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.vendor.storeName.toLowerCase()
                                                    ) +
                                                    " in\n                        "
                                                ),
                                                _c(
                                                  "router-link",
                                                  {
                                                    attrs: {
                                                      to: {
                                                        name: "SubjectMatters",
                                                        params: {
                                                          id:
                                                            latest.subjectMatter
                                                              .id,
                                                          name: latest.subjectMatter.name
                                                            .toLowerCase()
                                                            .replace(/ /g, "-")
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.subjectMatter.name.toLowerCase()
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "joinDate" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      latest.articles
                                                        .created_at,
                                                      "dddd, MMMM Do YYYY"
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "ArticleSinglePage",
                                              params: {
                                                id: latest.articles.product_id,
                                                name: latest.articles.title
                                                  .replace(/[^\w\s]/gi, "")
                                                  .replace(/ /g, "-")
                                                  .replace(/\?/g, "-")
                                                  .replace(/\!/g, "")
                                                  .replace(/\$/g, "")
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "img_cover" },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src: latest.coverImage,
                                                  alt: latest.articles.title,
                                                  srcset: ""
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "img_text" }, [
                                        _c("h3", [
                                          _vm._v(_vm._s(latest.articles.title))
                                        ]),
                                        _vm._v(" "),
                                        _c("p", {
                                          staticClass: "textContent",
                                          domProps: {
                                            innerHTML: _vm._s(
                                              latest.articles.description
                                            )
                                          }
                                        })
                                      ])
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ])
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "swiper-pagination",
                            attrs: { slot: "pagination" },
                            slot: "pagination"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.courses
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "mb-4" },
                    [
                      _c("h4", [_vm._v("Courses")]),
                      _vm._v(" "),
                      _c(
                        "swiper",
                        {
                          staticClass: "mb-4",
                          attrs: { options: _vm.swiperOptions }
                        },
                        [
                          _vm._l(_vm.vendorProducts, function(latest) {
                            return _c("swiper-slide", { key: latest.id }, [
                              latest.prodType === "Courses"
                                ? _c(
                                    "div",
                                    { staticClass: "popular_box" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "subject" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: "/entrepreneur/courses"
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.prodType.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v("/\n                    "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "Industry",
                                                  params: {
                                                    id: latest.industry.id,
                                                    name: latest.industry.name.replace(
                                                      / /g,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.industry.name.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "miniAbout" }, [
                                        _c("div", { staticClass: "imgCover" }, [
                                          _c("img", {
                                            attrs: {
                                              src: _vm.vendor.valid_id,
                                              alt: _vm.vendor.storeName,
                                              srcset: ""
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "aboutCover" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "vendor mr-2" },
                                              [
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.vendor.storeName.toLowerCase()
                                                    ) +
                                                    " in\n                        "
                                                ),
                                                _c(
                                                  "router-link",
                                                  {
                                                    attrs: {
                                                      to: {
                                                        name: "SubjectMatters",
                                                        params: {
                                                          id:
                                                            latest.subjectMatter
                                                              .id,
                                                          name: latest.subjectMatter.name
                                                            .toLowerCase()
                                                            .replace(/ /g, "-")
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.subjectMatter.name.toLowerCase()
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "joinDate" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      latest.courses.created_at,
                                                      "dddd, MMMM Do YYYY"
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "routerlink",
                                          attrs: {
                                            to: {
                                              name: "CourseFullPage",
                                              params: {
                                                id: latest.courses.product_id,
                                                name: latest.courses.title.replace(
                                                  / /g,
                                                  "-"
                                                )
                                              }
                                            },
                                            tag: "a"
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "img_cover" },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src: latest.coverImage,
                                                  alt: latest.courses.title,
                                                  srcset: ""
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "img_text" }, [
                                        _c("h3", [
                                          _vm._v(_vm._s(latest.courses.title))
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "textContent" },
                                          [
                                            _vm._v(
                                              _vm._s(latest.courses.overview)
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ])
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "swiper-pagination",
                            attrs: { slot: "pagination" },
                            slot: "pagination"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.videos
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "mb-4" },
                    [
                      _c("h4", [_vm._v("Videos")]),
                      _vm._v(" "),
                      _c(
                        "swiper",
                        {
                          staticClass: "mb-4",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.vendorProducts, function(latest) {
                            return _c("swiper-slide", { key: latest.id }, [
                              latest.prodType === "Videos"
                                ? _c(
                                    "div",
                                    { staticClass: "popular_box" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "subject" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: "/entrepreneur/videos"
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.prodType.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v("/\n                    "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "Industry",
                                                  params: {
                                                    id: latest.industry.id,
                                                    name: latest.industry.name.replace(
                                                      / /g,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.industry.name.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "miniAbout" }, [
                                        _c("div", { staticClass: "imgCover" }, [
                                          _c("img", {
                                            attrs: {
                                              src: _vm.vendor.valid_id,
                                              alt: _vm.vendor.storeName,
                                              srcset: ""
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "aboutCover" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "vendor mr-2" },
                                              [
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.vendor.storeName.toLowerCase()
                                                    ) +
                                                    " in\n                        "
                                                ),
                                                _c(
                                                  "router-link",
                                                  {
                                                    attrs: {
                                                      to: {
                                                        name: "SubjectMatters",
                                                        params: {
                                                          id:
                                                            latest.subjectMatter
                                                              .id,
                                                          name: latest.subjectMatter.name
                                                            .toLowerCase()
                                                            .replace(/ /g, "-")
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.subjectMatter.name.toLowerCase()
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "joinDate" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      latest.webinar.created_at,
                                                      "dddd, MMMM Do YYYY"
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "Video",
                                              params: {
                                                id: latest.webinar.product_id
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "img_cover" },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src: latest.coverImage,
                                                  alt: latest.webinar.title,
                                                  srcset: ""
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "img_text" }, [
                                        _c("h3", [
                                          _vm._v(_vm._s(latest.webinar.title))
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "textContent" },
                                          [
                                            _vm._v(
                                              _vm._s(latest.webinar.description)
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ])
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "swiper-pagination",
                            attrs: { slot: "pagination" },
                            slot: "pagination"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.podcast
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "mb-4" },
                    [
                      _c("h4", [_vm._v("Podcasts")]),
                      _vm._v(" "),
                      _c(
                        "swiper",
                        {
                          staticClass: "mb-4",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.vendorProducts, function(latest) {
                            return _c("swiper-slide", { key: latest.id }, [
                              latest.prodType === "Podcast"
                                ? _c(
                                    "div",
                                    { staticClass: "popular_box" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "subject" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: "/entrepreneur/podcast"
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.prodType.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v("/\n                    "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "Industry",
                                                  params: {
                                                    id: latest.industry.id,
                                                    name: latest.industry.name.replace(
                                                      / /g,
                                                      ""
                                                    )
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.industry.name.toLowerCase()
                                                  )
                                                )
                                              ])
                                            ]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "miniAbout" }, [
                                        _c("div", { staticClass: "imgCover" }, [
                                          _c("img", {
                                            attrs: {
                                              src: _vm.vendor.valid_id,
                                              alt: _vm.vendor.storeName,
                                              srcset: ""
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "aboutCover" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "vendor mr-2" },
                                              [
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.vendor.storeName.toLowerCase()
                                                    ) +
                                                    " in\n                        "
                                                ),
                                                _c(
                                                  "router-link",
                                                  {
                                                    attrs: {
                                                      to: {
                                                        name: "SubjectMatters",
                                                        params: {
                                                          id:
                                                            latest.subjectMatter
                                                              .id,
                                                          name: latest.subjectMatter.name
                                                            .toLowerCase()
                                                            .replace(/ /g, "-")
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.subjectMatter.name.toLowerCase()
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "joinDate" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      latest.webinarVideo
                                                        .created_at,
                                                      "dddd, MMMM Do YYYY"
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "SinglePodcastPage",
                                              params: {
                                                id:
                                                  latest.webinarVideo.product_id
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "img_cover" },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src: latest.coverImage,
                                                  alt:
                                                    "latest.webinarVideo.title",
                                                  srcset: ""
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "img_text" }, [
                                        _c("h3", [
                                          _vm._v(
                                            _vm._s(latest.webinarVideo.title)
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "textContent" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                latest.webinarVideo.description
                                              )
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ])
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "swiper-pagination",
                            attrs: { slot: "pagination" },
                            slot: "pagination"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.research
              ? _c("div", [
                  _c(
                    "div",
                    { staticClass: "mb-4" },
                    [
                      _c("h4", [_vm._v("Market Research")]),
                      _vm._v(" "),
                      _c(
                        "swiper",
                        {
                          staticClass: "mb-4",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.vendorProducts, function(latest) {
                            return _c("swiper-slide", { key: latest.id }, [
                              latest.prodType === "Market Research"
                                ? _c(
                                    "div",
                                    { staticClass: "popular_box" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "subject" },
                                        [
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "WhitePaperAdmin",
                                                  params: {
                                                    name: "market-research"
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.prodType.toLowerCase()
                                                  )
                                                )
                                              ]),
                                              _vm._v(
                                                " /\n                      "
                                              ),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Industry",
                                                      params: {
                                                        id: latest.industry.id,
                                                        name: latest.industry.name.replace(
                                                          / /g,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.industry.name.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "miniAbout" }, [
                                        _c("div", { staticClass: "imgCover" }, [
                                          _c("img", {
                                            attrs: {
                                              src: _vm.vendor.valid_id,
                                              alt: _vm.vendor.storeName,
                                              srcset: ""
                                            }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "aboutCover" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "vendor mr-2" },
                                              [
                                                _vm._v(
                                                  "\n                        " +
                                                    _vm._s(
                                                      _vm.vendor.storeName.toLowerCase()
                                                    ) +
                                                    " in\n                        "
                                                ),
                                                _c(
                                                  "router-link",
                                                  {
                                                    attrs: {
                                                      to: {
                                                        name: "SubjectMatters",
                                                        params: {
                                                          id:
                                                            latest.subjectMatter
                                                              .id,
                                                          name: latest.subjectMatter.name
                                                            .toLowerCase()
                                                            .replace(/ /g, "-")
                                                        }
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.subjectMatter.name.toLowerCase()
                                                      )
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "joinDate" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm._f("moment")(
                                                      latest.marketResearch
                                                        .created_at,
                                                      "dddd, MMMM Do YYYY"
                                                    )
                                                  )
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "ProductPaperDetail",
                                              params: {
                                                id:
                                                  latest.marketResearch
                                                    .product_id,
                                                name: (latest.marketResearch
                                                  .title
                                                  ? latest.marketResearch.title
                                                  : latest.marketResearch
                                                      .articleTitle
                                                )
                                                  .replace(/\s+/g, "-")
                                                  .toLowerCase(),
                                                type: "subscribe"
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "img_cover" },
                                            [
                                              _c("img", {
                                                attrs: {
                                                  src: latest.coverImage,
                                                  alt:
                                                    latest.marketResearch.title,
                                                  srcset: ""
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "img_text" }, [
                                        _c("h3", [
                                          _vm._v(
                                            _vm._s(latest.marketResearch.title)
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "textContent" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                latest.marketResearch.overview
                                              )
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                : _vm._e()
                            ])
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "swiper-pagination",
                            attrs: { slot: "pagination" },
                            slot: "pagination"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                ])
              : _vm._e()
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2d153976", module.exports)
  }
}

/***/ }),

/***/ 601:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1613)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1615)
/* template */
var __vue_template__ = __webpack_require__(1616)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2d153976"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/partnerProfileComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2d153976", Component.options)
  } else {
    hotAPI.reload("data-v-2d153976", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});