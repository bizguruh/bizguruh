webpackJsonp([103],{

/***/ 1139:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1140);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("19ab3446", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c0bef7b8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProductShippingComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c0bef7b8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProductShippingComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1140:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n#localShippingRates[data-v-c0bef7b8], #localShippingTimes[data-v-c0bef7b8], #intShippingRates[data-v-c0bef7b8], #intShippingTimes[data-v-c0bef7b8], #yourLocation[data-v-c0bef7b8] {\n    width: 50%;\n}\n#addressLineOne[data-v-c0bef7b8], #localShippingRates[data-v-c0bef7b8], #intShippingRates[data-v-c0bef7b8], #deliveryInfo[data-v-c0bef7b8] {\n    margin-top: 150px;\n}\n.shippingDetails[data-v-c0bef7b8] {\n    padding: 10px;\n    background: #ffffff;\n}\n.shippingDetails div[data-v-c0bef7b8] {\n    margin-bottom: 50px;\n}\n.biz-submit[data-v-c0bef7b8] {\n    padding: 20px 40px;\n    font-size: 20px;\n}\n.butt[data-v-c0bef7b8] {\n    text-align: center;\n    margin-top: 40px;\n}\ninput[data-v-c0bef7b8], textarea[data-v-c0bef7b8] {\n    border-top: 0 solid green !important;\n    border-left: 0 solid green !important;\n    border-right: 0 solid green !important;\n}\ninput[data-v-c0bef7b8], textarea[data-v-c0bef7b8] {\n    border-bottom: 1px solid #d2d6de;\n}\ninput[data-v-c0bef7b8]::-webkit-input-placeholder, textarea[data-v-c0bef7b8]::-webkit-input-placeholder  {\n    color: #dbdbdb !important;\n    font-weight: bold;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "add-product-shipping-component",
    data: function data() {
        return {
            shippingDetails: {
                id: this.$route.params.id,
                location: '',
                addressOne: '',
                addressTwo: '',
                state: '',
                country: '',
                localShippingRates: '',
                localShippingTime: '',
                intlShippingRates: '',
                intlShippingTime: '',
                deliveryInformation: '',
                returnPolicy: '',
                extraInformation: ''
            },
            token: '',
            shippingDetail: {}
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authVendor')) {
            var authVendor = JSON.parse(localStorage.getItem('authVendor'));
            this.token = authVendor.access_token;
            axios.get('api/shipping-detail-vendor', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {

                if (response.status === 200) {

                    _this.shippingDetail = response.data;
                    _this.shippingDetails.location = _this.shippingDetail.location;
                    _this.shippingDetails.addressOne = _this.shippingDetail.addressOne;
                    _this.shippingDetails.addressTwo = _this.shippingDetail.addressTwo;
                    _this.shippingDetails.state = _this.shippingDetail.state;
                    _this.shippingDetails.country = _this.shippingDetail.country;
                    _this.shippingDetails.localShippingRates = _this.shippingDetail.localShippingRates;
                    _this.shippingDetails.localShippingTime = _this.shippingDetail.localShippingTime;
                    _this.shippingDetails.intlShippingRates = _this.shippingDetail.intlShippingRates;
                    _this.shippingDetails.intlShippingTime = _this.shippingDetail.intlShippingTime;
                    _this.shippingDetails.deliveryInformation = _this.shippingDetail.deliveryInformation;
                    _this.shippingDetails.returnPolicy = _this.shippingDetail.returnPolicy;
                    _this.shippingDetails.extraInformation = _this.shippingDetail.extraInformation;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
        axios.get('../js/countries.json').then(function (response) {
            console.log(JSON.parse(JSON.stringify(response.data)));
        });
    },

    methods: {
        addProduct: function addProduct() {
            var _this2 = this;

            axios.post('api/shipping-detail', JSON.parse(JSON.stringify(this.shippingDetails)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                console.log(_this2.shippingDetails);
                console.log(response);
                if (response.status === 201) {
                    _this2.$toasted.success("Shipping Detail Successfully Added");
                } else if (response.status === 200) {
                    _this2.$toasted.success("Shipping Detail Successfully Updated");
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),

/***/ 1142:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "form-grids row widget-shadow" }, [
      _c("h2", [_vm._v("Shipping Detail")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 shippingDetails" }, [
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.location,
                expression: "shippingDetails.location"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "yourLocation",
              placeholder: "Select Your Location"
            },
            domProps: { value: _vm.shippingDetails.location },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "location", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.addressOne,
                expression: "shippingDetails.addressOne"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "addressLineOne",
              placeholder: "Address Line 1"
            },
            domProps: { value: _vm.shippingDetails.addressOne },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "addressOne", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.addressTwo,
                expression: "shippingDetails.addressTwo"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "addressLineTwo",
              placeholder: "Address Line 2"
            },
            domProps: { value: _vm.shippingDetails.addressTwo },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "addressTwo", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.state,
                expression: "shippingDetails.state"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", id: "state", placeholder: "State" },
            domProps: { value: _vm.shippingDetails.state },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "state", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.country,
                expression: "shippingDetails.country"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", id: "state", placeholder: "State" },
            domProps: { value: _vm.shippingDetails.country },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.shippingDetails, "country", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.localShippingRates,
                expression: "shippingDetails.localShippingRates"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "localShippingRates",
              placeholder: "Enter Local Shipping Rates"
            },
            domProps: { value: _vm.shippingDetails.localShippingRates },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "localShippingRates",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.localShippingTime,
                expression: "shippingDetails.localShippingTime"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "localShippingTimes",
              placeholder: "Enter Local Shipping Times"
            },
            domProps: { value: _vm.shippingDetails.localShippingTime },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "localShippingTime",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.intlShippingRates,
                expression: "shippingDetails.intlShippingRates"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "intShippingRates",
              placeholder: "Enter International Shipping Rates"
            },
            domProps: { value: _vm.shippingDetails.intlShippingRates },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "intlShippingRates",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.intlShippingTime,
                expression: "shippingDetails.intlShippingTime"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "intShippingTimes",
              placeholder: "Enter International Shipping Times"
            },
            domProps: { value: _vm.shippingDetails.intlShippingTime },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "intlShippingTime",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.deliveryInformation,
                expression: "shippingDetails.deliveryInformation"
              }
            ],
            staticClass: "form-control",
            attrs: {
              rows: "10",
              id: "deliveryInfo",
              placeholder: "Delivery Information (Max 500 words)"
            },
            domProps: { value: _vm.shippingDetails.deliveryInformation },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "deliveryInformation",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.returnPolicy,
                expression: "shippingDetails.returnPolicy"
              }
            ],
            staticClass: "form-control",
            attrs: { rows: "10", placeholder: "Return Policy (Max 500 words)" },
            domProps: { value: _vm.shippingDetails.returnPolicy },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "returnPolicy",
                  $event.target.value
                )
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.shippingDetails.extraInformation,
                expression: "shippingDetails.extraInformation"
              }
            ],
            staticClass: "form-control",
            attrs: {
              rows: "10",
              placeholder: "Extra Comments (Max 500 words)"
            },
            domProps: { value: _vm.shippingDetails.extraInformation },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.shippingDetails,
                  "extraInformation",
                  $event.target.value
                )
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 butt" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-submit biz-submit",
            on: {
              click: function($event) {
                return _vm.addProduct()
              }
            }
          },
          [_vm._v("Submit")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c0bef7b8", module.exports)
  }
}

/***/ }),

/***/ 502:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1139)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1141)
/* template */
var __vue_template__ = __webpack_require__(1142)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c0bef7b8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/ProductShippingComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c0bef7b8", Component.options)
  } else {
    hotAPI.reload("data-v-c0bef7b8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});