webpackJsonp([174],{

/***/ 1241:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1242);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("78092980", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f231daf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminCreateOpsComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f231daf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminCreateOpsComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1242:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-0f231daf] {\n    height: 100vh;\n}\n.container[data-v-0f231daf] {\n       width: 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ 1243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-create-ops-component",
    data: function data() {
        return {
            fade: true,
            modal: true,
            user: {
                email: "",
                password: "",
                password_confirmation: "",
                token: ''
            },
            ops: [],
            disable: 'Disable'

        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authAdmin')) {
            var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
            this.token = authAdmin.access_token;
            axios.get('/api/admin/add/ops', { headers: { "Authorization": "Bearer " + authAdmin.access_token } }).then(function (response) {
                console.log(response.data);
                _this.ops = response.data;
                _this.vendors = response.data;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        showModal: function showModal() {
            this.fade = false;
        },
        registerOpsUser: function registerOpsUser() {
            var _this2 = this;

            axios.post('/api/admin/ops/register', JSON.parse(JSON.stringify(this.user)), { headers: { "Authorization": "Bearer " + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this2.$toasted.success('Operation user created succesfully');
                    window.location.reload();
                } else {
                    console.log(response.message);
                }
            }).catch(function (error) {
                // console.log(error.data.message);
            });
        },
        changeStatus: function changeStatus(ops) {
            var _this3 = this;

            axios.get("/api/admin/enabledisable/vendor/" + ops, { headers: { "Authorization": "Bearer " + this.token } }).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    _this3.$toasted.success('Status changed');
                    window.location.reload();
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1244:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c(
      "button",
      {
        staticClass: "btn btn-primary",
        attrs: {
          type: "button",
          "data-toggle": "modal",
          "data-target": "#myModal"
        },
        on: {
          click: function($event) {
            return _vm.showModal()
          }
        }
      },
      [_vm._v("\n        Create Operation User\n    ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        class: { modal: _vm.modal, fade: _vm.fade },
        attrs: {
          id: "myModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "myModalLabel"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "email" } }, [_vm._v("Email:")]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.email,
                        expression: "user.email"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "email", id: "email" },
                    domProps: { value: _vm.user.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.user, "email", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "password" } }, [
                    _vm._v("Password:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.password,
                        expression: "user.password"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "password", id: "password" },
                    domProps: { value: _vm.user.password },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.user, "password", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "confirmpassword" } }, [
                    _vm._v("Confirm Password:")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.user.password_confirmation,
                        expression: "user.password_confirmation"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "password", id: "confirmpassword" },
                    domProps: { value: _vm.user.password_confirmation },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.user,
                          "password_confirmation",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", [_vm._v("Access Level")]),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _vm._m(3)
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "modal-footer" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-default",
                    attrs: { type: "button", "data-dismiss": "modal" }
                  },
                  [_vm._v("Close")]
                ),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.registerOpsUser()
                      }
                    }
                  },
                  [_vm._v("Create Ops User")]
                )
              ])
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("table", { staticClass: "table" }, [
        _vm._m(4),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.ops, function(op, index) {
            return _c("tr", [
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(op.email))]),
              _vm._v(" "),
              _c("td", [
                _vm._v(
                  _vm._s(_vm._f("moment")(op.created_at, "dddd, MMMM Do YYYY"))
                )
              ]),
              _vm._v(" "),
              op.verified
                ? _c("td", [
                    _c("span", { staticClass: "label label-success" }, [
                      _vm._v("Verified")
                    ])
                  ])
                : _c("td", [
                    _c("span", { staticClass: "label label-default" }, [
                      _vm._v("Not Verified")
                    ])
                  ]),
              _vm._v(" "),
              op.verified
                ? _c("td", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary btn-sm",
                        on: {
                          click: function($event) {
                            return _vm.changeStatus(op.id)
                          }
                        }
                      },
                      [_vm._v("Disable")]
                    )
                  ])
                : _c("td", [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary btn-sm",
                        on: {
                          click: function($event) {
                            return _vm.changeStatus(op.id)
                          }
                        }
                      },
                      [_vm._v("Activate")]
                    )
                  ])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      ),
      _vm._v(" "),
      _c("h4", { staticClass: "modal-title", attrs: { id: "myModalLabel" } }, [
        _vm._v("Create a New Operation User")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("input", { attrs: { type: "radio", name: "accessLevel" } }),
      _vm._v("One")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("input", { attrs: { type: "radio", name: "accessLevel" } }),
      _vm._v("Two")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("input", { attrs: { type: "radio", name: "accessLevel" } }),
      _vm._v("Three")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("SN")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")]),
        _vm._v(" "),
        _c("th", [_vm._v("Member Since")]),
        _vm._v(" "),
        _c("th", [_vm._v("Status")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f231daf", module.exports)
  }
}

/***/ }),

/***/ 520:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1241)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1243)
/* template */
var __vue_template__ = __webpack_require__(1244)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f231daf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminCreateOpsComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f231daf", Component.options)
  } else {
    hotAPI.reload("data-v-0f231daf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});