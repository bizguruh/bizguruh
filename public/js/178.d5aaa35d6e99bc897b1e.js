webpackJsonp([178],{

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(875)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(877)
/* template */
var __vue_template__ = __webpack_require__(878)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules/bp-vuejs-dropdown/Dropdown.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42ca018e", Component.options)
  } else {
    hotAPI.reload("data-v-42ca018e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 875:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(876);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("bcb0d66c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../css-loader/index.js!../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42ca018e\",\"scoped\":false,\"hasInlineConfig\":true}!../vue-loader/lib/selector.js?type=styles&index=0!./Dropdown.vue", function() {
     var newContent = require("!!../css-loader/index.js!../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42ca018e\",\"scoped\":false,\"hasInlineConfig\":true}!../vue-loader/lib/selector.js?type=styles&index=0!./Dropdown.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 876:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bp-dropdown--sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__btn,\n.bp-dropdown--sub .bp-dropdown__sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__icon {\n    margin-left: auto;\n}\n.bp-dropdown__btn {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    padding: 3px 5px;\n    border: 1px solid #efefef;\n    cursor: pointer;\n    -webkit-transition: background-color .1s ease;\n    transition: background-color .1s ease;\n}\n.bp-dropdown__sub {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bp-dropdown__btn--active {\n    background-color: #eee;\n}\n.bp-dropdown__icon {\n    display: inline-block;\n    width: 15px;\n    height: 15px;\n    overflow: visible;\n    -webkit-transition: -webkit-transform .1s ease;\n    transition: -webkit-transform .1s ease;\n    transition: transform .1s ease;\n    transition: transform .1s ease, -webkit-transform .1s ease;\n}\n.bp-dropdown__icon--spin {\n    width: 12px;\n    height: 12px;\n    -webkit-animation: spin 2s infinite linear;\n            animation: spin 2s infinite linear;\n}\n.bp-dropdown__icon--top {\n    -webkit-transform: rotate(-180deg);\n            transform: rotate(-180deg);\n}\n.bp-dropdown__icon--right {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n}\n.bp-dropdown__icon--bottom {\n    -webkit-transform: rotate(0);\n            transform: rotate(0);\n}\n.bp-dropdown__icon--left {\n    -webkit-transform: rotate(-270deg);\n            transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--top,\n.bp-dropdown__sub--active .bp-dropdown__icon--top {\n    -webkit-transform: rotate(0);\n            transform: rotate(0);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--right,\n.bp-dropdown__sub--active .bp-dropdown__icon--right {\n    -webkit-transform: rotate(-270deg);\n            transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--bottom,\n.bp-dropdown__sub--active .bp-dropdown__icon--bottom {\n    -webkit-transform: rotate(-180deg);\n            transform: rotate(-180deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--left,\n.bp-dropdown__sub--active .bp-dropdown__icon--left {\n    -webkit-transform: rotate(-90deg);\n            transform: rotate(-90deg);\n}\n.bp-dropdown__body {\n    position: fixed;\n    top: 0;\n    left: 0;\n    padding: 6px 8px;\n    background-color: #fff;\n    -webkit-box-shadow: 0 5px 15px -5px rgba(0, 0, 0, .5);\n            box-shadow: 0 5px 15px -5px rgba(0, 0, 0, .5);\n    z-index: 9999;\n}\n.fade-enter-active, .fade-leave-active {\n    -webkit-transition: opacity .1s;\n    transition: opacity .1s;\n}\n.fade-enter, .fade-leave-to {\n    opacity: 0;\n}\n@-webkit-keyframes spin {\n0% {\n        -webkit-transform:rotate(0);\n                transform:rotate(0)\n}\n100% {\n        -webkit-transform:rotate(360deg);\n                transform:rotate(360deg)\n}\n}\n@keyframes spin {\n0% {\n        -webkit-transform:rotate(0);\n                transform:rotate(0)\n}\n100% {\n        -webkit-transform:rotate(360deg);\n                transform:rotate(360deg)\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'bp-vuejs-dropdown',

    props: {
        role: {
            type: String,
            required: false,
            default: ''
        },

        unscroll: {
            type: [HTMLElement, String],
            required: false,
            default: null
        },

        align: {
            type: String,
            required: false,
            default: 'bottom'
        },

        x: {
            type: Number,
            required: false,
            default: 0
        },

        y: {
            type: Number,
            required: false,
            default: 0
        },

        beforeOpen: {
            type: Function,
            required: false,
            default: function _default(resolve) {
                return resolve();
            }
        },

        trigger: {
            type: String,
            required: false,
            default: 'click'
        },

        closeOnClick: {
            type: Boolean,
            required: false,
            default: false
        },

        isIcon: {
            type: Boolean,
            required: false,
            default: true
        },

        className: {
            type: String,
            required: false,
            default: ''
        }
    },

    data: function data() {
        return {
            isHidden: true,
            isLoading: false,

            id: null,
            timeout: null,

            top: undefined,
            right: undefined,
            bottom: undefined,
            left: undefined,
            width: undefined
        };
    },


    watch: {
        isHidden: function isHidden(_isHidden) {
            if (this.unscroll) {
                var el = this.unscroll instanceof HTMLElement ? this.unscroll : document.querySelector(this.unscroll);

                if (el) {
                    el.style.overflow = !_isHidden ? 'hidden' : '';
                }
            }
        }
    },

    created: function created() {
        var _this = this;

        var $root = this.$root;

        // --- hide dropdown if other dropdowns show
        // --- or document clicked
        $root.$on('bp-dropdown:open', function () {
            return _this.isHidden = true;
        });
        $root.$on('bp-dropdown:hide', function () {
            return _this.isHidden = true;
        });

        // --- hide dropdown on document click event
        if (this.trigger === 'click' && !$root['is-bp-dropdown']) {
            Object.defineProperty($root, 'is-bp-dropdown', {
                enumerable: false,
                configurable: false,
                writable: false,
                value: true
            });

            document.onmousedown = function (e) {
                var target = e.target;
                var dropdown = target.closest('.bp-dropdown__btn') || target.closest('.bp-dropdown__body');

                if (!dropdown) {
                    $root.$emit('bp-dropdown:hide');
                }
            };
        }

        this.id = 'bp-dropdown-' + this.generateRandomId();
    },


    methods: {
        // --- generate random id for query selector
        generateRandomId: function generateRandomId() {
            return Math.random().toString(36).substr(2, 10);
        },
        _onToggle: function _onToggle(e) {
            if (this.trigger !== 'click') {
                return;
            }

            this.checkCustomCallback(e);
        },
        _onBtnEnter: function _onBtnEnter(e) {
            if (this.trigger !== 'hover' || !this.isHidden) {
                return;
            }

            this.checkCustomCallback(e);
        },
        _onBtnLeave: function _onBtnLeave(e) {
            var _this2 = this;

            if (this.trigger !== 'hover') {
                return;
            }

            if (this.role) {
                this.timeout = setTimeout(function () {
                    return _this2.isHidden = true;
                }, 100);
            }

            var to = e.toElement;
            if (!to) {
                return;
            }

            var isDropdown = to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__body');
            if (isDropdown) {
                return;
            }

            this.prepare();
        },
        _onBodyClick: function _onBodyClick() {
            if (this.closeOnClick) {
                this.isHidden = true;
            }
        },
        _onBodyEnter: function _onBodyEnter() {
            if (this.timeout) {
                clearTimeout(this.timeout);
            }
        },
        _onBodyLeave: function _onBodyLeave(e) {
            if (this.trigger !== 'hover') {
                return;
            }

            var to = e.toElement;
            if (!to) {
                return;
            }

            if (to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__sub')) {
                return;
            }

            this.prepare();
        },
        checkCustomCallback: function checkCustomCallback(e) {
            var _this3 = this;

            if (!this.isHidden) {
                this.prepare();
                return;
            }

            // --- custom callback before open
            var promise = new Promise(function (resolve) {
                _this3.isLoading = true;
                _this3.beforeOpen.call(_this3, resolve);
            });

            promise.then(function () {
                _this3.isLoading = false;
                if (!e.target.closest('.bp-dropdown__body')) {
                    // --- hide dropdown if other dropdowns show
                    _this3.$root.$emit('bp-dropdown:open');
                }

                setTimeout(_this3.prepare, 0);
            });

            promise.catch(function () {
                throw Error('bp-dropdown promise error');
            });
        },
        prepare: function prepare() {
            var _this4 = this;

            this.isHidden = !this.isHidden;
            if (!this.isHidden) {
                this.$nextTick(function () {
                    var button = _this4.$el.firstElementChild;
                    var container = document.getElementById(_this4.id);

                    _this4.setWidth(button.offsetWidth);
                    _this4.setPosition(button, container);
                });
            }
        },
        setWidth: function setWidth(width) {
            this.width = width;
        },
        setPosition: function setPosition(btn, body) {
            if (!btn || !body) {
                return;
            }

            var coords = this.getCoords(btn);

            // --- current position
            var currentTop = coords.top;
            var currentLeft = coords.left;

            // --- btn size
            var btnWidth = btn.offsetWidth;
            var btnHeight = btn.offsetHeight;

            // --- body size
            var bodyWidth = body.offsetWidth;
            var bodyHeight = body.offsetHeight;

            switch (this.align) {
                case 'top':
                    this.top = currentTop + pageYOffset - bodyHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
                case 'right':
                    this.top = currentTop + pageYOffset;
                    this.left = currentLeft + pageXOffset + btnWidth;
                    break;
                case 'bottom':
                    this.top = currentTop + pageYOffset + btnHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
                case 'left':
                    this.top = currentTop + pageYOffset;
                    this.left = currentLeft + pageXOffset - bodyWidth;
                    break;
                default:
                    this.top = currentTop + pageYOffset + btnHeight;
                    this.left = currentLeft + pageXOffset;
                    break;
            }

            this.top += this.y;
            this.left += this.x;
        },
        getCoords: function getCoords(el) {
            el = el.getBoundingClientRect();
            return {
                top: el.top - pageYOffset,
                left: el.left - pageXOffset
            };
        }
    }
});

/***/ }),

/***/ 878:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _obj, _obj$1, _obj$2
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "bp-dropdown",
      class: { className: _vm.className, "bp-dropdown--sub": _vm.role }
    },
    [
      _c(
        "span",
        {
          class:
            ((_obj = {}),
            (_obj["bp-dropdown__" + (_vm.role ? "sub" : "btn")] = true),
            (_obj[
              "bp-dropdown__" + (_vm.role ? "sub" : "btn") + "--active"
            ] = !_vm.isHidden),
            (_obj[_vm.className + "-bp__btn"] = _vm.className),
            (_obj[_vm.className + "-bp__btn--active"] = !_vm.isHidden),
            _obj),
          on: {
            click: _vm._onToggle,
            mouseenter: _vm._onBtnEnter,
            mouseleave: _vm._onBtnLeave
          }
        },
        [
          _vm._t("btn"),
          _vm._v(" "),
          _vm.isIcon
            ? _vm._t("icon", [
                _vm.isLoading
                  ? _c(
                      "svg",
                      {
                        staticClass:
                          "bp-dropdown__icon bp-dropdown__icon--spin",
                        attrs: { viewBox: "0 0 512 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"
                          }
                        })
                      ]
                    )
                  : _c(
                      "svg",
                      {
                        staticClass: "bp-dropdown__icon",
                        class:
                          ((_obj$1 = {}),
                          (_obj$1["bp-dropdown__icon--" + _vm.align] =
                            _vm.align),
                          _obj$1),
                        attrs: { viewBox: "0 0 256 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"
                          }
                        })
                      ]
                    )
              ])
            : _vm._e()
        ],
        2
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        !_vm.isHidden
          ? _c(
              "div",
              {
                staticClass: "bp-dropdown__body",
                class:
                  ((_obj$2 = {}),
                  (_obj$2[_vm.className + "-bp__body"] = _vm.className),
                  _obj$2),
                style: {
                  minWidth: _vm.width + "px",
                  top: _vm.top + "px",
                  left: _vm.left + "px"
                },
                attrs: { id: _vm.id },
                on: {
                  click: _vm._onBodyClick,
                  mouseenter: _vm._onBodyEnter,
                  mouseleave: _vm._onBodyLeave
                }
              },
              [_vm._t("body")],
              2
            )
          : _vm._e()
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42ca018e", module.exports)
  }
}

/***/ })

});