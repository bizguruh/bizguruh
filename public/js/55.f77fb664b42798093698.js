webpackJsonp([55],{

/***/ 1348:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1349);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("591557c6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eb3adac0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminEditProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eb3adac0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminEditProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1349:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.eventsD[data-v-eb3adac0] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.eventsD .form-group[data-v-eb3adac0] {\n    margin-right: 20px;\n}\n.dd[data-v-eb3adac0] {\n    margin-right: 10px;\n}\n.eventDiv[data-v-eb3adac0] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.pubDates[data-v-eb3adac0] {\n    margin: 15px;\n}\n#mainForm[data-v-eb3adac0] {\n    width: 900px;\n    margin: 20px;\n}\n.dayOfWeek[data-v-eb3adac0] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n.main-page[data-v-eb3adac0] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end;\n}\n.pubDates[data-v-eb3adac0] {\n    margin: 15px;\n}\n.audioText[data-v-eb3adac0] {\n    overflow-x: hidden;\n    text-overflow: ellipsis;\n}\ninput[type=number][data-v-eb3adac0]::-webkit-inner-spin-button,\ninput[type=number][data-v-eb3adac0]::-webkit-outer-spin-button {\n    -webkit-appearance: none;\n    margin: 0;\n}\n.faq li div[data-v-eb3adac0] {\n    margin-bottom: 10px;\n    padding: 0px 20px;\n    border-bottom: 1px solid #ccc;\n}\n.trashfaq[data-v-eb3adac0] {\n    float: right;\n}\n.faq li[data-v-eb3adac0] {\n    margin-bottom: 0px;\n    border: 1px solid #ccc;\n}\n.labelText[data-v-eb3adac0] {\n    position: absolute;\n    top: -30px;\n}\n.ola[data-v-eb3adac0] {\n    margin-bottom: 3px !important;\n}\n.solo[data-v-eb3adac0] {\n    float: left;\n    margin-right: 30px;\n}\n.progressMonitor[data-v-eb3adac0] {\n    color: tomato;\n    text-align: center;\n    font-weight: bold;\n}\n.audioList[data-v-eb3adac0], .videoList[data-v-eb3adac0] {\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n}\n.btn-addProduct[data-v-eb3adac0] {\n    background-color: #333333;\n    color: #ffffff;\n    padding: 15px 50px;\n    font-size: 20px;\n}\n.btn-drafs[data-v-eb3adac0] {\n    background-color: #a3c2dc !important;\n    padding: 15px 30px;\n    font-size: 20px;\n}\n.submitForms[data-v-eb3adac0] {\n    margin-top: 60px;\n}\n.submitForms div[data-v-eb3adac0] {\n    text-align: center;\n}\n.webinar[data-v-eb3adac0] {\n    display: block;\n}\n.hide-input[data-v-eb3adac0] {\n    width: 0.1px;\n    height: 0.1px;\n    opacity: 0;\n    overflow: hidden;\n    position: absolute;\n    z-index: -1;\n}\n.productDetail div[data-v-eb3adac0] {\n    margin-bottom: 50px;\n}\n.documentPercentage[data-v-eb3adac0], .videoPercentage[data-v-eb3adac0] {\n    margin-bottom: 0px;\n    width: 30px;\n    padding: 2px 5px;\n    color: #ffffff;\n    text-align: center;\n    margin-right: auto;\n    border-radius: 50%;\n    margin-left: auto;\n    background: #a3c2dc;\n}\n.documentPercentageText[data-v-eb3adac0], .videoPercentageText[data-v-eb3adac0] {\n    padding: 2px 5px;\n    color: #ffffff;\n    text-align: center;\n    margin-right: auto;\n    margin-left: auto;\n    background: #a3c2dc;\n}\n#isbn[data-v-eb3adac0], #pageNumber[data-v-eb3adac0], #publicationDate[data-v-eb3adac0] {\n    width: 50%;\n}\nlabel[data-v-eb3adac0] {\n    float: left;\n    clear: none;\n    display: block;\n    padding: 2px 1em 0 0;\n}\ninput[type=radio][data-v-eb3adac0],\ninput.radio[data-v-eb3adac0] {\n    float: left;\n    clear: none;\n    margin: 2px 0 0 2px;\n}\n.inputstyle[data-v-eb3adac0] {\n    margin-right: 20px !important;\n}\n.epititle[data-v-eb3adac0] {\n    margin-bottom: 31px;\n}\n#new-file-form[data-v-eb3adac0] {\n    width: 100vw;\n}\n.videoCover[data-v-eb3adac0] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n.iconEp .fa[data-v-eb3adac0] {\n    margin: 3px;\n}\n.divInside[data-v-eb3adac0] {\n    padding: 5px 10px;\n}\n.podHeader[data-v-eb3adac0] {\n    margin-bottom: 0 !important;\n}\n.productDetail[data-v-eb3adac0] {\n    background-color: #ffffff;\n}\ninput[data-v-eb3adac0], textarea[data-v-eb3adac0], .vdp-datepicker *[data-v-eb3adac0] {\n    border-top: 0 solid green !important;\n    border-left: 0 solid green !important;\n    border-right: 0 solid green !important;\n}\ninput[data-v-eb3adac0], textarea[data-v-eb3adac0], .vdp-datepicker *[data-v-eb3adac0] {\n    border-bottom: 1px solid #d2d6de !important;\n}\n.durationText[data-v-eb3adac0] {\n    float: left;\n}\n.pub[data-v-eb3adac0] {\n    position: relative;\n}\ninput[data-v-eb3adac0]::-webkit-input-placeholder, textarea[data-v-eb3adac0]::-webkit-input-placeholder, .pub label[data-v-eb3adac0], .outerText[data-v-eb3adac0]  {\n    color: #dbdbdb !important;\n    font-weight: bold;\n}\n.hardCoverContainer[data-v-eb3adac0] {\n    border: 1px solid #a3c2dc;\n    margin: 5px;\n}\n.hardCover[data-v-eb3adac0], .softCover[data-v-eb3adac0], .videoCover[data-v-eb3adac0], .audioCover[data-v-eb3adac0] {\n    padding: 20px;\n    background-color: #a3c2dc;\n    text-align: center;\n    color: #ffffff;\n    font-weight: bold;\n}\ni.fa-file[data-v-eb3adac0], i.fa-camera[data-v-eb3adac0] {\n    margin: 10px;\n    cursor: pointer;\n    font-size: 30px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuejs_datepicker__ = __webpack_require__(923);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    components: {
        'app-editor': __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */],
        'app-date-picker': __WEBPACK_IMPORTED_MODULE_1_vuejs_datepicker__["a" /* default */]
    },
    data: function data() {
        return {
            id: this.$route.params.id,
            token: '',
            insight: '',
            bookStore: '',
            expertPaymentMethod: '',
            experts: '',
            productPick: '',
            subscriptions: [],
            catSection: false,
            videoFile: false,
            publicationDate: 'Publication Date',
            faqs: {
                productId: this.$route.params.id,
                question: '',
                answer: ''
            },
            faq: [],
            cloudinary: {
                uploadPreset: "knkccgjv",
                apiKey: "634813511968181",
                cloudName: "bizguruh-com"
            },
            thumb: {
                url: ''
            },
            thumbs: [],
            day: '',
            month: '',
            year: '',
            todayDate: new Date(),
            showBookHard: false,
            showBookDigital: false,
            showAudioDigital: false,
            showJournalHard: true,
            showJournalDigital: false,
            showResearchHard: true,
            showResearchDigital: false,
            showReportHard: true,
            showReportDigital: false,
            showPaperHard: true,
            showPaperDigital: false,
            showCourseHard: true,
            showCourseDigital: false,
            showCourseAudio: false,
            showCourseVideo: false,
            redirect: '',
            vendorDatas: [],
            expertDatas: [],
            fileNameVideos: 'Upload Video',
            modalTip: '',
            audio: false,
            progressMonitor: false,
            progressMonitorVideo: false,
            webinarVideo: false,
            video: true,
            Submit: 'Submit',
            fileName: 'Upload File',
            saveAsDraft: 'Save to Draft',
            fileNameVideo: 'Upload File',
            fileTypeName: 'Audio',
            imageName: 'Upload Image',
            imagePlaceholder: '/images/addImage.png',
            book: true,
            modal: true,
            fade: true,
            chapterRow: false,
            epiFade: true,
            isOpen: true,
            epiModal: true,
            documentFile: false,
            productAudio: [],
            productVideo: [],
            quantity: '',
            dis: false,
            formEditData: {},
            episodeformEditData: {},
            settings: {},
            selected: 1,
            productImage: {
                mainImage: ""
            },
            productEpisode: {
                title: '',
                description: '',
                videoFile: '',
                videoName: '',
                mediaType: '',
                product_details_id: this.$route.params.id
            },
            audioPercentage: 0,
            audioFileName: 'Upload Audio',
            productChapters: [],
            productChapter: {
                product_id: this.$route.params.id,
                prodType: '',
                title: '',
                description: ''
            },
            documentPercentage: 0,
            videoPercentage: 0,
            product: {
                id: this.$route.params.id,
                prodType: 'Courses',
                draftType: '',
                eventPrice: '',
                subscriptionLevel: '',
                productCategory: 'BP',
                vendorUserId: '',
                editFlag: '',
                type: '',
                documentPublicId: '',
                marketReport: {
                    title: '',
                    author: '',
                    contributor: '',
                    tableOfContent: '',
                    aboutThisReport: '',
                    overview: '',
                    excerpt: '',
                    excerptFile: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    excerptPublicId: '',
                    excerptName: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''
                },
                articles: {
                    title: '',
                    description: '',
                    tags: '',
                    writer: '',
                    aboutWriter: '',
                    featured: '',
                    media: ''
                },
                events: {
                    title: '',
                    description: '',
                    date: '',
                    time: '',
                    endDate: '',
                    endTime: '',
                    eventSchedule: [{
                        startDay: '',
                        eventStart: '',
                        eventEnd: '',
                        endDay: '',
                        eventStartEnd: '',
                        eventEndEnd: '',
                        moreInformation: ''
                    }],
                    address: '',
                    sameDayCheck: '',
                    eventStart: '',
                    eventEnd: '',
                    city: '',
                    state: '',
                    unlimited: '',
                    country: '',
                    quantity: '',
                    convener: '',
                    convenerName: '',
                    aboutConvener: ''
                },
                marketResearch: {
                    title: '',
                    author: '',
                    publisher: '',
                    isbn: '',
                    aboutThisReport: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    overview: '',
                    excerpt: '',
                    excerptFile: '',
                    excerptPublicId: '',
                    excerptName: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''
                },
                books: {
                    title: '',
                    author: '',
                    publisher: '',
                    publishingDate: '',
                    tableOfContent: '',
                    noOfPages: '',
                    isbn: '',
                    aboutThisAuthor: '',
                    excerpt: '',
                    excerptName: '',
                    excerptPublicId: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    audioFile: '',
                    audioName: '',
                    audioPublicId: '',
                    excerptFile: '',
                    edition: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''
                },
                whitePaper: {
                    title: '',
                    author: '',
                    contributor: '',
                    problemStatement: '',
                    executiveSummary: '',
                    excerpt: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    excerptName: '',
                    excerptFile: '',
                    excerptPublicId: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''
                },
                course: {
                    title: '',
                    overview: '',
                    duration: '',
                    durationPeriod: '',
                    courseModules: '',
                    courseObjectives: '',
                    aboutTheAuthor: '',
                    whoThisCourseFor: '',
                    modules: [],
                    fullOnline: '',
                    whatYouWillLearn: [{
                        whatYouWillLearn: ''
                    }],
                    mixedClass: '',
                    faq: '',
                    keyBenefits: '',
                    whenYouComplete: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    level: '',
                    certification: '',
                    aboutAuthor: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: '',
                    excerpt: '',
                    excerptName: '',
                    excerptFile: '',
                    excerptPublicId: ''
                },
                journal: {
                    journalTitle: '',
                    title: '',
                    articleTitle: '',
                    abstract: '',
                    volume: '',
                    issue: '',
                    monthYear: '',
                    pages: '',
                    publicationDate: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    issn: '',
                    tableOfContent: '',
                    sponsor: '',
                    sponsorName: '',
                    aboutSponsor: '',
                    publisher: ''
                },
                podcast: {
                    title: '',
                    host: '',
                    aboutHost: '',
                    guest: '',
                    episode: [],
                    overview: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: '',
                    excerpt: '',
                    excerptName: '',
                    excerptPublicId: ''
                },
                videos: {
                    title: '',
                    host: '',
                    aboutHost: '',
                    guest: '',
                    episode: [],
                    description: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    overview: '',
                    sponsorName: '',
                    aboutSponsor: '',
                    excerpts: '',
                    excerptsName: '',
                    excerptsPublicId: ''
                },
                webinarVideo: {
                    title: '',
                    description: '',
                    host: '',
                    aboutHost: '',
                    guest: '',
                    aboutGuest: '',
                    excerptActive: false,
                    excerptPercentage: '',
                    overview: '',
                    startDate: '',
                    excerpt: '',
                    excerptName: '',
                    excerptPublicId: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''

                },
                webinar: {
                    title: '',
                    host: '',
                    guest: '',
                    episode: [],
                    overview: '',
                    sponsor: false,
                    sponsorName: '',
                    aboutSponsor: ''
                },
                readonline: false,
                readOnlinePrice: '',
                hardCopy: '',
                video: '',
                subscribePrice: '',
                streamOnlinePrice: '',
                webinarPrice: '',
                softCopy: '',
                quantity: '',
                document: '',
                documentName: '',
                softCopyPrice: '',
                videoPrice: '',
                audioPrice: '',
                hardCopyPrice: '',
                coverImage: '',
                vendor_user_id: ''
            },
            newArray: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authAdmin')) {
            var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
            this.token = authAdmin.access_token;
            this.product.vendor_user_id = authAdmin.id;

            axios.get('/api/product/' + this.id + '/edit', { headers: { "Authorization": 'Bearer ' + authAdmin.access_token } }).then(function (response) {
                if (response.status === 200) {

                    _this.product.subscriptionLevel = response.data.data.subscriptionLevel;

                    if (response.data.data.readOnlinePrice !== '') {
                        _this.product.readonline = true;
                    }
                    response.data.data.softCopy === 1 ? _this.showBookDigital = true : _this.showBookDigital = false;
                    response.data.data.audioCopy === 1 ? _this.showAudioDigital = true : _this.showAudioDigital = false;
                    response.data.data.readOnline === 1 ? _this.product.readonline = true : _this.product.readonline = false;
                    console.log(response.data.data);
                    if (response.data.data.document !== null) {
                        _this.showBookDigital = true;
                    } else {
                        _this.showBookDigital = false;
                    }
                    _this.getSubscriptionType();
                    switch (response.data.data.prodType) {
                        case 'Books':
                            _this.product.prodType = 'Books';
                            _this.product.books = response.data.data.books;
                            _this.audioFileName = _this.product.books.audioName;
                            var dateArray = response.data.data.books.publicationDate.split(' ');
                            _this.day = dateArray[0];
                            _this.month = dateArray[1];
                            _this.year = dateArray[2];
                            if (_this.product.books.sponsorName === '') {
                                _this.product.books.sponsor = false;
                            } else {
                                _this.product.books.sponsor = true;
                            }

                            if (_this.product.books.audioFile !== null) {
                                _this.showAudioDigital = true;
                            } else {
                                _this.showAudioDigital = false;
                            }
                            break;
                        case 'Courses':
                            _this.product.prodType = 'Courses';
                            console.log(response.data.data.courses);
                            //this.product.course = response.data.data.courses;
                            _this.product.course.title = response.data.data.courses.title;
                            _this.product.course.overview = response.data.data.courses.overview;
                            _this.product.course.whoThisCourseFor = response.data.data.courses.whoThisCourseFor;
                            _this.product.course.courseModules = response.data.data.courses.courseModules;
                            _this.product.course.keyBenefit = response.data.data.courses.keyBenefits;
                            _this.product.course.duration = response.data.data.courses.duration;
                            _this.product.course.durationPeriod = response.data.data.courses.durationPeriod;
                            _this.product.course.fullOnline = response.data.data.courses.fullOnline;
                            _this.product.course.mixedClass = response.data.data.courses.mixedClass;
                            _this.product.course.certification = response.data.data.courses.certification;
                            _this.product.course.courseObjectives = response.data.data.courses.courseObjectives;
                            _this.product.course.level = response.data.data.courses.level;
                            _this.product.course.excerpt = response.data.data.courses.excerpt;
                            _this.product.course.excerptPublicId = response.data.data.courses.excerptPublicId;
                            _this.product.course.excerptName = response.data.data.courses.excerptName;
                            _this.product.course.excerptFile = response.data.data.courses.excerptFile;
                            _this.product.course.sponsorName = response.data.data.courses.sponsorName;
                            _this.product.course.aboutSponsor = response.data.data.courses.aboutSponsor;
                            _this.product.course.whenYouComplete = response.data.data.courses.whenYouComplete;
                            _this.product.course.modules = response.data.data.courses.product_course_module;
                            _this.product.course.whatYouWillLearn = response.data.data.courses.course_to_what_learn;

                            if (response.data.data.courses.sponsorName === '') {
                                _this.product.course.sponsor = false;
                            } else {
                                _this.product.course.sponsor = true;
                            }

                            response.data.data.softCopy === 1 ? _this.showCourseDigital = true : _this.showCourseDigital = false;
                            response.data.data.audioCopy === 1 ? _this.showCourseAudio = true : _this.showCourseAudio = false;
                            response.data.data.videoCopy === 1 ? _this.showCourseVideo = true : _this.showCourseVideo = false;
                            _this.getFaqForProduct(_this.$route.params.id);

                            break;
                        case 'Journals':
                            _this.product.prodType = 'Journals';
                            _this.product.journal = response.data.data.journal;
                            //this.product.journal.pageNo = response.data.data.pages;
                            response.data.data.softCopy === 1 ? _this.showJournalDigital = true : _this.showJournalDigital = false;
                            response.data.data.hardCopy === 1 ? _this.showJournalHard = true : _this.showJournalHard = false;
                            var publicationDate = response.data.data.journal.publicationDate.split(' ');
                            _this.day = publicationDate[0];
                            _this.month = publicationDate[1];
                            _this.year = publicationDate[2];
                            if (response.data.data.document === '') {
                                _this.showJournalDigital = false;
                            } else {
                                _this.showJournalDigital = true;
                            }

                            if (response.data.data.journal.sponsorName === '') {
                                _this.product.journal.sponsor = false;
                            } else {
                                _this.product.journal.sponsor = true;
                            }
                            break;
                        case 'Videos':
                            _this.product.prodType = 'Videos';
                            console.log(response.data.data.webinar);
                            _this.product.videos.aboutHost = response.data.data.webinar.aboutHost;
                            _this.product.videos.title = response.data.data.webinar.title;
                            _this.product.videos.host = response.data.data.webinar.host;
                            _this.product.videos.description = response.data.data.webinar.description;
                            _this.product.videos.sponsorName = response.data.data.webinar.sponsorName;
                            _this.product.videos.aboutSponsor = response.data.data.webinar.aboutSponsor;
                            //this.product.videos.episode = response.data.data.webinar.product_video_detail;
                            response.data.data.webinarVideo.forEach(function (item, index) {
                                item.videosActive = false;
                                item.videosPercentage = '';
                                _this.product.videos.episode.push(item);
                            });
                            _this.product.videos.excerpts = response.data.data.webinar.excerpts;
                            _this.product.videos.excerptsName = response.data.data.webinar.excerptsName;
                            _this.product.videos.excerptsPublicId = response.data.data.webinar.excerptsPublicId;
                            _this.product.subscribePrice = response.data.data.subscribePrice;
                            _this.product.streamOnlinePrice = response.data.data.streamOnlinePrice;
                            _this.product.videoPrice = response.data.data.videoPrice;
                            //this.product.videos.episode = response.data.data.webinarVideo;
                            if (response.data.data.webinar.sponsorName === '') {
                                _this.product.videos.sponsor = false;
                            } else {
                                _this.product.videos.sponsor = true;
                            }
                            break;
                        case 'Podcast':
                            _this.product.prodType = 'Podcast';
                            // this.product.podcast = response.data.data.webinar;
                            console.log(response.data.data.webinar);
                            _this.product.podcast.aboutHost = response.data.data.webinar.aboutHost;
                            _this.product.podcast.title = response.data.data.webinar.title;
                            _this.product.podcast.host = response.data.data.webinar.host;
                            _this.product.podcast.overview = response.data.data.webinar.description;
                            _this.product.podcast.sponsorName = response.data.data.webinar.sponsorName;
                            _this.product.podcast.aboutSponsor = response.data.data.webinar.aboutSponsor;
                            //this.product.podcast.episode = response.data.data.webinar.product_video_detail;
                            response.data.data.webinarVideo.forEach(function (item, index) {
                                item.podcastActive = false;
                                item.podcastPercentage = '';
                                _this.product.podcast.episode.push(item);
                            });
                            _this.product.podcast.excerpts = response.data.data.webinar.excerpts;
                            _this.product.podcast.excerptsName = response.data.data.webinar.excerptsName;
                            _this.product.podcast.excerptsPublicId = response.data.data.webinar.excerptsPublicId;
                            _this.product.audioPrice = response.data.data.audioPrice;
                            _this.product.subscribePrice = response.data.data.subscribePrice;
                            _this.product.streamOnlinePrice = response.data.data.streamOnlinePrice;
                            _this.product.podcast.episode = response.data.data.webinarVideo;
                            if (response.data.data.webinar.sponsorName === '') {
                                _this.product.podcast.sponsor = false;
                            } else {
                                _this.product.podcast.sponsor = true;
                            }
                            break;
                        case 'Webinar':
                            _this.product.prodType = 'Webinar';
                            _this.product.webinarVideo = response.data.data.webinar;
                            _this.product.webinarPrice = response.data.data.webinarPrice;
                            if (response.data.data.sponsorName === '') {
                                _this.product.webinarVideo.sponsor = false;
                            }
                            break;
                        case 'White Paper':
                            _this.product.prodType = 'White Paper';
                            _this.product.whitePaper = response.data.data.whitePaper;
                            response.data.data.hardCopy === 1 ? _this.showPaperHard = true : _this.showPaperHard = false;
                            response.data.data.softCopy === 1 ? _this.showPaperDigital = true : _this.showPaperDigital = false;
                            _this.product.hardCopyPrice = response.data.data.hardCopyPrice;
                            _this.product.quantity = response.data.data.quantity;
                            _this.product.softCopyPrice = response.data.data.softCopyPrice;

                            if (_this.product.whitePaper.sponsorName === '') {
                                _this.product.whitePaper.sponsor = false;
                            } else {
                                _this.product.whitePaper.sponsor = true;
                            }
                            break;
                        case 'Market Reports':
                            _this.product.prodType = 'Market Reports';
                            _this.product.marketReport = response.data.data.marketReport;
                            response.data.data.hardCopy === 1 ? _this.showReportHard = true : _this.showReportHard = false;
                            response.data.data.softCopy === 1 ? _this.showReportDigital = true : _this.showReportDigital = false;
                            _this.product.hardCopyPrice = response.data.data.hardCopyPrice;
                            _this.product.softCopyPrice = response.data.data.softCopyPrice;
                            _this.product.quantity = response.data.data.quantity;
                            if (_this.product.marketReport.sponsorName === '') {
                                _this.product.marketReport.sponsor = false;
                            } else {
                                _this.product.marketReport.sponsor = true;
                            }
                            break;
                        case 'Articles':
                            _this.product.prodType = 'Articles';
                            _this.product.articles = response.data.data.articles;
                            break;
                        case 'Events':
                            _this.product.prodType = 'Events';
                            _this.product.eventPrice = response.data.data.eventPrice;
                            _this.product.events = response.data.data.events;
                            if (_this.product.events.convenerName === '') {
                                _this.product.events.convener = false;
                            } else {
                                _this.product.events.convener = true;
                            }
                            _this.product.events.eventSchedule = response.data.data.eventSchedule;
                            break;
                        case 'Market Research':
                            _this.product.prodType = 'Market Research';
                            _this.product.marketResearch = response.data.data.marketResearch;
                            response.data.data.hardCopy === 1 ? _this.showResearchHard = true : _this.showResearchHard = false;
                            response.data.data.softCopy === 1 ? _this.showResearchDigital = true : _this.showResearchDigital = false;
                            _this.product.hardCopyPrice = response.data.data.hardCopyPrice;
                            _this.product.quantity = response.data.data.quantity;

                            _this.product.softCopyPrice = response.data.data.softCopyPrice;
                            if (_this.product.marketResearch.sponsorName === '') {
                                _this.product.marketResearch.sponsor = false;
                            } else {
                                _this.product.marketResearch.sponsor = true;
                            }
                            break;
                        default:
                            return false;
                    }
                    _this.imagePlaceholder = response.data.data.coverImage;
                    _this.product.vendorUserId = response.data.data.vendor_user_id;
                    _this.product.hardCopyPrice = response.data.data.hardCopyPrice;
                    _this.product.quantity = response.data.data.quantity;
                    _this.getVendorForProduct();
                    _this.getExpertForProduct();
                    _this.product.softCopyPrice = response.data.data.softCopyPrice;
                    _this.product.videoPrice = response.data.data.videoPrice;
                    _this.product.subscribePrice = response.data.data.subscribePrice;
                    _this.product.streamOnlinePrice = response.data.data.streamOnlinePrice;
                    _this.product.audioPrice = response.data.data.audioPrice;
                    response.data.data.readOnline ? _this.product.readonline = true : _this.product.readonline = false;
                    _this.product.readOnlinePrice = response.data.data.readOnlinePrice;
                    _this.product.document = response.data.data.document;
                    _this.product.documentName = response.data.data.documentName;
                    _this.product.documentPublicId = response.data.data.documentPublicId;
                    _this.fileName = response.data.data.documentName;
                    _this.dis = true;
                    //  this.getVideoAndAudio(response.data.data.id);
                    if (response.data.data.prodCategoryType === 'BP') {
                        _this.productPick = 'insight';
                        _this.experts = 'bizguruh';
                    } else if (response.data.data.prodCategoryType === 'EP') {
                        _this.productPick = 'insight';
                        _this.experts = 'experts';
                        _this.expertPaymentMethod = 'paid';
                    } else if (response.data.data.prodCategoryType === 'ES') {
                        _this.productPick = 'insight';
                        _this.experts = 'experts';
                        _this.expertPaymentMethod = 'subscription';
                    }

                    var toDataURL = function toDataURL(url) {
                        return fetch(url).then(function (response) {
                            return response.blob();
                        }).then(function (blob) {
                            return new Promise(function (resolve, reject) {
                                var reader = new FileReader();
                                reader.onloadend = function () {
                                    return resolve(reader.result);
                                };
                                reader.onerror = reject;
                                reader.readAsDataURL(blob);
                            });
                        });
                    };

                    toDataURL(response.data.data.coverImage).then(function (dataUrl) {
                        _this.imagePlaceholder = dataUrl;
                        _this.product.coverImage = dataUrl;
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    computed: {
        clUrl: function clUrl() {
            return 'https://api.cloudinary.com/v1_1/' + this.cloudinary.cloudName + '/upload';
        }
    },
    methods: {
        getExpertForProduct: function getExpertForProduct() {
            var _this2 = this;

            axios.get('/api/admin/expert/product', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response.data);
                    _this2.expertDatas = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getVendorForProduct: function getVendorForProduct() {
            var _this3 = this;

            axios.get('/api/admin/vendor/product', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.vendorDatas = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        addMoreLearnContent: function addMoreLearnContent(e) {
            e.preventDefault();
            this.product.course.whatYouWillLearn.push({
                whatYouWillLearn: ''
            });
        },
        addMoreDateandTime: function addMoreDateandTime(e) {
            e.preventDefault();
            this.product.events.eventSchedule.push({
                startDay: '',
                eventStart: '',
                eventEnd: '',
                endDay: '',
                eventStartEnd: '',
                eventEndEnd: '',
                moreInformation: ''
            });
        },
        getSubscriptionType: function getSubscriptionType() {
            var _this4 = this;

            axios.get('/api/subscription', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response.data);
                    _this4.subscriptions = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        removeItem: function removeItem(index, type) {
            switch (type) {
                case 'podcast':
                    this.product.podcast.episode.splice(index, 1);
                    break;
                case 'videos':
                    this.product.videos.episode.splice(index, 1);
                    break;
                case 'courses':
                    this.product.course.modules.splice(index, 1);
                    break;
                default:
                    return false;
            }
        },
        removeFaq: function removeFaq(id, index) {
            var _this5 = this;

            axios.delete('/api/faqs/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this5.faq.splice(index, 1);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getFaqForProduct: function getFaqForProduct(id) {
            var _this6 = this;

            axios.get('/api/get/faq/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this6.faq = response.data;
                    _this6.faqs.question = _this6.faqs.answer = '';
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        submitFaq: function submitFaq(e) {
            var _this7 = this;

            e.preventDefault();
            axios.post('/api/faqs', this.faqs, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 201) {
                    _this7.faq.push(response.data);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        createMoreWebinarVideo: function createMoreWebinarVideo(e) {
            e.preventDefault();
            this.product.webinar.episode.push({
                webinarActive: false,
                webinarPercentage: '',
                title: '',
                description: '',
                videoFile: '',
                videoName: '',
                mediaType: 'Webinars'
            });
        },
        createMoreVideo: function createMoreVideo() {
            this.product.videos.episode.push({
                videosActive: false,
                videosPercentage: '',
                title: '',
                description: '',
                overview: '',
                date: '',
                guest: '',
                aboutGuest: '',
                videoFile: '',
                videoName: 'Upload File',
                videoPublicId: '',
                mediaType: 'Videos'
            });
            console.log('ccc');
        },
        createMoreModule: function createMoreModule(e) {
            e.preventDefault();
            this.product.course.modules.push({
                courseActive: false,
                coursePercentage: '',
                title: '',
                description: '',
                file: '',
                filePublicId: '',
                audioPublicId: '',
                audioFile: '',
                fileName: 'Upload Course',
                fileAudioName: 'Upload Audio',
                author: '',
                videoFile: '',
                videoPublicId: '',
                fileVideoName: 'Upload Video',
                aboutAuthor: '',
                duration: '',
                durationPeriod: ''

            });
        },
        createMorePodcast: function createMorePodcast(e) {
            e.preventDefault();
            this.product.podcast.episode.push({
                podcastActive: false,
                podcastPercentage: '',
                title: '',
                description: '',
                date: '',
                guest: '',
                aboutGuest: '',
                videoFile: '',
                videoPublicId: '',
                videoName: 'Upload Podcast',
                mediaType: 'Podcast'
            });
        },
        uploadFileModule: function uploadFileModule(index, file, type) {

            var that = this;
            that.product.course.modules[index].courseActive = true;
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    that.product.course.modules[index].coursePercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            if (type === 'file') {
                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        var res = JSON.parse(request.response);
                        that.product.course.modules[index].fileName = res.original_filename;
                        that.product.course.modules[index].file = res.secure_url;
                        that.product.course.modules[index].filePublicId = res.public_id;
                        that.product.course.modules[index].coursePercentage = 'Completed';
                    }
                };
            } else if (type === 'audio') {
                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        var res = JSON.parse(request.response);
                        that.product.course.modules[index].fileAudioName = res.original_filename;
                        that.product.course.modules[index].audioFile = res.secure_url;
                        that.product.course.modules[index].audioPublicId = res.public_id;
                        that.product.course.modules[index].coursePercentage = 'Completed';
                    }
                };
            } else if (type === 'video') {
                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        var res = JSON.parse(request.response);
                        that.product.course.modules[index].fileVideoName = res.original_filename;
                        that.product.course.modules[index].videoFile = res.secure_url;
                        that.product.course.modules[index].videoPublicId = res.public_id;
                        that.product.course.modules[index].coursePercentage = 'Completed';
                    }
                };
            } else if (type === 'bookPodcast') {
                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        var res = JSON.parse(request.response);
                        that.product.course.modules[index].fileVideoName = res.original_filename;
                        that.product.books.excerpts = res.secure_url;
                        that.product.course.modules[index].videoPublicId = res.public_id;
                        that.product.course.modules[index].coursePercentage = 'Completed';
                    }
                };
            }

            request.upload.onloadstart = function (e) {
                console.log("start");
                that.product.course.modules[index].coursePercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                that.product.course.modules[index].coursePercentage = 'Completing...';
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        uploadPodcast: function uploadPodcast(index, file) {
            var that = this;
            that.product.podcast.episode[index].podcastActive = true;
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    that.product.podcast.episode[index].podcastPercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    that.product.podcast.episode[index].videoName = res.original_filename;
                    that.product.podcast.episode[index].videoFile = res.secure_url;
                    that.product.podcast.episode[index].videoPublicId = res.public_id;
                    that.product.podcast.episode[index].podcastPercentage = 'Completed';
                }
            };

            request.upload.onloadstart = function (e) {
                console.log("start");
                that.product.podcast.episode[index].podcastPercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                that.product.podcast.episode[index].podcastPercentage = 'Completing...';
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        uploadVVideos: function uploadVVideos(index, file) {
            // this.createMoreVideo();
            var that = this;
            that.product.videos.episode[index].videosActive = true;
            that.product.videos.episode[index].videosPercentage = '';
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(that.product.videos.episode[index]);
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    that.product.videos.episode[index].videosPercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    that.product.videos.episode[index].videoName = res.original_filename;
                    that.product.videos.episode[index].videoFile = res.secure_url;
                    that.product.videos.episode[index].videoPublicId = res.public_id;
                    that.product.videos.episode[index].videosPercentage = 'Completed';
                }
            };

            request.upload.onloadstart = function (e) {
                console.log("start");
                that.product.videos.episode[index].videosPercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                that.product.videos.episode[index].videosPercentage = 'Completing...';
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        uploadWebinarVideos: function uploadWebinarVideos(index, file) {
            var that = this;
            that.product.webinar.episode[index].webinarActive = true;
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    that.product.webinar.episode[index].webinarPercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    that.product.webinar.episode[index].videoName = res.original_filename;
                    that.product.webinar.episode[index].videoFile = res.secure_url;
                    console.log(JSON.parse(request.response));
                    that.product.webinar.episode[index].webinarPercentage = 'Completed';
                }
            };

            request.upload.onloadstart = function (e) {
                console.log("start");
                that.product.webinar.episode[index].webinarPercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                that.product.webinar.episode[index].webinarPercentage = 'Completing...';
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        uploadeExcerptsVideo: function uploadeExcerptsVideo(type, file) {
            var that = this;
            if (type === 'videosExcerpts') {
                this.product.videos.excerptActive = true;
            } else if (type === 'podcastExcerpts') {
                this.product.podcast.excerptActive = true;
            } else if (type === 'white-paper') {
                this.product.whitePaper.excerptActive = true;
            } else if (type === 'report') {
                this.product.marketReport.excerptActive = true;
            } else if (type === 'research') {
                this.product.marketReport.excerptActive = true;
            } else if (type === 'bookExcerpts') {
                this.product.books.excerptActive = true;
            } else if (type === 'webinarExcerpt') {
                this.product.webinarVideo.excerptActive = true;
            } else if (type === 'courseExcerpt') {
                this.product.course.excerptActive = true;
            }
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    if (type === 'videosExcerpts') {
                        that.product.videos.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'podcastExcerpts') {
                        that.product.podcast.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'white-paper') {
                        that.product.whitePaper.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'report') {
                        that.product.marketReport.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'research') {
                        that.product.marketReport.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'bookExcerpts') {
                        that.product.books.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'webinarExcerpt') {
                        that.product.webinarVideo.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'courseExcerpt') {
                        that.product.course.excerptPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    }
                    //  console.log(that.videoPercentage);
                    //that.videoPercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    //  that.fileNameVideo = res.original_filename;
                    if (type === 'report') {
                        that.product.marketReport.excerptName = res.original_filename;
                        that.product.marketReport.excerptFile = res.secure_url;
                        that.product.marketReport.excerptPublicId = res.public_id;
                        that.product.marketReport.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'research') {
                        that.product.marketResearch.excerptName = res.original_filename;
                        that.product.marketResearch.excerptFile = res.secure_url;
                        that.product.marketResearch.excerptPublicId = res.public_id;
                        that.product.marketResearch.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'bookExcerpts') {
                        that.product.books.excerptName = res.original_filename;
                        that.product.books.excerptFile = res.secure_url;
                        that.product.books.excerptPublicId = res.public_id;
                        that.product.books.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'white-paper') {
                        that.product.whitePaper.excerptName = res.original_filename;
                        that.product.whitePaper.excerptFile = res.secure_url;
                        that.product.whitePaper.excerptPublicId = res.public_id;
                        that.product.whitePaper.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'videosExcerpts') {
                        that.product.videos.excerpts = res.secure_url;
                        that.product.videos.excerptsName = res.original_filename;
                        that.product.videos.excerptsPublicId = res.public_id;
                        that.product.videos.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'podcastExcerpts') {
                        that.product.podcast.excerpt = res.secure_url;
                        that.product.podcast.excerptName = res.original_filename;
                        that.product.podcast.excerptPublicId = res.public_id;
                        that.product.podcast.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'webinarExcerpt') {
                        that.product.webinarVideo.excerpt = res.secure_url;
                        that.product.webinarVideo.excerptName = res.original_filename;
                        that.product.webinarVideo.excerptPublicId = res.public_id;
                        that.product.webinarVideo.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    } else if (type === 'courseExcerpt') {
                        that.product.course.excerpt = res.secure_url;
                        that.product.course.excerptName = res.original_filename;
                        that.product.course.excerptsPublicId = res.public_id;
                        that.product.course.excerptPercentage = 'Completed';
                        that.progressMonitor = false;
                    }

                    console.log(JSON.parse(request.response));
                    that.videoPercentage = 'Completed';
                }
            };

            request.upload.onloadstart = function (e) {
                console.log("start");
                if (type === 'videosExcerpts') {
                    that.product.videos.excerptPercentage = 'Starting...';
                } else if (type === 'podcastExcerpts') {
                    that.product.podcast.excerptPercentage = 'Starting...';
                } else if (type === 'white-paper') {
                    that.product.whitePaper.excerptPercentage = 'Starting...';
                } else if (type === 'report') {
                    that.product.marketReport.excerptPercentage = 'Starting...';
                } else if (type === 'bookExcerpts') {
                    that.product.books.excerptPercentage = 'Starting...';
                } else if (type === 'research') {
                    that.product.marketResearch.excerptPercentage = 'Starting...';
                } else if (type === 'webinarExcerpt') {
                    that.product.webinarVideo.excerptPercentage = 'Starting...';
                } else if (type === 'courseExcerpt') {
                    that.product.course.excerptPercentage = 'Starting...';
                }

                //that.videoPercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                if (type === 'videosExcerpts') {
                    that.product.videos.excerptPercentage = 'Completing...';
                } else if (type === 'podcastExcerpts') {
                    that.product.podcast.excerptPercentage = 'Completing...';
                } else if (type === 'white-paper') {
                    that.product.whitePaper.excerptPercentage = 'Completing...';
                } else if (type === 'report') {
                    that.product.marketReport.excerptPercentage = 'Completing...';
                } else if (type === 'bookExcerpts') {
                    that.product.books.excerptPercentage = 'Completing...';
                } else if (type === 'research') {
                    that.product.marketResearch.excerptPercentage = 'Completing...';
                } else if (type === 'webinarExcerpt') {
                    that.product.webinarVideo.excerptPercentage = 'Completing...';
                } else if (type === 'courseExcerpt') {
                    that.product.course.excerptPercentage = 'Completing...';
                }
                // that.videoPercentage = 'Completing...'
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },

        uploadVideos: function uploadVideos(file) {

            var that = this;
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    that.videoPercentage = Math.round(e.loaded / e.total * 100) + '%';
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    that.fileNameVideo = res.original_filename;
                    that.productEpisode.videoName = res.original_filename;
                    that.productEpisode.videoFile = res.secure_url;
                    that.progressMonitor = false;

                    console.log(JSON.parse(request.response));
                    that.videoPercentage = 'Completed';
                }
            };

            request.upload.onloadstart = function (e) {
                console.log("start");
                that.videoPercentage = 'Starting...';
            };
            request.upload.onloadend = function (e) {
                console.log("end");
                that.videoPercentage = 'Completing...';
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        upload: function upload(file, type) {

            var that = this;
            var formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            var request = new XMLHttpRequest();
            console.log(request);
            request.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    if (type === 'ebook') {
                        that.documentPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'audiobook') {
                        that.audioPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    } else if (type === 'journal') {
                        that.documentPercentage = Math.round(e.loaded / e.total * 100) + '%';
                    }
                }
            };

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    var res = JSON.parse(request.response);
                    if (type === 'ebook') {
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    } else if (type === 'audiobook') {
                        that.product.books.audioName = res.original_filename;
                        that.product.books.audioFile = res.secure_url;
                        that.product.books.audioPublicId = res.public_id;
                        that.product.audioFileName = res.original_filename;
                        that.progressMonitor = false;
                        that.audioPercentage = 'Completed';
                    } else if (type === 'journal') {
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    } else if (type === 'whitePaper') {
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    } else if (type === 'marketResearch') {
                        console.log('fjfjjf');
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    } else if (type === 'whitePaper') {
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    } else if (type === 'marketReport') {
                        that.fileName = res.original_filename;
                        that.product.documentName = res.original_filename;
                        that.product.document = res.secure_url;
                        that.product.documentPublicId = res.public_id;
                        that.progressMonitor = false;
                        that.documentPercentage = 'Completed';
                    }
                }
            };

            request.upload.onloadstart = function (e) {
                if (type === 'ebook') {
                    that.documentPercentage = 'Starting...';
                } else if (type === 'audiobook') {
                    that.audioPercentage = 'Starting...';
                }
            };
            request.upload.onloadend = function (e) {
                if (type === 'ebook') {
                    that.documentPercentage = 'Completing...';
                } else if (type === 'audiobook') {
                    that.audioPercentage = 'Completing...';
                } else if (type === 'journal') {
                    that.documentPercentage = 'Completing...';
                }
            };

            request.open("POST", this.clUrl);
            request.send(formData);
        },
        getProductType: function getProductType(e) {
            switch (e.target.value) {
                case 'Journals':
                    this.video = false;
                    this.audio = false;
                    this.book = true;
                    break;
                case 'Books':
                    this.video = false;
                    this.audio = false;
                    this.book = true;
                    break;
                case 'Podcast':
                    this.video = false;
                    this.audio = true;
                    this.book = false;
                    break;
                case 'Videos':
                    this.video = true;
                    this.audio = false;
                    this.book = false;
                    break;
                case 'Market Reports':
                    this.video = false;
                    this.audio = false;
                    this.book = true;
                    break;
                case 'White Paper':
                    this.video = false;
                    this.audio = false;
                    this.book = true;
                    break;
                case 'Courses':
                    this.video = true;
                    this.audio = true;
                    this.book = true;
                    break;
                case 'Market Research':
                    this.video = false;
                    this.audio = false;
                    this.book = true;
                    break;
                case 'Webinar':
                    this.video = true;
                    this.audio = false;
                    this.book = false;
                    break;
                default:
                    this.video = true;
                    this.audio = true;
                    this.book = true;
            }
        },
        getFileName: function getFileName(e, name) {
            var _this8 = this;

            switch (name) {
                case 'document':
                    this.fileName = e.target.files[0].name;
                    console.log(e.target.files[0].name);
                    this.product.document = this.$refs.files.files[0];
                    break;
                case 'image':
                    var file = e.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        _this8.imagePlaceholder = event.target.result;
                        _this8.product.coverImage = event.target.result;
                    };
                    reader.readAsDataURL(file);

                    break;
                default:
                    return false;
            }
        },
        getVideoAndAudio: function getVideoAndAudio(id) {
            var _this9 = this;

            axios.get('/api/get/product-video/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response.data);
                    response.data.forEach(function (item) {
                        if (item.mediaType === 'audio') {
                            _this9.productAudio.push(item);
                        } else {
                            _this9.productVideo.push(item);
                        }
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        openModal: function openModal(id, index, val) {
            var _this10 = this;

            console.log(id);
            console.log(index);
            console.log(val);
            if (val === 'new') {
                this.fade = false;
                this.productChapter.title = this.productChapter.description = this.productChapter.prodType = '';
            } else {
                this.fade = false;
                this.index = index;
                axios.get('/api/product-chapter/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this10.productChapter = response.data;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        toggleDiv: function toggleDiv(file) {
            switch (file) {
                case 'pdf':
                    this.documentFile = true;
                    this.videoFile = false;
                    this.chapterRow = false;
                    break;
                case 'media':
                    this.documentFile = false;
                    this.videoFile = true;
                    this.chapterRow = true;
                    break;

            }
        },
        uploadEpiVideo: function uploadEpiVideo() {
            //console.log(this.$refs);
            this.productEpisode.videoFile = this.$refs.file.files[0];
            this.productEpisode.videoName = this.$refs.file.files[0].name;
        },
        submitDraft: function submitDraft() {
            this.redirect = 'D';
            this.saveAsDraft = 'Saving to Draft';
            this.product.draftType = 'Y';
            this.updateProductEdit();
        },
        updateProduct: function updateProduct() {
            this.redirect = 'S';
            this.product.editFlag = 'Y';

            this.Submit = 'Processing';
            this.product.draftType = 'N';
            console.log(this.product);
            this.updateProductEdit();
        },
        updateProductEdit: function updateProductEdit() {
            var _this11 = this;

            console.log(JSON.parse(JSON.stringify(this.product)));

            axios.post('/api/edit/existing/product', JSON.parse(JSON.stringify(this.product)), { headers: { "Authorization": 'Bearer ' + this.token, 'Content-Type': 'multipart/form-data' } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response);

                    if (_this11.redirect === 'S') {
                        _this11.$router.push({ name: 'bizguruhProduct' });
                        _this11.$toasted.success("Product successfully updated");
                    } else if (_this11.redirect === 'D') {
                        _this11.$router.push({ name: 'bizguruhProduct' });
                        _this11.$toasted.success("Draft product successfully added");
                    }
                }
            }).catch(function (error) {
                console.log(error.response.data);
                console.log(error);
            });
        },
        createChapter: function createChapter(id) {
            var _this12 = this;

            if (id === undefined) {
                axios.post('/api/product-chapter', JSON.parse(JSON.stringify(this.productChapter)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    console.log(response);
                    if (response.status === 201) {
                        _this12.fade = true;
                        _this12.productChapters.push(response.data);
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                axios.put('/api/product-chapter/' + id, JSON.parse(JSON.stringify(this.productChapter)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this12.productChapters[_this12.index].title = response.data.title;
                        _this12.productChapters[_this12.index].description = response.data.description;
                        _this12.productChapters[_this12.index].prodType = response.data.prodType;
                        _this12.fade = true;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        addEpisode: function addEpisode(id) {
            var _this13 = this;

            this.episodeformEditData = new FormData();
            this.episodeformEditData.append('title', this.productEpisode.title);
            this.episodeformEditData.append('description', this.productEpisode.description);
            this.episodeformEditData.append('videoFile', this.productEpisode.videoFile);
            this.episodeformEditData.append('mediaType', this.productEpisode.mediaType);
            this.episodeformEditData.append('videoName', this.productEpisode.videoName);
            this.episodeformEditData.append('product_details_id', this.productEpisode.product_details_id);
            if (id === undefined) {
                axios.post('/api/product-video', this.episodeformEditData, { headers: { "Authorization": 'Bearer ' + this.token, 'Content-Type': 'multipart/form-data' } }).then(function (response) {
                    console.log(response);
                    if (response.status === 201) {
                        if (response.data.mediaType === 'audio') {
                            _this13.productAudio.push({ 'videoName': response.data.videoName });
                            _this13.modalTip = 'modal';
                            $("#createModalEpisode").modal("hide");
                            $(".modal").removeClass("in");
                            $(".modal-backdrop").remove();
                            $('body').removeClass('modal-open');
                            $('body').css('padding-right', '');
                            $(".modal").hide();
                        } else {
                            _this13.productVideo.push({ 'videoName': response.data.videoName });
                            _this13.modalTip = 'modal';
                            $("#createModalEpisode").modal("hide");
                            $(".modal").removeClass("in");
                            $(".modal-backdrop").remove();
                            $('body').removeClass('modal-open');
                            $('body').css('padding-right', '');
                            $(".modal").hide();
                        }
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                console.log(this.episodeformEditData);
                axios.put('/api/product-video/' + id, this.episodeformEditData, { headers: { "Authorization": 'Bearer ' + this.token, 'Content-Type': 'multipart/form-data' } }).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        deleteEpisode: function deleteEpisode(id, index, indexes) {
            var _this14 = this;

            axios.delete('/api/product-video/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this14.productChapters[index].episodes.splice(indexes, 1);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        deleteChapter: function deleteChapter(id, index) {
            var _this15 = this;

            axios.delete('/api/product-chapter/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this15.productChapters.splice(index, 1);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        imageUpload: function imageUpload(e, image) {
            var _this16 = this;

            var file = e.target.files[0];
            var reader = new FileReader();
            var product = this.product.productImage;
            var productData = this.productImage;
            reader.onload = function (event) {
                switch (image) {
                    case "mainImage":
                        //this.product.productImage.splice(0, 1);
                        var mainSize = parseInt(event.loaded) / 1024;
                        if (mainSize < 500) {
                            _this16.productImage.image = event.target.result;
                            _this16.product.coverImage = event.target.result;
                        } else {
                            _this16.$toasted.error("Image cannot be more than 500kb");
                        }
                        break;
                    default:
                        console.log('You cannot upload any image');

                }
            };
            reader.readAsDataURL(file);
        }
    }
});

/***/ }),

/***/ 1351:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "forms", attrs: { id: "mainForm" } }, [
      _c("h2", { staticClass: "title1" }, [_vm._v("Product Details")]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-grids row widget-shadow form-add",
          attrs: { "data-example-id": "basic-forms" }
        },
        [
          _c(
            "form",
            {
              attrs: {
                id: "new-file-form",
                action: "#",
                method: "#",
                enctype: "multipart/form-data"
              },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.updateProduct($event)
                }
              }
            },
            [
              _c("div", { staticClass: "form-body productDetail" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.product.prodType,
                          expression: "product.prodType"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { disabled: "" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              _vm.product,
                              "prodType",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.getProductType($event)
                          }
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { value: "Books" } }, [
                        _vm._v("Books")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Journals" } }, [
                        _vm._v("Journals")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Videos" } }, [
                        _vm._v("Videos")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Podcast" } }, [
                        _vm._v("Podcast")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Courses" } }, [
                        _vm._v("Courses")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Articles" } }, [
                        _vm._v("Articles")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Webinar" } }, [
                        _vm._v("Webinar")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Events" } }, [
                        _vm._v("Events")
                      ]),
                      _vm._v(" "),
                      _c(
                        "option",
                        { attrs: { value: "Bizguruh International" } },
                        [_vm._v("Bizguruh International")]
                      ),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Free Downloads" } }, [
                        _vm._v("Free Downloads")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Degrees" } }, [
                        _vm._v("Degrees")
                      ]),
                      _vm._v(" "),
                      _c(
                        "option",
                        { attrs: { value: "Professional Certificates" } },
                        [_vm._v("Professional Certificates")]
                      ),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "White Paper" } }, [
                        _vm._v("White Papers")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Market Research" } }, [
                        _vm._v("Market Research")
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _vm.product.prodType === "Courses"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.course.title,
                              expression: "product.course.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            id: "productName",
                            name: "productCourseTitle",
                            required: "",
                            placeholder: "Course Title"
                          },
                          domProps: { value: _vm.product.course.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.course,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.course.overview,
                              expression: "product.course.overview"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            id: "overview",
                            name: "productCourseOverview",
                            required: "",
                            placeholder: "Course Overview"
                          },
                          domProps: { value: _vm.product.course.overview },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.course,
                                "overview",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.course.duration,
                              expression: "product.course.duration"
                            }
                          ],
                          staticClass: "durationText",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Duration"
                          },
                          domProps: { value: _vm.product.course.duration },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.course,
                                "duration",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.durationPeriod,
                                expression: "product.course.durationPeriod"
                              }
                            ],
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.product.course,
                                  "durationPeriod",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", [_vm._v("Hours")]),
                            _vm._v(" "),
                            _c("option", [_vm._v("Days")]),
                            _vm._v(" "),
                            _c("option", [_vm._v("Weeks")]),
                            _vm._v(" "),
                            _c("option", [_vm._v("Months")]),
                            _vm._v(" "),
                            _c("option", [_vm._v("Years")])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.course.whoThisCourseFor,
                              expression: "product.course.whoThisCourseFor"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            id: "whoIsthisCourseFor",
                            name: "productCourseFor",
                            required: "",
                            placeholder: "Who is this course for"
                          },
                          domProps: {
                            value: _vm.product.course.whoThisCourseFor
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.course,
                                "whoThisCourseFor",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Course Requirement(s)")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.course.courseModules,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.course,
                                    "courseModules",
                                    $$v
                                  )
                                },
                                expression: "product.course.courseModules"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Course Description(s)")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.course.courseObjectives,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.course,
                                    "courseObjectives",
                                    $$v
                                  )
                                },
                                expression: "product.course.courseObjectives"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c(
                        "ul",
                        { staticClass: "faq" },
                        _vm._l(_vm.faq, function(fa, index) {
                          return _c("li", [
                            _c(
                              "div",
                              {
                                on: {
                                  click: function($event) {
                                    return _vm.removeFaq(fa.id, index)
                                  }
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "fa fa-trash mt-3 trashfaq"
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c("div", [_vm._v(_vm._s(fa.question))]),
                            _vm._v(" "),
                            _c("div", [_vm._v(_vm._s(fa.answer))])
                          ])
                        }),
                        0
                      ),
                      _vm._v(" "),
                      _c("div", [
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.faqs.question,
                                expression: "faqs.question"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              placeholder: "Enter Question"
                            },
                            domProps: { value: _vm.faqs.question },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.faqs,
                                  "question",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.faqs.answer,
                                expression: "faqs.answer"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              id: "faq",
                              name: "productCourseFor",
                              placeholder: "Enter Answer"
                            },
                            domProps: { value: _vm.faqs.answer },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.faqs,
                                  "answer",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary",
                            on: { click: _vm.submitFaq }
                          },
                          [_vm._v("Submit and Add More FAQ'S")]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _vm._l(_vm.product.course.whatYouWillLearn, function(
                            learn
                          ) {
                            return _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: learn.whatYouWillLearn,
                                    expression: "learn.whatYouWillLearn"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "What you will learn"
                                },
                                domProps: { value: learn.whatYouWillLearn },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      learn,
                                      "whatYouWillLearn",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          }),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-primary ",
                              on: {
                                click: function($event) {
                                  return _vm.addMoreLearnContent($event)
                                }
                              }
                            },
                            [_vm._v("Add More")]
                          )
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.course.sponsor,
                              expression: "product.course.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.product.course.sponsor)
                              ? _vm._i(_vm.product.course.sponsor, null) > -1
                              : _vm.product.course.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.course.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.course,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.course,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.course, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.course.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.course.sponsorName,
                                    expression: "product.course.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.course.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.course,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.course.aboutSponsor,
                                    expression: "product.course.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.course.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.course,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.product.course.excerptActive
                        ? _c("div", [
                            _vm.product.course.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.course.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.course.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.course.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.course.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.course.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.course.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.course.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.course.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "courseExcerpt",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [_vm._v("Level:")]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.level,
                                expression: "product.course.level"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.product.course,
                                  "level",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", { attrs: { value: "beginner" } }, [
                              _vm._v("Beginner")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "advanced" } }, [
                              _vm._v("Advanced")
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Events"
                  ? _c(
                      "div",
                      [
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.title,
                                expression: "product.events.title"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              placeholder: "Enter Title*",
                              required: ""
                            },
                            domProps: { value: _vm.product.events.title },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product.events,
                                  "title",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("app-editor", {
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.events.description,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.events,
                                    "description",
                                    $$v
                                  )
                                },
                                expression: "product.events.description"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", [_vm._v("Event Start Date and Time")]),
                        _vm._v(" "),
                        _vm._l(_vm.product.events.eventSchedule, function(
                          event,
                          index
                        ) {
                          return _c("div", [
                            _c("div", { staticClass: "eventsD" }, [
                              _c("p", [_vm._v("Start Date")]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.startDay,
                                      expression: "event.startDay"
                                    }
                                  ],
                                  attrs: { type: "date" },
                                  domProps: { value: event.startDay },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "startDay",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.eventStart,
                                      expression: "event.eventStart"
                                    }
                                  ],
                                  attrs: { type: "time" },
                                  domProps: { value: event.eventStart },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "eventStart",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group " }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.eventEnd,
                                      expression: "event.eventEnd"
                                    }
                                  ],
                                  attrs: { type: "time" },
                                  domProps: { value: event.eventEnd },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "eventEnd",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "eventsD" }, [
                              _c("p", [_vm._v("End Date")]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.endDay,
                                      expression: "event.endDay"
                                    }
                                  ],
                                  attrs: { type: "date" },
                                  domProps: { value: event.endDay },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "endDay",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.eventStartEnd,
                                      expression: "event.eventStartEnd"
                                    }
                                  ],
                                  attrs: { type: "time" },
                                  domProps: { value: event.eventStartEnd },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "eventStartEnd",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group " }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: event.eventEndEnd,
                                      expression: "event.eventEndEnd"
                                    }
                                  ],
                                  attrs: { type: "time" },
                                  domProps: { value: event.eventEndEnd },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        event,
                                        "eventEndEnd",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: event.moreInformation,
                                    expression: "event.moreInformation"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "More Information" },
                                domProps: { value: event.moreInformation },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      event,
                                      "moreInformation",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        }),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Same Day?")]),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.sameDayCheck,
                                expression: "product.events.sameDayCheck"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              checked: Array.isArray(
                                _vm.product.events.sameDayCheck
                              )
                                ? _vm._i(
                                    _vm.product.events.sameDayCheck,
                                    null
                                  ) > -1
                                : _vm.product.events.sameDayCheck
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.product.events.sameDayCheck,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.product.events,
                                        "sameDayCheck",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.product.events,
                                        "sameDayCheck",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(
                                    _vm.product.events,
                                    "sameDayCheck",
                                    $$c
                                  )
                                }
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          !_vm.product.events.sameDayCheck
                            ? _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-add",
                                    on: {
                                      click: function($event) {
                                        return _vm.addMoreDateandTime($event)
                                      }
                                    }
                                  },
                                  [_vm._v("Add Date and Time")]
                                )
                              ])
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.address,
                                expression: "product.events.address"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              placeholder: "Enter Address*",
                              required: ""
                            },
                            domProps: { value: _vm.product.events.address },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product.events,
                                  "address",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.city,
                                expression: "product.events.city"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { type: "text", placeholder: "Enter City" },
                            domProps: { value: _vm.product.events.city },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product.events,
                                  "city",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.state,
                                expression: "product.events.state"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { type: "text", placeholder: "Enter State" },
                            domProps: { value: _vm.product.events.state },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product.events,
                                  "state",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.country,
                                expression: "product.events.country"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              placeholder: "Enter Country"
                            },
                            domProps: { value: _vm.product.events.country },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product.events,
                                  "country",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", [_vm._v("Tickets")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.eventPrice,
                                expression: "product.eventPrice"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              placeholder: "Enter Price*",
                              required: ""
                            },
                            domProps: { value: _vm.product.eventPrice },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.product,
                                  "eventPrice",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "eventDiv" }, [
                          !_vm.product.events.unlimited
                            ? _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.events.quantity,
                                      expression: "product.events.quantity"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Enter Quantity*",
                                    required: ""
                                  },
                                  domProps: {
                                    value: _vm.product.events.quantity
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product.events,
                                        "quantity",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Unlimited?")]),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.product.events.unlimited,
                                  expression: "product.events.unlimited"
                                }
                              ],
                              attrs: { type: "checkbox" },
                              domProps: {
                                checked: Array.isArray(
                                  _vm.product.events.unlimited
                                )
                                  ? _vm._i(_vm.product.events.unlimited, null) >
                                    -1
                                  : _vm.product.events.unlimited
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.product.events.unlimited,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? true : false
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.product.events,
                                          "unlimited",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.product.events,
                                          "unlimited",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(
                                      _vm.product.events,
                                      "unlimited",
                                      $$c
                                    )
                                  }
                                }
                              }
                            })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", [_vm._v("Ticket Sales End Date and Time")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "eventDiv" }, [
                          _c("div", { staticClass: "form-group dd" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.product.events.endDate,
                                  expression: "product.events.endDate"
                                }
                              ],
                              attrs: { type: "date" },
                              domProps: { value: _vm.product.events.endDate },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.product.events,
                                    "endDate",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.product.events.endTime,
                                  expression: "product.events.endTime"
                                }
                              ],
                              attrs: { type: "time" },
                              domProps: { value: _vm.product.events.endTime },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.product.events,
                                    "endTime",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Convener")]),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.events.convener,
                                expression: "product.events.convener"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              checked: Array.isArray(
                                _vm.product.events.convener
                              )
                                ? _vm._i(_vm.product.events.convener, null) > -1
                                : _vm.product.events.convener
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.product.events.convener,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.product.events,
                                        "convener",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.product.events,
                                        "convener",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.product.events, "convener", $$c)
                                }
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _vm.product.events.convener
                          ? _c("div", [
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.events.convenerName,
                                      expression: "product.events.convenerName"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Enter Convener"
                                  },
                                  domProps: {
                                    value: _vm.product.events.convenerName
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product.events,
                                        "convenerName",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.events.aboutConvener,
                                      expression: "product.events.aboutConvener"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { placeholder: "About Convener" },
                                  domProps: {
                                    value: _vm.product.events.aboutConvener
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product.events,
                                        "aboutConvener",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          : _vm._e()
                      ],
                      2
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Articles"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.articles.title,
                              expression: "product.articles.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Title*",
                            required: ""
                          },
                          domProps: { value: _vm.product.articles.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.articles,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c("app-editor", {
                            staticClass: "form-control",
                            attrs: {
                              init: {
                                plugins:
                                  "advlist autolink lists link image imagetools charmap print preview anchor textcolor insertdatetime media table paste code help wordcount",

                                toolbar:
                                  "undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat |image help",
                                image_title: true,

                                height: 300,

                                images_upload_credentials: true,
                                images_upload_url: "/api/image-upload/",
                                images_upload_base_path: "/images/",
                                automatic_uploads: true
                              }
                            },
                            model: {
                              value: _vm.product.articles.description,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.product.articles,
                                  "description",
                                  $$v
                                )
                              },
                              expression: "product.articles.description"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.articles.tags,
                              expression: "product.articles.tags"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Tags" },
                          domProps: { value: _vm.product.articles.tags },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.articles,
                                "tags",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.articles.writer,
                              expression: "product.articles.writer"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Writer Name"
                          },
                          domProps: { value: _vm.product.articles.writer },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.articles,
                                "writer",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.articles.aboutWriter,
                              expression: "product.articles.aboutWriter"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About Writer" },
                          domProps: { value: _vm.product.articles.aboutWriter },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.articles,
                                "aboutWriter",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.articles.featured,
                                expression: "product.articles.featured"
                              }
                            ],
                            attrs: { type: "checkbox", value: "featured" },
                            domProps: {
                              checked: Array.isArray(
                                _vm.product.articles.featured
                              )
                                ? _vm._i(
                                    _vm.product.articles.featured,
                                    "featured"
                                  ) > -1
                                : _vm.product.articles.featured
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.product.articles.featured,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = "featured",
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.product.articles,
                                        "featured",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.product.articles,
                                        "featured",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(
                                    _vm.product.articles,
                                    "featured",
                                    $$c
                                  )
                                }
                              }
                            }
                          }),
                          _vm._v("Featured")
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._m(1)
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Reports"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.title,
                              expression: "product.marketReport.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.marketReport.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.author,
                              expression: "product.marketReport.author"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Author" },
                          domProps: { value: _vm.product.marketReport.author },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "author",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.contributor,
                              expression: "product.marketReport.contributor"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Contributor(s)"
                          },
                          domProps: {
                            value: _vm.product.marketReport.contributor
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "contributor",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.aboutThisReport,
                              expression: "product.marketReport.aboutThisReport"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About This Report" },
                          domProps: {
                            value: _vm.product.marketReport.aboutThisReport
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "aboutThisReport",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Table of Content")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.marketReport.tableOfContent,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.marketReport,
                                    "tableOfContent",
                                    $$v
                                  )
                                },
                                expression:
                                  "product.marketReport.tableOfContent"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.overview,
                              expression: "product.marketReport.overview"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Overview" },
                          domProps: {
                            value: _vm.product.marketReport.overview
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "overview",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.marketReport.excerptActive
                        ? _c("div", [
                            _vm.product.marketReport.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketReport
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketReport.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketReport
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketReport.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketReport
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketReport.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketReport
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.marketReport.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "report",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.excerpt,
                              expression: "product.marketReport.excerpt"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Enter Excerpt(Text)" },
                          domProps: { value: _vm.product.marketReport.excerpt },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketReport,
                                "excerpt",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketReport.sponsor,
                              expression: "product.marketReport.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(
                              _vm.product.marketReport.sponsor
                            )
                              ? _vm._i(_vm.product.marketReport.sponsor, null) >
                                -1
                              : _vm.product.marketReport.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.marketReport.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.marketReport,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.marketReport,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(
                                  _vm.product.marketReport,
                                  "sponsor",
                                  $$c
                                )
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.marketReport.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.marketReport.sponsorName,
                                    expression:
                                      "product.marketReport.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.marketReport.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.marketReport,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value:
                                      _vm.product.marketReport.aboutSponsor,
                                    expression:
                                      "product.marketReport.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.marketReport.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.marketReport,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Research"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.title,
                              expression: "product.marketResearch.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.marketResearch.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.author,
                              expression: "product.marketResearch.author"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Author" },
                          domProps: {
                            value: _vm.product.marketResearch.author
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "author",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.contributor,
                              expression: "product.marketResearch.contributor"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Contributor(s)"
                          },
                          domProps: {
                            value: _vm.product.marketResearch.contributor
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "contributor",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.aboutThisReport,
                              expression:
                                "product.marketResearch.aboutThisReport"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About This Report" },
                          domProps: {
                            value: _vm.product.marketResearch.aboutThisReport
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "aboutThisReport",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.overview,
                              expression: "product.marketResearch.overview"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Overview" },
                          domProps: {
                            value: _vm.product.marketResearch.overview
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "overview",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Table of Content")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value:
                                  _vm.product.marketResearch.tableOfContent,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.marketResearch,
                                    "tableOfContent",
                                    $$v
                                  )
                                },
                                expression:
                                  "product.marketResearch.tableOfContent"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.excerpt,
                              expression: "product.marketResearch.excerpt"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Enter Excerpt(Text)" },
                          domProps: {
                            value: _vm.product.marketResearch.excerpt
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.marketResearch,
                                "excerpt",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.marketResearch.excerptActive
                        ? _c("div", [
                            _vm.product.marketResearch.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketResearch
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketResearch.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketResearch
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketResearch.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketResearch
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.marketResearch.excerptPercentage !==
                                0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.marketResearch
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.marketResearch.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "research",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.marketResearch.sponsor,
                              expression: "product.marketResearch.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(
                              _vm.product.marketResearch.sponsor
                            )
                              ? _vm._i(
                                  _vm.product.marketResearch.sponsor,
                                  null
                                ) > -1
                              : _vm.product.marketResearch.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.marketResearch.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.marketResearch,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.marketResearch,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(
                                  _vm.product.marketResearch,
                                  "sponsor",
                                  $$c
                                )
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.marketResearch.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value:
                                      _vm.product.marketResearch.sponsorName,
                                    expression:
                                      "product.marketResearch.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.marketResearch.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.marketResearch,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value:
                                      _vm.product.marketResearch.aboutSponsor,
                                    expression:
                                      "product.marketResearch.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.marketResearch.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.marketResearch,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Books"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.title,
                              expression: "product.books.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.books.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.author,
                              expression: "product.books.author"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Author(s)"
                          },
                          domProps: { value: _vm.product.books.author },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "author",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.publisher,
                              expression: "product.books.publisher"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Publisher"
                          },
                          domProps: { value: _vm.product.books.publisher },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "publisher",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Publication Date*")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "pub dayOfWeek" }, [
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.day,
                                    expression: "day"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.day = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", [_vm._v("N/A")]),
                                _vm._v(" "),
                                _vm._l(31, function(i) {
                                  return _c("option", [
                                    _vm._v(
                                      "\n                                                " +
                                        _vm._s(i) +
                                        "\n                                            "
                                    )
                                  ])
                                })
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.month,
                                    expression: "month"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.month = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", [_vm._v("N/A")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("January")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("Febuary")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("March")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("April")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("May")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("June")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("July")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("August")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("September")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("October")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("November")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("December")])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.year,
                                    expression: "year"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.year = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              _vm._l(140, function(x) {
                                return _c("option", [_vm._v(_vm._s(x + 1900))])
                              }),
                              0
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.noOfPages,
                              expression: "product.books.noOfPages"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Number of Pages e.g: 239"
                          },
                          domProps: { value: _vm.product.books.noOfPages },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "noOfPages",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.isbn,
                              expression: "product.books.isbn"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "ISBN" },
                          domProps: { value: _vm.product.books.isbn },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "isbn",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Table of Content")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.books.tableOfContent,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.books,
                                    "tableOfContent",
                                    $$v
                                  )
                                },
                                expression: "product.books.tableOfContent"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.aboutThisAuthor,
                              expression: "product.books.aboutThisAuthor"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About This Author(s)" },
                          domProps: {
                            value: _vm.product.books.aboutThisAuthor
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "aboutThisAuthor",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.excerpt,
                              expression: "product.books.excerpt"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Excerpt(Text)" },
                          domProps: { value: _vm.product.books.excerpt },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "excerpt",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.books.excerptActive
                        ? _c("div", [
                            _vm.product.books.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.books.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.books.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.books.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.books.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.books.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.books.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.books.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Upload Excerpt(Media)")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "etitle" }, [
                          _vm._v(_vm._s(_vm.product.books.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "bookExcerpts",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.edition,
                              expression: "product.books.edition"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Enter Edition" },
                          domProps: { value: _vm.product.books.edition },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.books,
                                "edition",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.books.sponsor,
                              expression: "product.books.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.product.books.sponsor)
                              ? _vm._i(_vm.product.books.sponsor, null) > -1
                              : _vm.product.books.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.books.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.books,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.books,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.books, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.books.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.books.sponsorName,
                                    expression: "product.books.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.books.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.books,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.books.aboutSponsor,
                                    expression: "product.books.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.books.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.books,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "White Paper"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.title,
                              expression: "product.whitePaper.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.whitePaper.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.author,
                              expression: "product.whitePaper.author"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Author" },
                          domProps: { value: _vm.product.whitePaper.author },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "author",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.contributors,
                              expression: "product.whitePaper.contributors"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Contributor(s)"
                          },
                          domProps: {
                            value: _vm.product.whitePaper.contributors
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "contributors",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.problemStatement,
                              expression: "product.whitePaper.problemStatement"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Problem Statement" },
                          domProps: {
                            value: _vm.product.whitePaper.problemStatement
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "problemStatement",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.executiveSummary,
                              expression: "product.whitePaper.executiveSummary"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Executive Summary" },
                          domProps: {
                            value: _vm.product.whitePaper.executiveSummary
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "executiveSummary",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.whitePaper.excerptActive
                        ? _c("div", [
                            _vm.product.whitePaper.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.whitePaper.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.whitePaper.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.whitePaper.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.whitePaper.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.whitePaper.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.whitePaper.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.whitePaper.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt(Media)")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.whitePaper.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "white-paper",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.excerpt,
                              expression: "product.whitePaper.excerpt"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Enter Excerpt(Text)" },
                          domProps: { value: _vm.product.whitePaper.excerpt },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.whitePaper,
                                "excerpt",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.whitePaper.sponsor,
                              expression: "product.whitePaper.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(
                              _vm.product.whitePaper.sponsor
                            )
                              ? _vm._i(_vm.product.whitePaper.sponsor, null) >
                                -1
                              : _vm.product.whitePaper.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.whitePaper.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.whitePaper,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.whitePaper,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.whitePaper, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.whitePaper.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.whitePaper.sponsorName,
                                    expression: "product.whitePaper.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.whitePaper.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.whitePaper,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.whitePaper.aboutSponsor,
                                    expression:
                                      "product.whitePaper.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.whitePaper.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.whitePaper,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Journals"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.articleTitle,
                              expression: "product.journal.articleTitle"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Article Title" },
                          domProps: { value: _vm.product.journal.articleTitle },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "articleTitle",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.author,
                              expression: "product.journal.author"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Author(s)" },
                          domProps: { value: _vm.product.journal.author },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "author",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.abstract,
                              expression: "product.journal.abstract"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Abstract/Info" },
                          domProps: { value: _vm.product.journal.abstract },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "abstract",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.pages,
                              expression: "product.journal.pages"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Pages eg: 13-20"
                          },
                          domProps: { value: _vm.product.journal.pages },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "pages",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.journalTitle,
                              expression: "product.journal.journalTitle"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Journal Title" },
                          domProps: { value: _vm.product.journal.journalTitle },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "journalTitle",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.volume,
                              expression: "product.journal.volume"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Volume" },
                          domProps: { value: _vm.product.journal.volume },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "volume",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.issue,
                              expression: "product.journal.issue"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Issue" },
                          domProps: { value: _vm.product.journal.issue },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "issue",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Publication Date*")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "pub dayOfWeek" }, [
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.day,
                                    expression: "day"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.day = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", [_vm._v("N/A")]),
                                _vm._v(" "),
                                _vm._l(31, function(i) {
                                  return _c("option", [
                                    _vm._v(
                                      "\n                                                " +
                                        _vm._s(i) +
                                        "\n                                            "
                                    )
                                  ])
                                })
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.month,
                                    expression: "month"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.month = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", [_vm._v("N/A")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("January")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("Febuary")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("March")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("April")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("May")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("June")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("July")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("August")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("September")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("October")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("November")]),
                                _vm._v(" "),
                                _c("option", [_vm._v("December")])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "pubDates" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.year,
                                    expression: "year"
                                  }
                                ],
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.year = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              _vm._l(140, function(x) {
                                return _c("option", [_vm._v(_vm._s(x + 1900))])
                              }),
                              0
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.issn,
                              expression: "product.journal.issn"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "ISSN" },
                          domProps: { value: _vm.product.journal.issn },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "issn",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.publisher,
                              expression: "product.journal.publisher"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Publisher" },
                          domProps: { value: _vm.product.journal.publisher },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.journal,
                                "publisher",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Course Module")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _c("app-editor", {
                              staticClass: "form-control",
                              attrs: {
                                apiKey:
                                  "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                                init: { plugins: "wordcount, lists, advlist" }
                              },
                              model: {
                                value: _vm.product.journal.tableOfContent,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.journal,
                                    "tableOfContent",
                                    $$v
                                  )
                                },
                                expression: "product.journal.tableOfContent"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.journal.sponsor,
                              expression: "product.journal.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.product.journal.sponsor)
                              ? _vm._i(_vm.product.journal.sponsor, null) > -1
                              : _vm.product.journal.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.journal.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.journal,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.journal,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.journal, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.journal.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.journal.sponsorName,
                                    expression: "product.journal.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.journal.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.journal,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.journal.aboutSponsor,
                                    expression: "product.journal.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.journal.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.journal,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Webinar"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.title,
                              expression: "product.webinarVideo.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.webinarVideo.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.host,
                              expression: "product.webinarVideo.host"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Host" },
                          domProps: { value: _vm.product.webinarVideo.host },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "host",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.aboutHost,
                              expression: "product.webinarVideo.aboutHost"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About Host" },
                          domProps: {
                            value: _vm.product.webinarVideo.aboutHost
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "aboutHost",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.guest,
                              expression: "product.webinarVideo.guest"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            placeholder: "Enter Guest(s)"
                          },
                          domProps: { value: _vm.product.webinarVideo.guest },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "guest",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.aboutGuest,
                              expression: "product.webinarVideo.aboutGuest"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About Guest(s)" },
                          domProps: {
                            value: _vm.product.webinarVideo.aboutGuest
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "aboutGuest",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.guest,
                              expression: "product.webinarVideo.guest"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Episode" },
                          domProps: { value: _vm.product.webinarVideo.guest },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "guest",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.overview,
                              expression: "product.webinarVideo.overview"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Overview" },
                          domProps: {
                            value: _vm.product.webinarVideo.overview
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.webinarVideo,
                                "overview",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", { staticClass: "labelText" }, [
                          _vm._v("Date")
                        ]),
                        _c(
                          "div",
                          [
                            _c("app-date-picker", {
                              attrs: { format: "dd MMMM yyyy" },
                              model: {
                                value: _vm.product.webinarVideo.startDate,
                                callback: function($$v) {
                                  _vm.$set(
                                    _vm.product.webinarVideo,
                                    "startDate",
                                    $$v
                                  )
                                },
                                expression: "product.webinarVideo.startDate"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _vm.product.webinarVideo.excerptActive
                        ? _c("div", [
                            _vm.product.webinarVideo.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.webinarVideo
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.webinarVideo.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.webinarVideo
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.webinarVideo.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.webinarVideo
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.webinarVideo.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.webinarVideo
                                          .excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.webinarVideo.excerptName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "webinarExcerpt",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarPrice,
                              expression: "product.webinarPrice"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Price" },
                          domProps: { value: _vm.product.webinarPrice },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product,
                                "webinarPrice",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.webinarVideo.sponsor,
                              expression: "product.webinarVideo.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(
                              _vm.product.webinarVideo.sponsor
                            )
                              ? _vm._i(_vm.product.webinarVideo.sponsor, null) >
                                -1
                              : _vm.product.webinarVideo.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.webinarVideo.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.webinarVideo,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.webinarVideo,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(
                                  _vm.product.webinarVideo,
                                  "sponsor",
                                  $$c
                                )
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.webinarVideo.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.webinarVideo.sponsorName,
                                    expression:
                                      "product.webinarVideo.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.webinarVideo.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.webinarVideo,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value:
                                      _vm.product.webinarVideo.aboutSponsor,
                                    expression:
                                      "product.webinarVideo.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.webinarVideo.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.webinarVideo,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Videos"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.videos.title,
                              expression: "product.videos.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.videos.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.videos,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.videos.host,
                              expression: "product.videos.host"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Host" },
                          domProps: { value: _vm.product.videos.host },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.videos,
                                "host",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.videos.aboutHost,
                              expression: "product.videos.aboutHost"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About the Host" },
                          domProps: { value: _vm.product.videos.aboutHost },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.videos,
                                "aboutHost",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.videos.description,
                              expression: "product.videos.description"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Overview" },
                          domProps: { value: _vm.product.videos.description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.videos,
                                "description",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.videos.excerptActive
                        ? _c("div", [
                            _vm.product.videos.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.videos.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.videos.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.videos.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.videos.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.videos.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.videos.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.videos.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.videos.excerptsName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "videosExcerpts",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.videos.sponsor,
                              expression: "product.videos.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.product.videos.sponsor)
                              ? _vm._i(_vm.product.videos.sponsor, null) > -1
                              : _vm.product.videos.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.videos.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.videos,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.videos,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.videos, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.videos.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.videos.sponsorName,
                                    expression: "product.videos.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.videos.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.videos,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.videos.aboutSponsor,
                                    expression: "product.videos.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.videos.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.videos,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("SUBSCRIBE")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.subscribePrice,
                                    expression: "product.subscribePrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: { value: _vm.product.subscribePrice },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "subscribePrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("ONE TIME")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.videoPrice,
                                    expression: "product.videoPrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: { value: _vm.product.videoPrice },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "videoPrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("STREAM ONLINE")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.streamOnlinePrice,
                                    expression: "product.streamOnlinePrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: {
                                  value: _vm.product.streamOnlinePrice
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "streamOnlinePrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Podcast"
                  ? _c("div", [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.podcast.title,
                              expression: "product.podcast.title"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Title" },
                          domProps: { value: _vm.product.podcast.title },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.podcast,
                                "title",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.podcast.host,
                              expression: "product.podcast.host"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", placeholder: "Enter Host" },
                          domProps: { value: _vm.product.podcast.host },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.podcast,
                                "host",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.podcast.aboutHost,
                              expression: "product.podcast.aboutHost"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "About Host" },
                          domProps: { value: _vm.product.podcast.aboutHost },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.podcast,
                                "aboutHost",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.podcast.overview,
                              expression: "product.podcast.overview"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { placeholder: "Overview" },
                          domProps: { value: _vm.product.podcast.overview },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.product.podcast,
                                "overview",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm.product.podcast.excerptActive
                        ? _c("div", [
                            _vm.product.podcast.excerptPercentage ===
                            "Starting..."
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.podcast.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.podcast.excerptPercentage ===
                                "Completing"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.podcast.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.podcast.excerptPercentage ===
                                "Completed"
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.podcast.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm.product.podcast.excerptPercentage !== 0
                              ? _c(
                                  "div",
                                  { staticClass: "documentPercentageText" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.product.podcast.excerptPercentage
                                      )
                                    )
                                  ]
                                )
                              : _vm._e()
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group pub" }, [
                        _c("label", [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.product.podcast.excerptsName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("input", {
                            staticClass: "form-control",
                            attrs: { type: "file" },
                            on: {
                              change: function($event) {
                                return _vm.uploadeExcerptsVideo(
                                  "podcastExcerpts",
                                  $event.target.files
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.product.podcast.sponsor,
                              expression: "product.podcast.sponsor"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            checked: Array.isArray(_vm.product.podcast.sponsor)
                              ? _vm._i(_vm.product.podcast.sponsor, null) > -1
                              : _vm.product.podcast.sponsor
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.product.podcast.sponsor,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.product.podcast,
                                      "sponsor",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.product.podcast,
                                      "sponsor",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.product.podcast, "sponsor", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v("Sponsor")
                      ]),
                      _vm._v(" "),
                      _vm.product.podcast.sponsor
                        ? _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.podcast.sponsorName,
                                    expression: "product.podcast.sponsorName"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Sponsor Name"
                                },
                                domProps: {
                                  value: _vm.product.podcast.sponsorName
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.podcast,
                                      "sponsorName",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.podcast.aboutSponsor,
                                    expression: "product.podcast.aboutSponsor"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { placeholder: "About Sponsor" },
                                domProps: {
                                  value: _vm.product.podcast.aboutSponsor
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product.podcast,
                                      "aboutSponsor",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("SUBSCRIBE")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.subscribePrice,
                                    expression: "product.subscribePrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: { value: _vm.product.subscribePrice },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "subscribePrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("ONE TIME")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.audioPrice,
                                    expression: "product.audioPrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: { value: _vm.product.audioPrice },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "audioPrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-3 p-0 hardCoverContainer" },
                          [
                            _c("div", { staticClass: "videoCover" }, [
                              _vm._v("STREAM ONLINE")
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.product.streamOnlinePrice,
                                    expression: "product.streamOnlinePrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Price (₦)"
                                },
                                domProps: {
                                  value: _vm.product.streamOnlinePrice
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.product,
                                      "streamOnlinePrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "m-0" }, [
                  _c("div", [_vm._v("What categories is this product for?")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row m-0" }, [
                    _c("div", [
                      _c("label", [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.productPick,
                              expression: "productPick"
                            }
                          ],
                          attrs: { type: "radio", value: "insight" },
                          domProps: {
                            checked: _vm._q(_vm.productPick, "insight")
                          },
                          on: {
                            change: function($event) {
                              _vm.productPick = "insight"
                            }
                          }
                        }),
                        _vm._v("Insight")
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", [
                      _c("label", [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.productPick,
                              expression: "productPick"
                            }
                          ],
                          attrs: { type: "radio", value: "bookStore" },
                          domProps: {
                            checked: _vm._q(_vm.productPick, "bookStore")
                          },
                          on: {
                            change: function($event) {
                              _vm.productPick = "bookStore"
                            }
                          }
                        }),
                        _vm._v("Book Store")
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm.productPick === "insight"
                    ? _c("div", { staticClass: "row m-0" }, [
                        _c("div", [
                          _c("label", [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.experts,
                                  expression: "experts"
                                }
                              ],
                              attrs: { type: "radio", value: "bizguruh" },
                              domProps: {
                                checked: _vm._q(_vm.experts, "bizguruh")
                              },
                              on: {
                                change: function($event) {
                                  _vm.experts = "bizguruh"
                                }
                              }
                            }),
                            _vm._v("Bizguruh")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("label", [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.experts,
                                  expression: "experts"
                                }
                              ],
                              attrs: { type: "radio", value: "experts" },
                              domProps: {
                                checked: _vm._q(_vm.experts, "experts")
                              },
                              on: {
                                change: function($event) {
                                  _vm.experts = "experts"
                                }
                              }
                            }),
                            _vm._v("Experts")
                          ])
                        ])
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _vm.productPick === "bookStore"
                  ? _c("div", { staticClass: "row m-0" }, [
                      _c("div", { staticClass: "col-md-12 m-0" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Please select a vendor")]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.product.vendorUserId,
                                  expression: "product.vendorUserId"
                                }
                              ],
                              staticClass: "form-control",
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.product,
                                    "vendorUserId",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", [
                                _vm._v("--please select vendor--")
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.vendorDatas, function(vendorData) {
                                return _c(
                                  "option",
                                  {
                                    key: vendorData.id,
                                    domProps: { value: vendorData.id }
                                  },
                                  [_vm._v(_vm._s(vendorData.storeName))]
                                )
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.experts === "experts" && _vm.productPick === "insight"
                  ? _c("div", { staticClass: "row m-0" }, [
                      _c("div", { staticClass: "col-md-12 m-0" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Please select an expert")]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.product.vendorUserId,
                                  expression: "product.vendorUserId"
                                }
                              ],
                              staticClass: "form-control",
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.product,
                                    "vendorUserId",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", [
                                _vm._v("--please select an expert--")
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.expertDatas, function(expertData) {
                                return _c(
                                  "option",
                                  {
                                    key: expertData.id,
                                    domProps: { value: expertData.id }
                                  },
                                  [_vm._v(_vm._s(expertData.email))]
                                )
                              })
                            ],
                            2
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12 m-0" }, [
                        _c("div", [_vm._v("Select Payment Method")]),
                        _vm._v(" "),
                        _c("div", [
                          _c("label", [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.expertPaymentMethod,
                                  expression: "expertPaymentMethod"
                                }
                              ],
                              attrs: { type: "radio", value: "paid" },
                              domProps: {
                                checked: _vm._q(_vm.expertPaymentMethod, "paid")
                              },
                              on: {
                                change: function($event) {
                                  _vm.expertPaymentMethod = "paid"
                                }
                              }
                            }),
                            _vm._v("Paid")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _c("label", [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.expertPaymentMethod,
                                  expression: "expertPaymentMethod"
                                }
                              ],
                              attrs: { type: "radio", value: "subscription" },
                              domProps: {
                                checked: _vm._q(
                                  _vm.expertPaymentMethod,
                                  "subscription"
                                )
                              },
                              on: {
                                change: function($event) {
                                  _vm.expertPaymentMethod = "subscription"
                                }
                              }
                            }),
                            _vm._v("Subscription")
                          ])
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.bookStore
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "form-group" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.vendorUserId,
                                expression: "product.vendorUserId"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.product,
                                  "vendorUserId",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", [_vm._v("--please select vendor--")]),
                            _vm._v(" "),
                            _vm._l(_vm.vendorDatas, function(vendorData) {
                              return _c(
                                "option",
                                {
                                  key: vendorData.id,
                                  domProps: { value: vendorData.id }
                                },
                                [_vm._v(_vm._s(vendorData.storeName))]
                              )
                            })
                          ],
                          2
                        )
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Videos" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", [
                      _c(
                        "div",
                        {
                          staticClass: "btn btn-primary btn-sm",
                          on: {
                            click: function($event) {
                              return _vm.createMoreVideo()
                            }
                          }
                        },
                        [_vm._v("Add Episodes")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row" },
                        _vm._l(_vm.product.videos.episode, function(
                          episodes,
                          index
                        ) {
                          return _c(
                            "div",
                            { staticClass: "col-md-5 p-0 hardCoverContainer" },
                            [
                              _c(
                                "div",
                                { staticClass: "form-group videoCover" },
                                [
                                  _c("div", { staticClass: "podHeader" }, [
                                    _vm._v("Video")
                                  ]),
                                  _c("div", { staticClass: "podHeader" }, [
                                    _c("i", {
                                      staticClass: "fa fa-close",
                                      on: {
                                        click: function($event) {
                                          return _vm.removeItem(index, "videos")
                                        }
                                      }
                                    })
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                episodes.videosActive
                                  ? _c("div", [
                                      episodes.videosPercentage ===
                                      "Starting..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.videosPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.videosPercentage ===
                                          "Completing..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.videosPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.videosPercentage ===
                                          "Completed"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.videosPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.videosPercentage !== 0
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "documentPercentage"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.videosPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : _c("div")
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.title,
                                      expression: "episodes.title"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "title",
                                    placeholder: "Episode title"
                                  },
                                  domProps: { value: episodes.title },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "title",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.overview,
                                      expression: "episodes.overview"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { placeholder: "Overview" },
                                  domProps: { value: episodes.overview },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "overview",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.guest,
                                      expression: "episodes.guest"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Guest(s)"
                                  },
                                  domProps: { value: episodes.guest },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "guest",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.aboutGuest,
                                      expression: "episodes.aboutGuest"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { placeholder: "About Guest(s)" },
                                  domProps: { value: episodes.aboutGuest },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "aboutGuest",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group pub" }, [
                                _c("label", { staticClass: "labelText" }, [
                                  _vm._v("Date")
                                ]),
                                _c(
                                  "div",
                                  [
                                    _c("app-date-picker", {
                                      attrs: {
                                        format: "dd MMMM yyyy",
                                        placeholder: _vm.publicationDate
                                      },
                                      model: {
                                        value: episodes.date,
                                        callback: function($$v) {
                                          _vm.$set(episodes, "date", $$v)
                                        },
                                        expression: "episodes.date"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ]),
                              _vm._v(" "),
                              _c("label", [
                                _c("i", { staticClass: "fa fa-file-audio-o" }),
                                _c("span", [
                                  _vm._v(_vm._s(episodes.videoName))
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  staticClass: "form-control hide-input",
                                  attrs: { type: "file" },
                                  on: {
                                    change: function($event) {
                                      return _vm.uploadVVideos(
                                        index,
                                        $event.target.files
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c(
                                "ul",
                                _vm._l(_vm.productVideo, function(
                                  video,
                                  index
                                ) {
                                  return _c(
                                    "li",
                                    { staticClass: "videoList" },
                                    [_vm._v(_vm._s(video.videoName))]
                                  )
                                }),
                                0
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Podcast" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", [
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary btn-sm",
                            on: {
                              click: function($event) {
                                return _vm.createMorePodcast($event)
                              }
                            }
                          },
                          [_vm._v("Add Episodes")]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row" },
                        _vm._l(_vm.product.podcast.episode, function(
                          episodes,
                          index
                        ) {
                          return _c(
                            "div",
                            { staticClass: "col-md-5 p-0 hardCoverContainer" },
                            [
                              _c(
                                "div",
                                { staticClass: "form-group videoCover" },
                                [
                                  _c("div", { staticClass: "podHeader" }, [
                                    _vm._v("Podcast")
                                  ]),
                                  _c("div", { staticClass: "podHeader" }, [
                                    _c("i", {
                                      staticClass: "fa fa-close",
                                      on: {
                                        click: function($event) {
                                          return _vm.removeItem(
                                            index,
                                            "podcast"
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                episodes.podcastActive
                                  ? _c("div", [
                                      episodes.podcastPercentage ===
                                      "Starting..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.podcastPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.podcastPercentage ===
                                          "Completing..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.podcastPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.podcastPercentage ===
                                          "Completed"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.podcastPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : episodes.podcastPercentage !== 0
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "documentPercentage"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  episodes.podcastPercentage
                                                )
                                              )
                                            ]
                                          )
                                        : _c("div")
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.title,
                                      expression: "episodes.title"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Episode Title"
                                  },
                                  domProps: { value: episodes.title },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "title",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.overview,
                                      expression: "episodes.overview"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { placeholder: "Overivew" },
                                  domProps: { value: episodes.overview },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "overview",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.guest,
                                      expression: "episodes.guest"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Guest(s)"
                                  },
                                  domProps: { value: episodes.guest },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "guest",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: episodes.aboutGuest,
                                      expression: "episodes.aboutGuest"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { placeholder: "About Guest(s)" },
                                  domProps: { value: episodes.aboutGuest },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        episodes,
                                        "aboutGuest",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group pub" }, [
                                _c("label", { staticClass: "labelText" }, [
                                  _vm._v(" Date")
                                ]),
                                _c(
                                  "div",
                                  [
                                    _c("app-date-picker", {
                                      attrs: {
                                        format: "dd MMMM yyyy",
                                        placeholder: _vm.publicationDate
                                      },
                                      model: {
                                        value: episodes.date,
                                        callback: function($$v) {
                                          _vm.$set(episodes, "date", $$v)
                                        },
                                        expression: "episodes.date"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ]),
                              _vm._v(" "),
                              _c("label", [
                                _c("i", { staticClass: "fa fa-file-audio-o" }),
                                _c("span", [
                                  _vm._v(_vm._s(episodes.videoName))
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  staticClass: "form-control hide-input",
                                  attrs: { type: "file" },
                                  on: {
                                    change: function($event) {
                                      return _vm.uploadPodcast(
                                        index,
                                        $event.target.files
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c(
                                "ul",
                                _vm._l(_vm.productVideo, function(
                                  video,
                                  index
                                ) {
                                  return _c(
                                    "li",
                                    { staticClass: "videoList" },
                                    [_vm._v(_vm._s(video.videoName))]
                                  )
                                }),
                                0
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Courses"
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "ola" }, [
                          _vm._v("100% Online")
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "yes" } }, [
                          _vm._v("Yes"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.fullOnline,
                                expression: "product.course.fullOnline"
                              }
                            ],
                            attrs: {
                              id: "yes",
                              type: "radio",
                              value: "1",
                              name: "fullOnline"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.fullOnline,
                                "1"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "fullOnline",
                                  "1"
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "no" } }, [
                          _vm._v("No"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.fullOnline,
                                expression: "product.course.fullOnline"
                              }
                            ],
                            attrs: {
                              id: "no",
                              type: "radio",
                              value: "0",
                              name: "fullOnline"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.fullOnline,
                                "0"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "fullOnline",
                                  "0"
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "ola" }, [
                          _vm._v("Mixed class(Online & Offline)")
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "mixedYes" } }, [
                          _vm._v("Yes"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.mixedClass,
                                expression: "product.course.mixedClass"
                              }
                            ],
                            attrs: {
                              id: "mixedYes",
                              value: "1",
                              type: "radio",
                              name: "mixedClass"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.mixedClass,
                                "1"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "mixedClass",
                                  "1"
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "mixedNo" } }, [
                          _vm._v("No"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.mixedClass,
                                expression: "product.course.mixedClass"
                              }
                            ],
                            attrs: {
                              id: "mixedNo",
                              value: "0",
                              type: "radio",
                              name: "mixedClass"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.mixedClass,
                                "0"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "mixedClass",
                                  "0"
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "ola" }, [
                          _vm._v("Certification")
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "cerY" } }, [
                          _vm._v("Yes"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.certification,
                                expression: "product.course.certification"
                              }
                            ],
                            attrs: {
                              id: "cerY",
                              type: "radio",
                              value: "1",
                              name: "courseCertification"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.certification,
                                "1"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "certification",
                                  "1"
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "cerN" } }, [
                          _vm._v("No"),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.course.certification,
                                expression: "product.course.certification"
                              }
                            ],
                            attrs: {
                              id: "cerN",
                              type: "radio",
                              value: "0",
                              name: "courseCertification"
                            },
                            domProps: {
                              checked: _vm._q(
                                _vm.product.course.certification,
                                "0"
                              )
                            },
                            on: {
                              change: function($event) {
                                return _vm.$set(
                                  _vm.product.course,
                                  "certification",
                                  "0"
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showCourseHard" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showCourseHard,
                                expression: "showCourseHard"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showCourseHard" },
                            domProps: {
                              checked: Array.isArray(_vm.showCourseHard)
                                ? _vm._i(_vm.showCourseHard, null) > -1
                                : _vm.showCourseHard
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showCourseHard,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showCourseHard = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showCourseHard = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showCourseHard = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Hard Copy")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showCourseDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showCourseDigital,
                                expression: "showCourseDigital"
                              }
                            ],
                            attrs: {
                              type: "checkbox",
                              id: "showCourseDigital"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.showCourseDigital)
                                ? _vm._i(_vm.showCourseDigital, null) > -1
                                : _vm.showCourseDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showCourseDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showCourseDigital = $$a.concat([
                                        $$v
                                      ]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showCourseDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showCourseDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Digital Copy")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showCourseAudio" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showCourseAudio,
                                expression: "showCourseAudio"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showCourseAudio" },
                            domProps: {
                              checked: Array.isArray(_vm.showCourseAudio)
                                ? _vm._i(_vm.showCourseAudio, null) > -1
                                : _vm.showCourseAudio
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showCourseAudio,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showCourseAudio = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showCourseAudio = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showCourseAudio = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Audio Copy")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showCourseVideo" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showCourseVideo,
                                expression: "showCourseVideo"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showCourseVideo" },
                            domProps: {
                              checked: Array.isArray(_vm.showCourseVideo)
                                ? _vm._i(_vm.showCourseVideo, null) > -1
                                : _vm.showCourseVideo
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showCourseVideo,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showCourseVideo = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showCourseVideo = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showCourseVideo = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Video Copy")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Courses"
                  ? _c("div", [
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary btn-sm",
                            on: {
                              click: function($event) {
                                return _vm.createMoreModule($event)
                              }
                            }
                          },
                          [_vm._v("Add More Module")]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row" },
                        _vm._l(_vm.product.course.modules, function(
                          module,
                          index
                        ) {
                          return _c(
                            "div",
                            { staticClass: "col-md-5 p-0 hardCoverContainer" },
                            [
                              _c(
                                "div",
                                { staticClass: "form-group videoCover" },
                                [
                                  _c("div", { staticClass: "podHeader" }, [
                                    _vm._v("Course Module")
                                  ]),
                                  _c("div", { staticClass: "podHeader" }, [
                                    _c("i", {
                                      staticClass: "fa fa-close",
                                      on: {
                                        click: function($event) {
                                          return _vm.removeItem(
                                            index,
                                            "courses"
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                module.courseActive
                                  ? _c("div", [
                                      _c(
                                        "div",
                                        { staticClass: "form-group softCover" },
                                        [_vm._v("Episode")]
                                      ),
                                      _vm._v(" "),
                                      module.coursePercentage === "Starting..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(module.coursePercentage)
                                              )
                                            ]
                                          )
                                        : module.coursePercentage ===
                                          "Completing..."
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(module.coursePercentage)
                                              )
                                            ]
                                          )
                                        : module.coursePercentage ===
                                          "Completed"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "documentPercentageText"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(module.coursePercentage)
                                              )
                                            ]
                                          )
                                        : module.coursePercentage !== 0
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "documentPercentage"
                                            },
                                            [
                                              _vm._v(
                                                _vm._s(module.coursePercentage)
                                              )
                                            ]
                                          )
                                        : _c("div")
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: module.title,
                                        expression: "module.title"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      placeholder: "Enter Title"
                                    },
                                    domProps: { value: module.title },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          module,
                                          "title",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: module.description,
                                        expression: "module.description"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { placeholder: "Enter Description" },
                                    domProps: { value: module.description },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          module,
                                          "description",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: module.author,
                                        expression: "module.author"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      placeholder: "Tutor Name"
                                    },
                                    domProps: { value: module.author },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          module,
                                          "author",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: module.aboutAuthor,
                                        expression: "module.aboutAuthor"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { placeholder: "About Tutor" },
                                    domProps: { value: module.aboutAuthor },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          module,
                                          "aboutAuthor",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c(
                                    "div",
                                    [
                                      _c("app-date-picker", {
                                        attrs: {
                                          format: "MMMM dd yyyy",
                                          "open-date": _vm.todayDate
                                        },
                                        model: {
                                          value: _vm.todayDate,
                                          callback: function($$v) {
                                            _vm.todayDate = $$v
                                          },
                                          expression: "todayDate"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ]),
                                _vm._v(" "),
                                _vm.showCourseDigital
                                  ? _c("label", [
                                      _c("i", { staticClass: "fa fa-file" }),
                                      _c("span", [
                                        _vm._v(_vm._s(module.fileName))
                                      ]),
                                      _vm._v(" "),
                                      _c("input", {
                                        staticClass: "form-control hide-input",
                                        attrs: { type: "file" },
                                        on: {
                                          change: function($event) {
                                            return _vm.uploadFileModule(
                                              index,
                                              $event.target.files,
                                              "file"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm.progressMonitor
                                        ? _c(
                                            "div",
                                            { staticClass: "progressMonitor" },
                                            [_vm._v("Uploading...")]
                                          )
                                        : _vm._e()
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.showCourseAudio
                                  ? _c("label", [
                                      _c("i", { staticClass: "fa fa-file" }),
                                      _c("span", [
                                        _vm._v(_vm._s(module.fileAudioName))
                                      ]),
                                      _vm._v(" "),
                                      _c("input", {
                                        staticClass: "form-control hide-input",
                                        attrs: { type: "file" },
                                        on: {
                                          change: function($event) {
                                            return _vm.uploadFileModule(
                                              index,
                                              $event.target.files,
                                              "audio"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm.progressMonitor
                                        ? _c(
                                            "div",
                                            { staticClass: "progressMonitor" },
                                            [_vm._v("Uploading...")]
                                          )
                                        : _vm._e()
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.showCourseVideo
                                  ? _c("label", [
                                      _c("i", { staticClass: "fa fa-file" }),
                                      _c("span", [
                                        _vm._v(_vm._s(module.fileVideoName))
                                      ]),
                                      _vm._v(" "),
                                      _c("input", {
                                        staticClass: "form-control hide-input",
                                        attrs: { type: "file" },
                                        on: {
                                          change: function($event) {
                                            return _vm.uploadFileModule(
                                              index,
                                              $event.target.files,
                                              "video"
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm.progressMonitor
                                        ? _c(
                                            "div",
                                            { staticClass: "progressMonitor" },
                                            [_vm._v("Uploading...")]
                                          )
                                        : _vm._e()
                                    ])
                                  : _vm._e()
                              ])
                            ]
                          )
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Books" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", { staticClass: "row" }, [
                      _vm.productPick === "bookStore"
                        ? _c("div", [
                            _c("label", { attrs: { for: "showBookHard" } }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.showBookHard,
                                    expression: "showBookHard"
                                  }
                                ],
                                attrs: { type: "checkbox", id: "showBookHard" },
                                domProps: {
                                  checked: Array.isArray(_vm.showBookHard)
                                    ? _vm._i(_vm.showBookHard, null) > -1
                                    : _vm.showBookHard
                                },
                                on: {
                                  change: function($event) {
                                    var $$a = _vm.showBookHard,
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = null,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          (_vm.showBookHard = $$a.concat([$$v]))
                                      } else {
                                        $$i > -1 &&
                                          (_vm.showBookHard = $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1)))
                                      }
                                    } else {
                                      _vm.showBookHard = $$c
                                    }
                                  }
                                }
                              }),
                              _vm._v("Hard Copy")
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showBookDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showBookDigital,
                                expression: "showBookDigital"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showBookDigital" },
                            domProps: {
                              checked: Array.isArray(_vm.showBookDigital)
                                ? _vm._i(_vm.showBookDigital, null) > -1
                                : _vm.showBookDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showBookDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showBookDigital = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showBookDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showBookDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Ebook")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("label", { attrs: { for: "showAudioDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showAudioDigital,
                                expression: "showAudioDigital"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showAudioDigital" },
                            domProps: {
                              checked: Array.isArray(_vm.showAudioDigital)
                                ? _vm._i(_vm.showAudioDigital, null) > -1
                                : _vm.showAudioDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showAudioDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showAudioDigital = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showAudioDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showAudioDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Audio Book")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Journals" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", [
                        _c("label", { attrs: { for: "showJournalDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showJournalDigital,
                                expression: "showJournalDigital"
                              }
                            ],
                            attrs: {
                              type: "checkbox",
                              id: "showJournalDigital"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.showJournalDigital)
                                ? _vm._i(_vm.showJournalDigital, null) > -1
                                : _vm.showJournalDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showJournalDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showJournalDigital = $$a.concat([
                                        $$v
                                      ]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showJournalDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showJournalDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Digital Copy")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Research" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", [
                        _c("label", { attrs: { for: "showResearchDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showResearchDigital,
                                expression: "showResearchDigital"
                              }
                            ],
                            attrs: {
                              type: "checkbox",
                              id: "showResearchDigital"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.showResearchDigital)
                                ? _vm._i(_vm.showResearchDigital, null) > -1
                                : _vm.showResearchDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showResearchDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showResearchDigital = $$a.concat([
                                        $$v
                                      ]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showResearchDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showResearchDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Digital Copy")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Reports" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", [
                        _c("label", { attrs: { for: "showReportDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showReportDigital,
                                expression: "showReportDigital"
                              }
                            ],
                            attrs: {
                              type: "checkbox",
                              id: "showReportDigital"
                            },
                            domProps: {
                              checked: Array.isArray(_vm.showReportDigital)
                                ? _vm._i(_vm.showReportDigital, null) > -1
                                : _vm.showReportDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showReportDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showReportDigital = $$a.concat([
                                        $$v
                                      ]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showReportDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showReportDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Digital Copy")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "White Paper" &&
                (_vm.productPick === "bookStore" ||
                  _vm.productPick === "insight")
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", [
                        _c("label", { attrs: { for: "showPaperDigital" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.showPaperDigital,
                                expression: "showPaperDigital"
                              }
                            ],
                            attrs: { type: "checkbox", id: "showPaperDigital" },
                            domProps: {
                              checked: Array.isArray(_vm.showPaperDigital)
                                ? _vm._i(_vm.showPaperDigital, null) > -1
                                : _vm.showPaperDigital
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.showPaperDigital,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.showPaperDigital = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.showPaperDigital = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.showPaperDigital = $$c
                                }
                              }
                            }
                          }),
                          _vm._v("Digital Copy")
                        ])
                      ])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Books"
                  ? _c("div", { staticClass: "row" }, [
                      _vm.showBookHard
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c(
                                "div",
                                { staticClass: "form-group hardCover" },
                                [_vm._v("Hard Copy")]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.hardCopyPrice,
                                      expression: "product.hardCopyPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "hardCopyPrice",
                                    placeholder: "Enter Price (₦)"
                                  },
                                  domProps: {
                                    value: _vm.product.hardCopyPrice
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "hardCopyPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.quantity,
                                      expression: "product.quantity"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "quantity",
                                    placeholder: "Quantity"
                                  },
                                  domProps: { value: _vm.product.quantity },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "quantity",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.showBookDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Ebook")]
                                ),
                                _vm._v(" "),
                                _vm.documentPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [_vm._v(_vm._s(_vm.fileName))]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "ebook"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.softCopyPrice,
                                      expression: "product.softCopyPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Enter Price"
                                  },
                                  domProps: {
                                    value: _vm.product.softCopyPrice
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "softCopyPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("span", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.product.readonline,
                                        expression: "product.readonline"
                                      }
                                    ],
                                    attrs: { type: "checkbox" },
                                    domProps: {
                                      checked: Array.isArray(
                                        _vm.product.readonline
                                      )
                                        ? _vm._i(_vm.product.readonline, null) >
                                          -1
                                        : _vm.product.readonline
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = _vm.product.readonline,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                _vm.product,
                                                "readonline",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                _vm.product,
                                                "readonline",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(
                                            _vm.product,
                                            "readonline",
                                            $$c
                                          )
                                        }
                                      }
                                    }
                                  })
                                ]),
                                _c("span", [_vm._v("Read Online")])
                              ]),
                              _vm._v(" "),
                              _vm.product.readonline
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.product.readOnlinePrice,
                                          expression: "product.readOnlinePrice"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "text",
                                        placeholder: "Enter Price"
                                      },
                                      domProps: {
                                        value: _vm.product.readOnlinePrice
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.product,
                                            "readOnlinePrice",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                : _vm._e()
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.showAudioDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Audio Book")]
                                ),
                                _vm._v(" "),
                                _vm.audioPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.audioPercentage))]
                                    )
                                  : _vm.audioPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.audioPercentage))]
                                    )
                                  : _vm.audioPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.audioPercentage))]
                                    )
                                  : _vm.audioPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.audioPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", { staticClass: "audioText" }, [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [
                                    _vm._v(_vm._s(_vm.audioFileName))
                                  ]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "audiobook"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.audioPrice,
                                      expression: "product.audioPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "softCopyPrice",
                                    placeholder: "Enter Price"
                                  },
                                  domProps: { value: _vm.product.audioPrice },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "audioPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Journals"
                  ? _c("div", { staticClass: "row" }, [
                      _vm.showJournalHard
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c(
                                "div",
                                { staticClass: "form-group hardCover" },
                                [_vm._v("Hard Copy")]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.hardCopyPrice,
                                      expression: "product.hardCopyPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Enter Price (₦)"
                                  },
                                  domProps: {
                                    value: _vm.product.hardCopyPrice
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "hardCopyPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.quantity,
                                      expression: "product.quantity"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Quantity"
                                  },
                                  domProps: { value: _vm.product.quantity },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "quantity",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.showJournalDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Digital")]
                                ),
                                _vm._v(" "),
                                _vm.documentPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [_vm._v(_vm._s(_vm.fileName))]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "journal"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ])
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Research"
                  ? _c("div", { staticClass: "row" }, [
                      _vm.showResearchDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Digital")]
                                ),
                                _vm._v(" "),
                                _vm.documentPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [_vm._v(_vm._s(_vm.fileName))]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "marketResearch"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ])
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "Market Reports"
                  ? _c("div", { staticClass: "row" }, [
                      _vm.showReportDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Digital")]
                                ),
                                _vm._v(" "),
                                _vm.documentPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [_vm._v(_vm._s(_vm.fileName))]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "marketReport"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ])
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product.prodType === "White Paper"
                  ? _c("div", { staticClass: "row" }, [
                      _vm.showPaperDigital
                        ? _c(
                            "div",
                            { staticClass: "col-md-2 p-0 hardCoverContainer" },
                            [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "div",
                                  { staticClass: "form-group softCover" },
                                  [_vm._v("Digital")]
                                ),
                                _vm._v(" "),
                                _vm.documentPercentage === "Starting..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completing..."
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage === "Completed"
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentageText" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _vm.documentPercentage !== 0
                                  ? _c(
                                      "div",
                                      { staticClass: "documentPercentage" },
                                      [_vm._v(_vm._s(_vm.documentPercentage))]
                                    )
                                  : _c("div"),
                                _vm._v(" "),
                                _c("label", [
                                  _c("i", { staticClass: "fa fa-file" }),
                                  _c("span", [_vm._v(_vm._s(_vm.fileName))]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control hide-input",
                                    attrs: { type: "file" },
                                    on: {
                                      change: function($event) {
                                        return _vm.upload(
                                          $event.target.files,
                                          "whitePaper"
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm.progressMonitor
                                    ? _c(
                                        "div",
                                        { staticClass: "progressMonitor" },
                                        [_vm._v("Uploading...")]
                                      )
                                    : _vm._e()
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.product.softCopyPrice,
                                      expression: "product.softCopyPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    placeholder: "Enter Price"
                                  },
                                  domProps: {
                                    value: _vm.product.softCopyPrice
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.product,
                                        "softCopyPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("span", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.product.readonline,
                                        expression: "product.readonline"
                                      }
                                    ],
                                    attrs: { type: "checkbox" },
                                    domProps: {
                                      checked: Array.isArray(
                                        _vm.product.readonline
                                      )
                                        ? _vm._i(_vm.product.readonline, null) >
                                          -1
                                        : _vm.product.readonline
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = _vm.product.readonline,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                _vm.product,
                                                "readonline",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                _vm.product,
                                                "readonline",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(
                                            _vm.product,
                                            "readonline",
                                            $$c
                                          )
                                        }
                                      }
                                    }
                                  })
                                ]),
                                _c("span", [_vm._v("Read Online")])
                              ]),
                              _vm._v(" "),
                              _vm.product.readonline
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.product.readOnlinePrice,
                                          expression: "product.readOnlinePrice"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "text",
                                        placeholder: "Enter Price"
                                      },
                                      domProps: {
                                        value: _vm.product.readOnlinePrice
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.product,
                                            "readOnlinePrice",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                : _vm._e()
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _vm.expertPaymentMethod === "subscription" ||
                  _vm.experts === "bizguruh"
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm._m(2),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.product.subscriptionLevel,
                                expression: "product.subscriptionLevel"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.product,
                                  "subscriptionLevel",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", [_vm._v("--Please Select--")]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "0" } }, [
                              _vm._v("Free")
                            ]),
                            _vm._v(" "),
                            _vm._l(_vm.subscriptions, function(
                              subscription,
                              index
                            ) {
                              return _vm.subscriptions.length > 0
                                ? _c(
                                    "option",
                                    { domProps: { value: subscription.level } },
                                    [_vm._v(_vm._s(subscription.title))]
                                  )
                                : _vm._e()
                            })
                          ],
                          2
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-2 p-0" }, [
                    _c("label", [
                      _c("img", {
                        attrs: { src: _vm.imagePlaceholder, height: "100" }
                      }),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "hide-input",
                        attrs: { type: "file", id: "coverImage" },
                        on: {
                          change: function($event) {
                            return _vm.getFileName($event, "image")
                          }
                        }
                      })
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row submitForms" }, [
                  _c("div", { staticClass: "col-md-6" }, [
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-drafs",
                        on: {
                          click: function($event) {
                            return _vm.submitDraft()
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.saveAsDraft))]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c("input", {
                      staticClass: "btn-addProduct",
                      attrs: { type: "submit" },
                      domProps: { value: _vm.Submit }
                    })
                  ])
                ])
              ])
            ]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pub" }, [
      _c("div", { staticClass: "labelText outerText" }, [_vm._v("Course FAQs")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("input", { staticClass: "form-control", attrs: { type: "file" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [_c("b", [_vm._v("Subscription Level")])])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-eb3adac0", module.exports)
  }
}

/***/ }),

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1348)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1350)
/* template */
var __vue_template__ = __webpack_require__(1351)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-eb3adac0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminEditProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eb3adac0", Component.options)
  } else {
    hotAPI.reload("data-v-eb3adac0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ }),

/***/ 923:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    var ownKeys = Object.keys(source);

    if (typeof Object.getOwnPropertySymbols === 'function') {
      ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
        return Object.getOwnPropertyDescriptor(source, sym).enumerable;
      }));
    }

    ownKeys.forEach(function (key) {
      _defineProperty(target, key, source[key]);
    });
  }

  return target;
}

var Language =
/*#__PURE__*/
function () {
  function Language(language, months, monthsAbbr, days) {
    _classCallCheck(this, Language);

    this.language = language;
    this.months = months;
    this.monthsAbbr = monthsAbbr;
    this.days = days;
    this.rtl = false;
    this.ymd = false;
    this.yearSuffix = '';
  }

  _createClass(Language, [{
    key: "language",
    get: function get() {
      return this._language;
    },
    set: function set(language) {
      if (typeof language !== 'string') {
        throw new TypeError('Language must be a string');
      }

      this._language = language;
    }
  }, {
    key: "months",
    get: function get() {
      return this._months;
    },
    set: function set(months) {
      if (months.length !== 12) {
        throw new RangeError("There must be 12 months for ".concat(this.language, " language"));
      }

      this._months = months;
    }
  }, {
    key: "monthsAbbr",
    get: function get() {
      return this._monthsAbbr;
    },
    set: function set(monthsAbbr) {
      if (monthsAbbr.length !== 12) {
        throw new RangeError("There must be 12 abbreviated months for ".concat(this.language, " language"));
      }

      this._monthsAbbr = monthsAbbr;
    }
  }, {
    key: "days",
    get: function get() {
      return this._days;
    },
    set: function set(days) {
      if (days.length !== 7) {
        throw new RangeError("There must be 7 days for ".concat(this.language, " language"));
      }

      this._days = days;
    }
  }]);

  return Language;
}(); // eslint-disable-next-line

var en = new Language('English', ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']) // eslint-disable-next-line
;

var utils = {
  /**
   * @type {Boolean}
   */
  useUtc: false,

  /**
   * Returns the full year, using UTC or not
   * @param {Date} date
   */
  getFullYear: function getFullYear(date) {
    return this.useUtc ? date.getUTCFullYear() : date.getFullYear();
  },

  /**
   * Returns the month, using UTC or not
   * @param {Date} date
   */
  getMonth: function getMonth(date) {
    return this.useUtc ? date.getUTCMonth() : date.getMonth();
  },

  /**
   * Returns the date, using UTC or not
   * @param {Date} date
   */
  getDate: function getDate(date) {
    return this.useUtc ? date.getUTCDate() : date.getDate();
  },

  /**
   * Returns the day, using UTC or not
   * @param {Date} date
   */
  getDay: function getDay(date) {
    return this.useUtc ? date.getUTCDay() : date.getDay();
  },

  /**
   * Returns the hours, using UTC or not
   * @param {Date} date
   */
  getHours: function getHours(date) {
    return this.useUtc ? date.getUTCHours() : date.getHours();
  },

  /**
   * Returns the minutes, using UTC or not
   * @param {Date} date
   */
  getMinutes: function getMinutes(date) {
    return this.useUtc ? date.getUTCMinutes() : date.getMinutes();
  },

  /**
   * Sets the full year, using UTC or not
   * @param {Date} date
   */
  setFullYear: function setFullYear(date, value, useUtc) {
    return this.useUtc ? date.setUTCFullYear(value) : date.setFullYear(value);
  },

  /**
   * Sets the month, using UTC or not
   * @param {Date} date
   */
  setMonth: function setMonth(date, value, useUtc) {
    return this.useUtc ? date.setUTCMonth(value) : date.setMonth(value);
  },

  /**
   * Sets the date, using UTC or not
   * @param {Date} date
   * @param {Number} value
   */
  setDate: function setDate(date, value, useUtc) {
    return this.useUtc ? date.setUTCDate(value) : date.setDate(value);
  },

  /**
   * Check if date1 is equivalent to date2, without comparing the time
   * @see https://stackoverflow.com/a/6202196/4455925
   * @param {Date} date1
   * @param {Date} date2
   */
  compareDates: function compareDates(date1, date2) {
    var d1 = new Date(date1.getTime());
    var d2 = new Date(date2.getTime());

    if (this.useUtc) {
      d1.setUTCHours(0, 0, 0, 0);
      d2.setUTCHours(0, 0, 0, 0);
    } else {
      d1.setHours(0, 0, 0, 0);
      d2.setHours(0, 0, 0, 0);
    }

    return d1.getTime() === d2.getTime();
  },

  /**
   * Validates a date object
   * @param {Date} date - an object instantiated with the new Date constructor
   * @return {Boolean}
   */
  isValidDate: function isValidDate(date) {
    if (Object.prototype.toString.call(date) !== '[object Date]') {
      return false;
    }

    return !isNaN(date.getTime());
  },

  /**
   * Return abbreviated week day name
   * @param {Date}
   * @param {Array}
   * @return {String}
   */
  getDayNameAbbr: function getDayNameAbbr(date, days) {
    if (_typeof(date) !== 'object') {
      throw TypeError('Invalid Type');
    }

    return days[this.getDay(date)];
  },

  /**
   * Return name of the month
   * @param {Number|Date}
   * @param {Array}
   * @return {String}
   */
  getMonthName: function getMonthName(month, months) {
    if (!months) {
      throw Error('missing 2nd parameter Months array');
    }

    if (_typeof(month) === 'object') {
      return months[this.getMonth(month)];
    }

    if (typeof month === 'number') {
      return months[month];
    }

    throw TypeError('Invalid type');
  },

  /**
   * Return an abbreviated version of the month
   * @param {Number|Date}
   * @return {String}
   */
  getMonthNameAbbr: function getMonthNameAbbr(month, monthsAbbr) {
    if (!monthsAbbr) {
      throw Error('missing 2nd paramter Months array');
    }

    if (_typeof(month) === 'object') {
      return monthsAbbr[this.getMonth(month)];
    }

    if (typeof month === 'number') {
      return monthsAbbr[month];
    }

    throw TypeError('Invalid type');
  },

  /**
   * Alternative get total number of days in month
   * @param {Number} year
   * @param {Number} m
   * @return {Number}
   */
  daysInMonth: function daysInMonth(year, month) {
    return /8|3|5|10/.test(month) ? 30 : month === 1 ? !(year % 4) && year % 100 || !(year % 400) ? 29 : 28 : 31;
  },

  /**
   * Get nth suffix for date
   * @param {Number} day
   * @return {String}
   */
  getNthSuffix: function getNthSuffix(day) {
    switch (day) {
      case 1:
      case 21:
      case 31:
        return 'st';

      case 2:
      case 22:
        return 'nd';

      case 3:
      case 23:
        return 'rd';

      default:
        return 'th';
    }
  },

  /**
   * Formats date object
   * @param {Date}
   * @param {String}
   * @param {Object}
   * @return {String}
   */
  formatDate: function formatDate(date, format, translation) {
    translation = !translation ? en : translation;
    var year = this.getFullYear(date);
    var month = this.getMonth(date) + 1;
    var day = this.getDate(date);
    var str = format.replace(/dd/, ('0' + day).slice(-2)).replace(/d/, day).replace(/yyyy/, year).replace(/yy/, String(year).slice(2)).replace(/MMMM/, this.getMonthName(this.getMonth(date), translation.months)).replace(/MMM/, this.getMonthNameAbbr(this.getMonth(date), translation.monthsAbbr)).replace(/MM/, ('0' + month).slice(-2)).replace(/M(?!a|ä|e)/, month).replace(/su/, this.getNthSuffix(this.getDate(date))).replace(/D(?!e|é|i)/, this.getDayNameAbbr(date, translation.days));
    return str;
  },

  /**
   * Creates an array of dates for each day in between two dates.
   * @param {Date} start
   * @param {Date} end
   * @return {Array}
   */
  createDateArray: function createDateArray(start, end) {
    var dates = [];

    while (start <= end) {
      dates.push(new Date(start));
      start = this.setDate(new Date(start), this.getDate(new Date(start)) + 1);
    }

    return dates;
  },

  /**
   * method used as a prop validator for input values
   * @param {*} val
   * @return {Boolean}
   */
  validateDateInput: function validateDateInput(val) {
    return val === null || val instanceof Date || typeof val === 'string' || typeof val === 'number';
  }
};
var makeDateUtils = function makeDateUtils(useUtc) {
  return _objectSpread({}, utils, {
    useUtc: useUtc
  });
};
var utils$1 = _objectSpread({}, utils) // eslint-disable-next-line
;

var script = {
  props: {
    selectedDate: Date,
    resetTypedDate: [Date],
    format: [String, Function],
    translation: Object,
    inline: Boolean,
    id: String,
    name: String,
    refName: String,
    openDate: Date,
    placeholder: String,
    inputClass: [String, Object, Array],
    clearButton: Boolean,
    clearButtonIcon: String,
    calendarButton: Boolean,
    calendarButtonIcon: String,
    calendarButtonIconContent: String,
    disabled: Boolean,
    required: Boolean,
    typeable: Boolean,
    bootstrapStyling: Boolean,
    useUtc: Boolean
  },
  data: function data() {
    var constructedDateUtils = makeDateUtils(this.useUtc);
    return {
      input: null,
      typedDate: false,
      utils: constructedDateUtils
    };
  },
  computed: {
    formattedValue: function formattedValue() {
      if (!this.selectedDate) {
        return null;
      }

      if (this.typedDate) {
        return this.typedDate;
      }

      return typeof this.format === 'function' ? this.format(this.selectedDate) : this.utils.formatDate(new Date(this.selectedDate), this.format, this.translation);
    },
    computedInputClass: function computedInputClass() {
      if (this.bootstrapStyling) {
        if (typeof this.inputClass === 'string') {
          return [this.inputClass, 'form-control'].join(' ');
        }

        return _objectSpread({
          'form-control': true
        }, this.inputClass);
      }

      return this.inputClass;
    }
  },
  watch: {
    resetTypedDate: function resetTypedDate() {
      this.typedDate = false;
    }
  },
  methods: {
    showCalendar: function showCalendar() {
      this.$emit('showCalendar');
    },

    /**
     * Attempt to parse a typed date
     * @param {Event} event
     */
    parseTypedDate: function parseTypedDate(event) {
      // close calendar if escape or enter are pressed
      if ([27, // escape
      13 // enter
      ].includes(event.keyCode)) {
        this.input.blur();
      }

      if (this.typeable) {
        var typedDate = Date.parse(this.input.value);

        if (!isNaN(typedDate)) {
          this.typedDate = this.input.value;
          this.$emit('typedDate', new Date(this.typedDate));
        }
      }
    },

    /**
     * nullify the typed date to defer to regular formatting
     * called once the input is blurred
     */
    inputBlurred: function inputBlurred() {
      if (this.typeable && isNaN(Date.parse(this.input.value))) {
        this.clearDate();
        this.input.value = null;
        this.typedDate = null;
      }

      this.$emit('closeCalendar');
    },

    /**
     * emit a clearDate event
     */
    clearDate: function clearDate() {
      this.$emit('clearDate');
    }
  },
  mounted: function mounted() {
    this.input = this.$el.querySelector('input');
  }
} // eslint-disable-next-line
;

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

/* script */
const __vue_script__ = script;

/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { class: { "input-group": _vm.bootstrapStyling } },
    [
      _vm.calendarButton
        ? _c(
            "span",
            {
              staticClass: "vdp-datepicker__calendar-button",
              class: { "input-group-prepend": _vm.bootstrapStyling },
              style: { "cursor:not-allowed;": _vm.disabled },
              on: { click: _vm.showCalendar }
            },
            [
              _c(
                "span",
                { class: { "input-group-text": _vm.bootstrapStyling } },
                [
                  _c("i", { class: _vm.calendarButtonIcon }, [
                    _vm._v(
                      "\n        " +
                        _vm._s(_vm.calendarButtonIconContent) +
                        "\n        "
                    ),
                    !_vm.calendarButtonIcon
                      ? _c("span", [_vm._v("…")])
                      : _vm._e()
                  ])
                ]
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("input", {
        ref: _vm.refName,
        class: _vm.computedInputClass,
        attrs: {
          type: _vm.inline ? "hidden" : "text",
          name: _vm.name,
          id: _vm.id,
          "open-date": _vm.openDate,
          placeholder: _vm.placeholder,
          "clear-button": _vm.clearButton,
          disabled: _vm.disabled,
          required: _vm.required,
          readonly: !_vm.typeable,
          autocomplete: "off"
        },
        domProps: { value: _vm.formattedValue },
        on: {
          click: _vm.showCalendar,
          keyup: _vm.parseTypedDate,
          blur: _vm.inputBlurred
        }
      }),
      _vm._v(" "),
      _vm.clearButton && _vm.selectedDate
        ? _c(
            "span",
            {
              staticClass: "vdp-datepicker__clear-button",
              class: { "input-group-append": _vm.bootstrapStyling },
              on: {
                click: function($event) {
                  return _vm.clearDate()
                }
              }
            },
            [
              _c(
                "span",
                { class: { "input-group-text": _vm.bootstrapStyling } },
                [
                  _c("i", { class: _vm.clearButtonIcon }, [
                    !_vm.clearButtonIcon ? _c("span", [_vm._v("×")]) : _vm._e()
                  ])
                ]
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm._t("afterDateInput")
    ],
    2
  )
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  const __vue_inject_styles__ = undefined;
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var DateInput = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    undefined,
    undefined
  );

//
var script$1 = {
  props: {
    showDayView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    fullMonthName: Boolean,
    allowedToShowView: Function,
    dayCellContent: {
      type: Function,
      "default": function _default(day) {
        return day.date;
      }
    },
    disabledDates: Object,
    highlighted: Object,
    calendarClass: [String, Object, Array],
    calendarStyle: Object,
    translation: Object,
    isRtl: Boolean,
    mondayFirst: Boolean,
    useUtc: Boolean
  },
  data: function data() {
    var constructedDateUtils = makeDateUtils(this.useUtc);
    return {
      utils: constructedDateUtils
    };
  },
  computed: {
    /**
     * Returns an array of day names
     * @return {String[]}
     */
    daysOfWeek: function daysOfWeek() {
      if (this.mondayFirst) {
        var tempDays = this.translation.days.slice();
        tempDays.push(tempDays.shift());
        return tempDays;
      }

      return this.translation.days;
    },

    /**
     * Returns the day number of the week less one for the first of the current month
     * Used to show amount of empty cells before the first in the day calendar layout
     * @return {Number}
     */
    blankDays: function blankDays() {
      var d = this.pageDate;
      var dObj = this.useUtc ? new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), 1)) : new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());

      if (this.mondayFirst) {
        return this.utils.getDay(dObj) > 0 ? this.utils.getDay(dObj) - 1 : 6;
      }

      return this.utils.getDay(dObj);
    },

    /**
     * @return {Object[]}
     */
    days: function days() {
      var d = this.pageDate;
      var days = []; // set up a new date object to the beginning of the current 'page'

      var dObj = this.useUtc ? new Date(Date.UTC(d.getUTCFullYear(), d.getUTCMonth(), 1)) : new Date(d.getFullYear(), d.getMonth(), 1, d.getHours(), d.getMinutes());
      var daysInMonth = this.utils.daysInMonth(this.utils.getFullYear(dObj), this.utils.getMonth(dObj));

      for (var i = 0; i < daysInMonth; i++) {
        days.push({
          date: this.utils.getDate(dObj),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedDate(dObj),
          isDisabled: this.isDisabledDate(dObj),
          isHighlighted: this.isHighlightedDate(dObj),
          isHighlightStart: this.isHighlightStart(dObj),
          isHighlightEnd: this.isHighlightEnd(dObj),
          isToday: this.utils.compareDates(dObj, new Date()),
          isWeekend: this.utils.getDay(dObj) === 0 || this.utils.getDay(dObj) === 6,
          isSaturday: this.utils.getDay(dObj) === 6,
          isSunday: this.utils.getDay(dObj) === 0
        });
        this.utils.setDate(dObj, this.utils.getDate(dObj) + 1);
      }

      return days;
    },

    /**
     * Gets the name of the month the current page is on
     * @return {String}
     */
    currMonthName: function currMonthName() {
      var monthName = this.fullMonthName ? this.translation.months : this.translation.monthsAbbr;
      return this.utils.getMonthNameAbbr(this.utils.getMonth(this.pageDate), monthName);
    },

    /**
     * Gets the name of the year that current page is on
     * @return {Number}
     */
    currYearName: function currYearName() {
      var yearSuffix = this.translation.yearSuffix;
      return "".concat(this.utils.getFullYear(this.pageDate)).concat(yearSuffix);
    },

    /**
     * Is this translation using year/month/day format?
     * @return {Boolean}
     */
    isYmd: function isYmd() {
      return this.translation.ymd && this.translation.ymd === true;
    },

    /**
     * Is the left hand navigation button disabled?
     * @return {Boolean}
     */
    isLeftNavDisabled: function isLeftNavDisabled() {
      return this.isRtl ? this.isNextMonthDisabled(this.pageTimestamp) : this.isPreviousMonthDisabled(this.pageTimestamp);
    },

    /**
     * Is the right hand navigation button disabled?
     * @return {Boolean}
     */
    isRightNavDisabled: function isRightNavDisabled() {
      return this.isRtl ? this.isPreviousMonthDisabled(this.pageTimestamp) : this.isNextMonthDisabled(this.pageTimestamp);
    }
  },
  methods: {
    selectDate: function selectDate(date) {
      if (date.isDisabled) {
        this.$emit('selectedDisabled', date);
        return false;
      }

      this.$emit('selectDate', date);
    },

    /**
     * @return {Number}
     */
    getPageMonth: function getPageMonth() {
      return this.utils.getMonth(this.pageDate);
    },

    /**
     * Emit an event to show the month picker
     */
    showMonthCalendar: function showMonthCalendar() {
      this.$emit('showMonthCalendar');
    },

    /**
     * Change the page month
     * @param {Number} incrementBy
     */
    changeMonth: function changeMonth(incrementBy) {
      var date = this.pageDate;
      this.utils.setMonth(date, this.utils.getMonth(date) + incrementBy);
      this.$emit('changedMonth', date);
    },

    /**
     * Decrement the page month
     */
    previousMonth: function previousMonth() {
      if (!this.isPreviousMonthDisabled()) {
        this.changeMonth(-1);
      }
    },

    /**
     * Is the previous month disabled?
     * @return {Boolean}
     */
    isPreviousMonthDisabled: function isPreviousMonthDisabled() {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false;
      }

      var d = this.pageDate;
      return this.utils.getMonth(this.disabledDates.to) >= this.utils.getMonth(d) && this.utils.getFullYear(this.disabledDates.to) >= this.utils.getFullYear(d);
    },

    /**
     * Increment the current page month
     */
    nextMonth: function nextMonth() {
      if (!this.isNextMonthDisabled()) {
        this.changeMonth(+1);
      }
    },

    /**
     * Is the next month disabled?
     * @return {Boolean}
     */
    isNextMonthDisabled: function isNextMonthDisabled() {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false;
      }

      var d = this.pageDate;
      return this.utils.getMonth(this.disabledDates.from) <= this.utils.getMonth(d) && this.utils.getFullYear(this.disabledDates.from) <= this.utils.getFullYear(d);
    },

    /**
     * Whether a day is selected
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedDate: function isSelectedDate(dObj) {
      return this.selectedDate && this.utils.compareDates(this.selectedDate, dObj);
    },

    /**
     * Whether a day is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledDate: function isDisabledDate(date) {
      var _this = this;

      var disabledDates = false;

      if (typeof this.disabledDates === 'undefined') {
        return false;
      }

      if (typeof this.disabledDates.dates !== 'undefined') {
        this.disabledDates.dates.forEach(function (d) {
          if (_this.utils.compareDates(date, d)) {
            disabledDates = true;
            return true;
          }
        });
      }

      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to && date < this.disabledDates.to) {
        disabledDates = true;
      }

      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from && date > this.disabledDates.from) {
        disabledDates = true;
      }

      if (typeof this.disabledDates.ranges !== 'undefined') {
        this.disabledDates.ranges.forEach(function (range) {
          if (typeof range.from !== 'undefined' && range.from && typeof range.to !== 'undefined' && range.to) {
            if (date < range.to && date > range.from) {
              disabledDates = true;
              return true;
            }
          }
        });
      }

      if (typeof this.disabledDates.days !== 'undefined' && this.disabledDates.days.indexOf(this.utils.getDay(date)) !== -1) {
        disabledDates = true;
      }

      if (typeof this.disabledDates.daysOfMonth !== 'undefined' && this.disabledDates.daysOfMonth.indexOf(this.utils.getDate(date)) !== -1) {
        disabledDates = true;
      }

      if (typeof this.disabledDates.customPredictor === 'function' && this.disabledDates.customPredictor(date)) {
        disabledDates = true;
      }

      return disabledDates;
    },

    /**
     * Whether a day is highlighted (only if it is not disabled already except when highlighted.includeDisabled is true)
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightedDate: function isHighlightedDate(date) {
      var _this2 = this;

      if (!(this.highlighted && this.highlighted.includeDisabled) && this.isDisabledDate(date)) {
        return false;
      }

      var highlighted = false;

      if (typeof this.highlighted === 'undefined') {
        return false;
      }

      if (typeof this.highlighted.dates !== 'undefined') {
        this.highlighted.dates.forEach(function (d) {
          if (_this2.utils.compareDates(date, d)) {
            highlighted = true;
            return true;
          }
        });
      }

      if (this.isDefined(this.highlighted.from) && this.isDefined(this.highlighted.to)) {
        highlighted = date >= this.highlighted.from && date <= this.highlighted.to;
      }

      if (typeof this.highlighted.days !== 'undefined' && this.highlighted.days.indexOf(this.utils.getDay(date)) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.daysOfMonth !== 'undefined' && this.highlighted.daysOfMonth.indexOf(this.utils.getDate(date)) !== -1) {
        highlighted = true;
      }

      if (typeof this.highlighted.customPredictor === 'function' && this.highlighted.customPredictor(date)) {
        highlighted = true;
      }

      return highlighted;
    },
    dayClasses: function dayClasses(day) {
      return {
        'selected': day.isSelected,
        'disabled': day.isDisabled,
        'highlighted': day.isHighlighted,
        'today': day.isToday,
        'weekend': day.isWeekend,
        'sat': day.isSaturday,
        'sun': day.isSunday,
        'highlight-start': day.isHighlightStart,
        'highlight-end': day.isHighlightEnd
      };
    },

    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightStart: function isHighlightStart(date) {
      return this.isHighlightedDate(date) && this.highlighted.from instanceof Date && this.utils.getFullYear(this.highlighted.from) === this.utils.getFullYear(date) && this.utils.getMonth(this.highlighted.from) === this.utils.getMonth(date) && this.utils.getDate(this.highlighted.from) === this.utils.getDate(date);
    },

    /**
     * Whether a day is highlighted and it is the first date
     * in the highlighted range of dates
     * @param {Date}
     * @return {Boolean}
     */
    isHighlightEnd: function isHighlightEnd(date) {
      return this.isHighlightedDate(date) && this.highlighted.to instanceof Date && this.utils.getFullYear(this.highlighted.to) === this.utils.getFullYear(date) && this.utils.getMonth(this.highlighted.to) === this.utils.getMonth(date) && this.utils.getDate(this.highlighted.to) === this.utils.getDate(date);
    },

    /**
     * Helper
     * @param  {mixed}  prop
     * @return {Boolean}
     */
    isDefined: function isDefined(prop) {
      return typeof prop !== 'undefined' && prop;
    }
  } // eslint-disable-next-line

};

/* script */
const __vue_script__$1 = script$1;

/* template */
var __vue_render__$1 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      directives: [
        {
          name: "show",
          rawName: "v-show",
          value: _vm.showDayView,
          expression: "showDayView"
        }
      ],
      class: [_vm.calendarClass, "vdp-datepicker__calendar"],
      style: _vm.calendarStyle,
      on: {
        mousedown: function($event) {
          $event.preventDefault();
        }
      }
    },
    [
      _vm._t("beforeCalendarHeader"),
      _vm._v(" "),
      _c("header", [
        _c(
          "span",
          {
            staticClass: "prev",
            class: { disabled: _vm.isLeftNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.nextMonth() : _vm.previousMonth();
              }
            }
          },
          [_vm._v("<")]
        ),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "day__month_btn",
            class: _vm.allowedToShowView("month") ? "up" : "",
            on: { click: _vm.showMonthCalendar }
          },
          [
            _vm._v(
              _vm._s(_vm.isYmd ? _vm.currYearName : _vm.currMonthName) +
                " " +
                _vm._s(_vm.isYmd ? _vm.currMonthName : _vm.currYearName)
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "next",
            class: { disabled: _vm.isRightNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.previousMonth() : _vm.nextMonth();
              }
            }
          },
          [_vm._v(">")]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { class: _vm.isRtl ? "flex-rtl" : "" },
        [
          _vm._l(_vm.daysOfWeek, function(d) {
            return _c(
              "span",
              { key: d.timestamp, staticClass: "cell day-header" },
              [_vm._v(_vm._s(d))]
            )
          }),
          _vm._v(" "),
          _vm.blankDays > 0
            ? _vm._l(_vm.blankDays, function(d) {
                return _c("span", {
                  key: d.timestamp,
                  staticClass: "cell day blank"
                })
              })
            : _vm._e(),
          _vm._l(_vm.days, function(day) {
            return _c("span", {
              key: day.timestamp,
              staticClass: "cell day",
              class: _vm.dayClasses(day),
              domProps: { innerHTML: _vm._s(_vm.dayCellContent(day)) },
              on: {
                click: function($event) {
                  return _vm.selectDate(day)
                }
              }
            })
          })
        ],
        2
      )
    ],
    2
  )
};
var __vue_staticRenderFns__$1 = [];
__vue_render__$1._withStripped = true;

  /* style */
  const __vue_inject_styles__$1 = undefined;
  /* scoped */
  const __vue_scope_id__$1 = undefined;
  /* module identifier */
  const __vue_module_identifier__$1 = undefined;
  /* functional template */
  const __vue_is_functional_template__$1 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var PickerDay = normalizeComponent_1(
    { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
    __vue_inject_styles__$1,
    __vue_script__$1,
    __vue_scope_id__$1,
    __vue_is_functional_template__$1,
    __vue_module_identifier__$1,
    undefined,
    undefined
  );

//
var script$2 = {
  props: {
    showMonthView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    disabledDates: Object,
    calendarClass: [String, Object, Array],
    calendarStyle: Object,
    translation: Object,
    isRtl: Boolean,
    allowedToShowView: Function,
    useUtc: Boolean
  },
  data: function data() {
    var constructedDateUtils = makeDateUtils(this.useUtc);
    return {
      utils: constructedDateUtils
    };
  },
  computed: {
    months: function months() {
      var d = this.pageDate;
      var months = []; // set up a new date object to the beginning of the current 'page'

      var dObj = this.useUtc ? new Date(Date.UTC(d.getUTCFullYear(), 0, d.getUTCDate())) : new Date(d.getFullYear(), 0, d.getDate(), d.getHours(), d.getMinutes());

      for (var i = 0; i < 12; i++) {
        months.push({
          month: this.utils.getMonthName(i, this.translation.months),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedMonth(dObj),
          isDisabled: this.isDisabledMonth(dObj)
        });
        this.utils.setMonth(dObj, this.utils.getMonth(dObj) + 1);
      }

      return months;
    },

    /**
     * Get year name on current page.
     * @return {String}
     */
    pageYearName: function pageYearName() {
      var yearSuffix = this.translation.yearSuffix;
      return "".concat(this.utils.getFullYear(this.pageDate)).concat(yearSuffix);
    },

    /**
     * Is the left hand navigation disabled
     * @return {Boolean}
     */
    isLeftNavDisabled: function isLeftNavDisabled() {
      return this.isRtl ? this.isNextYearDisabled(this.pageTimestamp) : this.isPreviousYearDisabled(this.pageTimestamp);
    },

    /**
     * Is the right hand navigation disabled
     * @return {Boolean}
     */
    isRightNavDisabled: function isRightNavDisabled() {
      return this.isRtl ? this.isPreviousYearDisabled(this.pageTimestamp) : this.isNextYearDisabled(this.pageTimestamp);
    }
  },
  methods: {
    /**
     * Emits a selectMonth event
     * @param {Object} month
     */
    selectMonth: function selectMonth(month) {
      if (month.isDisabled) {
        return false;
      }

      this.$emit('selectMonth', month);
    },

    /**
     * Changes the year up or down
     * @param {Number} incrementBy
     */
    changeYear: function changeYear(incrementBy) {
      var date = this.pageDate;
      this.utils.setFullYear(date, this.utils.getFullYear(date) + incrementBy);
      this.$emit('changedYear', date);
    },

    /**
     * Decrements the year
     */
    previousYear: function previousYear() {
      if (!this.isPreviousYearDisabled()) {
        this.changeYear(-1);
      }
    },

    /**
     * Checks if the previous year is disabled or not
     * @return {Boolean}
     */
    isPreviousYearDisabled: function isPreviousYearDisabled() {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false;
      }

      return this.utils.getFullYear(this.disabledDates.to) >= this.utils.getFullYear(this.pageDate);
    },

    /**
     * Increments the year
     */
    nextYear: function nextYear() {
      if (!this.isNextYearDisabled()) {
        this.changeYear(1);
      }
    },

    /**
     * Checks if the next year is disabled or not
     * @return {Boolean}
     */
    isNextYearDisabled: function isNextYearDisabled() {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false;
      }

      return this.utils.getFullYear(this.disabledDates.from) <= this.utils.getFullYear(this.pageDate);
    },

    /**
     * Emits an event that shows the year calendar
     */
    showYearCalendar: function showYearCalendar() {
      this.$emit('showYearCalendar');
    },

    /**
     * Whether the selected date is in this month
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedMonth: function isSelectedMonth(date) {
      return this.selectedDate && this.utils.getFullYear(this.selectedDate) === this.utils.getFullYear(date) && this.utils.getMonth(this.selectedDate) === this.utils.getMonth(date);
    },

    /**
     * Whether a month is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledMonth: function isDisabledMonth(date) {
      var disabledDates = false;

      if (typeof this.disabledDates === 'undefined') {
        return false;
      }

      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to) {
        if (this.utils.getMonth(date) < this.utils.getMonth(this.disabledDates.to) && this.utils.getFullYear(date) <= this.utils.getFullYear(this.disabledDates.to) || this.utils.getFullYear(date) < this.utils.getFullYear(this.disabledDates.to)) {
          disabledDates = true;
        }
      }

      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from) {
        if (this.utils.getMonth(date) > this.utils.getMonth(this.disabledDates.from) && this.utils.getFullYear(date) >= this.utils.getFullYear(this.disabledDates.from) || this.utils.getFullYear(date) > this.utils.getFullYear(this.disabledDates.from)) {
          disabledDates = true;
        }
      }

      if (typeof this.disabledDates.customPredictor === 'function' && this.disabledDates.customPredictor(date)) {
        disabledDates = true;
      }

      return disabledDates;
    }
  } // eslint-disable-next-line

};

/* script */
const __vue_script__$2 = script$2;

/* template */
var __vue_render__$2 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      directives: [
        {
          name: "show",
          rawName: "v-show",
          value: _vm.showMonthView,
          expression: "showMonthView"
        }
      ],
      class: [_vm.calendarClass, "vdp-datepicker__calendar"],
      style: _vm.calendarStyle,
      on: {
        mousedown: function($event) {
          $event.preventDefault();
        }
      }
    },
    [
      _vm._t("beforeCalendarHeader"),
      _vm._v(" "),
      _c("header", [
        _c(
          "span",
          {
            staticClass: "prev",
            class: { disabled: _vm.isLeftNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.nextYear() : _vm.previousYear();
              }
            }
          },
          [_vm._v("<")]
        ),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "month__year_btn",
            class: _vm.allowedToShowView("year") ? "up" : "",
            on: { click: _vm.showYearCalendar }
          },
          [_vm._v(_vm._s(_vm.pageYearName))]
        ),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "next",
            class: { disabled: _vm.isRightNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.previousYear() : _vm.nextYear();
              }
            }
          },
          [_vm._v(">")]
        )
      ]),
      _vm._v(" "),
      _vm._l(_vm.months, function(month) {
        return _c(
          "span",
          {
            key: month.timestamp,
            staticClass: "cell month",
            class: { selected: month.isSelected, disabled: month.isDisabled },
            on: {
              click: function($event) {
                $event.stopPropagation();
                return _vm.selectMonth(month)
              }
            }
          },
          [_vm._v(_vm._s(month.month))]
        )
      })
    ],
    2
  )
};
var __vue_staticRenderFns__$2 = [];
__vue_render__$2._withStripped = true;

  /* style */
  const __vue_inject_styles__$2 = undefined;
  /* scoped */
  const __vue_scope_id__$2 = undefined;
  /* module identifier */
  const __vue_module_identifier__$2 = undefined;
  /* functional template */
  const __vue_is_functional_template__$2 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var PickerMonth = normalizeComponent_1(
    { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
    __vue_inject_styles__$2,
    __vue_script__$2,
    __vue_scope_id__$2,
    __vue_is_functional_template__$2,
    __vue_module_identifier__$2,
    undefined,
    undefined
  );

//
var script$3 = {
  props: {
    showYearView: Boolean,
    selectedDate: Date,
    pageDate: Date,
    pageTimestamp: Number,
    disabledDates: Object,
    highlighted: Object,
    calendarClass: [String, Object, Array],
    calendarStyle: Object,
    translation: Object,
    isRtl: Boolean,
    allowedToShowView: Function,
    useUtc: Boolean
  },
  computed: {
    years: function years() {
      var d = this.pageDate;
      var years = []; // set up a new date object to the beginning of the current 'page'7

      var dObj = this.useUtc ? new Date(Date.UTC(Math.floor(d.getUTCFullYear() / 10) * 10, d.getUTCMonth(), d.getUTCDate())) : new Date(Math.floor(d.getFullYear() / 10) * 10, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());

      for (var i = 0; i < 10; i++) {
        years.push({
          year: this.utils.getFullYear(dObj),
          timestamp: dObj.getTime(),
          isSelected: this.isSelectedYear(dObj),
          isDisabled: this.isDisabledYear(dObj)
        });
        this.utils.setFullYear(dObj, this.utils.getFullYear(dObj) + 1);
      }

      return years;
    },

    /**
     * @return {String}
     */
    getPageDecade: function getPageDecade() {
      var decadeStart = Math.floor(this.utils.getFullYear(this.pageDate) / 10) * 10;
      var decadeEnd = decadeStart + 9;
      var yearSuffix = this.translation.yearSuffix;
      return "".concat(decadeStart, " - ").concat(decadeEnd).concat(yearSuffix);
    },

    /**
     * Is the left hand navigation button disabled?
     * @return {Boolean}
     */
    isLeftNavDisabled: function isLeftNavDisabled() {
      return this.isRtl ? this.isNextDecadeDisabled(this.pageTimestamp) : this.isPreviousDecadeDisabled(this.pageTimestamp);
    },

    /**
     * Is the right hand navigation button disabled?
     * @return {Boolean}
     */
    isRightNavDisabled: function isRightNavDisabled() {
      return this.isRtl ? this.isPreviousDecadeDisabled(this.pageTimestamp) : this.isNextDecadeDisabled(this.pageTimestamp);
    }
  },
  data: function data() {
    var constructedDateUtils = makeDateUtils(this.useUtc);
    return {
      utils: constructedDateUtils
    };
  },
  methods: {
    selectYear: function selectYear(year) {
      if (year.isDisabled) {
        return false;
      }

      this.$emit('selectYear', year);
    },
    changeYear: function changeYear(incrementBy) {
      var date = this.pageDate;
      this.utils.setFullYear(date, this.utils.getFullYear(date) + incrementBy);
      this.$emit('changedDecade', date);
    },
    previousDecade: function previousDecade() {
      if (this.isPreviousDecadeDisabled()) {
        return false;
      }

      this.changeYear(-10);
    },
    isPreviousDecadeDisabled: function isPreviousDecadeDisabled() {
      if (!this.disabledDates || !this.disabledDates.to) {
        return false;
      }

      var disabledYear = this.utils.getFullYear(this.disabledDates.to);
      var lastYearInPreviousPage = Math.floor(this.utils.getFullYear(this.pageDate) / 10) * 10 - 1;
      return disabledYear > lastYearInPreviousPage;
    },
    nextDecade: function nextDecade() {
      if (this.isNextDecadeDisabled()) {
        return false;
      }

      this.changeYear(10);
    },
    isNextDecadeDisabled: function isNextDecadeDisabled() {
      if (!this.disabledDates || !this.disabledDates.from) {
        return false;
      }

      var disabledYear = this.utils.getFullYear(this.disabledDates.from);
      var firstYearInNextPage = Math.ceil(this.utils.getFullYear(this.pageDate) / 10) * 10;
      return disabledYear < firstYearInNextPage;
    },

    /**
     * Whether the selected date is in this year
     * @param {Date}
     * @return {Boolean}
     */
    isSelectedYear: function isSelectedYear(date) {
      return this.selectedDate && this.utils.getFullYear(this.selectedDate) === this.utils.getFullYear(date);
    },

    /**
     * Whether a year is disabled
     * @param {Date}
     * @return {Boolean}
     */
    isDisabledYear: function isDisabledYear(date) {
      var disabledDates = false;

      if (typeof this.disabledDates === 'undefined' || !this.disabledDates) {
        return false;
      }

      if (typeof this.disabledDates.to !== 'undefined' && this.disabledDates.to) {
        if (this.utils.getFullYear(date) < this.utils.getFullYear(this.disabledDates.to)) {
          disabledDates = true;
        }
      }

      if (typeof this.disabledDates.from !== 'undefined' && this.disabledDates.from) {
        if (this.utils.getFullYear(date) > this.utils.getFullYear(this.disabledDates.from)) {
          disabledDates = true;
        }
      }

      if (typeof this.disabledDates.customPredictor === 'function' && this.disabledDates.customPredictor(date)) {
        disabledDates = true;
      }

      return disabledDates;
    }
  } // eslint-disable-next-line

};

/* script */
const __vue_script__$3 = script$3;

/* template */
var __vue_render__$3 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      directives: [
        {
          name: "show",
          rawName: "v-show",
          value: _vm.showYearView,
          expression: "showYearView"
        }
      ],
      class: [_vm.calendarClass, "vdp-datepicker__calendar"],
      style: _vm.calendarStyle,
      on: {
        mousedown: function($event) {
          $event.preventDefault();
        }
      }
    },
    [
      _vm._t("beforeCalendarHeader"),
      _vm._v(" "),
      _c("header", [
        _c(
          "span",
          {
            staticClass: "prev",
            class: { disabled: _vm.isLeftNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.nextDecade() : _vm.previousDecade();
              }
            }
          },
          [_vm._v("<")]
        ),
        _vm._v(" "),
        _c("span", [_vm._v(_vm._s(_vm.getPageDecade))]),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "next",
            class: { disabled: _vm.isRightNavDisabled },
            on: {
              click: function($event) {
                _vm.isRtl ? _vm.previousDecade() : _vm.nextDecade();
              }
            }
          },
          [_vm._v(">")]
        )
      ]),
      _vm._v(" "),
      _vm._l(_vm.years, function(year) {
        return _c(
          "span",
          {
            key: year.timestamp,
            staticClass: "cell year",
            class: { selected: year.isSelected, disabled: year.isDisabled },
            on: {
              click: function($event) {
                $event.stopPropagation();
                return _vm.selectYear(year)
              }
            }
          },
          [_vm._v(_vm._s(year.year))]
        )
      })
    ],
    2
  )
};
var __vue_staticRenderFns__$3 = [];
__vue_render__$3._withStripped = true;

  /* style */
  const __vue_inject_styles__$3 = undefined;
  /* scoped */
  const __vue_scope_id__$3 = undefined;
  /* module identifier */
  const __vue_module_identifier__$3 = undefined;
  /* functional template */
  const __vue_is_functional_template__$3 = false;
  /* style inject */
  
  /* style inject SSR */
  

  
  var PickerYear = normalizeComponent_1(
    { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
    __vue_inject_styles__$3,
    __vue_script__$3,
    __vue_scope_id__$3,
    __vue_is_functional_template__$3,
    __vue_module_identifier__$3,
    undefined,
    undefined
  );

//
var script$4 = {
  components: {
    DateInput: DateInput,
    PickerDay: PickerDay,
    PickerMonth: PickerMonth,
    PickerYear: PickerYear
  },
  props: {
    value: {
      validator: function validator(val) {
        return utils$1.validateDateInput(val);
      }
    },
    name: String,
    refName: String,
    id: String,
    format: {
      type: [String, Function],
      "default": 'dd MMM yyyy'
    },
    language: {
      type: Object,
      "default": function _default() {
        return en;
      }
    },
    openDate: {
      validator: function validator(val) {
        return utils$1.validateDateInput(val);
      }
    },
    dayCellContent: Function,
    fullMonthName: Boolean,
    disabledDates: Object,
    highlighted: Object,
    placeholder: String,
    inline: Boolean,
    calendarClass: [String, Object, Array],
    inputClass: [String, Object, Array],
    wrapperClass: [String, Object, Array],
    mondayFirst: Boolean,
    clearButton: Boolean,
    clearButtonIcon: String,
    calendarButton: Boolean,
    calendarButtonIcon: String,
    calendarButtonIconContent: String,
    bootstrapStyling: Boolean,
    initialView: String,
    disabled: Boolean,
    required: Boolean,
    typeable: Boolean,
    useUtc: Boolean,
    minimumView: {
      type: String,
      "default": 'day'
    },
    maximumView: {
      type: String,
      "default": 'year'
    }
  },
  data: function data() {
    var startDate = this.openDate ? new Date(this.openDate) : new Date();
    var constructedDateUtils = makeDateUtils(this.useUtc);
    var pageTimestamp = constructedDateUtils.setDate(startDate, 1);
    return {
      /*
       * Vue cannot observe changes to a Date Object so date must be stored as a timestamp
       * This represents the first day of the current viewing month
       * {Number}
       */
      pageTimestamp: pageTimestamp,

      /*
       * Selected Date
       * {Date}
       */
      selectedDate: null,

      /*
       * Flags to show calendar views
       * {Boolean}
       */
      showDayView: false,
      showMonthView: false,
      showYearView: false,

      /*
       * Positioning
       */
      calendarHeight: 0,
      resetTypedDate: new Date(),
      utils: constructedDateUtils
    };
  },
  watch: {
    value: function value(_value) {
      this.setValue(_value);
    },
    openDate: function openDate() {
      this.setPageDate();
    },
    initialView: function initialView() {
      this.setInitialView();
    }
  },
  computed: {
    computedInitialView: function computedInitialView() {
      if (!this.initialView) {
        return this.minimumView;
      }

      return this.initialView;
    },
    pageDate: function pageDate() {
      return new Date(this.pageTimestamp);
    },
    translation: function translation() {
      return this.language;
    },
    calendarStyle: function calendarStyle() {
      return {
        position: this.isInline ? 'static' : undefined
      };
    },
    isOpen: function isOpen() {
      return this.showDayView || this.showMonthView || this.showYearView;
    },
    isInline: function isInline() {
      return !!this.inline;
    },
    isRtl: function isRtl() {
      return this.translation.rtl === true;
    }
  },
  methods: {
    /**
     * Called in the event that the user navigates to date pages and
     * closes the picker without selecting a date.
     */
    resetDefaultPageDate: function resetDefaultPageDate() {
      if (this.selectedDate === null) {
        this.setPageDate();
        return;
      }

      this.setPageDate(this.selectedDate);
    },

    /**
     * Effectively a toggle to show/hide the calendar
     * @return {mixed}
     */
    showCalendar: function showCalendar() {
      if (this.disabled || this.isInline) {
        return false;
      }

      if (this.isOpen) {
        return this.close(true);
      }

      this.setInitialView();
    },

    /**
     * Sets the initial picker page view: day, month or year
     */
    setInitialView: function setInitialView() {
      var initialView = this.computedInitialView;

      if (!this.allowedToShowView(initialView)) {
        throw new Error("initialView '".concat(this.initialView, "' cannot be rendered based on minimum '").concat(this.minimumView, "' and maximum '").concat(this.maximumView, "'"));
      }

      switch (initialView) {
        case 'year':
          this.showYearCalendar();
          break;

        case 'month':
          this.showMonthCalendar();
          break;

        default:
          this.showDayCalendar();
          break;
      }
    },

    /**
     * Are we allowed to show a specific picker view?
     * @param {String} view
     * @return {Boolean}
     */
    allowedToShowView: function allowedToShowView(view) {
      var views = ['day', 'month', 'year'];
      var minimumViewIndex = views.indexOf(this.minimumView);
      var maximumViewIndex = views.indexOf(this.maximumView);
      var viewIndex = views.indexOf(view);
      return viewIndex >= minimumViewIndex && viewIndex <= maximumViewIndex;
    },

    /**
     * Show the day picker
     * @return {Boolean}
     */
    showDayCalendar: function showDayCalendar() {
      if (!this.allowedToShowView('day')) {
        return false;
      }

      this.close();
      this.showDayView = true;
      return true;
    },

    /**
     * Show the month picker
     * @return {Boolean}
     */
    showMonthCalendar: function showMonthCalendar() {
      if (!this.allowedToShowView('month')) {
        return false;
      }

      this.close();
      this.showMonthView = true;
      return true;
    },

    /**
     * Show the year picker
     * @return {Boolean}
     */
    showYearCalendar: function showYearCalendar() {
      if (!this.allowedToShowView('year')) {
        return false;
      }

      this.close();
      this.showYearView = true;
      return true;
    },

    /**
     * Set the selected date
     * @param {Number} timestamp
     */
    setDate: function setDate(timestamp) {
      var date = new Date(timestamp);
      this.selectedDate = date;
      this.setPageDate(date);
      this.$emit('selected', date);
      this.$emit('input', date);
    },

    /**
     * Clear the selected date
     */
    clearDate: function clearDate() {
      this.selectedDate = null;
      this.setPageDate();
      this.$emit('selected', null);
      this.$emit('input', null);
      this.$emit('cleared');
    },

    /**
     * @param {Object} date
     */
    selectDate: function selectDate(date) {
      this.setDate(date.timestamp);

      if (!this.isInline) {
        this.close(true);
      }

      this.resetTypedDate = new Date();
    },

    /**
     * @param {Object} date
     */
    selectDisabledDate: function selectDisabledDate(date) {
      this.$emit('selectedDisabled', date);
    },

    /**
     * @param {Object} month
     */
    selectMonth: function selectMonth(month) {
      var date = new Date(month.timestamp);

      if (this.allowedToShowView('day')) {
        this.setPageDate(date);
        this.$emit('changedMonth', month);
        this.showDayCalendar();
      } else {
        this.selectDate(month);
      }
    },

    /**
     * @param {Object} year
     */
    selectYear: function selectYear(year) {
      var date = new Date(year.timestamp);

      if (this.allowedToShowView('month')) {
        this.setPageDate(date);
        this.$emit('changedYear', year);
        this.showMonthCalendar();
      } else {
        this.selectDate(year);
      }
    },

    /**
     * Set the datepicker value
     * @param {Date|String|Number|null} date
     */
    setValue: function setValue(date) {
      if (typeof date === 'string' || typeof date === 'number') {
        var parsed = new Date(date);
        date = isNaN(parsed.valueOf()) ? null : parsed;
      }

      if (!date) {
        this.setPageDate();
        this.selectedDate = null;
        return;
      }

      this.selectedDate = date;
      this.setPageDate(date);
    },

    /**
     * Sets the date that the calendar should open on
     */
    setPageDate: function setPageDate(date) {
      if (!date) {
        if (this.openDate) {
          date = new Date(this.openDate);
        } else {
          date = new Date();
        }
      }

      this.pageTimestamp = this.utils.setDate(new Date(date), 1);
    },

    /**
     * Handles a month change from the day picker
     */
    handleChangedMonthFromDayPicker: function handleChangedMonthFromDayPicker(date) {
      this.setPageDate(date);
      this.$emit('changedMonth', date);
    },

    /**
     * Set the date from a typedDate event
     */
    setTypedDate: function setTypedDate(date) {
      this.setDate(date.getTime());
    },

    /**
     * Close all calendar layers
     * @param {Boolean} emitEvent - emit close event
     */
    close: function close(emitEvent) {
      this.showDayView = this.showMonthView = this.showYearView = false;

      if (!this.isInline) {
        if (emitEvent) {
          this.$emit('closed');
        }

        document.removeEventListener('click', this.clickOutside, false);
      }
    },

    /**
     * Initiate the component
     */
    init: function init() {
      if (this.value) {
        this.setValue(this.value);
      }

      if (this.isInline) {
        this.setInitialView();
      }
    }
  },
  mounted: function mounted() {
    this.init();
  }
} // eslint-disable-next-line
;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD = document.head || document.getElementsByTagName('head')[0];
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) style.element.setAttribute('media', css.media);
      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) style.element.removeChild(nodes[index]);
      if (nodes.length) style.element.insertBefore(textNode, nodes[index]);else style.element.appendChild(textNode);
    }
  }
}

var browser = createInjector;

/* script */
const __vue_script__$4 = script$4;

/* template */
var __vue_render__$4 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      staticClass: "vdp-datepicker",
      class: [_vm.wrapperClass, _vm.isRtl ? "rtl" : ""]
    },
    [
      _c(
        "date-input",
        {
          attrs: {
            selectedDate: _vm.selectedDate,
            resetTypedDate: _vm.resetTypedDate,
            format: _vm.format,
            translation: _vm.translation,
            inline: _vm.inline,
            id: _vm.id,
            name: _vm.name,
            refName: _vm.refName,
            openDate: _vm.openDate,
            placeholder: _vm.placeholder,
            inputClass: _vm.inputClass,
            typeable: _vm.typeable,
            clearButton: _vm.clearButton,
            clearButtonIcon: _vm.clearButtonIcon,
            calendarButton: _vm.calendarButton,
            calendarButtonIcon: _vm.calendarButtonIcon,
            calendarButtonIconContent: _vm.calendarButtonIconContent,
            disabled: _vm.disabled,
            required: _vm.required,
            bootstrapStyling: _vm.bootstrapStyling,
            "use-utc": _vm.useUtc
          },
          on: {
            showCalendar: _vm.showCalendar,
            closeCalendar: _vm.close,
            typedDate: _vm.setTypedDate,
            clearDate: _vm.clearDate
          }
        },
        [_vm._t("afterDateInput", null, { slot: "afterDateInput" })],
        2
      ),
      _vm._v(" "),
      _vm.allowedToShowView("day")
        ? _c(
            "picker-day",
            {
              attrs: {
                pageDate: _vm.pageDate,
                selectedDate: _vm.selectedDate,
                showDayView: _vm.showDayView,
                fullMonthName: _vm.fullMonthName,
                allowedToShowView: _vm.allowedToShowView,
                disabledDates: _vm.disabledDates,
                highlighted: _vm.highlighted,
                calendarClass: _vm.calendarClass,
                calendarStyle: _vm.calendarStyle,
                translation: _vm.translation,
                pageTimestamp: _vm.pageTimestamp,
                isRtl: _vm.isRtl,
                mondayFirst: _vm.mondayFirst,
                dayCellContent: _vm.dayCellContent,
                "use-utc": _vm.useUtc
              },
              on: {
                changedMonth: _vm.handleChangedMonthFromDayPicker,
                selectDate: _vm.selectDate,
                showMonthCalendar: _vm.showMonthCalendar,
                selectedDisabled: _vm.selectDisabledDate
              }
            },
            [
              _vm._t("beforeCalendarHeader", null, {
                slot: "beforeCalendarHeader"
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.allowedToShowView("month")
        ? _c(
            "picker-month",
            {
              attrs: {
                pageDate: _vm.pageDate,
                selectedDate: _vm.selectedDate,
                showMonthView: _vm.showMonthView,
                allowedToShowView: _vm.allowedToShowView,
                disabledDates: _vm.disabledDates,
                calendarClass: _vm.calendarClass,
                calendarStyle: _vm.calendarStyle,
                translation: _vm.translation,
                isRtl: _vm.isRtl,
                "use-utc": _vm.useUtc
              },
              on: {
                selectMonth: _vm.selectMonth,
                showYearCalendar: _vm.showYearCalendar,
                changedYear: _vm.setPageDate
              }
            },
            [
              _vm._t("beforeCalendarHeader", null, {
                slot: "beforeCalendarHeader"
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.allowedToShowView("year")
        ? _c(
            "picker-year",
            {
              attrs: {
                pageDate: _vm.pageDate,
                selectedDate: _vm.selectedDate,
                showYearView: _vm.showYearView,
                allowedToShowView: _vm.allowedToShowView,
                disabledDates: _vm.disabledDates,
                calendarClass: _vm.calendarClass,
                calendarStyle: _vm.calendarStyle,
                translation: _vm.translation,
                isRtl: _vm.isRtl,
                "use-utc": _vm.useUtc
              },
              on: { selectYear: _vm.selectYear, changedDecade: _vm.setPageDate }
            },
            [
              _vm._t("beforeCalendarHeader", null, {
                slot: "beforeCalendarHeader"
              })
            ],
            2
          )
        : _vm._e()
    ],
    1
  )
};
var __vue_staticRenderFns__$4 = [];
__vue_render__$4._withStripped = true;

  /* style */
  const __vue_inject_styles__$4 = function (inject) {
    if (!inject) return
    inject("data-v-64ca2bb5_0", { source: ".rtl {\n  direction: rtl;\n}\n.vdp-datepicker {\n  position: relative;\n  text-align: left;\n}\n.vdp-datepicker * {\n  box-sizing: border-box;\n}\n.vdp-datepicker__calendar {\n  position: absolute;\n  z-index: 100;\n  background: #fff;\n  width: 300px;\n  border: 1px solid #ccc;\n}\n.vdp-datepicker__calendar header {\n  display: block;\n  line-height: 40px;\n}\n.vdp-datepicker__calendar header span {\n  display: inline-block;\n  text-align: center;\n  width: 71.42857142857143%;\n  float: left;\n}\n.vdp-datepicker__calendar header .prev,\n.vdp-datepicker__calendar header .next {\n  width: 14.285714285714286%;\n  float: left;\n  text-indent: -10000px;\n  position: relative;\n}\n.vdp-datepicker__calendar header .prev:after,\n.vdp-datepicker__calendar header .next:after {\n  content: '';\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translateX(-50%) translateY(-50%);\n  border: 6px solid transparent;\n}\n.vdp-datepicker__calendar header .prev:after {\n  border-right: 10px solid #000;\n  margin-left: -5px;\n}\n.vdp-datepicker__calendar header .prev.disabled:after {\n  border-right: 10px solid #ddd;\n}\n.vdp-datepicker__calendar header .next:after {\n  border-left: 10px solid #000;\n  margin-left: 5px;\n}\n.vdp-datepicker__calendar header .next.disabled:after {\n  border-left: 10px solid #ddd;\n}\n.vdp-datepicker__calendar header .prev:not(.disabled),\n.vdp-datepicker__calendar header .next:not(.disabled),\n.vdp-datepicker__calendar header .up:not(.disabled) {\n  cursor: pointer;\n}\n.vdp-datepicker__calendar header .prev:not(.disabled):hover,\n.vdp-datepicker__calendar header .next:not(.disabled):hover,\n.vdp-datepicker__calendar header .up:not(.disabled):hover {\n  background: #eee;\n}\n.vdp-datepicker__calendar .disabled {\n  color: #ddd;\n  cursor: default;\n}\n.vdp-datepicker__calendar .flex-rtl {\n  display: flex;\n  width: inherit;\n  flex-wrap: wrap;\n}\n.vdp-datepicker__calendar .cell {\n  display: inline-block;\n  padding: 0 5px;\n  width: 14.285714285714286%;\n  height: 40px;\n  line-height: 40px;\n  text-align: center;\n  vertical-align: middle;\n  border: 1px solid transparent;\n}\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year {\n  cursor: pointer;\n}\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day:hover,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month:hover,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year:hover {\n  border: 1px solid #4bd;\n}\n.vdp-datepicker__calendar .cell.selected {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.selected:hover {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.selected.highlighted {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.highlighted {\n  background: #cae5ed;\n}\n.vdp-datepicker__calendar .cell.highlighted.disabled {\n  color: #a3a3a3;\n}\n.vdp-datepicker__calendar .cell.grey {\n  color: #888;\n}\n.vdp-datepicker__calendar .cell.grey:hover {\n  background: inherit;\n}\n.vdp-datepicker__calendar .cell.day-header {\n  font-size: 75%;\n  white-space: nowrap;\n  cursor: inherit;\n}\n.vdp-datepicker__calendar .cell.day-header:hover {\n  background: inherit;\n}\n.vdp-datepicker__calendar .month,\n.vdp-datepicker__calendar .year {\n  width: 33.333%;\n}\n.vdp-datepicker__clear-button,\n.vdp-datepicker__calendar-button {\n  cursor: pointer;\n  font-style: normal;\n}\n.vdp-datepicker__clear-button.disabled,\n.vdp-datepicker__calendar-button.disabled {\n  color: #999;\n  cursor: default;\n}\n", map: {"version":3,"sources":["Datepicker.vue"],"names":[],"mappings":"AAAA;EACE,cAAc;AAChB;AACA;EACE,kBAAkB;EAClB,gBAAgB;AAClB;AACA;EACE,sBAAsB;AACxB;AACA;EACE,kBAAkB;EAClB,YAAY;EACZ,gBAAgB;EAChB,YAAY;EACZ,sBAAsB;AACxB;AACA;EACE,cAAc;EACd,iBAAiB;AACnB;AACA;EACE,qBAAqB;EACrB,kBAAkB;EAClB,yBAAyB;EACzB,WAAW;AACb;AACA;;EAEE,0BAA0B;EAC1B,WAAW;EACX,qBAAqB;EACrB,kBAAkB;AACpB;AACA;;EAEE,WAAW;EACX,kBAAkB;EAClB,SAAS;EACT,QAAQ;EACR,4CAA4C;EAC5C,6BAA6B;AAC/B;AACA;EACE,6BAA6B;EAC7B,iBAAiB;AACnB;AACA;EACE,6BAA6B;AAC/B;AACA;EACE,4BAA4B;EAC5B,gBAAgB;AAClB;AACA;EACE,4BAA4B;AAC9B;AACA;;;EAGE,eAAe;AACjB;AACA;;;EAGE,gBAAgB;AAClB;AACA;EACE,WAAW;EACX,eAAe;AACjB;AACA;EACE,aAAa;EACb,cAAc;EACd,eAAe;AACjB;AACA;EACE,qBAAqB;EACrB,cAAc;EACd,0BAA0B;EAC1B,YAAY;EACZ,iBAAiB;EACjB,kBAAkB;EAClB,sBAAsB;EACtB,6BAA6B;AAC/B;AACA;;;EAGE,eAAe;AACjB;AACA;;;EAGE,sBAAsB;AACxB;AACA;EACE,gBAAgB;AAClB;AACA;EACE,gBAAgB;AAClB;AACA;EACE,gBAAgB;AAClB;AACA;EACE,mBAAmB;AACrB;AACA;EACE,cAAc;AAChB;AACA;EACE,WAAW;AACb;AACA;EACE,mBAAmB;AACrB;AACA;EACE,cAAc;EACd,mBAAmB;EACnB,eAAe;AACjB;AACA;EACE,mBAAmB;AACrB;AACA;;EAEE,cAAc;AAChB;AACA;;EAEE,eAAe;EACf,kBAAkB;AACpB;AACA;;EAEE,WAAW;EACX,eAAe;AACjB","file":"Datepicker.vue","sourcesContent":[".rtl {\n  direction: rtl;\n}\n.vdp-datepicker {\n  position: relative;\n  text-align: left;\n}\n.vdp-datepicker * {\n  box-sizing: border-box;\n}\n.vdp-datepicker__calendar {\n  position: absolute;\n  z-index: 100;\n  background: #fff;\n  width: 300px;\n  border: 1px solid #ccc;\n}\n.vdp-datepicker__calendar header {\n  display: block;\n  line-height: 40px;\n}\n.vdp-datepicker__calendar header span {\n  display: inline-block;\n  text-align: center;\n  width: 71.42857142857143%;\n  float: left;\n}\n.vdp-datepicker__calendar header .prev,\n.vdp-datepicker__calendar header .next {\n  width: 14.285714285714286%;\n  float: left;\n  text-indent: -10000px;\n  position: relative;\n}\n.vdp-datepicker__calendar header .prev:after,\n.vdp-datepicker__calendar header .next:after {\n  content: '';\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translateX(-50%) translateY(-50%);\n  border: 6px solid transparent;\n}\n.vdp-datepicker__calendar header .prev:after {\n  border-right: 10px solid #000;\n  margin-left: -5px;\n}\n.vdp-datepicker__calendar header .prev.disabled:after {\n  border-right: 10px solid #ddd;\n}\n.vdp-datepicker__calendar header .next:after {\n  border-left: 10px solid #000;\n  margin-left: 5px;\n}\n.vdp-datepicker__calendar header .next.disabled:after {\n  border-left: 10px solid #ddd;\n}\n.vdp-datepicker__calendar header .prev:not(.disabled),\n.vdp-datepicker__calendar header .next:not(.disabled),\n.vdp-datepicker__calendar header .up:not(.disabled) {\n  cursor: pointer;\n}\n.vdp-datepicker__calendar header .prev:not(.disabled):hover,\n.vdp-datepicker__calendar header .next:not(.disabled):hover,\n.vdp-datepicker__calendar header .up:not(.disabled):hover {\n  background: #eee;\n}\n.vdp-datepicker__calendar .disabled {\n  color: #ddd;\n  cursor: default;\n}\n.vdp-datepicker__calendar .flex-rtl {\n  display: flex;\n  width: inherit;\n  flex-wrap: wrap;\n}\n.vdp-datepicker__calendar .cell {\n  display: inline-block;\n  padding: 0 5px;\n  width: 14.285714285714286%;\n  height: 40px;\n  line-height: 40px;\n  text-align: center;\n  vertical-align: middle;\n  border: 1px solid transparent;\n}\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year {\n  cursor: pointer;\n}\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day:hover,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month:hover,\n.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year:hover {\n  border: 1px solid #4bd;\n}\n.vdp-datepicker__calendar .cell.selected {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.selected:hover {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.selected.highlighted {\n  background: #4bd;\n}\n.vdp-datepicker__calendar .cell.highlighted {\n  background: #cae5ed;\n}\n.vdp-datepicker__calendar .cell.highlighted.disabled {\n  color: #a3a3a3;\n}\n.vdp-datepicker__calendar .cell.grey {\n  color: #888;\n}\n.vdp-datepicker__calendar .cell.grey:hover {\n  background: inherit;\n}\n.vdp-datepicker__calendar .cell.day-header {\n  font-size: 75%;\n  white-space: nowrap;\n  cursor: inherit;\n}\n.vdp-datepicker__calendar .cell.day-header:hover {\n  background: inherit;\n}\n.vdp-datepicker__calendar .month,\n.vdp-datepicker__calendar .year {\n  width: 33.333%;\n}\n.vdp-datepicker__clear-button,\n.vdp-datepicker__calendar-button {\n  cursor: pointer;\n  font-style: normal;\n}\n.vdp-datepicker__clear-button.disabled,\n.vdp-datepicker__calendar-button.disabled {\n  color: #999;\n  cursor: default;\n}\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$4 = undefined;
  /* module identifier */
  const __vue_module_identifier__$4 = undefined;
  /* functional template */
  const __vue_is_functional_template__$4 = false;
  /* style inject SSR */
  

  
  var Datepicker = normalizeComponent_1(
    { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
    __vue_inject_styles__$4,
    __vue_script__$4,
    __vue_scope_id__$4,
    __vue_is_functional_template__$4,
    __vue_module_identifier__$4,
    browser,
    undefined
  );

/* harmony default export */ __webpack_exports__["a"] = (Datepicker);


/***/ })

});