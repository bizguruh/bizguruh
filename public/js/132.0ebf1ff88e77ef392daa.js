webpackJsonp([132],{

/***/ 1788:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1789);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("13574560", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4316a2f8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privateChat.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4316a2f8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privateChat.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1789:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-4316a2f8] {\n  background: white;\n  height: 92vh;\n  padding: 30px;\n}\n.new_message[data-v-4316a2f8] {\n  font-size: 12px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.time[data-v-4316a2f8] {\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 10px;\n}\n.active[data-v-4316a2f8] {\n  background: white;\n  border-right: 5px solid #ccc;\n  border-bottom: none !important;\n  border-top: none !important;\n}\n.form-group[data-v-4316a2f8] {\n  position: relative;\n  overflow: hidden;\n}\n.bg-ccc[data-v-4316a2f8] {\n  background: #ccc;\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.submit[data-v-4316a2f8] {\n  width: 85%;\n  margin: 0 auto;\n}\n.search_icon[data-v-4316a2f8] {\n  position: absolute;\n  top: 50%;\n  right: 10px;\n  font-size: 16px;\n  margin-top: -8px;\n  color: rgba(0, 0, 0, 0.5);\n}\n.profile_img[data-v-4316a2f8] {\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  margin-right: 10px;\n}\n.align_image[data-v-4316a2f8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.chat_header[data-v-4316a2f8] {\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  padding-left: 20px;\n}\n.status[data-v-4316a2f8] {\n  font-size: 11px;\n  color: rgba(0, 0, 0, 0.54);\n  text-align: right;\n}\n.message-tab[data-v-4316a2f8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 100%;\n  border: 1px solid #f7f8fa;\n}\n.message-body[data-v-4316a2f8] {\n  height: 82%;\n  max-height: 82%;\n  overflow-y: scroll;\n  background: #333;\n}\n.message-body div li[data-v-4316a2f8] {\n  width: 50%;\n  padding: 10px 30px;\n  font-size: 15px;\n}\n.chat-left[data-v-4316a2f8] {\n   background-color: #d1e0ed;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  position: relative;\n   line-height: 1.2;\n}\n.chat-left[data-v-4316a2f8]::before {\n  content: \"\";\n   background-color: #d1e0ed;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(50deg);\n          transform: skew(50deg);\n  position: absolute;\n  left: -2px;\n  top: 0px;\n}\n.chat-right[data-v-4316a2f8] {\n  position: relative;\n  background: #f7f8fa;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: auto;\n  text-align: left;\n   line-height: 1.2;\n}\n.chat-right[data-v-4316a2f8]::before {\n  content: \"\";\n  background: #f7f8fa;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(-50deg);\n          transform: skew(-50deg);\n  position: absolute;\n  right: -2px;\n  top: 0px;\n}\n.form-group[data-v-4316a2f8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul[data-v-4316a2f8],\nol[data-v-4316a2f8],\nli[data-v-4316a2f8] {\n  list-style: none;\n}\n.users[data-v-4316a2f8] {\n  width: 30%;\n  padding: 15px 20px;\n  background: #f7f8fa;\n  height: 100%;\n  overflow: hidden;\n}\n.message[data-v-4316a2f8] {\n  height: 100%;\n  max-height: 100%;\n  width: 100%;\n  position: relative;\n}\n.user_name[data-v-4316a2f8] {\n  height: 100%;\n  max-height: 100%;\n  overflow-y: scroll;\n  font-size: 15px;\n}\n.user_name li[data-v-4316a2f8] {\n  padding: 10px 15px 10px;\n  border-bottom: 1px solid hsl(220, 23%, 90%);\n  cursor: pointer;\n}\n.user_name li[data-v-4316a2f8]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n.mobile[data-v-4316a2f8] {\n  display: none;\n}\n@media (max-width: 768px) {\n.main-page[data-v-4316a2f8] {\n    padding: 0;\n}\n.mobile[data-v-4316a2f8] {\n    display: block;\n}\n.chat_header[data-v-4316a2f8] {\n    font-size: 14px;\n    padding: 20px 10px !important;\n}\n.message-tab[data-v-4316a2f8] {\n    position: relative;\n}\n.message-body div li[data-v-4316a2f8] {\n    width: 70%;\n    padding: 10px;\n    font-size: 14px;\n}\n.submit[data-v-4316a2f8]{\n    width: 97%;\n}\n.pm[data-v-4316a2f8]{\n    font-size: 12px;\n}\n.user_name li[data-v-4316a2f8] {\n    padding: 15px 10px;\n    font-size: 12px;\n}\n.users[data-v-4316a2f8] {\n    width: 60%;\n    position: absolute;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    z-index: 1;\n    padding: 15px 10px;\n    -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n            box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n}\n.profile_img[data-v-4316a2f8] {\n    width: 20px;\n    height: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      messages: [],
      users: [],
      message: "",
      allVendors: [],
      vendor: [],
      id: null,
      active: 0,
      hide: false,
      search: "",
      activeUser: 0,
      new_message: "",
      status: ""
    };
  },
  mounted: function mounted() {
    this.getVendors();
    var user = JSON.parse(localStorage.getItem("authUser"));
    this.id = user.id;

    if (window.innerWidth < 768) {
      this.hide = true;
    } else {
      this.hide = false;
    }
  },


  computed: {
    sortUser: function sortUser() {
      var _this = this;

      return this.allVendors.filter(function (item) {
        return item.id == _this.$route.params.id;
      });
    },
    sortMessages: function sortMessages() {
      var _this2 = this;

      return this.messages.filter(function (item) {
        return item.vendor_user_id == _this2.activeUser;
      });
    }
  },
  methods: {
    showUsers: function showUsers() {
      this.hide = !this.hide;
    },
    getVendor: function getVendor(id, index) {
      var _this3 = this;

      if (window.innerWidth < 768) {
        this.hide = true;
      }
      var user = JSON.parse(localStorage.getItem("authUser"));

      Echo.private("chat." + id + user.id).listen("PrivateVendorMessages", function (e) {
        _this3.messages.push({
          message: e.message.message,
          created_at: e.message.created_at,
          status: e.message.status,
          vendor_user_id: e.message.vendor_user_id
        });
        _this3.new_message = e.message.vendor_user_id;
      });
      this.active = index;
      this.activeUser = id;
      this.new_message = "";
      this.vendor = this.allVendors.filter(function (item) {
        return item.id == id;
      });
    },
    getVendors: function getVendors() {
      var _this4 = this;

      axios.get("/api/get-all-vendor").then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            _this4.allVendors.push(item);
          });
          _this4.getVendor(_this4.allVendors[0].id, 0);
          _this4.fetchMessages();
        }
      });
    },
    fetchMessages: function fetchMessages() {
      var _this5 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/user-messages/" + this.vendor[0].id, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this5.messages = response.data;
        });
      }
    },
    sendMessage: function sendMessage() {
      var _this6 = this;

      this.status = "sending..";
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        var text = {};
        text.message = this.message;
        text.sender_id = this.id;
        text.vendor_user_id = this.$route.params.id;
        text.created_at = new Date();
        this.messages.push(text);

        var data = {
          message: this.message,
          vendor: this.vendor[0].id
        };

        axios.post("/api/user-message", data, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {

          _this6.message = " ";
          if (response.status == 200) {
            _this6.status = "Sent";
          }
        }).catch(function (err) {
          _this6.$toasted.error("Message not sent!!");
          _this6.status = "failed";
        });
      }
    }
  }
});

/***/ }),

/***/ 1791:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "message-tab" }, [
      !_vm.hide
        ? _c("div", { staticClass: "users animated slideIn" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.search,
                    expression: "search"
                  }
                ],
                staticClass: "form-control rounded-pill border-0",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Search"
                },
                domProps: { value: _vm.search },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.search = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("i", {
                staticClass: "fa fa-search search_icon",
                attrs: { "aria-hidden": "true" }
              })
            ]),
            _vm._v(" "),
            _vm.sortUser.length
              ? _c(
                  "ul",
                  { staticClass: "user_name" },
                  _vm._l(_vm.sortUser, function(vendor, idx) {
                    return _c(
                      "li",
                      {
                        key: idx,
                        staticClass: "toCaps align_image",
                        class: { active: _vm.active == idx },
                        on: {
                          click: function($event) {
                            return _vm.getVendor(vendor.id, idx)
                          }
                        }
                      },
                      [
                        vendor.valid_id !== null &&
                        vendor.valid_id !== undefined
                          ? _c("div", [
                              _c("img", {
                                staticClass: "profile_img",
                                attrs: { src: vendor.valid_id, alt: "" }
                              })
                            ])
                          : _c("div", { staticClass: "profile_img" }, [
                              _c("i", {
                                staticClass: "fa fa-user-circle",
                                attrs: { "aria-hidden": "true" }
                              })
                            ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          [
                            _vm._v(
                              "\n            " +
                                _vm._s(vendor.username.toLowerCase()) +
                                "\n            "
                            ),
                            _vm._l(_vm.messages, function(message, id) {
                              return message.vendor_user_id == vendor.id &&
                                _vm.messages.length - 1 == id
                                ? _c(
                                    "p",
                                    { key: id, staticClass: "new_message" },
                                    [_vm._v(_vm._s(message.message))]
                                  )
                                : _vm._e()
                            })
                          ],
                          2
                        )
                      ]
                    )
                  }),
                  0
                )
              : _c("ul", { staticClass: "user_name" }, [
                  _c("li", [_vm._v("Not available")])
                ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.vendor.length
        ? _c(
            "form",
            {
              staticClass: "message",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.sendMessage($event)
                }
              }
            },
            [
              _c(
                "div",
                {
                  staticClass: "toCaps p-2 chat_header align_image text-right"
                },
                [
                  _vm.vendor[0].valid_id !== null &&
                  _vm.vendor[0].valid_id !== undefined &&
                  _vm.vendor[0].valid_id !== ""
                    ? _c("div", [
                        _c("img", {
                          staticClass: "profile_img",
                          attrs: { src: _vm.vendor[0].valid_id, alt: "" }
                        })
                      ])
                    : _c("div", { staticClass: "profile_img" }, [
                        _c("i", {
                          staticClass: "fa fa-user-circle",
                          attrs: { "aria-hidden": "true" }
                        })
                      ]),
                  _vm._v(" "),
                  _c("div", [
                    _vm._v(_vm._s(_vm.vendor[0].username.toLowerCase()))
                  ]),
                  _vm._v(" "),
                  _c("i", {
                    staticClass:
                      "fa fa-ellipsis-v text-main ml-auto pr-3 mobile",
                    attrs: { "aria-hidden": "true" },
                    on: { click: _vm.showUsers }
                  })
                ]
              ),
              _vm._v(" "),
              _vm.vendor.length
                ? _c(
                    "ul",
                    {
                      directives: [
                        { name: "chat-scroll", rawName: "v-chat-scroll" }
                      ],
                      staticClass: "message-body"
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "p-2 py-3 pb-4" },
                        _vm._l(_vm.sortMessages, function(message, index) {
                          return _c(
                            "li",
                            {
                              key: index,
                              staticClass: "py-1",
                              class: {
                                "text-right": _vm.id == message.sender_id,
                                "ml-auto": _vm.id == message.sender_id
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  class: {
                                    "chat-right": _vm.id == message.sender_id,
                                    "chat-left": _vm.id !== message.sender_id
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(message.message) +
                                      "\n              "
                                  ),
                                  _c("span", { staticClass: "time" }, [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("moment")(
                                          message.created_at,
                                          "HH:mm"
                                        )
                                      )
                                    )
                                  ]),
                                  _vm._v(" "),
                                  message.status
                                    ? _c("div", { staticClass: "status" }, [
                                        _vm._v(_vm._s(message.status))
                                      ])
                                    : index == _vm.sortMessages.length - 1
                                    ? _c("div", { staticClass: "status" }, [
                                        _vm._v(_vm._s(_vm.status))
                                      ])
                                    : _vm._e()
                                ]
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "bg-ccc p-2" }, [
                _c("div", { staticClass: "form-group submit" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.message,
                        expression: "message"
                      }
                    ],
                    staticClass: "form-control rounded-pill mr-3",
                    attrs: {
                      required: "",
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "Type your message here .."
                    },
                    domProps: { value: _vm.message },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.message = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(0)
                ])
              ])
            ]
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass:
          "elevated_btn elevated_btn_sm m-0 btn-compliment text-white shadow-none rounded-pill",
        attrs: { type: "submit" }
      },
      [
        _c("i", {
          staticClass: "fas fa-paper-plane text-white",
          attrs: { "aria-hidden": "true" }
        })
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4316a2f8", module.exports)
  }
}

/***/ }),

/***/ 630:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1788)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1790)
/* template */
var __vue_template__ = __webpack_require__(1791)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4316a2f8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/privateChat.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4316a2f8", Component.options)
  } else {
    hotAPI.reload("data-v-4316a2f8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});