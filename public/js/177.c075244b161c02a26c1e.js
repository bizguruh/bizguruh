webpackJsonp([177],{

/***/ 1320:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1321);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("45af2ea2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6d640539\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminAddProductCategoryComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6d640539\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminAddProductCategoryComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1321:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.multiDis[data-v-6d640539] {\n    overflow: scroll;\n}\n.content-wrapper[data-v-6d640539] {\n    height: 100vh;\n}\n.main-page[data-v-6d640539] {\n    padding: 40px 20px;\n    max-height: 100vh;\n    height: 100vh;\n    overflow: scroll;\n}\n.category[data-v-6d640539] {\n    width: 100%;\n    padding: 8px;\n    border-bottom: 1px solid black;\n    cursor: pointer;\n    text-transform: capitalize;\n}\n.hover[data-v-6d640539], .clickColor[data-v-6d640539] {\n    background: #222d32;\n    color: #ffffff;\n}\n.firstBar[data-v-6d640539] {\n    border: 4px solid black;\n    height: 55vh;\n    overflow: scroll;\n}\n.cat-header[data-v-6d640539] {\n    margin: 19px 0;\n    font-size: 17px;\n    font-weight: bold;\n}\n", ""]);

// exports


/***/ }),

/***/ 1322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-add-product-category-component",
    data: function data() {
        return {
            categories: [],
            subjectMatters: [],
            topics: [],
            insights: [],
            industries: [],
            token: '',
            catd: true,
            category_name: '',
            subcategory_name: '',
            subcategoryBrand_name: '',
            subcategoriesBrand: [],
            subcategories: [],
            cat: {
                category_id: '',
                sub_category_id: '',
                sub_category_brand_id: [],
                topicsId: '',
                vendor_user_id: '',
                productCategory: 'BP',
                insight_id: '',
                industry_id: ''
            },
            btn: true,
            topicId: [],
            subcat: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
        this.cat.vendor_user_id = authAdmin.id;
        if (authAdmin !== null) {
            this.token = authAdmin.access_token;
            axios.get('/api/admin/get/category', { headers: { "Authorization": 'Bearer ' + authAdmin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this.categories = response.data;
                    _this.getRealSubjectMatter();
                    _this.getVendorInsight();
                    _this.getVendorTopics();
                    _this.getVendorIndustry();
                    _this.categories.forEach(function (item) {
                        _this.$set(item, 'hover', false);
                    });
                    _this.categories.forEach(function (item) {
                        _this.$set(item, 'click', false);
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },

    methods: {
        prodtype: function prodtype() {
            var _this2 = this;

            this.categories.forEach(function (item, index) {
                if (item.id === _this2.cat.category_id) {
                    _this2.category_name = item.name;
                }
            });
        },
        getSubCategory: function getSubCategory(subcat, category, category_name) {
            var _this3 = this;

            this.categories.forEach(function (val) {
                val.click = false;
            });
            console.log(subcat);
            axios.get('/api/admin/get/subcategory/' + subcat, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.cat.category_id = subcat;
                    _this3.cat.sub_category_id = '';
                    _this3.subcategories = response.data;
                    _this3.subcategories.forEach(function (item) {
                        _this3.$set(item, 'hover', false);
                    });
                    category.click = true;
                    _this3.category_name = category_name;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getSubCategoryId: function getSubCategoryId(subcatid, subcategory, subcategory_name) {
            var _this4 = this;

            this.subcat = subcategory_name;
            axios.get('/api/admin/get/topic/' + subcatid, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.subcategoriesBrand = response.data;
                    _this4.subcategoriesBrand.forEach(function (item) {
                        _this4.$set(item, 'hover', false);
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
            this.subcategories.forEach(function (val) {
                val.click = false;
            });
            this.cat.sub_category_id = subcatid;
            subcategory.click = true;
            this.subcategory_name = subcategory_name;
            this.btn = false;
        },
        getSubCategoryBrandId: function getSubCategoryBrandId(subbrandId, subcategoryBrand, subcategoryBrand_name, index) {
            if (this.topicId.includes(subbrandId)) {
                var a = this.topicId.indexOf(subbrandId);
                if (a > -1) {
                    this.topicId.splice(a, 1);
                    this.cat.sub_category_brand_id = JSON.stringify(this.topicId);
                    subcategoryBrand.click = false;
                }
            } else {
                this.topicId.push(subbrandId);
                this.cat.sub_category_brand_id = JSON.stringify(this.topicId);
                subcategoryBrand.click = true;
            }
        },
        addCategory: function addCategory() {
            var _this5 = this;

            console.log(this.cat);
            this.cat.topicsId = JSON.stringify(this.cat.sub_category_brand_id);
            console.log(this.cat);

            axios.post('/api/product', JSON.parse(JSON.stringify(this.cat)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 201) {
                    var id = response.data.data.id;
                    _this5.$toasted.success('Category successfully added');
                    //  this.$router.push(`/admin/add/product/${id}/${this.category_name}`);
                    _this5.$router.push({ name: 'adminAddProduct', params: { id: id, name: _this5.category_name.replace(/ /g, '-') } });
                }
            }).catch(function (error) {
                for (var key in error.response.data.errors) {
                    _this5.$toasted.error(error.response.data.errors[key][0]);
                }
            });
        },
        getRealSubjectMatter: function getRealSubjectMatter() {
            var _this6 = this;

            axios.get('/api/vendor-subject-matter').then(function (response) {
                if (response.status === 200) {
                    _this6.subjectMatters = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getVendorInsight: function getVendorInsight() {
            var _this7 = this;

            axios.get('/api/vendor-insights').then(function (response) {
                if (response.status === 200) {
                    _this7.insights = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getVendorTopics: function getVendorTopics() {
            var _this8 = this;

            axios.get('/api/vendor-topics').then(function (response) {
                if (response.status === 200) {
                    _this8.topics = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getVendorIndustry: function getVendorIndustry() {
            var _this9 = this;

            axios.get('/api/vendor-industry').then(function (response) {
                if (response.status === 200) {
                    _this9.industries = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1323:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "row" }, [
        _c("label", [_vm._v("Select Categories")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.cat.category_id,
                expression: "cat.category_id"
              }
            ],
            staticClass: "form-control",
            on: {
              change: [
                function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.$set(
                    _vm.cat,
                    "category_id",
                    $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                  )
                },
                function($event) {
                  return _vm.prodtype($event)
                }
              ]
            }
          },
          _vm._l(_vm.categories, function(category, index) {
            return _vm.categories.length > 0
              ? _c("option", { domProps: { value: category.id } }, [
                  _vm._v(_vm._s(category.name))
                ])
              : _vm._e()
          }),
          0
        ),
        _vm._v(" "),
        _c("label", [_vm._v("Select Subject Matter")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.cat.sub_category_id,
                expression: "cat.sub_category_id"
              }
            ],
            staticClass: "form-control",
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.cat,
                  "sub_category_id",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          _vm._l(_vm.subjectMatters, function(subjectMatter, index) {
            return _vm.subjectMatters.length > 0
              ? _c("option", { domProps: { value: subjectMatter.id } }, [
                  _vm._v(_vm._s(subjectMatter.name))
                ])
              : _vm._e()
          }),
          0
        ),
        _vm._v(" "),
        _c("label", [_vm._v("Select Insights")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.cat.insight_id,
                expression: "cat.insight_id"
              }
            ],
            staticClass: "form-control",
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.cat,
                  "insight_id",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          _vm._l(_vm.insights, function(insight, index) {
            return _vm.insights.length > 0
              ? _c("option", { domProps: { value: insight.id } }, [
                  _vm._v(_vm._s(insight.name))
                ])
              : _vm._e()
          }),
          0
        ),
        _vm._v(" "),
        _c("label", [_vm._v("Select Industries")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.cat.industry_id,
                expression: "cat.industry_id"
              }
            ],
            staticClass: "form-control",
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.cat,
                  "industry_id",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          _vm._l(_vm.industries, function(industry, index) {
            return _vm.industries.length > 0
              ? _c("option", { domProps: { value: industry.id } }, [
                  _vm._v(_vm._s(industry.name))
                ])
              : _vm._e()
          }),
          0
        ),
        _vm._v(" "),
        _c("label", [_vm._v("Select Concept")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.cat.sub_category_brand_id,
                expression: "cat.sub_category_brand_id"
              }
            ],
            staticClass: "form-control multiDis",
            attrs: { multiple: "" },
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.cat,
                  "sub_category_brand_id",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          _vm._l(_vm.topics, function(topic, index) {
            return _vm.topics.length > 0
              ? _c("option", { domProps: { value: topic.id } }, [
                  _vm._v(_vm._s(topic.name))
                ])
              : _vm._e()
          }),
          0
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            on: {
              click: function($event) {
                return _vm.addCategory()
              }
            }
          },
          [_vm._v("Add Category")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6d640539", module.exports)
  }
}

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1320)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1322)
/* template */
var __vue_template__ = __webpack_require__(1323)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6d640539"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminAddProductCategoryComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6d640539", Component.options)
  } else {
    hotAPI.reload("data-v-6d640539", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});