webpackJsonp([80],{

/***/ 1099:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1100);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("c7e592dc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ee78ea2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./showProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ee78ea2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./showProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1100:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.stDiv[data-v-3ee78ea2] {\n  float: right;\n}\n.moreProductDetail[data-v-3ee78ea2] {\n  text-align: center;\n  padding: 40px;\n}\n.overview[data-v-3ee78ea2]{\n  max-height:200px;\n  overflow:hidden;\n}\n.main-page[data-v-3ee78ea2] {\n  background-color: #ffffff;\n  padding: 30px;\n  height:100%;\n}\n.productAuthor[data-v-3ee78ea2] {\n  float: left;\n  margin-right: 200px;\n}\n.hardCover[data-v-3ee78ea2],\n.[data-v-3ee78ea2] {\n  border: 2px solid #4c4c4c;\n  max-width: 25%;\n  padding: 11px;\n}\n.customTab[data-v-3ee78ea2] {\n  text-align: center;\n  border: 2px solid #f5f5f5;\n  padding: 20px;\n  text-transform: uppercase;\n  margin: 20px 0;\n}\n.proReview[data-v-3ee78ea2] {\n  margin-left: 50px;\n  margin-right: 50px;\n}\n.divC[data-v-3ee78ea2] {\n  font-size: 40px;\n  margin: 17px 0 50px 0;\n}\n.imgFullSide[data-v-3ee78ea2] {\n  float: left;\n  height: 60%;\n  width: 70%;\n  -o-object-fit:cover;\n     object-fit:cover;\n}\n.videoList[data-v-3ee78ea2] {\n  padding: 3px 5px;\n  border: 1px solid black;\n  border-radius: 4px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__ = __webpack_require__(756);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_social_sharing__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      id: this.$route.params.id,
      products: {},
      isActive: true,
      productDetails: true,
      tableOfContent: false,
      checkBook: false,
      token: "",
      storeName: "",
      categoryName: "",
      excerpts: false,
      aboutAuthor: false,
      linkTag: false,
      price: "",
      title: "Hard Copy",
      dis: false
    };
  },
  components: {
    SocialSharing: __WEBPACK_IMPORTED_MODULE_0_vue_social_sharing___default.a
  },
  mounted: function mounted() {
    var _this = this;

    var vendor = JSON.parse(localStorage.getItem("authVendor"));
    if (vendor != null) {
      this.token = vendor.access_token;
      axios.get("/api/vendor-product/" + this.id, {
        headers: { Authorization: "Bearer " + vendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          var emptyObject = {};
          console.log(response.data.data);
          switch (response.data.data.prodType) {
            case "Books":
              _this.$set(response.data.data.books, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.books, "uniId", response.data.data.id);
              _this.$set(response.data.data.books, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.books, "verify", response.data.data.verify);
              _this.$set(response.data.data.books, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.books, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.books, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.books, "audioPrice", null);
              _this.$set(response.data.data.books, "videoPrice", null);
              _this.$set(response.data.data.books, "webinarPrice", null);
              _this.$set(response.data.data.books, "streamPrice", null);
              _this.$set(response.data.data.books, "subscribePrice", null);
              emptyObject = response.data.data.books;
              console.log("fff");
              break;
            case "White Paper":
              _this.$set(response.data.data.whitePaper, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.whitePaper, "uniId", response.data.data.id);
              _this.$set(response.data.data.whitePaper, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.whitePaper, "verify", response.data.data.verify);
              _this.$set(response.data.data.whitePaper, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.whitePaper, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.whitePaper, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.whitePaper, "audioPrice", null);
              _this.$set(response.data.data.whitePaper, "videoPrice", null);
              _this.$set(response.data.data.whitePaper, "webinarPrice", null);
              _this.$set(response.data.data.whitePaper, "streamPrice", null);
              _this.$set(response.data.data.whitePaper, "subscribePrice", null);
              emptyObject = response.data.data.whitePaper;
              break;
            case "Market Reports":
              _this.$set(response.data.data.marketReport, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.marketReport, "uniId", response.data.data.id);
              _this.$set(response.data.data.marketReport, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.marketReport, "verify", response.data.data.verify);
              _this.$set(response.data.data.marketReport, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.marketReport, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.marketReport, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.marketReport, "audioPrice", null);
              _this.$set(response.data.data.marketReport, "videoPrice", null);
              _this.$set(response.data.data.marketReport, "webinarPrice", null);
              _this.$set(response.data.data.marketReport, "streamPrice", null);
              _this.$set(response.data.data.marketReport, "subscribePrice", null);
              emptyObject = response.data.data.marketReport;
              break;
            case "Market Research":
              _this.$set(response.data.data.marketResearch, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.marketResearch, "uniId", response.data.data.id);
              _this.$set(response.data.data.marketResearch, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.marketResearch, "verify", response.data.data.verify);
              _this.$set(response.data.data.marketResearch, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.marketResearch, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.marketResearch, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.marketResearch, "audioPrice", null);
              _this.$set(response.data.data.marketResearch, "videoPrice", null);
              _this.$set(response.data.data.marketResearch, "webinarPrice", null);
              _this.$set(response.data.data.marketResearch, "streamPrice", null);
              _this.$set(response.data.data.marketResearch, "subscribePrice", null);
              emptyObject = response.data.data.marketResearch;
              break;
            case "Webinar":
              _this.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.webinar, "uniId", response.data.data.id);
              _this.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.webinar, "verify", response.data.data.verify);
              _this.$set(response.data.data.webinar, "readOnlinePrice", null);
              _this.$set(response.data.data.webinar, "hardCopyPrice", null);
              _this.$set(response.data.data.webinar, "softCopyPrice", null);
              _this.$set(response.data.data.webinar, "audioPrice", null);
              _this.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
              _this.$set(response.data.data.webinar, "videoPrice", null);
              emptyObject = response.data.data.webinar;
              break;
            case "Podcast":
              _this.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.webinar, "uniId", response.data.data.id);
              _this.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.webinar, "verify", response.data.data.verify);
              _this.$set(response.data.data.webinar, "readOnlinePrice", response.data.data.readOnlinePrice);
              _this.$set(response.data.data.webinar, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.webinar, "softCopyPrice", null);
              _this.$set(response.data.data.webinar, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
              _this.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
              _this.$set(response.data.data.webinar, "streamPrice", response.data.data.streamPrice ? response.data.data.streamPrice : null);
              _this.$set(response.data.data.webinar, "subscribePrice", response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
              _this.$set(response.data.data.webinar, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
              emptyObject = response.data.data.webinar;
              break;
            case "Videos":
              _this.$set(response.data.data.webinar, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.webinar, "uniId", response.data.data.id);
              _this.$set(response.data.data.webinar, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.webinar, "verify", response.data.data.verify);
              _this.$set(response.data.data.webinar, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.webinar, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.webinar, "softCopyPrice", null);
              _this.$set(response.data.data.webinar, "audioPrice", null);
              _this.$set(response.data.data.webinar, "webinarPrice", response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
              _this.$set(response.data.data.webinar, "streamPrice", response.data.data.streamPrice ? response.data.data.streamPrice : null);
              _this.$set(response.data.data.webinar, "subscribePrice", response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
              _this.$set(response.data.data.webinar, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
              emptyObject = response.data.data.webinar;
              break;
            case "Journals":
              _this.$set(response.data.data.journal, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.journal, "uniId", response.data.data.id);
              _this.$set(response.data.data.journal, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.journal, "verify", response.data.data.verify);
              _this.$set(response.data.data.journal, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.journal, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.journal, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.journal, "audioPrice", null);
              _this.$set(response.data.data.journal, "videoPrice", null);
              _this.$set(response.data.data.journal, "webinarPrice", null);
              _this.$set(response.data.data.journal, "streamPrice", null);
              _this.$set(response.data.data.journal, "subscribePrice", null);
              emptyObject = response.data.data.journal;
              break;
            case "Courses":
              _this.$set(response.data.data.courses, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.courses, "uniId", response.data.data.id);
              _this.$set(response.data.data.courses, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.courses, "verify", response.data.data.verify);
              _this.$set(response.data.data.courses, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.courses, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.courses, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.courses, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
              _this.$set(response.data.data.courses, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
              _this.$set(response.data.data.courses, "webinarPrice", null);
              _this.$set(response.data.data.courses, "streamPrice", null);
              _this.$set(response.data.data.courses, "subscribePrice", null);
              emptyObject = response.data.data.courses;
              break;
            case "Articles":
              _this.$set(response.data.data.articles, "vendor", response.data.data.vendor);
              _this.$set(response.data.data.articles, "prodType", response.data.data.prodType);
              _this.$set(response.data.data.articles, "uniId", response.data.data.id);
              _this.$set(response.data.data.articles, "coverImage", response.data.data.coverImage);
              _this.$set(response.data.data.articles, "verify", response.data.data.verify);
              _this.$set(response.data.data.articles, "readOnlinePrice", response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
              _this.$set(response.data.data.articles, "hardCopyPrice", response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
              _this.$set(response.data.data.articles, "softCopyPrice", response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
              _this.$set(response.data.data.articles, "audioPrice", response.data.data.audioPrice ? response.data.data.audioPrice : null);
              _this.$set(response.data.data.articles, "videoPrice", response.data.data.videoPrice ? response.data.data.videoPrice : null);
              _this.$set(response.data.data.articles, "webinarPrice", null);
              _this.$set(response.data.data.articles, "streamPrice", null);
              _this.$set(response.data.data.articles, "subscribePrice", null);
              emptyObject = response.data.data.articles;
              break;
            default:
              console.log("ffsss");
              return false;
          }
          console.log(emptyObject);
          _this.products = emptyObject;
          // this.products = response.data.data;
          if (response.data.data.softCopyPrice || response.data.data.hardCopyPrice) {
            _this.checkBook = true;
          }
          _this.isActive = false;
          _this.linkTag = true;
          _this.dis = true;
        }
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    swipeImage: function swipeImage(index) {
      console.log(index);
      for (var i = 0; i < this.products.productImage.length; i++) {
        this.imgSrc = this.products.productImage[index].image;
      }
    },
    changeTabsContent: function changeTabsContent(tabArr) {
      switch (tabArr) {
        case "product detail":
          this.productDetails = true;
          this.tableOfContent = false;
          this.excerpts = false;
          this.aboutAuthor = false;
          break;
        case "About the author":
          this.productDetails = false;
          this.tableOfContent = false;
          this.excerpts = false;
          this.aboutAuthor = true;
          break;
        case "table of content":
          this.productDetails = false;
          this.tableOfContent = true;
          this.excerpts = false;
          this.aboutAuthor = false;
          break;
        case "read an excerpts":
          this.productDetails = false;
          this.tableOfContent = false;
          this.excerpts = true;
          this.aboutAuthor = false;
          break;
        default:
          this.productDetails = false;
          this.tableOfContent = false;
          this.excerpts = false;
          this.aboutAuthor = false;
      }
    },
    deleteProduct: function deleteProduct(id) {
      var _this2 = this;

      console.log(id);
      confirm("Are you sure?");
      axios.delete("/api/vendor-product/" + id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        if (response.status === 200) {
          _this2.$router.push("/vendor/all");
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  }
});

/***/ }),

/***/ 1102:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.dis
    ? _c(
        "div",
        { staticClass: "main-page" },
        [
          _c("vue-loading", {
            attrs: {
              active: _vm.isActive,
              spinner: "bar-fade-scale",
              color: "#FF6700"
            }
          }),
          _vm._v(" "),
          _vm.linkTag
            ? _c(
                "router-link",
                {
                  staticClass: "btn btn-primary mb-4",
                  attrs: {
                    tag: "a",
                    to: {
                      name: "editVendorProduct",
                      params: { id: _vm.products.uniId }
                    }
                  }
                },
                [_vm._v("Edit")]
              )
            : _vm._e(),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-danger mb-4",
              on: {
                click: function($event) {
                  return _vm.deleteProduct(_vm.products.uniId)
                }
              }
            },
            [_vm._v("Delete")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "stDiv" }, [
            _vm._v(
              "STATUS: " +
                _vm._s(_vm.products.verify === 1 ? "ACTIVE" : "INACTIVE")
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("img", {
                staticClass: "imgFullSide",
                attrs: { src: _vm.products.coverImage }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("h2", [_vm._v(_vm._s(_vm.products.title))]),
              _vm._v(" "),
              _vm.products.host
                ? _c("p", [
                    _vm._v(
                      _vm._s(_vm.products.host ? "Host:" : "") +
                        " " +
                        _vm._s(_vm.products.host)
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(_vm.products.level ? "Level: " : "") +
                    " " +
                    _vm._s(_vm.products.level)
                )
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(
                    _vm.products.certification
                      ? "Certification Avaliability: "
                      : ""
                  ) +
                    " " +
                    _vm._s(_vm.products.certification)
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "productAuthor" }, [
                _vm._v(
                  "\n        " +
                    _vm._s(_vm.products.author ? "Author: " : "") +
                    " " +
                    _vm._s(_vm.products.author) +
                    "\n        "
                ),
                _vm.products.guest !== null
                  ? _c("span", [
                      _vm._v(
                        _vm._s(_vm.products.guest ? "Guest:" : "") +
                          " " +
                          _vm._s(_vm.products.guest)
                      )
                    ])
                  : _vm._e(),
                _vm._v("\n        " + _vm._s(_vm.storeName) + "\n      ")
              ]),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.categoryName))]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(_vm.products.publisher ? "Publisher: " : "") +
                    " " +
                    _vm._s(_vm.products.publisher)
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "social" }),
              _vm._v(" "),
              _c("div", {}),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _vm.products.hardCopyPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "hardCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("hardCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.hardCopyPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Hard Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.softCopyPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.softCopyPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Digital Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.audioPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.audioPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Audio Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.readOnlinePrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("readOnline")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.readOnlinePrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Read Online")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.videoPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.videoPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Video Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.webinarPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("webinarCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.webinarPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Webinar Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.streamPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("streamCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.streamPrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Streaming Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.subscribePrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: " col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("subscribeCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n          ₦" +
                            _vm._s(_vm.products.subscribePrice) +
                            ".00\n          "
                        ),
                        _c("p", [_vm._v("Subscribe Copy")])
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("hr")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _vm.checkBook
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("product detail")
                      }
                    }
                  },
                  [_vm._v("Product Details")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.aboutAuthor
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("About the author")
                      }
                    }
                  },
                  [_vm._v("About the Author")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.aboutThisAuthor
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("About the author")
                      }
                    }
                  },
                  [_vm._v("About the Author")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.tableOfContent
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("table of content")
                      }
                    }
                  },
                  [_vm._v("Table of Content")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.excerpts
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("read an excerpts")
                      }
                    }
                  },
                  [_vm._v("Read an Excerpts")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.description
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("overview")
                      }
                    }
                  },
                  [_vm._v("Overview")]
                )
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm.checkBook
            ? _c("div", [
                _vm.products.fileType !== null
                  ? _c("p", [_vm._v("ISBN: " + _vm._s(_vm.products.isbn))])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.publisher !== null
                  ? _c("p", [
                      _vm._v("Publisher: " + _vm._s(_vm.products.publisher))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.publicationDate !== null
                  ? _c("p", [
                      _vm._v(
                        "Publication Date: " +
                          _vm._s(_vm.products.publisherDate)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.pageNo !== null
                  ? _c("p", [
                      _vm._v("Pages: " + _vm._s(_vm.products.noOfPages))
                    ])
                  : _vm._e()
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.aboutAuthor
            ? _c("div", [_vm._v(_vm._s(_vm.products.aboutAuthor))])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.aboutThisAuthor
            ? _c("div", [_vm._v(_vm._s(_vm.products.aboutThisAuthor))])
            : _vm._e(),
          _vm._v(" "),
          _vm.tableOfContent
            ? _c("div", [_vm._v(_vm._s(_vm.products.tableOfContent))])
            : _vm._e(),
          _vm._v(" "),
          _vm.excerpts
            ? _c("div", [_vm._v(_vm._s(_vm.products.excerpts))])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.prodType !== "Articles"
            ? _c("div", { staticClass: "overview" }, [
                _vm.products.description
                  ? _c("div", [_vm._v(_vm._s(_vm.products.description))])
                  : _vm._e()
              ])
            : _c("div", {
                staticClass: "overview",
                domProps: { innerHTML: _vm._s(_vm.products.description) }
              }),
          _vm._v(" "),
          _c("hr")
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3ee78ea2", module.exports)
  }
}

/***/ }),

/***/ 492:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1099)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1101)
/* template */
var __vue_template__ = __webpack_require__(1102)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3ee78ea2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/showProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ee78ea2", Component.options)
  } else {
    hotAPI.reload("data-v-3ee78ea2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 756:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * vue-social-sharing v2.4.7 
 * (c) 2019 nicolasbeauvais
 * Released under the MIT License.
 */


function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var Vue = _interopDefault(__webpack_require__(40));

var SocialSharingNetwork = {
  functional: true,

  props: {
    network: {
      type: String,
      default: ''
    }
  },

  render: function (createElement, context) {
    var network = context.parent._data.baseNetworks[context.props.network];

    if (!network) {
      return console.warn(("Network " + (context.props.network) + " does not exist"));
    }

    return createElement(context.parent.networkTag, {
      staticClass: context.data.staticClass || null,
      staticStyle: context.data.staticStyle || null,
      class: context.data.class || null,
      style: context.data.style || null,
      attrs: {
        id: context.data.attrs.id || null,
        tabindex: context.data.attrs.tabindex || 0,
        'data-link': network.type === 'popup'
          ? '#share-' + context.props.network
          : context.parent.createSharingUrl(context.props.network),
        'data-action': network.type === 'popup' ? null : network.action
      },
      on: {
        click: network.type === 'popup' ? function () {
          context.parent.share(context.props.network);
        } : function () {
          context.parent.touch(context.props.network);
        }
      }
    }, context.children);
  }
};

var email = {"sharer":"mailto:?subject=@title&body=@url%0D%0A%0D%0A@description","type":"direct"};
var facebook = {"sharer":"https://www.facebook.com/sharer/sharer.php?u=@url&title=@title&description=@description&quote=@quote&hashtag=@hashtags","type":"popup"};
var googleplus = {"sharer":"https://plus.google.com/share?url=@url","type":"popup"};
var line = {"sharer":"http://line.me/R/msg/text/?@description%0D%0A@url","type":"popup"};
var linkedin = {"sharer":"https://www.linkedin.com/shareArticle?mini=true&url=@url&title=@title&summary=@description","type":"popup"};
var odnoklassniki = {"sharer":"https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=@url&st.comments=@description","type":"popup"};
var pinterest = {"sharer":"https://pinterest.com/pin/create/button/?url=@url&media=@media&description=@title","type":"popup"};
var reddit = {"sharer":"https://www.reddit.com/submit?url=@url&title=@title","type":"popup"};
var skype = {"sharer":"https://web.skype.com/share?url=@description%0D%0A@url","type":"popup"};
var telegram = {"sharer":"https://t.me/share/url?url=@url&text=@description","type":"popup"};
var twitter = {"sharer":"https://twitter.com/intent/tweet?text=@title&url=@url&hashtags=@hashtags@twitteruser","type":"popup"};
var viber = {"sharer":"viber://forward?text=@url @description","type":"direct"};
var vk = {"sharer":"https://vk.com/share.php?url=@url&title=@title&description=@description&image=@media&noparse=true","type":"popup"};
var weibo = {"sharer":"http://service.weibo.com/share/share.php?url=@url&title=@title","type":"popup"};
var whatsapp = {"sharer":"https://api.whatsapp.com/send?text=@description%0D%0A@url","type":"popup","action":"share/whatsapp/share"};
var sms = {"sharer":"sms:?body=@url%20@description","type":"direct"};
var sms_ios = {"sharer":"sms:;body=@url%20@description","type":"direct"};
var BaseNetworks = {
	email: email,
	facebook: facebook,
	googleplus: googleplus,
	line: line,
	linkedin: linkedin,
	odnoklassniki: odnoklassniki,
	pinterest: pinterest,
	reddit: reddit,
	skype: skype,
	telegram: telegram,
	twitter: twitter,
	viber: viber,
	vk: vk,
	weibo: weibo,
	whatsapp: whatsapp,
	sms: sms,
	sms_ios: sms_ios
};

var inBrowser = typeof window !== 'undefined';
var $window = inBrowser ? window : null;

var SocialSharing = {
  props: {
    /**
     * URL to share.
     * @var string
     */
    url: {
      type: String,
      default: inBrowser ? window.location.href : ''
    },

    /**
     * Sharing title, if available by network.
     * @var string
     */
    title: {
      type: String,
      default: ''
    },

    /**
     * Sharing description, if available by network.
     * @var string
     */
    description: {
      type: String,
      default: ''
    },

    /**
     * Facebook quote
     * @var string
     */
    quote: {
      type: String,
      default: ''
    },

    /**
     * Twitter hashtags
     * @var string
     */
    hashtags: {
      type: String,
      default: ''
    },

    /**
     * Twitter user.
     * @var string
     */
    twitterUser: {
      type: String,
      default: ''
    },

    /**
     * Flag that indicates if counts should be retrieved.
     * - NOT WORKING IN CURRENT VERSION
     * @var mixed
     */
    withCounts: {
      type: [String, Boolean],
      default: false
    },

    /**
     * Google plus key.
     * @var string
     */
    googleKey: {
      type: String,
      default: undefined
    },

    /**
     * Pinterest Media URL.
     * Specifies the image/media to be used.
     */
    media: {
      type: String,
      default: ''
    },

    /**
     * Network sub component tag.
     * Default to span tag
     */
    networkTag: {
      type: String,
      default: 'span'
    },

    /**
     * Additional or overridden networks.
     * Default to BaseNetworks
     */
    networks: {
      type: Object,
      default: function () {
        return {};
      }
    }
  },

  data: function data () {
    return {
      /**
       * Available sharing networks.
       * @param object
       */
      baseNetworks: BaseNetworks,

      /**
       * Popup settings.
       * @param object
       */
      popup: {
        status: false,
        resizable: true,
        toolbar: false,
        menubar: false,
        scrollbars: false,
        location: false,
        directories: false,
        width: 626,
        height: 436,
        top: 0,
        left: 0,
        window: undefined,
        interval: null
      }
    };
  },

  methods: {
    /**
     * Returns generated sharer url.
     *
     * @param network Social network key.
     */
    createSharingUrl: function createSharingUrl (network) {
      var ua = navigator.userAgent.toLowerCase();

      /**
       * On IOS, SMS sharing link need a special formating
       * Source: https://weblog.west-wind.com/posts/2013/Oct/09/Prefilling-an-SMS-on-Mobile-Devices-with-the-sms-Uri-Scheme#Body-only
        */
      if (network === 'sms' && (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1)) {
        network += '_ios';
      }

      var url = this.baseNetworks[network].sharer;

      /**
       * On IOS, Twitter sharing shouldn't include a hashtag parameter if the hashtag value is empty
       * Source: https://github.com/nicolasbeauvais/vue-social-sharing/issues/143
        */
      if (network === 'twitter' && this.hashtags.length === 0) {
        url = url.replace('&hashtags=@hashtags', '');
      }

      return url
        .replace(/@url/g, encodeURIComponent(this.url))
        .replace(/@title/g, encodeURIComponent(this.title))
        .replace(/@description/g, encodeURIComponent(this.description))
        .replace(/@quote/g, encodeURIComponent(this.quote))
        .replace(/@hashtags/g, this.generateHashtags(network, this.hashtags))
        .replace(/@media/g, this.media)
        .replace(/@twitteruser/g, this.twitterUser ? '&via=' + this.twitterUser : '');
    },
    /**
     * Encode hashtags for the specified social network.
     *
     * @param  network Social network key
     * @param  hashtags All hashtags specified
     */
    generateHashtags: function generateHashtags (network, hashtags) {
      if (network === 'facebook' && hashtags.length > 0) {
        return '%23' + hashtags.split(',')[0];
      }

      return hashtags;
    },
    /**
     * Shares URL in specified network.
     *
     * @param network Social network key.
     */
    share: function share (network) {
      this.openSharer(network, this.createSharingUrl(network));

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Touches network and emits click event.
     *
     * @param network Social network key.
     */
    touch: function touch (network) {
      window.open(this.createSharingUrl(network), '_self');

      this.$root.$emit('social_shares_open', network, this.url);
      this.$emit('open', network, this.url);
    },

    /**
     * Opens sharer popup.
     *
     * @param network Social network key
     * @param url Url to share.
     */
    openSharer: function openSharer (network, url) {
      var this$1 = this;

      // If a popup window already exist it will be replaced, trigger a close event.
      var popupWindow = null;
      if (popupWindow && this.popup.interval) {
        clearInterval(this.popup.interval);

        popupWindow.close();// Force close (for Facebook)

        this.$root.$emit('social_shares_change', network, this.url);
        this.$emit('change', network, this.url);
      }

      popupWindow = window.open(
        url,
        'sharer',
        'status=' + (this.popup.status ? 'yes' : 'no') +
        ',height=' + this.popup.height +
        ',width=' + this.popup.width +
        ',resizable=' + (this.popup.resizable ? 'yes' : 'no') +
        ',left=' + this.popup.left +
        ',top=' + this.popup.top +
        ',screenX=' + this.popup.left +
        ',screenY=' + this.popup.top +
        ',toolbar=' + (this.popup.toolbar ? 'yes' : 'no') +
        ',menubar=' + (this.popup.menubar ? 'yes' : 'no') +
        ',scrollbars=' + (this.popup.scrollbars ? 'yes' : 'no') +
        ',location=' + (this.popup.location ? 'yes' : 'no') +
        ',directories=' + (this.popup.directories ? 'yes' : 'no')
      );

      popupWindow.focus();

      // Create an interval to detect popup closing event
      this.popup.interval = setInterval(function () {
        if (!popupWindow || popupWindow.closed) {
          clearInterval(this$1.popup.interval);

          popupWindow = undefined;

          this$1.$root.$emit('social_shares_close', network, this$1.url);
          this$1.$emit('close', network, this$1.url);
        }
      }, 500);
    }
  },

  /**
   * Merge base networks list with user's list
   */
  beforeMount: function beforeMount () {
    this.baseNetworks = Vue.util.extend(this.baseNetworks, this.networks);
  },

  /**
   * Sets popup default dimensions.
   */
  mounted: function mounted () {
    if (!inBrowser) {
      return;
    }

    /**
     * Center the popup on dual screens
     * http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen/32261263
     */
    var dualScreenLeft = $window.screenLeft !== undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop !== undefined ? $window.screenTop : screen.top;

    var width = $window.innerWidth ? $window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width);
    var height = $window.innerHeight ? $window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height);

    this.popup.left = ((width / 2) - (this.popup.width / 2)) + dualScreenLeft;
    this.popup.top = ((height / 2) - (this.popup.height / 2)) + dualScreenTop;
  },

  /**
   * Set component aliases for buttons and links.
   */
  components: {
    'network': SocialSharingNetwork
  }
};

SocialSharing.version = '2.4.7';

SocialSharing.install = function (Vue) {
  Vue.component('social-sharing', SocialSharing);
};

if (typeof window !== 'undefined') {
  window.SocialSharing = SocialSharing;
}

module.exports = SocialSharing;

/***/ })

});