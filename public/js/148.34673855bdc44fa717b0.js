webpackJsonp([148],{

/***/ 1198:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1199);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("e7381a18", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5bbc5fea\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewAllProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5bbc5fea\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./viewAllProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1199:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-5bbc5fea] {\n    padding: 40px 20px;\n}\n.btn-below[data-v-5bbc5fea] {\n    padding-bottom: 10px;\n}\n.btn-custom-market input[data-v-5bbc5fea] {\n    position: absolute !important;\n    clip: rect(0, 0, 0, 0);\n    height: 1px;\n    width: 1px;\n    border: 0;\n    overflow: hidden;\n}\n.btn-custom-market label[data-v-5bbc5fea] {\n    float: left;\n    display: inline-block;\n    width: auto;\n    background-color: #e4e4e4;\n    color: rgba(0, 0, 0, 0.6);\n    font-size: 14px;\n    font-weight: normal;\n    text-align: center;\n    text-shadow: none;\n    padding: 6px 14px;\n    border: 1px solid rgba(0, 0, 0, 0.2);\n    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    -webkit-transition: all 0.1s ease-in-out;\n    transition:         all 0.1s ease-in-out;\n}\n.btn-custom-market[data-v-5bbc5fea] {\n    padding: 40px;\n    overflow: hidden;\n}\n.btn-custom-market label[data-v-5bbc5fea]:hover {\n    cursor: pointer;\n}\n.btn-custom-market input[data-v-5bbc5fea]:checked {\n    background-color: #A5DC86;\n    -webkit-box-shadow: none;\n    box-shadow: none;\n}\n.btn-custom-market label[data-v-5bbc5fea]:first-of-type {\n    border-radius: 4px 0 0 4px;\n}\n.btn-custom-market label[data-v-5bbc5fea]:last-of-type {\n    border-radius: 0 4px 4px 0;\n}\n.main-page[data-v-5bbc5fea] {\n    height: 100vh;\n    overflow: auto;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "view-all-product-component",
    data: function data() {
        return {
            products: [],
            token: '',
            verified: 'All',
            sponsor: '',
            isActive: true
        };
    },
    mounted: function mounted() {
        var _this = this;

        var ops = JSON.parse(localStorage.getItem('authOps'));
        this.token = ops.access_token;

        if (ops != null) {
            axios.get('api/all/product', { headers: { "Authorization": 'Bearer ' + ops.access_token } }).then(function (response) {
                console.log(response.data.data);
                _this.products = response.data.data;
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/ops/login');
        }
    },

    computed: {
        showData: function showData() {
            var verify = this.verified;
            var sponsor = this.sponsor;
            if (verify === 'All') {
                return this.products;
            } else if (verify === "1" || verify === "0") {
                return this.products.filter(function (value) {
                    return value.verify === parseInt(verify);
                });
            } else {
                return this.products.filter(function (value) {
                    return value.sponsor === verify;
                });
            }
        }
    }
});

/***/ }),

/***/ 1201:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "btn-below" }, [
        _c("div", { staticClass: "btn-custom-market" }, [
          _c("label", { class: { active: _vm.isActive } }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.verified,
                  expression: "verified"
                }
              ],
              attrs: { type: "radio", value: "All" },
              domProps: { checked: _vm._q(_vm.verified, "All") },
              on: {
                change: function($event) {
                  _vm.verified = "All"
                }
              }
            }),
            _vm._v("All")
          ]),
          _vm._v(" "),
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.verified,
                  expression: "verified"
                }
              ],
              attrs: { type: "radio", value: "1" },
              domProps: { checked: _vm._q(_vm.verified, "1") },
              on: {
                change: function($event) {
                  _vm.verified = "1"
                }
              }
            }),
            _vm._v("Verified")
          ]),
          _vm._v(" "),
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.verified,
                  expression: "verified"
                }
              ],
              attrs: { type: "radio", value: "0" },
              domProps: { checked: _vm._q(_vm.verified, "0") },
              on: {
                change: function($event) {
                  _vm.verified = "0"
                }
              }
            }),
            _vm._v("Unverified")
          ]),
          _vm._v(" "),
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.verified,
                  expression: "verified"
                }
              ],
              attrs: { type: "radio", value: "Y" },
              domProps: { checked: _vm._q(_vm.verified, "Y") },
              on: {
                change: function($event) {
                  _vm.verified = "Y"
                }
              }
            }),
            _vm._v("Sponsor")
          ]),
          _vm._v(" "),
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.verified,
                  expression: "verified"
                }
              ],
              attrs: { type: "radio", value: "N" },
              domProps: { checked: _vm._q(_vm.verified, "N") },
              on: {
                change: function($event) {
                  _vm.verified = "N"
                }
              }
            }),
            _vm._v("Unsponsor")
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row" },
        _vm._l(_vm.showData, function(product) {
          return _c(
            "div",
            { staticClass: "col-md-3 grid_box1 border rounded" },
            [
              _c(
                "router-link",
                {
                  attrs: {
                    to: { name: "showOpsProduct", params: { id: product.id } }
                  }
                },
                [
                  _c("h2", { staticClass: "text-center" }, [
                    _vm._v(_vm._s(product.title))
                  ]),
                  _vm._v(" "),
                  _vm._l(product.productImage.slice(0, 1), function(
                    productimage
                  ) {
                    return _c(
                      "div",
                      [
                        _c("img", {
                          staticClass: "img-fluid",
                          attrs: {
                            src: productimage.image,
                            alt: "product image"
                          }
                        }),
                        _vm._v(" "),
                        _vm._l(product.productDetail, function(productDetails) {
                          return _c("div", [
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm._v(_vm._s(productDetails.size))
                              ]),
                              _c("span", { staticClass: "float-right" }, [
                                _vm._v(
                                  "Quanity: " + _vm._s(productDetails.quantity)
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "col-md-6" }, [
                                _vm._v(
                                  "Price:" + _vm._s(productDetails.salePrice)
                                )
                              ]),
                              _c("span", { staticClass: "float-right" }, [
                                _c("i", { staticClass: "fa fa-times" }),
                                _vm._v("Unverify")
                              ])
                            ])
                          ])
                        })
                      ],
                      2
                    )
                  })
                ],
                2
              )
            ],
            1
          )
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5bbc5fea", module.exports)
  }
}

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1198)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1200)
/* template */
var __vue_template__ = __webpack_require__(1201)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5bbc5fea"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/operation/components/viewAllProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5bbc5fea", Component.options)
  } else {
    hotAPI.reload("data-v-5bbc5fea", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});