webpackJsonp([23],{

/***/ 1951:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1952);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("b165ff62", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eafabbe0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./formComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-eafabbe0\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./formComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1952:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-eafabbe0] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n  position: relative;\n}\n.preview[data-v-eafabbe0] {\n  position: absolute;\n  background: rgba(255, 255, 255, 0.8);\n  width: 100%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px;\n  z-index: 1;\n}\n.body[data-v-eafabbe0] {\n  background: #fff;\n  padding: 25px;\n  height: 90%;\n  overflow: scroll;\n  width: 90%;\n}\n.gui[data-v-eafabbe0] {\n  width: 20%;\n  background: #f7f8fa;\n  height: 100%;\n  padding: 30px;\n  text-align: center;\n}\n.live[data-v-eafabbe0] {\n  width: 80%;\n  height: 100%;\n  position: relative;\n}\n", ""]);

// exports


/***/ }),

/***/ 1953:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__guiForm__ = __webpack_require__(1954);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__guiForm___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__guiForm__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__liveForm__ = __webpack_require__(1959);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__liveForm___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__liveForm__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__previewForm__ = __webpack_require__(994);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__previewForm___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__previewForm__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      prev: false,
      start: true,
      value: "",
      template: {
        form: {
          legend: "",
          sections: []
        }
      },
      item: {},
      myHead: "",
      group: ''
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        vm.auth = true;
      } else {
        vm.$toasted.error('Log in to access');
        next('/');
      }
    });
  },

  components: {
    Gui: __WEBPACK_IMPORTED_MODULE_0__guiForm___default.a,
    Live: __WEBPACK_IMPORTED_MODULE_1__liveForm___default.a,
    Preview: __WEBPACK_IMPORTED_MODULE_2__previewForm___default.a
  },
  methods: {
    changeGroup: function changeGroup(value) {
      this.group = value;
    },
    previewNow: function previewNow() {
      this.prev = !this.prev;
    },
    updateRowTitle: function updateRowTitle(value, sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.push(value);
    },
    updateValue: function updateValue(value) {
      this.value = value;
    },
    updateHeader: function updateHeader(value) {
      this.myHead = value;
    },
    addForm: function addForm() {
      this.start = true;
    },
    addSection: function addSection() {
      this.template.form.sections.push({
        section: {
          label: "",
          rows: []
        }
      });
    },
    removeSection: function removeSection() {
      this.template.form.sections.pop();
    },
    removeRow: function removeRow(id) {
      this.template.form.sections[id].section.rows.pop();
    },
    addRow: function addRow(id) {
      this.template.form.sections[id].section.rows.push({
        row: [{
          type: "selected",
          multiple: false,
          placeholder: "input text",
          label: "",
          description: "",
          note: "",
          required: false,
          answer: "",
          answers: [],
          values: [],
          table: {
            items: [],
            headers: [],
            row_titles: []
          }
        }]
      });
    },
    addValue: function addValue(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].values.push(this.value);
    },
    addItem: function addItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.push(this.item);
    },
    removeItem: function removeItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.pop();
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.pop();
    },
    addHeader: function addHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.push(this.myHead);

      this.myHead = "";
    },
    removeHeader: function removeHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.pop();
    },
    createForm: function createForm() {
      var _this = this;

      var data = {
        group: this.group,
        title: this.template.form.legend,
        template: this.template
      };
      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.post("/api/save-form-template", JSON.parse(JSON.stringify(data)), {
        headers: { Authorization: "Bearer " + user.access_token
        }
      }).then(function (res) {
        if (res.status == 201) {
          _this.$router.push('/form-questions');
        }
      }).catch();
    }
  }
});

/***/ }),

/***/ 1954:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1955)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1957)
/* template */
var __vue_template__ = __webpack_require__(1958)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-37789256"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/guiForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-37789256", Component.options)
  } else {
    hotAPI.reload("data-v-37789256", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1955:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1956);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("20802ebe", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37789256\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guiForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37789256\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guiForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1956:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-37789256] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n}\n", ""]);

// exports


/***/ }),

/***/ 1957:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },


  methods: {
    addSection: function addSection() {
      this.$emit("addSection");
    },
    removeSection: function removeSection() {
      this.$emit("removeSection");
    },
    previewNow: function previewNow() {
      this.$emit("previewNow");
    }
  }
});

/***/ }),

/***/ 1958:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", [
      _c("div", { staticClass: "mb-5" }, [
        _c(
          "button",
          {
            staticClass: "elevated_btn elevated_btn_sm text-main",
            attrs: { type: "button" },
            on: { click: _vm.previewNow }
          },
          [_vm._v("Preview")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "mb-5" }, [
        _c(
          "button",
          {
            staticClass: "elevated_btn elevated_btn_sm  text-main",
            attrs: { type: "button" },
            on: { click: _vm.addSection }
          },
          [_vm._v("\n        Add Section\n       \n      ")]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "elevated_btn elevated_btn_sm text-main",
            attrs: { type: "button" },
            on: { click: _vm.removeSection }
          },
          [_vm._v("\n        Remove Section\n       \n      ")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-37789256", module.exports)
  }
}

/***/ }),

/***/ 1959:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1960)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1962)
/* template */
var __vue_template__ = __webpack_require__(1963)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-097a2fa9"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/liveForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-097a2fa9", Component.options)
  } else {
    hotAPI.reload("data-v-097a2fa9", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1960:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1961);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7618c84a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-097a2fa9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./liveForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-097a2fa9\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./liveForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1961:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.sub-page[data-v-097a2fa9] {\n  height: 90vh;\n  padding: 30px 20px;\n  overflow-y: scroll;\n}\np[data-v-097a2fa9] {\n  font-size: 15px;\n}\nsmall[data-v-097a2fa9] {\n  font-size: 12px;\n  font-style: italic;\n  color: red;\n}\n.form-control[data-v-097a2fa9]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-097a2fa9]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-097a2fa9]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-097a2fa9]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-097a2fa9]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-097a2fa9]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\ntable[data-v-097a2fa9] {\n  font-size: 14px;\n}\nh4[data-v-097a2fa9] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: normal;\n}\n.form[data-v-097a2fa9] {\n  padding: 30px 15px;\n}\n.fa-plus-circle[data-v-097a2fa9],\n.fa-minus-circle[data-v-097a2fa9] {\n  font-size: 12px;\n  float: right;\n  padding: 10px;\n}\nsection[data-v-097a2fa9] {\n  background: rgb(204, 204, 204, 0.5);\n  padding: 15px;\n}\n.rows[data-v-097a2fa9] {\n  background: #f7f8fa;\n  padding: 10px 15px;\n}\n.configs[data-v-097a2fa9] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 10px;\n  background: white;\n  margin-bottom: 10px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mini-label[data-v-097a2fa9] {\n  font-size: 12px;\n}\nlabel span[data-v-097a2fa9] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-097a2fa9] {\n  display: fleX;\n}\n.custom-control-input[data-v-097a2fa9] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-097a2fa9] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-097a2fa9],\ntd[data-v-097a2fa9] {\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["template", "start"],
  data: function data() {
    return {
      config: false,
      current: 0,
      groups: [],
      myValue: "",
      newHeader: "",
      new_group: false,
      row_title: "",
      form_group: "selected",
      currentIndex: 0
    };
  },

  components: {
    "app-editor": __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]

  },
  mounted: function mounted() {
    this.getGroups();
  },

  watch: {
    myValue: "addNewValue",
    newHeader: "addNewHeader",
    form_group: "changeGroup"
  },
  methods: {
    groupType: function groupType() {
      this.new_group = !this.new_group;
      if (this.new_group == true) {
        this.form_group = "";
      } else {
        this.form_group = "selected";
      }
    },
    getGroups: function getGroups() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-form-templates", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (res) {
        if (res.status = 200) {
          var groups = [];

          res.data.forEach(function (item) {
            groups.push(item.group);
          });

          var newSet = new Set(groups);
          _this.groups = Array.from(newSet);
        }
      }).catch();
    },
    changeGroup: function changeGroup() {
      this.$emit("changeGroup", this.form_group);
    },
    createForm: function createForm() {
      this.$emit("createForm");
    },
    updateRowTitle: function updateRowTitle(sectionId, rowsId, rowId) {
      this.$emit("updateRowTitle", this.row_title, sectionId, rowsId, rowId);
      this.row_title = "";
    },
    addHeader: function addHeader(sectionId, rowsId, rowId) {
      if (this.newHeader != "") {
        this.$emit("addHeader", sectionId, rowsId, rowId);
        this.newHeader = "";
      } else {
        this.$toasted.error("Input header title");
      }
    },
    removeHeader: function removeHeader(sectionId, rowsId, rowId) {
      this.$emit("removeHeader", sectionId, rowsId, rowId);
    },
    addItem: function addItem(sectionId, rowsId, rowId) {
      if (this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.length) {
        this.$emit("addItem", sectionId, rowsId, rowId);
        this.updateRowTitle(sectionId, rowsId, rowId);
      } else {
        this.$toasted.error("Input header title");
      }
    },
    removeItem: function removeItem(sectionId, rowsId, rowId) {
      this.$emit("removeItem", sectionId, rowsId, rowId);
    },
    addForm: function addForm() {
      this.$emit("addForm");
    },
    addRow: function addRow(id) {
      this.$emit("addRow", id);
    },
    removeRow: function removeRow(id) {
      this.$emit("removeRow", id);
    },
    addValue: function addValue(sectionId, rowsId, rowId) {
      this.$emit("addValue", sectionId, rowsId, rowId);
      this.myValue = "";
    },
    addNewValue: function addNewValue() {
      this.$emit("updateValue", this.myValue);
    },
    addNewHeader: function addNewHeader() {
      this.$emit("updateHeader", this.newHeader);
    },
    showConfig: function showConfig(index, id) {
      this.current = id;
      this.currentIndex = index;
    },
    closeConfig: function closeConfig() {
      this.current = null;
      this.currentIndex = null;
    }
  }
});

/***/ }),

/***/ 1963:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "sub-page" }, [
    _c("div", [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm btn-compliment text-white",
          attrs: { type: "button" },
          on: { click: _vm.createForm }
        },
        [_vm._v("Create Form")]
      )
    ]),
    _vm._v(" "),
    _vm.start
      ? _c(
          "div",
          { staticClass: "form" },
          [
            _vm.new_group
              ? _c("div", { staticClass: "form-group mb-5" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form_group,
                        expression: "form_group"
                      }
                    ],
                    staticClass: "form-control w-50 mb-3",
                    attrs: {
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "Enter form group title"
                    },
                    domProps: { value: _vm.form_group },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.form_group = $event.target.value
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn elevated_btn_sm text-main mb-3",
                      attrs: { type: "button" },
                      on: { click: _vm.groupType }
                    },
                    [_vm._v("Select group")]
                  )
                ])
              : _c("div", { staticClass: "form-group mb-5" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form_group,
                          expression: "form_group"
                        }
                      ],
                      staticClass: "form-control mb-3 w-50",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.form_group = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c(
                        "option",
                        { attrs: { disabled: "", value: "selected" } },
                        [_vm._v("Select Group")]
                      ),
                      _vm._v(" "),
                      _c("option", [_vm._v("Discovering what is within")]),
                      _vm._v(" "),
                      _c("option", [_vm._v("Business Education")]),
                      _vm._v(" "),
                      _vm._l(_vm.groups, function(item, idx) {
                        return _c("option", { key: idx }, [
                          _vm._v(_vm._s(item))
                        ])
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "elevated_btn elevated_btn_sm  text-main",
                      attrs: { type: "button" },
                      on: { click: _vm.groupType }
                    },
                    [_vm._v("Add new group")]
                  )
                ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.template.form.legend,
                    expression: "template.form.legend"
                  }
                ],
                staticClass: "form-control w-50",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Enter form title"
                },
                domProps: { value: _vm.template.form.legend },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.template.form, "legend", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _vm._l(_vm.template.form.sections, function(section, index) {
              return _c(
                "section",
                { key: index, staticClass: "mb-3" },
                [
                  _c("div", { staticClass: "mb-2" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: section.section.label,
                          expression: "section.section.label"
                        }
                      ],
                      staticClass: "form-control w-25",
                      attrs: {
                        type: "text",
                        placeholder: "Enter section title",
                        required: ""
                      },
                      domProps: { value: section.section.label },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            section.section,
                            "label",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("label", { staticClass: "mini-label" }, [
                        _vm._v("Section Description")
                      ]),
                      _vm._v(" "),
                      _c("app-editor", {
                        staticClass: "form-control",
                        attrs: {
                          apiKey:
                            "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3",
                          init: { plugins: "wordcount, lists, advlist" }
                        },
                        model: {
                          value: section.section.description,
                          callback: function($$v) {
                            _vm.$set(section.section, "description", $$v)
                          },
                          expression: "section.section.description"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "right mini-label mr-4",
                      on: {
                        click: function($event) {
                          return _vm.addRow(index)
                        }
                      }
                    },
                    [
                      _c(
                        "i",
                        {
                          staticClass: "fa fa-plus-circle text-main",
                          attrs: { "aria-hidden": "true" }
                        },
                        [_vm._v("Add row")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "right mini-label",
                      on: {
                        click: function($event) {
                          return _vm.removeRow(index)
                        }
                      }
                    },
                    [
                      _c(
                        "i",
                        {
                          staticClass: "fa fa-minus-circle text-main",
                          attrs: { "aria-hidden": "true" }
                        },
                        [_vm._v("Remove row")]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm._l(section.section.rows, function(rows, idx) {
                    return _c(
                      "div",
                      { key: idx },
                      _vm._l(rows.row, function(row, id) {
                        return _c(
                          "div",
                          { key: id, staticClass: "mb-3 rows" },
                          [
                            _c("span", { staticClass: "text-right" }, [
                              idx !== _vm.current || index !== _vm.currentIndex
                                ? _c("i", {
                                    staticClass: "fa fa-plus-circle",
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.showConfig(index, idx)
                                      }
                                    }
                                  })
                                : _vm._e(),
                              _vm._v(" "),
                              idx == _vm.current && index == _vm.currentIndex
                                ? _c("i", {
                                    staticClass: "fa fa-minus-circle",
                                    attrs: { "aria-hidden": "true" },
                                    on: {
                                      click: function($event) {
                                        return _vm.closeConfig()
                                      }
                                    }
                                  })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            idx == _vm.current && index == _vm.currentIndex
                              ? _c("div", { staticClass: "configs" }, [
                                  _c("div", [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c(
                                        "label",
                                        { staticClass: "mini-label" },
                                        [_vm._v("Select type")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.type,
                                              expression: "row.type"
                                            }
                                          ],
                                          staticClass: "custom-select",
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.$set(
                                                row,
                                                "type",
                                                $event.target.multiple
                                                  ? $$selectedVal
                                                  : $$selectedVal[0]
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "option",
                                            {
                                              attrs: {
                                                disabled: "",
                                                value: "selected"
                                              }
                                            },
                                            [_vm._v("Select one")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "text" } },
                                            [_vm._v("Text")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "textarea" } },
                                            [_vm._v("Textarea")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "number" } },
                                            [_vm._v("Number")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "date" } },
                                            [_vm._v("Date")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "time" } },
                                            [_vm._v("Time")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "select" } },
                                            [_vm._v("Select Option")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "checkbox" } },
                                            [_vm._v("Checkbox")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "radio" } },
                                            [_vm._v("Radio")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            { attrs: { value: "table" } },
                                            [_vm._v("Table")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            {
                                              attrs: {
                                                value: "table with textarea"
                                              }
                                            },
                                            [_vm._v("Table with Textarea")]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "option",
                                            {
                                              attrs: {
                                                value:
                                                  "table with multiple data"
                                              }
                                            },
                                            [_vm._v("Table with Multiple Data")]
                                          )
                                        ]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    row.type == "table"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex p-2 border align-items-start"
                                          },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "form-group mr-3"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.newHeader,
                                                      expression: "newHeader"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder:
                                                      "type header here",
                                                    "aria-describedby": "helpId"
                                                  },
                                                  domProps: {
                                                    value: _vm.newHeader
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.newHeader =
                                                        $event.target.value
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add header")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove header")]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "d-flex" },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.row_title,
                                                          expression:
                                                            "row_title"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        placeholder:
                                                          "type row title here..",
                                                        "aria-describedby":
                                                          "helpId"
                                                      },
                                                      domProps: {
                                                        value: _vm.row_title
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.row_title =
                                                            $event.target.value
                                                        }
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add row")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove row")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    row.type == "table with textarea"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex p-2 border align-items-start"
                                          },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "form-group mr-3"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.newHeader,
                                                      expression: "newHeader"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder:
                                                      "type header here",
                                                    "aria-describedby": "helpId"
                                                  },
                                                  domProps: {
                                                    value: _vm.newHeader
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.newHeader =
                                                        $event.target.value
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add header")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove header")]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "d-flex" },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.row_title,
                                                          expression:
                                                            "row_title"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        placeholder:
                                                          "type row title here..",
                                                        "aria-describedby":
                                                          "helpId"
                                                      },
                                                      domProps: {
                                                        value: _vm.row_title
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.row_title =
                                                            $event.target.value
                                                        }
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add row")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove row")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    row.type == "table with multiple data"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex p-2 border align-items-start"
                                          },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "form-group mr-3"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.newHeader,
                                                      expression: "newHeader"
                                                    }
                                                  ],
                                                  staticClass: "form-control",
                                                  attrs: {
                                                    type: "text",
                                                    placeholder:
                                                      "type header here",
                                                    "aria-describedby": "helpId"
                                                  },
                                                  domProps: {
                                                    value: _vm.newHeader
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.newHeader =
                                                        $event.target.value
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add header")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeHeader(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove header")]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "d-flex" },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.row_title,
                                                          expression:
                                                            "row_title"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        placeholder:
                                                          "type row title here..",
                                                        "aria-describedby":
                                                          "helpId"
                                                      },
                                                      domProps: {
                                                        value: _vm.row_title
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.row_title =
                                                            $event.target.value
                                                        }
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.addItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add row")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "elevated_btn elevated_btn_sm text-main",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeItem(
                                                          index,
                                                          idx,
                                                          id
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Remove row")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    row.type == "radio" ||
                                    row.type == "checkbox" ||
                                    row.type == "select"
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "form-group d-flex border p-2"
                                          },
                                          [
                                            _c("div", { staticClass: "mr-3" }, [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.myValue,
                                                    expression: "myValue"
                                                  }
                                                ],
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "text",
                                                  "aria-describedby": "helpId",
                                                  placeholder:
                                                    "Enter value here"
                                                },
                                                domProps: {
                                                  value: _vm.myValue
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.myValue =
                                                      $event.target.value
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "button",
                                                {
                                                  staticClass:
                                                    "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                  attrs: { type: "button" },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.addValue(
                                                        index,
                                                        idx,
                                                        id
                                                      )
                                                    }
                                                  }
                                                },
                                                [_vm._v("Add Value")]
                                              )
                                            ]),
                                            _vm._v(" "),
                                            row.type == "checkbox"
                                              ? _c(
                                                  "div",
                                                  { staticClass: "form-group" },
                                                  [
                                                    _c(
                                                      "label",
                                                      {
                                                        staticClass:
                                                          "mini-label",
                                                        attrs: { for: "" }
                                                      },
                                                      [_vm._v("Multiple")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "d-flex align-items-center"
                                                      },
                                                      [
                                                        _c(
                                                          "label",
                                                          {
                                                            staticClass:
                                                              "custom-control custom-radio mini-label mr-3"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    row.multiple,
                                                                  expression:
                                                                    "row.multiple"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "custom-control-input",
                                                              attrs: {
                                                                type: "radio",
                                                                value: "true"
                                                              },
                                                              domProps: {
                                                                checked: _vm._q(
                                                                  row.multiple,
                                                                  "true"
                                                                )
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.$set(
                                                                    row,
                                                                    "multiple",
                                                                    "true"
                                                                  )
                                                                }
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "custom-control-indicator"
                                                              },
                                                              [_vm._v("True")]
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "label",
                                                          {
                                                            staticClass:
                                                              "custom-control custom-radio mini-label"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    row.multiple,
                                                                  expression:
                                                                    "row.multiple"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "custom-control-input",
                                                              attrs: {
                                                                type: "radio",
                                                                value: "false"
                                                              },
                                                              domProps: {
                                                                checked: _vm._q(
                                                                  row.multiple,
                                                                  "false"
                                                                )
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  return _vm.$set(
                                                                    row,
                                                                    "multiple",
                                                                    "false"
                                                                  )
                                                                }
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "span",
                                                              {
                                                                staticClass:
                                                                  "custom-control-indicator"
                                                              },
                                                              [_vm._v("False")]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e()
                                          ]
                                        )
                                      : _vm._e()
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group mb-2" },
                                      [
                                        _c("textarea", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.description,
                                              expression: "row.description"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            rows: "3",
                                            placeholder: "Enter description"
                                          },
                                          domProps: { value: row.description },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "description",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.label,
                                            expression: "row.label"
                                          }
                                        ],
                                        staticClass: "form-control mb-2",
                                        attrs: {
                                          type: "text",
                                          placeholder: "Enter question title"
                                        },
                                        domProps: { value: row.label },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              row,
                                              "label",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.note,
                                            expression: "row.note"
                                          }
                                        ],
                                        staticClass: "form-control mb-2",
                                        attrs: {
                                          type: "text",
                                          placeholder: "Enter note"
                                        },
                                        domProps: { value: row.note },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              row,
                                              "note",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group mb-3" },
                                      [
                                        _c(
                                          "label",
                                          {
                                            staticClass: "mini-label",
                                            attrs: { for: "" }
                                          },
                                          [_vm._v("Placeholder")]
                                        ),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.placeholder,
                                              expression: "row.placeholder"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            type: "text",
                                            placeholder: "Enter placeholder"
                                          },
                                          domProps: { value: row.placeholder },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "placeholder",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "form-group" }, [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "mini-label",
                                          attrs: { for: "" }
                                        },
                                        [_vm._v("Required")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex align-items-center"
                                        },
                                        [
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "custom-control custom-radio mini-label mr-3"
                                            },
                                            [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: row.required,
                                                    expression: "row.required"
                                                  }
                                                ],
                                                staticClass:
                                                  "custom-control-input",
                                                attrs: {
                                                  type: "radio",
                                                  value: "true"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    row.required,
                                                    "true"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      row,
                                                      "required",
                                                      "true"
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "custom-control-indicator"
                                                },
                                                [_vm._v("True")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "label",
                                            {
                                              staticClass:
                                                "custom-control custom-radio mini-label"
                                            },
                                            [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: row.required,
                                                    expression: "row.required"
                                                  }
                                                ],
                                                staticClass:
                                                  "custom-control-input",
                                                attrs: {
                                                  type: "radio",
                                                  value: "false"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    row.required,
                                                    "false"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      row,
                                                      "required",
                                                      "false"
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "custom-control-indicator"
                                                },
                                                [_vm._v("False")]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ])
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("div", [
                              _c("p", [_vm._v(_vm._s(row.description))]),
                              _vm._v(" "),
                              row.type == "radio"
                                ? _c(
                                    "div",
                                    { staticClass: "form-group" },
                                    [
                                      _c("h4", [_vm._v(_vm._s(row.label))]),
                                      _vm._v(" "),
                                      _c("small", [_vm._v(_vm._s(row.note))]),
                                      _vm._v(" "),
                                      _vm._l(row.values, function(value, id) {
                                        return _c(
                                          "label",
                                          {
                                            key: id,
                                            staticClass:
                                              "custom-control custom-radio"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answer,
                                                  expression: "row.answer"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: { type: "radio" },
                                              domProps: {
                                                value: value,
                                                checked: _vm._q(
                                                  row.answer,
                                                  value
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    row,
                                                    "answer",
                                                    value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v(_vm._s(value))]
                                            )
                                          ]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                : row.type == "select"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answer,
                                            expression: "row.answer"
                                          }
                                        ],
                                        staticClass: "custom-select",
                                        on: {
                                          change: function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              row,
                                              "answer",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "option",
                                          {
                                            attrs: {
                                              disabled: "",
                                              selected: ""
                                            }
                                          },
                                          [_vm._v("Select one")]
                                        ),
                                        _vm._v(" "),
                                        _vm._l(row.values, function(value, id) {
                                          return _c("option", { key: id }, [
                                            _vm._v(_vm._s(value))
                                          ])
                                        })
                                      ],
                                      2
                                    )
                                  ])
                                : row.type == "checkbox"
                                ? _c(
                                    "div",
                                    { staticClass: "form-group" },
                                    [
                                      _c("h4", [_vm._v(_vm._s(row.label))]),
                                      _vm._v(" "),
                                      _c("small", [_vm._v(_vm._s(row.note))]),
                                      _vm._v(" "),
                                      _vm._l(row.values, function(value, id) {
                                        return _c(
                                          "label",
                                          {
                                            key: id,
                                            staticClass:
                                              "custom-control custom-checkbox"
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answers,
                                                  expression: "row.answers"
                                                }
                                              ],
                                              staticClass:
                                                "custom-control-input",
                                              attrs: {
                                                type: "checkbox",
                                                multiple: row.multiple
                                              },
                                              domProps: {
                                                value: value,
                                                checked: Array.isArray(
                                                  row.answers
                                                )
                                                  ? _vm._i(row.answers, value) >
                                                    -1
                                                  : row.answers
                                              },
                                              on: {
                                                change: function($event) {
                                                  var $$a = row.answers,
                                                    $$el = $event.target,
                                                    $$c = $$el.checked
                                                      ? true
                                                      : false
                                                  if (Array.isArray($$a)) {
                                                    var $$v = value,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          row,
                                                          "answers",
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          row,
                                                          "answers",
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(
                                                      row,
                                                      "answers",
                                                      $$c
                                                    )
                                                  }
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                staticClass:
                                                  "custom-control-indicator"
                                              },
                                              [_vm._v(_vm._s(value))]
                                            )
                                          ]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                : row.type == "table"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    row.table.headers.length
                                      ? _c(
                                          "table",
                                          {
                                            staticClass: "table table-bordered"
                                          },
                                          [
                                            _c("thead", [
                                              _c(
                                                "tr",
                                                [
                                                  _c("th"),
                                                  _vm._v(" "),
                                                  _vm._l(
                                                    row.table.headers,
                                                    function(column, index) {
                                                      return _c(
                                                        "th",
                                                        { key: index },
                                                        [_vm._v(_vm._s(column))]
                                                      )
                                                    }
                                                  )
                                                ],
                                                2
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "tbody",
                                              _vm._l(row.table.items, function(
                                                item,
                                                index
                                              ) {
                                                return _c(
                                                  "tr",
                                                  { key: index },
                                                  [
                                                    _c("td", [
                                                      _vm._v(
                                                        _vm._s(
                                                          row.table.row_titles[
                                                            index
                                                          ]
                                                        )
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _vm._l(
                                                      row.table.headers,
                                                      function(
                                                        column,
                                                        indexColumn
                                                      ) {
                                                        return _c(
                                                          "td",
                                                          {
                                                            key: indexColumn,
                                                            staticClass: "p-0"
                                                          },
                                                          [
                                                            _c("input", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    item[
                                                                      column +
                                                                        index +
                                                                        indexColumn
                                                                    ],
                                                                  expression:
                                                                    "item[column + index + indexColumn]"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "form-control border-0",
                                                              attrs: {
                                                                type: "text",
                                                                placeholder:
                                                                  row.placeholder
                                                              },
                                                              domProps: {
                                                                value:
                                                                  item[
                                                                    column +
                                                                      index +
                                                                      indexColumn
                                                                  ]
                                                              },
                                                              on: {
                                                                input: function(
                                                                  $event
                                                                ) {
                                                                  if (
                                                                    $event
                                                                      .target
                                                                      .composing
                                                                  ) {
                                                                    return
                                                                  }
                                                                  _vm.$set(
                                                                    item,
                                                                    column +
                                                                      index +
                                                                      indexColumn,
                                                                    $event
                                                                      .target
                                                                      .value
                                                                  )
                                                                }
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      }
                                                    )
                                                  ],
                                                  2
                                                )
                                              }),
                                              0
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  ])
                                : row.type == "table with textarea"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    row.table.headers.length
                                      ? _c(
                                          "table",
                                          {
                                            staticClass: "table table-bordered"
                                          },
                                          [
                                            _c("thead", [
                                              _c(
                                                "tr",
                                                [
                                                  _c("th"),
                                                  _vm._v(" "),
                                                  _vm._l(
                                                    row.table.headers,
                                                    function(column, index) {
                                                      return _c(
                                                        "th",
                                                        { key: index },
                                                        [_vm._v(_vm._s(column))]
                                                      )
                                                    }
                                                  )
                                                ],
                                                2
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "tbody",
                                              _vm._l(row.table.items, function(
                                                item,
                                                index
                                              ) {
                                                return _c(
                                                  "tr",
                                                  { key: index },
                                                  [
                                                    _c("td", [
                                                      _vm._v(
                                                        _vm._s(
                                                          row.table.row_titles[
                                                            index
                                                          ]
                                                        )
                                                      )
                                                    ]),
                                                    _vm._v(" "),
                                                    _vm._l(
                                                      row.table.headers,
                                                      function(
                                                        column,
                                                        indexColumn
                                                      ) {
                                                        return _c(
                                                          "td",
                                                          {
                                                            key: indexColumn,
                                                            staticClass: "p-0"
                                                          },
                                                          [
                                                            _c("textarea", {
                                                              directives: [
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    item[
                                                                      column +
                                                                        index +
                                                                        indexColumn
                                                                    ],
                                                                  expression:
                                                                    "item[column + index + indexColumn]"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "form-control",
                                                              attrs: {
                                                                rows: "5",
                                                                placeholder:
                                                                  row.placeholder
                                                              },
                                                              domProps: {
                                                                value:
                                                                  item[
                                                                    column +
                                                                      index +
                                                                      indexColumn
                                                                  ]
                                                              },
                                                              on: {
                                                                input: function(
                                                                  $event
                                                                ) {
                                                                  if (
                                                                    $event
                                                                      .target
                                                                      .composing
                                                                  ) {
                                                                    return
                                                                  }
                                                                  _vm.$set(
                                                                    item,
                                                                    column +
                                                                      index +
                                                                      indexColumn,
                                                                    $event
                                                                      .target
                                                                      .value
                                                                  )
                                                                }
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      }
                                                    )
                                                  ],
                                                  2
                                                )
                                              }),
                                              0
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  ])
                                : row.type == "textarea"
                                ? _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("textarea", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: row.answer,
                                          expression: "row.answer"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        rows: "3",
                                        required: row.required,
                                        placeholder: row.placeholder
                                      },
                                      domProps: { value: row.answer },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            row,
                                            "answer",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                : _c("div", { staticClass: "form-group" }, [
                                    _c("h4", [_vm._v(_vm._s(row.label))]),
                                    _vm._v(" "),
                                    _c("small", [_vm._v(_vm._s(row.note))]),
                                    _vm._v(" "),
                                    row.type === "checkbox"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: "checkbox"
                                          },
                                          domProps: {
                                            checked: Array.isArray(row.answer)
                                              ? _vm._i(row.answer, null) > -1
                                              : row.answer
                                          },
                                          on: {
                                            change: function($event) {
                                              var $$a = row.answer,
                                                $$el = $event.target,
                                                $$c = $$el.checked
                                                  ? true
                                                  : false
                                              if (Array.isArray($$a)) {
                                                var $$v = null,
                                                  $$i = _vm._i($$a, $$v)
                                                if ($$el.checked) {
                                                  $$i < 0 &&
                                                    _vm.$set(
                                                      row,
                                                      "answer",
                                                      $$a.concat([$$v])
                                                    )
                                                } else {
                                                  $$i > -1 &&
                                                    _vm.$set(
                                                      row,
                                                      "answer",
                                                      $$a
                                                        .slice(0, $$i)
                                                        .concat(
                                                          $$a.slice($$i + 1)
                                                        )
                                                    )
                                                }
                                              } else {
                                                _vm.$set(row, "answer", $$c)
                                              }
                                            }
                                          }
                                        })
                                      : row.type === "radio"
                                      ? _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: "radio"
                                          },
                                          domProps: {
                                            checked: _vm._q(row.answer, null)
                                          },
                                          on: {
                                            change: function($event) {
                                              return _vm.$set(
                                                row,
                                                "answer",
                                                null
                                              )
                                            }
                                          }
                                        })
                                      : _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            placeholder: row.placeholder,
                                            required: row.required,
                                            type: row.type
                                          },
                                          domProps: { value: row.answer },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                  ])
                            ])
                          ]
                        )
                      }),
                      0
                    )
                  })
                ],
                2
              )
            })
          ],
          2
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-097a2fa9", module.exports)
  }
}

/***/ }),

/***/ 1964:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c(
      "div",
      { staticClass: "live" },
      [
        _c(
          "router-link",
          { staticClass: "back py-3 px-2 ", attrs: { to: "/form-questions" } },
          [
            _c("i", {
              staticClass: "fa fa-long-arrow-left",
              attrs: { "aria-hidden": "true" }
            }),
            _vm._v(" Back \n    ")
          ]
        ),
        _vm._v(" "),
        _vm.prev
          ? _c("div", { staticClass: "preview" }, [
              _c(
                "div",
                { staticClass: "body shadow" },
                [
                  _c("Preview", {
                    attrs: { template: _vm.template },
                    on: { close: _vm.previewNow }
                  })
                ],
                1
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("Live", {
          attrs: { template: _vm.template, start: _vm.start },
          on: {
            changeGroup: _vm.changeGroup,
            addForm: _vm.addForm,
            addRow: _vm.addRow,
            removeRow: _vm.removeRow,
            addValue: _vm.addValue,
            updateValue: _vm.updateValue,
            addItem: _vm.addItem,
            removeItem: _vm.removeItem,
            addHeader: _vm.addHeader,
            removeHeader: _vm.removeHeader,
            updateHeader: _vm.updateHeader,
            updateRowTitle: _vm.updateRowTitle,
            createForm: _vm.createForm
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "gui" },
      [
        _c("Gui", {
          on: {
            addSection: _vm.addSection,
            removeSection: _vm.removeSection,
            previewNow: _vm.previewNow
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-eafabbe0", module.exports)
  }
}

/***/ }),

/***/ 659:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1951)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1953)
/* template */
var __vue_template__ = __webpack_require__(1964)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-eafabbe0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/formComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-eafabbe0", Component.options)
  } else {
    hotAPI.reload("data-v-eafabbe0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ }),

/***/ 994:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(995)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(997)
/* template */
var __vue_template__ = __webpack_require__(998)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2fcaa063"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/form/previewForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2fcaa063", Component.options)
  } else {
    hotAPI.reload("data-v-2fcaa063", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 995:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(996);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("72991d8e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2fcaa063\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2fcaa063\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 996:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.number[data-v-2fcaa063] {\n  font-size: 14px;\n}\n.form-text[data-v-2fcaa063]{\n  color: red;\n}\n.main-preview[data-v-2fcaa063] {\n  width: 80%;\n  margin: 0 auto;\n}\n.form-control[data-v-2fcaa063]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-2fcaa063]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\ntable[data-v-2fcaa063] {\n  font-size: 14px;\n}\nh4[data-v-2fcaa063] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\nh5[data-v-2fcaa063] {\n  font-size: 14px;\n  margin-bottom: 10px;\n}\np[data-v-2fcaa063] {\n  margin: 0 0 10px;\n   font-size: 15px;\n}\n.mini-label[data-v-2fcaa063] {\n  font-size: 13px;\n}\nlabel span[data-v-2fcaa063] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-2fcaa063] {\n  display: fleX;\n}\n.custom-control-input[data-v-2fcaa063] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-2fcaa063] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-2fcaa063],\ntd[data-v-2fcaa063] {\n  text-align: center;\n}\ntable[data-v-2fcaa063] {\n  font-size: 14px;\n}\nli[data-v-2fcaa063] {\n  font-size: 14px;\n}\n@media (max-width: 425px) {\n.main-preview[data-v-2fcaa063] {\n    width: 100%;\n    margin: 0 auto;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 997:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["template"],
  methods: {
    close: function close() {
      this.$emit('close');
    }
  }
});

/***/ }),

/***/ 998:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "p-4 bg-white main-preview" },
    [
      _c("h3", { staticClass: "text-center mb-3" }, [
        _vm._v(_vm._s(_vm.template.form.legend))
      ]),
      _vm._v(" "),
      _c("p", [_vm._v(_vm._s(_vm.template.form.description))]),
      _vm._v(" "),
      _vm._l(_vm.template.form.sections, function(section, index) {
        return _c("section", { key: index, staticClass: "mb-3" }, [
          _c("div", { staticClass: "number" }, [
            _vm._v("Section " + _vm._s(index + 1))
          ]),
          _vm._v(" "),
          _c("h4", { staticClass: "mb-2" }, [
            _vm._v(_vm._s(section.section.label))
          ]),
          _vm._v(" "),
          _c("p", {
            staticClass: "section_desc",
            domProps: { innerHTML: _vm._s(section.section.description) }
          }),
          _vm._v(" "),
          _c(
            "ol",
            _vm._l(section.section.rows, function(rows, idx) {
              return _c(
                "div",
                { key: idx },
                _vm._l(rows.row, function(row, id) {
                  return _c("div", { key: id, staticClass: "mb-3 rows" }, [
                    _c("p", [_vm._v(_vm._s(row.description))]),
                    _vm._v(" "),
                    _c("li", [
                      _c("div", [
                        row.type == "radio"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass: "custom-control custom-radio"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answer,
                                            expression: "row.answer"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: { readonly: "", type: "radio" },
                                        domProps: {
                                          value: value,
                                          checked: _vm._q(row.answer, value)
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              row,
                                              "answer",
                                              value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "select"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: row.answer,
                                      expression: "row.answer"
                                    }
                                  ],
                                  staticClass: "custom-select",
                                  attrs: { readonly: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        row,
                                        "answer",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    { attrs: { disabled: "", selected: "" } },
                                    [_vm._v("Select one")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(row.values, function(value, id) {
                                    return _c("option", { key: id }, [
                                      _vm._v(_vm._s(value))
                                    ])
                                  })
                                ],
                                2
                              )
                            ])
                          : row.type == "checkbox"
                          ? _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("h5", [_vm._v(_vm._s(row.label))]),
                                _vm._v(" "),
                                _c(
                                  "small",
                                  { staticClass: "form-text text-muted" },
                                  [_vm._v(_vm._s(row.note))]
                                ),
                                _vm._v(" "),
                                _vm._l(row.values, function(value, id) {
                                  return _c(
                                    "label",
                                    {
                                      key: id,
                                      staticClass:
                                        "custom-control custom-checkbox"
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: row.answers,
                                            expression: "row.answers"
                                          }
                                        ],
                                        staticClass: "custom-control-input",
                                        attrs: {
                                          readonly: "",
                                          type: "checkbox",
                                          multiple: row.multiple
                                        },
                                        domProps: {
                                          value: value,
                                          checked: Array.isArray(row.answers)
                                            ? _vm._i(row.answers, value) > -1
                                            : row.answers
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = row.answers,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = value,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    row,
                                                    "answers",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(row, "answers", $$c)
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "custom-control-indicator"
                                        },
                                        [_vm._v(_vm._s(value))]
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          : row.type == "table"
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.table.headers.length
                                ? _c(
                                    "table",
                                    { staticClass: "table table-bordered" },
                                    [
                                      _c("thead", [
                                        _c(
                                          "tr",
                                          [
                                            _c("th"),
                                            _vm._v(" "),
                                            _vm._l(row.table.headers, function(
                                              column,
                                              index
                                            ) {
                                              return _c("th", { key: index }, [
                                                _vm._v(_vm._s(column))
                                              ])
                                            })
                                          ],
                                          2
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        _vm._l(row.table.items, function(
                                          item,
                                          index
                                        ) {
                                          return _c(
                                            "tr",
                                            { key: index },
                                            [
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    row.table.row_titles[index]
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _vm._l(
                                                row.table.headers,
                                                function(column, indexColumn) {
                                                  return _c(
                                                    "td",
                                                    {
                                                      key: indexColumn,
                                                      staticClass: "p-0"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value:
                                                              item[
                                                                column +
                                                                  index +
                                                                  indexColumn
                                                              ],
                                                            expression:
                                                              "item[column + index + indexColumn]"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "form-control border-0",
                                                        attrs: {
                                                          readonly: "",
                                                          type: "text",
                                                          placeholder:
                                                            "type here"
                                                        },
                                                        domProps: {
                                                          value:
                                                            item[
                                                              column +
                                                                index +
                                                                indexColumn
                                                            ]
                                                        },
                                                        on: {
                                                          input: function(
                                                            $event
                                                          ) {
                                                            if (
                                                              $event.target
                                                                .composing
                                                            ) {
                                                              return
                                                            }
                                                            _vm.$set(
                                                              item,
                                                              column +
                                                                index +
                                                                indexColumn,
                                                              $event.target
                                                                .value
                                                            )
                                                          }
                                                        }
                                                      })
                                                    ]
                                                  )
                                                }
                                              )
                                            ],
                                            2
                                          )
                                        }),
                                        0
                                      )
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : (row.type = "text")
                          ? _c("div", { staticClass: "form-group" }, [
                              _c("h5", [_vm._v(_vm._s(row.label))]),
                              _vm._v(" "),
                              _c(
                                "small",
                                { staticClass: "form-text text-muted" },
                                [_vm._v(_vm._s(row.note))]
                              ),
                              _vm._v(" "),
                              row.type === "checkbox"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "checkbox"
                                    },
                                    domProps: {
                                      checked: Array.isArray(row.answer)
                                        ? _vm._i(row.answer, null) > -1
                                        : row.answer
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = row.answer,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = null,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a.concat([$$v])
                                              )
                                          } else {
                                            $$i > -1 &&
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1))
                                              )
                                          }
                                        } else {
                                          _vm.$set(row, "answer", $$c)
                                        }
                                      }
                                    }
                                  })
                                : row.type === "radio"
                                ? _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: "radio"
                                    },
                                    domProps: {
                                      checked: _vm._q(row.answer, null)
                                    },
                                    on: {
                                      change: function($event) {
                                        return _vm.$set(row, "answer", null)
                                      }
                                    }
                                  })
                                : _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: row.answer,
                                        expression: "row.answer"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      readonly: "",
                                      placeholder: row.placeholder,
                                      required: row.required,
                                      type: row.type
                                    },
                                    domProps: { value: row.answer },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          row,
                                          "answer",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                            ])
                          : _c("div")
                      ])
                    ])
                  ])
                }),
                0
              )
            }),
            0
          )
        ])
      }),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "butt mt-3 text-center w-100" }, [
        _c(
          "button",
          {
            staticClass:
              "elevated_btn elevated_btn_sm text-white btn-compliment mx-auto",
            attrs: { type: "button" },
            on: { click: _vm.close }
          },
          [_vm._v("Close")]
        )
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2fcaa063", module.exports)
  }
}

/***/ })

});