webpackJsonp([91],{

/***/ 1107:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1108);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("daf69204", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-889ae410\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorMessages.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-889ae410\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorMessages.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1108:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-889ae410] {\n  padding: 30px;\n  height: 100%;\n  width: 100%;\n}\n.faded[data-v-889ae410]{\n  color: rgba(0, 0, 0, 0.4);\n}\n.option-box[data-v-889ae410] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: 100%;\n  width: 100%;\n}\n.private[data-v-889ae410],\n.group[data-v-889ae410] {\n  width: 400px;\n  height: 400px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: white;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  cursor: pointer;\n}\n.private[data-v-889ae410]:hover,\n.group[data-v-889ae410]:hover {\n  -webkit-box-shadow: 0 0.175rem 0.24rem rgba(0, 0, 0, 0.15) !important;\n          box-shadow: 0 0.175rem 0.24rem rgba(0, 0, 0, 0.15) !important;\n}\n.private:hover .fas[data-v-889ae410],\n.group:hover .fas[data-v-889ae410],\n.private:hover h3[data-v-889ae410],\n.group:hover h3[data-v-889ae410] {\n  -webkit-transform: scale(1.05);\n          transform: scale(1.05);\n}\n@media(max-width: 425px){\n.option-box[data-v-889ae410]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.private[data-v-889ae410], .group[data-v-889ae410]{\n    width: 200px;\n    height: 200px;\n}\n.fa-5x[data-v-889ae410]{\n    font-size: 3em;\n}\nh3.josefin[data-v-889ae410]{\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['vendorSub']

});

/***/ }),

/***/ 1110:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _vm.vendorSub > 1
      ? _c(
          "div",
          { staticClass: "option-box" },
          [
            _c(
              "router-link",
              { attrs: { to: { name: "VendorPrivateChat" } } },
              [
                _c("div", { staticClass: "private" }, [
                  _c("i", {
                    staticClass: "fas fa-user-lock fa-5x text-main mb-3"
                  }),
                  _vm._v(" "),
                  _c("h3", { staticClass: "josefin text-main" }, [
                    _vm._v("Private Messages")
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c("router-link", { attrs: { to: { name: "VendorGroupChat" } } }, [
              _c("div", { staticClass: "group" }, [
                _c("i", { staticClass: "fas fa-users fa-5x text-main mb-3" }),
                _vm._v(" "),
                _c("h3", { staticClass: "josefin text-main" }, [
                  _vm._v("Group Messages")
                ])
              ])
            ])
          ],
          1
        )
      : _c("div", { staticClass: "option-box" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1)
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", [
      _c("div", { staticClass: "private" }, [
        _c("i", { staticClass: "fas fa-user-lock fa-5x text-main mb-3 faded" }),
        _vm._v(" "),
        _c("h3", { staticClass: "josefin faded" }, [_vm._v("Private Messages")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", [
      _c("div", { staticClass: "group" }, [
        _c("i", { staticClass: "fas fa-users fa-5x text-main mb-3 faded" }),
        _vm._v(" "),
        _c("h3", { staticClass: "josefin faded" }, [_vm._v("Group Messages")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-889ae410", module.exports)
  }
}

/***/ }),

/***/ 494:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1107)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1109)
/* template */
var __vue_template__ = __webpack_require__(1110)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-889ae410"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorMessages.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-889ae410", Component.options)
  } else {
    hotAPI.reload("data-v-889ae410", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});