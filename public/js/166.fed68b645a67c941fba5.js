webpackJsonp([166],{

/***/ 1308:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1309);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("b991b860", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37ee41ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminReviewsComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37ee41ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminReviewsComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1309:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-37ee41ca] {\n    height: 100vh;\n}\n.main-page[data-v-37ee41ca] {\n    padding: 40px 20px;\n}\nth[data-v-37ee41ca] {\n    background-color: #f3f3f3;\n    color: #000000;\n    font-weight: bold;\n}\nth[data-v-37ee41ca], td[data-v-37ee41ca] {\n    padding: 30px !important;\n}\ntd[data-v-37ee41ca] {\n    color: #aaaaaa;\n}\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-reviews-component",
    data: function data() {
        return {
            reviews: [],
            token: '',
            verifyBtn: 'Verify'
        };
    },
    mounted: function mounted() {
        var _this = this;

        var admin = JSON.parse(localStorage.getItem('authAdmin'));
        this.token = admin.access_token;

        if (admin != null) {
            axios.get('/api/reviews', { headers: { "Authorization": 'Bearer ' + admin.access_token } }).then(function (response) {
                if (response.status === 200) {
                    console.log(response.data.data);

                    _this.reviews = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        verifyReview: function verifyReview(id) {
            var _this2 = this;

            axios.get('/api/admin/verify/reviews/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    window.location.reload();
                    _this2.$toasted.success('Status successfully change');
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        deleteReview: function deleteReview(id, index) {
            var _this3 = this;

            console.log(id);
            axios.delete('/api/reviews/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.reviews.splice(index, 1);
                    _this3.$toasted.success('Successfully remove');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1311:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("table", { staticClass: "table" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.reviews, function(review, index) {
            return _c("tr", [
              _c("td", [_vm._v(_vm._s(index + 1))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(review.title))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(review.description))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(review.rating))]),
              _vm._v(" "),
              _c("td", [
                _vm._v(
                  _vm._s(review.user.name) + " " + _vm._s(review.user.lastName)
                )
              ]),
              _vm._v(" "),
              _c("td", [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-sm",
                    on: {
                      click: function($event) {
                        return _vm.verifyReview(review.id)
                      }
                    }
                  },
                  [_vm._v(_vm._s(review.verify === 0 ? "Verify" : "Unverify"))]
                ),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa fa-trash",
                  on: {
                    click: function($event) {
                      return _vm.deleteReview(review.id, index)
                    }
                  }
                })
              ])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Identification")]),
        _vm._v(" "),
        _c("th", [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", [_vm._v("Description")]),
        _vm._v(" "),
        _c("th", [_vm._v("Rating")]),
        _vm._v(" "),
        _c("th", [_vm._v("Users")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-37ee41ca", module.exports)
  }
}

/***/ }),

/***/ 533:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1308)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1310)
/* template */
var __vue_template__ = __webpack_require__(1311)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-37ee41ca"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminReviewsComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-37ee41ca", Component.options)
  } else {
    hotAPI.reload("data-v-37ee41ca", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});