webpackJsonp([25],{

/***/ 1416:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1417);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("14702e2a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ad89e2e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userTopicFComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ad89e2e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userTopicFComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1417:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.reviewAvatar[data-v-3ad89e2e] {\n    text-transform: capitalize;\n    background: #a3c2dc;\n    color: #ffffff;\n    padding: 7px 10px;\n    border-radius: 50%;\n}\n.rateStyle[data-v-3ad89e2e] {\n    float: left;\n    margin: 2px;\n}\n.review-div[data-v-3ad89e2e] {\n    width: 70%;\n    margin-left: auto;\n    margin-right: auto;\n}\n.shipTabSwitch[data-v-3ad89e2e], .reviewTabSwitch[data-v-3ad89e2e], .shareTabSwitch[data-v-3ad89e2e] {\n    background-color: #a3c2dc;\n    border: 1px solid #a3c2dc;\n    color: #ffffff !important;\n    padding: 15px 0;\n    cursor: pointer;\n}\n.shipTab[data-v-3ad89e2e], .reviewTab[data-v-3ad89e2e], .shareTab[data-v-3ad89e2e] {\n    color: #a4a4a4;\n    border: 1px solid #a4a4a4;\n    padding: 15px 0;\n    cursor: pointer;\n}\n.relatedDiv[data-v-3ad89e2e] {\n    text-align: center;\n}\n.relatedLeft[data-v-3ad89e2e] {\n    width: 40%;\n    height: 4px;\n    float: left;\n    background: #c4c4c4;\n}\n.relatedRight[data-v-3ad89e2e] {\n    width: 40%;\n    height: 4px;\n    float: right;\n    background: #c4c4c4;\n}\n.course-Detail[data-v-3ad89e2e] {\n    margin: 100px 0 0 0 !important;\n}\n.userStyle .course-thumbnail img[data-v-3ad89e2e]\n{\n    -o-object-fit: inherit !important;\n       object-fit: inherit !important;\n    height: 220px !important;\n}\n.entry-title a[data-v-3ad89e2e] {\n    font-size: 16px;\n}\n.catImage[data-v-3ad89e2e] {\n    width: 150px;\n}\n.filterTest[data-v-3ad89e2e] {\n    border-right: 1px solid #bdbfbf;\n    padding-left: 60px;\n}\n.rowCat[data-v-3ad89e2e] {\n    margin-top: 40px;\n}\n.prodDis[data-v-3ad89e2e] {\n    float: left;\n    margin: 10px 50px;\n    height: 315px;\n}\n.catName[data-v-3ad89e2e], .fa-star[data-v-3ad89e2e], .fa-star-o[data-v-3ad89e2e], .fa-star-half-o[data-v-3ad89e2e] {\n    color: #cdddec;\n}\n.pTitle[data-v-3ad89e2e] {\n    font-weight: bold;\n}\n.product-dialog[data-v-3ad89e2e] {\n    width: 90%;\n    max-width: 90%;\n    margin-top: 130px;\n}\n.prodImg[data-v-3ad89e2e] {\n    text-align: center;\n}\n.prodImg img[data-v-3ad89e2e] {\n    width: 200px;\n}\n.proReview[data-v-3ad89e2e] {\n    margin-left: 100px;\n    margin-right: 50px;\n}\n.btn-guruh-cart[data-v-3ad89e2e] {\n    background-color: #a3c2dc !important;\n    padding: 20px 42%;\n}\n.btn-guruh-online[data-v-3ad89e2e] {\n    color: #a3c2dc;\n    border: 1px solid #a3c2dc;\n    padding: 20px 42%;\n}\n.addCart[data-v-3ad89e2e] {\n    margin-bottom: 50px !important;\n}\n.btn-wishlist[data-v-3ad89e2e] {\n    background-color: #f5f5f5 !important;\n    color: #000000;\n    padding: 20px 26%;\n    margin-top: 30px;\n}\n.tab-content p[data-v-3ad89e2e], .shipDetailRow p[data-v-3ad89e2e] {\n    padding: 20px;\n}\n.abProd[data-v-3ad89e2e] {\n    background-color: #f2f6fa;\n    padding: 40px;\n    margin: 70px;\n    text-align: center;\n}\ninput[data-v-3ad89e2e]::-webkit-input-placeholder, textarea[data-v-3ad89e2e]::-webkit-input-placeholder  {\n    color: #dbdbdb !important;\n    font-weight: bold;\n}\n.detailTabs[data-v-3ad89e2e]{\n    padding: 0 80px;\n}\n.hardCover[data-v-3ad89e2e], .softCover[data-v-3ad89e2e] {\n    border: 2px solid #4c4c4c;\n    max-width: 25%;\n    padding: 20px;\n    margin-right: 50px;\n}\n.divC[data-v-3ad89e2e] {\n    font-size: 40px;\n    margin: 17px 0 50px 0;\n}\n.btn-reviews[data-v-3ad89e2e] {\n    background-color: #a3c2dc !important;\n    padding: 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryBannerComponent__ = __webpack_require__(888);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_categoryBannerComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_registerTodayComponent__ = __webpack_require__(757);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_registerTodayComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_registerTodayComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_productfilterComponent__ = __webpack_require__(893);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_productfilterComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_productfilterComponent__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-category-f-component",
    components: {
        'app-banner': __WEBPACK_IMPORTED_MODULE_0__components_categoryBannerComponent___default.a,
        'app-register': __WEBPACK_IMPORTED_MODULE_1__components_registerTodayComponent___default.a,
        'app-filter': __WEBPACK_IMPORTED_MODULE_2__components_productfilterComponent___default.a
    },
    data: function data() {
        return {
            id: this.$route.params.id,
            categories: [],
            productDetails: {},
            user: {},
            user_id: '',
            rateFa: true,
            review: {
                productId: '',
                title: '',
                description: '',
                rating: 0
            },
            star: 0,
            NotratedOne: true,
            NotratedTwo: true,
            RatedTwo: false,
            RatedOne: false,
            NotratedThree: true,
            RatedThree: false,
            NotratedFour: true,
            RatedFour: false,
            NotratedFive: true,
            RatedFive: false,
            shipTab: false,
            reviewTabSwitch: false,
            shareTabSwitch: false,
            reviewTab: true,
            shareTab: true,
            shipTabSwitch: true,
            shipping: true,
            reviews: false,
            share: false,
            allReviews: [],
            relatedProducts: [],
            anonymousCart: [],
            epiFade: true,
            epiModal: true,
            softCopy: false,
            authenticate: false,
            hardCopy: true,
            price: '',
            title: 'Hard Copy'
        };
    },
    watch: {
        '$route': function $route(to, from) {
            var _this = this;

            this.id = to.params.id;
            axios.get('api/product/topic/' + this.id).then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    _this.categories = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },
    mounted: function mounted() {
        var _this2 = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.email = user.email;
            this.user_id = user.id;
            this.user = user;
            this.authenticate = true;
        }

        axios.get('api/product/topic/' + this.id).then(function (response) {
            console.log(response);
            if (response.status === 200) {
                _this2.categories = response.data.data;
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        showTabs: function showTabs(params) {
            switch (params) {
                case 'shipping':
                    this.shipping = this.shipTabSwitch = this.reviewTab = this.shareTab = true;
                    this.reviews = this.shipTab = this.reviewTabSwitch = this.shareTabSwitch = this.shareTabSwitch = this.share = false;
                    break;
                case 'reviews':
                    this.reviewTabSwitch = this.reviews = this.shipTab = this.shareTab = true;
                    this.shipTabSwitch = this.reviewTab = this.shipping = this.shareTabSwitch = this.share = false;
                    break;
                case 'share':
                    this.shipping = this.reviews = this.reviewTabSwitch = this.shipTab = this.shareTab = this.shipTabSwitch = false;
                    this.share = this.shareTabSwitch = this.reviewTab = this.shipTab = true;
                    break;
                default:
                    this.shipping = false;
                    this.reviews = false;
            }
        },
        openModal: function openModal(id) {
            var _this3 = this;

            this.epiFade = false;
            axios.get('api/product-detail/' + id).then(function (response) {
                if (response.status === 200) {
                    var emptyObject = {};
                    switch (response.data.data[0].prodType) {
                        case 'Market Reports':
                            emptyObject = response.data.data[0].marketReport;
                            break;
                        case 'Books':
                            emptyObject = response.data.data[0].books;
                            break;
                        case 'White Paper':
                            emptyObject = response.data.data[0].whitePaper;
                            break;
                        case 'Journals':
                            emptyObject = response.data.data[0].journal;
                            break;
                        case 'Market Research':
                            emptyObject = response.data.data[0].marketResearch;
                            break;
                        case 'Webinar':
                            emptyObject = response.data.data[0].webinar;
                            break;
                        case 'Podcast':
                            emptyObject = response.data.data[0].webinar;
                            break;
                        case 'Videos':
                            emptyObject = response.data.data[0].webinar;
                            break;
                        case 'Courses':
                            emptyObject = response.data.data[0].courses;
                            break;
                        default:
                            return false;

                    }

                    _this3.prodd = response.data.data[0];
                    _this3.productDetails = _extends({}, emptyObject, _this3.prodd);
                    // this.productDetails = response.data.data[0];
                    _this3.productDetails.hardCopyPrice !== null ? _this3.hardCopy = true : _this3.hardCopy = false;
                    _this3.productDetails.softCopyPrice !== null ? _this3.softCopy = true : _this3.softCopy = false;
                    if (_this3.productDetails.hardCopyPrice !== null) {
                        _this3.price = _this3.productDetails.hardCopyPrice;
                    } else if (_this3.productDetails.softCopyPrice !== null) {
                        _this3.price = _this3.productDetails.softCopyPrice;
                    } else if (_this3.productDetails.audioPrice !== null) {
                        _this3.price = _this3.productDetails.audioPrice;
                    } else if (_this3.productDetails.videoPrice !== null) {
                        _this3.price = _this3.productDetails.videoPrice;
                    }
                    _this3.getRelatedProduct(_this3.productDetails.sub_category_id);
                    _this3.getProductReviews(_this3.productDetails.id);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        togglePrice: function togglePrice(cat) {
            if (cat === 'hardCopy') {
                this.price = this.productDetails.hardCopyPrice;
                this.title = 'Hard Copy';
            } else if (cat === 'softCopy') {
                this.price = this.productDetails.softCopyPrice;
                this.title = 'Soft Copy';
            } else if (cat === 'audioCopy') {
                this.price = this.productDetails.audioPrice;
                this.title = 'Audio Copy';
            } else if (cat === 'videoCopy') {
                this.price = this.productDetails.videoPrice;
                this.title = 'Video Copy';
            }
        },
        getProductReviews: function getProductReviews(id) {
            var _this4 = this;

            axios.get('api/product/get-reviews/' + id).then(function (response) {
                if (response.status === 200) {
                    console.log(response);
                    _this4.allReviews = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        addToWishlist: function addToWishlist() {
            var _this5 = this;

            if (this.authenticate === false) {
                this.$router.push('/auth');
            } else {
                var wishlist = {
                    productId: this.productDetails.id,
                    vendorId: this.productDetails.vendor_user_id,
                    userId: this.user_id
                };

                axios.post('/api/wishlist', JSON.parse(JSON.stringify(wishlist)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this5.$toasted.success('Successfully added to wishlist');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        addToCart: function addToCart(id) {
            var cart = {
                productId: id,
                price: this.price
            };
            if (this.title === 'Hard Copy') {
                cart.quantity = 1;
            } else if (this.title === 'Soft Copy') {
                cart.quantity = 0;
            }
            if (this.authenticate === false) {
                this.anonymousCart.push(cart);
                if (JSON.parse(localStorage.getItem('userCart')) === null) {
                    localStorage.setItem('userCart', JSON.stringify(this.anonymousCart));
                    var sessionCart = JSON.parse(localStorage.getItem('userCart'));
                    var cartCount = sessionCart.length;
                    this.$emit('getCartCount', cartCount);
                } else if (JSON.parse(localStorage.getItem('userCart')) !== null) {
                    var _sessionCart = JSON.parse(localStorage.getItem('userCart'));
                    var ss = _sessionCart.length;
                    _sessionCart[ss] = cart;
                    localStorage.setItem('userCart', JSON.stringify(_sessionCart));
                    var a = JSON.parse(localStorage.getItem('userCart'));
                    var aCount = a.length;
                    this.$emit('getCartCount', aCount);
                }
            } else {

                cart.id = this.productDetails.vendor_user_id;
                this.addCart(cart);
            }
        },
        addCart: function addCart(cart) {
            axios.post('/api/cart', JSON.parse(JSON.stringify(cart)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 200) {

                    window.location.reload();
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        paystack: function paystack() {
            var price = this.productDetails.hardCopyPrice * 1000;
            var handler = PaystackPop.setup({
                key: 'pk_test_6e9d2124cf38dc63844315e49b1d982d1aeadbf1',
                email: 'customer@email.com',
                amount: price,
                ref: '' + Math.floor(Math.random() * 1000000000 + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                firstname: 'Stephen',
                lastname: 'King',
                // label: "Optional string that replaces customer email"
                metadata: {
                    custom_fields: [{
                        display_name: "test",
                        variable_name: "john doe",
                        value: "+2348012345678"
                    }]
                },
                callback: function callback(response) {
                    alert('success. transaction ref is ' + response.reference);
                },
                onClose: function onClose() {
                    // alert('window closed');
                }
            });
            handler.openIframe();
        },
        getRelatedProduct: function getRelatedProduct(subCatId) {
            var _this6 = this;

            axios.get('api/get-related-product/' + subCatId).then(function (response) {
                if (response.status === 200) {
                    _this6.relatedProducts = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        rateWithStar: function rateWithStar(num) {
            switch (num) {
                case '1':
                    this.NotratedOne = !this.NotratedOne;
                    this.RatedOne = !this.RatedOne;
                    this.review.rating = 1;
                    break;
                case '2':
                    this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
                    this.RatedTwo = this.RatedOne = !this.RatedTwo;
                    this.review.rating = 2;
                    break;
                case '3':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
                    this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
                    this.review.rating = 3;
                    break;
                case '4':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
                    this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
                    this.review.rating = 4;
                    break;
                case '5':
                    this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
                    this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
                    this.review.rating = 5;
                    break;
                default:
                    this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
                    this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
            }
        },
        submitReview: function submitReview() {
            var _this7 = this;

            if (this.authenticate === false) {
                this.$toasted.error('You must log in to review a product');
            } else {
                this.review.productId = this.productDetails.id;
                axios.post('/api/user/reviews', JSON.parse(JSON.stringify(this.review)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this7.$toasted.success('Reviews successfully saved');
                        _this7.review.productId = _this7.review.title = _this7.review.description = '';
                        _this7.review.rating = 0;
                        _this7.NotratedOne = _this7.NotratedTwo = _this7.NotratedThree = _this7.NotratedFour = _this7.NotratedFive = true;
                        _this7.RatedOne = _this7.RatedTwo = _this7.RatedThree = _this7.RatedFour = _this7.RatedFive = false;
                    } else {
                        _this7.$toasted.error('You cannot review this product until you purchase it');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }
});

/***/ }),

/***/ 1419:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "course-Detail" },
    [
      _c("app-banner"),
      _vm._v(" "),
      _c(
        "div",
        {
          class: { modal: _vm.epiModal, fade: _vm.epiFade },
          attrs: {
            id: "exampleModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalLabel",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog product-dialog",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-6 prodImg" }, [
                      _c("img", {
                        attrs: { src: _vm.productDetails.coverImage }
                      }),
                      _vm._v(" "),
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-wishlist",
                            on: {
                              click: function($event) {
                                return _vm.addToWishlist()
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fa fa-heart-o" }),
                            _vm._v("Add to Wishlist")
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6" }, [
                      _c("div", [
                        _c("h1", [_vm._v(_vm._s(_vm.productDetails.title))]),
                        _vm._v(" "),
                        _vm.productDetails.vendor !== undefined
                          ? _c("div", [
                              _c("span", [
                                _vm._v(_vm._s(_vm.productDetails.author) + " ")
                              ]),
                              _c("span", [
                                _vm._v(
                                  "/ " +
                                    _vm._s(_vm.productDetails.vendor.storeName)
                                )
                              ])
                            ])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c("div", {}, [
                        _c("p", [_vm._v(_vm._s(_vm.title))]),
                        _vm._v(" "),
                        _c("div", { staticClass: "divC" }, [
                          _vm._v("₦" + _vm._s(_vm.price) + ".00")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _vm.productDetails.hardCopyPrice !== null
                          ? _c(
                              "div",
                              {
                                staticClass: "hardCover col-md-6",
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("hardCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.productDetails.hardCopyPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Hard Copy")])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.productDetails.softCopyPrice !== null
                          ? _c(
                              "div",
                              {
                                staticClass: "softCover col-md-6",
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("softCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.productDetails.softCopyPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Soft Copy")])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.productDetails.audioPrice !== null
                          ? _c(
                              "div",
                              {
                                staticClass: "softCover col-md-6",
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("audioCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.productDetails.audioPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Audio Copy")])
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.productDetails.videoPrice !== null
                          ? _c(
                              "div",
                              {
                                staticClass: "softCover col-md-6",
                                on: {
                                  click: function($event) {
                                    return _vm.togglePrice("videoCopy")
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                    ₦" +
                                    _vm._s(_vm.productDetails.videoPrice) +
                                    ".00\n                                    "
                                ),
                                _c("p", [_vm._v("Video Copy")])
                              ]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-md-12 addCart" }, [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-guruh-cart",
                              on: {
                                click: function($event) {
                                  return _vm.addToCart(_vm.productDetails.id)
                                }
                              }
                            },
                            [_vm._v("Add Cart")]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-12" }, [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-guruh-online",
                              on: {
                                click: function($event) {
                                  return _vm.paystack()
                                }
                              }
                            },
                            [_vm._v("Read Online")]
                          )
                        ])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "abProd" }, [
                    _c("h1", [_vm._v("Overview")]),
                    _vm._v(" "),
                    _c("div", [_vm._v(_vm._s(_vm.productDetails.description))])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "detailTabs" }, [
                    _vm._m(2),
                    _vm._v(" "),
                    _c("div", { staticClass: "tab-content" }, [
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane active",
                          attrs: { id: "home" }
                        },
                        [
                          _vm.productDetails.fileType !== null
                            ? _c("p", [
                                _vm._v(
                                  "ISBN: " + _vm._s(_vm.productDetails.fileType)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.productDetails.publisher !== null
                            ? _c("p", [
                                _vm._v(
                                  "Publisher: " +
                                    _vm._s(_vm.productDetails.publisher)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.productDetails.publicationDate !== null
                            ? _c("p", [
                                _vm._v(
                                  "Publication Date: " +
                                    _vm._s(_vm.productDetails.publicationDate)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.productDetails.pageNo !== null
                            ? _c("p", [
                                _vm._v(
                                  "Page No: " +
                                    _vm._s(_vm.productDetails.pageNo)
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.productDetails.series !== null
                            ? _c("p", [
                                _vm._v(
                                  "Series: " + _vm._s(_vm.productDetails.series)
                                )
                              ])
                            : _vm._e()
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu1" }
                        },
                        [
                          _c("p", [
                            _vm._v(_vm._s(_vm.productDetails.aboutAuthor))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu2" }
                        },
                        [
                          _c("p", [
                            _vm._v(_vm._s(_vm.productDetails.tableOfContent))
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "tab-pane fade",
                          attrs: { id: "menu3" }
                        },
                        [_c("p", [_vm._v(_vm._s(_vm.productDetails.excerpts))])]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          shipTab: _vm.shipTab,
                          shipTabSwitch: _vm.shipTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("shipping")
                          }
                        }
                      },
                      [_vm._v("Shipping and Delivery")]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          reviewTab: _vm.reviewTab,
                          reviewTabSwitch: _vm.reviewTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("reviews")
                          }
                        }
                      },
                      [_vm._v("Write a Reviews")]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "col-md-4 text-center",
                        class: {
                          shareTab: _vm.shareTab,
                          shareTabSwitch: _vm.shareTabSwitch
                        },
                        on: {
                          click: function($event) {
                            return _vm.showTabs("share")
                          }
                        }
                      },
                      [_vm._v("Share this Items")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _vm.shipping
                      ? _c("div", { staticClass: "col-md-12 shipDetailRow" }, [
                          _c("p", [
                            _vm._v(
                              "Location: " + _vm._s(_vm.productDetails.location)
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Local Shipping Rate: " +
                                _vm._s(_vm.productDetails.localShippingRates)
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Local Shipping Time: " +
                                _vm._s(_vm.productDetails.localShippingTime)
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Int Shipping Rate: " +
                                _vm._s(_vm.productDetails.intShippingRates)
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "Int Shipping Time: " +
                                _vm._s(_vm.productDetails.intShippingTime)
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.reviews
                      ? _c("div", { staticClass: "col-md-12 " }, [
                          _c("div", { staticClass: "review-div" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.review.title,
                                    expression: "review.title"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder: "Enter Title"
                                },
                                domProps: { value: _vm.review.title },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.review,
                                      "title",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group" }, [
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.review.description,
                                    expression: "review.description"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  rows: "5",
                                  placeholder: "Enter Reviews"
                                },
                                domProps: { value: _vm.review.description },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.review,
                                      "description",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", [
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedOne,
                                  "fa-star": _vm.RatedOne
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("1")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedTwo,
                                  "fa-star": _vm.RatedTwo
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("2")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedThree,
                                  "fa-star": _vm.RatedThree
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("3")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedFour,
                                  "fa-star": _vm.RatedFour
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("4")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", {
                                class: {
                                  fa: _vm.rateFa,
                                  "fa-star-o": _vm.NotratedFive,
                                  "fa-star": _vm.RatedFive
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.rateWithStar("5")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("i", { staticClass: "ml-lg-5" }, [
                                _vm._v(
                                  _vm._s(_vm.review.rating) +
                                    " " +
                                    _vm._s(
                                      _vm.review.rating > 1 ? "stars" : "star"
                                    )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-reviews mb-4",
                                on: {
                                  click: function($event) {
                                    return _vm.submitReview()
                                  }
                                }
                              },
                              [_vm._v("Submit")]
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.share
                      ? _c("div", { staticClass: "col-md-12" }, [
                          _c("p", [_vm._v("Share across social media")])
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _vm.allReviews.length > 0
                    ? _c("div", { staticClass: "row" }, [
                        _c("h1", [_vm._v("Reviews")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-12" }, [
                          _c(
                            "div",
                            [
                              _vm._l(_vm.allReviews, function(Review, index) {
                                return _c(
                                  "div",
                                  { staticClass: "mb-5" },
                                  [
                                    _c("div", { staticClass: "avatarD" }, [
                                      _c(
                                        "span",
                                        { staticClass: "reviewAvatar" },
                                        [
                                          _vm._v(
                                            _vm._s(Review.user.name.charAt(0))
                                          )
                                        ]
                                      ),
                                      _c(
                                        "span",
                                        { staticClass: "avatartitle" },
                                        [_vm._v(_vm._s(Review.title))]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "avatarDescription" },
                                      [_vm._v(_vm._s(Review.description))]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(Review.rating, function(star) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [_c("i", { staticClass: "fa fa-star" })]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _vm._l(5 - Review.rating, function(star) {
                                      return _c(
                                        "div",
                                        { staticClass: "rateStyle" },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-star-o"
                                          })
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              _vm._v(" "),
                              _c("hr")
                            ],
                            2
                          )
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._m(3),
                  _vm._v(" "),
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-md-12" },
                      _vm._l(_vm.relatedProducts, function(product, index) {
                        return _c("div", { staticClass: "prodDis" }, [
                          _c(
                            "a",
                            {
                              attrs: {
                                href: "#",
                                "data-toggle": "modal",
                                "data-target": "#exampleModal"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.openModal(product.id)
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "catImage",
                                attrs: { src: product.coverImage }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "pTitle" }, [
                            _vm._v(_vm._s(product.title))
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "catName" }, [
                            _vm._v(_vm._s(product.vendor.storeName))
                          ]),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" }),
                          _vm._v(" "),
                          _c("i", { staticClass: "fa fa-star" })
                        ])
                      }),
                      0
                    )
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row rowCat" }, [
        _c(
          "div",
          { staticClass: "col-md-3 filterTest" },
          [
            _c("app-filter", {
              on: {
                sendData: function($event) {
                  _vm.categories = $event
                }
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-md-9" },
          _vm._l(_vm.categories, function(category, index) {
            return _c("div", { staticClass: "prodDis" }, [
              _c(
                "a",
                {
                  attrs: {
                    href: "#",
                    "data-toggle": "modal",
                    "data-target": "#exampleModal"
                  },
                  on: {
                    click: function($event) {
                      return _vm.openModal(category.id)
                    }
                  }
                },
                [
                  _c("img", {
                    staticClass: "catImage",
                    attrs: { src: category.coverImage }
                  })
                ]
              ),
              _vm._v(" "),
              _c("p", { staticClass: "pTitle" }, [
                _vm._v(_vm._s(category.title))
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "catName" }, [
                _vm._v(_vm._s(category.vendor.storeName))
              ]),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" })
            ])
          }),
          0
        )
      ]),
      _vm._v(" "),
      _c("app-register")
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("span", { staticClass: "proReview" }, [_vm._v("4 Customer Reviews")]),
      _c("span", { staticClass: "proEdi" }, [_vm._v("Editorial Review")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "nav nav-tabs" }, [
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#home" } }, [
          _vm._v("PRODUCT DETAILS")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu1" } }, [
          _vm._v("ABOUT THE AUTHOR")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu2" } }, [
          _vm._v("TABLE OF CONTENT")
        ])
      ]),
      _vm._v(" "),
      _c("li", [
        _c("a", { attrs: { "data-toggle": "tab", href: "#menu3" } }, [
          _vm._v("READ AN EXCERPT")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "relatedDiv" }, [
      _c("div", { staticClass: "relatedLeft" }),
      _vm._v("RELATED ITEMS"),
      _c("div", { staticClass: "relatedRight" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3ad89e2e", module.exports)
  }
}

/***/ }),

/***/ 557:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1416)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1418)
/* template */
var __vue_template__ = __webpack_require__(1419)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3ad89e2e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userTopicFComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ad89e2e", Component.options)
  } else {
    hotAPI.reload("data-v-3ad89e2e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 757:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(758)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(760)
/* template */
var __vue_template__ = __webpack_require__(761)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4a583674"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/registerTodayComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4a583674", Component.options)
  } else {
    hotAPI.reload("data-v-4a583674", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 758:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(759);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("81795432", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4a583674\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./registerTodayComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4a583674\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./registerTodayComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 759:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.theme--dark.v-btn[data-v-4a583674]:not(.v-btn--icon):not(.v-btn--flat) {\n  background-color: #a4c2db !important;\n}\n.m[data-v-4a583674]{\n   background:rgba(255, 255, 255, 0.6)\n}\na[data-v-4a583674] {\n  color: #ffffff !important;\n}\n.regPolariod[data-v-4a583674] {\n  /* background-color: white; */\n  width: 85%;\n  padding-top:60px ;\n  padding-bottom: 45px;\n  height: auto;\n  margin: 0 auto;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n.text-biz[data-v-4a583674] {\n  color:#373a3c;\n}\n.text-muted[data-v-4a583674] {\n  color:#373a3c !important;\n}\n.reg[data-v-4a583674] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 0 auto;\n  width: 100%;\n  padding: 0px 20px;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.regButt[data-v-4a583674] {\n  display: grid;\n  text-align: center;\n}\n.fa-angle-double-down[data-v-4a583674] {\n  font-size: 6rem;\n  color: rgba(163, 194, 220, 0.5);\n}\n.first[data-v-4a583674]{\n  width:50%;\n  text-align: center;\n}\n.Img[data-v-4a583674]{\n  width: 50%;\n  -webkit-transition: all .4s;\n  transition: all .4s;\n  overflow: hidden;\n}\n.Img img[data-v-4a583674]{\n  padding: 30px 0;\n}\n.Img:hover img[data-v-4a583674] {\n  -webkit-transform: scale(1.01);\n          transform: scale(1.01);\n}\n.getStarted[data-v-4a583674] {\n  color: rgba(0, 0, 0, 0.54);\n  font-weight: bold;\n  font-size: calc(\n    16px + (36 - 16) * ((100vw - 300px) / (1600 - 300))\n  ) !important;\n  line-height: calc(\n    1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300))\n  ) !important;\n  text-align: center;\n}\n.platform[data-v-4a583674] {\n  font-size: 18px;\n}\nbutton.primary[data-v-4a583674] {\n  font-size: 14px !important;\n  text-transform: capitalize;\n  min-height: 35px !important;\n  padding: 8px 30px !important;\n  color: white;\n  background-color: #a4c2db;\n  border: 1px solid #a4c2db;\n  margin: 0 auto;\n  font-weight: bold;\n}\n.button.primary[data-v-4a583674]:hover {\n  background: rgba(163, 194, 220, 0.5);\n}\n.underline[data-v-4a583674]{\n  /* background-color: white;\n  border-top:1px solid #ccc;\n  padding: 40px; */\n  width:90%;\n  margin:0 auto\n}\nbutton.btn-register[data-v-4a583674] {\n  background-color: #a4c2db;\n  font-size: 14px;\n  /* max-height: 30px; */\n  color: #373a3c !important;\n}\nbutton.btn-register[data-v-4a583674] {\n  padding: 1px 30px;\n}\nbutton.btn-register a[data-v-4a583674] {\n  color: #373a3c !important;\n  font-weight: bold;\n}\nbutton.btn-bizguruh[data-v-4a583674]:hover {\n  background-color: #373a3c;\n  font-size: 14px;\n  max-height: 30px;\n  color: #ffffff !important;\n}\nbutton.btn-bizguruh a[data-v-4a583674]:hover {\n  color: #ffffff !important;\n}\n.fa-2x[data-v-4a583674] {\n    font-size: 1.5em;\n}\n.connect[data-v-4a583674]{\n        font-size: 28px;\n    line-height: 40px;\n    margin-bottom: 10px;\n}\n@media (max-width: 768px) {\n.regPolariod[data-v-4a583674]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    padding:100px 10px;\n    margin-top: 20px;\n    width:80%;\n}\n.Img[data-v-4a583674]{\n    width:80%;\n    margin-bottom: 20px;\n}\n.Img img[data-v-4a583674]{\n  padding:  0;\n}\n.first[data-v-4a583674]{\n    width:100%;\n}\n.underline[data-v-4a583674]{\n    padding:5px;\n    width:100%;\n    margin-bottom:20px;\n}\n.connect[data-v-4a583674]{\n    font-size: 18px;\n}\n.reg[data-v-4a583674] {\n    width: 100%;\n}\n.getStarted[data-v-4a583674] {\n    width: 100%;\n}\n.regButt[data-v-4a583674] {\n    width: 100%;\n}\n}\n@media (max-width: 575px) {\n.platform[data-v-4a583674] {\n    font-size: 16px;\n}\nbutton.primary[data-v-4a583674] {\n    margin: auto;\n    font-size: 12px !important;\n    text-transform: capitalize !important;\n    height: 30px !important;\n    padding: 0 15px !important;\n}\n.getStarted[data-v-4a583674] {\n    padding: 10px;\n    width: 100%;\n    font-size: 18px !important;\n}\n.regButt[data-v-4a583674] {\n    width: 100%;\n}\n.fa-angle-double-down[data-v-4a583674] {\n    font-size: 3rem;\n    margin-bottom: 15px;\n}\n.regPolariod[data-v-4a583674] {\n    height: auto;\n}\nbutton.btn-register[data-v-4a583674] {\n    background-color: #ffffff;\n    font-size: 9px;\n    max-height: 20px;\n    color: #373a3c !important;\n}\nbutton.btn-register[data-v-4a583674] {\n    padding: 1px 15px;\n}\n.mr-4[data-v-4a583674] {\n    margin-right: 0 !important;\n}\n.reg[data-v-4a583674] {\n    width: 100%;\n    padding: 0;\n}\n.fa-2x[data-v-4a583674] {\n    font-size: 1em;\n}\n.regPolariod[data-v-4a583674]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    padding:100px 0px;\n    margin-top: 20px;\n    width:90%;\n}\n}\n@media (max-width: 375px) {\n  /* .why-biz, .bgShadow{\n        height:1250px;\n    } */\n.reg[data-v-4a583674] {\n    width: 100%;\n}\nbutton.primary[data-v-4a583674] {\n    font-size: 12px !important;\n    height: 25px !important;\n    font-size: 12px !important;\n    height: 30px !important;\n    padding: 0 10px !important;\n}\n}\n@media (max-width: 320px) {\n.getStarted[data-v-4a583674] {\n    padding: 0;\n}\nbutton.primary[data-v-4a583674] {\n    font-size: 11px !important;\n    height: 25px !important;\n    padding: 0 10px !important;\n}\n.reg[data-v-4a583674] {\n    padding: 0;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "register-today-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
  },

  watch: {
    scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 4.4) {
        this.swing = true;
      }
    },
    signup: function signup() {
      window.scrollTo(0, window.innerHeight * .7);
    }
  }
});

/***/ }),

/***/ 761:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "m" }, [
    _c("div", { staticClass: "regPolariod" }, [
      _c("div", { staticClass: "first" }, [
        _c("h3", { staticClass: "getStarted animated fadeInLeft mb-4 " }, [
          _vm._v("So what are you waiting for ?")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "regButt" }, [
          _c("i", { staticClass: "fas fa-angle-double-down" }),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass:
                "elevated_btn btn-compliment text-white animated mx-auto",
              on: { click: _vm.signup }
            },
            [_vm._v("Sign up today")]
          )
        ])
      ]),
      _vm._v(" "),
      _vm.swing
        ? _c("div", { staticClass: "Img animated zoomIn" }, [
            _c("img", { attrs: { src: "/images/mockups/sign.png", alt: "" } })
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4a583674", module.exports)
  }
}

/***/ }),

/***/ 888:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(889)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(891)
/* template */
var __vue_template__ = __webpack_require__(892)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-37e91bdc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-37e91bdc", Component.options)
  } else {
    hotAPI.reload("data-v-37e91bdc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 889:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(890);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2d97ec2f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37e91bdc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-37e91bdc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 890:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.storBanner[data-v-37e91bdc]{\n    position: relative;\n}\nimg[data-v-37e91bdc]{\n    margin-top: 40px;\n    width: 100%;\n    max-height: 300px;\n}\n.text[data-v-37e91bdc]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size:60px;\n    color: #000000;\n}\n@media(max-width: 480px){\n.text[data-v-37e91bdc]{\n            font-size:30px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-banner-component"
});

/***/ }),

/***/ 892:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "storBanner" }, [
      _c("img", { attrs: { src: "/images/store.jpg" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Books")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-37e91bdc", module.exports)
  }
}

/***/ }),

/***/ 893:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(894)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(896)
/* template */
var __vue_template__ = __webpack_require__(897)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-bcfe8ec6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/productfilterComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bcfe8ec6", Component.options)
  } else {
    hotAPI.reload("data-v-bcfe8ec6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 894:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(895);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2af5fbf7", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bcfe8ec6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./productfilterComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bcfe8ec6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./productfilterComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 895:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.filterList[data-v-bcfe8ec6] {\n     color: #a7a7a7 !important;\n     height: 400px;\n     overflow: scroll;\n}\n.filterList li[data-v-bcfe8ec6] {\n     line-height: 50px;\n     cursor: pointer;\n}\n.searchBarContent[data-v-bcfe8ec6] {\n     display: -webkit-box;\n     display: -ms-flexbox;\n     display: flex;\n     position: relative;\n}\n.searchBar[data-v-bcfe8ec6] {\n     width: auto;\n     margin-left: auto;\n     margin-right: auto;\n}\ninput[data-v-bcfe8ec6]::-webkit-input-placeholder {\n     color: #c5c5c5 !important;\n}\n.search-btn[data-v-bcfe8ec6] {\n     border: 0px;\n     color: #000000;\n     margin: 7px 0;\n     position: absolute;\n     right: 4px;\n     top: 4px;\n}\n.filterList li[data-v-bcfe8ec6]:hover{\n     color:#adc9e0;\n}\n\n/* .checkboxTopic, .checkboxName {\n     float: left;\n }*/\n.checkboxTopic[data-v-bcfe8ec6]{\n     margin-top: 18px;\n     margin-right: 10px;\n}\n.filterleft[data-v-bcfe8ec6] {\n     display: -webkit-box;\n     display: -ms-flexbox;\n     display: flex;\n    /* overflow: hidden;*/\n}\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 896:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "productfilter-component",
    data: function data() {
        return {
            categoryFilter: [],
            topicForProduct: [],
            searchItem: '',
            filterTopic: []
        };
    },
    mounted: function mounted() {
        this.getAllBizguruhTopic();
    },

    methods: {
        searchDataItem: function searchDataItem() {
            this.$emit('sendSearchData', this.searchItem);
        },
        filterBasedOnTopic: function filterBasedOnTopic() {
            this.$emit('sendTopicData', this.filterTopic);
        },
        getAllBizguruhTopic: function getAllBizguruhTopic() {
            var _this = this;

            axios.get('/api/bizguruh-topics').then(function (response) {
                if (response.status === 200) {
                    _this.topicForProduct = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getFilterProduct: function getFilterProduct(type) {
            var _this2 = this;

            axios.get('/api/products').then(function (response) {
                if (response.status === 200) {
                    var emptyArray = [];
                    response.data.data.forEach(function (item) {
                        if (item.hardCopy) {
                            emptyArray.push(item);
                        } else {
                            console.log('no');
                        }
                    });
                    _this2.categoryFilter = emptyArray;
                    _this2.$emit('sendData', _this2.categoryFilter);
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAllProduct: function getAllProduct() {
            var _this3 = this;

            axios.get('/api/products').then(function (response) {
                if (response.status === 200) {
                    _this3.categoryFilter = response.data.data;
                    _this3.$emit('sendData', _this3.categoryFilter);
                }
            });
        },
        getAudioProduct: function getAudioProduct(type) {
            var _this4 = this;

            axios.get('/api/products').then(function (response) {
                if (response.status === 200) {
                    var emptyArray = [];
                    response.data.data.forEach(function (item) {
                        if (item.fileType === type) {
                            emptyArray.push(item);
                        } else {
                            console.log('no');
                        }
                    });
                    _this4.categoryFilter = emptyArray;
                    _this4.$emit('sendData', _this4.categoryFilter);
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },
    computed: {}
});

/***/ }),

/***/ 897:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "searchBar" }, [
      _c("div", { staticClass: "searchBarContent mb-2" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.searchItem,
              expression: "searchItem"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "text", placeholder: "Search title" },
          domProps: { value: _vm.searchItem },
          on: {
            keyup: function($event) {
              return _vm.searchDataItem()
            },
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.searchItem = $event.target.value
            }
          }
        }),
        _vm._v(" "),
        _vm._m(0)
      ])
    ]),
    _vm._v(" "),
    _c(
      "ul",
      { staticClass: "filterList" },
      _vm._l(_vm.topicForProduct, function(topicForProducts, index) {
        return _vm.topicForProduct.length > 0
          ? _c("li", [
              _c("div", { staticClass: "filterleft" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.filterTopic,
                      expression: "filterTopic"
                    }
                  ],
                  staticClass: "checkboxTopic",
                  attrs: { type: "checkbox" },
                  domProps: {
                    value: topicForProducts.id,
                    checked: Array.isArray(_vm.filterTopic)
                      ? _vm._i(_vm.filterTopic, topicForProducts.id) > -1
                      : _vm.filterTopic
                  },
                  on: {
                    change: [
                      function($event) {
                        var $$a = _vm.filterTopic,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = topicForProducts.id,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.filterTopic = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.filterTopic = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.filterTopic = $$c
                        }
                      },
                      _vm.filterBasedOnTopic
                    ]
                  }
                }),
                _c("div", { staticClass: "checkboxName" }, [
                  _vm._v(_vm._s(topicForProducts.name))
                ])
              ])
            ])
          : _vm._e()
      }),
      0
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-btn mt-0 ml-0" }, [
      _c("i", { staticClass: "fa fa-search" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-bcfe8ec6", module.exports)
  }
}

/***/ })

});