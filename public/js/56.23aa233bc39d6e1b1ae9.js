webpackJsonp([56],{

/***/ 1486:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1487);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("36d2e4fc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6fc7766d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPaperDetailComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6fc7766d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPaperDetailComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1487:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.course-Detail[data-v-6fc7766d] {\n  width: 100%;\n  margin: 0 auto !important;\n}\nul[data-v-6fc7766d],\nol[data-v-6fc7766d],\nli[data-v-6fc7766d] {\n  list-style: none !important;\n}\n.upgradeContainer[data-v-6fc7766d] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  top: 0;\n}\n.upgrade[data-v-6fc7766d] {\n  position: absolute;\n  width: 50%;\n  height: auto;\n  background: #ffffff;\n  top: 30%;\n  right: 25%;\n  padding: 20px 40px;\n  border-radius: 3px;\n}\n.upgradeButtons[data-v-6fc7766d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  padding: 10px 0;\n  font-size: 18px;\n}\n.upgradeButton[data-v-6fc7766d] {\n  cursor: pointer;\n  font-size: 16px;\n  font-weight: bold;\n  border: 1px solid;\n  border-radius: 2px;\n  padding: 2px 10px;\n  margin: 10px;\n}\n.upgradeButton[data-v-6fc7766d]:hover {\n  color: #a4c2db;\n  font-weight: bold;\n}\np[data-v-6fc7766d] {\n  margin-bottom: 10px;\n}\n.expert[data-v-6fc7766d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n.expertImg[data-v-6fc7766d] {\n  max-width: 80px;\n  max-height: 80px;\n  width: 80px;\n  height: 80px;\n  border-radius: 50%;\n  overflow: hidden;\n  margin-right: 10px;\n}\n.expertImg img[data-v-6fc7766d] {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n.aboutExpert[data-v-6fc7766d] {\n  margin-right: 20px;\n  width: 30%;\n}\n.vn[data-v-6fc7766d] {\n  text-transform: capitalize;\n}\n.followExpert[data-v-6fc7766d] {\n  border: 1px solid #5b84a7;\n  padding: 1px 10px;\n  color: #5b84a7;\n  margin-right: 10px;\n  height: 25px;\n  border-radius: 5px;\n  font-size: 12px;\n}\n.followingExpert[data-v-6fc7766d] {\n  background: #5b84a7;\n  color: #fff;\n  padding: 1px 10px;\n  margin-right: 10px;\n  height: 25px;\n  border-radius: 5px;\n  font-size: 12px;\n}\n.center[data-v-6fc7766d] {\n  text-align: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -ms-flex-line-pack: center;\n      align-content: center;\n}\n.imagin[data-v-6fc7766d] {\n  width: 100%;\n  height: 100%;\n}\n.imagin img[data-v-6fc7766d] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.text[data-v-6fc7766d] {\n  position: absolute;\n  top: 10px;\n  left: 10px;\n  font-size: 40px;\n  color: #000;\n}\nbutton.btn.btn-block[data-v-6fc7766d] {\n  background-color: #5b84a7 !important;\n  font-size: 11px;\n}\n.btn-block[data-v-6fc7766d] {\n  background-color: #a4c2db !important;\n  padding: 7px 16px;\n  text-transform: capitalize;\n  border: none;\n  -webkit-box-shadow: 0 4px #eee;\n          box-shadow: 0 4px #eee;\n  color: #fff !important;\n}\n.btn-block[data-v-6fc7766d]:hover {\n  background-color: rgba(164, 194, 219, 0.64) !important ;\n}\n.btn-block[data-v-6fc7766d]:active {\n  background-color: #a4c2db;\n  -webkit-box-shadow: 0 2px #eee;\n          box-shadow: 0 2px #eee;\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n@media (max-width: 425px) {\n.upgrade[data-v-6fc7766d] {\n    width: 95%;\n    right: 2%;\n}\n.market[data-v-6fc7766d] {\n    display: none;\n}\n}\n.vieww[data-v-6fc7766d] {\n  margin-bottom: 10px;\n}\n.follow[data-v-6fc7766d] {\n  border: 1px solid #5b84a7;\n  color: #5b84a7;\n  padding: 1px 9px;\n  border-radius: 5px;\n  cursor: pointer;\n}\n.imgD[data-v-6fc7766d] {\n  width: 80px;\n}\n.imgD img[data-v-6fc7766d] {\n  height: 100%;\n}\n.vendorDisp[data-v-6fc7766d] {\n  background: #5b84a7;\n  color: #ffffff;\n  font-size: 30px;\n  padding: 12px 18px;\n  border-radius: 50%;\n  margin-right: 20px;\n}\n.purchase[data-v-6fc7766d] {\n  text-align: center;\n}\n.writtenAuthor[data-v-6fc7766d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.hardCopy[data-v-6fc7766d],\n.softCopy[data-v-6fc7766d],\n.readOnline[data-v-6fc7766d] {\n  background: #5b84a7;\n  color: #ffffff !important;\n  font-weight: bold;\n  padding: 10px;\n  margin: 10px;\n  cursor: pointer;\n}\n.conti[data-v-6fc7766d] {\n  text-transform: none;\n}\n.tabC[data-v-6fc7766d] {\n  margin: 20px 0;\n  border-bottom: 0.5px solid #ecebeb;\n  padding-bottom: 4px;\n  padding-top: 4px;\n}\n.tabT[data-v-6fc7766d] {\n  font-size: 20px;\n  line-height: 16px;\n  color: #333333;\n  font-weight: bold;\n}\nol[data-v-6fc7766d],\nul[data-v-6fc7766d] {\n  list-style: none;\n}\nsmall[data-v-6fc7766d] {\n  font-size: 12px;\n}\n.paperAttr ul li[data-v-6fc7766d] {\n  font-size: 24px;\n  position: relative;\n}\n.paperAttr ul li a[data-v-6fc7766d] {\n  color: #333333;\n}\n.btn-cart[data-v-6fc7766d] {\n  border: 1px solid #5b84a7;\n  color: #5b84a7;\n  padding: 10px 120px;\n  font-weight: bold;\n  margin: 10px;\n}\n.btn-cart-outline[data-v-6fc7766d] {\n  color: #ffffff;\n  background: #5b84a7 !important;\n  padding: 10px 120px;\n  font-weight: bold;\n  margin: 10px;\n}\n.priceShow[data-v-6fc7766d] {\n  color: #333;\n  font-weight: bold;\n  font-size: 20px;\n  margin: 20px;\n}\n.btn-reviews[data-v-6fc7766d] {\n  background-color: #5b84a7 !important;\n  padding: 10px 20px;\n}\n/* .btn-block{\n         background-color: #5b84a7 !important;\n         padding: 10px 20px;\n         margin: 10px;\n         font-size: 8px;\n    } */\n.hardpriceCont[data-v-6fc7766d],\n.softpriceCont[data-v-6fc7766d],\n.readOnlinepriceCont[data-v-6fc7766d] {\n  border: 2px solid #5b84a7;\n  padding: 10px;\n  color: #5b84a7;\n  margin: 10px;\n  cursor: pointer;\n}\n.PriceDIV[data-v-6fc7766d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.paperT[data-v-6fc7766d] {\n  font-size: 30px;\n  line-height: 1.2;\n  font-weight: bold;\n  text-transform: capitalize;\n  color: rgba(0, 0, 0, 0.84);\n}\n.paperDetailContent[data-v-6fc7766d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.transparentBtn[data-v-6fc7766d] {\n  cursor: pointer;\n  font-size: 17px;\n  margin-bottom: 15px;\n}\n.paperAttr[data-v-6fc7766d] {\n  margin-top: 0;\n  margin-bottom: 0;\n  width: 20%;\n  padding: 40px 10px;\n  background: #f7f8fa;\n}\n.paperContent[data-v-6fc7766d] {\n  background: #ffffff;\n  width: 80%;\n  padding: 20px 20px;\n}\n.reviewAvatar[data-v-6fc7766d] {\n  text-transform: capitalize;\n  background: #5b84a7;\n  color: #ffffff;\n  padding: 11px 15px;\n  font-size: 15px;\n  border-radius: 50%;\n}\n.review-div[data-v-6fc7766d] {\n  width: 70%;\n  margin-right: auto;\n}\n.avatarD[data-v-6fc7766d] {\n  margin-right: 20px;\n  margin-top: 10px;\n}\n.tabre[data-v-6fc7766d] {\n  overflow: hidden;\n  margin-top: 10px;\n}\n.dateCreated[data-v-6fc7766d] {\n  font-size: 13px;\n  color: #adacac;\n  font-weight: 400;\n}\n.reviewerName[data-v-6fc7766d] {\n  text-transform: capitalize;\n  margin-top: -6px;\n}\n.floatRT[data-v-6fc7766d] {\n  overflow: hidden;\n}\n.btn-reviews[data-v-6fc7766d] {\n  background-color: #5b84a7 !important;\n  padding: 10px 20px;\n  font-size: 11px;\n}\n.reviewdiv[data-v-6fc7766d] {\n  margin: 20px 0;\n  margin-right: auto;\n  margin-left: 0;\n  padding-bottom: 10px;\n  font-size: 14px;\n}\n.proReview[data-v-6fc7766d],\n.proEdi[data-v-6fc7766d] {\n  font-size: 13px;\n}\n.floatUserRating[data-v-6fc7766d] {\n  margin-right: 104px;\n  margin-left: 0px;\n}\n.floatReview[data-v-6fc7766d] {\n  float: left;\n}\n.reviewList[data-v-6fc7766d] {\n  margin-bottom: 20px;\n}\n.rateStyle[data-v-6fc7766d] {\n  float: left;\n  margin: 2px;\n}\n.reviews[data-v-6fc7766d] {\n  font-size: 14px;\n  font-weight: 500;\n}\n@media only screen and (max-width: 768px) {\nbutton.btn.btn-block[data-v-2976cded][data-v-6fc7766d] {\n    background-color: #5b84a7 !important;\n    font-size: 11px;\n}\n.paperT[data-v-6fc7766d] {\n    font-size: 16px;\n    line-height: 24px;\n}\n.paperAttr[data-v-6fc7766d] {\n    font-size: 16px;\n}\np.tabT[data-v-6fc7766d] {\n    font-size: 16px;\n}\ndiv.transparentBtn[data-v-6fc7766d] {\n    font-size: 16px;\n}\n}\n@media only screen and (max-width: 425px) {\n.container[data-v-6fc7766d] {\n    padding-top: 100px;\n    padding-left: 0;\n    padding-right: 0;\n}\ndiv.col-md-12.col-sm-12.col-xs-12[data-v-6fc7766d] {\n    padding-left: 0;\n    padding-right: 0;\n}\ndiv.col-lg-4.col-md-4.col-sm-4.col-xs-4.vieww[data-v-6fc7766d] {\n    padding-left: 0;\n    padding-right: 0;\n}\nsmall.created[data-v-6fc7766d] {\n    font-size: 11px;\n}\n.tabC[data-v-6fc7766d] {\n    padding-top: 0;\n    padding-bottom: 0;\n}\n.sponsor[data-v-6fc7766d] {\n    font-size: 11px;\n}\n.tabT.sponsor[data-v-6fc7766d] {\n    font-size: 11px;\n}\n.paperAttr[data-v-6fc7766d] {\n    display: none;\n}\n.paperContent[data-v-6fc7766d] {\n    width: 100%;\n    margin-top: 120px;\n    padding: 15px;\n}\n.reviewdiv[data-v-6fc7766d] {\n    padding: 0;\n}\n.expertImg[data-v-41dd3c56][data-v-6fc7766d] {\n    max-width: 60px;\n    max-height: 60px;\n    width: 60px;\n    height: 60px;\n    border-radius: 50%;\n    overflow: hidden;\n    margin-right: 10px;\n}\n.market[data-v-6fc7766d] {\n    height: 200px;\n    display: none;\n}\n.text[data-v-6fc7766d] {\n    font-size: 20px;\n}\n.paperT[data-v-6fc7766d] {\n    font-size: 16px;\n    line-height: 24px;\n}\n.paperAttr[data-v-6fc7766d] {\n    font-size: 16px;\n}\np.tabT[data-v-6fc7766d] {\n    font-size: 16px;\n}\n.tabC[data-v-6fc7766d] {\n    margin: 10px 0;\n}\n.followExpert[data-v-6fc7766d] {\n    margin-right: 0;\n}\n.aboutExpert[data-v-6fc7766d] {\n    width: 70%;\n    margin-right: 10px;\n}\ndiv.transparentBtn[data-v-6fc7766d] {\n    font-size: 16px;\n}\n}\n@media only screen and (max-width: 375px) {\n.paperT[data-v-6fc7766d] {\n    font-size: 16px;\n    line-height: 24px;\n}\n.paperAttr[data-v-6fc7766d] {\n    font-size: 16px;\n}\np.tabT[data-v-6fc7766d] {\n    font-size: 16px;\n}\na.viewDetailCart[data-v-6fc7766d] {\n    font-size: 8px;\n}\ndiv.viewDetailCart.sndtoli[data-v-6fc7766d] {\n    font-size: 8px;\n}\ndiv.transparentBtn[data-v-6fc7766d] {\n    font-size: 11px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(669);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-paper-detail-component",
  components: {
    "app-video-banner": __WEBPACK_IMPORTED_MODULE_0__components_categoryVideoBannerComponent___default.a
  },
  data: function data() {
    return {
      id: this.$route.params.id,
      paperDetail: {},
      price: "",
      email: "",
      user_id: "",
      title: "",
      user: {},
      prodType: "",
      getType: "",
      authenticate: false,
      anonymousCart: [],
      hardCopy: false,
      hardpriceCont: true,
      softCopy: false,
      softpriceCont: true,
      readOnline: false,
      loginCart: [],
      readOnlinepriceCont: true,
      product: false,
      subscribe: false,
      NotratedOne: true,
      NotratedTwo: true,
      RatedTwo: false,
      RatedOne: false,
      NotratedThree: true,
      RatedThree: false,
      NotratedFour: true,
      RatedFour: false,
      NotratedFive: true,
      RatedFive: false,
      review: {
        productId: "",
        title: "",
        description: "",
        rating: 0
      },
      star: 0,
      reviews: false,
      rateFa: true,
      share: false,
      allReviews: [],
      purchaseCheckReal: false,
      excerptCheck: false,
      vendor: {},
      vendorDetail: false,
      usersub: null,
      freeTrial: true,
      checkLibrary: [],
      inLibrary: false,
      userFollowing: {},
      following: false,
      followText: "follow",
      update: 0,
      showUpgrade: false,
      suLe: null
    };
  },
  created: function created() {
    var _this = this;

    var user = JSON.parse(localStorage.getItem("authUser"));

    if (user != null) {
      this.token = user.access_token;
      axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_1__config__["b" /* getCustomerHeader */])() }).then(function (response) {
        var userDate = response.data.created_at;
        _this.startDate = new Date(userDate);

        _this.startDateParsed = Date.parse(new Date(userDate));

        _this.currentDate = Date.parse(new Date());

        _this.endDate = Date.parse(new Date(_this.startDate.setDate(_this.startDate.getDate(userDate) + 30)));

        if (_this.currentDate >= _this.endDate) {
          _this.freeTrial = false;
        } else {}
      });

      axios.get("/api/user/orders", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        response.data.data.forEach(function (element) {
          element.orderDetail.forEach(function (item) {
            if (item.type === "MARKET RESEARCH") {
              _this.checkLibrary.push(item);
            }
          });
        });
      });
      axios.get("/api/user/subscription-plans", {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (response) {
        _this.usersub = Number(response.data[0].level);
      });
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    var user = JSON.parse(localStorage.getItem("authUser"));

    axios.get("/api/product-detail/" + this.id).then(function (response) {
      if (response.status === 200) {
        _this2.suLe = response.data.data[0].subscriptionLevel;
        if (_this2.$route.params.type === "product") {
          _this2.product = true;

          if (response.data.data[0].prodType === "White Paper") {
            _this2.paperDetail = response.data.data[0].whitePaper;
          } else if (response.data.data[0].prodType === "Market Reports") {
            _this2.paperDetail = response.data.data[0].marketReport;
          } else if (response.data.data[0].prodType === "Market Research") {
            _this2.paperDetail = response.data.data[0].marketResearch;
          }
          _this2.paperDetail.softCopyPrice = response.data.data[0].softCopyPrice;
          _this2.paperDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
          _this2.paperDetail.uid = response.data.data[0].id;
          _this2.paperDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;

          if (!isNaN(parseInt(_this2.paperDetail.hardCopyPrice))) {
            _this2.price = _this2.paperDetail.softCopyPrice;
            _this2.hardpriceCont = false;
            _this2.hardCopy = true;
            _this2.title = "HardCopy";
          } else if (!isNaN(_this2.paperDetail.softCopyPrice)) {
            _this2.price = _this2.paperDetail.softCopyPrice;
            _this2.softpriceCont = false;
            _this2.softCopy = true;
            _this2.title = "SoftCopy";
          } else if (!isNaN(_this2.paperDetail.readOnlinePrice)) {
            _this2.price = _this2.paperDetail.softCopyPrice;
            _this2.readOnlinepriceCont = false;
            _this2.readOnline = true;
            _this2.title = "ReadOnline";
          }
          _this2.price = _this2.paperDetail.softCopyPrice;
        } else if (_this2.$route.params.type === "subscribe") {
          _this2.subscribe = true;
          _this2.prodType = response.data.data[0].prodType;
          _this2.prodType = _this2.prodType.replace(/ /g, "-");

          if (response.data.data[0].prodType === "Journals") {
            _this2.paperDetail = response.data.data[0].journal;
            if (response.data.data[0].hardCopyPrice !== "" || response.data.data[0].softCopyPrice !== "" || response.data.data[0].readOnlinePrice !== "") {
              _this2.paperDetail.purchaseCheck = true;
              _this2.purchaseCheckReal = true;
              _this2.paperDetail.softCopyPrice = response.data.data[0].softCopyPrice;
              _this2.paperDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
              _this2.paperDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
            } else {
              _this2.paperDetail.purchaseCheck = false;
              _this2.purchaseCheckReal = false;
            }
            _this2.getType = "Insight Subscription Journal";
          } else if (response.data.data[0].prodType === "White Paper") {
            _this2.paperDetail = response.data.data[0].whitePaper;
            if (response.data.data[0].hardCopyPrice !== "" || response.data.data[0].softCopyPrice !== "" || response.data.data[0].readOnlinePrice !== "") {
              _this2.paperDetail.purchaseCheck = true;
              _this2.purchaseCheckReal = true;
              _this2.paperDetail.softCopyPrice = response.data.data[0].softCopyPrice;
              _this2.paperDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
              _this2.paperDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
            } else {
              _this2.paperDetail.purchaseCheck = false;
              _this2.purchaseCheckReal = false;
            }
            _this2.getType = "Insight Subscription White Paper";
          } else if (response.data.data[0].prodType === "Market Reports") {
            if (response.data.data[0].hardCopyPrice !== "" || response.data.data[0].softCopyPrice !== "" || response.data.data[0].readOnlinePrice !== "") {
              _this2.paperDetail.purchaseCheck = true;
              _this2.purchaseCheckReal = true;
              _this2.paperDetail.softCopyPrice = response.data.data[0].softCopyPrice;
              _this2.paperDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
              _this2.paperDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
            } else {
              _this2.paperDetail.purchaseCheck = false;
              _this2.purchaseCheckReal = false;
            }
            _this2.paperDetail = response.data.data[0].marketReport;

            _this2.getType = "Insight Subscription Market Report";
          } else if (response.data.data[0].prodType === "Market Research") {
            _this2.paperDetail = response.data.data[0].marketResearch;
            if (response.data.data[0].hardCopyPrice !== "" || response.data.data[0].softCopyPrice !== "" || response.data.data[0].readOnlinePrice !== "") {
              _this2.paperDetail.purchaseCheck = true;
              _this2.purchaseCheckReal = true;
              _this2.paperDetail.softCopyPrice = response.data.data[0].softCopyPrice;
              _this2.paperDetail.subLevel = response.data.data[0].subscriptionLevel;

              _this2.paperDetail.hardCopyPrice = response.data.data[0].hardCopyPrice;
              _this2.paperDetail.readOnlinePrice = response.data.data[0].readOnlinePrice;
            } else {
              _this2.paperDetail.purchaseCheck = false;
              _this2.purchaseCheckReal = false;
            }
            _this2.getType = "Insight Subscription Market Research";
          }

          _this2.paperDetail.uid = response.data.data[0].id;
        }

        if (_this2.paperDetail.excerptFile.includes("http")) {
          _this2.excerptCheck = true;
        } else {
          _this2.excerptCheck = false;
        }

        _this2.vendor = response.data.data[0].vendor ? response.data.data[0].vendor : null;
        _this2.vendorDetail = true;

        _this2.getProductReviews(_this2.paperDetail.uid);
        _this2.checkInLibrary(_this2.id, _this2.paperDetail.title);
      }
    }).catch(function (error) {
      console.log(error);
    });

    if (user !== null) {
      this.email = user.email;
      this.user_id = user.id;
      this.user = user;
      this.authenticate = true;

      axios.get("/api/user-following", {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            if (item.user_id === _this2.user.id && item.vendorId === _this2.vendor.id) {
              _this2.following = true;
              _this2.followText = "following";

              _this2.update++;
            } else {
              _this2.following = false;
              _this2.followText = "follow";

              _this2.update++;
            }
          });
        }
      });
    }
  },

  methods: {
    follow: function follow(params, name) {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));

      var data = {
        vendorName: name,
        vendorId: params
      };
      if (user != null) {
        axios.post("/api/user-following", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 201) {
            _this3.following = true;
            _this3.followText = "following";
            _this3.update++;
            _this3.$toasted.success("Successfully followed");
          } else if (response.data === " Already following") {
            _this3.$toasted.error("Already following");
          } else {
            _this3.$toasted.error("Error");
          }
        }).catch(function (error) {
          console.log(error);
        });
      } else {
        this.$toasted.error("You have to log in to follow");
      }
    },
    unFollow: function unFollow() {
      var _this4 = this;

      var vendor = this.vendor.id;
      var unfollow = confirm("unfollow " + this.vendor.storeName + "?");
      if (unfollow) {
        axios.delete("/api/user-following/" + vendor, {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 200) {
            _this4.following = false;
            _this4.followText = "follow";
            _this4.update++;
            _this4.$toasted.success("Successfully Unfollowed");
          } else {
            _this4.$toasted.error("Already Unfollowed");
          }
        });
      }
    },
    followToggle: function followToggle() {
      this.following ? this.unFollow() : this.follow(this.vendor.id, this.vendor.storeName);
    },
    togglePrice: function togglePrice(params) {
      if (params === "hardCopy") {
        this.price = this.paperDetail.softCopyPrice;
        this.hardpriceCont = this.softCopy = this.readOnline = false;
        this.hardCopy = this.softpriceCont = this.readOnlinepriceCont = true;
        this.title = "HardCopy";
      } else if (params === "softCopy") {
        this.price = this.paperDetail.softCopyPrice;
        this.softpriceCont = this.hardCopy = this.readOnline = false;
        this.softCopy = this.hardpriceCont = this.readOnlinepriceCont = true;
        this.title = "SoftCopy";
      } else if (params === "readOnline") {
        this.price = this.paperDetail.softCopyPrice;
        this.readOnlinepriceCont = this.hardCopy = this.softCopy = false;
        this.readOnline = this.softpriceCont = this.hardpriceCont = true;
        this.title = "ReadOnline";
      }
    },
    addToCart: function addToCart(id) {
      this.price = this.paperDetail.softCopyPrice;
      var cart = {
        productId: id,
        price: this.price
      };
      if (!cart.price) {
        this.$toasted.error("Please select price");
      }

      if (this.title === "HardCopy") {
        cart.quantity = 1;
        cart.prodType = "HardCopy";
      } else if (this.title === "SoftCopy") {
        cart.quantity = 0;
        cart.prodType = "SoftCopy";
      } else if (this.title === "ReadOnline") {
        cart.quantity = 0;
        cart.prodType = "ReadOnline";
      }

      if (this.authenticate === false) {
        cart.cartNum = 1;
        this.anonymousCart.push(cart);
        if (JSON.parse(localStorage.getItem("userCart")) === null) {
          localStorage.setItem("userCart", JSON.stringify(this.anonymousCart));
          var sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var cartCount = sessionCart.length;
          this.$emit("getCartCount", cartCount);
          this.$notify({
            group: "cart",
            title: this.paperDetail.title,
            text: "Successfully Added to Cart!"
          });
        } else if (JSON.parse(localStorage.getItem("userCart")) !== null) {
          var _sessionCart = JSON.parse(localStorage.getItem("userCart"));
          var ss = _sessionCart.length;
          _sessionCart[ss] = cart;
          localStorage.setItem("userCart", JSON.stringify(_sessionCart));
          var a = JSON.parse(localStorage.getItem("userCart"));
          var aCount = a.length;
          this.$emit("getCartCount", aCount);
          this.$notify({
            group: "cart",
            title: this.paperDetail.title,
            text: "Successfully Added to Cart!"
          });
        }
      } else {
        this.addCart(cart);
      }
    },
    addCart: function addCart(cart) {
      var _this5 = this;

      axios.post("/api/cart", JSON.parse(JSON.stringify(cart)), {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (response) {
        if (response.status === 201) {
          _this5.loginCart.push(response.data);
          var aCount = _this5.loginCart.length;
          _this5.$emit("getCartCount", aCount);
          _this5.$emit("plusCart");
          _this5.$notify({
            group: "cart",
            title: _this5.paperDetail.title,
            text: "Successfully Added to Cart!"
          });
        } else if (response.status == 200) {
          _this5.$toasted.info("Already in cart");
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    sendItemToLibrary: function sendItemToLibrary(id, name) {
      var _this6 = this;

      var data = {
        id: id,
        prodType: this.prodType.replace(/\-/g, " "),
        getType: this.getType,
        name: name
      };
      if (this.freeTrial === true || this.usersub > 0) {
        axios.post("/api/user/sendtolibrary", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + this.user.access_token }
        }).then(function (response) {
          if (response.status === 201) {
            _this6.$toasted.success("Successfully added to BizLibrary");
            _this6.inLibrary = true;
          } else if (response.data == "Order Already Added") {
            _this6.inLibrary = true;
            _this6.$toasted.error("You have already added this product before");
          } else if (response.data.status === 405 || response.data === " ") {
            _this6.showUpgrade = true;
          } else {
            _this6.showUpgrade = true;
          }
        }).catch(function (error) {
          if (error.response.data.message === "Unauthenticated.") {
            _this6.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: _this6.$router.fullPath }
            });
          } else {
            _this6.$toasted.error(error.response.data.message);
            console.log(error.response.data.message);
          }
        });
      } else {
        this.showUpgrade = true;
      }
    },
    rateWithStar: function rateWithStar(num) {
      switch (num) {
        case "1":
          this.NotratedOne = !this.NotratedOne;
          this.RatedOne = !this.RatedOne;
          this.review.rating = 1;
          break;
        case "2":
          this.NotratedTwo = this.NotratedOne = !this.NotratedTwo;
          this.RatedTwo = this.RatedOne = !this.RatedTwo;
          this.review.rating = 2;
          break;
        case "3":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = !this.NotratedThree;
          this.RatedTwo = this.RatedOne = this.RatedThree = !this.RatedThree;
          this.review.rating = 3;
          break;
        case "4":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = !this.NotratedFour;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = !this.RatedFour;
          this.review.rating = 4;
          break;
        case "5":
          this.NotratedTwo = this.NotratedOne = this.NotratedThree = this.NotratedFour = this.NotratedFive = !this.NotratedFive;
          this.RatedTwo = this.RatedOne = this.RatedThree = this.RatedFour = this.RatedFive = !this.RatedFive;
          this.review.rating = 5;
          break;
        default:
          this.NotratedOne = this.NotratedTwo = this.NotratedThree = this.NotratedFour = this.NotratedFive = true;
          this.RatedOne = this.RatedTwo = this.RatedThree = this.RatedFour = this.RatedFive = false;
      }
    },
    alreadyAdded: function alreadyAdded() {
      this.$toasted.error("Already added to BizLibrary");
    },
    submitReview: function submitReview() {
      var _this7 = this;

      if (this.authenticate === false) {
        this.$toasted.error("You must log in to review a product");
      } else {
        this.review.productId = this.paperDetail.uid;
        axios.post("/api/user/reviews", JSON.parse(JSON.stringify(this.review)), {
          headers: { Authorization: "Bearer " + this.user.access_token }
        }).then(function (response) {
          if (response.status === 201) {
            _this7.$toasted.success("Reviews successfully saved");
            _this7.review.productId = _this7.review.title = _this7.review.description = "";
            _this7.review.rating = 0;
            _this7.NotratedOne = _this7.NotratedTwo = _this7.NotratedThree = _this7.NotratedFour = _this7.NotratedFive = true;
            _this7.RatedOne = _this7.RatedTwo = _this7.RatedThree = _this7.RatedFour = _this7.RatedFive = false;
          } else {
            _this7.$toasted.error("You cannot review this product until you purchase it");
          }
        }).catch(function (error) {
          var errors = Object.values(error.response.data.errors);
          errors.forEach(function (item) {
            _this7.$toasted.error(item[0]);
          });
        });
      }
    },
    checkInLibrary: function checkInLibrary(id, title) {
      for (var index = 0; index < this.checkLibrary.length; index++) {
        var element = this.checkLibrary[index];

        if (id === Number(element.productId)) {
          console.log("true");

          this.inLibrary = true;
          break;
        } else {
          console.log("false");

          this.inLibrary = false;
        }
      }
    },
    getProductReviews: function getProductReviews(id) {
      var _this8 = this;

      var data = {
        id: id
      };
      axios.post("/api/product/get-reviews", JSON.parse(JSON.stringify(data))).then(function (response) {
        if (response.status === 200) {
          _this8.allReviews = response.data.data;
          if (_this8.allReviews.length === 0) {
            _this8.customerReviewCount = "No Customer Review";
          } else if (_this8.allReviews.length === 1) {
            _this8.customerReviewCount = _this8.allReviews.length + " Customer Review";
          } else {
            _this8.customerReviewCount = _this8.allReviews.length + " Customer Reviews";
          }
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    showUp: function showUp() {
      this.showUpgrade = true;
    },
    redirectUpgrade: function redirectUpgrade() {
      this.showUpgrade = false;
      var routeData = this.$router.resolve({
        name: "SubscriptionProfile",
        params: {
          level: this.suLe
        }
      });
      window.open(routeData.href, "_blank");
    }
  }
});

/***/ }),

/***/ 1489:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "course-Detail" }, [
    _vm._m(0),
    _vm._v(" "),
    _vm.paperDetail
      ? _c("div", { staticClass: "paperDetailContent" }, [
          _c(
            "div",
            {
              staticClass: "paperAttr shadow",
              attrs: {
                "relative-element-selector": "#example-content",
                offset: { top: 40, bottom: 40 }
              }
            },
            [
              _c("ul", [
                _vm.paperDetail.aboutAuthor
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#aboutAuthor",
                              expression: "'#aboutAuthor'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("About Author")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.abstract
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#abstract",
                              expression: "'#abstract'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Abstract")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.aboutThisReport
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#aboutReport",
                              expression: "'#aboutReport'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("About Report")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.issn || _vm.paperDetail.issue
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#journalInfo",
                              expression: "'#journalInfo'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Journal Info")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.problemStatement
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#problemStatement",
                              expression: "'#problemStatement'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Problem Statement")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.executiveSummary
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#executiveSummary",
                              expression: "'#executiveSummary'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Executive Summary")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.excerptCheck
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#excerpts",
                              expression: "'#excerpts'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Excerpt")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.tableOfContent
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#tableOfContent",
                              expression: "'#tableOfContent'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Table of Content")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.sponsorName !== ""
                  ? _c("li", [
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "scroll-to",
                              rawName: "v-scroll-to",
                              value: "#sponsor",
                              expression: "'#sponsor'"
                            }
                          ],
                          staticClass: "transparentBtn"
                        },
                        [_vm._v("Sponsor")]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("li", [
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "scroll-to",
                          rawName: "v-scroll-to",
                          value: "#review",
                          expression: "'#review'"
                        }
                      ],
                      staticClass: "transparentBtn"
                    },
                    [_vm._v("Review/Rating")]
                  )
                ]),
                _vm._v(" "),
                _c("li", [
                  _c(
                    "div",
                    {
                      directives: [
                        {
                          name: "scroll-to",
                          rawName: "v-scroll-to",
                          value: "#expert",
                          expression: "'#expert'"
                        }
                      ],
                      staticClass: "transparentBtn"
                    },
                    [_vm._v("Expert")]
                  )
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _vm.paperDetail != {}
            ? _c("div", { staticClass: "paperContent" }, [
                _c("h2", { staticClass: "mb-3" }, [
                  _vm._v(
                    _vm._s(
                      _vm.paperDetail.title
                        ? _vm.paperDetail.title
                        : _vm.paperDetail.articleTitle
                    )
                  )
                ]),
                _vm._v(" "),
                _vm.paperDetail.author
                  ? _c("p", [
                      _vm._v("Author: " + _vm._s(_vm.paperDetail.author))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.contributors
                  ? _c("p", [
                      _vm._v(
                        "Contributors: " + _vm._s(_vm.paperDetail.contributors)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.contributor
                  ? _c("p", [
                      _vm._v(
                        "Contributors: " + _vm._s(_vm.paperDetail.contributor)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("small", { staticClass: "created" }, [
                  _vm._v(
                    _vm._s(
                      _vm._f("moment")(_vm.paperDetail.created_at, "MMM YYYY")
                    )
                  )
                ]),
                _vm._v(" "),
                _vm.product
                  ? _c("div", {}, [
                      _vm.paperDetail.hardCopyPrice !== ""
                        ? _c(
                            "div",
                            {
                              class: {
                                hardpriceCont: _vm.hardpriceCont,
                                hardCopy: _vm.hardCopy
                              },
                              on: {
                                click: function($event) {
                                  return _vm.togglePrice("hardCopy")
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n          ₦" +
                                  _vm._s(_vm.paperDetail.hardCopyPrice) +
                                  ".00\n          "
                              ),
                              _c("div", [_vm._v("Hard Copy")])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.paperDetail.softCopyPrice !== ""
                        ? _c(
                            "div",
                            {
                              class: {
                                softpriceCont: _vm.softpriceCont,
                                softCopy: _vm.softCopy
                              },
                              on: {
                                click: function($event) {
                                  return _vm.togglePrice("softCopy")
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n          ₦" +
                                  _vm._s(_vm.paperDetail.softCopyPrice) +
                                  ".00\n          "
                              ),
                              _c("div", [_vm._v("Digital Copy")])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.paperDetail.readOnlinePrice !== ""
                        ? _c(
                            "div",
                            {
                              class: {
                                readOnlinepriceCont: _vm.readOnlinepriceCont,
                                readOnline: _vm.readOnline
                              },
                              on: {
                                click: function($event) {
                                  return _vm.togglePrice("readOnline")
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n          ₦" +
                                  _vm._s(_vm.paperDetail.readOnlinePrice) +
                                  ".00\n          "
                              ),
                              _c("div", [_vm._v("Read Online")])
                            ]
                          )
                        : _vm._e()
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.product
                  ? _c("div", {}, [
                      _vm.inLibrary
                        ? _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn elevated_btn_sm text-main",
                              on: { click: _vm.alreadyAdded }
                            },
                            [_vm._v("Added To Library")]
                          )
                        : _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn elevated_btn_sm text-white btn-compliment",
                              on: {
                                click: function($event) {
                                  return _vm.addToCart(_vm.paperDetail.uid)
                                }
                              }
                            },
                            [_vm._v("Add to Cart")]
                          )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.aboutAuthor
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "aboutAuthor" } },
                      [_c("p", [_vm._v(_vm._s(_vm.paperDetail.author))])]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.abstract
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "abstract" } },
                      [
                        _c("p", { staticClass: "tabT" }, [
                          _vm._v("Abstract Information")
                        ]),
                        _vm._v(" "),
                        _c("p", [_vm._v(_vm._s(_vm.paperDetail.abstract))])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.aboutThisReport
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "aboutReport" } },
                      [
                        _c("p", { staticClass: "tabT" }, [
                          _vm._v("About this Report")
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(_vm._s(_vm.paperDetail.aboutThisReport))
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.issn || _vm.paperDetail.issue
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "journalInfo" } },
                      [
                        _c("p", { staticClass: "tabT" }, [
                          _vm._v("Journal Info")
                        ]),
                        _vm._v(" "),
                        _vm.paperDetail.issn !== ""
                          ? _c("div", [
                              _vm._v("Issn: " + _vm._s(_vm.paperDetail.issn))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.paperDetail.volume !== ""
                          ? _c("div", [
                              _vm._v(
                                "Volume: " + _vm._s(_vm.paperDetail.volume)
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.paperDetail.issue !== ""
                          ? _c("div", [
                              _vm._v("Issue: " + _vm._s(_vm.paperDetail.issue))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.paperDetail.pages !== ""
                          ? _c("div", [
                              _vm._v(
                                "Page Number: " + _vm._s(_vm.paperDetail.pages)
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.paperDetail.publisher !== ""
                          ? _c("div", [
                              _vm._v(
                                "Publisher: " +
                                  _vm._s(_vm.paperDetail.publisher)
                              )
                            ])
                          : _vm._e()
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.problemStatement
                  ? _c(
                      "div",
                      {
                        staticClass: "tabC",
                        attrs: { id: "problemStatement" }
                      },
                      [
                        _c("p", { staticClass: "tabT" }, [
                          _vm._v("Problem Statement")
                        ]),
                        _vm._v(
                          "\n        " +
                            _vm._s(_vm.paperDetail.problemStatement) +
                            "\n      "
                        )
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.paperDetail.executiveSummary
                  ? _c(
                      "div",
                      {
                        staticClass: "tabC",
                        attrs: { id: "executiveSummary" }
                      },
                      [
                        _c("p", { staticClass: "tabT" }, [
                          _vm._v("Executive Summary")
                        ]),
                        _vm._v(
                          "\n        " +
                            _vm._s(_vm.paperDetail.executiveSummary) +
                            "\n      "
                        )
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.excerptCheck
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "excerpts" } },
                      [
                        _c("p", { staticClass: "tabT" }, [_vm._v("Excerpt")]),
                        _vm._v(" "),
                        _c("div", [_vm._v(_vm._s(_vm.paperDetail.excerpt))]),
                        _vm._v(" "),
                        _vm.paperDetail.excerptFile.split(".").pop() == "jpg" ||
                        _vm.paperDetail.excerptFile.split(".").pop() == "png" ||
                        _vm.paperDetail.excerptFile.split(".").pop() == "jpeg"
                          ? _c("div", { staticClass: "mt-4 imagin" }, [
                              _c("img", {
                                attrs: {
                                  src: _vm.paperDetail.excerptFile,
                                  alt: "excerpt image"
                                }
                              })
                            ])
                          : _c("div", { staticClass: "mt-4" }, [
                              _c("video", {
                                attrs: {
                                  src: _vm.paperDetail.excerptFile,
                                  controls: "",
                                  width: "400"
                                }
                              })
                            ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "mt-3" }, [
                  _c("h4", {}, [_vm._v("Table of Content")]),
                  _vm._v(" "),
                  _vm.paperDetail.tableOfContent
                    ? _c(
                        "div",
                        {
                          staticClass: "tabC px-3 mt-0",
                          attrs: { id: "tableOfContent" }
                        },
                        [
                          _c("p", {
                            staticClass: "px-2",
                            domProps: {
                              innerHTML: _vm._s(_vm.paperDetail.tableOfContent)
                            }
                          })
                        ]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _vm.subscribe
                    ? _c("div", {}, [
                        _vm.purchaseCheckReal
                          ? _c("div", {}, [
                              _vm.paperDetail.hardCopyPrice !== ""
                                ? _c(
                                    "div",
                                    {
                                      class: {
                                        hardpriceCont: _vm.hardpriceCont,
                                        hardCopy: _vm.hardCopy
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.togglePrice("hardCopy")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n              ₦" +
                                          _vm._s(
                                            _vm.paperDetail.hardCopyPrice
                                          ) +
                                          ".00\n              "
                                      ),
                                      _c("div", [_vm._v("Hard Copy")])
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.paperDetail.softCopyPrice !== ""
                                ? _c(
                                    "div",
                                    {
                                      on: {
                                        click: function($event) {
                                          return _vm.togglePrice("softCopy")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n              ₦" +
                                          _vm._s(
                                            _vm.paperDetail.softCopyPrice
                                          ) +
                                          ".00\n              "
                                      ),
                                      _c("div", [_vm._v("Digital Copy")])
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.paperDetail.readOnlinePrice !== ""
                                ? _c(
                                    "div",
                                    {
                                      class: {
                                        readOnlinepriceCont:
                                          _vm.readOnlinepriceCont,
                                        readOnline: _vm.readOnline
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.togglePrice("readOnline")
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n              ₦" +
                                          _vm._s(
                                            _vm.paperDetail.readOnlinePrice
                                          ) +
                                          ".00\n              "
                                      ),
                                      _c("div", [_vm._v("Read Online")])
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.purchaseCheckReal
                          ? _c("div", { staticClass: "PriceDIV" }, [
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                                  on: {
                                    click: function($event) {
                                      return _vm.addToCart(_vm.paperDetail.uid)
                                    }
                                  }
                                },
                                [_vm._v("Add to Cart")]
                              )
                            ])
                          : _c("div", { staticClass: "center" }, [
                              _c("div", { staticClass: "vieww" }, [
                                _vm.inLibrary
                                  ? _c(
                                      "div",
                                      { staticClass: "viewDetailCart sndtoli" },
                                      [
                                        _c(
                                          "button",
                                          {
                                            staticClass:
                                              "elevated_btn elevated_btn_sm text-main",
                                            on: { click: _vm.alreadyAdded }
                                          },
                                          [_vm._v("Added To My Library")]
                                        )
                                      ]
                                    )
                                  : _c(
                                      "div",
                                      {
                                        staticClass: "viewDetailCart sndtoli",
                                        on: {
                                          click: function($event) {
                                            return _vm.sendItemToLibrary(
                                              _vm.paperDetail.uid,
                                              _vm.paperDetail.title
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "button",
                                          {
                                            staticClass:
                                              "elevated_btn elevated_btn_sm btn-compliment text-white"
                                          },
                                          [_vm._v("Send To My Library")]
                                        )
                                      ]
                                    )
                              ])
                            ])
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _vm.paperDetail.sponsorName !== ""
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "sponsor" } },
                      [
                        _c("p", { staticClass: "tabT sponsor" }, [
                          _vm._v("Sponsor")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "sponsor" }, [
                          _vm._v(_vm._s(_vm.paperDetail.sponsorName))
                        ]),
                        _vm._v(" "),
                        _c("div", [
                          _vm._v(_vm._s(_vm.paperDetail.aboutSponsor))
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "tabC", attrs: { id: "review" } }, [
                  _c("div", { staticClass: "row" }, [
                    _c(
                      "div",
                      { staticClass: "col-md-12 col-sm-12 col-xs-12" },
                      [
                        _c("div", { staticClass: "review-div" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.review.description,
                                  expression: "review.description"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                rows: "5",
                                placeholder: "Enter Reviews"
                              },
                              domProps: { value: _vm.review.description },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.review,
                                    "description",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", [
                            _c("i", {
                              class: {
                                fa: _vm.rateFa,
                                "fa-star-o": _vm.NotratedOne,
                                "fa-star": _vm.RatedOne
                              },
                              on: {
                                click: function($event) {
                                  return _vm.rateWithStar("1")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", {
                              class: {
                                fa: _vm.rateFa,
                                "fa-star-o": _vm.NotratedTwo,
                                "fa-star": _vm.RatedTwo
                              },
                              on: {
                                click: function($event) {
                                  return _vm.rateWithStar("2")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", {
                              class: {
                                fa: _vm.rateFa,
                                "fa-star-o": _vm.NotratedThree,
                                "fa-star": _vm.RatedThree
                              },
                              on: {
                                click: function($event) {
                                  return _vm.rateWithStar("3")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", {
                              class: {
                                fa: _vm.rateFa,
                                "fa-star-o": _vm.NotratedFour,
                                "fa-star": _vm.RatedFour
                              },
                              on: {
                                click: function($event) {
                                  return _vm.rateWithStar("4")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", {
                              class: {
                                fa: _vm.rateFa,
                                "fa-star-o": _vm.NotratedFive,
                                "fa-star": _vm.RatedFive
                              },
                              on: {
                                click: function($event) {
                                  return _vm.rateWithStar("5")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", { staticClass: "ml-lg-5" }, [
                              _vm._v(
                                _vm._s(_vm.review.rating) +
                                  " " +
                                  _vm._s(
                                    _vm.review.rating > 1 ? "stars" : "star"
                                  )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn elevated_btn_sm btn-compliment text-white mb-4",
                              on: {
                                click: function($event) {
                                  return _vm.submitReview()
                                }
                              }
                            },
                            [_vm._v("Submit")]
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _vm.share
                      ? _c("div", { staticClass: "col-md-12" }, [
                          _c("p", [_vm._v("Share across social media")])
                        ])
                      : _vm._e()
                  ])
                ]),
                _vm._v(" "),
                _vm.allReviews.length > 0
                  ? _c("div", { staticClass: "tabC" }, [
                      _c(
                        "div",
                        { staticClass: "row", attrs: { id: "reviewed" } },
                        [
                          _c("div", { staticClass: "reviews" }, [
                            _vm._v("Reviews")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.allReviews, function(Review, index) {
                            return _c(
                              "div",
                              {
                                key: index,
                                staticClass: "col-md-12 reviewdiv"
                              },
                              [
                                _c("div", { staticClass: "tabre" }, [
                                  _c("div", { staticClass: "mb-5 disRfb" }, [
                                    _c(
                                      "div",
                                      { staticClass: "floatReview avatarD" },
                                      [
                                        _c(
                                          "span",
                                          { staticClass: "reviewAvatar" },
                                          [
                                            _vm._v(
                                              _vm._s(Review.user.name.charAt(0))
                                            )
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "floatReview floatUserRating"
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "dateCreated" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                _vm._f("moment")(
                                                  Review.created_at.date,
                                                  "from"
                                                )
                                              )
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "reviewerName" },
                                          [
                                            _vm._v(
                                              _vm._s(Review.user.name) +
                                                " " +
                                                _vm._s(Review.user.lastName)
                                            )
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "floatReview" }, [
                                      _c(
                                        "div",
                                        { staticClass: "floatRT" },
                                        [
                                          _vm._l(Review.rating, function(star) {
                                            return _c(
                                              "div",
                                              { staticClass: "rateStyle" },
                                              [
                                                _c("i", {
                                                  staticClass: "fa fa-star"
                                                })
                                              ]
                                            )
                                          }),
                                          _vm._v(" "),
                                          _vm._l(5 - Review.rating, function(
                                            star
                                          ) {
                                            return _c(
                                              "div",
                                              { staticClass: "rateStyle" },
                                              [
                                                _c("i", {
                                                  staticClass: "fa fa-star-o"
                                                })
                                              ]
                                            )
                                          })
                                        ],
                                        2
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "avatarDescription" },
                                        [_vm._v(_vm._s(Review.description))]
                                      )
                                    ])
                                  ])
                                ])
                              ]
                            )
                          })
                        ],
                        2
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.vendor !== null
                  ? _c(
                      "div",
                      { staticClass: "tabC", attrs: { id: "expert" } },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "reviews" }, [
                            _vm._v("Expert")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-md-12 reviewdiv" }, [
                            _c("div", { staticClass: "tabre" }, [
                              _c("div", { staticClass: "mb-5 expert" }, [
                                _c("div", { staticClass: "expertImg" }, [
                                  _c("img", {
                                    attrs: { src: _vm.vendor.valid_id }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "aboutExpert" }, [
                                  _c("div", { staticClass: "vn" }, [
                                    _vm._v(_vm._s(_vm.vendor.storeName))
                                  ]),
                                  _vm._v(" "),
                                  _c("p", [_vm._v(_vm._s(_vm.vendor.bio))])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "elevated_btn elevated_btn_sm btn-compliment text-white",
                                    class: {
                                      followExpert: !_vm.following,
                                      followingExpert: _vm.following
                                    },
                                    on: { click: _vm.followToggle }
                                  },
                                  [_vm._v(_vm._s(_vm.followText))]
                                )
                              ])
                            ])
                          ])
                        ])
                      ]
                    )
                  : _vm._e()
              ])
            : _vm._e()
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.showUpgrade
      ? _c("div", { staticClass: "upgradeContainer animated fadeIn" }, [
          _c("div", { staticClass: "upgrade" }, [
            _c("p", { staticClass: "upgradeText" }, [
              _vm._v(
                "Oops, looks like you need to upgrade to access this. It’ll only take a sec though. 😁"
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "upgradeButtons" }, [
              _c(
                "div",
                {
                  staticClass: "upgradeButton",
                  on: { click: _vm.redirectUpgrade }
                },
                [_vm._v("Yes please!")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "upgradeButton",
                  on: { click: _vm.closeUpgrade }
                },
                [_vm._v("I'll keep exploring")]
              )
            ])
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "market" }, [
      _c("h2", { staticClass: "text" }, [_vm._v("Books")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6fc7766d", module.exports)
  }
}

/***/ }),

/***/ 573:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1486)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1488)
/* template */
var __vue_template__ = __webpack_require__(1489)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6fc7766d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userPaperDetailComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6fc7766d", Component.options)
  } else {
    hotAPI.reload("data-v-6fc7766d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 745:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(746)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(748)
/* template */
var __vue_template__ = __webpack_require__(749)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0f443e1b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoryVideoBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0f443e1b", Component.options)
  } else {
    hotAPI.reload("data-v-0f443e1b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(747);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("cd849544", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0f443e1b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoryVideoBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vidBanner[data-v-0f443e1b]{\n     position: relative;\n}\nimg[data-v-0f443e1b]{\n    margin-top: 40px;\n    width: 100%;\n    /* max-height: 300px; */\n}\n.text[data-v-0f443e1b]{\n    position: absolute;\n    top: 100px;\n    left: 16px;\n    font-size: 60px;\n    color: #ffffff;\n}\n@media(max-width: 475px){\n.vidBanner[data-v-0f443e1b]{\n     display: none;\n}\n.text[data-v-0f443e1b]{\n            font-size:30px;\n            top: 110px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "category-video-banner-component"
});

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "vidBanner" }, [
      _c("img", { attrs: { src: "/images/vid.png" } }),
      _vm._v(" "),
      _c("h2", { staticClass: "text" }, [_vm._v(" Videos")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0f443e1b", module.exports)
  }
}

/***/ })

});