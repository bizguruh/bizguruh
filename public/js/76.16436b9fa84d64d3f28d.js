webpackJsonp([76],{

/***/ 1581:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1582);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("d6561748", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-53d780b8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userProfileSubscriptionComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-53d780b8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userProfileSubscriptionComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1582:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.selected-pack[data-v-53d780b8] {\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1);\n  z-index: 3;\n}\n.duration[data-v-53d780b8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 400px;\n  margin: 0 auto;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.annual[data-v-53d780b8] {\n  padding: 8px 20px;\n  background: hsl(207, 46%, 20%);\n  color: white;\n  margin-left: 15px;\n  border-radius: 5px;\n}\n.annual-faded[data-v-53d780b8] {\n  background: rgb(128, 128, 128, 0.3);\n  color: rgb(255, 255, 255, 0.3);\n}\n.package[data-v-53d780b8]:hover {\n  border-color: black;\n}\n.messageBox[data-v-53d780b8] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(255, 255, 255, 0.6);\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n}\n.message-mini-box[data-v-53d780b8] {\n  position: relative;\n  padding: 40px;\n  width: 50%;\n  background: #f7f8fa;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.message-text[data-v-53d780b8] {\n  font-size: 18px;\n  text-align: center;\n  font-family: \"Josefin Sans\", \"sans-serif\";\n}\n.nextBox[data-v-53d780b8] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(255, 255, 255, 0.6);\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n}\n.mini-container[data-v-53d780b8] {\n  position: relative;\n  padding: 20px;\n  height: 30%;\n  background: #f7f8fa;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.closing[data-v-53d780b8] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.6);\n}\nlabel[data-v-53d780b8] {\n  margin-bottom: 12px;\n}\n.showModal[data-v-53d780b8] {\n  height: 100vh;\n  overflow: hidden;\n}\n.close[data-v-53d780b8] {\n  position: absolute;\n  right: -10px;\n  top: -10px;\n  opacity: 1;\n}\n.sponsor[data-v-53d780b8] {\n  position: relative;\n  text-align: center;\n  height: 100px;\n  background-image: url(\"/images/bgSponsor.jpg\");\n  background-size: cover;\n\n  font-size: 20px;\n}\n.main-content[data-v-53d780b8] {\n  height: 100%;\n  width: 100%;\n  padding-top: 40px;\n}\n.pricing[data-v-53d780b8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  width: 80%;\n  margin: 0 auto;\n  padding-top: 60px;\n  padding-bottom: 70px;\n}\n.price_box[data-v-53d780b8] {\n  height: auto;\n  width: 31%;\n  background: #f7f8fa;\n  border-radius: 4px;\n  position: relative;\n  text-align: center;\n}\n.bap_box[data-v-53d780b8] {\n  height: auto;\n  width: 50%;\n  background: #f7f8fa;\n  border-radius: 4px;\n  position: relative;\n  text-align: center;\n}\nul[data-v-53d780b8],\nol[data-v-53d780b8],\nli[data-v-53d780b8] {\n  list-style: square;\n  text-align: left;\n}\nul[data-v-53d780b8] {\n  padding: 15px 35px;\n}\n.price_name[data-v-53d780b8] {\n  padding: 15px;\n  text-align: center;\n  background: #22cade;\n  position: relative;\n  overflow: hidden;\n  font-weight: bold;\n}\n.price_name[data-v-53d780b8]::after {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(-45deg);\n          transform: rotate(-45deg);\n  bottom: 0;\n  left: 53%;\n}\n.price_name[data-v-53d780b8]::before {\n  content: \"\";\n  height: 100%;\n  width: 100%;\n  background: hsl(207, 46%, 20%);\n  position: absolute;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  bottom: 0;\n  left: -53%;\n}\n.price[data-v-53d780b8] {\n  font-weight: bold;\n  font-size: 24px;\n}\n.price small[data-v-53d780b8] {\n  font-size: 14px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.oPlan[data-v-53d780b8] {\n  font-size: 36px;\n  color: white;\n  text-align: center;\n}\nli[data-v-53d780b8]{\n  padding:7px;\n}\n.oPrice[data-v-53d780b8] {\n  font-size: 36px;\n  text-align: center;\n  color: white;\n}\n.price-overlay[data-v-53d780b8] {\n  background: rgba(255, 255, 255, 0.7);\n  position: absolute;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n    top:0;\n  bottom: 0;\n}\n.form[data-v-53d780b8] {\n  width: 30%;\n  height: auto;\n  padding: 15px;\n  position: relative;\n}\n.closePrice[data-v-53d780b8] {\n  position: absolute;\n  top: 0;\n  right: 0;\n}\na.linkup[data-v-53d780b8] {\n  color: #5b84a7 !important;\n  padding: 5px;\n}\n.moreDetail[data-v-53d780b8] {\n  text-align: center;\n  padding: 40px 30px;\n  background: white;\n}\n.packages[data-v-53d780b8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 95%;\n  margin: 0 auto;\n}\n.package[data-v-53d780b8] {\n  background: -webkit-gradient(linear, left top, right top, from(#fdfbfb), to(#ebedee));\n  background: linear-gradient(90deg, #fdfbfb 0%, #ebedee 100%);\n  border-radius: 3px;\n  -webkit-box-shadow: 0 4px 16px rgba(20, 23, 28, 0.25);\n          box-shadow: 0 4px 16px rgba(20, 23, 28, 0.25);\n  position: relative;\n  overflow: hidden;\n  margin: 5px;\n  border: 1px solid transparent;\n}\n.package_text[data-v-53d780b8] {\n  padding: 0 10px;\n  height: 165px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.package p[data-v-53d780b8] {\n  font-size: 14px;\n}\n.package_btn[data-v-53d780b8] {\n  display: grid;\n  margin-top: 0px;\n  width: 100%;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding-bottom: 10px;\n}\n.sponsor .spons[data-v-53d780b8] {\n  padding: 5px 20px;\n  color: #ffffff;\n  border-radius: 50px;\n  background: #5b84a7;\n  cursor: pointer;\n}\n.sponsor .spons[data-v-53d780b8]:hover {\n  background: #a4c2db;\n}\n.sponsText[data-v-53d780b8] {\n  color: #ffffff;\n}\n.container-fluid[data-v-53d780b8] {\n  padding: 20px;\n  padding-bottom: 60px;\n  position: relative;\n  font-size: 16px;\n  background: #ffffff;\n  background-image: url(\"/images/bgSubs.jpg\");\n  background-size: cover;\n  min-height: 100vh;\n}\ntd[data-v-53d780b8] {\n  font-size: 14px;\n}\n.fs12[data-v-53d780b8] {\n  position: relative;\n  font-size: 16px;\n  height: 75vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bgGreen[data-v-53d780b8] {\n  background: -webkit-gradient(linear, left top, right top, from(#44a08d), to(#093637));\n  background: linear-gradient(90deg, #44a08d 0%, #093637 100%);\n  color: #fff;\n}\n.bgBlack[data-v-53d780b8] {\n  background: -webkit-gradient(linear, left top, right top, from(#000000), to(#434343));\n  background: linear-gradient(90deg, #000000 0%, #434343 100%);\n  color: #fff;\n}\n.bgBlue[data-v-53d780b8] {\n  background: -webkit-gradient(linear, left top, right top, from(#0575e6), to(#021b79));\n  background: linear-gradient(90deg, #0575e6 0%, #021b79 100%);\n  color: #fff;\n}\n.bgPurple[data-v-53d780b8] {\n  background: -webkit-gradient(linear, left top, right top, from(#673ab7), to(#512da8));\n  background: linear-gradient(90deg, #673ab7 0%, #512da8 100%);\n  color: #fff;\n}\n.slower[data-v-53d780b8] {\n  -webkit-transition-delay: 5s !important;\n          transition-delay: 5s !important;\n  -webkit-transition-duration: 5s !important;\n          transition-duration: 5s !important;\n}\n.seeMore[data-v-53d780b8] {\n  display: none;\n  color: #a3c2dc;\n  text-transform: capitalize;\n}\n.mobile[data-v-53d780b8] {\n  display: none;\n}\n.clhere[data-v-53d780b8] {\n  color: #a3c2dc;\n  font-size: xx-large;\n}\n.txtCenter[data-v-53d780b8] {\n  text-align: center;\n  margin: 0 auto;\n}\n.bgWhite[data-v-53d780b8] {\n  background: #fff;\n}\n.shadowSponsor[data-v-53d780b8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.5);\n  top: 0;\n  left: 0;\n}\n.Bar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  font-weight: bold;\n  padding: 10px 0;\n}\n.firstBar[data-v-53d780b8] {\n  height: 100px;\n  text-align: left;\n  margin-bottom: 10px;\n  padding: 10px 0;\n}\n.secondBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n}\n.thirdBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n}\n.fourthBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  line-height: 1.2;\n  margin-bottom: 10px;\n}\n.fifthBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n}\n.sixthBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n}\n.faint[data-v-53d780b8] {\n  opacity: 0.5;\n}\n.seventhBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n}\n.eightBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n  line-height: 1.2;\n}\n.ninthBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n}\n.tenthBar[data-v-53d780b8] {\n  height: 50px;\n  text-align: center;\n  margin-bottom: 10px;\n  padding: 10px 0;\n}\n.txtLeft[data-v-53d780b8] {\n  text-align: left;\n}\n.fa[data-v-53d780b8],\n.fas[data-v-53d780b8] {\n  color: #a3c2dc;\n}\n.headerCon[data-v-53d780b8] {\n  text-align: center;\n  padding: 20px 0;\n  font-size: 18px;\n  color: black;\n}\n.subscribeHeader[data-v-53d780b8] {\n  font-size: 28px;\n  text-align: center;\n  font-weight: bold;\n  line-height: 1.5;\n  color: black;\n}\n.subscribed[data-v-53d780b8] {\n  background: #a3c2dc !important;\n  color: #ffffff !important;\n}\n.firstBar[data-v-53d780b8] {\n  text-align: left !important;\n}\n/* .stbtn {\n        margin-top: -15px !important;\n\n    } */\n.delay[data-v-53d780b8] {\n  -webkit-animation-delay: 5s;\n  animation-delay: 5s;\n}\n.subContent[data-v-53d780b8] {\n  font-size: 14px;\n  color: #717070;\n  height: auto;\n  padding: 0 10px;\n  line-height: 1.2;\n  margin-bottom: 10px;\n}\n.subscriptionHeaderTitle[data-v-53d780b8] {\n  font-weight: 900;\n  padding: 20px 0;\n  font-size: 18px;\n  margin-bottom: 16px;\n}\n.fa-check[data-v-53d780b8] {\n  color: #a3c2dc;\n}\n.gStart[data-v-53d780b8] {\n  color: #fff;\n  background: #a3c2dc;\n  padding: 2px 5px;\n  border-radius: 5px;\n  margin: 30px auto 0;\n  cursor: pointer;\n  width: 80%;\n  -webkit-box-shadow: 0 6px #eee;\n          box-shadow: 0 6px #eee;\n}\n.gStart[data-v-53d780b8]:active {\n  background: rgba(0, 0, 0, 0.64) !important;\n  -webkit-box-shadow: 0 2px #eee;\n          box-shadow: 0 2px #eee;\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.gStart a[data-v-53d780b8] {\n  color: #ffffff !important;\n}\n.gStartt[data-v-53d780b8] {\n  padding: 2px 5px;\n  border-radius: 5px;\n  margin: 30px auto 0;\n  cursor: pointer;\n  width: 80%;\n  -webkit-box-shadow: 0 6px #eee;\n          box-shadow: 0 6px #eee;\n}\n.gStartt[data-v-53d780b8]:active {\n  -webkit-box-shadow: 0 2px #eee;\n          box-shadow: 0 2px #eee;\n  -webkit-transform: translateY(4px);\n          transform: translateY(4px);\n}\n.rowBtn[data-v-53d780b8] {\n  padding: 50px;\n}\n.rowBtn .paddbtn div[data-v-53d780b8] {\n  font-size: 13px;\n  color: #6d6c6c;\n  text-align: center;\n}\n.paddbtn[data-v-53d780b8] {\n  border-bottom: 1.4px solid #e8e8e8;\n  padding-bottom: 50px;\n}\n.subCol[data-v-53d780b8] {\n  padding: 60px;\n}\n.row-sub[data-v-53d780b8] {\n  text-align: center;\n  margin: 0;\n}\n.subDiv[data-v-53d780b8] {\n  margin: 20px;\n}\n.subRow div[data-v-53d780b8] {\n  margin: 20px;\n}\n.btn-subscribe[data-v-53d780b8] {\n  background: #a3c2dc !important;\n  color: #ffffff;\n}\n@media (max-width: 768px) {\n.pricing[data-v-53d780b8] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.price_box[data-v-53d780b8] {\n    width: 70%;\n    margin-bottom: 40px;\n}\n.message-mini-box[data-v-53d780b8] {\n    width: 70%;\n}\n.mini-container[data-v-53d780b8] {\n    height: auto;\n}\n.container-fluid[data-v-53d780b8] {\n    padding: 10px 3px;\n}\n.subscribeHeader[data-v-53d780b8] {\n    font-size: 20px;\n}\n.row-sub[data-v-53d780b8] {\n    padding: 0;\n}\n.bgWhite[data-v-53d780b8] {\n    padding-right: 10px;\n    padding-left: 10px;\n    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f2f6f8+1,d8e1e7+50,b5c6d0+51,e0eff9+100;Grey+Gloss+%232 */\n    background: rgb(242, 246, 248); /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\n    background: linear-gradient(\n      45deg,\n      rgba(242, 246, 248, 0.4) 1%,\n      rgba(216, 225, 231, 0.4) 50%,\n      rgba(181, 198, 208, 0.4) 51%,\n      rgba(224, 239, 249, 0.4) 100%\n    ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */\n}\n.lessPad[data-v-53d780b8] {\n    padding: 0;\n    font-size: 12px;\n}\n.packages[data-v-53d780b8] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    width: 90%;\n    padding: 0 0 30px;\n}\n.package[data-v-53d780b8] {\n    margin: 10px 5px;\n}\n.package_btn[data-v-53d780b8] {\n    margin-top: 10px;\n}\n.package_text[data-v-53d780b8] {\n    height: auto;\n    display: block;\n    padding: 10px;\n}\n.shadowSponsor[data-v-53d780b8] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.sponsText[data-v-53d780b8] {\n    margin-bottom: 20px;\n}\n}\n@media (max-width: 425px) {\n.duration[data-v-53d780b8]{\n    width:auto;\n    font-size:14px;\n    margin:20px auto;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.annual[data-v-53d780b8]{\n    font-size:12px;\n}\n.message-mini-box[data-v-53d780b8] {\n    width: 80%;\n}\n.shadowSponsor[data-v-53d780b8] {\n    margin-top: 0;\n}\n.bgWhite[data-v-53d780b8] {\n    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f2f6f8+0,d8e1e7+50,b5c6d0+51,e0eff9+100;Grey+Gloss+%232 */\n    background: rgb(242, 246, 248); /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\n    background: linear-gradient(\n      45deg,\n      rgba(242, 246, 248, 0.4) 0%,\n      rgba(216, 225, 231, 0.4) 50%,\n      rgba(181, 198, 208, 0.4) 51%,\n      rgba(224, 239, 249, 0.4) 100%\n    ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */\n}\n.fs12[data-v-53d780b8] {\n    height: 100%;\n}\n.shadowWhite[data-v-53d780b8] {\n    position: unset;\n}\n.sponsor[data-v-53d780b8] {\n    font-size: 16px;\n}\n.row-sub[data-v-53d780b8] {\n    margin: 0;\n}\n.mobile[data-v-53d780b8] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.toggleOn[data-v-53d780b8] {\n    display: block;\n}\n.seeMore[data-v-53d780b8] {\n    display: block;\n    font-size: 11px;\n}\n.headerCon[data-v-53d780b8] {\n    font-size: 16px;\n}\n.subContent[data-v-53d780b8] {\n    font-size: 16px;\n    margin: 0;\n    padding: 1px 3px;\n    line-height: 1.2;\n}\n.row-sub[data-v-53d780b8] {\n    padding: 0;\n}\n.subscriptionHeaderTitle[data-v-53d780b8] {\n    font-size: 18px;\n    font-weight: bold;\n}\n.pry_header[data-v-53d780b8] {\n    height: auto;\n    margin-bottom: 20px;\n}\n.pricing[data-v-53d780b8] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    font-size: 18px;\n    font-weight: bold;\n    color: #000;\n    padding: 10px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\nol ul[data-v-53d780b8] {\n    margin-bottom: 10px;\n}\nli[data-v-53d780b8] {\n    font-size: 16px;\n    font-weight: normal;\n}\n.bap_box[data-v-53d780b8] {\n    width: 70%;\n}\n}\n@media (max-width: 425px) {\n.bap_box[data-v-53d780b8] {\n    width: 80%;\n}\n.price_box[data-v-53d780b8] {\n    width: 100%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1583:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__toggle__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["currentSub"],
  name: "user-profile-subscription-component",
  data: function data() {
    var _ref;

    return _ref = {
      time: false,
      selected: false,
      selected_1: false,
      selected_3: false,
      showMessage: false,
      duration: "",
      half: false,
      showOverlay: false,
      plan: "",
      price: ""
    }, _defineProperty(_ref, "duration", "monthly"), _defineProperty(_ref, "essential_pack", false), _defineProperty(_ref, "bizuruh_pack", false), _defineProperty(_ref, "bizguruh_pack", false), _defineProperty(_ref, "subLevel", null), _defineProperty(_ref, "level", 0), _defineProperty(_ref, "subscriptions", []), _defineProperty(_ref, "token", ""), _defineProperty(_ref, "email", ""), _defineProperty(_ref, "isActive", true), _defineProperty(_ref, "authenticate", false), _defineProperty(_ref, "essential", false), _defineProperty(_ref, "bizuruh", false), _defineProperty(_ref, "premium", false), _defineProperty(_ref, "isShown1", false), _defineProperty(_ref, "isShown2", false), _defineProperty(_ref, "isShown3", false), _defineProperty(_ref, "isShown4", false), _defineProperty(_ref, "essentialLevel", false), _defineProperty(_ref, "premiumLevel", false), _defineProperty(_ref, "bizuruhLevel", false), _defineProperty(_ref, "freeLevel", false), _defineProperty(_ref, "breadcrumbList", []), _defineProperty(_ref, "subHover", false), _defineProperty(_ref, "userSub", null), _defineProperty(_ref, "accountMessage", false), _defineProperty(_ref, "bapMessage", false), _ref;
  },
  components: {
    Toggle: __WEBPACK_IMPORTED_MODULE_0__toggle___default.a
  },
  mounted: function mounted() {
    var _this = this;

    this.subLevel = Number(this.$route.params.level);

    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user != null) {
      this.authenticate = true;
      this.token = user.access_token;
    }

    axios.get("/api/subscription-plans").then(function (response) {
      if (response.status === 200) {
        _this.isActive = false;
        //this.subscriptions = response.data;
        if (user != null) {
          _this.getUserSubscriptionPlan(response.data);
        } else {
          _this.subscriptions = response.data;
        }
      }
    }).catch(function (error) {
      console.log(error);
    });
    if (this.subLevel === 0) {
      this.freeLevel = true;
    } else if (this.subLevel === 1) {
      this.essentialLevel = true;
    } else if (this.subLevel === 2) {
      this.bizuruhLevel = true;
    } else if (this.subLevel === 3) {
      this.premiumLevel = true;
    } else {}
  },

  watch: {
    $route: function $route() {
      this.updateList();
    }
  },
  methods: {
    handleToggle: function handleToggle(value) {
      this.time = value;

      if (!value) {
        this.duration = "monthly";
      } else {
        this.duration = "yearly";
      }
    },
    redirectMessages: function redirectMessages() {
      if (this.$route.params.level == 2) {
        this.selected = true;
        this.showMessage = true;
        this.accountMessage = true;
        this.half = true;
      }
      if (this.$route.params.level == 1) {
        this.selected_1 = true;
      }
      if (this.$route.params.level == 3) {
        this.selected_3 = true;
      }

      if (this.$route.query.redirect_from == "bap_progress") {
        this.showMessage = true, this.bapMessage = true;
      }
    },
    selectPackage: function selectPackage(value) {
      switch (value) {
        case "contributor":
          this.plan = "contributor";
          this.selectPlan();
          this.handleOverlay();

          break;
        case "essential":
          this.plan = "essential";
          this.level = 2;
          this.selectPlan();
          this.handleOverlay();
          break;
        case "bizguruh":
          this.plan = "bizguruh";
          this.level = 3;
          this.selectPlan();
          this.handleOverlay();
          break;
        case "bap":
          this.plan = "bap";
          this.level = 4;
          this.duration = "yearly";
          this.selectPlan();
          this.handleOverlay();
          break;

        default:
          break;
      }
    },
    selectPlan: function selectPlan() {
      if (this.plan == "contributor" && this.duration == "monthly") {
        this.price = 0;
      } else if (this.plan == "contributor" && this.duration == "yearly") {
        this.price = 0;
      }

      if (this.plan == "essential" && this.duration == "monthly") {
        this.price = 3600;
      } else if (this.plan == "essential" && this.duration == "yearly") {
        this.price = 36000;
      }

      if (this.plan == "bizguruh" && this.duration == "monthly") {
        this.price = 5000;
      } else if (this.plan == "bizguruh" && this.duration == "yearly") {
        this.price = 50000;
      }
      if (this.plan == "bap") {
        this.price = 30000;
      }
    },
    handleOverlay: function handleOverlay() {
      this.showOverlay = !this.showOverlay;
      if (!this.showOverlay) {
        this.price = 0;
      }
    },
    closeBox: function closeBox() {
      this.essential_pack = this.bizuruh_pack = this.bizguruh_pack = false;
      this.plan = "default";
      this.half = false;
      this.showMessage = this.accountMessage = this.bapMessage = false;
    },
    checkStatus: function checkStatus() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      this.user = user;
      if (user != null) {
        this.token = user.access_token;

        axios.post("/api/user/subscription-plan/" + user.id).then(function (response) {
          if (response.status === 200) {
            if (response.data.length > 0) {
              _this2.userSub = Number(response.data[0].level);
            }
          }
        });
      }
    },
    sponsor: function sponsor() {
      if (this.authenticate) {
        this.$router.push({
          name: "Sponsor"
        });
      } else {
        this.$router.replace({
          name: "auth",
          query: { redirect: "sponsor" }
        });
      }
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    getUserSubscriptionPlan: function getUserSubscriptionPlan(data) {
      var _this3 = this;

      var subscription = [];
      var sub = [];
      axios.get("/api/user/subscription-plans", {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            if (item.level === "3" && item.verify === 1) {
              _this3.premium = true;
            } else if (item.level === "2" && item.verify === 1) {
              _this3.bizuruh = true;
            } else if (item.level === "1" && item.verify === 1) {
              _this3.essential = true;
            }
          });
        }
      }).catch(function (error) {
        console.log(error);
      });
    },
    subscribe: function subscribe(type, duration) {
      var _this4 = this;

      if (this.authenticate) {
        localStorage.removeItem("type");

        var data = {
          type: type,
          duration: duration,
          level: this.level,
          subType: "entrepreneur"
        };

        axios.post("/api/usersubscription", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 200) {
            localStorage.setItem("type", "entrepreneur");
            window.location.href = response.data;
          }
        }).catch(function (error) {
          console.log(error.response.data.message);
          localStorage.removeItem("type");
          if (error.response.data.message === "Unauthenticated.") {
            _this4.$router.push({
              name: "auth",
              params: { name: "login" },
              query: { redirect: "user-subscription" }
            });
          }
        });
      } else {
        localStorage.removeItem("type");
        this.$router.replace({
          name: "auth",
          params: { name: "login" },
          query: { redirect: "user-subscription" }
        });
      }
    }
  }
});

/***/ }),

/***/ 1584:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container-fluid", class: { showModal: _vm.half } },
    [
      _vm.showMessage
        ? _c("div", { staticClass: "messageBox" }, [
            _c("div", { staticClass: "closing", on: { click: _vm.closeBox } }),
            _vm._v(" "),
            _c("div", { staticClass: "message-mini-box" }, [
              _c(
                "span",
                { staticClass: "close", on: { click: _vm.closeBox } },
                [
                  _c("i", {
                    staticClass: "fa fa-times-circle-o text-main",
                    attrs: { "aria-hidden": "true" }
                  })
                ]
              ),
              _vm._v(" "),
              _vm.accountMessage
                ? _c("p", { staticClass: "message-text" }, [
                    _vm._v(
                      "Subscribe to our Accounting package to get full access to our Accounting software."
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.bapMessage
                ? _c("p", { staticClass: "message-text" }, [
                    _vm._v(
                      "Subscribe to our BAP-6 package to get started today."
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("p", [_vm._v("Click outside the box to continue !!")])
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "main-content" }, [
        _vm.showOverlay
          ? _c(
              "div",
              {
                staticClass:
                  "price-overlay d-flex justify-content-center align-items-center"
              },
              [
                _c("div", { staticClass: "p-3 btn-compliment form" }, [
                  _c("i", {
                    staticClass:
                      "fas fa-times-circle closePrice text-white p-2",
                    attrs: { "aria-hidden": "true" },
                    on: { click: _vm.handleOverlay }
                  }),
                  _vm._v(" "),
                  _c("form", [
                    _c("div", { staticClass: "form-group" }, [
                      _c(
                        "label",
                        {
                          staticClass: "toCaps oPlan mb-4",
                          attrs: { for: "" }
                        },
                        [_vm._v(_vm._s(_vm.plan) + " plan")]
                      ),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted  text-center" }, [
                        _vm._v("\n              Duration :\n              "),
                        _c("strong", { staticClass: "toCaps text-white" }, [
                          _vm._v(_vm._s(_vm.duration))
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "oPrice mb-4" }, [
                      _vm._v("₦" + _vm._s(_vm.price) + ".00")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "my-3" }, [
                      _c(
                        "button",
                        {
                          staticClass:
                            "elevated_btn elevated_btn_sm mx-auto text-main",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.subscribe(_vm.plan, _vm.duration)
                            }
                          }
                        },
                        [_vm._v("Pay")]
                      )
                    ])
                  ])
                ])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("h3", { staticClass: "desc_header text-center mb-5" }, [
          _vm._v("Pricing Plans")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "duration" },
          [
            _c("span", [_vm._v("Monthly")]),
            _vm._v(" "),
            _c("Toggle", {
              staticClass: "px-2",
              on: { handleToggle: _vm.handleToggle }
            }),
            _vm._v(" "),
            _c("span", [
              _vm._v("\n        Annual\n        "),
              _c(
                "span",
                { staticClass: "annual", class: { "annual-faded": !_vm.time } },
                [_vm._v("+2 Months Free")]
              )
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "pricing" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "price_box shadow" }, [
            _c("div", { staticClass: "price_name" }, [_vm._v("ESSENTIAL")]),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "price my-3" }, [
              !_vm.time
                ? _c("span", { staticClass: "myPrice" }, [
                    _vm._v("\n            ₦3,600\n            "),
                    _c("small", [_vm._v("/month")])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.time
                ? _c("span", { staticClass: "myPrice" }, [
                    _vm._v("\n            ₦3,000\n            "),
                    _c("small", [_vm._v("/month billed annually")])
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "text-center my-3" }, [
              _vm.currentSub < 2 || _vm.currentSub == 4
                ? _c(
                    "button",
                    {
                      staticClass: "button-blue mx-auto",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.selectPackage("essential")
                        }
                      }
                    },
                    [_vm._v("Select")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentSub == 2
                ? _c(
                    "button",
                    {
                      staticClass: "button-blue mx-auto",
                      attrs: { type: "button" }
                    },
                    [_vm._v("Subscribed")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentSub == 3
                ? _c(
                    "button",
                    {
                      staticClass: "button-blue mx-auto",
                      attrs: { type: "button" }
                    },
                    [_vm._v("Unavailable")]
                  )
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "price_box shadow" }, [
            _c("div", { staticClass: "price_name" }, [_vm._v("BIZGURUH")]),
            _vm._v(" "),
            _vm._m(2),
            _vm._v(" "),
            _c("div", { staticClass: "price my-3" }, [
              !_vm.time
                ? _c("span", { staticClass: "myPrice" }, [
                    _vm._v("\n            ₦5,000\n            "),
                    _c("small", [_vm._v("/month")])
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.time
                ? _c("span", { staticClass: "myPrice" }, [
                    _vm._v("\n            ₦4,167\n            "),
                    _c("small", [_vm._v("/month billed annually")])
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "text-center my-3" }, [
              _vm.currentSub < 3 || _vm.currentSub == 4
                ? _c(
                    "button",
                    {
                      staticClass: "button-blue mx-auto",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.selectPackage("bizguruh")
                        }
                      }
                    },
                    [_vm._v("Select")]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.currentSub == 3
                ? _c(
                    "button",
                    {
                      staticClass: "button-blue mx-auto",
                      attrs: { type: "button" }
                    },
                    [_vm._v("Subscribed")]
                  )
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "bap_box shadow mx-auto mt-5" }, [
          _c("div", { staticClass: "price_name" }, [_vm._v("BAP-6")]),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _vm._m(4),
          _vm._v(" "),
          _c("div", { staticClass: "text-center py-3" }, [
            _vm.currentSub < 4
              ? _c(
                  "button",
                  {
                    staticClass: "button-blue mx-auto",
                    attrs: { type: "button" },
                    on: {
                      click: function($event) {
                        return _vm.selectPackage("bap")
                      }
                    }
                  },
                  [_vm._v("Select")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.currentSub == 4
              ? _c(
                  "button",
                  {
                    staticClass: "button-blue mx-auto",
                    attrs: { type: "button", disabled: "" }
                  },
                  [_vm._v("Subscribed")]
                )
              : _vm._e()
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "price_box shadow" }, [
      _c("div", { staticClass: "price_name" }, [_vm._v("COMMUNITY MEMBER")]),
      _vm._v(" "),
      _c("ul", [
        _c("li", [_vm._v("Access to select business resources")]),
        _vm._v(" "),
        _c("li", [
          _vm._v("\n            Exclusive BizGuruh Library\n            "),
          _c("small", { staticClass: "bold" }, [_vm._v("(Free Trial)")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "price my-3" }, [_vm._v("Free")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("Access to curated Business Insights and Resources")]),
      _vm._v(" "),
      _c("li", [_vm._v("Follow and engage with Industry Experts")]),
      _vm._v(" "),
      _c("li", [_vm._v("Exclusive BizGuruh Library")]),
      _vm._v(" "),
      _c("li", [_vm._v("Weekly Newsletter")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          "Subscribe to BizGuruh’s 6-week Business Acceleration Program (BAP-6)"
        )
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("Access to Community & Partner Events")]),
      _vm._v(" "),
      _c("li", [_vm._v("Weekly Newsletter")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          "Dedicated subject matter and industry experts who will work with you to develop a business roadmap that is structured to be fulfilling and profitable, even whilst serving others."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Develop and deploy strategies that best align your resources and capabilities to the requirements of your business operating environment and overall vision."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Templates/checklists/guides on your custom dashboard for easy reference at any time!"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Become part of a community of like-minded entrepreneurs and experts."
        )
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("Access to community and partner events.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "price my-3 myPrice" }, [
      _vm._v("\n        ₦30,000\n        "),
      _c("small", [_vm._v("One-Time Payment")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-53d780b8", module.exports)
  }
}

/***/ }),

/***/ 593:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1581)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1583)
/* template */
var __vue_template__ = __webpack_require__(1584)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-53d780b8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userProfileSubscriptionComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-53d780b8", Component.options)
  } else {
    hotAPI.reload("data-v-53d780b8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 870:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(871)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(873)
/* template */
var __vue_template__ = __webpack_require__(874)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4f25ce06"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/toggle.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4f25ce06", Component.options)
  } else {
    hotAPI.reload("data-v-4f25ce06", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 871:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(872);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4407b836", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue", function() {
     var newContent = require("!!../../../node_modules/css-loader/index.js!../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4f25ce06\",\"scoped\":true,\"hasInlineConfig\":true}!../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./toggle.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 872:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.switch[data-v-4f25ce06] {\n  position: relative;\n  display: inline-block;\n  width: 60px;\n  height: 34px;\n}\n\n/* Hide default HTML checkbox */\n.switch input[data-v-4f25ce06] {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n\n/* The slider */\n.slider[data-v-4f25ce06] {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: hsl(207, 46%, 20%);\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\n.slider[data-v-4f25ce06]:before {\n  position: absolute;\n  content: \"\";\n  height: 26px;\n  width: 26px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  -webkit-transition: 0.4s;\n  transition: 0.4s;\n}\ninput:checked + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:focus + .slider[data-v-4f25ce06] {\n   background-color: hsl(207, 46%, 20%);\n}\ninput:checked + .slider[data-v-4f25ce06]:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px);\n}\n\n/* Rounded sliders */\n.slider.round[data-v-4f25ce06] {\n  border-radius: 34px;\n}\n.slider.round[data-v-4f25ce06]:before {\n  border-radius: 50%;\n}\n@media(max-width:425px){\n.switch[data-v-4f25ce06] {\n\n  width: 50px;\n  height: 24px;\n}\n.slider[data-v-4f25ce06]:before {\n\n  height: 16px;\n  width: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            time: false
        };
    },

    watch: {
        'time': 'change'
    },
    methods: {
        change: function change() {
            this.$emit('handleToggle', this.time);
        }
    }
});

/***/ }),

/***/ 874:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("label", { staticClass: "switch" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.time,
            expression: "time"
          }
        ],
        attrs: { type: "checkbox" },
        domProps: {
          checked: Array.isArray(_vm.time)
            ? _vm._i(_vm.time, null) > -1
            : _vm.time
        },
        on: {
          change: function($event) {
            var $$a = _vm.time,
              $$el = $event.target,
              $$c = $$el.checked ? true : false
            if (Array.isArray($$a)) {
              var $$v = null,
                $$i = _vm._i($$a, $$v)
              if ($$el.checked) {
                $$i < 0 && (_vm.time = $$a.concat([$$v]))
              } else {
                $$i > -1 &&
                  (_vm.time = $$a.slice(0, $$i).concat($$a.slice($$i + 1)))
              }
            } else {
              _vm.time = $$c
            }
          }
        }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "slider round" })
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4f25ce06", module.exports)
  }
}

/***/ })

});