webpackJsonp([117],{

/***/ 1424:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1425);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("314418e6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3eb53e3a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCheckoutComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3eb53e3a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userCheckoutComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1425:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nul[data-v-3eb53e3a],ol[data-v-3eb53e3a],li[data-v-3eb53e3a]{\n    list-style: none;\n}\n.prodTit[data-v-3eb53e3a]{\n    width:50%;\n}\n.course-Detail[data-v-3eb53e3a]{\n    background:#f7f8fa;\n    min-height: 100vh;\n    display:-webkit-box;\n    display:-ms-flexbox;\n    display:flex;\n}\n.bill[data-v-3eb53e3a] {\n        padding: 12px 0 2px 20px;\n        margin-top:0;\n}\n.addressDiv[data-v-3eb53e3a] {\n    border: 1px solid #ffffff;\n}\n.addressAdd[data-v-3eb53e3a] {\n        margin: 5px;\n        padding:0 10px;\n}\n.billingAddress[data-v-3eb53e3a] {\n        background-color: #a6c4dd !important;\n        color: #ffffff !important;\n}\n.sumCart[data-v-3eb53e3a] {\n       \n        padding: 20px 0;\n}\n#exampleModal[data-v-3eb53e3a] {\n        z-index: 10001;\n}\n.totalCheckout[data-v-3eb53e3a] {\n        padding: 20px 0;\n        font-size: 25px;\n        font-weight: bold;\n}\n.itemCheckout[data-v-3eb53e3a] {\n         width:100%;\n        color: #ffffff !important;\n        text-align: center;\n}\nli[data-v-3eb53e3a]{\n       margin-bottom: 20px;\n}\n.itemList[data-v-3eb53e3a] {\n    \n        padding: 15px 5px;\n        display:-webkit-box;\n        display:-ms-flexbox;\n        display:flex;\n}\n.addAddress[data-v-3eb53e3a] {\n        color: #000000;\n        background-color: #ffffff !important;\n        border: 0px solid #ffffff !important;\n        text-transform: capitalize;\n}\n.pay[data-v-3eb53e3a] {\n        width: 40%;\n        margin-left: auto;\n        margin-right: auto;\n}\n.imgCheckout[data-v-3eb53e3a]{\n    width: 50px;\n    height: 80px;\n    padding: 0 0 10px 0;\n}\n.nameDiv[data-v-3eb53e3a] {\n        padding: 0;\n        margin:0 ;\n        font-size: 16px;\n}\n.nameDivs[data-v-3eb53e3a] {\n        padding: 0;\n        margin:0 ;\n}\n.editName[data-v-3eb53e3a] {\n        text-align: right;\n}\n.editMo[data-v-3eb53e3a] {\n        margin-right: 10px;\n}\n.text[data-v-3eb53e3a] {\n        border: 1px solid #a3c2dc;\n        padding: 20px;\n        margin-bottom: 0;\n}\n.btn-o[data-v-3eb53e3a] {\n        margin-top: 0;\n        text-align: center;\n        border-top: 1px solid #ffffff;\n        padding: 20px;\n}\n.mob1[data-v-3eb53e3a]{\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-pack:center;\n            -ms-flex-pack:center;\n                justify-content:center;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        background-color: hsl(207, 46%, 20%) !important;\n        text-align:center;\n        padding:20px;\n}\n@media(max-width:425px){\n.sumCart[data-v-3eb53e3a] {\n       \n       font-size:16px;\n}\n.bill[data-v-3eb53e3a]{\n            font-size:16px;\n}\n.mob1[data-v-3eb53e3a]{\n            margin-bottom:20px;\n            width:95% !important;\n            height:100%;\n}\n.mob2[data-v-3eb53e3a]{\n            padding:20px;\n}\n.nameDiv[data-v-3eb53e3a] {\n            margin:0;\n            padding:0;\n}\n.nameDivs[data-v-3eb53e3a]{\n             margin:0;\n             padding:0;\n}\n.course-Detail[data-v-3eb53e3a] {\n     \n        padding:20px 0 0;\n}\nol[data-v-3eb53e3a], ul[data-v-3eb53e3a]{\n    padding-left:0;\n}\n.itemList[data-v-3eb53e3a] {\n    border-bottom: 1px solid #ffffff;\n    padding-top: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: space-evenly;\n        -ms-flex-pack: space-evenly;\n            justify-content: space-evenly;\n}\n.imgCheckout[data-v-3eb53e3a]{\n    width: 60px;\n    height: 80px;\n    padding: 0 0 10px 0;\n}\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-checkout-component",
    data: function data() {
        return {
            payment: 'Pay',
            addressCheck: '',
            id: '',
            radioCheck: '',
            addressId: 0,
            products: [],
            email: '',
            totalAmount: '',
            shippingAmount: '',
            subAmount: '',
            modal: true,
            name: '',
            fade: true,
            userDetails: [],
            editTab: false,
            show: false,
            token: '',
            userDetail: {
                firstName: '',
                lastName: '',
                phoneNo: '',
                address: '',
                state: '',
                city: '',
                country: '',
                user_id: ''
            },
            redirect: false
        };
    },
    mounted: function mounted() {
        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user != null) {
            this.email = user.email;
            this.name = user.name;
            this.token = user.access_token;
            this.userDetail.user_id = user.id;

            this.transactionGetCart('id');

            // axios.get(`/api/user-details/${this.userDetail.user_id}`, { headers: {"Authorization" : `Bearer ${this.token}`} })

            //     .then((response) => {
            //         if(response.status === 200) {
            //             response.data[0].addressFirstCheck = 1;
            //             console.log(response);
            //             this.addressId = response.data[0].id;
            //             this.radioCheck = response.data[0].id;
            //             console.log(response.data);
            //             this.userDetails = response.data;
            //             this.transactionGetCart(this.addressId);
            //         }
            //     }).catch((error) => {
            //     console.log(error);
            // });
        } else {
            this.$router.push({ name: 'auth', query: { redirect: 'product/checkout' } });
        }
    },

    methods: {
        transactionGetCart: function transactionGetCart(id) {
            var _this = this;

            var customerUser = JSON.parse(localStorage.getItem('authUser')) ? JSON.parse(localStorage.getItem('authUser')) : 0;
            var sessionCart = JSON.parse(localStorage.getItem('userCart')) ? JSON.parse(localStorage.getItem('userCart')) : 0;
            var itemArray = [];
            if (sessionCart instanceof Array) {
                sessionCart.forEach(function (item) {
                    itemArray.push({
                        id: item.productId,
                        quantity: item.quantity,
                        prodType: item.prodType,
                        mediaId: item.mediaId
                    });
                });
            }

            var cart = {
                customerId: customerUser.id,
                itemId: itemArray,
                sessionCheck: customerUser.id === undefined ? 'NC' : 'C',
                addressId: id
            };

            axios.post('/api/post/cart/bk', JSON.parse(JSON.stringify(cart))).then(function (response) {
                if (response.status === 200) {

                    _this.products = response.data.item;

                    _this.totalAmount = response.data.totalAmount;
                    _this.shippingAmount = response.data.shippingPrice;
                    _this.subAmount = response.data.subTotal;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        showModal: function showModal(index, id) {
            var _this2 = this;

            if (index === 'new') {
                this.fade = false;
                this.editTab = false;
                this.userDetail.firstName = this.userDetail.lastName = this.userDetail.phoneNo = this.userDetail.address = this.userDetail.state = this.userDetail.city = this.userDetail.country = '';
            } else {
                this.fade = false;
                this.editTab = true;
                this.id = id;
                axios.get('/api/user-detail/' + id + '/edit', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this2.userDetail.firstName = response.data.firstName;
                        _this2.userDetail.lastName = response.data.lastName;
                        _this2.userDetail.phoneNo = response.data.phoneNo;
                        _this2.userDetail.address = response.data.address;
                        _this2.userDetail.state = response.data.state;
                        _this2.userDetail.city = response.data.city;
                        _this2.userDetail.country = response.data.country;
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        addAddress: function addAddress() {
            var _this3 = this;

            if (this.editTab) {

                axios.put('/api/user-detail/' + this.id, JSON.parse(JSON.stringify(this.userDetail)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        _this3.$toasted.success('Address successfully updated');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {

                axios.post('/api/user-detail', JSON.parse(JSON.stringify(this.userDetail)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 201) {
                        _this3.$toasted.success('Address successfully added');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        deleteAddress: function deleteAddress(index, id) {
            var _this4 = this;

            axios.delete('/api/user-detail/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this4.userDetails.splice(index, 1);
                    _this4.$toasted.success('Successfully deleted');
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        pay: function pay() {
            this.payment = 'Processing payment..';
            var data = {
                deliveryId: this.addressId,
                email: this.email,
                totalAmount: this.totalAmount,
                paidAmount: this.totalAmount,
                item: this.products,
                paymentType: 'paystack'
            };

            axios.post('/api/order', JSON.parse(JSON.stringify(data)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {

                    window.location.href = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAddressId: function getAddressId(id) {
            // this.addressId = id;
            this.transactionGetCart(id);
        }
    }

});

/***/ }),

/***/ 1427:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "course-Detail  justify-content-center align-items-center "
    },
    [
      _c("div", { staticClass: "w-25  mob1 shadow" }, [
        _c("div", { staticClass: "carts itemCheckout" }, [
          _c("h2", { staticClass: "sumCart" }, [_vm._v("Payment Summary")]),
          _vm._v(" "),
          _vm.products.length
            ? _c(
                "ul",
                { staticClass: "animated fadeIn" },
                [
                  _vm._l(_vm.products, function(product, index) {
                    return _c("li", { key: index, staticClass: "itemList" }, [
                      _c("div", { staticClass: "mr-4" }, [
                        _vm._v(_vm._s(product.title))
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c("span", [
                          _vm._v("₦" + _vm._s(product.amount) + ".00")
                        ])
                      ]),
                      _vm._v(" "),
                      product.shippingPrice
                        ? _c("div", [
                            _c("span", [_vm._v("ShippingPrice: ")]),
                            _c("span", [
                              _vm._v(
                                "₦" + _vm._s(product.shippingPrice) + ".00"
                              )
                            ])
                          ])
                        : _vm._e()
                    ])
                  }),
                  _vm._v(" "),
                  _c("li", { staticClass: "subTotal" }, [
                    _c("div", [
                      _c("span", [_vm._v("Sub Total: ")]),
                      _vm._v(" "),
                      _c("span", [_vm._v("₦" + _vm._s(_vm.subAmount) + ".00")])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm.shippingAmount !== 0
                    ? _c("li", { staticClass: "shipping" }, [
                        _c("div", [
                          _c("span", [_vm._v("Shipping: ")]),
                          _vm._v(" "),
                          _c("span", [
                            _vm._v("₦" + _vm._s(_vm.shippingAmount) + ".00")
                          ])
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("li", { staticClass: "totalCheckout" }, [
                    _c("div", [
                      _c("span", [_vm._v("Total: ")]),
                      _vm._v(" "),
                      _c("span", [
                        _vm._v("₦" + _vm._s(_vm.totalAmount) + ".00")
                      ])
                    ])
                  ])
                ],
                2
              )
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mt-3" }, [
          _c(
            "button",
            {
              staticClass: "elevated_btn elevated_btn_sm text-main ",
              attrs: { id: "pay" },
              on: { click: _vm.pay }
            },
            [_vm._v(_vm._s(_vm.payment))]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3eb53e3a", module.exports)
  }
}

/***/ }),

/***/ 559:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1424)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1426)
/* template */
var __vue_template__ = __webpack_require__(1427)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3eb53e3a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userCheckoutComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3eb53e3a", Component.options)
  } else {
    hotAPI.reload("data-v-3eb53e3a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});