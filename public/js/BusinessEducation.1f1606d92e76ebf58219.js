webpackJsonp([143],{

/***/ 1875:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1876);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7ffc99da", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45a18d94\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./businessEducation.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45a18d94\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./businessEducation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1876:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container-fluid[data-v-45a18d94] {\n  padding-top: 50px;\n  min-height: 100vh;\n  background-color: #f7f8fa;\n}\n.bap_box[data-v-45a18d94] {\n  display: grid;\n  padding: 10px;\n  width: 100%;\n  height: 100%;\n  grid-column-gap: 15px;\n  grid-row-gap: 30px;\n}\n.grid_box[data-v-45a18d94] {\n  height: 100px;\n  position: relative;\n  border: 1px solid #f7f8fa;\n  text-align: center;\n  border-radius: 4px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(#414d57),\n    color-stop(#52616e),\n    color-stop(#647786),\n    color-stop(#778c9f),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#8aa3b8),\n    color-stop(#778c9f),\n    color-stop(#647786),\n    color-stop(#52616e),\n    to(#414d57)\n  );\n  background-image: linear-gradient(\n    to right top,\n    #414d57,\n    #52616e,\n    #647786,\n    #778c9f,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #8aa3b8,\n    #778c9f,\n    #647786,\n    #52616e,\n    #414d57\n  );\n  -webkit-transition: all 2s;\n  transition: all 2s;\n}\n.grid_text[data-v-45a18d94] {\n  text-align: center;\n  position: absolute;\n  font-weight: bold;\n  font-size: 18px;\n  color: white;\n  text-transform: capitalize;\n  font-family: \"Josefin Sans\";\n}\n.grid_box[data-v-45a18d94]:hover {\n  background-image: -webkit-gradient(\n    linear,\n    right bottom, left top,\n    from(#a4c2db),\n    color-stop(#90abc1),\n    color-stop(#7d94a7),\n    color-stop(#6a7e8e),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#586876),\n    color-stop(#6a7e8e),\n    color-stop(#7d94a7),\n    color-stop(#90abc1),\n    to(#a4c2db)\n  );\n  background-image: linear-gradient(\n    to left top,\n    #a4c2db,\n    #90abc1,\n    #7d94a7,\n    #6a7e8e,\n    #586876,\n    #586876,\n    #586876,\n    #586876,\n    #6a7e8e,\n    #7d94a7,\n    #90abc1,\n    #a4c2db\n  );\n}\nh3[data-v-45a18d94] {\n  font-size: 18.5px;\n}\n@media only screen and (min-width: 768px) {\n.container-fluid[data-v-45a18d94] {\n    padding: 30px 20px;\n}\n.bap_box[data-v-45a18d94] {\n    grid-template-columns: auto auto auto;\n}\n.grid_text[data-v-45a18d94] {\n    font-size: 24px;\n}\n}\n@media only screen and (min-width: 1024px) {\n.container-fluid[data-v-45a18d94] {\n    padding: 30px 40px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      current_month: 0,
      forms: []
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {});
  },
  mounted: function mounted() {
    this.getRecord();
    this.getForms();
  },

  methods: {
    gotoTemplate: function gotoTemplate(id) {
      this.$router.push("/entrepreneur/bap/form-question/" + id);
    },
    getForms: function getForms() {
      var _this = this;

      var name = this.$route.query.name;
      axios.get("/api/get-bap-forms/" + name).then(function (res) {
        if (res.status == 200) {
          _this.forms = res.data;
        }
      }).catch();
    },
    switchTemp: function switchTemp() {
      this.one = !this.one;
      window.scrollTo(0, 0);
    },
    getRecord: function getRecord() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user != null) {
        axios.get("/api/get-active-record", {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this2.current_month = response.data.current_month;
        });
      }
    }
  }
});

/***/ }),

/***/ 1878:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container-fluid" },
    [
      _c(
        "router-link",
        { staticClass: "back mb-5", attrs: { to: "/entrepreneur/bap" } },
        [
          _c("i", {
            staticClass: "fa fa-long-arrow-left",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" Back to BAP-6\n  ")
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "first" }, [
        _c("h3", { staticClass: "mb-3" }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "bap_box" },
          [
            _c(
              "router-link",
              {
                staticClass: "grid_box shadow-sm",
                attrs: { to: "/entrepreneur/bap/model" }
              },
              [
                _c("p", { staticClass: "grid_text" }, [
                  _vm._v("My Business Model")
                ])
              ]
            ),
            _vm._v(" "),
            _vm._l(_vm.forms, function(form, index) {
              return _vm.forms.length
                ? _c(
                    "div",
                    {
                      key: index,
                      staticClass: "grid_box shadow-sm",
                      on: {
                        click: function($event) {
                          return _vm.gotoTemplate(form.id)
                        }
                      }
                    },
                    [
                      _c("p", { staticClass: "grid_text" }, [
                        _vm._v(_vm._s(form.form_title.toLowerCase()))
                      ])
                    ]
                  )
                : _vm._e()
            })
          ],
          2
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-45a18d94", module.exports)
  }
}

/***/ }),

/***/ 647:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1875)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1877)
/* template */
var __vue_template__ = __webpack_require__(1878)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-45a18d94"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/businessEducation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45a18d94", Component.options)
  } else {
    hotAPI.reload("data-v-45a18d94", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});