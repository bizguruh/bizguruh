webpackJsonp([176],{

/***/ 1316:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1317);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("1c21afd4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b81d3d8c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminBizguruhProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b81d3d8c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminBizguruhProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1317:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-b81d3d8c] {\n    height: 100vh;\n}\n.main-page[data-v-b81d3d8c] {\n    padding: 40px 20px;\n    max-height: 100vh;\n    height: 100vh;\n    overflow: scroll;\n}\n.rowProduct[data-v-b81d3d8c] {\n    margin-bottom: 50px;\n}\nth[data-v-b81d3d8c] {\n    background-color: #f3f3f3;\n    padding: 40px 10px !important;\n    color: #000000;\n}\ntd[data-v-b81d3d8c] {\n    color: #a4a4a4 !important;\n    padding: 30px 10px !important;\n}\n.dive[data-v-b81d3d8c] {\n    text-align: right;\n}\n.btn-custom-market[data-v-b81d3d8c] {\n    overflow: hidden;\n}\n.btn-custom-market label[data-v-b81d3d8c]:hover {\n    cursor: pointer;\n}\n.btn-custom-market input[data-v-b81d3d8c]:checked {\n    background-color: #A5DC86;\n    -webkit-box-shadow: none;\n    box-shadow: none;\n}\n.btn-custom-market label[data-v-b81d3d8c]:first-of-type {\n    border-radius: 4px 0 0 4px;\n}\n.btn-custom-market label[data-v-b81d3d8c]:last-of-type {\n    border-radius: 0 4px 4px 0;\n}\n.btn-custom-market label[data-v-b81d3d8c] {\n    float: left;\n    display: inline-block;\n    width: auto;\n    background-color: #e4e4e4;\n    color: rgba(0, 0, 0, 0.6);\n    font-size: 14px;\n    font-weight: normal;\n    text-align: center;\n    text-shadow: none;\n    padding: 6px 14px;\n    border: 1px solid rgba(0, 0, 0, 0.2);\n    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);\n    -webkit-transition: all 0.1s ease-in-out;\n    transition:         all 0.1s ease-in-out;\n}\n.btn-below[data-v-b81d3d8c] {\n    padding-bottom: 10px;\n}\n.btn-custom-market input[data-v-b81d3d8c] {\n    position: absolute !important;\n    clip: rect(0, 0, 0, 0);\n    height: 1px;\n    width: 1px;\n    border: 0;\n    overflow: hidden;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-bizguruh-product-component",
    data: function data() {
        return {
            products: [],
            overallCheck: false,
            checkIn: false,
            verified: 'All',
            sponsor: '',
            isActive: true,
            pagination: {}
        };
    },
    mounted: function mounted() {
        var _this = this;

        var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
        if (authAdmin != null) {
            axios.get('/api/admin/get/products', { headers: { "Authorization": 'Bearer ' + authAdmin.access_token } }).then(function (response) {
                console.log(response);
                if (response.data.data.length > 0) {
                    var emptyArray = [];

                    response.data.data.forEach(function (item) {
                        switch (item.prodType) {
                            case 'Books':
                                _this.$set(item.books, 'verify', item.verify);
                                _this.$set(item.books, 'uniId', item.id);
                                _this.$set(item.books, 'created_at', item.myDate);
                                emptyArray.push(item.books);
                                break;
                            case 'White Paper':
                                _this.$set(item.whitePaper, 'verify', item.verify);
                                _this.$set(item.whitePaper, 'uniId', item.id);
                                _this.$set(item.whitePaper, 'created_at', item.myDate);
                                emptyArray.push(item.whitePaper);
                                break;
                            case 'Market Reports':
                                _this.$set(item.marketReport, 'verify', item.verify);
                                _this.$set(item.marketReport, 'uniId', item.id);
                                _this.$set(item.marketReport, 'created_at', item.myDate);
                                emptyArray.push(item.marketReport);
                                break;
                            case 'Market Research':
                                _this.$set(item.marketResearch, 'verify', item.verify);
                                _this.$set(item.marketResearch, 'uniId', item.id);
                                _this.$set(item.marketResearch, 'created_at', item.myDate);
                                emptyArray.push(item.marketResearch);
                                break;
                            case 'Webinar':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Podcast':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Videos':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Journals':
                                _this.$set(item.journal, 'verify', item.verify);
                                _this.$set(item.journal, 'uniId', item.id);
                                _this.$set(item.journal, 'created_at', item.myDate);
                                emptyArray.push(item.journal);
                                break;
                            case 'Courses':
                                _this.$set(item.courses, 'verify', item.verify);
                                _this.$set(item.courses, 'uniId', item.id);
                                _this.$set(item.courses, 'created_at', item.myDate);
                                emptyArray.push(item.courses);
                                break;
                            case 'Events':
                                _this.$set(item.events, 'verify', item.verify);
                                _this.$set(item.events, 'uniId', item.id);
                                _this.$set(item.events, 'created_at', item.myDate);
                                emptyArray.push(item.events);
                                break;
                            case 'Articles':
                                _this.$set(item.articles, 'verify', item.verify);
                                _this.$set(item.articles, 'uniId', item.id);
                                _this.$set(item.articles, 'created_at', item.myDate);
                                emptyArray.push(item.articles);
                                break;
                            default:
                                return false;
                        }
                    });
                    _this.products = emptyArray;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },

    methods: {
        overallCheckBox: function overallCheckBox() {
            var _this2 = this;

            //  this.checkIn = !this.checkIn;
            this.products.forEach(function (item) {
                if (!('itemClick' in item)) {
                    _this2.$set(item, 'itemClick', true);
                } else if (_this2.overallCheck) {
                    _this2.$set(item, 'itemClick', true);
                } else {
                    _this2.$set(item, 'itemClick', false);
                }
            });
        },
        deleteItem: function deleteItem() {
            var _this3 = this;

            var deletedArray = [];
            this.products.forEach(function (item) {
                if (item.itemClick) {
                    console.log(item);
                    console.log('yes');
                    deletedArray.push(item.uniId);
                }
            });
            if (deletedArray.length === 0) {
                this.$toasted.error('You must select at least one product to delete');
            } else {
                console.log(deletedArray);
                axios.post('/api/delete/many-products', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        console.log(response);
                        for (var i = 0; i < deletedArray.length; i++) {
                            console.log(deletedArray[i]);
                            _this3.products.splice(i, 1);
                        }
                        _this3.$toasted.success('All selected items deleted');
                    } else {
                        _this3.$toasted.error('Unable to delete');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    },
    computed: {
        showData: function showData() {
            var verify = this.verified;
            var sponsor = this.sponsor;
            if (verify === 'All') {
                return this.products;
            } else if (verify === "1" || verify === "0") {
                return this.products.filter(function (value) {
                    return value.verify === parseInt(verify);
                });
            } else if (verify === 'PN') {
                return this.products.filter(function (value) {
                    return value.editFlag === 'Y';
                });
            } else {
                return this.products.filter(function (value) {
                    return value.sponsor === verify;
                });
            }
        }
    }
});

/***/ }),

/***/ 1319:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-8" }, [
          _c("div", { staticClass: "btn-custom-market" }, [
            _c("label", { class: { active: _vm.isActive } }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "All" },
                domProps: { checked: _vm._q(_vm.verified, "All") },
                on: {
                  change: function($event) {
                    _vm.verified = "All"
                  }
                }
              }),
              _vm._v("All")
            ]),
            _vm._v(" "),
            _c("label", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "1" },
                domProps: { checked: _vm._q(_vm.verified, "1") },
                on: {
                  change: function($event) {
                    _vm.verified = "1"
                  }
                }
              }),
              _vm._v("Verified")
            ]),
            _vm._v(" "),
            _c("label", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "0" },
                domProps: { checked: _vm._q(_vm.verified, "0") },
                on: {
                  change: function($event) {
                    _vm.verified = "0"
                  }
                }
              }),
              _vm._v("Unverified")
            ]),
            _vm._v(" "),
            _c("label", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "Y" },
                domProps: { checked: _vm._q(_vm.verified, "Y") },
                on: {
                  change: function($event) {
                    _vm.verified = "Y"
                  }
                }
              }),
              _vm._v("Sponsor")
            ]),
            _vm._v(" "),
            _c("label", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "N" },
                domProps: { checked: _vm._q(_vm.verified, "N") },
                on: {
                  change: function($event) {
                    _vm.verified = "N"
                  }
                }
              }),
              _vm._v("Unsponsor")
            ]),
            _vm._v(" "),
            _c("label", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.verified,
                    expression: "verified"
                  }
                ],
                attrs: { type: "radio", value: "PN" },
                domProps: { checked: _vm._q(_vm.verified, "PN") },
                on: {
                  change: function($event) {
                    _vm.verified = "PN"
                  }
                }
              }),
              _vm._v("Pending Update")
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row rowProduct" }, [
        _c("div", { staticClass: "col-md-12 dive" }, [
          _c(
            "button",
            { staticClass: "btn btn-create" },
            [
              _c(
                "router-link",
                {
                  attrs: { tag: "a", to: { name: "adminProductAddCategory" } }
                },
                [
                  _vm._v(
                    "\n                        Create New Product\n                    "
                  )
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("button", { staticClass: "btn btn-danger" }, [
            _vm._v("\n                    Delete\n                ")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("table", { staticClass: "table" }, [
        _c("thead", [
          _c("tr", [
            _c("th", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.overallCheck,
                    expression: "overallCheck"
                  }
                ],
                attrs: { type: "checkbox" },
                domProps: {
                  checked: Array.isArray(_vm.overallCheck)
                    ? _vm._i(_vm.overallCheck, null) > -1
                    : _vm.overallCheck
                },
                on: {
                  change: [
                    function($event) {
                      var $$a = _vm.overallCheck,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && (_vm.overallCheck = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.overallCheck = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.overallCheck = $$c
                      }
                    },
                    _vm.overallCheckBox
                  ]
                }
              })
            ]),
            _vm._v(" "),
            _c("th", [_vm._v("Identification")]),
            _vm._v(" "),
            _c("th", [_vm._v("Details")]),
            _vm._v(" "),
            _c("th", [_vm._v("Status")]),
            _vm._v(" "),
            _c("th", [_vm._v("Created On")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.showData, function(product, index) {
            return _vm.products.length > 0
              ? _c("tr", [
                  _c("td", [
                    _c("input", {
                      attrs: { type: "checkbox" },
                      domProps: { checked: product.itemClick },
                      on: {
                        change: function($event) {
                          product.itemClick = !product.itemClick
                          _vm.overallCheck = false
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(index + 1))]),
                  _vm._v(" "),
                  product.articleTitle
                    ? _c("td", [_vm._v(_vm._s(product.articleTitle))])
                    : _c("td", [_vm._v(_vm._s(product.title))]),
                  _vm._v(" "),
                  product.verify === 1
                    ? _c("td", [_vm._v("Active")])
                    : _c("td", [_vm._v("Pending")]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(
                      _vm._s(_vm._f("moment")(product.created_at.date, "L"))
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "linktoproduct",
                          attrs: {
                            to: {
                              name: "adminProductshowSave",
                              params: { id: product.uniId }
                            }
                          }
                        },
                        [_c("i", { staticClass: "fa fa-arrow-right" })]
                      )
                    ],
                    1
                  )
                ])
              : _c("tr", [_vm._v("No Product")])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b81d3d8c", module.exports)
  }
}

/***/ }),

/***/ 535:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1316)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1318)
/* template */
var __vue_template__ = __webpack_require__(1319)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b81d3d8c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminBizguruhProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b81d3d8c", Component.options)
  } else {
    hotAPI.reload("data-v-b81d3d8c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});