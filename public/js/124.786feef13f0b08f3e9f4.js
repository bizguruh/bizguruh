webpackJsonp([124],{

/***/ 1819:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1820);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7c63a266", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4230955c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./listDiagnosticsComponents.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4230955c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./listDiagnosticsComponents.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1820:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.containers[data-v-4230955c] {\n  padding: 80px 15px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  min-height: 100vh;\n  width: 100%;\n  background-image: url(\"/images/1.jpg\");\n  background-size: cover;\n  margin-top: 40px;\n}\n.main[data-v-4230955c] {\n  background: white;\n  padding: 30px 30px;\n  border-radius: 4px;\n  width: 60%;\n  min-height: 500px;\n  z-index: 1;\n    text-align: center;\n}\n.overlay[data-v-4230955c] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(111, 148, 250, 0.1);\n}\n.guides[data-v-4230955c]{\n    margin: 40px 0\n}\n.menu-box[data-v-4230955c]{\n     display: grid;\n    grid-template-rows: 400px;\n    grid-template-columns: auto auto auto;\n}\n.d-grid[data-v-4230955c]{\n  display:grid;\n}\n.guide[data-v-4230955c]{\ncursor: pointer;\n}\n@media (max-width: 425px){\n.main[data-v-4230955c]{\n        width: 100%;\n}\n.card-body[data-v-4230955c]{\n      padding:.6rem;\n}\n.container[data-v-4230955c]{\n      margin-top: 0;\n}\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1821:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "list-diagnostics",
  data: function data() {
    return {
      result: [],
      authenticated: false,
      area: this.$route.params.area

    };
  },
  mounted: function mounted() {
    var _this = this;

    var user = localStorage.getItem('authUser');
    if (user !== null) {
      this.authenticated = true;
    }

    axios.get("/api/questionsVerified").then(function (response) {
      _this.result = response.data;
    });
  },


  methods: {
    openLink: function openLink(id) {
      if (this.authenticated) {
        this.$router.push({
          name: 'Checklist',
          params: { id: id }
        });
      } else {
        this.$toasted.error('login to continue');
        this.$router.push({
          name: 'auth',
          params: { name: 'login' },
          query: { check: 'guides' }
        });
      }
    },
    openGym: function openGym(id) {
      if (this.authenticated) {
        this.$router.push({
          name: 'Diagnostics',
          params: { id: id }
        });
      } else {
        this.$toasted.error('login to continue');
        this.$router.push({
          name: 'auth',
          params: { name: 'login' },
          query: { check: 'guides' }
        });
      }
    }
  }
});

/***/ }),

/***/ 1822:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "containers" }, [
    _c("span", { staticClass: "overlay" }),
    _vm._v(" "),
    _vm.result.length > 0
      ? _c("div", { staticClass: "main shadow-lg  animated fadeIn slow" }, [
          _vm.area === "template"
            ? _c("table", { staticClass: "table" }, [
                _vm._m(0),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.result, function(question, index) {
                    return _c("tr", { key: index }, [
                      question.name === "template"
                        ? _c("td", { attrs: { scope: "row" } }, [
                            _c(
                              "span",
                              {
                                staticClass: "text-capitalize guide",
                                on: {
                                  click: function($event) {
                                    return _vm.openLink(question.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(question.title.toLowerCase()))]
                            )
                          ])
                        : _vm._e()
                    ])
                  }),
                  0
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.area === "checklist"
            ? _c("table", { staticClass: "table" }, [
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.result, function(question, index) {
                    return _c("tr", { key: index }, [
                      question.name === "qa"
                        ? _c("td", { attrs: { scope: "row" } }, [
                            _c(
                              "span",
                              {
                                staticClass: "text-capitalize guide",
                                on: {
                                  click: function($event) {
                                    return _vm.openLink(question.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(question.title.toLowerCase()))]
                            )
                          ])
                        : _vm._e()
                    ])
                  }),
                  0
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.area === "guides"
            ? _c("table", { staticClass: "table" }, [
                _vm._m(2),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.result, function(question, index) {
                    return _c("tr", { key: index }, [
                      question.name === "bg"
                        ? _c("td", { attrs: { scope: "row" } }, [
                            _c(
                              "span",
                              {
                                staticClass: "text-capitalize guides",
                                on: {
                                  click: function($event) {
                                    return _vm.openLink(question.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(question.title.toLowerCase()))]
                            )
                          ])
                        : _vm._e()
                    ])
                  }),
                  0
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.area === "brain-gym"
            ? _c("table", { staticClass: "table" }, [
                _vm._m(3),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.result, function(question, index) {
                    return _c("tr", { key: index }, [
                      question.name === "brain-gym"
                        ? _c("td", { attrs: { scope: "row" } }, [
                            _c(
                              "span",
                              {
                                staticClass: "text-capitalize guides",
                                on: {
                                  click: function($event) {
                                    return _vm.openGym(question.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(question.title.toLowerCase()))]
                            )
                          ])
                        : _vm._e()
                    ])
                  }),
                  0
                )
              ])
            : _vm._e()
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center" }, [_vm._v(" Business Plans")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center" }, [_vm._v("Checklist")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center" }, [_vm._v("Business Guides")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center" }, [_vm._v("Case Studies")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4230955c", module.exports)
  }
}

/***/ }),

/***/ 638:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1819)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1821)
/* template */
var __vue_template__ = __webpack_require__(1822)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4230955c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/listDiagnosticsComponents.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4230955c", Component.options)
  } else {
    hotAPI.reload("data-v-4230955c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});