webpackJsonp([126],{

/***/ 1832:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1833);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("31624b6a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c3adf498\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidesComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c3adf498\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./guidesComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1833:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.containers[data-v-c3adf498] {\n  padding: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  min-height: 100vh;\n  width: 100%;\n  background: white;\n}\n.main[data-v-c3adf498] {\n  background: white;\n  padding: 0;\n  border-radius: 4px;\n  width: 100%;\n  min-height: 500px;\n  z-index: 1;\n  display: grid;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 16px;\n  text-align: center;\n}\n.title[data-v-c3adf498]{\n  text-transform:capitalize;\n}\n.custom-control-indicator[data-v-c3adf498]{\n  color:hsl(207, 46%, 20%)\n}\n.next-box[data-v-c3adf498] {\n  margin-top: 50px;\n  width: 100%;\n  min-height: 500px;\n}\n.single-box[data-v-c3adf498] {\n  width: 100%;\n  min-height: 100px;\n  border: 1px solid #eee;\n  text-align: left;\n  padding: 15px 30px;\n}\n.mini-box[data-v-c3adf498] {\n  display: grid;\n  grid-template-rows: 65px;\n  grid-template-columns: auto auto auto;\n  grid-column-gap: 5px;\n  grid-row-gap: 10px;\n  -webkit-box-shadow: 0 0 1px #eee;\n          box-shadow: 0 0 1px #eee;\n  text-align: center;\n}\n.under[data-v-c3adf498] {\n  width: 30px;\n  margin-top: 5px;\n  margin-right: auto;\n  margin-left: 0;\n  border: 0;\n  border-top: 2px solid #eee;\n}\n.cpoint[data-v-c3adf498] {\n  cursor: pointer;\n  border-radius: 50px;\n}\n.overlay[data-v-c3adf498] {\n  padding: 15px;\n  display: none;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(111, 148, 250, 0.5);\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  text-align: left;\n}\n.guides[data-v-c3adf498] {\n  margin: 40px 0;\n}\n.menu-box[data-v-c3adf498] {\n  display: grid;\n  grid-template-rows: 400px;\n  grid-template-columns: auto auto auto;\n  background-color: #f2f5fe;\n}\n.d-grid[data-v-c3adf498] {\n  display: grid;\n}\n.guide[data-v-c3adf498] {\n  cursor: pointer;\n}\n.item[data-v-c3adf498] {\n  cursor: pointer;\n  position: relative;\n  -webkit-transition: all 1s;\n  transition: all 1s;\n  color: white;\n}\n.item[data-v-c3adf498]:hover {\n  -webkit-box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.175) !important;\n          box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.175) !important;\n}\n.item:hover .overlay[data-v-c3adf498] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.cpoint[data-v-c3adf498]:hover {\n  background-color: #fafafa !important;\n  color: black !important;\n}\n.filters[data-v-c3adf498] {\n  display: grid;\n  grid-template-columns: 1fr 1fr 1fr 1fr;\n  grid-column-gap: 20px;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.filter[data-v-c3adf498] {\n  border: 1px solid;\n  padding: 10px;\n  text-align: center;\n  cursor: pointer;\n}\n.filter_selected[data-v-c3adf498]{\n  background-color: hsl(207, 46%, 20%);\n  color:white;\n}\n.filter_selected label .custom-control-indicator[data-v-c3adf498]{\n  color:white !important;\n}\n@media (max-width: 575px) {\n.single-box[data-v-c3adf498] {\n    padding: 15px;\n}\n.filters[data-v-c3adf498]{\n    grid-template-columns: 1fr;\n    grid-row-gap: 15px;\n}\n.containers[data-v-c3adf498] {\n    padding: 0;\n}\n.main[data-v-c3adf498] {\n    width: 100%;\n    padding: 0;\n}\n.menu-box[data-v-c3adf498] {\n    grid-template-rows: 250px;\n}\n.mini-box[data-v-c3adf498] {\n    grid-template-rows: 40px;\n    font-size: 14px;\n}\n.card-body[data-v-c3adf498] {\n    padding: 0.6rem;\n}\n.container[data-v-c3adf498] {\n    margin-top: 0;\n}\nh3[data-v-c3adf498] {\n    font-size: 16px !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "checklist-guide",
  data: function data() {
    return {
      result: [],
      authenticated: false,
      checklist: [],
      guides: [],
      template: [],
      gym: [],
      item: "qa",
      category: ''
    };
  },
  mounted: function mounted() {
    var _this = this;

    var user = localStorage.getItem("authUser");
    if (user !== null) {
      this.authenticated = true;
    }

    axios.get("/api/questionsVerified").then(function (response) {
      _this.result = response.data;
      _this.setCategory();
    });
  },

  computed: {
    questions: function questions() {
      var _this2 = this;

      return this.result.filter(function (value) {
        return value.name.toLowerCase() === _this2.item.toLowerCase();
      });
    }
  },
  watch: {
    item: 'setCategory'
  },
  methods: {
    setCategory: function setCategory() {
      if (this.item == 'qa') {
        this.category = 'Checklists';
      } else if (this.item == 'bg') {
        this.category = 'Guides';
      } else if (this.item == 'template') {
        this.category = 'Templates';
      } else {
        this.category = 'Case Studies';
      }
    },
    goto: function goto(x) {
      this.$router.push({
        name: "ListDiagnostics",
        params: { area: x }
      });
    },
    openLink: function openLink(id) {
      if (this.authenticated) {
        this.$router.push({
          name: "Checklist",
          params: { id: id }
        });
      } else {
        this.$toasted.error("Unauthorized access");
        this.$router.push({
          name: "auth",
          params: { name: "login" },
          query: { check: "guides" }
        });
      }
    },
    gotoHere: function gotoHere(type, id) {
      if (this.authenticated) {
        if (type === "gym") {
          this.$router.push({
            name: "Diagnostics",
            params: {
              id: id
            }
          });
        } else {
          this.$router.push({
            name: "Checklist",
            params: {
              id: id
            }
          });
        }
      } else {
        this.$router.push({
          name: "auth",
          params: {
            name: "login"
          },
          query: { redirect: this.$route.fullPath }
        });
      }
    }
  }
});

/***/ }),

/***/ 1835:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "containers" }, [
    _c("span", { staticClass: "overlay" }),
    _vm._v(" "),
    _vm.result.length > 0
      ? _c("div", { staticClass: "main shadow-lg animated fadeIn fast" }, [
          _c("div", { staticClass: "menu-box" }, [
            _c(
              "div",
              {
                staticClass:
                  "border p-3 d-grid align-items-center item btn-compliment rounded",
                on: {
                  click: function($event) {
                    return _vm.goto("template")
                  }
                }
              },
              [
                _vm._m(0),
                _vm._v(" "),
                _c("i", { staticClass: "fas fa-book-open fa-3x" }),
                _vm._v(" "),
                _c("h4", { staticClass: "te" }, [_vm._v("Template")])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "d-grid" }, [
              _c(
                "div",
                {
                  staticClass:
                    "border p-3 d-grid align-items-center item btn-compliment rounded",
                  on: {
                    click: function($event) {
                      return _vm.goto("checklist")
                    }
                  }
                },
                [
                  _vm._m(1),
                  _vm._v(" "),
                  _c("i", {
                    staticClass: "fa fa-clipboard-check fa-3x",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("h4", { staticClass: "te" }, [_vm._v("Checklist")])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "border p-3 d-grid align-items-center item btn-compliment rounded",
                  on: {
                    click: function($event) {
                      return _vm.goto("guides")
                    }
                  }
                },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("i", {
                    staticClass: "fa fa-list-ol fa-3x",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("h4", { staticClass: "te" }, [_vm._v("Guides")])
                ]
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "border p-3 d-grid align-items-center item btn-compliment rounded",
                on: {
                  click: function($event) {
                    return _vm.goto("brain-gym")
                  }
                }
              },
              [
                _vm._m(3),
                _vm._v(" "),
                _c("i", { staticClass: "fas fa-dumbbell fa-3x" }),
                _vm._v(" "),
                _c("h4", { staticClass: "te" }, [_vm._v("Case Studies")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "next-box p-3" }, [
            _c("div", { staticClass: "filters mb-5" }, [
              _c(
                "div",
                {
                  staticClass: "filter shadow-sm",
                  class: { filter_selected: _vm.item == "qa" }
                },
                [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.item,
                          expression: "item"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { type: "radio", value: "qa" },
                      domProps: { checked: _vm._q(_vm.item, "qa") },
                      on: {
                        change: function($event) {
                          _vm.item = "qa"
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Checklists")
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "filter shadow-sm",
                  class: { filter_selected: _vm.item == "templates" }
                },
                [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.item,
                          expression: "item"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { type: "radio", value: "templates" },
                      domProps: { checked: _vm._q(_vm.item, "templates") },
                      on: {
                        change: function($event) {
                          _vm.item = "templates"
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Templates")
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "filter shadow-sm",
                  class: { filter_selected: _vm.item == "bg" }
                },
                [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.item,
                          expression: "item"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { type: "radio", value: "bg" },
                      domProps: { checked: _vm._q(_vm.item, "bg") },
                      on: {
                        change: function($event) {
                          _vm.item = "bg"
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Guides")
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "filter shadow-sm",
                  class: { filter_selected: _vm.item == "brain-gym" }
                },
                [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.item,
                          expression: "item"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { type: "radio", value: "brain-gym" },
                      domProps: { checked: _vm._q(_vm.item, "brain-gym") },
                      on: {
                        change: function($event) {
                          _vm.item = "brain-gym"
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Case studies")
                    ])
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("h4", { staticClass: "mb-3" }, [_vm._v(_vm._s(_vm.category))]),
            _vm._v(" "),
            _c("div", { staticClass: "table-responsive" }, [
              _vm.questions.length
                ? _c("table", { staticClass: "table table-bordered" }, [
                    _vm._m(4),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.questions, function(item, idx) {
                        return _c("tr", { key: idx }, [
                          _c("td", { attrs: { scope: "row" } }, [
                            _vm._v(_vm._s(idx + 1))
                          ]),
                          _vm._v(" "),
                          item == "brain-gym"
                            ? _c(
                                "td",
                                {
                                  staticClass: "title",
                                  on: {
                                    click: function($event) {
                                      return _vm.gotoHere("gym", item.id)
                                    }
                                  }
                                },
                                [_vm._v(_vm._s(item.title))]
                              )
                            : _c(
                                "td",
                                {
                                  staticClass: "title",
                                  on: {
                                    click: function($event) {
                                      return _vm.gotoHere("check", item.id)
                                    }
                                  }
                                },
                                [_vm._v(_vm._s(item.title))]
                              )
                        ])
                      }),
                      0
                    )
                  ])
                : _c("div", [
                    _c("span", { staticClass: "form-control" }, [
                      _vm._v("Not available")
                    ])
                  ])
            ])
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "overlay" }, [
      _c("h4", { staticClass: "mb-1" }, [_vm._v("Template")]),
      _vm._v(" "),
      _c("span", { staticClass: "under" }),
      _vm._v(" "),
      _c("p", { staticClass: "text-faded" }, [
        _vm._v(
          "A template asks questions that when answered gives an output which represents the business document."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "overlay" }, [
      _c("h4", { staticClass: "mb-1" }, [_vm._v("Checklist")]),
      _vm._v(" "),
      _c("span", { staticClass: "under" }),
      _vm._v(" "),
      _c("p", { staticClass: "text-faded" }, [
        _vm._v(
          "A list of the different elements you need to have in your business which when completed shows how ready you are (in percentage)."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "overlay" }, [
      _c("h4", { staticClass: "mb-1" }, [_vm._v("Guide")]),
      _vm._v(" "),
      _c("span", { staticClass: "under" }),
      _vm._v(" "),
      _c("p", { staticClass: "text-faded" }, [
        _vm._v(
          "A Guide employs keywords, diagrams, games, etc. to help you define aspects of your business."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "overlay" }, [
      _c("h4", { staticClass: "mb-1" }, [_vm._v("Case Studies")]),
      _vm._v(" "),
      _c("span", { staticClass: "under" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th", [_vm._v("Title")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c3adf498", module.exports)
  }
}

/***/ }),

/***/ 640:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1832)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1834)
/* template */
var __vue_template__ = __webpack_require__(1835)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c3adf498"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/guidesComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c3adf498", Component.options)
  } else {
    hotAPI.reload("data-v-c3adf498", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});