webpackJsonp([95],{

/***/ 1178:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1179);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7a9f4646", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8a1974cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorEditForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8a1974cc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorEditForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1179:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main_page[data-v-8a1974cc] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  min-height: 100vh;\n  position: relative;\n}\n.preview[data-v-8a1974cc] {\n  position: absolute;\n  background: rgba(255, 255, 255, 0.8);\n  width: 100%;\n  height: 100%;\n  top: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px;\n  z-index: 1;\n}\n.body[data-v-8a1974cc] {\n  background: #fff;\n  padding: 25px;\n  height: 90%;\n  overflow: scroll;\n  width: 100%;\n}\n.gui[data-v-8a1974cc] {\n  width: 15%;\n  background: #f7f8fa;\n  height: 100%;\n  padding: 20px;\n  text-align: center;\n}\n.live[data-v-8a1974cc] {\n  width: 85%;\n  height: 100%;\n  position: relative;\n}\n.sub-page[data-v-8a1974cc] {\n  height: 90vh;\n  padding: 15px;\n  overflow-y: scroll;\n}\np[data-v-8a1974cc] {\n  font-size: 15px;\n}\nsmall[data-v-8a1974cc] {\n  font-size: 12px;\n  font-style: italic;\n  color: red;\n}\n.form-control[data-v-8a1974cc]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-8a1974cc]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-8a1974cc]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-8a1974cc]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 12px;\n}\n.form-control[data-v-8a1974cc]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\n.form-control[data-v-8a1974cc]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 12px;\n}\ntable[data-v-8a1974cc] {\n  font-size: 14px;\n}\nh4[data-v-8a1974cc] {\n  margin-bottom: 14px;\n  text-transform: inherit;\n  font-size: 15px;\n  font-weight: normal;\n}\n.form[data-v-8a1974cc] {\n  padding: 30px 15px;\n}\n.fa-plus-circle[data-v-8a1974cc],\n.fa-minus-circle[data-v-8a1974cc] {\n  font-size: 12px;\n  float: right;\n  padding: 10px;\n}\nsection[data-v-8a1974cc] {\n  background: rgb(204, 204, 204, 0.5);\n  padding:10px;\n}\n.rows[data-v-8a1974cc] {\n  background: #f7f8fa;\n  padding: 10px 15px;\n}\n.configs[data-v-8a1974cc] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 10px;\n  background: white;\n  margin-bottom: 10px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.mini-label[data-v-8a1974cc] {\n  font-size: 13px;\n}\nlabel span[data-v-8a1974cc] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.custom-control[data-v-8a1974cc] {\n  display: fleX;\n}\n.custom-control-input[data-v-8a1974cc] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-8a1974cc] {\n  color: hsl(207, 43%, 20%);\n  font-size: 13px;\n}\nth[data-v-8a1974cc],\ntd[data-v-8a1974cc] {\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),

/***/ 1180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: {},
      template: {},
      value: "",
      group: "",
      prev: false,
      current: 0,
      row_title: ""
    };
  },
  mounted: function mounted() {
    var user = JSON.parse(localStorage.getItem("authVendor"));
    this.user = user;
    this.getQuestion();
  },

  methods: {
    showConfig: function showConfig(id) {
      this.current = id;
    },
    closeConfig: function closeConfig() {
      this.current = null;
    },
    getQuestion: function getQuestion() {
      var _this = this;

      var id = this.$route.params.id;
      axios.get("/api/get-form-vendor/" + id, {
        headers: { Authorization: "Bearer " + this.user.access_token }
      }).then(function (res) {
        if (res.status = 200) {
          _this.template = JSON.parse(res.data.template);
          _this.group = res.data.group;
        }
      }).catch();
    },
    changeGroup: function changeGroup(value) {
      this.group = value;
    },
    previewNow: function previewNow() {
      this.prev = !this.prev;
    },
    updateRowTitle: function updateRowTitle(value, sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.push(this.row_title);
    },
    updateValue: function updateValue(value) {
      this.value = value;
    },
    updateHeader: function updateHeader(value) {
      this.myHead = value;
    },
    addForm: function addForm() {
      this.start = true;
    },
    addSection: function addSection() {
      this.template.form.sections.push({
        section: {
          label: "",
          rows: []
        }
      });
    },
    removeSection: function removeSection() {
      this.template.form.sections.pop();
    },
    removeRow: function removeRow(id) {
      this.template.form.sections[id].section.rows.pop();
    },
    addRow: function addRow(id) {
      this.template.form.sections[id].section.rows.push({
        row: [{
          type: "selected",
          multiple: false,
          placeholder: "input text",
          label: "",
          description: "",
          note: "",
          required: false,
          answer: "",
          answers: [],
          values: [],
          table: {
            items: [],
            headers: [],
            row_titles: []
          }
        }]
      });
    },
    addValue: function addValue(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].values.push(this.value);
    },
    addItem: function addItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.push(this.item);
    },
    removeItem: function removeItem(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.items.pop();
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.row_titles.pop();
    },
    addHeader: function addHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.push(this.myHead);

      this.myHead = "";
    },
    removeHeader: function removeHeader(sectionId, rowsId, rowId) {
      this.template.form.sections[sectionId].section.rows[rowsId].row[rowId].table.headers.pop();
    },
    updateForm: function updateForm() {
      var _this2 = this;

      var id = this.$route.params.id;
      var data = {
        id: id,
        group: this.group,
        title: this.template.form.legend,
        template: this.template
      };
      var user = JSON.parse(localStorage.getItem("authVendor"));
      axios.post("/api/update-form-vendor", JSON.parse(JSON.stringify(data)), {
        headers: { Authorization: "Bearer " + user.access_token }
      }).then(function (res) {
        if (res.status == 200) {
          _this2.$router.push('/vendor-form-questions');
        }
      }).catch();
    }
  }
});

/***/ }),

/***/ 1181:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.template.form
    ? _c("div", { staticClass: "main_page" }, [
        _c("div", { staticClass: "live" }, [
          _vm.prev
            ? _c("div", { staticClass: "preview" }, [
                _c("div", { staticClass: "body shadow" }, [
                  _c(
                    "div",
                    { staticClass: "p-2" },
                    [
                      _c("h3", { staticClass: "mb-3" }, [
                        _vm._v(_vm._s(_vm.template.form.legend))
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.template.form.sections, function(
                        section,
                        index
                      ) {
                        return _c(
                          "section",
                          { key: index, staticClass: "mb-3" },
                          [
                            _c("div", { staticClass: "number" }, [
                              _vm._v("Section " + _vm._s(index + 1))
                            ]),
                            _vm._v(" "),
                            _c("h4", { staticClass: "mb-2" }, [
                              _vm._v(_vm._s(section.section.label))
                            ]),
                            _vm._v(" "),
                            _vm._l(section.section.rows, function(rows, idx) {
                              return _c(
                                "div",
                                { key: idx },
                                _vm._l(rows.row, function(row, id) {
                                  return _c(
                                    "div",
                                    { key: id, staticClass: "mb-3 rows" },
                                    [
                                      _c("div", [
                                        _c("div", { staticClass: "number" }, [
                                          _vm._v("Q." + _vm._s(id + 1))
                                        ]),
                                        _vm._v(" "),
                                        _c("p", [
                                          _vm._v(_vm._s(row.description))
                                        ]),
                                        _vm._v(" "),
                                        row.type == "radio"
                                          ? _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c("h4", [
                                                  _vm._v(_vm._s(row.label))
                                                ]),
                                                _vm._v(" "),
                                                _c("small", [
                                                  _vm._v(_vm._s(row.note))
                                                ]),
                                                _vm._v(" "),
                                                _vm._l(row.values, function(
                                                  value,
                                                  id
                                                ) {
                                                  return _c(
                                                    "label",
                                                    {
                                                      key: id,
                                                      staticClass:
                                                        "custom-control custom-radio"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: row.answer,
                                                            expression:
                                                              "row.answer"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "custom-control-input",
                                                        attrs: {
                                                          type: "radio"
                                                        },
                                                        domProps: {
                                                          value: value,
                                                          checked: _vm._q(
                                                            row.answer,
                                                            value
                                                          )
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.$set(
                                                              row,
                                                              "answer",
                                                              value
                                                            )
                                                          }
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "custom-control-indicator"
                                                        },
                                                        [_vm._v(_vm._s(value))]
                                                      )
                                                    ]
                                                  )
                                                })
                                              ],
                                              2
                                            )
                                          : row.type == "select"
                                          ? _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c("h4", [
                                                  _vm._v(_vm._s(row.label))
                                                ]),
                                                _vm._v(" "),
                                                _c("small", [
                                                  _vm._v(_vm._s(row.note))
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "select",
                                                  {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value: row.answer,
                                                        expression: "row.answer"
                                                      }
                                                    ],
                                                    staticClass:
                                                      "custom-select",
                                                    on: {
                                                      change: function($event) {
                                                        var $$selectedVal = Array.prototype.filter
                                                          .call(
                                                            $event.target
                                                              .options,
                                                            function(o) {
                                                              return o.selected
                                                            }
                                                          )
                                                          .map(function(o) {
                                                            var val =
                                                              "_value" in o
                                                                ? o._value
                                                                : o.value
                                                            return val
                                                          })
                                                        _vm.$set(
                                                          row,
                                                          "answer",
                                                          $event.target.multiple
                                                            ? $$selectedVal
                                                            : $$selectedVal[0]
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "option",
                                                      {
                                                        attrs: {
                                                          disabled: "",
                                                          selected: ""
                                                        }
                                                      },
                                                      [_vm._v("Select one")]
                                                    ),
                                                    _vm._v(" "),
                                                    _vm._l(row.values, function(
                                                      value,
                                                      id
                                                    ) {
                                                      return _c(
                                                        "option",
                                                        { key: id },
                                                        [_vm._v(_vm._s(value))]
                                                      )
                                                    })
                                                  ],
                                                  2
                                                )
                                              ]
                                            )
                                          : row.type == "checkbox"
                                          ? _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c("h4", [
                                                  _vm._v(_vm._s(row.label))
                                                ]),
                                                _vm._v(" "),
                                                _c("small", [
                                                  _vm._v(_vm._s(row.note))
                                                ]),
                                                _vm._v(" "),
                                                _vm._l(row.values, function(
                                                  value,
                                                  id
                                                ) {
                                                  return _c(
                                                    "label",
                                                    {
                                                      key: id,
                                                      staticClass:
                                                        "custom-control custom-checkbox"
                                                    },
                                                    [
                                                      _c("input", {
                                                        directives: [
                                                          {
                                                            name: "model",
                                                            rawName: "v-model",
                                                            value: row.answers,
                                                            expression:
                                                              "row.answers"
                                                          }
                                                        ],
                                                        staticClass:
                                                          "custom-control-input",
                                                        attrs: {
                                                          type: "checkbox",
                                                          multiple: row.multiple
                                                        },
                                                        domProps: {
                                                          value: value,
                                                          checked: Array.isArray(
                                                            row.answers
                                                          )
                                                            ? _vm._i(
                                                                row.answers,
                                                                value
                                                              ) > -1
                                                            : row.answers
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            var $$a =
                                                                row.answers,
                                                              $$el =
                                                                $event.target,
                                                              $$c = $$el.checked
                                                                ? true
                                                                : false
                                                            if (
                                                              Array.isArray($$a)
                                                            ) {
                                                              var $$v = value,
                                                                $$i = _vm._i(
                                                                  $$a,
                                                                  $$v
                                                                )
                                                              if (
                                                                $$el.checked
                                                              ) {
                                                                $$i < 0 &&
                                                                  _vm.$set(
                                                                    row,
                                                                    "answers",
                                                                    $$a.concat([
                                                                      $$v
                                                                    ])
                                                                  )
                                                              } else {
                                                                $$i > -1 &&
                                                                  _vm.$set(
                                                                    row,
                                                                    "answers",
                                                                    $$a
                                                                      .slice(
                                                                        0,
                                                                        $$i
                                                                      )
                                                                      .concat(
                                                                        $$a.slice(
                                                                          $$i +
                                                                            1
                                                                        )
                                                                      )
                                                                  )
                                                              }
                                                            } else {
                                                              _vm.$set(
                                                                row,
                                                                "answers",
                                                                $$c
                                                              )
                                                            }
                                                          }
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "custom-control-indicator"
                                                        },
                                                        [_vm._v(_vm._s(value))]
                                                      )
                                                    ]
                                                  )
                                                })
                                              ],
                                              2
                                            )
                                          : row.type == "table"
                                          ? _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c("h4", [
                                                  _vm._v(_vm._s(row.label))
                                                ]),
                                                _vm._v(" "),
                                                _c("small", [
                                                  _vm._v(_vm._s(row.note))
                                                ]),
                                                _vm._v(" "),
                                                row.table.headers.length
                                                  ? _c(
                                                      "table",
                                                      {
                                                        staticClass:
                                                          "table table-bordered"
                                                      },
                                                      [
                                                        _c("thead", [
                                                          _c(
                                                            "tr",
                                                            [
                                                              _c("th"),
                                                              _vm._v(" "),
                                                              _vm._l(
                                                                row.table
                                                                  .headers,
                                                                function(
                                                                  column,
                                                                  index
                                                                ) {
                                                                  return _c(
                                                                    "th",
                                                                    {
                                                                      key: index
                                                                    },
                                                                    [
                                                                      _vm._v(
                                                                        _vm._s(
                                                                          column
                                                                        )
                                                                      )
                                                                    ]
                                                                  )
                                                                }
                                                              )
                                                            ],
                                                            2
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c(
                                                          "tbody",
                                                          _vm._l(
                                                            row.table.items,
                                                            function(
                                                              item,
                                                              index
                                                            ) {
                                                              return _c(
                                                                "tr",
                                                                { key: index },
                                                                [
                                                                  _c("td", [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        row
                                                                          .table
                                                                          .row_titles[
                                                                          index
                                                                        ]
                                                                      )
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _vm._l(
                                                                    row.table
                                                                      .headers,
                                                                    function(
                                                                      column,
                                                                      indexColumn
                                                                    ) {
                                                                      return _c(
                                                                        "td",
                                                                        {
                                                                          key: indexColumn,
                                                                          staticClass:
                                                                            "p-0"
                                                                        },
                                                                        [
                                                                          _c(
                                                                            "input",
                                                                            {
                                                                              directives: [
                                                                                {
                                                                                  name:
                                                                                    "model",
                                                                                  rawName:
                                                                                    "v-model",
                                                                                  value:
                                                                                    item[
                                                                                      column +
                                                                                        index +
                                                                                        indexColumn
                                                                                    ],
                                                                                  expression:
                                                                                    "item[column + index + indexColumn]"
                                                                                }
                                                                              ],
                                                                              staticClass:
                                                                                "form-control border-0",
                                                                              attrs: {
                                                                                type:
                                                                                  "text",
                                                                                placeholder:
                                                                                  "type here"
                                                                              },
                                                                              domProps: {
                                                                                value:
                                                                                  item[
                                                                                    column +
                                                                                      index +
                                                                                      indexColumn
                                                                                  ]
                                                                              },
                                                                              on: {
                                                                                input: function(
                                                                                  $event
                                                                                ) {
                                                                                  if (
                                                                                    $event
                                                                                      .target
                                                                                      .composing
                                                                                  ) {
                                                                                    return
                                                                                  }
                                                                                  _vm.$set(
                                                                                    item,
                                                                                    column +
                                                                                      index +
                                                                                      indexColumn,
                                                                                    $event
                                                                                      .target
                                                                                      .value
                                                                                  )
                                                                                }
                                                                              }
                                                                            }
                                                                          )
                                                                        ]
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                2
                                                              )
                                                            }
                                                          ),
                                                          0
                                                        )
                                                      ]
                                                    )
                                                  : _vm._e()
                                              ]
                                            )
                                          : (row.type = "text")
                                          ? _c(
                                              "div",
                                              { staticClass: "form-group" },
                                              [
                                                _c("h4", [
                                                  _vm._v(_vm._s(row.label))
                                                ]),
                                                _vm._v(" "),
                                                _c("small", [
                                                  _vm._v(_vm._s(row.note))
                                                ]),
                                                _vm._v(" "),
                                                row.type === "checkbox"
                                                  ? _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: row.answer,
                                                          expression:
                                                            "row.answer"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        placeholder:
                                                          row.placeholder,
                                                        required: row.required,
                                                        type: "checkbox"
                                                      },
                                                      domProps: {
                                                        checked: Array.isArray(
                                                          row.answer
                                                        )
                                                          ? _vm._i(
                                                              row.answer,
                                                              null
                                                            ) > -1
                                                          : row.answer
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          var $$a = row.answer,
                                                            $$el =
                                                              $event.target,
                                                            $$c = $$el.checked
                                                              ? true
                                                              : false
                                                          if (
                                                            Array.isArray($$a)
                                                          ) {
                                                            var $$v = null,
                                                              $$i = _vm._i(
                                                                $$a,
                                                                $$v
                                                              )
                                                            if ($$el.checked) {
                                                              $$i < 0 &&
                                                                _vm.$set(
                                                                  row,
                                                                  "answer",
                                                                  $$a.concat([
                                                                    $$v
                                                                  ])
                                                                )
                                                            } else {
                                                              $$i > -1 &&
                                                                _vm.$set(
                                                                  row,
                                                                  "answer",
                                                                  $$a
                                                                    .slice(
                                                                      0,
                                                                      $$i
                                                                    )
                                                                    .concat(
                                                                      $$a.slice(
                                                                        $$i + 1
                                                                      )
                                                                    )
                                                                )
                                                            }
                                                          } else {
                                                            _vm.$set(
                                                              row,
                                                              "answer",
                                                              $$c
                                                            )
                                                          }
                                                        }
                                                      }
                                                    })
                                                  : row.type === "radio"
                                                  ? _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: row.answer,
                                                          expression:
                                                            "row.answer"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        placeholder:
                                                          row.placeholder,
                                                        required: row.required,
                                                        type: "radio"
                                                      },
                                                      domProps: {
                                                        checked: _vm._q(
                                                          row.answer,
                                                          null
                                                        )
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          return _vm.$set(
                                                            row,
                                                            "answer",
                                                            null
                                                          )
                                                        }
                                                      }
                                                    })
                                                  : _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: row.answer,
                                                          expression:
                                                            "row.answer"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        placeholder:
                                                          row.placeholder,
                                                        required: row.required,
                                                        type: row.type
                                                      },
                                                      domProps: {
                                                        value: row.answer
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.$set(
                                                            row,
                                                            "answer",
                                                            $event.target.value
                                                          )
                                                        }
                                                      }
                                                    })
                                              ]
                                            )
                                          : _c("div")
                                      ])
                                    ]
                                  )
                                }),
                                0
                              )
                            })
                          ],
                          2
                        )
                      })
                    ],
                    2
                  )
                ])
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "sub-page" }, [
            _c("div", [
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                  attrs: { type: "button" },
                  on: { click: _vm.updateForm }
                },
                [_vm._v("Update Form")]
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "form" },
              [
                _c("div", { staticClass: "form-group mb-2" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.group,
                        expression: "group"
                      }
                    ],
                    staticClass: "form-control w-50",
                    attrs: {
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "Enter form group"
                    },
                    domProps: { value: _vm.group },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.group = $event.target.value
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.template.form.legend,
                        expression: "template.form.legend"
                      }
                    ],
                    staticClass: "form-control w-50",
                    attrs: {
                      type: "text",
                      "aria-describedby": "helpId",
                      placeholder: "Enter form title"
                    },
                    domProps: { value: _vm.template.form.legend },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.template.form,
                          "legend",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _vm._l(_vm.template.form.sections, function(section, index) {
                  return _c(
                    "section",
                    { key: index, staticClass: "mb-3" },
                    [
                      _c("h4", { staticClass: "mb-2" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: section.section.label,
                              expression: "section.section.label"
                            }
                          ],
                          staticClass: "form-control w-25",
                          attrs: {
                            type: "text",
                            placeholder: "Enter section title",
                            required: ""
                          },
                          domProps: { value: section.section.label },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                section.section,
                                "label",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group mb-2" }, [
                        _c("label", { staticClass: "mini-label" }, [
                          _vm._v("Description")
                        ]),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: section.section.description,
                              expression: "section.section.description"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { rows: "3" },
                          domProps: { value: section.section.description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                section.section,
                                "description",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "right mini-label mr-4",
                          on: {
                            click: function($event) {
                              return _vm.addRow(index)
                            }
                          }
                        },
                        [
                          _c(
                            "i",
                            {
                              staticClass: "fa fa-plus-circle text-main",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("Add row")]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "right mini-label",
                          on: {
                            click: function($event) {
                              return _vm.removeRow(index)
                            }
                          }
                        },
                        [
                          _c(
                            "i",
                            {
                              staticClass: "fa fa-minus-circle text-main",
                              attrs: { "aria-hidden": "true" }
                            },
                            [_vm._v("Remove row")]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm._l(section.section.rows, function(rows, idx) {
                        return _c(
                          "div",
                          { key: idx },
                          _vm._l(rows.row, function(row, id) {
                            return _c(
                              "div",
                              { key: id, staticClass: "mb-3 rows" },
                              [
                                _c("span", { staticClass: "text-right" }, [
                                  idx !== _vm.current
                                    ? _c("i", {
                                        staticClass: "fa fa-plus-circle",
                                        attrs: { "aria-hidden": "true" },
                                        on: {
                                          click: function($event) {
                                            return _vm.showConfig(idx)
                                          }
                                        }
                                      })
                                    : _vm._e(),
                                  _vm._v(" "),
                                  idx == _vm.current
                                    ? _c("i", {
                                        staticClass: "fa fa-minus-circle",
                                        attrs: { "aria-hidden": "true" },
                                        on: {
                                          click: function($event) {
                                            return _vm.closeConfig()
                                          }
                                        }
                                      })
                                    : _vm._e()
                                ]),
                                _vm._v(" "),
                                idx == _vm.current
                                  ? _c("div", { staticClass: "configs" }, [
                                      _c("div", [
                                        _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c(
                                              "label",
                                              { staticClass: "mini-label" },
                                              [_vm._v("Select type")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "select",
                                              {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: row.type,
                                                    expression: "row.type"
                                                  }
                                                ],
                                                staticClass: "custom-select",
                                                on: {
                                                  change: function($event) {
                                                    var $$selectedVal = Array.prototype.filter
                                                      .call(
                                                        $event.target.options,
                                                        function(o) {
                                                          return o.selected
                                                        }
                                                      )
                                                      .map(function(o) {
                                                        var val =
                                                          "_value" in o
                                                            ? o._value
                                                            : o.value
                                                        return val
                                                      })
                                                    _vm.$set(
                                                      row,
                                                      "type",
                                                      $event.target.multiple
                                                        ? $$selectedVal
                                                        : $$selectedVal[0]
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: {
                                                      disabled: "",
                                                      value: "selected"
                                                    }
                                                  },
                                                  [_vm._v("Select one")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  { attrs: { value: "text" } },
                                                  [_vm._v("Text")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: { value: "textarea" }
                                                  },
                                                  [_vm._v("Textarea")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: { value: "number" }
                                                  },
                                                  [_vm._v("Number")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  { attrs: { value: "date" } },
                                                  [_vm._v("Date")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  { attrs: { value: "time" } },
                                                  [_vm._v("Time")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: { value: "select" }
                                                  },
                                                  [_vm._v("Select Option")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  {
                                                    attrs: { value: "checkbox" }
                                                  },
                                                  [_vm._v("Checkbox")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  { attrs: { value: "radio" } },
                                                  [_vm._v("Radio")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "option",
                                                  { attrs: { value: "table" } },
                                                  [_vm._v("Table")]
                                                )
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        row.type == "table"
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "d-flex p-2 border align-items-start"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "form-group mr-3"
                                                  },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.myHead,
                                                          expression: "myHead"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        placeholder:
                                                          "type header here",
                                                        "aria-describedby":
                                                          "helpId"
                                                      },
                                                      domProps: {
                                                        value: _vm.myHead
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.myHead =
                                                            $event.target.value
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.addHeader(
                                                              index,
                                                              idx,
                                                              id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "Add Table header"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "elevated_btn elevated_btn_sm text-main",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.removeHeader(
                                                              index,
                                                              idx,
                                                              id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Remove header")]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "form-group" },
                                                  [
                                                    _c(
                                                      "div",
                                                      { staticClass: "d-flex" },
                                                      [
                                                        _c("input", {
                                                          directives: [
                                                            {
                                                              name: "model",
                                                              rawName:
                                                                "v-model",
                                                              value:
                                                                _vm.row_title,
                                                              expression:
                                                                "row_title"
                                                            }
                                                          ],
                                                          staticClass:
                                                            "form-control",
                                                          attrs: {
                                                            type: "text",
                                                            placeholder:
                                                              "type row title here..",
                                                            "aria-describedby":
                                                              "helpId"
                                                          },
                                                          domProps: {
                                                            value: _vm.row_title
                                                          },
                                                          on: {
                                                            input: function(
                                                              $event
                                                            ) {
                                                              if (
                                                                $event.target
                                                                  .composing
                                                              ) {
                                                                return
                                                              }
                                                              _vm.row_title =
                                                                $event.target.value
                                                            }
                                                          }
                                                        })
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.addItem(
                                                              index,
                                                              idx,
                                                              id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Add Table row")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "elevated_btn elevated_btn_sm text-main",
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.removeItem(
                                                              index,
                                                              idx,
                                                              id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Remove row")]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        row.type == "radio" ||
                                        row.type == "checkbox" ||
                                        row.type == "select"
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "form-group d-flex border p-2"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "mr-3" },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.value,
                                                          expression: "value"
                                                        }
                                                      ],
                                                      staticClass:
                                                        "form-control",
                                                      attrs: {
                                                        type: "text",
                                                        "aria-describedby":
                                                          "helpId",
                                                        placeholder:
                                                          "Enter value here"
                                                      },
                                                      domProps: {
                                                        value: _vm.value
                                                      },
                                                      on: {
                                                        input: function(
                                                          $event
                                                        ) {
                                                          if (
                                                            $event.target
                                                              .composing
                                                          ) {
                                                            return
                                                          }
                                                          _vm.value =
                                                            $event.target.value
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "elevated_btn elevated_btn_sm text-white btn-compliment",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.addValue(
                                                              index,
                                                              idx,
                                                              id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Add Value")]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                row.type == "checkbox"
                                                  ? _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "form-group"
                                                      },
                                                      [
                                                        _c(
                                                          "label",
                                                          {
                                                            staticClass:
                                                              "mini-label",
                                                            attrs: { for: "" }
                                                          },
                                                          [_vm._v("Multiple")]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "d-flex align-items-center"
                                                          },
                                                          [
                                                            _c(
                                                              "label",
                                                              {
                                                                staticClass:
                                                                  "custom-control custom-radio mini-label mr-3"
                                                              },
                                                              [
                                                                _c("input", {
                                                                  directives: [
                                                                    {
                                                                      name:
                                                                        "model",
                                                                      rawName:
                                                                        "v-model",
                                                                      value:
                                                                        row.multiple,
                                                                      expression:
                                                                        "row.multiple"
                                                                    }
                                                                  ],
                                                                  staticClass:
                                                                    "custom-control-input",
                                                                  attrs: {
                                                                    type:
                                                                      "radio",
                                                                    value:
                                                                      "true"
                                                                  },
                                                                  domProps: {
                                                                    checked: _vm._q(
                                                                      row.multiple,
                                                                      "true"
                                                                    )
                                                                  },
                                                                  on: {
                                                                    change: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.$set(
                                                                        row,
                                                                        "multiple",
                                                                        "true"
                                                                      )
                                                                    }
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "span",
                                                                  {
                                                                    staticClass:
                                                                      "custom-control-indicator"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "True"
                                                                    )
                                                                  ]
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "label",
                                                              {
                                                                staticClass:
                                                                  "custom-control custom-radio mini-label"
                                                              },
                                                              [
                                                                _c("input", {
                                                                  directives: [
                                                                    {
                                                                      name:
                                                                        "model",
                                                                      rawName:
                                                                        "v-model",
                                                                      value:
                                                                        row.multiple,
                                                                      expression:
                                                                        "row.multiple"
                                                                    }
                                                                  ],
                                                                  staticClass:
                                                                    "custom-control-input",
                                                                  attrs: {
                                                                    type:
                                                                      "radio",
                                                                    value:
                                                                      "false"
                                                                  },
                                                                  domProps: {
                                                                    checked: _vm._q(
                                                                      row.multiple,
                                                                      "false"
                                                                    )
                                                                  },
                                                                  on: {
                                                                    change: function(
                                                                      $event
                                                                    ) {
                                                                      return _vm.$set(
                                                                        row,
                                                                        "multiple",
                                                                        "false"
                                                                      )
                                                                    }
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "span",
                                                                  {
                                                                    staticClass:
                                                                      "custom-control-indicator"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "False"
                                                                    )
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  : _vm._e()
                                              ]
                                            )
                                          : _vm._e()
                                      ]),
                                      _vm._v(" "),
                                      _c("div", [
                                        _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass: "mini-label",
                                                attrs: { for: "" }
                                              },
                                              [_vm._v("Question title")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.label,
                                                  expression: "row.label"
                                                }
                                              ],
                                              staticClass: "form-control mb-2",
                                              attrs: {
                                                type: "text",
                                                placeholder:
                                                  "Enter label / question"
                                              },
                                              domProps: { value: row.label },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    row,
                                                    "label",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass: "mini-label",
                                                attrs: { for: "" }
                                              },
                                              [_vm._v("Description")]
                                            ),
                                            _vm._v(" "),
                                            _c("textarea", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.description,
                                                  expression: "row.description"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: { rows: "3" },
                                              domProps: {
                                                value: row.description
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    row,
                                                    "description",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "form-group" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass: "mini-label",
                                                attrs: { for: "" }
                                              },
                                              [_vm._v("Note")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.note,
                                                  expression: "row.note"
                                                }
                                              ],
                                              staticClass: "form-control mb-2",
                                              attrs: {
                                                type: "text",
                                                placeholder:
                                                  "Enter label / question"
                                              },
                                              domProps: { value: row.note },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    row,
                                                    "note",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "form-group" }, [
                                        _c(
                                          "label",
                                          {
                                            staticClass: "mini-label",
                                            attrs: { for: "" }
                                          },
                                          [_vm._v("Placeholder")]
                                        ),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.placeholder,
                                              expression: "row.placeholder"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            type: "text",
                                            placeholder: "Enter placeholder"
                                          },
                                          domProps: { value: row.placeholder },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "placeholder",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "form-group" }, [
                                        _c(
                                          "label",
                                          {
                                            staticClass: "mini-label",
                                            attrs: { for: "" }
                                          },
                                          [_vm._v("Required")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "d-flex align-items-center"
                                          },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass:
                                                  "custom-control custom-radio mini-label mr-3"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.required,
                                                      expression: "row.required"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "custom-control-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value: "true"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      row.required,
                                                      "true"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        row,
                                                        "required",
                                                        "true"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "custom-control-indicator"
                                                  },
                                                  [_vm._v("True")]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "label",
                                              {
                                                staticClass:
                                                  "custom-control custom-radio mini-label"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.required,
                                                      expression: "row.required"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "custom-control-input",
                                                  attrs: {
                                                    type: "radio",
                                                    value: "false"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      row.required,
                                                      "false"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        row,
                                                        "required",
                                                        "false"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "custom-control-indicator"
                                                  },
                                                  [_vm._v("False")]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("div", [
                                  _c("p", [_vm._v(_vm._s(row.description))]),
                                  _vm._v(" "),
                                  row.type == "radio"
                                    ? _c(
                                        "div",
                                        { staticClass: "form-group" },
                                        [
                                          _c("h4", [_vm._v(_vm._s(row.label))]),
                                          _vm._v(" "),
                                          _c("small", [
                                            _vm._v(_vm._s(row.note))
                                          ]),
                                          _vm._v(" "),
                                          _vm._l(row.values, function(
                                            value,
                                            id
                                          ) {
                                            return _c(
                                              "label",
                                              {
                                                key: id,
                                                staticClass:
                                                  "custom-control custom-radio"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.answer,
                                                      expression: "row.answer"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "custom-control-input",
                                                  attrs: { type: "radio" },
                                                  domProps: {
                                                    value: value,
                                                    checked: _vm._q(
                                                      row.answer,
                                                      value
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        row,
                                                        "answer",
                                                        value
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "custom-control-indicator"
                                                  },
                                                  [_vm._v(_vm._s(value))]
                                                )
                                              ]
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    : row.type == "select"
                                    ? _c("div", { staticClass: "form-group" }, [
                                        _c("h4", [_vm._v(_vm._s(row.label))]),
                                        _vm._v(" "),
                                        _c("small", [_vm._v(_vm._s(row.note))]),
                                        _vm._v(" "),
                                        _c(
                                          "select",
                                          {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: row.answer,
                                                expression: "row.answer"
                                              }
                                            ],
                                            staticClass: "custom-select",
                                            on: {
                                              change: function($event) {
                                                var $$selectedVal = Array.prototype.filter
                                                  .call(
                                                    $event.target.options,
                                                    function(o) {
                                                      return o.selected
                                                    }
                                                  )
                                                  .map(function(o) {
                                                    var val =
                                                      "_value" in o
                                                        ? o._value
                                                        : o.value
                                                    return val
                                                  })
                                                _vm.$set(
                                                  row,
                                                  "answer",
                                                  $event.target.multiple
                                                    ? $$selectedVal
                                                    : $$selectedVal[0]
                                                )
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "option",
                                              {
                                                attrs: {
                                                  disabled: "",
                                                  selected: ""
                                                }
                                              },
                                              [_vm._v("Select one")]
                                            ),
                                            _vm._v(" "),
                                            _vm._l(row.values, function(
                                              value,
                                              id
                                            ) {
                                              return _c("option", { key: id }, [
                                                _vm._v(_vm._s(value))
                                              ])
                                            })
                                          ],
                                          2
                                        )
                                      ])
                                    : row.type == "checkbox"
                                    ? _c(
                                        "div",
                                        { staticClass: "form-group" },
                                        [
                                          _c("h4", [_vm._v(_vm._s(row.label))]),
                                          _vm._v(" "),
                                          _c("small", [
                                            _vm._v(_vm._s(row.note))
                                          ]),
                                          _vm._v(" "),
                                          _vm._l(row.values, function(
                                            value,
                                            id
                                          ) {
                                            return _c(
                                              "label",
                                              {
                                                key: id,
                                                staticClass:
                                                  "custom-control custom-checkbox"
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: row.answers,
                                                      expression: "row.answers"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "custom-control-input",
                                                  attrs: {
                                                    type: "checkbox",
                                                    multiple: row.multiple
                                                  },
                                                  domProps: {
                                                    value: value,
                                                    checked: Array.isArray(
                                                      row.answers
                                                    )
                                                      ? _vm._i(
                                                          row.answers,
                                                          value
                                                        ) > -1
                                                      : row.answers
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      var $$a = row.answers,
                                                        $$el = $event.target,
                                                        $$c = $$el.checked
                                                          ? true
                                                          : false
                                                      if (Array.isArray($$a)) {
                                                        var $$v = value,
                                                          $$i = _vm._i($$a, $$v)
                                                        if ($$el.checked) {
                                                          $$i < 0 &&
                                                            _vm.$set(
                                                              row,
                                                              "answers",
                                                              $$a.concat([$$v])
                                                            )
                                                        } else {
                                                          $$i > -1 &&
                                                            _vm.$set(
                                                              row,
                                                              "answers",
                                                              $$a
                                                                .slice(0, $$i)
                                                                .concat(
                                                                  $$a.slice(
                                                                    $$i + 1
                                                                  )
                                                                )
                                                            )
                                                        }
                                                      } else {
                                                        _vm.$set(
                                                          row,
                                                          "answers",
                                                          $$c
                                                        )
                                                      }
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "custom-control-indicator"
                                                  },
                                                  [_vm._v(_vm._s(value))]
                                                )
                                              ]
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    : row.type == "table"
                                    ? _c("div", { staticClass: "form-group" }, [
                                        _c("h4", [_vm._v(_vm._s(row.label))]),
                                        _vm._v(" "),
                                        _c("small", [_vm._v(_vm._s(row.note))]),
                                        _vm._v(" "),
                                        row.table.headers.length
                                          ? _c(
                                              "table",
                                              {
                                                staticClass:
                                                  "table table-bordered"
                                              },
                                              [
                                                _c("thead", [
                                                  _c(
                                                    "tr",
                                                    [
                                                      _c("th"),
                                                      _vm._v(" "),
                                                      _vm._l(
                                                        row.table.headers,
                                                        function(
                                                          column,
                                                          index
                                                        ) {
                                                          return _c(
                                                            "th",
                                                            { key: index },
                                                            [
                                                              _vm._v(
                                                                _vm._s(column)
                                                              )
                                                            ]
                                                          )
                                                        }
                                                      )
                                                    ],
                                                    2
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "tbody",
                                                  _vm._l(
                                                    row.table.items,
                                                    function(item, index) {
                                                      return _c(
                                                        "tr",
                                                        { key: index },
                                                        [
                                                          _c("td", [
                                                            _vm._v(
                                                              _vm._s(
                                                                row.table
                                                                  .row_titles[
                                                                  index
                                                                ]
                                                              )
                                                            )
                                                          ]),
                                                          _vm._v(" "),
                                                          _vm._l(
                                                            row.table.headers,
                                                            function(
                                                              column,
                                                              indexColumn
                                                            ) {
                                                              return _c(
                                                                "td",
                                                                {
                                                                  key: indexColumn,
                                                                  staticClass:
                                                                    "p-0"
                                                                },
                                                                [
                                                                  _c("input", {
                                                                    directives: [
                                                                      {
                                                                        name:
                                                                          "model",
                                                                        rawName:
                                                                          "v-model",
                                                                        value:
                                                                          item[
                                                                            column +
                                                                              index +
                                                                              indexColumn
                                                                          ],
                                                                        expression:
                                                                          "item[column + index + indexColumn]"
                                                                      }
                                                                    ],
                                                                    staticClass:
                                                                      "form-control border-0",
                                                                    attrs: {
                                                                      type:
                                                                        "text",
                                                                      placeholder:
                                                                        "type here"
                                                                    },
                                                                    domProps: {
                                                                      value:
                                                                        item[
                                                                          column +
                                                                            index +
                                                                            indexColumn
                                                                        ]
                                                                    },
                                                                    on: {
                                                                      input: function(
                                                                        $event
                                                                      ) {
                                                                        if (
                                                                          $event
                                                                            .target
                                                                            .composing
                                                                        ) {
                                                                          return
                                                                        }
                                                                        _vm.$set(
                                                                          item,
                                                                          column +
                                                                            index +
                                                                            indexColumn,
                                                                          $event
                                                                            .target
                                                                            .value
                                                                        )
                                                                      }
                                                                    }
                                                                  })
                                                                ]
                                                              )
                                                            }
                                                          )
                                                        ],
                                                        2
                                                      )
                                                    }
                                                  ),
                                                  0
                                                )
                                              ]
                                            )
                                          : _vm._e()
                                      ])
                                    : (row.type = "text")
                                    ? _c("div", { staticClass: "form-group" }, [
                                        _c("h4", [_vm._v(_vm._s(row.label))]),
                                        _vm._v(" "),
                                        _c("small", [_vm._v(_vm._s(row.note))]),
                                        _vm._v(" "),
                                        row.type === "checkbox"
                                          ? _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answer,
                                                  expression: "row.answer"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: {
                                                placeholder: row.placeholder,
                                                required: row.required,
                                                type: "checkbox"
                                              },
                                              domProps: {
                                                checked: Array.isArray(
                                                  row.answer
                                                )
                                                  ? _vm._i(row.answer, null) >
                                                    -1
                                                  : row.answer
                                              },
                                              on: {
                                                change: function($event) {
                                                  var $$a = row.answer,
                                                    $$el = $event.target,
                                                    $$c = $$el.checked
                                                      ? true
                                                      : false
                                                  if (Array.isArray($$a)) {
                                                    var $$v = null,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          row,
                                                          "answer",
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          row,
                                                          "answer",
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(row, "answer", $$c)
                                                  }
                                                }
                                              }
                                            })
                                          : row.type === "radio"
                                          ? _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answer,
                                                  expression: "row.answer"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: {
                                                placeholder: row.placeholder,
                                                required: row.required,
                                                type: "radio"
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  row.answer,
                                                  null
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    row,
                                                    "answer",
                                                    null
                                                  )
                                                }
                                              }
                                            })
                                          : _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: row.answer,
                                                  expression: "row.answer"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              attrs: {
                                                placeholder: row.placeholder,
                                                required: row.required,
                                                type: row.type
                                              },
                                              domProps: { value: row.answer },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    row,
                                                    "answer",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                      ])
                                    : row.type == "textarea"
                                    ? _c("div", { staticClass: "form-group" }, [
                                        _c("h4", [_vm._v(_vm._s(row.label))]),
                                        _vm._v(" "),
                                        _c("textarea", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: row.answer,
                                              expression: "row.answer"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            rows: "3",
                                            required: row.required
                                          },
                                          domProps: { value: row.answer },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                row,
                                                "answer",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ])
                                    : _c("div")
                                ])
                              ]
                            )
                          }),
                          0
                        )
                      })
                    ],
                    2
                  )
                })
              ],
              2
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "gui" }, [
          _c("div", [
            _c("div", { staticClass: "mb-5" }, [
              _c(
                "button",
                {
                  staticClass: "elevated_btn elevated_btn_sm text-main",
                  attrs: { type: "button" },
                  on: { click: _vm.previewNow }
                },
                [_vm._v("Preview")]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-5" }, [
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                  attrs: { type: "button" },
                  on: { click: _vm.addSection }
                },
                [_vm._v("Add Section")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white",
                  attrs: { type: "button" },
                  on: { click: _vm.removeSection }
                },
                [_vm._v("Remove Section")]
              )
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8a1974cc", module.exports)
  }
}

/***/ }),

/***/ 508:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1178)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1180)
/* template */
var __vue_template__ = __webpack_require__(1181)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8a1974cc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorEditForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8a1974cc", Component.options)
  } else {
    hotAPI.reload("data-v-8a1974cc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});