webpackJsonp([12],{

/***/ 1007:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1008)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1010)
/* template */
var __vue_template__ = __webpack_require__(1011)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5e268f22"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userBannerComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5e268f22", Component.options)
  } else {
    hotAPI.reload("data-v-5e268f22", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1008:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1009);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2ef2ccbb", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e268f22\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userBannerComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5e268f22\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userBannerComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1009:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mobile-d[data-v-5e268f22]{\n  display: none;\n}\n.banner[data-v-5e268f22] {\n  width: 100%;\n  height: 85vh;\n  position: relative;\n  overflow: hidden;\n  background:#a4c2db;\n}\n.image-box[data-v-5e268f22]{\n   position: absolute;\n  width: 60%;\n  right: -5%;\n  height: 100%;\n   background-image: url(\"/images/bane1.jpg\");\n  background-position: right;\n  background-size: cover;\n}\n.slant_1[data-v-5e268f22] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n-webkit-transform:skewX(-25deg);\n        transform:skewX(-25deg);\n  top: -50%;\n    z-index: 2;\n    border-right:10px solid white;\n}\n.slant_2[data-v-5e268f22] {\n  background: #a4c2db;\n  position: absolute;\n  width: 50%;\n  left: 10%;\n  height: 85vh;\n  -webkit-transform:skewX(25deg);\n          transform:skewX(25deg);\n  bottom: -49.7%;\n  z-index: 2;\n  border-right:10px solid white;\n}\n.text_box[data-v-5e268f22] {\n  position: absolute;\n  width: 45%;\n  left: 0;\n  height: 100%;\n background: #a4c2db;\n  z-index: 3;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-orient:vertical;\n  -webkit-box-direction:normal;\n      -ms-flex-direction:column;\n          flex-direction:column;\n  padding-left:80px;\n}\n.text-container[data-v-5e268f22] {\n  font-family: \"Josefin Sans\", sans-serif;\n  line-height: 1.4;\n}\n.bannerA[data-v-5e268f22] {\n  font-size: 38px;\n  line-height: 1.4;\n}\n.bannerB[data-v-5e268f22] {\n  font-size: 28px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bannerC[data-v-5e268f22] {\n  font-size: 24px;\n  font-family: \"Open Sans\", sans-serif;\n}\n@media (max-width: 1024px) {\n.desktop-d[data-v-5e268f22]{\n    display: none;\n}\n.mobile-d[data-v-5e268f22]{\n    display: block;\n}\n.banner-container[data-v-5e268f22] {\n  position: relative;\n  overflow: hidden;\n  width: 100%;\n  height: 91vh;\n  border-right: 6px solid white;\n  border-left: 6px solid white;\n}\n.banner-over[data-v-5e268f22]{\n  position: absolute;\n  top: 0;\n  bottom:0;\n  left:0;\n  right:0;\n   background-image: -webkit-gradient(\n    linear,\n    left bottom, right top,\n    from(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(164, 194, 219,.9)),\n    color-stop(rgb(169, 197, 221,.9)),\n    color-stop(rgb(175, 201, 227 ,.9)),\n    color-stop(rgb(180, 204, 225,.9)),\n    color-stop(rgb(192, 212, 230,.9)),\n    color-stop(rgb(203, 220, 234,.9)),\n    color-stop(rgb(215, 228, 239,.9)),\n    to(rgb(227, 236, 244 ,.9))\n  );\n   background-image: linear-gradient(\n    to right top,\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(164, 194, 219,.9),\n    rgb(169, 197, 221,.9),\n    rgb(175, 201, 227 ,.9),\n    rgb(180, 204, 225,.9),\n    rgb(192, 212, 230,.9),\n    rgb(203, 220, 234,.9),\n    rgb(215, 228, 239,.9),\n    rgb(227, 236, 244 ,.9)\n  );\n  z-index: 2;\n}\n.banner-img[data-v-5e268f22] {\n    background-image: url(\"/images/bane1.jpg\");\n  background-position: right;\n  background-size: cover;\n  height: 100%;\n  width: 100%;\n}\n.banner-box[data-v-5e268f22] {\n  width: 60%;\n  height: 140%;\n \n  position: absolute;\n  top: -10%;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  left: -12%;\n  color: hsl(207, 43%, 20%);\n  background-image:url('/images/curve1.jpg');\n  background-size:contain;\n  z-index: 1;\n}\n.banner-white-box[data-v-5e268f22] {\n  width: 1%;\n  height: 140%;\n  background-color: #fff;\n  position: absolute;\n  top: -20%;\n  -webkit-transform: rotate(0deg);\n          transform: rotate(0deg);\n  right: 0;\n  z-index: 3;\n}\n.banner-text[data-v-5e268f22] {\n  position: absolute;\n  -webkit-transform: rotate(20deg);\n          transform: rotate(20deg);\n  right: 73px;\n  bottom: 30%;\n  width: 500px;\n  height: 500px;\n  z-index: 3;\n}\n.text-container[data-v-5e268f22] {\n  font-family: \"Josefin Sans\", sans-serif;\n  line-height: 1.4;\n}\n.bannerA[data-v-5e268f22] {\n  font-size: 38px;\n  line-height: 1.4;\n}\n.bannerB[data-v-5e268f22] {\n  font-size: 28px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bannerC[data-v-5e268f22] {\n  font-size: 24px;\n  font-family: \"Open Sans\", sans-serif;\n}\n.mobile-banner[data-v-5e268f22] {\n  display: none;\n}\n.banner-arrow[data-v-5e268f22] {\n  position: absolute;\n  width: 40px;\n  height: auto;\n  z-index: 7;\n  bottom: 15px;\n  left: 50%;\n  margin-left: -20px;\n  display: grid;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  cursor: pointer;\n  -webkit-transition: ease 0.4s;\n  transition: ease 0.4s;\n}\n.fa-caret-down[data-v-5e268f22] {\n  font-size: 36px;\n  color: white;\n}\n.banner-arrow:hover .fa1[data-v-5e268f22] {\n  color: hsl(207, 43%, 20%);\n}\n.banner-arrow:hover .fa2[data-v-5e268f22] {\n  color: white !important;\n}\n.mobile-banner-img[data-v-5e268f22] {\n  display: none;\n}\n.bannerA[data-v-5e268f22] {\n    line-height: 1.4;\n}\n.banner-text[data-v-5e268f22] {\n    right: 43px;\n    bottom: 28%;\n    width: 380px;\n    height: auto;\n}\n}\n@media (max-width: 768px) {\n.banner-box[data-v-5e268f22] {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n    width: 100%;\n    height: 100%;\n    top: 0;\n    left: 0;\n}\n.bannerA[data-v-5e268f22] {\n    line-height: 1.4;\n}\n.banner-white-box[data-v-5e268f22] {\n    width: 20%;\n    height: 100%;\n    top: 0;\n    right: 0;\n    background-color: #333;\n    z-index: 4;\n}\n.mobile-banner[data-v-5e268f22] {\n    display: block;\n    position: absolute;\n    z-index: 3;\n    bottom: 6%;\n    height: 400px;\n    width: 400px;\n    right: -15%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mini-border-1[data-v-5e268f22] {\n    border: 1px solid #bfd4e5;\n    padding: 10px;\n    height: 100%;\n    width: 100%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mini-border-2[data-v-5e268f22] {\n    border: 1px solid #bfd4e5;\n    padding: 10px;\n    height: 100%;\n    width: 100%;\n    border-radius: 50%;\n    overflow: hidden;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    display: block;\n    background: transparent;\n    width: 356px;\n    height: 356px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 9%;\n    right: -12%;\n    z-index: 5;\n    overflow: hidden;\n}\n.mobile-banner-img img[data-v-5e268f22] {\n    width: 100%;\n    height: 100%;\n    -o-object-fit: cover;\n       object-fit: cover;\n    -o-object-position: right;\n       object-position: right;\n}\n.banner-text[data-v-5e268f22] {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n    width: 75%;\n    bottom: 25%;\n    height: auto;\n    padding: 40px;\n    left: 0;\n    right: 0;\n    height: auto;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 32px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 17px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 17px;\n}\n.banner-arrow[data-v-5e268f22] {\n    left: 45%;\n}\n.banner-border[data-v-5e268f22] {\n    border-bottom: 1px solid white;\n    position: absolute;\n    width: 50%;\n    height: auto;\n    z-index: 7;\n    bottom: 50px;\n    right: 0;\n}\n}\n@media (max-width: 425px) {\n.banner-container[data-v-5e268f22] {\n    height: 90vh;\n    border: none;\n}\n.mobile-banner[data-v-5e268f22] {\n    bottom: 10%;\n    height: 300px;\n    width: 300px;\n    right: -25%;\n}\n.fa-caret-down[data-v-5e268f22] {\n    font-size: 24px;\n    color: white;\n}\n.banner-text[data-v-5e268f22] {\n    width: 81%;\n    bottom: 35%;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 24px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 17px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 17px;\n}\n.fa-caret-down[data-v-5e268f22] {\n    font-size: 24px;\n    color: white;\n}\n.banner-text[data-v-5e268f22] {\n    padding: 20px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 256px;\n    height: 256px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -20%;\n}\n}\n@media (max-width: 375px) {\n.mobile-banner[data-v-5e268f22] {\n    height: 274px;\n    width: 274px;\n    right: -30%;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 15px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 16px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 230px;\n    height: 230px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -24%;\n}\n}\n@media (max-width: 320px) {\n.mobile-banner[data-v-5e268f22] {\n    height: 274px;\n    width: 274px;\n    right: -30%;\n}\n.banner-text[data-v-5e268f22] {\n    bottom: 30%;\n    padding: 15px;\n}\n.bannerA[data-v-5e268f22] {\n    font-size: 20px;\n}\n.bannerB[data-v-5e268f22] {\n    font-size: 14px;\n}\n.bannerC[data-v-5e268f22] {\n    font-size: 14px;\n}\n.mobile-banner-img[data-v-5e268f22] {\n    background: white;\n    width: 230px;\n    height: 230px;\n    border-radius: 50%;\n    position: absolute;\n    bottom: 13%;\n    right: -24%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1010:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-banner-component",
  data: function data() {
    return {
      auth: false,
      search: "",
      show: false,
      scrollPos: 0,
      currentHeight: 0
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
    var user = localStorage.getItem('authUser');
    if (user !== null) {
      this.auth = true;
    }
  },


  methods: {
    scrollDown: function scrollDown() {
      window.scrollTo(0, window.innerHeight * 0.85);
    },
    mail: function mail() {
      axios.get('/api/mail').then(function (res) {});
    }
  }
});

/***/ }),

/***/ 1011:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "banner desktop-d" }, [
      _c("div", { staticClass: "slant_1" }),
      _vm._v(" "),
      _c("div", { staticClass: "slant_2" }),
      _vm._v(" "),
      _c("div", { staticClass: "slant_2" }),
      _vm._v(" "),
      _c("div", { staticClass: "text_box" }, [
        !_vm.auth
          ? _c("div", { staticClass: "text-container" }, [
              _c("p", { staticClass: "bannerA mb-3" }, [
                _vm._v(
                  " Join a community of entrepreneurs who can connect with experts and access practical business insights and resources, on the go."
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "bannerB mb-2" }, [
                _vm._v(
                  "We can take your business from 0 to 100 within six weeks."
                )
              ])
            ])
          : _c("div", { staticClass: "text-container" }, [
              _c("p", { staticClass: "bannerA mb-3" }, [
                _vm._v(
                  " Ready to be heard loud and clear through your business?"
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "bannerB mb-2" }, [
                _vm._v(
                  "We can take your business from 0 to 100 within six weeks."
                )
              ])
            ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "text-container" },
          [
            _c("p", { staticClass: "bannerC mb-2" }, [
              _vm._v("Don’t take our word for it.")
            ]),
            _vm._v(" "),
            !_vm.auth
              ? _c("router-link", { attrs: { to: "/auth/register" } }, [
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn btn-white text-main animated fadeIn slow"
                    },
                    [_vm._v(" Start for Free")]
                  )
                ])
              : _c("router-link", { attrs: { to: "/user-subscription/3" } }, [
                  _c(
                    "button",
                    {
                      staticClass:
                        "elevated_btn btn-white text-main animated fadeIn slow"
                    },
                    [_vm._v("Start BAP-6 program")]
                  )
                ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "image-box" })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "banner-container mobile-d" }, [
      _c("div", { staticClass: "banner-img" }),
      _vm._v(" "),
      _c("div", { staticClass: "banner-box" }, [
        _c("div", { staticClass: "banner-over" }),
        _vm._v(" "),
        _c("div", { staticClass: "banner-text animated fadeIn slow" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "text-container" },
            [
              _c("p", { staticClass: "bannerC mb-4" }, [
                _vm._v("Don’t take our word for it.")
              ]),
              _vm._v(" "),
              !_vm.auth
                ? _c("router-link", { attrs: { to: "/auth/register" } }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn btn-white text-main animated fadeIn slow"
                      },
                      [_vm._v(" Start for Free")]
                    )
                  ])
                : _c("router-link", { attrs: { to: "/user-subscription/3" } }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "elevated_btn btn-white text-main animated fadeIn slow"
                      },
                      [_vm._v("Start BAP-6 program")]
                    )
                  ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "banner-white-box" }),
        _vm._v(" "),
        _vm._m(2)
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "banner-arrow animated bounce",
          on: { click: _vm.scrollDown }
        },
        [
          _c("i", {
            staticClass: "fa fa-caret-down fa1",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" "),
          _c("i", {
            staticClass: "fa fa-caret-down fa1",
            attrs: { "aria-hidden": "true" }
          }),
          _vm._v(" "),
          _c("i", {
            staticClass: "fa fa-caret-down text-main fa2",
            attrs: { "aria-hidden": "true" }
          })
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "banner-border" }),
      _vm._v(" "),
      _c("div", { staticClass: "banner-bottom-bg" })
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-container" }, [
      _c("p", { staticClass: "bannerA mb-3" }, [
        _vm._v(
          " Join a community of entrepreneurs who can connect with experts and access practical business insights and resources, on the go."
        )
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "bannerB mb-2" }, [
        _vm._v("We can take your business from 0 to 100 within six weeks.")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "mobile-banner-img animated slideInRight" },
      [_c("img", { attrs: { src: "/images/optmobile.jpg", alt: "" } })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mobile-banner" }, [
      _c("div", { staticClass: "mini-border-1" }, [
        _c("div", { staticClass: "mini-border-2" })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5e268f22", module.exports)
  }
}

/***/ }),

/***/ 1012:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1013)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1015)
/* template */
var __vue_template__ = __webpack_require__(1016)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e1df3cf8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/maincontentComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e1df3cf8", Component.options)
  } else {
    hotAPI.reload("data-v-e1df3cf8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1013:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1014);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3c01a6b2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e1df3cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./maincontentComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e1df3cf8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./maincontentComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1014:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-e1df3cf8] {\n  min-height: 70vh;\n     background:rgba(255, 255, 255, 0.6);\n    border-top: 1px solid #eaeaea;\n     padding-bottom: 45px;\n}\n.card[data-v-e1df3cf8] {\n  border-radius: 2px;\n  -webkit-box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.05) !important;\n          box-shadow: 0 0 16px 1px rgba(0, 0, 0, 0.05) !important;\n  background: #fff;\n  color: #4a4a4a;\n  cursor: pointer;\n  -webkit-transition: box-shadow 0.3s ease-out;\n  -webkit-transition: -webkit-box-shadow 0.3s ease-out;\n  transition: -webkit-box-shadow 0.3s ease-out;\n  transition: box-shadow 0.3s ease-out;\n  transition: box-shadow 0.3s ease-out, -webkit-box-shadow 0.3s ease-out;\n}\n.active[data-v-e1df3cf8] {\n  border-color: hsl(207, 43%, 20%) !important;\n}\nh3[data-v-e1df3cf8] {\n  text-transform: initial;\n  font-size: 34px;\n  color: #333;\n}\n.menu-bar[data-v-e1df3cf8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  border-bottom: 1px solid #d4d6db;\n  margin-bottom: 36px;\n}\n.barItem[data-v-e1df3cf8] {\n  padding: 15px 20px;\n  font-size: 15px;\n  cursor: pointer;\n  widows: inherit;\n  border-bottom: 4px solid transparent;\n}\n.barItem[data-v-e1df3cf8]:hover {\n  color: hsl(207, 43%, 20%);\n}\n.rightSide[data-v-e1df3cf8] {\n  width: 100%;\n  height: auto;\n  padding: 0;\n  margin: 0 auto;\n}\n.my-content[data-v-e1df3cf8] {\n  width: 93%;\n  min-height: 50vh;\n  padding: 20px 0;\n  padding-top: 60px;\n \n  margin: 0 0 0 auto ;\n}\n.hover-text[data-v-e1df3cf8] {\n  display: none;\n  border-radius: 5px;\n  position: absolute;\n  height: 50%;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  color: white;\n  background-color: hsl(207, 43%, 20%);\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 10px;\n  z-index: 9;\n}\n.card:hover .hover-text[data-v-e1df3cf8] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.swiper-inner[data-v-e1df3cf8] {\n  width: 100%;\n  height: 500px;\n  padding-top: 50px;\n  padding-bottom: 50px;\n}\n.swiper-slide[data-v-e1df3cf8] {\n  background-position: center;\n  background-size: cover;\n  width: 500px;\n  height: auto;\n}\n.extraBg[data-v-e1df3cf8] {\n  width: 100%;\n  height: 45%;\n  bottom: 0;\n  position: absolute;\n}\n.btn-primary[data-v-e1df3cf8] {\n  background-color: #3c8dbc !important;\n  border-color: #367fa9 !important;\n  text-transform: capitalize;\n}\n.bg-offwhite[data-v-e1df3cf8] {\n  background-color: #ffffff;\n}\n.card[data-v-e1df3cf8] {\n  position: relative;\n  height: auto;\n}\n.card-img[data-v-e1df3cf8] {\n  height: 150px;\n  overflow: hidden;\n}\n.card-img-top[data-v-e1df3cf8] {\n  height: 150px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n  overflow: hidden;\n}\n.card-body[data-v-e1df3cf8] {\n  cursor: pointer;\n}\n.card:hover .card-img .card-img-top[data-v-e1df3cf8] {\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1);\n}\n.card-title.toCaps[data-v-e1df3cf8] {\n  color: hsl(207, 43%, 20%);\n  text-align: left;\n  font-weight: bold;\n  font-size: 16.5px;\n  height: 44px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.hover-title.toCaps[data-v-e1df3cf8] {\n  color: hsl(207, 43%, 20%);\n  text-align: left;\n  font-weight: bold;\n  font-size: 15px;\n  line-height: 1.3;\n  height: 60px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n  margin-bottom: 11px;\n}\n.card-text.industry[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 15px;\n  height: 20px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n  color: rgba(0, 0, 0, 0.54);\n}\n.expert-heading[data-v-e1df3cf8] {\n  width: 30%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n}\n.expert-heading strong[data-v-e1df3cf8] {\n  font-size: 24.5px;\n  color: #fff;\n}\n.card-desc[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 15px;\n  max-height: 113px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 3;\n  -moz-line-clamp: 3;\n  -ms-line-clamp: 3;\n  -o-line-clamp: 3;\n  line-clamp: 3;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.text[data-v-e1df3cf8] {\n  text-align: left;\n  font-size: 16px;\n  height: 300px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 12;\n  -moz-line-clamp: 12;\n  -ms-line-clamp: 12;\n  -o-line-clamp: 12;\n  line-clamp: 12;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  cursor: pointer;\n}\n.overlay-text[data-v-e1df3cf8] {\n  position: absolute;\n  display: none !important;\n  width: 93%;\n  height: 95%;\n  padding: 10px;\n  background-color: rgba(0, 0, 0, 0.9);\n  border-radius: 4px;\n}\n.card:hover .overlay-text[data-v-e1df3cf8] {\n  display: block !important;\n}\n.card-body[data-v-e1df3cf8] {\n  background-color: #ffffff;\n}\n.fa-4x[data-v-e1df3cf8] {\n  font-size: 2em;\n}\n@media (max-width: 768px) {\nh3[data-v-e1df3cf8] {\n    font-size: 24px;\n}\n.my-content[data-v-e1df3cf8] {\n    padding: 0;\n    padding-top: 60px;\n    padding-bottom: 45px;\n}\n.rightSide[data-v-e1df3cf8] {\n    width: 100%;\n    padding: 0;\n}\n.barItem[data-v-e1df3cf8] {\n    padding: 14px;\n    font-size: 14px;\n}\n.expert-heading[data-v-e1df3cf8] {\n    width: 90%;\n    padding: 15px 20px 10px;\n}\n.expert-heading strong[data-v-e1df3cf8] {\n    font-size: 18px;\n}\n}\n@media (max-width: 425px) {\n.expert-heading strong[data-v-e1df3cf8] {\n    font-size: 16px;\n}\nh3[data-v-e1df3cf8] {\n    font-size: 22px;\n}\n.barItem[data-v-e1df3cf8] {\n    padding: 6px;\n    font-size: 13px;\n}\n.d-no[data-v-e1df3cf8] {\n    display: none;\n}\n.mybox[data-v-e1df3cf8] {\n    width: 10%;\n}\n.fa-3x[data-v-e1df3cf8] {\n    font-size: 1em;\n}\n.card-title.toCaps[data-v-e1df3cf8] {\n    font-size: 13px;\n    line-height: 1.3;\n    height: 34px;\n    margin-bottom: 8px;\n}\n.card-body[data-v-e1df3cf8] {\n    padding: 10px;\n    line-height: 1.2;\n}\n.card-img[data-v-e1df3cf8] {\n    height: 100px;\n}\n.card-img-top[data-v-e1df3cf8] {\n    height: 120px;\n}\n.card[data-v-e1df3cf8] {\n    height: auto;\n    padding: 0 0 10px 0;\n}\n.card-text.industry[data-v-e1df3cf8] {\n    font-size: 12px;\n    margin-bottom: 8px;\n    height: 15px;\n}\n.card-text[data-v-e1df3cf8] {\n    font-size: 12px;\n    margin-bottom: 8px;\n}\n.card-desc[data-v-e1df3cf8] {\n    font-size: 13px;\n    height: 100px;\n    line-height: 1.6;\n    padding: 10px;\n}\n.short-text[data-v-e1df3cf8] {\n    height: 75px;\n    line-height: 1.2;\n}\n.btn[data-v-e1df3cf8] {\n    padding: 4px 18px 6px;\n}\n.bg-offwhite[data-v-e1df3cf8] {\n    background: none !important;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1015:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "main-content-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      type: "all",
      first: true,
      second: false,
      third: false,
      fourth: false,
      fifth: false,
      sixth: false,
      seventh: false,
      swiperOption: {
        slidesPerView: 5,
        slidesPerColumn: 1,
        spaceBetween: 15,
        breakpoints: {
          425: {
            slidesPerView: 2,
            spaceBetween: 10
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 10
          }
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        }
      },
      videoProduct: [],
      researchProduct: [],
      podcastProduct: [],
      articleProduct: [],
      courseProduct: [],
      allProduct: [],
      prevTop: 0
    };
  },
  created: function created() {
    this.getProducts();
  },
  mounted: function mounted() {},

  watch: {
    // scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 1.44) {
        this.swing = true;
      }
    },
    changeType: function changeType(type) {
      switch (type) {
        case "all":
          this.type = "all";
          this.first = true;
          this.second = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "finance":
          this.type = "finance";
          this.second = true;
          this.first = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "sales":
          this.type = "sales";
          this.third = true;
          this.first = this.second = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
        case "marketing":
          this.type = "marketing";
          this.fourth = true;
          this.first = this.second = this.third = this.fifth = this.sixth = this.seventh = false;
          break;
        case "modeling":
          this.type = "business modeling";
          this.fifth = true;
          this.first = this.second = this.third = this.fourth = this.sixth = this.seventh = false;
          break;
        case "personal_development":
          this.type = "personal development";
          this.sixth = true;
          this.first = this.second = this.third = this.fourth = this.fifth = this.seventh = false;
          break;
        case "branding":
          this.type = "branding";
          this.seventh = true;
          this.first = this.second = this.third = this.fourth = this.fifth = this.sixth = false;
          break;

        default:
          this.first = this.second = this.third = this.fourth = this.fifth = this.sixth = this.seventh = false;
          break;
      }
    },
    topSlide: function topSlide() {
      this.prevTop = this.$refs.articleSlide.swiper.activeIndex;
    },
    articleNext: function articleNext() {
      this.$refs.articleSlide.swiper.slideNext();
      this.topSlide();
    },
    articlePrev: function articlePrev() {
      this.$refs.articleSlide.swiper.slidePrev();
      this.topSlide();
    },
    getProducts: function getProducts() {
      var _this = this;

      axios.get("/api/products").then(function (response) {
        if (response.status === 200) {
          _this.isActive = false;

          response.data.data.forEach(function (item) {
            if (item.prodType == "Market Research") {
              item.marketResearch.coverImage = item.coverImage;
              item.marketResearch.subLevel = item.subscriptionLevel;
              _this.researchProduct.push(item.marketResearch);
            } else if (item.prodType == "Videos") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this.videoProduct.push(item.webinar);
            } else if (item.prodType == "Podcast") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this.podcastProduct.push(item.webinar);
            } else if (item.prodType == "Articles") {
              item.articles.coverImage = item.coverImage;
              item.articles.subLevel = item.subscriptionLevel;
              _this.articleProduct.push(item.articles);
            } else if (item.prodType == "Courses") {
              item.courses.coverImage = item.coverImage;
              item.courses.subLevel = item.subscriptionLevel;

              _this.courseProduct.push(item.courses);
            } else {
              return;
            }
          });

          _this.allProduct = response.data.data;
        }
      });
    },
    watch: function watch(id) {
      this.$router.push({
        name: "Video",
        params: { id: id }
      });
    }
  },
  computed: {
    myProducts: function myProducts() {
      var _this2 = this;

      return this.allProduct.reverse().filter(function (item) {
        if (item.subjectMatter.name.toLowerCase() == _this2.type) {
          return item;
        }
        if (_this2.type === "all") {
          return item;
        }
      });
    }
  },
  components: {}
});

/***/ }),

/***/ 1016:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "my-content d-flex align-center" }, [
      _c(
        "div",
        { staticClass: "rightSide" },
        [
          _c("div", { staticClass: "menu-bar" }, [
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.first },
                on: {
                  click: function($event) {
                    return _vm.changeType("all")
                  }
                }
              },
              [_vm._v("All")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.second },
                on: {
                  click: function($event) {
                    return _vm.changeType("finance")
                  }
                }
              },
              [_vm._v("Finance")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.third },
                on: {
                  click: function($event) {
                    return _vm.changeType("sales")
                  }
                }
              },
              [_vm._v("Sales")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.fourth },
                on: {
                  click: function($event) {
                    return _vm.changeType("marketing")
                  }
                }
              },
              [_vm._v("Marketing")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.fifth },
                on: {
                  click: function($event) {
                    return _vm.changeType("modeling")
                  }
                }
              },
              [_vm._v("Modelling")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem",
                class: { active: _vm.seventh },
                on: {
                  click: function($event) {
                    return _vm.changeType("branding")
                  }
                }
              },
              [_vm._v("Branding")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "barItem d-no",
                class: { active: _vm.sixth },
                on: {
                  click: function($event) {
                    return _vm.changeType("personal_development")
                  }
                }
              },
              [_vm._v("Personal Development")]
            )
          ]),
          _vm._v(" "),
          _c(
            "swiper",
            {
              ref: "articleSlide",
              staticClass: "bg-white p-2",
              attrs: { options: _vm.swiperOption }
            },
            [
              _vm._l(_vm.myProducts, function(item, idx) {
                return _c(
                  "swiper-slide",
                  { key: idx, staticClass: "card border-0" },
                  [
                    _c(
                      "div",
                      { staticClass: "hover-text" },
                      [
                        item.prodType == "Articles"
                          ? _c(
                              "p",
                              { staticClass: "hover-title toCaps text-white" },
                              [
                                _vm._v(
                                  _vm._s(item.articles.title.toLowerCase())
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c(
                              "p",
                              { staticClass: "card-title toCaps text-white" },
                              [_vm._v(_vm._s(item.webinar.title.toLowerCase()))]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Courses"
                          ? _c(
                              "p",
                              { staticClass: "card-title toCaps text-white" },
                              [_vm._v(_vm._s(item.courses.title.toLowerCase()))]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Articles"
                          ? _c("p", {
                              staticClass: "card-desc",
                              domProps: {
                                innerHTML: _vm._s(item.articles.description)
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c("p", { staticClass: "card-desc" }, [
                              _vm._v(_vm._s(item.webinar.description))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Courses"
                          ? _c("p", { staticClass: "card-desc" }, [
                              _vm._v(_vm._s(item.courses.overview))
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Articles"
                          ? _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "ArticleSinglePage",
                                    params: {
                                      id: item.articles.product_id,
                                      name: item.articles.title.replace(
                                        /[^a-z0-9]/gi,
                                        ""
                                      )
                                    }
                                  }
                                }
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "elevated_btn btn-sm elevated_btn_sm rounded-pill mt-0 text-main",
                                    attrs: { type: "button" }
                                  },
                                  [_vm._v("View")]
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        item.prodType == "Videos"
                          ? _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "Video",
                                    params: { id: item.webinar.product_id }
                                  }
                                }
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "elevated_btn btn-compliment elevated_btn_sm rounded-pill mt-0 text-white",
                                    attrs: { type: "button" }
                                  },
                                  [_vm._v("View")]
                                )
                              ]
                            )
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "card-img" }, [
                      _c("img", {
                        staticClass: "card-img-top",
                        attrs: { src: item.coverImage, alt: "" }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "card-body" }, [
                      item.prodType == "Articles"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.articles.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Videos"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.webinar.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Courses"
                        ? _c("p", { staticClass: "card-title toCaps" }, [
                            _vm._v(_vm._s(item.courses.title.toLowerCase()))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("p", { staticClass: "card-text text-left mb-1" }, [
                        _vm._v(_vm._s(item.prodType))
                      ]),
                      _vm._v(" "),
                      item.industry
                        ? _c(
                            "p",
                            {
                              staticClass: "card-text industry text-left mb-1"
                            },
                            [_vm._v(_vm._s(item.industry.name))]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Videos"
                        ? _c("p", { staticClass: "card-text text-left mb-1" }, [
                            _vm._v(_vm._s(item.webinar.host))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.prodType == "Articles"
                        ? _c("p", { staticClass: "card-text text-left mb-1" }, [
                            _vm._v(_vm._s(item.articles.writer))
                          ])
                        : _vm._e()
                    ])
                  ]
                )
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "swiper-button-next swiper-button-black",
                attrs: { slot: "button-next" },
                on: { click: _vm.articleNext },
                slot: "button-next"
              }),
              _vm._v(" "),
              _vm.prevTop !== 0
                ? _c("div", {
                    staticClass: "swiper-button-prev swiper-button-black",
                    attrs: { slot: "button-prev" },
                    on: { click: _vm.articlePrev },
                    slot: "button-prev"
                  })
                : _vm._e()
            ],
            2
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "explore-all  px-3" },
      [
        _c(
          "router-link",
          { staticClass: "ml-auto", attrs: { to: "/explore" } },
          [
            _c(
              "button",
              {
                staticClass: "elevated_btn elevated_btn_sm  text-main ml-auto"
              },
              [_vm._v("Explore all")]
            )
          ]
        )
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "expert-heading" }, [
      _c("strong", [_vm._v("Some of our featured resources")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e1df3cf8", module.exports)
  }
}

/***/ }),

/***/ 1017:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1018)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1020)
/* template */
var __vue_template__ = __webpack_require__(1021)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-d092364c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/subscribeComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d092364c", Component.options)
  } else {
    hotAPI.reload("data-v-d092364c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1018:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1019);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("b8668cc0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d092364c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subscribeComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d092364c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subscribeComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1019:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-d092364c]{\nbackground-image: -webkit-gradient(linear, left bottom, right top, from(#313a41), color-stop(#45515b), color-stop(#596a77), color-stop(#6f8394), color-stop(#859eb2), color-stop(#94adc1), color-stop(#a4bcd1), color-stop(#b4cce1), color-stop(#c0d4e6), color-stop(#cbdcea), color-stop(#d7e4ef), to(#e3ecf4));\nbackground-image: linear-gradient(to right top, #313a41, #45515b, #596a77, #6f8394, #859eb2, #94adc1, #a4bcd1, #b4cce1, #c0d4e6, #cbdcea, #d7e4ef, #e3ecf4);\npadding: 30px 0;\n}\n.my-content[data-v-d092364c] {\n  width: 80%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: auto;\n  margin:0 auto;\n}\n.image[data-v-d092364c] {\n  width: 20%;\n}\n.email[data-v-d092364c] {\n  width: 110px;\n  height: auto;\n}\n.sub[data-v-d092364c] {\n  width: 40%;\n  font-size: 20px;\n  font-weight: bold;\n  color: white;\n  padding: 0 10px;\n  line-height: 1.4;\n}\n.field[data-v-d092364c] {\n  width: 40%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.w-90[data-v-d092364c] {\n  width: 90%;\n}\n.form-control[data-v-d092364c] {\n  height: 60px;\n  border-radius: 6px;\n}\n@media (max-width: 768px) {\n.my-content[data-v-d092364c] {\n    padding: 10px 0px;\n    width:90%;\n}\n.image[data-v-d092364c] {\n}\n.sub[data-v-d092364c] {\n    font-size: 17px;\n}\n.field[data-v-d092364c] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.form-group[data-v-d092364c] {\n    margin-bottom: 10px !important;\n}\n}\n@media (max-width: 575px) {\n.my-content[data-v-d092364c] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 50px 20px;\n    width:90%;\n}\n.email[data-v-d092364c] {\n    margin-bottom: 16px;\n}\n.sub[data-v-d092364c] {\n    width: 100%;\n    text-align: center;\n    margin-bottom: 16px;\n    font-size: 16px;\n}\n.field[data-v-d092364c] {\n    width: 100%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1020:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "subscribe-component",
  data: function data() {
    return {
      email: '',
      wiggle: false
    };
  },
  mounted: function mounted() {},

  methods: {
    subscribe: function subscribe() {
      var _this = this;

      var data = {
        email: this.email,
        first_name: 'Bizguruh',
        last_name: 'bizguruh'
      };
      axios.post('/api/subscribe', data).then(function (response) {
        if (response.status === 200) {
          _this.$toasted.success("Subscription successful");
          _this.email = "";
          _this.wiggle = true;
        }
        {}
      });
    }
  }
});

/***/ }),

/***/ 1021:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _c("div", { staticClass: "my-content" }, [
      _c("div", { staticClass: "image" }, [
        _c("img", {
          staticClass: "email animated slow",
          class: { rubberBand: _vm.wiggle },
          attrs: { src: "/images/email.png", alt: "email icon" }
        })
      ]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c(
        "form",
        {
          staticClass: "field",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.subscribe($event)
            }
          }
        },
        [
          _c("div", { staticClass: "form-group mb-0 mr-2 w-90" }, [
            _c("label", { attrs: { for: "" } }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.email,
                  expression: "email"
                }
              ],
              staticClass: "form-control border",
              attrs: {
                type: "email",
                "aria-describedby": "helpId",
                placeholder: "example@email.com",
                required: ""
              },
              domProps: { value: _vm.email },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.email = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "elevated_btn bg-white mt-0",
              attrs: { type: "submit" }
            },
            [_vm._v("Subscribe")]
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "sub" }, [
      _c("p", [
        _vm._v(
          "For latest business informations, new marketing insights, business strategies and daily updates, subscribe to our newsletter today."
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d092364c", module.exports)
  }
}

/***/ }),

/***/ 1022:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1023)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1025)
/* template */
var __vue_template__ = __webpack_require__(1026)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-78dc3c7c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/expertComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-78dc3c7c", Component.options)
  } else {
    hotAPI.reload("data-v-78dc3c7c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1023:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1024);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2fe2169b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-78dc3c7c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./expertComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-78dc3c7c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./expertComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1024:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-78dc3c7c] {\n  padding: 10px 0;\n  padding-bottom: 45px;\n  min-height: 70vh;\n  background-color: rgb(247, 248, 250,.7);\n /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#f5f8fb+0,f5f8fb+50,e3ecf4+51,e3ecf4+100 */\n\noverflow: hidden;\nposition: relative;\n}\nh4[data-v-78dc3c7c]{\n  font-size:17px;\n}\n.bg-row[data-v-78dc3c7c]{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n   position: absolute;\n}\n.bg1[data-v-78dc3c7c]{\n    width: 60%;\n    background: rgb(227, 236, 244,.7);\n    -webkit-transform: rotate(20deg);\n            transform: rotate(20deg);\n    height: 156%;\n    top: -44%;\n    position: absolute;\n    left: -14%;\n}\n.expert-heading[data-v-78dc3c7c] {\n  width: 30%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n  margin-top: -10px;\n  position: absolute;\n  z-index: 4;\n}\n.expert-heading strong[data-v-78dc3c7c] {\n  font-size: 24.5px;\n  color: #fff;\n}\n.expert-box[data-v-78dc3c7c] {\n  padding-bottom: 20px;\n}\n.expert-about[data-v-78dc3c7c] {\n  text-align: center;\n  font-size: 15px;\n  color: rgba(55, 58, 60, 0.7);\n  height: 100.5px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 4;\n  -moz-line-clamp: 4;\n  -ms-line-clamp: 4;\n  -o-line-clamp: 4;\n  line-clamp: 4;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  margin-bottom: 8px;\n}\n.my-content[data-v-78dc3c7c] {\n  position: relative;\n  padding: 0px;\n  padding-top: 100px;\n  padding-bottom: 45px;\n  margin: 0 auto;\n  width: 85%;\n}\n.contain[data-v-78dc3c7c] {\n  width: 150px;\n  height: 150px;\n  overflow: hidden;\n  padding: 10px;\n  margin: 0 auto;\n}\n.contain img[data-v-78dc3c7c]{\n  width: 100%;\n  height: 100%;\n}\n.expert-image[data-v-78dc3c7c] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border:4px solid hsl(207, 43%, 94%);\n}\n.expert-box:hover .contain .expert-image[data-v-78dc3c7c]{\n  -webkit-transform: scale(1.05);\n          transform: scale(1.05);\n}\n.about[data-v-78dc3c7c] {\n  margin-top: -70px;\n  padding: 20px;\n  height: 315px;\n  background-color: #fff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  border-radius: 5px;\n}\n.about[data-v-78dc3c7c]:hover{\n-webkit-box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n}\n@media (max-width: 1024px) {\n.contain[data-v-78dc3c7c],\n  .expert-image[data-v-78dc3c7c] {\n    width: 110px;\n    height: 110px;\n}\n.my-content[data-v-78dc3c7c] {\n  width: 90%;\n}\n.about[data-v-78dc3c7c]{\n  padding:10px;\n}\n}\n@media (max-width: 768px) {\n.my-content[data-v-78dc3c7c] {\n  width: 100%;\n  padding-top: 60px;\n}\n.main[data-v-78dc3c7c] {\n    min-height: 50vh;\n    padding: 0;\n}\n.my-content[data-v-78dc3c7c] {\n    padding-left: 15px;\n    padding-right: 15px;\n}\n.expert-heading[data-v-78dc3c7c] {\n    width: 50%;\n    padding: 15px 20px 10px;\n}\n.expert-heading strong[data-v-78dc3c7c] {\n    font-size: 18px;\n}\n.contain[data-v-78dc3c7c],\n  .expert-image[data-v-78dc3c7c] {\n    width: 100px;\n    height: 100px;\n}\n.about[data-v-78dc3c7c] {\n    margin-top: -55px;\n    padding: 10px;\n    height: 230px;\n}\nh4[data-v-78dc3c7c] {\n    font-size: 12px;\n}\np[data-v-78dc3c7c]{\n    font-size: 12px;\n}\n.p-3[data-v-78dc3c7c]{\n    padding: 0 !important;\n}\n.mb-3[data-v-78dc3c7c]{\n    margin-bottom: 9px !important;\n}\n.mb-1[data-v-78dc3c7c] {\n    margin-bottom: 0px!important;\n}\n.expert-about[data-v-78dc3c7c]{\n    font-size: 12px;\n    line-height: 1.4;\n    height: 65.5px;\n    margin-bottom: 11px;\n}\n}\n@media (max-width: 375px) {\n.expert-heading strong[data-v-78dc3c7c] {\n  font-size: 16px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'experts-component',
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      allVendors: [],
      swiperOption: {
        slidesPerView: 4,
        spaceBetween: 30,
        breakpoints: {
          425: {
            slidesPerView: 2,
            spaceBetween: 15
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 15
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        },

        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        }
      }
    };
  },
  mounted: function mounted() {
    this.getVendors();
  },


  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 2.3) {
        this.swing = true;
      }
    },
    getVendors: function getVendors() {
      var _this = this;

      axios.get("/api/get-all-vendor").then(function (response) {
        if (response.status === 200) {
          response.data.forEach(function (item) {
            if (item.id !== 1) {
              _this.allVendors.push(item);
            }
          });
        }
      });
    }
  }
});

/***/ }),

/***/ 1026:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "my-content " },
      [
        _c(
          "swiper",
          {
            staticClass: "expertContainer py-3",
            attrs: { options: _vm.swiperOption }
          },
          [
            _vm._l(_vm.allVendors, function(vendor, index) {
              return _c(
                "swiper-slide",
                { key: index, staticClass: "expert-box" },
                [
                  _c("div", { staticClass: "contain rounded-circle" }, [
                    _c("img", {
                      staticClass: "expert-image rounded-circle",
                      attrs: { src: vendor.valid_id, alt: "" }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "about text-center text-white shadow" },
                    [
                      _c("h4", { staticClass: "toCaps mb-1 text-dark" }, [
                        _vm._v(_vm._s(vendor.storeName.toLowerCase()))
                      ]),
                      _vm._v(" "),
                      _c("p", { staticClass: "text-muted mb-3" }, [
                        _vm._v("@" + _vm._s(vendor.username))
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "expert-about" }, [
                        _vm._v(_vm._s(vendor.bio))
                      ]),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "PartnerProfile",
                              params: {
                                username: vendor.username
                              }
                            }
                          }
                        },
                        [
                          _c(
                            "span",
                            {
                              staticClass:
                                "elevated_btn elevated_btn_sm btn-compliment text-white"
                            },
                            [_vm._v("read more")]
                          )
                        ]
                      )
                    ],
                    1
                  )
                ]
              )
            }),
            _vm._v(" "),
            _c("div", {
              staticClass: "swiper-button-next swiper-button-black",
              attrs: { slot: "button-next" },
              slot: "button-next"
            }),
            _vm._v(" "),
            _c("div", {
              staticClass: "swiper-button-prev swiper-button-black",
              attrs: { slot: "button-prev" },
              slot: "button-prev"
            })
          ],
          2
        )
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "expert-heading" }, [
      _c("strong", [_vm._v("Our Experts")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-78dc3c7c", module.exports)
  }
}

/***/ }),

/***/ 1376:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1377);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7742592c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4be5be32\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userInsightComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4be5be32\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/sass-loader/lib/loader.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userInsightComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1377:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-4be5be32] {\n  position: relative;\n  background: #f7f8fa;\n}\n.bg-line[data-v-4be5be32] {\n  background: #f7f8fa;\n  /* Old browsers */\n  /* FF3.6-15 */\n  /* Chrome10-25,Safari5.1-6 */\n  background: linear-gradient(148deg, #fff 0%, #fff 50%, #f7f8fa 50%, #f7f8fa 100%);\n  /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff', endColorstr='#f7f8fa',GradientType=1 );\n  /* IE6-9 fallback on horizontal gradient */\n}\nimg[data-v-4be5be32] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.mobile[data-v-4be5be32] {\n  display: none;\n}\n.overdue[data-v-4be5be32] {\n  font-size: 14px;\n}\n.pageMenu[data-v-4be5be32] {\n  background: #fafafa;\n  background-image: url(/images/vb5.png);\n  background-repeat: repeat;\n  width: 100%;\n  min-height: 40px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  padding: 20px 15px 10px;\n}\n.pageNav[data-v-4be5be32] {\n  width: 60%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nlabel.toCaps.mainFontColor[data-v-4be5be32] {\n  cursor: pointer;\n}\nul[data-v-4be5be32] {\n  list-style: none;\n}\n.pageNav .pageNavi[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 65%;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n}\n.pageNav .pageNavi .mainNav[data-v-4be5be32] {\n  font-size: 16px;\n  padding: 10px;\n}\n.subNav[data-v-4be5be32]::before {\n  content: \"\";\n  border: 1px solid #fafafa;\n  -webkit-box-shadow: -2px 2px 3px 0 rgba(0, 0, 0, 0.1);\n          box-shadow: -2px 2px 3px 0 rgba(0, 0, 0, 0.1);\n  position: absolute;\n  -webkit-transform: rotate(135deg);\n          transform: rotate(135deg);\n  -webkit-transform-origin: 0 0;\n          transform-origin: 0 0;\n  width: 25px;\n  height: 25px;\n  top: 1px;\n  background: #fafafa;\n  left: 70px;\n}\n.subNav[data-v-4be5be32] {\n  display: none !important;\n  position: absolute;\n  border-radius: 4px;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 15px 30px;\n  z-index: 10;\n  background: #fafafa;\n  margin-top: 9px;\n  -webkit-box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);\n}\n.mainNav:hover .subNav[data-v-4be5be32] {\n  display: -webkit-box !important;\n  display: -ms-flexbox !important;\n  display: flex !important;\n}\n.search[data-v-4be5be32] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 320px;\n}\n.fa-search[data-v-4be5be32] {\n  position: absolute;\n  color: rgba(0, 0, 0, 0.34);\n  top: 25%;\n  right: 5px;\n  font-size: 16px;\n}\n.leftTitle[data-v-4be5be32] {\n  line-height: 1.2;\n  text-align: left;\n  color: #ffffff;\n  text-transform: capitalize;\n  font-size: 44px;\n  font-family: \"Josefin Sans\", sans-serif;\n}\n.middle[data-v-4be5be32] {\n  height: 50px;\n  background: #fff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.art[data-v-4be5be32] {\n  background-color: transparent !important;\n}\n.bttn-primary[data-v-4be5be32] {\n  margin: 0 10px 0 0;\n  background: transparent !important;\n  border: 2px solid white !important;\n  color: white !important;\n  border-radius: 50% !important;\n  text-transform: capitalize !important;\n  width: 40px;\n  height: 40px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  padding: 12px;\n  font-size: 12px;\n}\n.bttn-primary1[data-v-4be5be32] {\n  margin: 0 10px 0 0;\n  background: white !important;\n  border-color: white;\n  color: black !important;\n  border-radius: 20px !important;\n  padding: 0 15px;\n  text-transform: capitalize;\n  height: 40px;\n}\ninput[data-v-4be5be32] {\n  background-color: white !important;\n}\n.checkers[data-v-4be5be32] {\n  position: relative;\n  margin: auto 10px;\n}\n.navy[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.navy[data-v-4be5be32]:hover {\n  color: #88a2b8;\n}\n.bttn-primary[data-v-4be5be32]:hover {\n  background: rgba(164, 194, 219, 0.6) !important;\n}\n.bttn-primary1[data-v-4be5be32]:hover {\n  background: rgba(164, 194, 219, 0.6) !important;\n}\n.videos[data-v-4be5be32] {\n  position: absolute;\n  color: white;\n  z-index: 2;\n  right: 40px;\n  top: 40px;\n  padding: 5px;\n  background: rgba(0, 0, 0, 0.5);\n}\n.leftContainer[data-v-4be5be32] {\n  position: absolute;\n  width: 70%;\n  height: 250px;\n  top: 25%;\n  left: 5%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n}\n.leftContainer img[data-v-4be5be32] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.controlContainer[data-v-4be5be32] {\n  position: relative;\n  height: 100%;\n  width: 100%;\n  padding: 0px 30px;\n  color: #ffffff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n}\n.controller[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  text-align: center;\n}\n.hov[data-v-4be5be32] {\n  display: none;\n  position: absolute;\n  background: white;\n  padding: 5px;\n  bottom: -15px;\n  border-radius: 5px;\n  color: black;\n}\n.hov[data-v-4be5be32]::before {\n  position: absolute;\n  content: \"\";\n  top: -6px;\n  width: 15px;\n  height: 15px;\n  border-radius: 2px;\n  border: 1px solid white;\n  background: white;\n  left: 60px;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n  display: none;\n}\n.bttn-primary:hover .hov[data-v-4be5be32] {\n  display: block;\n}\n.bttn-primary:hover .hov[data-v-4be5be32]::before {\n  display: block;\n}\n.videoContainer[data-v-4be5be32] {\n  position: relative;\n  width: 70%;\n  height: 100%;\n  margin-left: auto;\n  background: black;\n}\nvideo[data-v-4be5be32] {\n  width: 100%;\n  height: 100%;\n}\n.courseContainer[data-v-4be5be32] {\n  position: relative;\n  width: 70%;\n  height: 100%;\n  margin-left: auto;\n  background: black;\n}\n.articleContainer[data-v-4be5be32] {\n  position: relative;\n  width: 70%;\n  height: 100%;\n  margin-left: auto;\n  background: black;\n}\n.researchContainer[data-v-4be5be32] {\n  position: relative;\n  width: 70%;\n  height: 100%;\n  margin-left: auto;\n  background: black;\n}\n.about[data-v-4be5be32] {\n  position: absolute;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n  top: 0px;\n  left: 0px;\n  background: rgba(0, 0, 0, 0.95);\n  padding: 10px;\n  display: none;\n}\n.recentHeader[data-v-4be5be32] {\n  margin: 20px 10px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.cover[data-v-4be5be32] {\n  width: 100%;\n  height: 200px;\n  position: relative;\n  -o-object-fit: cover;\n     object-fit: cover;\n  background-color: #fff;\n  border: 1px solid #c3c3c3;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.15);\n          box-shadow: 0 1px 6px rgba(0, 0, 0, 0.15);\n  overflow: hidden;\n  -webkit-transition: box-shadow 0.2s;\n  -webkit-transition: -webkit-box-shadow 0.2s;\n  transition: -webkit-box-shadow 0.2s;\n  transition: box-shadow 0.2s;\n  transition: box-shadow 0.2s, -webkit-box-shadow 0.2s;\n}\n.top .swiper-container .swiper-pagination-bullets .swiper-pagination-bullet[data-v-4be5be32] {\n  background: #fff !important;\n  opacity: 0.7 !important;\n}\nspan.swiper-pagination-bullet[data-v-4be5be32] {\n  background: #fff !important;\n  opacity: 0.7 !important;\n}\n.swiper-slide.slide-1[data-v-4be5be32] {\n  background: black;\n  height: 100%;\n  position: relative;\n}\n.swiper-slide.slide-2[data-v-4be5be32] {\n  background: black;\n  height: 100%;\n  position: relative;\n}\n.swiper-slide.slide-3[data-v-4be5be32] {\n  background: black;\n  height: 100%;\n  position: relative;\n}\n.swiper-slide.slide-4[data-v-4be5be32] {\n  background: black;\n  height: 100%;\n  position: relative;\n}\n.gallery-top[data-v-4be5be32] {\n  height: 84vh !important;\n  width: 100%;\n}\n.gallery-thumbs[data-v-4be5be32] {\n  height: 40% !important;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  padding: 0;\n}\n.gallery-thumbs .swiper-slide[data-v-4be5be32] {\n  width: 25%;\n  height: 30vh;\n}\n.gallery-thumbs .swiper-slide:hover .about[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.gallery-thumbs .swiper-slide-active[data-v-4be5be32] {\n  opacity: 1;\n}\n.leftShadow[data-v-4be5be32] {\n  background: -webkit-gradient(linear, right top, left top, from(rgba(0, 0, 0, 0)), to(black));\n  background: linear-gradient(to left, rgba(0, 0, 0, 0) 0%, black 100%);\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n.bottomShadow[data-v-4be5be32] {\n  background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0)), to(black));\n  background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, black 100%);\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n.hoverContent[data-v-4be5be32] {\n  position: absolute;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  z-index: 5;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: rgba(164, 194, 219, 0.95) !important;\n  display: none;\n  color: #ffffff;\n}\na:hover .businessBg:hover .hoverContent[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\na:hover .businessBgPod:hover .hoverContent[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\na:hover .businessBgVid:hover .hoverContent[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.inText[data-v-4be5be32] {\n  position: absolute;\n  top: 12%;\n  left: 5%;\n  color: #fff;\n  font-size: 1.7em;\n}\n.inText1[data-v-4be5be32] {\n  position: absolute;\n  color: #fff;\n  font-size: 16px;\n  background: rgba(0, 0, 0, 0.9);\n  width: 100%;\n  text-align: center;\n  height: 25px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  bottom: 18px;\n  border-bottom-right-radius: 4px;\n  border-bottom-left-radius: 4px;\n}\n.playBg[data-v-4be5be32] {\n  background: rgba(255, 255, 255, 0.7);\n  width: 80px;\n  height: 80px;\n  border-radius: 50%;\n  position: absolute;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  top: 30%;\n  left: 35%;\n}\n.playIcon[data-v-4be5be32] {\n  font-size: 40px;\n  position: absolute;\n  color: rgba(0, 0, 0, 0.7);\n  z-index: 2;\n}\n.businessBg[data-v-4be5be32] {\n  width: 100%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.diverHeader[data-v-4be5be32] {\n  text-align: center;\n  padding: 20px 0 15px;\n  color: rgba(0, 0, 0, 0.64);\n}\n.expp[data-v-4be5be32] {\n  color: whitesmoke;\n}\n.exp[data-v-4be5be32] {\n  width: 100%;\n  margin: 0 auto;\n}\n.username[data-v-4be5be32] {\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 16px;\n}\n.feat[data-v-4be5be32] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.cat-arrow[data-v-4be5be32] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: #ffffff;\n  font-size: 34px;\n}\n.recent[data-v-4be5be32] {\n  height: auto;\n  width: 100%;\n  padding: 15px;\n  background: #fafafa;\n  background-image: url(/images/vb5.png);\n  background-repeat: repeat;\n}\n.recentContent[data-v-4be5be32] {\n  width: 100%;\n  padding: 20px 0;\n}\n.recentVid[data-v-4be5be32] {\n  width: 100%;\n}\n.recentArt[data-v-4be5be32] {\n  width: 100%;\n}\n.recentVid .swiper-container[data-v-4be5be32] {\n  background: none;\n  height: 250px;\n}\n.overview[data-v-4be5be32] {\n  text-align: left;\n  font-size: 16px;\n  height: 100px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 4;\n  -moz-line-clamp: 4;\n  -ms-line-clamp: 4;\n  -o-line-clamp: 4;\n  line-clamp: 4;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.overview1[data-v-4be5be32] {\n  text-align: left;\n  font-size: 16px;\n  height: 100px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 4;\n  -moz-line-clamp: 4;\n  -ms-line-clamp: 4;\n  -o-line-clamp: 4;\n  line-clamp: 4;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.overviewMain[data-v-4be5be32] {\n  width: 50%;\n  font-size: 18px;\n  height: 54px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 3;\n  -moz-line-clamp: 3;\n  -ms-line-clamp: 3;\n  -o-line-clamp: 3;\n  line-clamp: 3;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.overviewMain p[data-v-4be5be32] {\n  font-size: 16px;\n}\n.subTit[data-v-4be5be32] {\n  color: white;\n  height: 25px;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  line-clamp: 1;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  text-align: left;\n  text-transform: capitalize;\n  font-size: 18px;\n}\n.bio[data-v-4be5be32] {\n  font-size: 16px;\n  padding: 10px;\n  text-align: left;\n  height: 230px;\n  color: white;\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 9;\n  -moz-line-clamp: 9;\n  -ms-line-clamp: 9;\n  -o-line-clamp: 9;\n  line-clamp: 9;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.articleSlide[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n  background: white;\n  padding: 10px;\n  -webkit-box-shadow: 0 0 1px 0 #ccc;\n          box-shadow: 0 0 1px 0 #ccc;\n  border-radius: 3px;\n}\n.articleText[data-v-4be5be32] {\n  width: 70%;\n}\n.articleTitle[data-v-4be5be32] {\n  font-size: 18px;\n  text-align: left;\n  height: 42px;\n  color: rgba(0, 0, 0, 0.84);\n  line-height: 1.3;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.articleImage[data-v-4be5be32] {\n  width: 30%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.articleImage img[data-v-4be5be32] {\n  width: 100px;\n  height: 100px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.articleDesc[data-v-4be5be32] {\n  font-size: 16px;\n  text-align: left;\n  max-height: 50px;\n  color: rgba(0, 0, 0, 0.54);\n  line-height: 1.6;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.videoBio1[data-v-4be5be32] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  display: none;\n  color: white;\n  background: rgba(0, 0, 0, 0.95);\n  padding: 15px;\n}\n.videoSlide1:hover .videoBio1[data-v-4be5be32] {\n  display: block;\n}\n.videoBio[data-v-4be5be32] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  display: none;\n  color: white;\n  background: rgba(0, 0, 0, 0.95);\n  padding: 15px;\n}\n.videoSlide:hover .videoBio[data-v-4be5be32] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.videoSlide:hover .playBg[data-v-4be5be32] {\n  display: none;\n}\n.results[data-v-4be5be32] {\n  width: 30%;\n  max-height: 400px;\n  position: absolute;\n  background: white;\n  z-index: 15;\n  border-radius: 2px;\n  padding: 10px;\n  font-weight: 300;\n  overflow: auto;\n  font-size: 14px;\n  -webkit-box-shadow: 1px 0 1px 0px #ccc;\n          box-shadow: 1px 0 1px 0px #ccc;\n}\n.searchResult[data-v-4be5be32] {\n  border-bottom: 1px solid #ccc;\n  height: 25px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  box-orient: vertical;\n  display: -webkit-box !important;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  line-clamp: 1;\n  -webkit-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -o-line-clamp: 1;\n  white-space: normal;\n  color: rgba(0, 0, 0, 0.64);\n}\n.mini-content[data-v-4be5be32] {\n  width: 85%;\n  margin: 0 auto;\n  padding-top: 60px;\n  padding-bottom: 45px;\n}\n.card-img-top[data-v-4be5be32] {\n  position: relative;\n  width: 100%;\n  height: 150px;\n  overflow: hidden;\n}\n.card:hover .card-img-top .img-overlay[data-v-4be5be32] {\n  display: inline;\n}\n.video-content[data-v-4be5be32] {\n  margin-bottom: 50px;\n  position: relative;\n}\n.article-content[data-v-4be5be32] {\n  position: relative;\n}\n.card[data-v-4be5be32]:hover {\n  -webkit-box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n          box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;\n  cursor: pointer;\n}\n.card:hover .card-img-top img[data-v-4be5be32] {\n  -webkit-transform: scale(1.1);\n          transform: scale(1.1);\n}\n.card-body[data-v-4be5be32] {\n  height: 200px;\n}\n.card-title[data-v-4be5be32] {\n  color: #1d3549;\n  text-transform: capitalize;\n  height: 45px;\n  display: -webkit-box !important;\n  line-height: 1.3;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.card-text[data-v-4be5be32] {\n  height: 65px;\n  color: rgba(55, 58, 60, 0.7);\n  display: -webkit-box !important;\n  -webkit-line-clamp: 3;\n  -moz-line-clamp: 3;\n  -ms-line-clamp: 3;\n  -o-line-clamp: 3;\n  line-clamp: 3;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n  font-size: 16px;\n  line-height: 1.4;\n}\n.card-bottom[data-v-4be5be32] {\n  position: absolute;\n  bottom: 0;\n  color: rgba(55, 58, 60, 0.64);\n  font-size: 12px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.mini-content-title[data-v-4be5be32] {\n  width: 40%;\n  padding: 30px 30px 25px;\n  background-color: #1d3549;\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10%;\n  margin-bottom: 50px;\n}\n.bms-heading[data-v-4be5be32] {\n  width: 40%;\n  padding: 30px 30px 25px;\n  background-color: #1d3549;\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n}\n.expert-heading strong[data-v-4be5be32] {\n  font-size: 34px;\n  color: #fff;\n}\n.bms[data-v-4be5be32] {\n  padding: 0px;\n  width: 100%;\n  margin: 0 auto;\n  padding-top: 40px;\n  padding-bottom: 5px;\n}\n.bms_em[data-v-4be5be32] {\n  padding: 40px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: #f7f8fa;\n}\n.leftSlide p[data-v-4be5be32] {\n  font-size: 38px;\n  line-height: 1.3;\n}\n.swiper[data-v-4be5be32] {\n  height: 600px;\n  width: 100%;\n}\n.accounting_slider[data-v-4be5be32] {\n  margin-top: 50px;\n  min-height: 400px;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width: 85%;\n  margin: 0 auto;\n}\n.leftSlide[data-v-4be5be32] {\n  width: 45%;\n  height: 100%;\n  text-align: center;\n}\n.rightSlide[data-v-4be5be32] {\n  width: 50%;\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 20px 0;\n}\n.rightSlide p[data-v-4be5be32] {\n  font-size: 32px;\n}\n.slide_button[data-v-4be5be32] {\n  width: 80%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin: 0 auto;\n}\n.my-slides[data-v-4be5be32] {\n  width: 100%;\n  height: 100%;\n  background: white;\n}\n.bms_img[data-v-4be5be32] {\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: left;\n     object-position: left;\n}\n@media (max-width: 1024px) {\n.mini-content[data-v-4be5be32],\n  .bms[data-v-4be5be32] {\n    width: 90%;\n}\n.bms-heading[data-v-4be5be32] {\n    width: 50%;\n}\n.mini-content-title[data-v-4be5be32] {\n    width: 35%;\n    margin-left: -7%;\n}\n.search[data-v-4be5be32] {\n    width: 250px;\n}\n}\n@media (max-width: 768px) {\n.pageNav .pageNavi[data-v-4be5be32] {\n    width: 100%;\n}\n.card-body[data-v-4be5be32] {\n    height: 140px;\n    padding: 10px;\n}\n.card-title[data-v-4be5be32] {\n    font-size: 14px;\n    height: 34px;\n}\n.card-text[data-v-4be5be32] {\n    font-size: 14px;\n    height: 55px;\n}\n.bms-heading strong[data-v-4be5be32] {\n    font-size: 18px;\n}\n.mini-content[data-v-4be5be32] {\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-top: 30px;\n}\n.slide_button[data-v-4be5be32] {\n    width: 60%;\n}\n.mini-content-title[data-v-4be5be32] {\n    font-size: 18px;\n    margin-left: -3%;\n    padding: 15px 20px 13px;\n}\n.overviewMain[data-v-4be5be32] {\n    width: 80%;\n}\n.p-3[data-v-4be5be32] {\n    padding: 0 !important;\n}\n.bms-heading[data-v-4be5be32] {\n    width: 85%;\n    padding: 20px 20px 15px;\n}\n.accounting_slider[data-v-4be5be32] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height: auto;\n    width: 100%;\n}\n.rightSlide[data-v-4be5be32],\n  .leftSlide[data-v-4be5be32] {\n    width: 80%;\n    margin: 0 auto;\n}\n.mini-content[data-v-4be5be32],\n  .bms[data-v-4be5be32] {\n    width: 100%;\n}\n.bms[data-v-4be5be32] {\n    padding-top: 0;\n}\n.bms-em[data-v-4be5be32] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 40px 15px;\n}\n.leftSlide[data-v-4be5be32] {\n    padding: 20px;\n}\n.leftSlide p[data-v-4be5be32] {\n    font-size: 24px;\n    text-align: center !important;\n    padding: 10px;\n}\n}\n@media (max-width: 425px) {\n.card-title[data-v-4be5be32] {\n    font-size: 13px;\n}\n.card-text[data-v-4be5be32] {\n    font-size: 13px;\n}\n.slide_button[data-v-4be5be32] {\n    width: 100%;\n}\n.leftSlide[data-v-4be5be32] {\n    width: 100%;\n    padding: 20px 5px;\n}\n.mini-content[data-v-4be5be32] {\n    padding-top: 10px;\n}\n.mini-content-title[data-v-4be5be32],\n  .bms-heading strong[data-v-4be5be32] {\n    font-size: 16px;\n}\n.mini-content[data-v-4be5be32],\n  .bms[data-v-4be5be32] {\n    width: 100%;\n}\n.mini-content-title[data-v-4be5be32] {\n    width: 60%;\n    margin-left: -4.5%;\n}\n.overview[data-v-4be5be32] {\n    font-size: 14px;\n}\n.business-words[data-v-4be5be32] {\n    display: grid;\n    grid-template-rows: auto auto;\n    grid-template-columns: repeat(10, 145px);\n    grid-column-gap: 20px;\n    grid-row-gap: 10px;\n    margin-bottom: 20px;\n    overflow-x: scroll;\n}\n.single-word[data-v-4be5be32] {\n    padding: 10px 9px;\n    font-size: 12px;\n    text-align: center;\n}\nh2[data-v-4be5be32] {\n    font-size: 20px;\n}\n.card-img-top[data-v-4be5be32] {\n    width: 100%;\n}\n.bio[data-v-4be5be32] {\n    font-size: 14px;\n    height: 145px;\n    line-clamp: 6;\n    -webkit-line-clamp: 6;\n    -ms-line-clamp: 6;\n    -moz-line-clamp: 6;\n    -o-line-clamp: 6;\n}\n.articleSlide[data-v-4be5be32] {\n    margin: 10px;\n}\n.thumby2[data-v-4be5be32] {\n    font-size: 9px;\n}\n.slide[data-v-4be5be32] {\n    position: -webkit-sticky;\n    position: sticky;\n    top: 0%;\n    padding: 5px 20px;\n    text-align: right;\n    z-index: 5;\n}\n.fa-sliders-h[data-v-4be5be32] {\n    font-size: 16px;\n    color: rgba(0, 0, 0, 0.4);\n}\n.articleTitle[data-v-4be5be32] {\n    font-size: 16px;\n}\n.pageNavi[data-v-4be5be32] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding: 0;\n}\n.recent[data-v-4be5be32] {\n    padding: 1px;\n}\n.recentVid[data-v-4be5be32] {\n    padding: 0;\n}\n.desktop[data-v-4be5be32] {\n    display: none;\n}\n.exp[data-v-4be5be32] {\n    width: 100%;\n}\n.card-img-top[data-v-4be5be32] {\n    height: 100px;\n}\n.username[data-v-4be5be32] {\n    font-size: 13px;\n}\n.gallery-thumbs .swiper-slide[data-v-4be5be32] {\n    width: 100%;\n    height: 30vh;\n}\n.top[data-v-4be5be32] {\n    height: auto;\n}\n.mobile[data-v-4be5be32] {\n    display: block;\n}\n.desktop[data-v-4be5be32] {\n    display: none;\n}\n.videoContainer[data-v-4be5be32],\n  .courseContainer[data-v-4be5be32],\n  .articleContainer[data-v-4be5be32],\n  .researchContainer[data-v-4be5be32] {\n    width: 80%;\n}\n.leftContainer[data-v-4be5be32] {\n    position: absolute;\n    bottom: 0;\n    width: 90%;\n    height: 250px;\n    min-height: 100px;\n    z-index: 2;\n    left: 5%;\n}\n.middle[data-v-4be5be32] {\n    height: 0;\n    display: block;\n}\n.mobile .swiper-container[data-v-4be5be32] {\n    background: unset;\n    height: 330px;\n}\n.diverHeader[data-v-4be5be32] {\n    font-size: 18px;\n    padding: 30px 0 5px;\n}\n.inText[data-v-4be5be32] {\n    position: absolute;\n    top: 12%;\n    left: 5%;\n    color: #fff;\n    font-size: 1.7em;\n}\n.cover[data-v-4be5be32] {\n    height: 200px;\n}\n.businessBg[data-v-4be5be32] {\n    padding: 15px 19px 250px 25px;\n    margin: 0;\n}\n.businessBgVid[data-v-4be5be32] {\n    padding: 15px 19px 250px 25px;\n    margin: 0;\n}\n.businessBgPod[data-v-4be5be32] {\n    padding: 15px 19px 250px 25px;\n    margin: 0;\n}\n.leftTitle[data-v-4be5be32] {\n    font-size: 28px;\n}\n.inText[data-v-4be5be32] {\n    font-size: 18px;\n}\n.inText1[data-v-4be5be32] {\n    font-size: 16px;\n    width: 100%;\n}\n.recentHeader[data-v-4be5be32] {\n    font-size: 16px;\n}\n.feat[data-v-4be5be32] {\n    font-size: 20px;\n    padding-top: 5px;\n}\n.recentVid[data-v-4be5be32] {\n    padding: 10px;\n}\n.diverHeader[data-v-4be5be32] {\n    font-size: 20px;\n}\n.overviewMain p[data-v-4be5be32] {\n    font-size: 16px !important;\n}\np[data-v-4be5be32] {\n    font-size: 16px;\n}\n.subNav[data-v-4be5be32]::before {\n    display: none;\n}\n.subNav[data-v-4be5be32] {\n    right: 10px;\n    top: 3%;\n    background: #fff;\n    padding: 15px;\n}\n.results[data-v-4be5be32] {\n    width: 95%;\n}\n.pageNav .pageNavi .mainNav[data-v-4be5be32] {\n    font-size: 16px;\n    padding: 10px 20px;\n}\n.pageMenu[data-v-4be5be32] {\n    background: #fafafa;\n    width: 100%;\n    min-height: 40px;\n    position: absolute;\n    left: 0;\n    top: 35px;\n    text-align: left;\n}\n.overviewMain[data-v-4be5be32] {\n    font-size: 15px;\n}\n}\n@media (max-width: 375px) {\n.leftTitle[data-v-4be5be32] {\n    font-size: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent__ = __webpack_require__(1007);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_userBannerComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_categoriesBarComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent__ = __webpack_require__(1012);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_maincontentComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_subscribeComponent__ = __webpack_require__(1017);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_subscribeComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_subscribeComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_testimonialComponent__ = __webpack_require__(944);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_testimonialComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_testimonialComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_expertComponent__ = __webpack_require__(1022);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_expertComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_expertComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config__ = __webpack_require__(669);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//










/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-explore-page",
  data: function data() {
    var _ref;

    return _ref = {
      videoProduct: [],
      researchProduct: [],
      podcastProduct: [],
      articleProduct: [],
      courseProduct: [],
      allProduct: [],
      allVendors: [],
      isActive: true,
      search: "",
      overdue: false,
      swiperOptionTop: {
        slidesPerView: 1,
        slidesPerColumn: 1,
        loop: true,
        effect: "fade",
        centeredSlides: true,
        autoplay: {
          delay: 4500,
          disableOnInteraction: true
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        },

        pagination: {
          el: ".swiper-pagination",
          clickable: true
        }
      },
      swiperOption: {
        slidesPerView: 4,
        spaceBetween: 30,
        breakpoints: {
          425: {
            slidesPerView: 2,
            spaceBetween: 10
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 15
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        },

        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev"
        }
      },
      swiperOptionBusiness: {
        direction: "vertical",
        zoom: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        },
        effect: "fade",
        loop: true,
        autoplay: {
          delay: 6000,
          disableOnInteraction: true
        }
      },
      userSub: null,
      freeTrial: null
    }, _defineProperty(_ref, "userSub", null), _defineProperty(_ref, "user", {}), _defineProperty(_ref, "inLibrary", null), _defineProperty(_ref, "checkLibrary", []), _defineProperty(_ref, "showUpgrade", false), _defineProperty(_ref, "allSubjectMatters", []), _defineProperty(_ref, "subject", ""), _defineProperty(_ref, "page", false), _defineProperty(_ref, "sideNav", false), _defineProperty(_ref, "prevTop", 0), _defineProperty(_ref, "prevVid", 0), _defineProperty(_ref, "prevArt", 0), _ref;
  },

  components: {
    Expert: __WEBPACK_IMPORTED_MODULE_5__components_expertComponent___default.a
  },

  mounted: function mounted() {
    var _this = this;

    this.isActive = true;
    this.updateList();
    this.getAllProducts();
    this.checkStatus();
    this.getVendors();
    this.getSubjectMatters();

    setTimeout(function () {
      if (_this.isActive) {
        _this.overdue = true;
      }
    }, 15000);
  },


  watch: {
    $route: function $route() {
      this.updateList();
    },

    subject: "subjectPage"
  },
  methods: {
    goToDemo: function goToDemo() {
      var user = localStorage.getItem("authUser");
      if (user !== null) {
        var demo = localStorage.getItem("demo");

        this.$router.push({
          name: "AccountingDemo",
          query: {
            redirect_to: "demo"
          }
        });
      } else {
        this.$router.push({
          name: "auth",
          params: { name: "register" },
          query: {
            redirect_from: "explore"
          }
        });
      }
    },
    openSide: function openSide() {
      this.sideNav = !this.sideNav;
    },
    subjectPage: function subjectPage() {
      if (this.subject === "") {
        this.page = false;
      } else {
        this.page = true;
      }
    },
    clickedSubject: function clickedSubject() {
      this.subject;
    },
    getSubjectMatters: function getSubjectMatters() {
      var _this2 = this;

      axios.get("/api/subjectmatter").then(function (response) {
        _this2.allSubjectMatters = response.data.data;
      });
    },
    getVendors: function getVendors() {
      var _this3 = this;

      axios.get("/api/get-all-vendor").then(function (response) {
        response.data.forEach(function (item) {
          if (item.id !== 1) {
            _this3.allVendors.push(item);
          }
        });
      });
    },
    checkStatus: function checkStatus() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      this.user = user;
      if (user != null) {
        axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_6__config__["b" /* getCustomerHeader */])() }).then(function (response) {
          var userDate = response.data.created_at;
          _this4.startDate = new Date(userDate);

          _this4.startDateParsed = Date.parse(new Date(userDate));
          _this4.currentDate = Date.parse(new Date());

          _this4.endDate = Date.parse(new Date(_this4.startDate.setDate(_this4.startDate.getDate(userDate) + 30)));

          if (_this4.currentDate >= _this4.endDate) {
            _this4.freeTrial = false;
          } else {}
        });

        this.token = user.access_token;
        axios.get("/api/user/orders", {
          headers: { Authorization: "Bearer " + user.access_token }
        }).then(function (response) {
          response.data.data.forEach(function (element) {
            element.orderDetail.forEach(function (item) {
              if (item.type === "VIDEOS") {
                _this4.checkLibrary.push(item);
              }
            });
          });
        });
        axios.post("/api/user/subscription-plan/" + user.id).then(function (response) {
          if (response.status === 200) {
            if (response.data.length !== 0) {
              _this4.userSub = Number(response.data[0].level);
            }
          }
        });
      }
    },
    loading: function loading() {
      this.isActive = true;
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    addVideoToLibrary: function addVideoToLibrary(id, name, level) {
      var _this5 = this;

      var data = {
        id: id,
        prodType: "Videos",
        getType: "Insight Subscription Video",
        name: name
      };

      if (this.freeTrial === true || this.userSub > 0) {
        if (this.userSub >= level) {
          axios.post("/api/user/sendvideotolibrary", JSON.parse(JSON.stringify(data)), {
            headers: {
              Authorization: "Bearer " + this.user.access_token
            }
          }).then(function (response) {
            if (response.status === 201) {
              _this5.$toasted.success("Successfully added to BizLibrary");
              _this5.inLibrary = true;
            } else if (response.data == "Order Already Added") {
              _this5.$toasted.error("You have already added this product before");
            } else if (response.data.status === 99 || response.data === " " || response.data.data === "error") {
              _this5.showUpgrade = true;
            } else {
              _this5.showUpgrade = true;
            }
          }).catch(function (error) {
            if (error.response.data.message === "Unauthenticated.") {
              _this5.$router.push({
                name: "auth",
                params: { name: "login" },
                query: { redirect: _this5.$route.fullPath }
              });
            } else {
              _this5.$toasted.error(error.response.data.message);
              console.log(error.response.data.message);
            }
          });
        } else {
          this.showUpgrade = true;
        }
      } else {
        this.showUpgrade = true;
      }
    },
    watch: function watch(id) {
      this.$router.push({
        name: "Video",
        params: { id: id }
      });
    },
    getAllProducts: function getAllProducts() {
      var _this6 = this;

      axios.get("/api/products").then(function (response) {
        if (response.status === 200) {
          _this6.isActive = false;
          _this6.allProduct = response.data.data;
          response.data.data.forEach(function (item) {
            if (item.prodType == "Market Research") {
              item.marketResearch.coverImage = item.coverImage;
              item.marketResearch.subLevel = item.subscriptionLevel;
              _this6.researchProduct.push(item.marketResearch);
              _this6.researchProduct = _this6.researchProduct.reverse();
            } else if (item.prodType == "Videos") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this6.videoProduct.push(item.webinar);
              _this6.videoProduct = _this6.videoProduct.reverse();
            } else if (item.prodType == "Podcast") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              _this6.podcastProduct.push(item.webinar);
            } else if (item.prodType == "Articles") {
              item.articles.coverImage = item.coverImage;
              item.articles.subLevel = item.subscriptionLevel;
              _this6.articleProduct.push(item.articles);
              _this6.articleProduct = _this6.articleProduct.reverse();
            } else if (item.prodType == "Courses") {
              item.courses.coverImage = item.coverImage;
              item.courses.subLevel = item.subscriptionLevel;

              _this6.courseProduct.push(item.courses);
              _this6.courseProduct = _this6.courseProduct.reverse();
            } else {
              return;
            }
          });
        }
      });
    },
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    redirectUpgrade: function redirectUpgrade(level) {
      this.showUpgrade = false;
      var routeData = this.$router.resolve({
        name: "SubscriptionProfile",
        params: {
          level: this.videoProduct[0].subLevel
        }
      });
    }
  },

  computed: {
    videoSearch: function videoSearch() {
      var _this7 = this;

      return this.videoProduct.filter(function (item) {
        return item.title.toLowerCase().includes(_this7.search.toLowerCase());
      });
    },
    articleSearch: function articleSearch() {
      var _this8 = this;

      return this.articleProduct.filter(function (item) {
        return item.title.toLowerCase().includes(_this8.search.toLowerCase());
      });
    },
    researchSearch: function researchSearch() {
      var _this9 = this;

      return this.researchProduct.filter(function (item) {
        return item.title.toLowerCase().includes(_this9.search.toLowerCase());
      });
    },
    courseSearch: function courseSearch() {
      var _this10 = this;

      return this.courseProduct.filter(function (item) {
        return item.title.toLowerCase().includes(_this10.search.toLowerCase());
      });
    }
  }
});

/***/ }),

/***/ 1379:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm.overdue
            ? _c("p", { staticClass: "overdue" }, [
                _vm._v("something is wrong.. please refresh page")
              ])
            : _vm._e()
        ])
      : _c(
          "div",
          { staticClass: "main" },
          [
            _vm.showUpgrade
              ? _c("div", { staticClass: "upgradeContainer animated fadeIn" }, [
                  _c("div", { staticClass: "upgrade" }, [
                    _c("p", { staticClass: "upgradeText" }, [
                      _vm._v(
                        "Oops, looks like you need to upgrade to access this. It’ll only take a sec though. 😁"
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "upgradeButtons" }, [
                      _c(
                        "div",
                        {
                          staticClass: "btn-info rounded shadow-sm",
                          on: { click: _vm.redirectUpgrade }
                        },
                        [_vm._v("Yes please!")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "btn-secondary rounded shadow-sm",
                          on: { click: _vm.closeUpgrade }
                        },
                        [_vm._v("I'll keep exploring")]
                      )
                    ])
                  ])
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "mobile slide" }, [
              _c("div", [
                _c("i", {
                  staticClass: "fas fa-sliders-h",
                  on: { click: _vm.openSide }
                })
              ]),
              _vm._v(" "),
              _vm.sideNav
                ? _c("div", { staticClass: "pageMenu mobile" }, [
                    _c("div", { staticClass: "pageNav" }, [
                      _c("ul", { staticClass: "pageNavi" }, [
                        _c("li", { staticClass: "mainNav" }, [
                          _vm._v(
                            "\n                Knowledge Area\n                "
                          ),
                          _c("i", {
                            staticClass: "fa fa-caret-right",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "subNav" },
                            [
                              _c("li", { staticClass: "navy" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.subject,
                                      expression: "subject"
                                    }
                                  ],
                                  staticClass: "form-check-input checkers",
                                  attrs: {
                                    type: "radio",
                                    name: "",
                                    id: "all",
                                    value: ""
                                  },
                                  domProps: {
                                    checked: _vm._q(_vm.subject, "")
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.subject = ""
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  {
                                    staticClass: "toCaps mainFontColor",
                                    attrs: { for: "all" }
                                  },
                                  [_vm._v("All")]
                                )
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.allSubjectMatters, function(
                                matter,
                                index
                              ) {
                                return _c(
                                  "li",
                                  { key: index, staticClass: "navy" },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.subject,
                                          expression: "subject"
                                        }
                                      ],
                                      staticClass: "form-check-input checkers",
                                      attrs: {
                                        type: "radio",
                                        name: "",
                                        id: matter.name
                                      },
                                      domProps: {
                                        value: matter.id,
                                        checked: _vm._q(_vm.subject, matter.id)
                                      },
                                      on: {
                                        change: function($event) {
                                          _vm.subject = matter.id
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "label",
                                      {
                                        staticClass: "toCaps mainFontColor",
                                        attrs: { for: matter.name }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(matter.name.toLowerCase())
                                        )
                                      ]
                                    )
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c("li", { staticClass: "mainNav" }, [
                          _vm._v("\n                Watch\n                "),
                          _c("i", {
                            staticClass: "fa fa-caret-right",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c("ul", { staticClass: "subNav" }, [
                            !_vm.page
                              ? _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: { to: "/entrepreneur/videos" }
                                      },
                                      [_vm._v("Video")]
                                    )
                                  ],
                                  1
                                )
                              : _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: {
                                          to: {
                                            name: "SubjectMatters",
                                            params: {
                                              id: _vm.subject,
                                              name: "video"
                                            }
                                          }
                                        }
                                      },
                                      [_vm._v("Video")]
                                    )
                                  ],
                                  1
                                )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", { staticClass: "mainNav" }, [
                          _vm._v("\n                Read\n                "),
                          _c("i", {
                            staticClass: "fa fa-caret-right",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c("ul", { staticClass: "subNav" }, [
                            !_vm.page
                              ? _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: { to: "/entrepreneur/articles" }
                                      },
                                      [_vm._v("Article")]
                                    )
                                  ],
                                  1
                                )
                              : _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: {
                                          to: {
                                            name: "SubjectMatters",
                                            params: {
                                              id: _vm.subject,
                                              name: "article"
                                            }
                                          }
                                        }
                                      },
                                      [_vm._v("Article")]
                                    )
                                  ],
                                  1
                                ),
                            _vm._v(" "),
                            _c("hr"),
                            _vm._v(" "),
                            !_vm.page
                              ? _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: {
                                          to:
                                            "/entrepreneur/paper/market-research"
                                        }
                                      },
                                      [_vm._v("Books")]
                                    )
                                  ],
                                  1
                                )
                              : _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: {
                                          to: {
                                            name: "SubjectMatters",
                                            params: {
                                              id: _vm.subject,
                                              name: "market-research"
                                            }
                                          }
                                        }
                                      },
                                      [_vm._v("Books")]
                                    )
                                  ],
                                  1
                                )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", { staticClass: "mainNav" }, [
                          _vm._v("\n                Study\n                "),
                          _c("i", {
                            staticClass: "fa fa-caret-right",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" "),
                          _c("ul", { staticClass: "subNav" }, [
                            !_vm.page
                              ? _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: { to: "/entrepreneur/courses" }
                                      },
                                      [_vm._v("Course")]
                                    )
                                  ],
                                  1
                                )
                              : _c(
                                  "li",
                                  [
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "mainFontColor",
                                        attrs: {
                                          to: {
                                            name: "SubjectMatters",
                                            params: {
                                              id: _vm.subject,
                                              name: "course"
                                            }
                                          }
                                        }
                                      },
                                      [_vm._v("Course")]
                                    )
                                  ],
                                  1
                                )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("li", { staticClass: "mainNav" }, [
                          _c("span", { staticClass: "search" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.search,
                                  expression: "search"
                                }
                              ],
                              staticClass: "form-control rounded-pill",
                              attrs: {
                                type: "text",
                                id: "input",
                                placeholder: "search"
                              },
                              domProps: { value: _vm.search },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.search = $event.target.value
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("i", {
                              staticClass: "fa fa-search",
                              attrs: { "aria-hidden": "true" }
                            })
                          ]),
                          _vm._v(" "),
                          _vm.search !== ""
                            ? _c("div", { staticClass: "results" }, [
                                _vm.videoSearch.length > 0
                                  ? _c(
                                      "div",
                                      _vm._l(_vm.videoSearch, function(
                                        video,
                                        index
                                      ) {
                                        return _c(
                                          "p",
                                          {
                                            key: index,
                                            staticClass: "searchResult toCaps"
                                          },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                staticClass:
                                                  "searchResult toCaps",
                                                attrs: {
                                                  to: {
                                                    name: "Video",
                                                    params: {
                                                      id: video.product_id
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    video.title.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      }),
                                      0
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.articleSearch.length > 0
                                  ? _c(
                                      "div",
                                      _vm._l(_vm.articleSearch, function(
                                        article,
                                        index
                                      ) {
                                        return _c(
                                          "p",
                                          {
                                            key: index,
                                            staticClass: "searchResult toCaps"
                                          },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                staticClass:
                                                  "searchResult toCaps",
                                                attrs: {
                                                  to: {
                                                    name: "ArticleSinglePage",
                                                    params: {
                                                      id: article.product_id,
                                                      name: article.title.replace(
                                                        /[^a-z0-9]/gi,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    article.title.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      }),
                                      0
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.courseSearch.length > 0
                                  ? _c(
                                      "div",
                                      _vm._l(_vm.courseSearch, function(
                                        course,
                                        index
                                      ) {
                                        return _c(
                                          "p",
                                          { key: index },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                staticClass:
                                                  "searchResult toCaps",
                                                attrs: {
                                                  to: {
                                                    name: "CourseFullPage",
                                                    params: {
                                                      id: course.product_id,
                                                      name: course.title.replace(
                                                        / /g,
                                                        "-"
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    course.title.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      }),
                                      0
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.researchSearch.length > 0
                                  ? _c(
                                      "div",
                                      _vm._l(_vm.researchSearch, function(
                                        research,
                                        index
                                      ) {
                                        return _c(
                                          "p",
                                          {
                                            key: index,
                                            staticClass: "searchResult toCaps"
                                          },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                staticClass:
                                                  "searchResult toCaps",
                                                attrs: {
                                                  to: {
                                                    name: "ProductPaperDetail",
                                                    params: {
                                                      id: research.product_id,
                                                      name: (research.title
                                                        ? research.title
                                                            .replace(
                                                              /[^a-z0-9]/gi,
                                                              ""
                                                            )
                                                            .toLowerCase()
                                                        : research.articleTitle
                                                      )
                                                        .replace(
                                                          /[^a-z0-9]/gi,
                                                          ""
                                                        )
                                                        .toLowerCase(),
                                                      type: "subscribe"
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    research.title.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      }),
                                      0
                                    )
                                  : _vm._e()
                              ])
                            : _vm._e()
                        ])
                      ])
                    ])
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("section", { staticClass: "pageMenu desktop" }, [
              _c("div", { staticClass: "pageNav" }, [
                _c("ul", { staticClass: "pageNavi" }, [
                  _c("li", { staticClass: "mainNav" }, [
                    _vm._v("\n              Knowledge Area\n              "),
                    _c("i", {
                      staticClass: "fa fa-caret-down",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c(
                      "ul",
                      { staticClass: "subNav" },
                      [
                        _c("li", { staticClass: "navy" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.subject,
                                expression: "subject"
                              }
                            ],
                            staticClass: "form-check-input checkers",
                            attrs: {
                              type: "radio",
                              name: "",
                              id: "all",
                              value: ""
                            },
                            domProps: { checked: _vm._q(_vm.subject, "") },
                            on: {
                              change: function($event) {
                                _vm.subject = ""
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "label",
                            {
                              staticClass: "toCaps mainFontColor",
                              attrs: { for: "all" }
                            },
                            [_vm._v("All")]
                          )
                        ]),
                        _vm._v(" "),
                        _vm._l(_vm.allSubjectMatters, function(matter, index) {
                          return _c("li", { key: index, staticClass: "navy" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.subject,
                                  expression: "subject"
                                }
                              ],
                              staticClass: "form-check-input checkers",
                              attrs: {
                                type: "radio",
                                name: "",
                                id: matter.name
                              },
                              domProps: {
                                value: matter.id,
                                checked: _vm._q(_vm.subject, matter.id)
                              },
                              on: {
                                change: function($event) {
                                  _vm.subject = matter.id
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "toCaps mainFontColor",
                                attrs: { for: matter.name }
                              },
                              [_vm._v(_vm._s(matter.name.toLowerCase()))]
                            )
                          ])
                        })
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "mainNav" }, [
                    _vm._v("\n              Watch\n              "),
                    _c("i", {
                      staticClass: "fa fa-caret-down",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticClass: "subNav" }, [
                      !_vm.page
                        ? _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: { to: "/entrepreneur/videos" }
                                },
                                [_vm._v("Video")]
                              )
                            ],
                            1
                          )
                        : _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: {
                                    to: {
                                      name: "SubjectMatters",
                                      params: { id: _vm.subject, name: "video" }
                                    }
                                  }
                                },
                                [_vm._v("Video")]
                              )
                            ],
                            1
                          )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "mainNav" }, [
                    _vm._v("\n              Read\n              "),
                    _c("i", {
                      staticClass: "fa fa-caret-down",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticClass: "subNav" }, [
                      !_vm.page
                        ? _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: { to: "/articles" }
                                },
                                [_vm._v("Article")]
                              )
                            ],
                            1
                          )
                        : _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: {
                                    to: {
                                      name: "SubjectMatters",
                                      params: {
                                        id: _vm.subject,
                                        name: "article"
                                      }
                                    }
                                  }
                                },
                                [_vm._v("Article")]
                              )
                            ],
                            1
                          ),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      !_vm.page
                        ? _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: { to: "/paper/market-research" }
                                },
                                [_vm._v("Books")]
                              )
                            ],
                            1
                          )
                        : _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: {
                                    to: {
                                      name: "SubjectMatters",
                                      params: {
                                        id: _vm.subject,
                                        name: "market-research"
                                      }
                                    }
                                  }
                                },
                                [_vm._v("Books")]
                              )
                            ],
                            1
                          )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "mainNav" }, [
                    _vm._v("\n              Study\n              "),
                    _c("i", {
                      staticClass: "fa fa-caret-down",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" "),
                    _c("ul", { staticClass: "subNav" }, [
                      !_vm.page
                        ? _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: { to: "/entrepreneur/courses" }
                                },
                                [_vm._v("Course")]
                              )
                            ],
                            1
                          )
                        : _c(
                            "li",
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "mainFontColor",
                                  attrs: {
                                    to: {
                                      name: "SubjectMatters",
                                      params: {
                                        id: _vm.subject,
                                        name: "course"
                                      }
                                    }
                                  }
                                },
                                [_vm._v("Course")]
                              )
                            ],
                            1
                          )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", [
                _c("ul", [
                  _c("li", { staticClass: "mainNav" }, [
                    _c("span", { staticClass: "search" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.search,
                            expression: "search"
                          }
                        ],
                        staticClass: "form-control rounded-pill",
                        attrs: {
                          type: "text",
                          id: "input",
                          placeholder: "search"
                        },
                        domProps: { value: _vm.search },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.search = $event.target.value
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        staticClass: "fa fa-search",
                        attrs: { "aria-hidden": "true" }
                      })
                    ]),
                    _vm._v(" "),
                    _vm.search !== ""
                      ? _c("div", { staticClass: "results" }, [
                          _vm.videoSearch.length > 0
                            ? _c(
                                "div",
                                _vm._l(_vm.videoSearch, function(video, index) {
                                  return _c(
                                    "p",
                                    {
                                      key: index,
                                      staticClass: "searchResult toCaps"
                                    },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "searchResult toCaps",
                                          attrs: {
                                            to: {
                                              name: "Video",
                                              params: { id: video.product_id }
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(video.title.toLowerCase())
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                }),
                                0
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.articleSearch.length > 0
                            ? _c(
                                "div",
                                _vm._l(_vm.articleSearch, function(
                                  article,
                                  index
                                ) {
                                  return _c(
                                    "p",
                                    {
                                      key: index,
                                      staticClass: "searchResult toCaps"
                                    },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "searchResult toCaps",
                                          attrs: {
                                            to: {
                                              name: "ArticleSinglePage",
                                              params: {
                                                id: article.product_id,
                                                name: article.title.replace(
                                                  /[^a-z0-9]/gi,
                                                  ""
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(article.title.toLowerCase())
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                }),
                                0
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.courseSearch.length > 0
                            ? _c(
                                "div",
                                _vm._l(_vm.courseSearch, function(
                                  course,
                                  index
                                ) {
                                  return _c(
                                    "p",
                                    { key: index },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "searchResult toCaps",
                                          attrs: {
                                            to: {
                                              name: "CourseFullPage",
                                              params: {
                                                id: course.product_id,
                                                name: course.title.replace(
                                                  / /g,
                                                  "-"
                                                )
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(course.title.toLowerCase())
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                }),
                                0
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.researchSearch.length > 0
                            ? _c(
                                "div",
                                _vm._l(_vm.researchSearch, function(
                                  research,
                                  index
                                ) {
                                  return _c(
                                    "p",
                                    {
                                      key: index,
                                      staticClass: "searchResult toCaps"
                                    },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "searchResult toCaps",
                                          attrs: {
                                            to: {
                                              name: "ProductPaperDetail",
                                              params: {
                                                id: research.product_id,
                                                name: (research.title
                                                  ? research.title
                                                      .replace(
                                                        /[^a-z0-9]/gi,
                                                        ""
                                                      )
                                                      .toLowerCase()
                                                  : research.articleTitle
                                                )
                                                  .replace(/[^a-z0-9]/gi, "")
                                                  .toLowerCase(),
                                                type: "subscribe"
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(research.title.toLowerCase())
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                }),
                                0
                              )
                            : _vm._e()
                        ])
                      : _vm._e()
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "section",
              { staticClass: "top pb-5" },
              [
                _c(
                  "swiper",
                  {
                    ref: "swiperTop",
                    staticClass: "gallery-top first",
                    attrs: { options: _vm.swiperOptionTop }
                  },
                  [
                    _vm.videoProduct.length > 0
                      ? _c("swiper-slide", { staticClass: "slide-1 d-flex" }, [
                          _c("div", { staticClass: "leftContainer" }, [
                            _c("div", { staticClass: "controlContainer" }, [
                              _c("h3", { staticClass: "leftTitle" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.videoProduct[0].title.toLowerCase()
                                  )
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "overviewMain" }, [
                                _vm._v(_vm._s(_vm.videoProduct[0].description))
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "controller" },
                                [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn bttn-primary1",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.watch(
                                            _vm.videoProduct[0].product_id
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-play",
                                        attrs: { "aria-hidden": "true" }
                                      }),
                                      _vm._v(" Watch\n                  ")
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn bttn-primary",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.addVideoToLibrary(
                                            _vm.videoProduct[0].product_id,
                                            _vm.videoProduct[0].title,
                                            _vm.videoProduct[0].subLevel
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-plus",
                                        attrs: { "aria-hidden": "true" }
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "hov" }, [
                                        _vm._v("Add to BizLibrary")
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    { attrs: { to: "/entrepreneur/videos" } },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn bttn-primary",
                                          attrs: { type: "button" }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-list",
                                            attrs: { "aria-hidden": "true" }
                                          }),
                                          _vm._v(" "),
                                          _c("span", { staticClass: "hov" }, [
                                            _vm._v("View all Videos")
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "videoContainer" }, [
                            _c("h2", { staticClass: "videos" }, [
                              _vm._v("Videos")
                            ]),
                            _vm._v(" "),
                            _c("img", {
                              attrs: {
                                src: _vm.videoProduct[0].coverImage,
                                alt: " test image"
                              }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "leftShadow" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "bottomShadow" })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.articleProduct.length > 0
                      ? _c("swiper-slide", { staticClass: "slide-3 d-flex" }, [
                          _c("div", { staticClass: "leftContainer" }, [
                            _c("div", { staticClass: "controlContainer" }, [
                              _c("h3", { staticClass: "leftTitle" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.articleProduct[0].title.toLowerCase()
                                  )
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "overviewMain",
                                  domProps: {
                                    innerHTML: _vm._s(
                                      _vm.articleProduct[0].description
                                    )
                                  }
                                },
                                [_vm._v("{")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "controller" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "ArticleSinglePage",
                                          params: {
                                            id:
                                              _vm.articleProduct[0].product_id,
                                            name: _vm.articleProduct[0].title.replace(
                                              /[^a-z0-9]/gi,
                                              ""
                                            )
                                          }
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn bttn-primary1",
                                          attrs: { type: "button" }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-play",
                                            attrs: { "aria-hidden": "true" }
                                          }),
                                          _vm._v(" Read\n                    ")
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    { attrs: { to: "/entrepreneur/articles" } },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn bttn-primary",
                                          attrs: { type: "button" }
                                        },
                                        [
                                          _c("span", { staticClass: "hov" }, [
                                            _vm._v("View all Articles")
                                          ]),
                                          _vm._v(" "),
                                          _c("i", {
                                            staticClass: "fa fa-list",
                                            attrs: { "aria-hidden": "true" }
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm.articleProduct.length > 0
                            ? _c("div", { staticClass: "articleContainer" }, [
                                _c("h2", { staticClass: "videos" }, [
                                  _vm._v("Articles")
                                ]),
                                _vm._v(" "),
                                _c("img", {
                                  attrs: {
                                    src: _vm.articleProduct[0].coverImage,
                                    alt: " test image"
                                  }
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "leftShadow" }),
                                _vm._v(" "),
                                _c("div", { staticClass: "bottomShadow" })
                              ])
                            : _vm._e()
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.courseProduct.length > 0
                      ? _c("swiper-slide", { staticClass: "slide-4 d-flex" }, [
                          _c("div", { staticClass: "leftContainer" }, [
                            _c("div", { staticClass: "controlContainer" }, [
                              _c("h3", { staticClass: "leftTitle" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.courseProduct[0].title.toLowerCase()
                                  )
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "overviewMain" }, [
                                _vm._v(_vm._s(_vm.courseProduct[0].overview))
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "controller" },
                                [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn bttn-primary1",
                                      attrs: { type: "button" }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-play",
                                        attrs: { "aria-hidden": "true" }
                                      }),
                                      _vm._v(" View\n                  ")
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn bttn-primary",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.addVideoToLibrary(
                                            _vm.courseProduct[0].product_id,
                                            _vm.courseProduct[0].title,
                                            _vm.courseProduct[0].subLevel
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("span", { staticClass: "hov" }, [
                                        _vm._v("Add BizLibrary")
                                      ]),
                                      _vm._v(" "),
                                      _c("i", {
                                        staticClass: "fa fa-plus",
                                        attrs: { "aria-hidden": "true" }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    { attrs: { to: "/entrepreneur/courses" } },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn bttn-primary",
                                          attrs: { type: "button" }
                                        },
                                        [
                                          _c("span", { staticClass: "hov" }, [
                                            _vm._v("View all Courses")
                                          ]),
                                          _vm._v(" "),
                                          _c("i", {
                                            staticClass: "fa fa-list",
                                            attrs: { "aria-hidden": "true" }
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "courseContainer" }, [
                            _c("h2", { staticClass: "videos" }, [
                              _vm._v("Courses")
                            ]),
                            _vm._v(" "),
                            _c("img", {
                              attrs: {
                                src: _vm.courseProduct[0].coverImage,
                                alt: " test image"
                              }
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "leftShadow" }),
                            _vm._v(" "),
                            _c("div", { staticClass: "bottomShadow" })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", {
                      staticClass:
                        "swiper-button-next swiper-button-white carousel-nav-next",
                      attrs: { slot: "button-next" },
                      slot: "button-next"
                    }),
                    _vm._v(" "),
                    _c("div", {
                      staticClass:
                        "swiper-button-prev swiper-button-white carousel-nav-prev",
                      attrs: { slot: "button-prev" },
                      slot: "button-prev"
                    }),
                    _vm._v(" "),
                    _c("div", {
                      staticClass: "swiper-pagination",
                      attrs: { slot: "pagination" },
                      slot: "pagination"
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("section", { staticClass: "mini-content" }, [
              _c(
                "div",
                { staticClass: "video-content" },
                [
                  _c("p", { staticClass: "mini-content-title" }, [
                    _vm._v("Featured Videos")
                  ]),
                  _vm._v(" "),
                  _vm.videoProduct.length
                    ? _c(
                        "swiper",
                        {
                          staticClass: "p-3",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.videoProduct, function(item, idx) {
                            return _c(
                              "swiper-slide",
                              { key: idx },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "Video",
                                        params: { id: item.product_id }
                                      }
                                    }
                                  },
                                  [
                                    _c("div", { staticClass: "card shadow" }, [
                                      _c(
                                        "div",
                                        { staticClass: "card-img-top" },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src: item.coverImage,
                                              alt: ""
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("div", {
                                            staticClass: "img-overlay"
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "card-body" }, [
                                        _c(
                                          "h4",
                                          { staticClass: "card-title" },
                                          [
                                            _vm._v(
                                              _vm._s(item.title.toLowerCase())
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("p", { staticClass: "card-text" }, [
                                          _vm._v(_vm._s(item.description))
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "card-bottom" },
                                          [_vm._v("Video")]
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              ],
                              1
                            )
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass:
                              "swiper-button-next swiper-button-black",
                            attrs: { slot: "button-next" },
                            slot: "button-next"
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass:
                              "swiper-button-prev swiper-button-black",
                            attrs: { slot: "button-prev" },
                            slot: "button-prev"
                          })
                        ],
                        2
                      )
                    : _vm._e()
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "article-content" },
                [
                  _c("p", { staticClass: "mini-content-title" }, [
                    _vm._v("Featured Articles")
                  ]),
                  _vm._v(" "),
                  _vm.articleProduct.length
                    ? _c(
                        "swiper",
                        {
                          staticClass: "p-3",
                          attrs: { options: _vm.swiperOption }
                        },
                        [
                          _vm._l(_vm.articleProduct, function(item, idx) {
                            return _c(
                              "swiper-slide",
                              { key: idx },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "ArticleSinglePage",
                                        params: {
                                          id: item.product_id,
                                          name: item.title.replace(
                                            /[^a-z0-9]/gi,
                                            ""
                                          )
                                        }
                                      }
                                    }
                                  },
                                  [
                                    _c("div", { staticClass: "card shadow" }, [
                                      _c(
                                        "div",
                                        { staticClass: "card-img-top" },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src: item.coverImage,
                                              alt: ""
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("div", {
                                            staticClass: "img-overlay"
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "card-body" }, [
                                        _c(
                                          "h4",
                                          { staticClass: "card-title" },
                                          [
                                            _vm._v(
                                              _vm._s(item.title.toLowerCase())
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("p", {
                                          staticClass: "card-text",
                                          domProps: {
                                            innerHTML: _vm._s(item.description)
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "card-bottom" },
                                          [_vm._v("Article")]
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              ],
                              1
                            )
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass:
                              "swiper-button-next swiper-button-black",
                            attrs: { slot: "button-next" },
                            slot: "button-next"
                          }),
                          _vm._v(" "),
                          _c("div", {
                            staticClass:
                              "swiper-button-prev swiper-button-black",
                            attrs: { slot: "button-prev" },
                            slot: "button-prev"
                          })
                        ],
                        2
                      )
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("expert"),
            _vm._v(" "),
            _c("section", {}, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "bms" }, [
                _c("div", { staticClass: "accounting_slider" }, [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("div", { staticClass: "leftSlide" }, [
                    _vm._m(3),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "slide_button" },
                      [
                        _c(
                          "button",
                          {
                            staticClass:
                              "elevated_btn btn-compliment text-white",
                            attrs: { type: "button" },
                            on: { click: _vm.goToDemo }
                          },
                          [_vm._v("Free Demo Now")]
                        ),
                        _vm._v(" "),
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "SubscriptionProfile",
                                params: {
                                  level: 0
                                },
                                query: {
                                  name: "subscribe-accounting"
                                }
                              }
                            }
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass:
                                  "elevated_btn btn-outline-compliment bg-white",
                                attrs: { type: "button" }
                              },
                              [_vm._v("Subscribe")]
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ])
              ])
            ])
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bms-heading" }, [
      _c("strong", [_vm._v("Business Management Solutions")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "rightSlide" }, [
      _c("img", {
        staticClass: "bms_img",
        attrs: { src: "/images/word.png", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "w-100 mb-4" }, [
      _c("span", { staticClass: "text-main bold" }, [_vm._v("BE EMPOWERED")]),
      _vm._v(" - To make decisions that\n                "),
      _c("span", { staticClass: "text-main bold" }, [_vm._v("GROW")]),
      _vm._v(" your business\n              ")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4be5be32", module.exports)
  }
}

/***/ }),

/***/ 549:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1376)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1378)
/* template */
var __vue_template__ = __webpack_require__(1379)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4be5be32"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userInsightComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4be5be32", Component.options)
  } else {
    hotAPI.reload("data-v-4be5be32", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ }),

/***/ 883:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(884)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(886)
/* template */
var __vue_template__ = __webpack_require__(887)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a85176d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/categoriesBarComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a85176d", Component.options)
  } else {
    hotAPI.reload("data-v-7a85176d", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 884:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(885);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("35bd739c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a85176d\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./categoriesBarComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 885:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.overlay-bus[data-v-7a85176d]{\n  position:absolute;\n  width:100%;\n  height:100%;\n  top:0;\n  background:rgba(255, 255, 255,.5);\n  z-index:0;\n}\n.it[data-v-7a85176d]{\n   position:relative;\n}\n.howIt[data-v-7a85176d]{\n  padding:65px 0 100px;\n  z-index:3;\n  position:relative;\n}\n.form[data-v-7a85176d] {\n  width: 65%;\n  margin-right: auto;\n  background: hsl(207, 43%, 20%);\n}\nh3.text-center[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n}\n.bms-heading[data-v-7a85176d] {\n  width: 40%;\n  padding: 30px 30px 25px;\n  background-color: hsl(207, 43%, 20%);\n  border-bottom-right-radius: 100px;\n  color: #fff;\n  margin-left: -10px;\n}\n.expert-heading strong[data-v-7a85176d] {\n  font-size: 34px;\n  color: #fff;\n}\n.bms[data-v-7a85176d] {\n  padding: 0px;\n  width: 100%;\n  margin: 0 auto;\n  padding-top: 40px;\n  padding-bottom: 5px;\n}\n.bms_em[data-v-7a85176d]{\n  padding:40px;\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n  background:#f7f8fa;\n}\n.leftSlide p[data-v-7a85176d]{\n  font-size: 38px;\n  line-height:1.3;\n}\n.swiper[data-v-7a85176d] {\n  height: 600px;\n  width: 100%;\n}\n.accounting_slider[data-v-7a85176d] {\n  margin-top: 50px;\n  min-height: 400px;\n  overflow: hidden;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient:horizontal;\n  -webkit-box-direction:reverse;\n      -ms-flex-direction:row-reverse;\n          flex-direction:row-reverse;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width:80%;\n  margin:0 auto;\n}\n.leftSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  text-align:center;\n}\n.rightSlide[data-v-7a85176d] {\n  width: 50%;\n  height: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 0;\n}\n.rightSlide p[data-v-7a85176d]{\n  font-size: 32px;\n}\n.rightSlide img[data-v-7a85176d]{\n  width:70%;\n  height:auto;\n}\n.slide_button[data-v-7a85176d] {\n  width: 80%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin:0 auto\n}\n.my-slides[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n  background: white;\n}\n.bms_img[data-v-7a85176d]{\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: left;\n     object-position: left;\n}\n.profit[data-v-7a85176d] {\n  height: 50px;\n  margin: 30px auto 10px;\n}\n.gp[data-v-7a85176d] {\n  font-size: 16px;\n  color: #fff;\n  text-align: center;\n}\n.gProfit[data-v-7a85176d] {\n  font-size: 24px;\n  font-weight: bold;\n  color: #fff;\n}\n.w-40[data-v-7a85176d] {\n  width: 40% !important;\n}\n.w-60[data-v-7a85176d] {\n  width: 70% !important;\n}\n.spin[data-v-7a85176d] {\n  right: 10px;\n  top: 43%;\n}\n.boxes[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 80%;\n  min-height: 50vh;\n  margin: 0 auto;\n \n  padding-bottom: 45px;\n  padding-top: 60px;\n}\n.leftBox[data-v-7a85176d] {\n  width: 50%;\n  padding: 40px 0px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.rightBox[data-v-7a85176d] {\n  width: 50%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 60px 0px;\n}\n.generate-text[data-v-7a85176d] {\n  padding: 10px 0;\n  font-size: 32px;\n  line-height: 1.6;\n  color: rgba(55, 58, 60);\n}\n.input-group[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: nowrap;\n  flex-wrap: nowrap;\n  -webkit-box-align: stretch;\n  -ms-flex-align: stretch;\n  align-items: stretch;\n  width: 100%;\n}\n.input-group-text[data-v-7a85176d] {\n  font-size: 16px;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  right: -12px;\n  top: -17px;\n  cursor: pointer;\n  color: black;\n  padding: 3px 4px !important;\n  border-radius: 50%;\n  z-index: 6;\n  background: white;\n}\n.askBiz[data-v-7a85176d] {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  cursor: pointer;\n  z-index: 100;\n  text-align: center;\n  width: 25%;\n  height: auto;\n}\n.fa-2x[data-v-7a85176d] {\n  font-size: 1.5em;\n}\n.bizzy[data-v-7a85176d] {\n  width: 50px;\n  height: 50px;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.goAsk[data-v-7a85176d] {\n  font-size: 18px;\n  font-weight: bold;\n  color: #a4c2db;\n}\n.askTimes[data-v-7a85176d] {\n  position: absolute;\n  top: -18px;\n  right: -18px;\n  cursor: pointer;\n}\n.askClose[data-v-7a85176d] {\n  margin-left: -7%;\n  text-align: right;\n  opacity: 0.4;\n  width: 9% !important;\n  padding: 0 !important;\n}\n.askClose[data-v-7a85176d]:hover {\n  opacity: 1;\n}\n.askIcons[data-v-7a85176d] {\n  -webkit-box-orient: vertical !important;\n  -webkit-box-direction: normal !important;\n      -ms-flex-direction: column !important;\n          flex-direction: column !important;\n  margin: 0 !important;\n  margin-left: auto !important;\n  -webkit-box-align: end;\n      -ms-flex-align: end;\n          align-items: flex-end;\n}\n.fa-times[data-v-7a85176d],\n.fa-plus[data-v-7a85176d] {\n  position: absolute;\n  top: 10px;\n  right: 10px;\n  cursor: pointer;\n}\n.news-box[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.news-button[data-v-7a85176d] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.newsletter[data-v-7a85176d] {\n  width: 400px;\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: fixed;\n  top: 40%;\n  right: 15px;\n  z-index: 99;\n  overflow: hidden;\n  background: #d1e8fd;\n}\n.newsletter_image[data-v-7a85176d] {\n  position: absolute;\n  right: -26px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: right;\n     object-position: right;\n  height: 100%;\n}\n.btn-red[data-v-7a85176d] {\n  background-color: red !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.btn-success[data-v-7a85176d] {\n  background-color: green !important;\n  border: none;\n  text-transform: capitalize !important;\n}\n.form-control[data-v-7a85176d]::-webkit-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-moz-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::placeholder {\n  /* Chrome, Firefox, Opera, Safari 10.1+ */\n  color: rgba(0, 0, 0, 0.44) !important;\n  opacity: 1; /* Firefox */\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d]::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: rgba(0, 0, 0, 0.44) !important;\n  font-size: 16px;\n}\n.form-control[data-v-7a85176d] {\n  border: none;\n  border-bottom: 1px solid #ccc;\n  border-radius: 5px;\n  height: 38px;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n\n/* .howw {\n  height: 100vh;\n} */\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n}\n.separator-danger[data-v-7a85176d] {\n  color: #5b84a7;\n}\n.separator[data-v-7a85176d] {\n  color: rgba(0, 0, 0, 0.64);\n  margin: 0 auto 20px;\n  max-width: 240px;\n  text-align: center;\n  position: relative;\n}\n*[data-v-7a85176d] {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:before {\n  float: left;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.separator[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator-danger[data-v-7a85176d]:before,\n.separator-danger[data-v-7a85176d]:after {\n  border-color: #5b84a7;\n}\n.separator[data-v-7a85176d]:after {\n  float: right;\n}\n.separator[data-v-7a85176d]:before,\n.separator[data-v-7a85176d]:after {\n  display: block;\n  width: 40%;\n  content: \" \";\n  margin-top: 10px;\n  border: 1px solid #5b84a7;\n}\n*[data-v-7a85176d]:before,\n*[data-v-7a85176d]:after {\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n}\n.center[data-v-7a85176d] {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.column[data-v-7a85176d] {\n  float: left;\n  width: 33.3%;\n  padding: 5px;\n  background: #ffffff;\n}\n.card-header[data-v-7a85176d] {\n  background: hsl(207, 45%, 95%);\n}\n.taskImg[data-v-7a85176d] {\n  color: #5b84a7 !important;\n  text-align: center;\n  font-weight: normal;\n  text-align: center;\n  font-size: calc(14px + (20 - 14) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n  width: 100%;\n}\n.howw[data-v-7a85176d] {\n  padding-bottom: 60px;\n}\n\n/* .taskImg[data-v-632021da] {\n    color: #ffffff !important;\n    font-weight: 600;\n    text-align: center;\n    font-size: 14px;\n    background: #143048;\n    #6a88a2\n    border-left: 2px solid white;\n    border-right: 2px solid white;\n} */\n.taskImg[data-v-7a85176d]:hover {\n  background: #5b84a7 !important;\n  color: #ffffff !important;\n}\n.taskImg .f-35[data-v-7a85176d] {\n  font-size: 25px;\n}\n/* .taskImg .f-35:hover{\n\n \tcolor: #ffffff !important;\n} */\n\n/* Clear floats after image containers */\n.row[data-v-7a85176d]::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n.courseBody[data-v-7a85176d] {\n  font-size: calc(13px + (18 - 16) * ((100vw - 300px) / (1600 - 300)));\n  line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.imgTask[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.text-grey[data-v-7a85176d] {\n  color: #ccc !important;\n}\n.bg-grey[data-v-7a85176d] {\n  background: #ccc !important;\n  border: 1px solid #ccc !important;\n}\n.works[data-v-7a85176d] {\n  text-align: center;\n  padding: 60px 0 0px 0;\n  font-weight: bolder;\n  color: rgba(3, 12, 38, 0.6);\n}\n.oneContainer[data-v-7a85176d],\n.twoContainer[data-v-7a85176d],\n.threeContainer[data-v-7a85176d],\n.fourContainer[data-v-7a85176d],\n.fiveContainer[data-v-7a85176d] {\n  position: relative;\n  height: 100%;\n  border-radius: 10px;\n  width: 130%;\n  /* overflow: hidden; */\n}\n.leftContainer[data-v-7a85176d] {\n  width: 50%;\n  max-height: 675px;\n  margin-right: 20px;\n  /* overflow-y: scroll; */\n}\n.leftContainer[data-v-7a85176d]::-webkit-scrollbar {\n  color: #5b84a7;\n}\n.d[data-v-7a85176d] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.oneContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.twoContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.threeContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fourContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.fiveContainer img[data-v-7a85176d] {\n  width: 100%;\n  height: 100%;\n}\n.howContent[data-v-7a85176d] {\n  background: #ffffff;\n  /* margin-left: 20px; */\n  max-width: 55%;\n  border-radius: 5px;\n\n  overflow: hidden;\n}\n.courseTitles[data-v-7a85176d] {\n  color: rgba(3, 12, 38, 0.74);\n  margin-bottom: 8px;\n  font-weight: bold;\n  font-size: 16px;\n}\n.hws[data-v-7a85176d] {\n  padding: 10px 0;\n  cursor: pointer;\n}\n.howBorderFive[data-v-7a85176d] {\n  border-top: 1px solid #5b84a7;\n}\n.howBorder[data-v-7a85176d],\n.howBorderThird[data-v-7a85176d],\n.howBorderFour[data-v-7a85176d] {\n  border-bottom: 1px solid #5b84a7;\n  border-top: 1px solid #5b84a7;\n}\n.howCount[data-v-7a85176d] {\n  position: relative;\n  width: 16px;\n  height: 16px;\n  border: 1px solid rgb(3, 12, 38, 0.7);\n  border-radius: 60%;\n  background: hsl(225, 86%, 8%);\n  margin-right: 10px;\n  font-size: 12px;\n  margin-top: 3px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.howCon[data-v-7a85176d] {\n  width: 80%;\n}\n.num[data-v-7a85176d] {\n  padding: 6px;\n  color: white;\n}\n.cat-arrow[data-v-7a85176d] {\n  right: 21px;\n  position: absolute;\n  bottom: 12px;\n  color: white;\n  font-size: 34px;\n}\n.brand[data-v-7a85176d] {\n  background: #c3f1d4;\n  margin: 13px;\n  padding: 13px 40px 400px;\n  position: relative;\n}\n.innovation[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7f0fb;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.procurement[data-v-7a85176d] {\n  margin: 13px;\n  background: #ffd3e6;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.timemgt[data-v-7a85176d] {\n  margin: 13px;\n  background: #d7e6f4;\n  padding: 13px 40px 200px;\n  position: relative;\n}\n.sales[data-v-7a85176d] {\n  margin: 13px;\n  background: #dbf1fa;\n  padding: 13px 40px 160px;\n  position: relative;\n}\n.diver[data-v-7a85176d] {\n  margin: 40px;\n}\n@media (max-width: 1024px) {\n.form[data-v-7a85176d]{\n    width:90%;\n}\n}\n@media (max-width: 768px) {\n}\n@media only screen and (max-width: 768px) {\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:90%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:50%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.boxes[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 14px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 0;\n    min-height: 100px;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 28px;\n    text-align: center;\n}\n.form[data-v-7a85176d] {\n    width: 90%;\n}\n.accounting_slider[data-v-7a85176d]{\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:28px;\n}\n}\n@media only screen and (max-width: 575px) {\n.boxes[data-v-7a85176d] {\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: reverse;\n        -ms-flex-direction: column-reverse;\n            flex-direction: column-reverse;\n    padding: 30px 20px;\n}\n.gp[data-v-7a85176d] {\n    font-size: 12px;\n}\n.gProfit[data-v-7a85176d] {\n    font-size: 16px;\n}\n.form[data-v-7a85176d] {\n    margin: 0 auto;\n    width: 90%;\n}\n.input-group-text[data-v-7a85176d] {\n    font-size: 13px;\n}\n.leftBox[data-v-7a85176d] {\n    width: 100%;\n    padding: 40px 0 0;\n}\n.rightBox[data-v-7a85176d] {\n    width: 100%;\n}\n.generate-text[data-v-7a85176d] {\n    font-size: 18.5px;\n    text-align: center;\n}\n.newsletter[data-v-7a85176d] {\n    width: 300px;\n}\n.taskImg .f-35[data-v-7a85176d] {\n    font-size: 16px;\n}\n.oneContainer[data-v-7a85176d],\n  .twoContainer[data-v-7a85176d],\n  .threeContainer[data-v-7a85176d],\n  .fourContainer[data-v-7a85176d],\n  .fiveContainer[data-v-7a85176d] {\n    border-radius: 0;\n}\n.howItWork[data-v-7a85176d] {\n    display: block !important;\n    margin-right: 0;\n    margin-left: 0;\n}\n.leftContainer[data-v-7a85176d] {\n    padding: 0 35px;\n    width: 100%;\n}\n.howContent[data-v-7a85176d] {\n    width: 100%;\n    max-width: 100%;\n    height: auto;\n}\n.hws[data-v-7a85176d] {\n    margin: 0;\n\n    padding: 20px 0 5px;\n}\n.howCount[data-v-7a85176d] {\n    width: 15px;\n    height: 15px;\n}\n.howCon[data-v-7a85176d] {\n    width: 100%;\n}\n.taskImg[data-v-7a85176d] {\n    color: #5b84a7 !important;\n    font-size: calc(8px + (26 - 8) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n}\n.works[data-v-7a85176d] {\n    font-size: calc(20px + (26 - 20) * ((100vw - 300px) / (1600 - 300)));\n    line-height: calc(1.3em + (1.5 - 1.2) * ((100vw - 300px) / (1600 - 300)));\n    margin: 20px 0 10px 0;\n    padding: 20px 0 0 0;\n}\n.num[data-v-7a85176d] {\n    top: -5px;\n    right: 3px;\n    font-size: 12px;\n}\n.fa-2x[data-v-7a85176d] {\n    font-size: 1em;\n}\n.scrollUp[data-v-7a85176d] {\n    padding: 5px 7px;\n}\n@media (max-width: 425px) {\n.goAsk[data-v-7a85176d] {\n      font-size: 16px !important;\n}\n.askBiz[data-v-7a85176d] {\n      width: 75%;\n}\n.bizzy[data-v-7a85176d] {\n      width: 40px;\n}\n.askClose[data-v-7a85176d] {\n      width: 11% !important;\n}\n.fa-times[data-v-7a85176d],\n    .fa-plus[data-v-7a85176d] {\n      font-size: 0.7em;\n      top: 0;\n      right: -16px;\n}\n.askTimes[data-v-7a85176d] {\n      top: -8px;\n      right: 8px;\n}\n.card-title[data-v-7a85176d]{\n      font-size:13px;\n}\n.card-text[data-v-7a85176d]{\n       font-size:13px;\n}\n.slide_button[data-v-7a85176d]{\n    width:100%\n}\n.leftSlide[data-v-7a85176d]{\n    width:100%;\n    padding:20px 5px\n}\n.mini-content[data-v-7a85176d]{\n    padding-top:10px;\n}\n.mini-content-title[data-v-7a85176d],  .bms-heading strong[data-v-7a85176d]{\n    font-size:16px;\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.mini-content-title[data-v-7a85176d]{\n    width:60%;\n    margin-left:-4.5%;\n}\n.business-words[data-v-7a85176d]{\n    display: grid;\n    grid-template-rows: auto auto;\n    grid-template-columns: repeat(10,145px);\n    grid-column-gap: 20px;\n    grid-row-gap: 10px;\n    margin-bottom: 20px;\n    overflow-x: scroll;\n}\n.single-word[data-v-7a85176d]{\n    padding: 10px 9px;\n    font-size: 12px;\n    text-align: center;\n}\n.bms-heading strong[data-v-7a85176d]{\n    font-size:18px;\n}\n.mini-content[data-v-7a85176d]{\n    padding-left:15px;\n    padding-right:15px;\n    padding-top:30px;\n}\n.mini-content-title[data-v-7a85176d]{\n    font-size:18px;\n    margin-left:-3%;\n      padding: 15px 20px 13px;\n}\n.overviewMain[data-v-7a85176d]{\n    width:80%;\n}\n.bms-heading[data-v-7a85176d]{\n    width:85%;\n    padding: 20px 20px 15px;\n}\n.accounting_slider[data-v-7a85176d]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    height:auto;\n    width:100%;\n    padding: 40px 0;\n}\n.rightSlide[data-v-7a85176d],.leftSlide[data-v-7a85176d]{\n    width:80%;\n    margin:0 auto\n}\n.mini-content[data-v-7a85176d], .bms[data-v-7a85176d]{\n    width:100%;\n}\n.bms[data-v-7a85176d]{\n    padding-top:0;\n}\n.bms-em[data-v-7a85176d]{\n    -webkit-box-orient:vertical;\n    -webkit-box-direction:normal;\n        -ms-flex-direction:column;\n            flex-direction:column;\n    padding:40px 15px;\n}\n.leftSlide[data-v-7a85176d]{\n    padding:20px;\n    width:100%;\n}\n.leftSlide p[data-v-7a85176d]{\n    font-size:24px;\n    text-align:center !important;\n    padding:10px;\n}\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "categories-bar-component",
  data: function data() {
    var _ref;

    return _ref = {
      gp: 0,
      revenue: null,
      cost: null,
      loader: false,
      calculate: "Continue to opex",
      scrollPos: 0,
      currentHeight: 0,
      swing: false,
      askOpen: false,
      show: false,
      first_name: "",
      last_name: "",
      email: "",
      one: true,
      two: false,
      three: false,
      four: false,
      five: false,
      howBor: false,
      howBoro: false,
      howBorod: false,
      howBorod4: false,
      howBorod5: false,
      textGreyOne: false,
      textGreyTwo: true,
      textGreyThree: true,
      textGreyFour: true,
      textGreyFive: true
    }, _defineProperty(_ref, "scrollPos", 0), _defineProperty(_ref, "currentHeight", 0), _defineProperty(_ref, "scrollUpButton", false), _defineProperty(_ref, "currencySymbol", "NGN "), _defineProperty(_ref, "position", "prefix"), _ref;
  },

  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
    var user = JSON.parse(localStorage.getItem("authUser"));
    if (user !== null) {
      window.addEventListener("scroll", function (e) {
        _this.scrollPos = window.scrollY;
        _this.currentHeight = window.innerHeight;
      });
    }
  },

  computed: {
    currency: function currency() {
      return _defineProperty({}, this.position, this.currencySymbol);
    }
  },
  watch: {
    scrollPos: "scrolling",
    cost: "calcGp"
  },
  methods: {
    goToDemo: function goToDemo() {
      var user = localStorage.getItem('authUser');
      if (user !== null) {
        var demo = localStorage.getItem('demo');

        this.$router.push({
          name: 'AccountingDemo',
          query: {
            redirect_to: 'demo'
          }
        });
      } else {
        this.$router.push({
          name: 'auth',
          params: { name: 'register' },
          query: {
            redirect_from: 'explore'
          }
        });
      }
    },
    scrolling: function scrolling() {
      if (this.scrollPos > window.innerHeight * 0.3) {
        this.scrollUpButton = true;
      } else {
        this.scrollUpButton = false;
      }

      if (this.scrollPos > window.innerHeight * 1.2) {
        this.swing = true;
      }
    },
    calcGp: function calcGp() {
      this.gp = this.revenue - this.cost;
    },
    calc: function calc() {
      var _this2 = this;

      if (this.gp != 0) {
        this.loader = true;
        this.calculate = "processing ...";
        setTimeout(function () {
          _this2.calculate = "calculating ...";
        }, 2000);

        setTimeout(function () {
          _this2.calculate = "calculate";
          _this2.loader = false;
          _this2.$router.push('/account/pricing?session=pricing');
        }, 4000);
      }
    },
    currencyFormat: function currencyFormat(num) {
      if (num !== null) {
        return "NGN " + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      } else {
        return "NGN 0.00";
      }
    },
    openAsk: function openAsk() {
      this.$router.push("/ask-bizguruh");
    },
    askClose: function askClose() {
      this.askOpen = !this.askOpen;
    },
    showNews: function showNews() {
      this.show = false;
    },
    subscribe: function subscribe() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        email: user.email,
        first_name: user.name,
        last_name: user.name
      };
      axios.post("/api/subscribe", data).then(function (response) {
        if (response.status === 200) {
          _this3.$toasted.success("Subscription successful");
          localStorage.removeItem("news");
          _this3.email = _this3.first_name = _this3.last_name = "";
          _this3.show = false;
        }
        {}
      });
    },
    switchTab: function switchTab(params) {
      switch (params) {
        case "one":
          this.one = this.howBor = this.textGreyTwo = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.two = this.three = this.four = this.five = this.howBoro = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyOne = false;
          break;
        case "two":
          this.two = this.howBoro = this.textGreyOne = this.textGreyThree = this.textGreyFour = this.textGreyFive = true;
          this.one = this.three = this.four = this.five = this.howBor = this.howBorod = this.howBorod4 = this.howBorod5 = this.textGreyTwo = false;
          break;
        case "three":
          this.three = this.howBorod = this.textGreyOne = this.textGreyTwo = this.textGreyFour = this.textGreyFive = true;
          this.one = this.two = this.four = this.five = this.howBor = this.howBoro = this.howBorod4 = this.howBorod5 = this.textGreyThree = false;
          break;
        case "four":
          this.four = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.textGreyFive = this.howBorod4 = true;
          this.one = this.two = this.three = this.five = this.howBor = this.howBoro = this.howBorod5 = this.howBorod = this.textGreyFour = false;
          break;
        case "five":
          this.five = this.textGreyOne = this.textGreyTwo = this.textGreyThree = this.howBorod5 = this.textGreyFour = true;
          this.one = this.two = this.three = this.four = this.howBor = this.howBoro = this.howBorod4 = this.howBorod = this.textGreyFive = false;
          break;
        default:
          this.one = this.two = this.three = this.four = this.five = false;
      }
    }
  }
});

/***/ }),

/***/ 887:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "it" }, [
    _c("div", { staticClass: "overlay-bus" }),
    _vm._v(" "),
    _c("div", { staticClass: "howIt" }, [
      _c("h3", { staticClass: "mb-5 desc_header josefin " }, [
        _vm._v("Business Management Solutions")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "boxes" }, [
        _c("div", { staticClass: "leftBox" }, [
          _c(
            "form",
            {
              staticClass: "form shadow p-lg-5 p-md-4 p-sm-3 p-4 rounded",
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.calc($event)
                }
              }
            },
            [
              _c("h3", { staticClass: "text-center mb-3" }, [
                _vm._v("Account Statement Generator")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.revenue,
                      callback: function($$v) {
                        _vm.revenue = $$v
                      },
                      expression: "revenue"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(1)
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "input-group my-3" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("CurrencyInput", {
                    staticClass: "form-control",
                    attrs: {
                      currency: _vm.currency,
                      placeholder: "NGN 0.00",
                      required: ""
                    },
                    model: {
                      value: _vm.cost,
                      callback: function($$v) {
                        _vm.cost = $$v
                      },
                      expression: "cost"
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(3)
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "profit" }, [
                _c("p", { staticClass: "gp" }, [
                  _c("span", { staticClass: "gProfit" }, [
                    _vm._v(_vm._s(_vm.currencyFormat(this.gp)))
                  ]),
                  _vm._v(" Gross profit\n           ")
                ])
              ]),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "elevated_btn bg-white text-main mx-auto",
                  attrs: { type: "submit" }
                },
                [
                  _vm._v(
                    "\n           " + _vm._s(_vm.calculate) + "\n           "
                  ),
                  _vm.loader
                    ? _c("span", {
                        staticClass:
                          "spinner-grow spinner-grow-sm spin text-grey"
                      })
                    : _vm._e()
                ]
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "rightBox" }, [
          _c(
            "p",
            {
              staticClass: "generate-text animated slideInUp",
              class: { "d-none": !_vm.swing }
            },
            [
              _vm._v(
                "\n         Ditch the paperwork, get organized and track your business\n         "
              ),
              _c("strong", [_vm._v("PROFITABILITY")]),
              _vm._v(" and\n         "),
              _c("strong", [_vm._v("GROWTH")]),
              _vm._v(" in\n         "),
              _c("strong", [_vm._v("REAL-TIME")]),
              _vm._v("!\n       ")
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _vm.$route.name !== "IndexPage"
        ? _c("section", {}, [
            _c("div", { staticClass: "bms" }, [
              _c("div", { staticClass: "accounting_slider" }, [
                _c("div", { staticClass: "rightSlide" }, [
                  _vm.swing
                    ? _c("img", {
                        staticClass: "bms_img animated slideInUp",
                        attrs: { src: "/images/word_threed.png", alt: "" }
                      })
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "leftSlide" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "slide_button" },
                    [
                      _c(
                        "button",
                        {
                          staticClass: "elevated_btn btn-compliment text-white",
                          attrs: { type: "button" },
                          on: { click: _vm.goToDemo }
                        },
                        [_vm._v("Free Demo Now")]
                      ),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "SubscriptionProfile",
                              params: {
                                level: 0
                              },
                              query: {
                                name: "subscribe-accounting"
                              }
                            }
                          }
                        },
                        [
                          _c(
                            "button",
                            {
                              staticClass:
                                "elevated_btn btn-outline-compliment bg-white",
                              attrs: { type: "button" }
                            },
                            [_vm._v("Subscribe")]
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("Revenue")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Your revenue refers to fees earned from providing goods or services. Under the accrual basis of accounting, revenues are recorded at the time of delivering the service or the merchandise, even if cash is not received at the time of delivery. Often the term income is used instead of revenue."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text", attrs: { id: "" } }, [
        _vm._v("COGS")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        {
          staticClass: "input-group-text",
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "bottom",
            title:
              "Cost of goods sold (COGS) refers to the direct costs of producing the goods sold by your company. This amount includes the cost of the materials and labor directly used to create the good but excludes indirect expenses, such as distribution costs and sales force costs."
          }
        },
        [_c("i", { staticClass: "fas fa-info" })]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "w-100  mb-4" }, [
      _c("span", { staticClass: "text-main bold" }, [_vm._v("BE EMPOWERED")]),
      _vm._v(" - To make decisions that "),
      _c("span", { staticClass: "text-main bold" }, [_vm._v("GROW")]),
      _vm._v(" your business")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a85176d", module.exports)
  }
}

/***/ }),

/***/ 944:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(945)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(947)
/* template */
var __vue_template__ = __webpack_require__(948)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a1b2036"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/testimonialComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a1b2036", Component.options)
  } else {
    hotAPI.reload("data-v-7a1b2036", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 945:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(946);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("460f1c1b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a1b2036\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./testimonialComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 946:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\nh3[data-v-7a1b2036] {\n  text-transform: initial;\n  font-size: 34px;\n  color: #333;\n}\n.main[data-v-7a1b2036] {\n background-color: white;\n  padding-top: 65px;\n  padding-bottom: 100px;\n\n  overflow: hidden;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bg-row[data-v-7a1b2036] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.bg1[data-v-7a1b2036] {\n  width: 60%;\n  -webkit-transform: rotate(-20deg);\n          transform: rotate(-20deg);\n  height: 156%;\n  top: -44%;\n  position: absolute;\n  right: -14%;\n}\n.body[data-v-7a1b2036]{\n\n    width: 100%;\n    padding: 15px 15px 45px;\n    border-radius: 5px;\n    z-index: 2;\n}\n.my-content[data-v-7a1b2036] {\n  display: grid;\n  grid-template-columns: auto auto;\n  grid-template-rows: auto auto;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 80%;\n  margin: 0 auto;\n  grid-column-gap: 50px;\n  grid-row-gap: 50px;\n  padding-top:20px\n}\n.testimonial-box[data-v-7a1b2036] {\n  height: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background:#f7f8fa;\n  height:200px;\n}\n.testimonial-image[data-v-7a1b2036] {\n  background:rgba(164, 194, 219,.5);\n  width: 40%;\n  height:100%;\n  text-align: center;\n  padding: 15px;\n}\n.testimonial-image img[data-v-7a1b2036] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.testimonial-text[data-v-7a1b2036] {\n  width: 60%;\n  padding: 15px;\n}\n@media (max-width: 768px) {\nh3[data-v-7a1b2036] {\n    font-size: 28px;\n    margin-bottom: 16px;\n}\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n}\n.testimonial-box shadow-sm[data-v-7a1b2036] {\n    padding: 15px;\n    background: white;\n    border-radius: 5px;\n}\nstrong[data-v-7a1b2036] {\n    font-size: 14px;\n}\np[data-v-7a1b2036]{\n  font-size:14px;\n}\n.body[data-v-7a1b2036]{\n  padding:0;\n}\n}\n@media (max-width: 575px) {\n.my-content[data-v-7a1b2036] {\n    width: 90%;\n    grid-template-columns: auto;\n    grid-template-rows: auto;\n}\n.bg1[data-v-7a1b2036] {\n    width: 60%;\n    /* background: #e3ecf4; */\n    -webkit-transform: rotate(-20deg);\n    transform: rotate(-20deg);\n    height: 156%;\n    top: -60%;\n    position: absolute;\n    right: -14%;\n}\n.body[data-v-7a1b2036] {\n    background: transparent;\n    width: 100%;\n    padding: 15px;\n    border-radius: 5px;\n    z-index: 3;\n    -webkit-box-shadow: none !important;\n            box-shadow: none !important;\n}\n.testimonial-text[data-v-7a1b2036]{\n  font-size: 14px;\n}\nstrong[data-v-7a1b2036]{\n  font-size: 14.5px;\n}\n/* .testimonial-image img{\n  width: 60px;\n  height: auto;\n} */\n}\n", ""]);

// exports


/***/ }),

/***/ 947:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "testimonial-component",
  data: function data() {
    return {
      scrollPos: 0,
      currentHeight: 0,
      swing: false
    };
  },
  mounted: function mounted() {
    var _this = this;

    window.addEventListener("scroll", function (e) {
      _this.scrollPos = window.scrollY;
      _this.currentHeight = window.innerHeight;
    });
  },

  watch: {
    scrollPos: "swinging"
  },
  methods: {
    swinging: function swinging() {
      if (this.scrollPos > window.innerHeight * 3.85) {
        this.swing = true;
      }
    }
  }
});

/***/ }),

/***/ 948:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main" }, [
      _c("div", { staticClass: "body " }, [
        _c("h3", { staticClass: "mb-5 w-100 text-center desc_header" }, [
          _vm._v("What our users think")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "my-content animated fadeIn" }, [
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-three.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          The post on registering ones business was\n          particularly beneficial. I’m currently saving\n          towards it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @oilbyo")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I am here because\n          @freesiafoodies\n          asked us to, but seriously I must say it is worth\n          it.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Oladunni Omotola")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-two.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "\n          I heard about your page from Nwanyiakamu and I\n          followed you. Your content is very educative and\n          qualitative always. Keep it up and thank you.\n        "
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- Tinuke Adegboye")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "testimonial-box shadow-sm" }, [
            _c("div", { staticClass: "testimonial-image" }, [
              _c("img", {
                attrs: { src: "/images/BizGuruhs-one.png", alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "testimonial-text" }, [
              _c("p", [
                _vm._v(
                  "I find your insights very inspiring, I stopped dreaming and started doing."
                )
              ]),
              _vm._v(" "),
              _c("strong", [_vm._v("- @theefosa")])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a1b2036", module.exports)
  }
}

/***/ })

});