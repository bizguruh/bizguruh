webpackJsonp([16],{

/***/ 1730:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1731);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("56b2e736", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c960e3d4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./marketing.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c960e3d4\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./marketing.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1731:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mi[data-v-c960e3d4]{\n  position: relative;\n}\n.containder-fluid[data-v-c960e3d4] {\n  padding-right: 1px;\n  padding-left: 1px;\n  font-size: 15px;\n}\n.back[data-v-c960e3d4] {\n  font-size: 14px;\n  position: absolute;\n  top: 0;\n}\nlabel[data-v-c960e3d4] {\n  font-weight: normal;\n}\n.question[data-v-c960e3d4] {\n  position: relative;\n  margin: 0px auto;\n  min-height: 100vh;\n  padding: 10px;\n  padding-top: 30px;\n  padding-bottom: 65px;\n  border-radius: 4px;\n  background: white;\n}\n.sub_title[data-v-c960e3d4] {\n  font-size: 9px;\n}\nlegend[data-v-c960e3d4] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\nform[data-v-c960e3d4] {\n  min-height: 80vh;\n  overflow: scroll;\n  width: 80%;\n  margin: 0 auto;\n   padding-top:50px;\n}\n.result[data-v-c960e3d4] {\n  padding: 10px;\n}\nol[data-v-c960e3d4],\nul[data-v-c960e3d4],\ntd[data-v-c960e3d4],\nth[data-v-c960e3d4] {\n  text-transform: capitalize;\n}\nol[data-v-c960e3d4] {\n  font-weight: bold;\n}\np[data-v-c960e3d4] {\n  margin-bottom: 10px;\n}\nh3[data-v-c960e3d4],\nh5[data-v-c960e3d4] {\n  margin-bottom: 14px;\n}\n.butt[data-v-c960e3d4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-input[data-v-c960e3d4] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.custom-control-indicator[data-v-c960e3d4] {\n  color: hsl(207, 43%, 20%) !important;\n}\n.options[data-v-c960e3d4] {\n  display: grid;\n  grid-template-columns: auto auto auto;\n  grid-column-gap: 15px;\n}\nlabel.custom-radio[data-v-c960e3d4] {\n}\n.form-group[data-v-c960e3d4] {\n  margin: 15px 0;\n}\n.preview-overlay[data-v-c960e3d4] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.progress_box[data-v-c960e3d4] {\n  padding: 25px 0;\n  position: relative;\n}\n.progress_bar[data-v-c960e3d4] {\n  text-align: center;\n  position: relative;\n}\n.progress_bar li[data-v-c960e3d4] {\n  display: inline-table;\n  position: relative;\n  width: 55px;\n}\n.progress_bar li .fa[data-v-c960e3d4] {\n  padding: 10px;\n  background: #ccc;\n  border-radius: 50%;\n  color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  width: 22px;\n  height: 22px;\n  margin: 0 auto;\n  font-size: 10px;\n  position: relative;\n  z-index: 1;\n  cursor: pointer;\n}\n.progress_bar li .fa[data-v-c960e3d4]::after {\n  content: \"\";\n  position: absolute;\n  width: 60px;\n  background: #ccc;\n  height: 3px;\n  display: block;\n  top: 50%;\n  margin-top: -1.5px;\n  z-index: -1;\n}\n.progress_bar li .fa.active[data-v-c960e3d4] {\n  background: hsl(207, 43%, 20%);\n}\n.progress_bar li .fa.active[data-v-c960e3d4]::after {\n  background: hsl(207, 43%, 20%);\n}\n.progress_bar li p[data-v-c960e3d4] {\n  font-size: 9px;\n  line-height: 1.4;\n}\n.preview[data-v-c960e3d4],\n.result[data-v-c960e3d4] {\n  background: white;\n  padding: 15px;\n  border-radius: 4px;\n  width: 96%;\n  margin: 90px auto;\n  margin-bottom: 0;\n  height: 85%;\n  font-size: 13px;\n  overflow: scroll;\n}\n.preview table[data-v-c960e3d4] {\n  font-size: 14px;\n}\nem[data-v-c960e3d4] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-c960e3d4] {\n  font-size: 12px;\n}\nlabel[data-v-c960e3d4] {\n  margin-bottom: 5px;\n  font-weight: normal;\n}\n.form-text[data-v-c960e3d4] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-c960e3d4] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-c960e3d4] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-c960e3d4],\ntd[data-v-c960e3d4] {\n  text-align: center;\n}\ntable[data-v-c960e3d4] {\n  font-size: 12px;\n}\nselect.form-control[data-v-c960e3d4] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-c960e3d4] {\n  font-size: 12px;\n}\n.quest[data-v-c960e3d4] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-c960e3d4] {\n  font-size: 12px;\n}\nh3[data-v-c960e3d4] {\n  font-family: \"Josefin Sans\";\n}\n@media only screen and (min-width: 320px) and (max-width: 375px) {\n.progress_bar li[data-v-c960e3d4] {\n    width: 50px;\n}\n.progress_bar li .fa[data-v-c960e3d4] {\n    padding: 10px;\n    width: 20px;\n    height: 20px;\n    font-size: 10px;\n}\n.progress_bar li .fa[data-v-c960e3d4]::after {\n    width: 60px;\n}\n}\n@media only screen and (min-width: 575px) and (max-width: 768px) {\n.question[data-v-c960e3d4] {\n    width: 95%;\n    padding-top: 80px;\n}\n.back[data-v-c960e3d4] {\n    top: 10px;\n}\n.progress_bar li p[data-v-c960e3d4] {\n    font-size: 14px;\n}\nh3[data-v-c960e3d4] {\n    font-size: 20px;\n}\n.quest[data-v-c960e3d4] {\n    width: 95%;\n}\n.progress_bar li[data-v-c960e3d4] {\n    width: 130px;\n}\n.progress_bar li .fa[data-v-c960e3d4] {\n    padding: 10px;\n\n    width: 40px;\n    height: 40px;\n    font-size: 15px;\n}\n.progress_bar li .fa[data-v-c960e3d4]::after {\n    width: 135px;\n}\n.tab-prev[data-v-c960e3d4] {\n    font-size: 14px;\n}\n.options[data-v-c960e3d4] {\n    display: grid;\n    grid-template-columns: auto auto auto auto auto;\n}\n.sub_title[data-v-c960e3d4] {\n    font-size: 12px;\n}\n}\n@media only screen and (min-width: 769px) {\n.sub_title[data-v-c960e3d4] {\n    font-size: 14px;\n}\n.back[data-v-c960e3d4] {\n    top: 10px;\n}\n.question[data-v-c960e3d4] {\n    margin: 0px auto;\n    width: 90%;\n    padding: 25px;\n    padding-top: 60px;\n    padding-bottom: 100px;\n\n    border: 1px solid #f7f8fa;\n}\n.options[data-v-c960e3d4] {\n    grid-template-columns: auto auto auto auto auto;\n}\nlegend[data-v-c960e3d4] {\n    font-size: 28px;\n}\n.preview[data-v-c960e3d4],\n  .result[data-v-c960e3d4] {\n    background: white;\n    padding: 30px;\n    border-radius: 4px;\n    width: 70%;\n    margin: 80px auto;\n    margin-bottom: 0;\n}\n.result[data-v-c960e3d4] {\n    font-size: 16px;\n}\nh3[data-v-c960e3d4] {\n    font-size: 24px;\n}\ntable[data-v-c960e3d4] {\n    font-size: 16px;\n}\nselect.form-control[data-v-c960e3d4] {\n    font-size: 16px;\n}\n.form-control.border-0[data-v-c960e3d4] {\n    font-size: 16px;\n}\nblockquote[data-v-c960e3d4] {\n    width: 80%;\n}\n.quest[data-v-c960e3d4] {\n    width: 95%;\n}\n.progress_bar li[data-v-c960e3d4] {\n    width: 150px;\n}\n.progress_bar li .fa[data-v-c960e3d4] {\n    padding: 15px;\n\n    width: 40px;\n    height: 40px;\n    font-size: 20px;\n}\n.progress_bar li .fa[data-v-c960e3d4]::after {\n    width: 155px;\n}\n}\n@media only screen and (min-width: 1024px) {\n.question[data-v-c960e3d4] {\n    width: 80%;\n}\n.progress_bar li p[data-v-c960e3d4] {\n    font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__marketing_target__ = __webpack_require__(1733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__marketing_target___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__marketing_target__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__marketing_goals__ = __webpack_require__(1738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__marketing_goals___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__marketing_goals__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__marketing_mix__ = __webpack_require__(1743);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__marketing_mix___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__marketing_mix__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__marketing_situation__ = __webpack_require__(1748);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__marketing_situation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__marketing_situation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__marketing_industry__ = __webpack_require__(1753);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__marketing_industry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__marketing_industry__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      target: false,
      goal: false,
      mix: false,
      situation: false,
      industry: false
    };
  },

  components: {
    Target: __WEBPACK_IMPORTED_MODULE_0__marketing_target___default.a,
    Goal: __WEBPACK_IMPORTED_MODULE_1__marketing_goals___default.a,
    Mix: __WEBPACK_IMPORTED_MODULE_2__marketing_mix___default.a,
    Situation: __WEBPACK_IMPORTED_MODULE_3__marketing_situation___default.a,
    Industry: __WEBPACK_IMPORTED_MODULE_4__marketing_industry___default.a
  },
  mounted: function mounted() {
    this.handleForm(this.$route.params.name);
  },

  methods: {
    handleForm: function handleForm(key) {
      switch (key) {
        case "target":
          this.target = true;
          break;
        case "goal":
          this.goal = true;
          break;
        case "mix":
          this.mix = true;
          break;
        case "situation":
          this.situation = true;
          break;
        case "industry":
          this.industry = true;
          break;

        default:
          break;
      }
    }
  }

});

/***/ }),

/***/ 1733:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1734)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1736)
/* template */
var __vue_template__ = __webpack_require__(1737)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0aeb9fda"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing/target.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0aeb9fda", Component.options)
  } else {
    hotAPI.reload("data-v-0aeb9fda", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1734:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1735);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("12d6cde8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0aeb9fda\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./target.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0aeb9fda\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./target.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1735:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.custom-control-input[data-v-0aeb9fda] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.preview-overlay[data-v-0aeb9fda] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.butt[data-v-0aeb9fda] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-indicator[data-v-0aeb9fda] {\n  color: hsl(207, 43%, 20%) !important;\n}\nem[data-v-0aeb9fda] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-0aeb9fda] {\n  font-size: 12px;\n}\nlabel[data-v-0aeb9fda] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-0aeb9fda] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-0aeb9fda] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-0aeb9fda] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-0aeb9fda],\ntd[data-v-0aeb9fda] {\n  text-align: center;\n}\ntable[data-v-0aeb9fda] {\n  font-size: 12px;\n}\nselect.form-control[data-v-0aeb9fda] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-0aeb9fda] {\n  font-size: 12px;\n}\n.quest[data-v-0aeb9fda] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-0aeb9fda] {\n  font-size: 12px;\n}\nh3[data-v-0aeb9fda] {\n  font-family: \"Josefin Sans\";\n}\np[data-v-0aeb9fda]{\n    margin-bottom: 10px;\n}\n.form-group[data-v-0aeb9fda] {\n  margin: 15px 0;\n}\n.sub_title[data-v-0aeb9fda] {\n  font-size: 9px;\n}\nlegend[data-v-0aeb9fda] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\nh3[data-v-0aeb9fda], h5[data-v-0aeb9fda]{\n    margin-bottom: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewTemplate__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bap: true,
      preview: false,
      record: {},
      questions: {
        one: '',
        two: '',
        three: '',
        four: '',
        five: ''
      }

    };
  },

  components: {
    previewTemp: __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default.a
  },
  mounted: function mounted() {
    this.getRecord();
    this.getDraft();
  },

  methods: {
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get('/api/get-active-record', {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    previewAnswer: function previewAnswer() {
      this.preview = !this.preview;
      window.scrollTo(0, 0);
    },
    closePreview: function closePreview() {
      this.preview = false;
      this.showResult = false;
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my target market overview"
      };
      axios.post('/api/save-draft', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my target market overview"
      };
      axios.post('/api/get-draft', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            if (response.data) {
              _this3.questions = JSON.parse(response.data.responses);
            }
          }
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my target market overview"
      };

      axios.post('/api/save-responses', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");

          _this4.preview = true;
        }
      });
    }
  }
});

/***/ }),

/***/ 1737:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.preview
      ? _c(
          "div",
          { staticClass: "preview-overlay" },
          [
            _c("previewTemp", {
              attrs: {
                bap: _vm.bap,
                questions: _vm.questions,
                title: "my target market overview"
              },
              on: { previewTemplate: _vm.previewAnswer, submit: _vm.submit }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "form-group mb-5" }, [
      _c("h5", [_vm._v("MY TARGET MARKET OVERVIEW")]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("textarea", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.questions.one,
            expression: "questions.one"
          }
        ],
        staticClass: "form-control",
        attrs: { placeholder: "", rows: "3" },
        domProps: { value: _vm.questions.one },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.$set(_vm.questions, "one", $event.target.value)
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group  mb-5" }, [
      _vm._m(1),
      _vm._v(" "),
      _c("textarea", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.questions.two,
            expression: "questions.two"
          }
        ],
        staticClass: "form-control",
        attrs: { rows: "3" },
        domProps: { value: _vm.questions.two },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.$set(_vm.questions, "two", $event.target.value)
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group  mb-5" }, [
      _vm._m(2),
      _vm._v(" "),
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.questions.three,
            expression: "questions.three"
          }
        ],
        staticClass: "form-control",
        attrs: { type: "text", "aria-describedby": "helpId", placeholder: "" },
        domProps: { value: _vm.questions.three },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.$set(_vm.questions, "three", $event.target.value)
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group  mb-5" }, [
      _c("label", { attrs: { for: "" } }, [
        _vm._v("Is it declining or increasing?")
      ]),
      _vm._v(" "),
      _c("label", { staticClass: "custom-control custom-radio" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.questions.four,
              expression: "questions.four"
            }
          ],
          staticClass: "custom-control-input",
          attrs: { type: "radio", value: "Declining" },
          domProps: { checked: _vm._q(_vm.questions.four, "Declining") },
          on: {
            change: function($event) {
              return _vm.$set(_vm.questions, "four", "Declining")
            }
          }
        }),
        _vm._v(" "),
        _c("span", { staticClass: "custom-control-indicator" }, [
          _vm._v("Declining")
        ])
      ]),
      _vm._v(" "),
      _c("label", { staticClass: "custom-control custom-radio" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.questions.four,
              expression: "questions.four"
            }
          ],
          staticClass: "custom-control-input",
          attrs: { type: "radio", value: "Increasing" },
          domProps: { checked: _vm._q(_vm.questions.four, "Increasing") },
          on: {
            change: function($event) {
              return _vm.$set(_vm.questions, "four", "Increasing")
            }
          }
        }),
        _vm._v(" "),
        _c("span", { staticClass: "custom-control-indicator" }, [
          _vm._v("Increasing")
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group  mb-5" }, [
      _c("label", { attrs: { for: "" } }, [_vm._v("At what rate?")]),
      _vm._v(" "),
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.questions.five,
            expression: "questions.five"
          }
        ],
        staticClass: "form-control",
        attrs: { type: "text", "aria-describedby": "helpId", placeholder: "" },
        domProps: { value: _vm.questions.five },
        on: {
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.$set(_vm.questions, "five", $event.target.value)
          }
        }
      }),
      _vm._v(" "),
      _c("label", { staticClass: "custom-control custom-checkbox" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.questions.five,
              expression: "questions.five"
            }
          ],
          staticClass: "custom-control-input",
          attrs: { type: "radio", value: "I dont know" },
          domProps: { checked: _vm._q(_vm.questions.five, "I dont know") },
          on: {
            change: function($event) {
              return _vm.$set(_vm.questions, "five", "I dont know")
            }
          }
        }),
        _vm._v(" "),
        _c("span", { staticClass: "custom-control-indicator" }, [
          _vm._v("I dont know")
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "w-100 butt" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.previewAnswer }
        },
        [_vm._v("Preview")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main",
          attrs: { type: "button" },
          on: { click: _vm.saveDraft }
        },
        [_vm._v("Save to draft")]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n       Briefly define your target market:\n       "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            " Typically, your customers cut across different categories that grows as you get closer to the end user, give yourself a competitive edge by thinking of each of these customers and plan for their needs and motivation."
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n       Briefly describe your customer persona:\n       "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "While your target audience encompasses the elements that show what different groups of people have in common, the customer persona looks for differentiating factors .i.e what makes each person within a certain demographic unique?\n           Simply put, your ideal customer is the one who benefits the most from what you’re selling. \n           "
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n       What is the estimated size of your market?\n       "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "This helps to determine if there are enough customers to serve"
        }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0aeb9fda", module.exports)
  }
}

/***/ }),

/***/ 1738:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1739)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1741)
/* template */
var __vue_template__ = __webpack_require__(1742)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b6587ff2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing/goals.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b6587ff2", Component.options)
  } else {
    hotAPI.reload("data-v-b6587ff2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1739:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1740);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("20ee989c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b6587ff2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goals.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b6587ff2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goals.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1740:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mi[data-v-b6587ff2]{\n  position:relative\n}\n.preview-overlay[data-v-b6587ff2] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.butt[data-v-b6587ff2] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-input[data-v-b6587ff2] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.custom-control-indicator[data-v-b6587ff2] {\n  color: hsl(207, 43%, 20%) !important;\n}\nem[data-v-b6587ff2] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-b6587ff2] {\n  font-size: 12px;\n}\np[data-v-b6587ff2]{\n    margin-bottom: 10px;\n}\nlabel[data-v-b6587ff2] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-b6587ff2] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-b6587ff2] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-b6587ff2] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-b6587ff2],\ntd[data-v-b6587ff2] {\n  text-align: center;\n}\ntable[data-v-b6587ff2] {\n  font-size: 12px;\n}\nselect.form-control[data-v-b6587ff2] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-b6587ff2] {\n  font-size: 12px;\n}\n.quest[data-v-b6587ff2] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-b6587ff2] {\n  font-size: 12px;\n}\nh3[data-v-b6587ff2] {\n  font-family: \"Josefin Sans\";\n}\n.form-group[data-v-b6587ff2] {\n  margin: 15px 0;\n}\n.sub_title[data-v-b6587ff2] {\n  font-size: 9px;\n}\nh3[data-v-b6587ff2], h5[data-v-b6587ff2]{\n    margin-bottom: 14px;\n}\nlegend[data-v-b6587ff2] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\n", ""]);

// exports


/***/ }),

/***/ 1741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewTemplate__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bap: true,
      preview: false,
      record: {},
      questions: {
        one: {
          q1: '',
          q2: '',
          q3: ''
        },
        two: {
          q1: '',
          q2: '',
          q3: ''
        },
        three: {
          q1: '',
          q2: '',
          q3: ''
        }

      }

    };
  },

  components: {
    previewTemp: __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default.a
  },
  mounted: function mounted() {
    this.getRecord();
    this.getDraft();
  },

  methods: {
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get('/api/get-active-record', {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    previewAnswer: function previewAnswer() {
      this.preview = !this.preview;
      window.scrollTo(0, 0);
    },
    closePreview: function closePreview() {
      this.preview = false;
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing goals"
      };
      axios.post('/api/save-draft', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing goals"
      };
      axios.post('/api/get-draft', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            if (response.data) {
              _this3.questions = JSON.parse(response.data.responses);
            }
          }
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing goals"
      };

      axios.post('/api/save-responses', data, {
        headers: {
          Authorization: 'Bearer ' + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");

          _this4.preview = true;
        }
      });
    }
  }
});

/***/ }),

/***/ 1742:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.preview
      ? _c(
          "div",
          { staticClass: "preview-overlay" },
          [
            _c("previewTemp", {
              attrs: {
                bap: _vm.bap,
                questions: _vm.questions,
                title: "my marketing goals"
              },
              on: { previewTemplate: _vm.previewAnswer, submit: _vm.submit }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c("h5", [_vm._v("MY MARKETING GOAL")]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("table", { staticClass: "table" }, [
      _vm._m(1),
      _vm._v(" "),
      _c("tbody", [
        _c("tr", [
          _c("td", { attrs: { scope: "row" } }, [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.one.q1,
                  expression: "questions.one.q1"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Finance" },
              domProps: { value: _vm.questions.one.q1 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.one, "q1", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.one.q2,
                  expression: "questions.one.q2"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Customer /Market" },
              domProps: { value: _vm.questions.one.q2 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.one, "q2", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.one.q3,
                  expression: "questions.one.q3"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Budget" },
              domProps: { value: _vm.questions.one.q3 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.one, "q3", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.two.q1,
                  expression: "questions.two.q1"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Finance" },
              domProps: { value: _vm.questions.two.q1 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.two, "q1", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.two.q2,
                  expression: "questions.two.q2"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Customer /Market" },
              domProps: { value: _vm.questions.two.q2 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.two, "q2", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.two.q3,
                  expression: "questions.two.q3"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Budget" },
              domProps: { value: _vm.questions.two.q3 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.two, "q3", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.three.q1,
                  expression: "questions.three.q1"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Finance" },
              domProps: { value: _vm.questions.three.q1 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.three, "q1", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.three.q2,
                  expression: "questions.three.q2"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Customer /Market" },
              domProps: { value: _vm.questions.three.q2 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.three, "q2", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.three.q3,
                  expression: "questions.three.q3"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "3", placeholder: "Budget" },
              domProps: { value: _vm.questions.three.q3 },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions.three, "q3", $event.target.value)
                }
              }
            })
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "w-100 butt" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.previewAnswer }
        },
        [_vm._v("Preview")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main",
          attrs: { type: "button" },
          on: { click: _vm.saveDraft }
        },
        [_vm._v("Save to draft")]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("strong", [_vm._v("BIZGURUH TIP:")]),
      _vm._v(
        " To make your marketing goals and budget accurate and effective, keep national and even local trends and events in mind. Adjust your annual and monthly budgets according to these trends.\n  "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("My short term marketing goals")]),
        _vm._v(" "),
        _c("th", [_vm._v("My mid term marketing goals")]),
        _vm._v(" "),
        _c("th", [_vm._v("My long term marketing goals")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b6587ff2", module.exports)
  }
}

/***/ }),

/***/ 1743:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1744)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1746)
/* template */
var __vue_template__ = __webpack_require__(1747)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b300173a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing/mix.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b300173a", Component.options)
  } else {
    hotAPI.reload("data-v-b300173a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1744:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1745);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0db48a5f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b300173a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./mix.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b300173a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./mix.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1745:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mi[data-v-b300173a] {\n  position: relative;\n}\n.preview-overlay[data-v-b300173a] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.butt[data-v-b300173a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-input[data-v-b300173a] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.custom-control-indicator[data-v-b300173a] {\n  color: hsl(207, 43%, 20%) !important;\n}\nem[data-v-b300173a] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-b300173a] {\n  font-size: 12px;\n}\nlabel[data-v-b300173a] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-b300173a] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-b300173a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-b300173a] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-b300173a],\ntd[data-v-b300173a] {\n  text-align: center;\n}\ntable[data-v-b300173a] {\n  font-size: 12px;\n}\nselect.form-control[data-v-b300173a] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-b300173a] {\n  font-size: 12px;\n}\n.quest[data-v-b300173a] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-b300173a] {\n  font-size: 12px;\n}\nh3[data-v-b300173a] {\n  font-family: \"Josefin Sans\";\n}\nh3[data-v-b300173a],\nh5[data-v-b300173a] {\n  margin-bottom: 14px;\n}\n.form-group[data-v-b300173a] {\n  margin: 15px 0;\n}\n.sub_title[data-v-b300173a] {\n  font-size: 9px;\n}\np[data-v-b300173a] {\n  margin-bottom: 10px;\n}\nlegend[data-v-b300173a] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\n", ""]);

// exports


/***/ }),

/***/ 1746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewTemplate__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bap: true,
      preview: false,
      record: {},
      questions: [{
        one: "",
        two: "",
        three: "",
        four: "",
        five: "",
        six: ""
      }]
    };
  },

  components: {
    previewTemp: __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default.a
  },
  mounted: function mounted() {
    this.getRecord();
    this.getDraft();
  },

  methods: {
    addrow: function addrow() {
      this.questions.push({
        one: "",
        two: "",
        three: "",
        four: ""
      });
    },
    removerow: function removerow() {
      this.questions.pop();
    },
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    previewAnswer: function previewAnswer() {
      this.preview = !this.preview;
      window.scrollTo(0, 0);
    },
    closePreview: function closePreview() {
      this.preview = false;
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing mix"
      };
      axios.post("/api/save-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing mix"
      };
      axios.post("/api/get-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            if (response.data) {
              _this3.questions = JSON.parse(response.data.responses);
            }
          }
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my marketing mix"
      };

      axios.post("/api/save-responses", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");

          _this4.preview = true;
        }
      });
    }
  }
});

/***/ }),

/***/ 1747:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.preview
      ? _c(
          "div",
          { staticClass: "preview-overlay" },
          [
            _c("previewTemp", {
              attrs: {
                bap: _vm.bap,
                questions: _vm.questions,
                title: "my marketing mix"
              },
              on: { previewTemplate: _vm.previewAnswer, submit: _vm.submit }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c("h5", [_vm._v("MY MARKETING MIX (4P’s)")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "This set of marketing tools will help your business sell products or services to its target customers."
      )
    ]),
    _vm._v(" "),
    _c("p", { staticClass: "mb-5" }, [
      _vm._v(
        "Consult the Marketing Mix Guide for detailed insight on how to adopt the different elements effectively."
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-flex ml-auto mb-3" }, [
      _c(
        "button",
        {
          staticClass:
            "elevated_btn elevated_btn_sm text-white btn-compliment mr-3",
          attrs: { type: "button" },
          on: { click: _vm.addrow }
        },
        [_vm._v("Add row")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main mr-3",
          attrs: { type: "button" },
          on: { click: _vm.removerow }
        },
        [_vm._v("Remove row")]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.questions, function(item, idx) {
          return _c("tr", { key: idx }, [
            _c("td", { attrs: { scope: "row" } }, [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.one,
                    expression: "item.one"
                  }
                ],
                staticClass: "form-control",
                attrs: { cols: "30", rows: "3", placeholder: "Customer need" },
                domProps: { value: item.one },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "one", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.two,
                    expression: "item.two"
                  }
                ],
                staticClass: "form-control",
                attrs: { cols: "30", rows: "3", placeholder: "Product" },
                domProps: { value: item.two },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "two", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.three,
                    expression: "item.three"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "number", placeholder: "Average market price" },
                domProps: { value: item.three },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "three", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.four,
                    expression: "item.four"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "number", placeholder: "My price" },
                domProps: { value: item.four },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "four", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.five,
                    expression: "item.five"
                  }
                ],
                staticClass: "form-control",
                attrs: { cols: "30", rows: "3", placeholder: "Where" },
                domProps: { value: item.five },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "five", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.six,
                    expression: "item.six"
                  }
                ],
                staticClass: "form-control",
                attrs: { cols: "30", rows: "3", placeholder: "How" },
                domProps: { value: item.six },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "six", $event.target.value)
                  }
                }
              })
            ])
          ])
        }),
        0
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "w-100 butt" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.previewAnswer }
        },
        [_vm._v("Preview")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main",
          attrs: { type: "button" },
          on: { click: _vm.saveDraft }
        },
        [_vm._v("Save to draft")]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Cutomer need")]),
        _vm._v(" "),
        _c("th", [_vm._v("Product")]),
        _vm._v(" "),
        _c("th", [_vm._v("Price")]),
        _vm._v(" "),
        _c("th", [_vm._v("Promotion")]),
        _vm._v(" "),
        _c("th", [_vm._v("Place")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b300173a", module.exports)
  }
}

/***/ }),

/***/ 1748:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1749)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1751)
/* template */
var __vue_template__ = __webpack_require__(1752)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7a48f7c5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing/situation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7a48f7c5", Component.options)
  } else {
    hotAPI.reload("data-v-7a48f7c5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1749:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1750);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7a7e4553", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a48f7c5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./situation.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7a48f7c5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./situation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1750:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.custom-control-input[data-v-7a48f7c5] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.mi[data-v-7a48f7c5] {\n  position: relative;\n}\n.preview-overlay[data-v-7a48f7c5] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.butt[data-v-7a48f7c5] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-indicator[data-v-7a48f7c5] {\n  color: hsl(207, 43%, 20%) !important;\n}\nh3[data-v-7a48f7c5],\nh5[data-v-7a48f7c5] {\n  margin-bottom: 14px;\n}\nem[data-v-7a48f7c5] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-7a48f7c5] {\n  font-size: 12px;\n}\nlabel[data-v-7a48f7c5] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-7a48f7c5] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-7a48f7c5] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-7a48f7c5] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-7a48f7c5],\ntd[data-v-7a48f7c5] {\n  text-align: center;\n}\ntable[data-v-7a48f7c5] {\n  font-size: 12px;\n}\nselect.form-control[data-v-7a48f7c5] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-7a48f7c5] {\n  font-size: 12px;\n}\n.quest[data-v-7a48f7c5] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-7a48f7c5] {\n  font-size: 12px;\n}\nh3[data-v-7a48f7c5] {\n  font-family: \"Josefin Sans\";\n}\n.form-group[data-v-7a48f7c5] {\n  margin: 15px 0;\n}\n.sub_title[data-v-7a48f7c5] {\n  font-size: 9px;\n}\np[data-v-7a48f7c5] {\n  margin-bottom: 10px;\n}\nlegend[data-v-7a48f7c5] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\n", ""]);

// exports


/***/ }),

/***/ 1751:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewTemplate__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bap: true,
      preview: false,
      record: {},
      questions: {
        one: "",
        two: "",
        three: "",
        four: ""
      }
    };
  },

  components: {
    previewTemp: __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default.a
  },
  mounted: function mounted() {
    this.getRecord();
    this.getDraft();
  },

  methods: {
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    previewAnswer: function previewAnswer() {
      this.preview = !this.preview;
      window.scrollTo(0, 0);
    },
    closePreview: function closePreview() {
      this.preview = false;
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my situation analysis"
      };
      axios.post("/api/save-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my situation analysis"
      };
      axios.post("/api/get-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            if (response.data) {
              _this3.questions = JSON.parse(response.data.responses);
            }
          }
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my situation analysis"
      };

      axios.post("/api/save-responses", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");

          _this4.preview = true;
        }
      });
    }
  }
});

/***/ }),

/***/ 1752:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.preview
      ? _c(
          "div",
          { staticClass: "preview-overlay" },
          [
            _c("previewTemp", {
              attrs: {
                bap: _vm.bap,
                questions: _vm.questions,
                title: "my situation analysis"
              },
              on: { previewTemplate: _vm.previewAnswer, submit: _vm.submit }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c("h5", [_vm._v("MY SITUATION ANALYSIS: SWOT ANALYSIS")]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("label", { attrs: { for: "" } }, [
      _vm._v("Let’s do a quick SWOT Analysis:")
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table" }, [
      _vm._m(1),
      _vm._v(" "),
      _c("tbody", [
        _c("tr", [
          _c("td", { attrs: { scope: "row" } }, [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.one,
                  expression: "questions.one"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "10" },
              domProps: { value: _vm.questions.one },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "one", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.two,
                  expression: "questions.two"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "10" },
              domProps: { value: _vm.questions.two },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "two", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.three,
                  expression: "questions.three"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "10" },
              domProps: { value: _vm.questions.three },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "three", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("td", [
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.four,
                  expression: "questions.four"
                }
              ],
              staticClass: "form-control",
              attrs: { rows: "10" },
              domProps: { value: _vm.questions.four },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "four", $event.target.value)
                }
              }
            })
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "w-100 butt" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.previewAnswer }
        },
        [_vm._v("Preview")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main",
          attrs: { type: "button" },
          on: { click: _vm.saveDraft }
        },
        [_vm._v("Save to draft")]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("b", [_vm._v("SWOT Analysis")]),
      _vm._v(
        " is the most renowned tool for audit and analysis of the overall strategic position of the business and its environment. Its key purpose is to identify the strategies that will best align an organization’s resources and capabilities to the requirements of the environment in which it operates.\n  "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Strength")]),
        _vm._v(" "),
        _c("th", [_vm._v("Weakness")]),
        _vm._v(" "),
        _c("th", [_vm._v("Opportunities")]),
        _vm._v(" "),
        _c("th", [_vm._v("Threats")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7a48f7c5", module.exports)
  }
}

/***/ }),

/***/ 1753:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1754)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1756)
/* template */
var __vue_template__ = __webpack_require__(1757)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1b9c3967"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing/industry.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1b9c3967", Component.options)
  } else {
    hotAPI.reload("data-v-1b9c3967", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1754:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1755);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("0792df98", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b9c3967\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./industry.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b9c3967\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./industry.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1755:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.mi[data-v-1b9c3967] {\n  position: relative;\n}\n.preview-overlay[data-v-1b9c3967] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\n.butt[data-v-1b9c3967] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  margin-top: 15px;\n  border-top: 1px solid #f7f8fa;\n}\n.custom-control-input[data-v-1b9c3967] {\n  z-index: 0 !important;\n  opacity: 1 !important;\n}\n.custom-control-indicator[data-v-1b9c3967] {\n  color: hsl(207, 43%, 20%) !important;\n}\nh3[data-v-1b9c3967],\nh5[data-v-1b9c3967] {\n  margin-bottom: 14px;\n}\nem[data-v-1b9c3967] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-1b9c3967] {\n  font-size: 12px;\n}\nlabel[data-v-1b9c3967] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-1b9c3967] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-1b9c3967] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-1b9c3967] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-1b9c3967],\ntd[data-v-1b9c3967] {\n  text-align: center;\n}\ntable[data-v-1b9c3967] {\n  font-size: 12px;\n}\nselect.form-control[data-v-1b9c3967] {\n  font-size: 12px;\n}\n.form-control.border-0[data-v-1b9c3967] {\n  font-size: 12px;\n}\n.quest[data-v-1b9c3967] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-1b9c3967] {\n  font-size: 12px;\n}\nh3[data-v-1b9c3967] {\n  font-family: \"Josefin Sans\";\n}\n.form-group[data-v-1b9c3967] {\n  margin: 15px 0;\n}\n.sub_title[data-v-1b9c3967] {\n  font-size: 9px;\n}\np[data-v-1b9c3967] {\n  margin-bottom: 10px;\n}\nlegend[data-v-1b9c3967] {\n  display: block;\n  margin-bottom: 24px;\n  width: 100%;\n  text-align: center;\n  font-family: \"Josefin Sans\";\n}\n", ""]);

// exports


/***/ }),

/***/ 1756:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__previewTemplate__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      bap: true,
      preview: false,
      record: {},
      questions: [{
        one: "",
        two: "",
        three: "",
        four: ""
      }]
    };
  },

  components: {
    previewTemp: __WEBPACK_IMPORTED_MODULE_0__previewTemplate___default.a
  },
  mounted: function mounted() {
    this.getRecord();
    this.getDraft();
  },

  methods: {
    addrow: function addrow() {
      this.questions.push({
        one: "",
        two: "",
        three: "",
        four: ""
      });
    },
    removerow: function removerow() {
      this.questions.pop();
    },
    getRecord: function getRecord() {
      var _this = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      axios.get("/api/get-active-record", {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        _this.record = response.data;
      });
    },
    previewAnswer: function previewAnswer() {
      this.preview = !this.preview;
      window.scrollTo(0, 0);
    },
    closePreview: function closePreview() {
      this.preview = false;
    },
    saveDraft: function saveDraft() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my industry analysis"
      };
      axios.post("/api/save-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this2.$toasted.success("Saved to draft");
        }
      });
    },
    getDraft: function getDraft() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my industry analysis"
      };
      axios.post("/api/get-draft", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.status == 200) {
          if (response.data) {
            if (response.data) {
              _this3.questions = JSON.parse(response.data.responses);
            }
          }
        }
      });
    },
    submit: function submit() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      var data = {
        responses: this.questions,
        month: this.record.current_month,
        week: this.record.current_week,
        template: "my industry analysis"
      };

      axios.post("/api/save-responses", data, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (response) {
        if (response.data.status == "saved") {
          _this4.$toasted.success("Completed");

          _this4.preview = true;
        }
      });
    }
  }
});

/***/ }),

/***/ 1757:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.preview
      ? _c(
          "div",
          { staticClass: "preview-overlay" },
          [
            _c("previewTemp", {
              attrs: {
                bap: _vm.bap,
                questions: _vm.questions,
                title: "my industry analysis"
              },
              on: { previewTemplate: _vm.previewAnswer, submit: _vm.submit }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c("h5", [_vm._v("MY INDUSTRY ANALYSIS: COMPETITOR ANALYSIS")]),
    _vm._v(" "),
    _c("p", { staticClass: "mb-5" }, [
      _vm._v(
        "This helps you determine the strengths and weaknesses of the competitors within your market, so that you can develop strategies that provide you with a distinct advantage and barriers that can prevent competition from entering your market, and any other weaknesses that can be exploited within the product development cycle."
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "d-flex ml-auto mb-3" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.addrow }
        },
        [_vm._v("Add row")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main mr-3",
          attrs: { type: "button" },
          on: { click: _vm.removerow }
        },
        [_vm._v("Remove row")]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.questions, function(item, idx) {
          return _c("tr", { key: idx }, [
            _c("td", { attrs: { scope: "row" } }, [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.one,
                    expression: "item.one"
                  }
                ],
                staticClass: "form-control",
                attrs: { rows: "3" },
                domProps: { value: item.one },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "one", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.two,
                    expression: "item.two"
                  }
                ],
                staticClass: "form-control",
                attrs: { rows: "3" },
                domProps: { value: item.two },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "two", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.three,
                    expression: "item.three"
                  }
                ],
                staticClass: "form-control",
                attrs: { rows: "3" },
                domProps: { value: item.three },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "three", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("td", [
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: item.four,
                    expression: "item.four"
                  }
                ],
                staticClass: "form-control",
                attrs: { rows: "3" },
                domProps: { value: item.four },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(item, "four", $event.target.value)
                  }
                }
              })
            ])
          ])
        }),
        0
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "w-100 butt" }, [
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-white btn-compliment",
          attrs: { type: "button" },
          on: { click: _vm.previewAnswer }
        },
        [_vm._v("Preview")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "elevated_btn elevated_btn_sm text-main",
          attrs: { type: "button" },
          on: { click: _vm.saveDraft }
        },
        [_vm._v("Save to draft")]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Who are my competitors?")]),
        _vm._v(" "),
        _c("th", [_vm._v("What are the similarities?")]),
        _vm._v(" "),
        _c("th", [_vm._v("What can i learn from them?")]),
        _vm._v(" "),
        _c("th", [_vm._v("How can i differentiate from them?")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1b9c3967", module.exports)
  }
}

/***/ }),

/***/ 1758:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "mi" }, [
    _c(
      "form",
      [
        _c("legend", [_vm._v("MY MARKETING PLAN")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "This aims at sharpening your skills at finding critical business information— whether it is about your industry, target market, competitors, or other business information you’ll need."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "The word ‘research’ probably conjures up school projects, but this kind of research isn’t like that. Instead you’ll get shortcuts to finding reliable information to help you build your business and make decisions."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "The emphasis will be on finding such information fast, easily and hopefully free."
          )
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Please read our guide on Developing your Marketing Plan for detailed insight on how to align your resources and capabilities to the requirements of the environment in which you operate."
          )
        ]),
        _vm._v(" "),
        _vm.target ? _c("Target", { staticClass: "mb-5" }) : _vm._e(),
        _vm._v(" "),
        _vm.situation ? _c("Situation", { staticClass: "mb-5" }) : _vm._e(),
        _vm._v(" "),
        _vm.industry ? _c("Industry", { staticClass: "mb-5" }) : _vm._e(),
        _vm._v(" "),
        _vm.goal ? _c("Goal", { staticClass: "mb-5" }) : _vm._e(),
        _vm._v(" "),
        _vm.mix ? _c("Mix", { staticClass: "mb-5" }) : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c960e3d4", module.exports)
  }
}

/***/ }),

/***/ 628:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1730)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1732)
/* template */
var __vue_template__ = __webpack_require__(1758)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c960e3d4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/marketing.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c960e3d4", Component.options)
  } else {
    hotAPI.reload("data-v-c960e3d4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 689:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(710)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(712)
/* template */
var __vue_template__ = __webpack_require__(713)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2e5b7052"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/bap/previewTemplate.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2e5b7052", Component.options)
  } else {
    hotAPI.reload("data-v-2e5b7052", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 710:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(711);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7d5ca584", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e5b7052\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewTemplate.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2e5b7052\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./previewTemplate.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 711:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.question[data-v-2e5b7052] {\n  position: relative;\n  margin: 0px auto;\n  min-height: 100vh;\n  padding: 10px;\n  padding-top: 30px;\n  padding-bottom: 65px;\n  border-radius: 4px;\n  background: white;\n}\n.custom-control-input[data-v-2e5b7052] {\n  z-index: 0;\n  opacity: 1;\n}\n.custom-control-indicator[data-v-2e5b7052] {\n  color: hsl(207, 43%, 20%);\n}\n.responses[data-v-2e5b7052] {\n}\n.response[data-v-2e5b7052] {\n  margin-bottom: 24px;\n  font-size: 16px;\n}\nli[data-v-2e5b7052],\nol[data-v-2e5b7052],\ntd[data-v-2e5b7052],\nth[data-v-2e5b7052] {\n  text-transform: capitalize;\n}\nli[data-v-2e5b7052] {\n  font-weight: normal;\n}\n.resp[data-v-2e5b7052] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.res1[data-v-2e5b7052],\n.res2[data-v-2e5b7052] {\n  width: 100%;\n  padding: 15px;\n}\n.butt[data-v-2e5b7052] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n}\n.preview-overlay[data-v-2e5b7052] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  width: 100%;\n  background-color: rgba(0, 0, 0, 0.6);\n  z-index: 2;\n}\nh5[data-v-2e5b7052] {\n  margin-bottom: 14px;\n}\n.preview[data-v-2e5b7052] {\n  background: white;\n  padding: 15px;\n  border-radius: 4px;\n  width: 96%;\n  margin: 90px auto;\n  margin-bottom: 0;\n  height: 85%;\n  font-size: 13px;\n  overflow: scroll;\n}\nul li[data-v-2e5b7052] {\n  font-weight: bold;\n}\n.preview table[data-v-2e5b7052] {\n  font-size: 14px;\n}\nem[data-v-2e5b7052] {\n  font-style: italic;\n  font-size: 15px;\n}\nsmall em[data-v-2e5b7052] {\n  font-size: 12px;\n}\n.form-borderless[data-v-2e5b7052] {\n  border-left: none;\n  border-right: none;\n  border-top: none;\n}\nlabel[data-v-2e5b7052] {\n  margin-bottom: 8px;\n  font-weight: bold;\n}\n.form-text[data-v-2e5b7052] {\n  font-size: 12px;\n  line-height: 1.4;\n  margin-bottom: 20px;\n}\n.input-group[data-v-2e5b7052] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\nth[data-v-2e5b7052] {\n  background-color: hsl(207, 43%, 20%);\n}\nth[data-v-2e5b7052],\ntd[data-v-2e5b7052] {\n  text-align: center;\n}\ntable[data-v-2e5b7052] {\n  font-size: 12px;\n}\nselect.form-contrli[data-v-2e5b7052] {\n  font-size: 12px;\n}\n.form-contrli.border-0[data-v-2e5b7052] {\n  font-size: 12px;\n}\n.quest[data-v-2e5b7052] {\n  width: 100%;\n  margin: 0 auto;\n}\n.tab-prev[data-v-2e5b7052] {\n  font-size: 12px;\n}\nh3[data-v-2e5b7052] {\n  font-family: \"Josefin Sans\";\n}\n@media only screen and (min-width: 320px) and (max-width: 375px) {\n}\n@media only screen and (min-width: 575px) and (max-width: 768px) {\n.question[data-v-2e5b7052] {\n    width: 95%;\n    padding-top: 80px;\n}\nh3[data-v-2e5b7052] {\n    font-size: 20px;\n}\n.quest[data-v-2e5b7052] {\n    width: 95%;\n}\n.tab-prev[data-v-2e5b7052] {\n    font-size: 14px;\n}\n}\n@media only screen and (min-width: 769px) {\n.resp[data-v-2e5b7052] {\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n}\n.res1[data-v-2e5b7052],\n  .res2[data-v-2e5b7052] {\n    width: 50%;\n}\n.question[data-v-2e5b7052] {\n    margin: 0px auto;\n    width: 90%;\n    padding: 25px;\n    padding-top: 60px;\n    padding-bottom: 100px;\n\n    border: 1px solid #f7f8fa;\n}\n.preview[data-v-2e5b7052] {\n    background: white;\n    padding: 30px;\n    border-radius: 4px;\n    width: 80%;\n    margin: 80px auto;\n    max-height: 85vh;\n    margin-bottom: 0;\n    font-size: 13.5px;\n}\nh3[data-v-2e5b7052] {\n    font-size: 24px;\n}\ntable[data-v-2e5b7052] {\n    font-size: 16px;\n}\nselect.form-contrli[data-v-2e5b7052] {\n    font-size: 16px;\n}\n.form-contrli.border-0[data-v-2e5b7052] {\n    font-size: 16px;\n}\nblockquote[data-v-2e5b7052] {\n    width: 80%;\n}\n.quest[data-v-2e5b7052] {\n    width: 95%;\n}\n.progress_bar li[data-v-2e5b7052] {\n    width: 150px;\n}\n.progress_bar li .fa[data-v-2e5b7052] {\n    padding: 15px;\n\n    width: 40px;\n    height: 40px;\n    font-size: 20px;\n}\n.progress_bar li .fa[data-v-2e5b7052]::after {\n    width: 155px;\n}\n}\n@media only screen and (min-width: 1024px) {\n.question[data-v-2e5b7052] {\n    width: 85%;\n}\n.progress_bar li p[data-v-2e5b7052] {\n    font-size: 16px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 712:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["questions", "bap", "title"],
  data: function data() {
    return {};
  },
  mounted: function mounted() {},

  methods: {
    preview: function preview() {
      this.$emit("previewTemplate");
    },
    submit: function submit() {
      this.$emit("submit");
    }
  }
});

/***/ }),

/***/ 713:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "preview" }, [
    _vm.title == "getting to know me"
      ? _c("div", {}, [
          _c("h3", { staticClass: "w-100 text-center mb-3" }, [
            _vm._v("Getting To Know Me")
          ]),
          _vm._v(" "),
          _c("ol", [
            _c("div", { staticClass: "resp" }, [
              _c("div", { staticClass: "res1" }, [
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("These 3 words describe me perfectly:")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q1.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q1.ans2))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q1.ans3))])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("Others would use these 5 words to describe me")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q2.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q2.ans2))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q2.ans3))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q2.ans4))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q2.ans5))])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v(
                      "People often come to me to solve this kind of problem :"
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", [_c("li", [_vm._v(_vm._s(_vm.questions.q3.ans1))])])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("What inspires me most ?")
                  ]),
                  _vm._v(" "),
                  _c("ul", [_c("li", [_vm._v(_vm._s(_vm.questions.q4.ans1))])])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("These two things make me special/unique ?")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q5.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q5.ans2))])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v(
                      "Given the chance, I would take these two superpowers ?"
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q6.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q6.ans2))])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "res2" }, [
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("I want others to think this about me :")
                  ]),
                  _vm._v(" "),
                  _c("ul", [_c("li", [_vm._v(_vm._s(_vm.questions.q7.ans1))])])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("I value these five (5) things most")
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q8.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q8.ans2))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q8.ans3))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q8.ans4))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q8.ans5))])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v("I believe my purpose is to :")
                  ]),
                  _vm._v(" "),
                  _c("ul", [_c("li", [_vm._v(_vm._s(_vm.questions.q9.ans1))])])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v(
                      "My top 2 goals in the flilowing areas of my life are :"
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans1))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans2))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans3))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans4))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans5))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans6))]),
                    _vm._v(" "),
                    _c("li", [_vm._v(_vm._s(_vm.questions.q10.ans7))])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v(
                      "My top 2 goals in the flilowing areas of my life are :"
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans1.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans1.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans2.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans2.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans3.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans3.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans4.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans4.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans5.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans5.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q11.ans6.name) +
                          " - " +
                          _vm._s(_vm.questions.q11.ans6.relationship)
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "mb-2" }, [
                  _c("p", { staticClass: "question_preview" }, [
                    _vm._v(
                      "These are the people in my inner circle; one for each category :"
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", [
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans1.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans1.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans2.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans2.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans3.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans3.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans4.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans4.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans5.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans5.relationship)
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", [
                      _vm._v(
                        _vm._s(_vm.questions.q12.ans6.name) +
                          " - " +
                          _vm._s(_vm.questions.q12.ans6.relationship)
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "mb-2" }, [
              _c("p", { staticClass: "question_preview mb-3" }, [
                _vm._v("These people inspire me most: one for each category :")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table prev-tab table-bordered" }, [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("tbody", [
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Family life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.one.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.one.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.one.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Social life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.two.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.two.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.two.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Spirituality")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.three.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.three.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.three.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Love life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.four.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.four.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.four.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Business / Careers")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.five.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.five.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q13.five.ans3))])
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("li", { staticClass: "mb-2" }, [
              _c("p", { staticClass: "question_preview mb-3" }, [
                _vm._v("I see myself here :")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "table-responsive" }, [
                _c("table", { staticClass: "table prev-tab table-bordered" }, [
                  _vm._m(1),
                  _vm._v(" "),
                  _c("tbody", [
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Family life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.one.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.one.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.one.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Social life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.two.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.two.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.two.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Spirituality")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.three.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.three.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.three.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Love life")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.four.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.four.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.four.ans3))])
                    ]),
                    _vm._v(" "),
                    _c("tr", [
                      _c("td", { attrs: { scope: "row" } }, [
                        _vm._v("Business / Careers")
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.five.ans1))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.five.ans2))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(_vm.questions.q14.five.ans3))])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my bright idea"
      ? _c("div", {}, [
          _c("h3", { staticClass: "w-100 text-center mb-3" }, [
            _vm._v("My Bright Idea Preview")
          ]),
          _vm._v(" "),
          _c("ol", [
            _c("div", { staticClass: "responses" }, [
              _c("li", [
                _c("h4", [_vm._v("What is my business idea?")]),
                _vm._v(" "),
                _c("p", { staticClass: "response" }, [
                  _vm._v(_vm._s(_vm.questions.one))
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("h4", [_vm._v("How did I come up with it?")]),
                _vm._v(" "),
                _c("p", { staticClass: "response" }, [
                  _vm._v(_vm._s(_vm.questions.two))
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("h4", [_vm._v("What excites me about it?")]),
                _vm._v(" "),
                _c("p", { staticClass: "response" }, [
                  _vm._v(_vm._s(_vm.questions.three))
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my business mission"
      ? _c("div", [
          _c("h3", { staticClass: "w-100 text-center mb-3" }, [
            _vm._v("My Business Mission Preview")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "mb-5" }, [
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("h4", { staticClass: "mb-3" }, [
                _vm._v("My Business Mission")
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Your Mission Statement provides a short summary of your business’ purpose and focus. Choose your words wisely and use terms that are easily understood and relevant to your business. Buzzwords and jargon are generally ineffective in a mission statement, because when your mission is hard to remember, it’s difficult for team members to align their daily activities with the goals outlined."
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._m(2),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: ""
                  },
                  domProps: { value: _vm.questions.one.q1 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q1", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q2,
                      expression: "questions.one.q2"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: ""
                  },
                  domProps: { value: _vm.questions.one.q2 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q2", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._m(4),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q3,
                      expression: "questions.one.q3"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: ""
                  },
                  domProps: { value: _vm.questions.one.q3 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q3", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._m(5),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q4,
                      expression: "questions.one.q4"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: ""
                  },
                  domProps: { value: _vm.questions.one.q4 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q4", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("Based on the foregoing, here’s my Mission Statement:")
                ]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two.q1,
                      expression: "questions.two.q1"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    readonly: "",
                    rows: "5",
                    placeholder: "200 characters",
                    maxlength: "200"
                  },
                  domProps: { value: _vm.questions.two.q1 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.two, "q1", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("h4", { staticClass: "mb-3" }, [
                _vm._v("Test your Mission Statement :")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("Could my competitors use exactly the same statement?")
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q2,
                        expression: "questions.two.q2"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.two.q2, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q2", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q2,
                        expression: "questions.two.q2"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.two.q2, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q2", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("Does it distinguish us from all other businesses?")
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q3,
                        expression: "questions.two.q3"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.two.q3, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q3", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q3,
                        expression: "questions.two.q3"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.two.q3, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q3", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "If I gave an employee or customer a blind screening test, asking her to read our mission statement and four others without identifying which is which, would she be able to tell which mission statement was ours?"
                  )
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q4,
                        expression: "questions.two.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.two.q4, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q4", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q4,
                        expression: "questions.two.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.two.q4, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.two, "q4", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my business vision"
      ? _c("div", [
          _c("div", { staticClass: "mb-5" }, [
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("h4", { staticClass: "mb-3" }, [_vm._v("My Business Vision")]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-2" }, [
                _vm._v(
                  "Consider your business a house. If you were building a house you would first have a vision of what you’d want the house to be: big or small, one story or one, in the heart of town or somewhere quiet etc."
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-2" }, [
                _vm._v(
                  "The same is true for building a business: you need a vision of what you hope to achieve. You don’t have to understand how to achieve it today, but it should help inform direction and help set priorities."
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "mb-3" }, [
                _vm._v(
                  "Your Vision should not be one of those things you just write and leave to gather dust, it should be considered every time you need to make an important decision in your business."
                )
              ]),
              _vm._v(" "),
              _c("h4", { staticClass: "mb-2" }, [
                _vm._v("When I imagine my business I hope:")
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "To make a lot of money"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one.q1)
                      ? _vm._i(_vm.questions.one.q1, "To make a lot of money") >
                        -1
                      : _vm.questions.one.q1
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one.q1,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "To make a lot of money",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions.one, "q1", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions.one,
                              "q1",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions.one, "q1", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("To make a lot of money")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "To use my creativity"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one.q1)
                      ? _vm._i(_vm.questions.one.q1, "To use my creativity") >
                        -1
                      : _vm.questions.one.q1
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one.q1,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "To use my creativity",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions.one, "q1", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions.one,
                              "q1",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions.one, "q1", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("To use my creativity")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "To have more flexibility in my life"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one.q1)
                      ? _vm._i(
                          _vm.questions.one.q1,
                          "To have more flexibility in my life"
                        ) > -1
                      : _vm.questions.one.q1
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one.q1,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "To have more flexibility in my life",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions.one, "q1", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions.one,
                              "q1",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions.one, "q1", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("To have more flexibility in my life")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "To work mostly alone but profitably"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one.q1)
                      ? _vm._i(
                          _vm.questions.one.q1,
                          "To work mostly alone but profitably"
                        ) > -1
                      : _vm.questions.one.q1
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one.q1,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "To work mostly alone but profitably",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions.one, "q1", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions.one,
                              "q1",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions.one, "q1", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("To work mostly alone but profitably")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "To build my company with partners and employees"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one.q1)
                      ? _vm._i(
                          _vm.questions.one.q1,
                          "To build my company with partners and employees"
                        ) > -1
                      : _vm.questions.one.q1
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one.q1,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v =
                            "To build my company with partners and employees",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions.one, "q1", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions.one,
                              "q1",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions.one, "q1", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("To build my company with partners and employees")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _vm._m(6),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one.q2,
                    expression: "questions.one.q2"
                  }
                ],
                staticClass: "form-control",
                attrs: { readonly: "", rows: "5" },
                domProps: { value: _vm.questions.one.q2 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.one, "q2", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _vm._m(7),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one.q3,
                    expression: "questions.one.q3"
                  }
                ],
                staticClass: "form-control form-borderless",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "type here.."
                },
                domProps: { value: _vm.questions.one.q3 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.one, "q3", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("h4", { staticClass: "mb-3" }, [_vm._v("Test Your Vision :")]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _vm._m(8),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q4,
                        expression: "questions.one.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q4, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q4", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q4,
                        expression: "questions.one.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q4, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q4", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _vm._m(9),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q5,
                        expression: "questions.one.q5"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q5, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q5", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q5,
                        expression: "questions.one.q5"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q5, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q5", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("Is it future-oriented?")]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q6,
                        expression: "questions.one.q6"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q6, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q6", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q6,
                        expression: "questions.one.q6"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q6, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q6", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my business values"
      ? _c("div", [
          _c("h3", { staticClass: "w-100 text-center mb-3" }, [
            _vm._v("My Bright Values Preview")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "mb-5" }, [
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("p", [
                _vm._v(
                  "Your business values define how your business will interact with others and how audiences connect with your business. Ideally, your core values should help you make decisions, like hiring or letting people go, while also prioritizing goals and plans for the business."
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-2" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("What parts of my business am I most proud of?")
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q1,
                      expression: "questions.one.q1"
                    }
                  ],
                  staticClass: "form-control form-borderless",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: "type here..."
                  },
                  domProps: { value: _vm.questions.one.q1 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q1", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-2" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("What is important to me/us?")
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q2,
                      expression: "questions.one.q2"
                    }
                  ],
                  staticClass: "form-control form-borderless",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: "type here..."
                  },
                  domProps: { value: _vm.questions.one.q2 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q2", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-2" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v(
                    "What will help guide me/us when faced with a difficult business decision?"
                  )
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one.q3,
                      expression: "questions.one.q3"
                    }
                  ],
                  staticClass: "form-control form-borderless",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: "type here..."
                  },
                  domProps: { value: _vm.questions.one.q3 },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.one, "q3", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("h4", { staticClass: "mb-3" }, [_vm._v("Test Your Values :")]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "Is this something I’ll still believe in 5 years? 10 years?"
                  )
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q4,
                        expression: "questions.one.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "radio", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q4, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q4", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q4,
                        expression: "questions.one.q4"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "radio", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q4, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q4", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "Is this something that I’m willing to hire, fire or partner based on?"
                  )
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q5,
                        expression: "questions.one.q5"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "radio", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q5, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q5", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q5,
                        expression: "questions.one.q5"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "radio", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q5, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q5", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("Is this something I can apply to customer relations?")
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q6,
                        expression: "questions.one.q6"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "radio", value: "Yes" },
                    domProps: { checked: _vm._q(_vm.questions.one.q6, "Yes") },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q6", "Yes")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Yes")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-radio" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q6,
                        expression: "questions.one.q6"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { type: "radio", readonly: "", value: "Not quite" },
                    domProps: {
                      checked: _vm._q(_vm.questions.one.q6, "Not quite")
                    },
                    on: {
                      change: function($event) {
                        return _vm.$set(_vm.questions.one, "q6", "Not quite")
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Not quite")
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "entrepreneur"
      ? _c("div", [
          _c("div", { staticClass: "form-group mb-5" }, [
            _c("h4", { staticClass: "mb-3" }, [
              _vm._v("WHAT KIND OF ENTREPRENEUR AM I?")
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-2" }, [
              _vm._v(
                "Many businesses fail because their founders are uncertain of what they really want to achieve with it, so do not structure the company and their responsibilities in ways that satisfy their personal needs and ambitions."
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-2" }, [
              _vm._v(
                "The Four C’s can sum up the personal goals of most entrepreneurs: Creativity, Control, Challenge, and Cash."
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-2" }, [
              _vm._v(
                "Certainly, we each want all four of these to some degree, but knowing which one we want most or need can help us structure our businesses in ways that are more fulfilling — even personally."
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-2" }, [
              _vm._v(
                "So what kind of entrepreneur are you -— Let’s find out what kind of entrepreneur(s) we are, shall we?"
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "one my-4 animated fadeIn" }, [
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I care about the look and feel of our products or packaging"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q1,
                          expression: "questions.one.creativity.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q1,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q1",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q1,
                          expression: "questions.one.creativity.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q1,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q1",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q1,
                          expression: "questions.one.creativity.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q1,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q1",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q1,
                          expression: "questions.one.creativity.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q1,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q1",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q1,
                          expression: "questions.one.creativity.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q1,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q1",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I have a strong sense of responsibility so I often find myself doing everything in my business"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q1,
                          expression: "questions.one.control.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q1,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q1",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q1,
                          expression: "questions.one.control.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q1, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q1",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q1,
                          expression: "questions.one.control.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q1, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q1",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q1,
                          expression: "questions.one.control.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q1,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q1",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q1,
                          expression: "questions.one.control.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q1,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q1",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I enjoy handling so many issues at once")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q1,
                          expression: "questions.one.challenge.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q1,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q1",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q1,
                          expression: "questions.one.challenge.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q1, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q1",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q1,
                          expression: "questions.one.challenge.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q1,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q1",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q1,
                          expression: "questions.one.challenge.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q1,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q1",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q1,
                          expression: "questions.one.challenge.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q1,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q1",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I would pick $5m over lunch with Bill Gates")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q1,
                          expression: "questions.one.cash.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q1,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q1",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q1,
                          expression: "questions.one.cash.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q1, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q1", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q1,
                          expression: "questions.one.cash.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q1, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q1",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q1,
                          expression: "questions.one.cash.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q1, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q1",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q1,
                          expression: "questions.one.cash.q1"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q1,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q1",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I worry about how our products and services come across to people"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q2,
                          expression: "questions.one.control.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q2,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q2",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q2,
                          expression: "questions.one.control.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q2, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q2",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q2,
                          expression: "questions.one.control.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q2, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q2",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q2,
                          expression: "questions.one.control.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q2,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q2",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q2,
                          expression: "questions.one.control.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q2,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q2",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I would buy a private jet if I could afford one")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q2,
                          expression: "questions.one.cash.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q2,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q2",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q2,
                          expression: "questions.one.cash.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q2, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q2", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q2,
                          expression: "questions.one.cash.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q2, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q2",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q2,
                          expression: "questions.one.cash.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q2, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q2",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q2,
                          expression: "questions.one.cash.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q2,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q2",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "two my-4 animated fadeIn" }, [
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I want my business to be impactful")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q6,
                          expression: "questions.one.creativity.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q6,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q6",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q6,
                          expression: "questions.one.creativity.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q6,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q6",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q6,
                          expression: "questions.one.creativity.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q6,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q6",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q6,
                          expression: "questions.one.creativity.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q6,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q6",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q6,
                          expression: "questions.one.creativity.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q6,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q6",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I’m a numbers guys")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q5,
                          expression: "questions.one.challenge.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q5,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q5",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q5,
                          expression: "questions.one.challenge.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q5, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q5",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q5,
                          expression: "questions.one.challenge.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q5,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q5",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q5,
                          expression: "questions.one.challenge.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q5,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q5",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q5,
                          expression: "questions.one.challenge.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q5,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q5",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I am open to partnerships that grow my business")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q5,
                          expression: "questions.one.cash.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q5,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q5",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q5,
                          expression: "questions.one.cash.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q5, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q5", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q5,
                          expression: "questions.one.cash.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q5, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q5",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q5,
                          expression: "questions.one.cash.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q5, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q5",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q5,
                          expression: "questions.one.cash.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q5,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q5",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I can spot a business opportunity from a mile away")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q5,
                          expression: "questions.one.creativity.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q5,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q5",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q5,
                          expression: "questions.one.creativity.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q5,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q5",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q5,
                          expression: "questions.one.creativity.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q5,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q5",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q5,
                          expression: "questions.one.creativity.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q5,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q5",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q5,
                          expression: "questions.one.creativity.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q5,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q5",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I am often described as a risk-taker")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q6,
                          expression: "questions.one.challenge.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q6,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q6",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q6,
                          expression: "questions.one.challenge.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q6, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q6",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q6,
                          expression: "questions.one.challenge.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q6,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q6",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q6,
                          expression: "questions.one.challenge.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q6,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q6",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q6,
                          expression: "questions.one.challenge.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q6,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q6",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I thrive best under structure")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q5,
                          expression: "questions.one.control.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q5,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q5",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q5,
                          expression: "questions.one.control.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q5, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q5",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q5,
                          expression: "questions.one.control.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q5, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q5",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q5,
                          expression: "questions.one.control.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q5,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q5",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q5,
                          expression: "questions.one.control.q5"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q5,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q5",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "three my-4 animated fadeIn" }, [
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I can create new products or services on a whim")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q2,
                          expression: "questions.one.creativity.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q2,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q2",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q2,
                          expression: "questions.one.creativity.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q2,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q2",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q2,
                          expression: "questions.one.creativity.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q2,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q2",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q2,
                          expression: "questions.one.creativity.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q2,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q2",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q2,
                          expression: "questions.one.creativity.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q2,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q2",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I dislike when I have to solve problems caused by others"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q2,
                          expression: "questions.one.challenge.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q2,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q2",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q2,
                          expression: "questions.one.challenge.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q2, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q2",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q2,
                          expression: "questions.one.challenge.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q2,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q2",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q2,
                          expression: "questions.one.challenge.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q2,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q2",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q2,
                          expression: "questions.one.challenge.q2"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q2,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q2",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I want to retire early and not need to work")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q6,
                          expression: "questions.one.cash.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q6,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q6",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q6,
                          expression: "questions.one.cash.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q6, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q6", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q6,
                          expression: "questions.one.cash.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q6, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q6",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q6,
                          expression: "questions.one.cash.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q6, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q6",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q6,
                          expression: "questions.one.cash.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q6,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q6",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I don’t like when people get distracted from the goal"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q6,
                          expression: "questions.one.control.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q6,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q6",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q6,
                          expression: "questions.one.control.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q6, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q6",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q6,
                          expression: "questions.one.control.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q6, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q6",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q6,
                          expression: "questions.one.control.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q6,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q6",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q6,
                          expression: "questions.one.control.q6"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q6,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q6",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("My business should fund my needs")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q4,
                          expression: "questions.one.cash.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q4,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q4",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q4,
                          expression: "questions.one.cash.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q4, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q4", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "label",
                    { staticClass: "custom-control custom-radio" },
                    [
                      _c("inputreadonly", {
                        staticClass: "custom-control-input",
                        attrs: { type: "radio", value: "Neutral" },
                        model: {
                          value: _vm.questions.one.cash.q4,
                          callback: function($$v) {
                            _vm.$set(_vm.questions.one.cash, "q4", $$v)
                          },
                          expression: "questions.one.cash.q4"
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "custom-control-indicator" }, [
                        _vm._v("Neutral")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q4,
                          expression: "questions.one.cash.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q4, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q4",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q4,
                          expression: "questions.one.cash.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q4,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q4",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I enjoy developing new ways of doing ’old’ things")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q4,
                          expression: "questions.one.creativity.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q4,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q4",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q4,
                          expression: "questions.one.creativity.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q4,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q4",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q4,
                          expression: "questions.one.creativity.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q4,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q4",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q4,
                          expression: "questions.one.creativity.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q4,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q4",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q4,
                          expression: "questions.one.creativity.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q4,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q4",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "four my-4 animated fadeIn" }, [
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("Business procedures and policies are my thing")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q3,
                          expression: "questions.one.creativity.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q3,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q3",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q3,
                          expression: "questions.one.creativity.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q3,
                          "Agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q3",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q3,
                          expression: "questions.one.creativity.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q3,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q3",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q3,
                          expression: "questions.one.creativity.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q3,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q3",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.creativity.q3,
                          expression: "questions.one.creativity.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.creativity.q3,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.creativity,
                            "q3",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "When I come up with an idea, I follow through to make sure it is executed just as I envisioned"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q3,
                          expression: "questions.one.control.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q3,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q3",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q3,
                          expression: "questions.one.control.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q3, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q3",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q3,
                          expression: "questions.one.control.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q3, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q3",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q3,
                          expression: "questions.one.control.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q3,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q3",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q3,
                          expression: "questions.one.control.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q3,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q3",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [_vm._v("I get bored with monotony at work")]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q3,
                          expression: "questions.one.challenge.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q3,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q3",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q3,
                          expression: "questions.one.challenge.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q3, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q3",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q3,
                          expression: "questions.one.challenge.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q3,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q3",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q3,
                          expression: "questions.one.challenge.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q3,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q3",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q3,
                          expression: "questions.one.challenge.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q3,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q3",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v("I am obsessed with the growth of my business")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q3,
                          expression: "questions.one.cash.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q3,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q3",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q3,
                          expression: "questions.one.cash.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q3, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(_vm.questions.one.cash, "q3", "Agree")
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q3,
                          expression: "questions.one.cash.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q3, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q3",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q3,
                          expression: "questions.one.cash.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.cash.q3, "Disagree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q3",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.cash.q3,
                          expression: "questions.one.cash.q3"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.cash.q3,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.cash,
                            "q3",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I want to be able to still maintain involvement in my non-work aspects of my life as I grow my business"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q4,
                          expression: "questions.one.control.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q4,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q4",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q4,
                          expression: "questions.one.control.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q4, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q4",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q4,
                          expression: "questions.one.control.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.control.q4, "Neutral")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q4",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q4,
                          expression: "questions.one.control.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q4,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q4",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.control.q4,
                          expression: "questions.one.control.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.control.q4,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.control,
                            "q4",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mb-3" }, [
                _c("p", [
                  _vm._v(
                    "I dislike when things don’t work as they’ve been designed to —perfectly"
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "options" }, [
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q4,
                          expression: "questions.one.challenge.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly agree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q4,
                          "Strongly agree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q4",
                            "Strongly agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q4,
                          expression: "questions.one.challenge.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Agree" },
                      domProps: {
                        checked: _vm._q(_vm.questions.one.challenge.q4, "Agree")
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q4",
                            "Agree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Agree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q4,
                          expression: "questions.one.challenge.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Neutral" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q4,
                          "Neutral"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q4",
                            "Neutral"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Neutral")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q4,
                          expression: "questions.one.challenge.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: { readonly: "", type: "radio", value: "Disagree" },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q4,
                          "Disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q4",
                            "Disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Disagree")
                    ])
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "custom-control custom-radio" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.challenge.q4,
                          expression: "questions.one.challenge.q4"
                        }
                      ],
                      staticClass: "custom-control-input",
                      attrs: {
                        readonly: "",
                        type: "radio",
                        value: "Strongly disagree"
                      },
                      domProps: {
                        checked: _vm._q(
                          _vm.questions.one.challenge.q4,
                          "Strongly disagree"
                        )
                      },
                      on: {
                        change: function($event) {
                          return _vm.$set(
                            _vm.questions.one.challenge,
                            "q4",
                            "Strongly disagree"
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "custom-control-indicator" }, [
                      _vm._v("Strongly disagree")
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my business concept"
      ? _c("div", [
          _c("div", [
            _c("legend", [_vm._v("My Business Concept")]),
            _vm._v(" "),
            _vm._m(10),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "It is easier to get a piece an existing market than to create a new one. The iPod wasn’t the first digital music player; Google wasn’t the first search engine; Facebook wasn’t the first social media platform. There are lots of advantages to being a follower."
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Please read our guide on Developing your Business Concept for a detailed insight on how to identify your strategic position."
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-5 w-100" }, [
              _c("h5", [_vm._v("MY BUSINESS OVERVIEW")]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [_vm._v("I am a")]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q1,
                        expression: "questions.one.q1"
                      }
                    ],
                    staticClass: "custom-select",
                    attrs: { readonly: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.questions.one,
                          "q1",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "selected" } }, [
                      _vm._v("Select one")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "retailer" } }, [
                      _vm._v("Retailer")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "manufacturer" } }, [
                      _vm._v("Manufacturer")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "distributor" } }, [
                      _vm._v("Distributor")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "service provider" } }, [
                      _vm._v("Service Provider")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("I belong in the industry")
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q2,
                        expression: "questions.one.q2"
                      }
                    ],
                    staticClass: "custom-select",
                    attrs: { readonly: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.questions.one,
                          "q2",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "selected" } }, [
                      _vm._v("Select one")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.industry, function(name, idx) {
                      return _c(
                        "option",
                        { key: idx, domProps: { value: name.name } },
                        [_vm._v(_vm._s(name.name))]
                      )
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("I run my business")
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.one.q3,
                        expression: "questions.one.q3"
                      }
                    ],
                    staticClass: "custom-select",
                    attrs: { readonly: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.questions.one,
                          "q3",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "selected" } }, [
                      _vm._v("Select one")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "online" } }, [
                      _vm._v("Online")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "offline" } }, [
                      _vm._v("Offline")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "both" } }, [_vm._v("Both")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", { attrs: { for: "" } }, [
                    _vm._v("My products/services are")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.questions.one.q4, function(item, idx) {
                    return _c("div", { key: idx, staticClass: "mb-1" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.name,
                            expression: "item.name"
                          }
                        ],
                        staticClass: "form-control form-borderless",
                        attrs: {
                          readonly: "",
                          type: "text",
                          "aria-describedby": "helpId",
                          placeholder: idx + 1 + "."
                        },
                        domProps: { value: item.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "name", $event.target.value)
                          }
                        }
                      })
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-5" }, [
              _c("h5", [_vm._v("MY CUSTOMER SEGMENTS")]),
              _vm._v(" "),
              _vm._m(11),
              _vm._v(" "),
              _c("p", { staticClass: "my-2" }, [
                _vm._v(
                  "A key to small business success is carving out a niche, rather than competing for every customer; small businesses just don’t have the resources of time or money to be generalist."
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _vm._m(12),
                _vm._v(" "),
                _c("div", { staticClass: "d-flex justify-content-between" }, [
                  _c("div", [
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Men"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Men") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Men",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Men")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Women"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Women") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Women",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Women")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Maternity"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Maternity") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Maternity",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Maternity")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Girls"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Girls") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Girls",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Girls")]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", [
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Boys"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Boys") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Boys",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Boys")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Toddlers"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Toddlers") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Toddlers",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Toddlers")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q1,
                              expression: "questions.two.q1"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Babies"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q1)
                              ? _vm._i(_vm.questions.two.q1, "Babies") > -1
                              : _vm.questions.two.q1
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q1,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Babies",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q1",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q1", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Babies")]
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two.q1[0],
                      expression: "questions.two.q1[0]"
                    }
                  ],
                  staticClass: "form-control form-borderless",
                  attrs: {
                    type: "text",
                    readonly: "",
                    "aria-describedby": "helpId",
                    placeholder: "For others specify here"
                  },
                  domProps: { value: _vm.questions.two.q1[0] },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.questions.two.q1, 0, $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _vm._m(13),
                _vm._v(" "),
                _c("div", { staticClass: "d-flex justify-content-between" }, [
                  _c("div", { staticClass: "P-1" }, [
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Lagos"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Lagos") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Lagos",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Lagos")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Abuja"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Abuja") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Abuja",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Abuja")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "PHC"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "PHC") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "PHC",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("PHC")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Africa"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Africa") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Africa",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Africa")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Nigeria"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Nigeria") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Nigeria",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Nigeria")]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "P-1" }, [
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Southwest"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Southwest") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Southwest",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Southwest")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Southeast"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Southeast") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Southeast",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Southeast")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "South-South"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "South-South") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "South-South",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("South-South")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "North-Central"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "North-Central") >
                                -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "North-Central",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("North-Central")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Northeast"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Northeast") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Northeast",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Northeast")]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "P-1" }, [
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Northwest"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Northwest") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Northwest",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Northwest")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Instagram"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Instagram") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Instagram",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Instagram")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Twitter"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Twitter") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Twitter",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Twitter")]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      { staticClass: "custom-control custom-checkbox" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.questions.two.q2,
                              expression: "questions.two.q2"
                            }
                          ],
                          staticClass: "custom-control-input",
                          attrs: {
                            readonly: "",
                            type: "checkbox",
                            value: "Whatsapp"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.questions.two.q2)
                              ? _vm._i(_vm.questions.two.q2, "Whatsapp") > -1
                              : _vm.questions.two.q2
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.questions.two.q2,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = "Whatsapp",
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.questions.two,
                                      "q2",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.questions.two, "q2", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "custom-control-indicator" },
                          [_vm._v("Whatsapp")]
                        )
                      ]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two.q2[0].name,
                      expression: "questions.two.q2[0].name"
                    }
                  ],
                  staticClass: "form-control form-borderless",
                  attrs: {
                    readonly: "",
                    type: "text",
                    "aria-describedby": "helpId",
                    placeholder: "For Others specify here"
                  },
                  domProps: { value: _vm.questions.two.q2[0].name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.questions.two.q2[0],
                        "name",
                        $event.target.value
                      )
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group mb-5" },
                [
                  _c("label", { attrs: { for: "" } }, [
                    _vm._v("My ideal customer works as any of the following:")
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.questions.two.q3, function(item, idx) {
                    return _c("div", { key: idx, staticClass: "mb-1" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.name,
                            expression: "item.name"
                          }
                        ],
                        staticClass: "form-control form-borderless",
                        attrs: {
                          readonly: "",
                          type: "text",
                          "aria-describedby": "helpId",
                          placeholder: idx + 1 + "."
                        },
                        domProps: { value: item.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "name", $event.target.value)
                          }
                        }
                      })
                    ])
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group mb-5" },
                [
                  _c("label", { attrs: { for: "" } }, [
                    _vm._v(
                      "My ideal customer wants the following from my product/service (List top 3)"
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.questions.two.q4, function(item, idx) {
                    return _c("div", { key: idx, staticClass: "mb-1" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.name,
                            expression: "item.name"
                          }
                        ],
                        staticClass: "form-control form-borderless",
                        attrs: {
                          readonly: "",
                          type: "text",
                          "aria-describedby": "helpId",
                          placeholder: idx + 1 + "."
                        },
                        domProps: { value: item.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "name", $event.target.value)
                          }
                        }
                      })
                    ])
                  })
                ],
                2
              ),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _c(
                  "label",
                  { staticClass: "d-flex", attrs: { for: "" } },
                  [
                    _vm._v(
                      "\n            My ideal customer earns\n            "
                    ),
                    _c("CurrencyInput", {
                      staticClass: "form-control w-25 form-borderless",
                      attrs: {
                        currency: _vm.currency,
                        placeholder: "NGN 0.00",
                        readonly: ""
                      },
                      model: {
                        value: _vm.questions.two.q5,
                        callback: function($$v) {
                          _vm.$set(_vm.questions.two, "q5", $$v)
                        },
                        expression: "questions.two.q5"
                      }
                    }),
                    _vm._v("per month\n          ")
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _c("label", { staticClass: "mb-3", attrs: { for: "" } }, [
                  _vm._v(
                    "My ideal customer will purchase my product/service at this frequency:"
                  )
                ]),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q6,
                        expression: "questions.two.q6"
                      }
                    ],
                    staticClass: "custom-select",
                    attrs: { readonly: "" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.questions.two,
                          "q6",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "selected" } }, [
                      _vm._v("Select one")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "Weekly" } }, [
                      _vm._v("Weekly")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "Monthly" } }, [
                      _vm._v("Monthly")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "Quarterly" } }, [
                      _vm._v("Quarterly")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "Annually" } }, [
                      _vm._v("Annually")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v("My ideal customer’s marital status is:")
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q7,
                        expression: "questions.two.q7"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "checkbox", value: "married" },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q7)
                        ? _vm._i(_vm.questions.two.q7, "married") > -1
                        : _vm.questions.two.q7
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q7,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "married",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q7", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Married")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q7,
                        expression: "questions.two.q7"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "checkbox", value: "single" },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q7)
                        ? _vm._i(_vm.questions.two.q7, "single") > -1
                        : _vm.questions.two.q7
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q7,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "single",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q7", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Single")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q7,
                        expression: "questions.two.q7"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "divorced"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q7)
                        ? _vm._i(_vm.questions.two.q7, "divorced") > -1
                        : _vm.questions.two.q7
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q7,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "divorced",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q7", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Divorced")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q7,
                        expression: "questions.two.q7"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: { readonly: "", type: "checkbox", value: "widowed" },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q7)
                        ? _vm._i(_vm.questions.two.q7, "widowed") > -1
                        : _vm.questions.two.q7
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q7,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "widowed",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q7",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q7", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("Widowed")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", { attrs: { for: "" } }, [
                  _vm._v(
                    "My ideal customer is in one or more of the following age brackets:"
                  )
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "ess than 5 Years"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "ess than 5 Years") > -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "ess than 5 Years",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("less than 5 Years")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "less than 11 Years"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "less than 11 Years") >
                          -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "less than 11 Years",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("less than 11 Years")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "12-17 Years"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "12-17 Years") > -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "12-17 Years",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("12-17 Years")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "18-34 Years"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "18-34 Years") > -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "18-34 Years",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("18-34 Years")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "35-50 Years"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "35-50 Years") > -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "35-50 Years",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("35-50 Years")
                  ])
                ]),
                _vm._v(" "),
                _c("label", { staticClass: "custom-control custom-checkbox" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.two.q8,
                        expression: "questions.two.q8"
                      }
                    ],
                    staticClass: "custom-control-input",
                    attrs: {
                      readonly: "",
                      type: "checkbox",
                      value: "50-70 Years "
                    },
                    domProps: {
                      checked: Array.isArray(_vm.questions.two.q8)
                        ? _vm._i(_vm.questions.two.q8, "50-70 Years ") > -1
                        : _vm.questions.two.q8
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.questions.two.q8,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "50-70 Years ",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.questions.two,
                                "q8",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.questions.two, "q8", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "custom-control-indicator" }, [
                    _vm._v("50-70 Years")
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "mb-5" }, [
              _c("h5", [_vm._v("UNIQUE VALUE PROPOSITION (UVP)")]),
              _vm._v(" "),
              _vm._m(14),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Understanding the needs, interests, habits, and preferences of your customers is the key to providing exceptional customer experiences."
                )
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "Your UVP helps you connect with your target market by describing your value to your customer and what makes you different"
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _c("table", { staticClass: "table" }, [
                  _vm._m(15),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.questions.three.q1, function(item, idx) {
                      return _c("tr", { key: idx }, [
                        _c("td", { attrs: { scope: "row" } }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: item.one,
                                expression: "item.one"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              readonly: "",
                              type: "text",
                              placeholder: "type here ..."
                            },
                            domProps: { value: item.one },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(item, "one", $event.target.value)
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: item.two,
                                expression: "item.two"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              readonly: "",
                              type: "text",
                              placeholder: "type here ..."
                            },
                            domProps: { value: item.two },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(item, "two", $event.target.value)
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("td", [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: item.three,
                                expression: "item.three"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              readonly: "",
                              type: "text",
                              placeholder: "type here ..."
                            },
                            domProps: { value: item.three },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(item, "three", $event.target.value)
                              }
                            }
                          })
                        ])
                      ])
                    }),
                    0
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group mb-5" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "" } }, [
                    _vm._v(
                      "Briefly summarise your customer’s pain point and how your product solves it:"
                    )
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.questions.four.q1,
                        expression: "questions.four.q1"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { readonly: "", rows: "3" },
                    domProps: { value: _vm.questions.four.q1 },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.questions.four, "q1", $event.target.value)
                      }
                    }
                  })
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my target market overview"
      ? _c("div", [
          _c("div", { staticClass: "form-group mb-5" }, [
            _c("h5", [_vm._v("MY TARGET MARKET OVERVIEW")]),
            _vm._v(" "),
            _vm._m(16),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.one,
                  expression: "questions.one"
                }
              ],
              staticClass: "form-control",
              attrs: { readonly: "", placeholder: "", rows: "3" },
              domProps: { value: _vm.questions.one },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "one", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group mb-5" }, [
            _vm._m(17),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.two,
                  expression: "questions.two"
                }
              ],
              staticClass: "form-control",
              attrs: { readonly: "", rows: "3" },
              domProps: { value: _vm.questions.two },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "two", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group mb-5" }, [
            _vm._m(18),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.three,
                  expression: "questions.three"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                readonly: "",
                "aria-describedby": "helpId",
                placeholder: ""
              },
              domProps: { value: _vm.questions.three },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "three", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group mb-5" }, [
            _c("label", { attrs: { for: "" } }, [
              _vm._v("Is it declining or increasing?")
            ]),
            _vm._v(" "),
            _c("label", { staticClass: "custom-control custom-radio" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.four,
                    expression: "questions.four"
                  }
                ],
                staticClass: "custom-control-input",
                attrs: { type: "radio", readonly: "", value: "Declining" },
                domProps: { checked: _vm._q(_vm.questions.four, "Declining") },
                on: {
                  change: function($event) {
                    return _vm.$set(_vm.questions, "four", "Declining")
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "custom-control-indicator" }, [
                _vm._v("Declining")
              ])
            ]),
            _vm._v(" "),
            _c("label", { staticClass: "custom-control custom-radio" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.four,
                    expression: "questions.four"
                  }
                ],
                staticClass: "custom-control-input",
                attrs: { type: "radio", readonly: "", value: "Increasing" },
                domProps: { checked: _vm._q(_vm.questions.four, "Increasing") },
                on: {
                  change: function($event) {
                    return _vm.$set(_vm.questions, "four", "Increasing")
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "custom-control-indicator" }, [
                _vm._v("Increasing")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group mb-5" }, [
            _c("label", { attrs: { for: "" } }, [_vm._v("At what rate?")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.questions.five,
                  expression: "questions.five"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                readonly: "",
                "aria-describedby": "helpId",
                placeholder: ""
              },
              domProps: { value: _vm.questions.five },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.questions, "five", $event.target.value)
                }
              }
            })
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my situation analysis"
      ? _c("div", [
          _c("div", {}, [
            _c("h5", [_vm._v("MY SITUATION ANALYSIS: SWOT ANALYSIS")]),
            _vm._v(" "),
            _vm._m(19),
            _vm._v(" "),
            _c("label", { attrs: { for: "" } }, [
              _vm._v("Let’s do a quick SWOT Analysis:")
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(20),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", { attrs: { scope: "row" } }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one,
                          expression: "questions.one"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.one },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "one", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two,
                          expression: "questions.two"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.two },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "two", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three,
                          expression: "questions.three"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.three },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "three", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.four,
                          expression: "questions.four"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.four },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "four", $event.target.value)
                        }
                      }
                    })
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my industry analysis"
      ? _c("div", [
          _c("div", {}, [
            _c("h5", [_vm._v("MY INDUSTRY ANALYSIS: COMPETITOR ANALYSIS")]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-5" }, [
              _vm._v(
                "This helps you determine the strengths and weaknesses of the competitors within your market, so that you can develop strategies that provide you with a distinct advantage and barriers that can prevent competition from entering your market, and any other weaknesses that can be exploited within the product development cycle."
              )
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(21),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.questions, function(item, idx) {
                  return _c("tr", { key: idx }, [
                    _c("td", { attrs: { scope: "row" } }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.one,
                            expression: "item.one"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { readonly: "", rows: "3" },
                        domProps: { value: item.one },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "one", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.two,
                            expression: "item.two"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { readonly: "", rows: "3" },
                        domProps: { value: item.two },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "two", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.three,
                            expression: "item.three"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { readonly: "", rows: "3" },
                        domProps: { value: item.three },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "three", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.four,
                            expression: "item.four"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { readonly: "", rows: "3" },
                        domProps: { value: item.four },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "four", $event.target.value)
                          }
                        }
                      })
                    ])
                  ])
                }),
                0
              )
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my marketing goals"
      ? _c("div", [
          _c("div", {}, [
            _c("h5", [_vm._v("MY MARKETING GOAL")]),
            _vm._v(" "),
            _vm._m(22),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(23),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", { attrs: { scope: "row" } }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.q1,
                          expression: "questions.one.q1"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Finance"
                      },
                      domProps: { value: _vm.questions.one.q1 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.one, "q1", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.q2,
                          expression: "questions.one.q2"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Customer /Market"
                      },
                      domProps: { value: _vm.questions.one.q2 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.one, "q2", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one.q3,
                          expression: "questions.one.q3"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { rows: "3", readonly: "", placeholder: "Budget" },
                      domProps: { value: _vm.questions.one.q3 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.one, "q3", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two.q1,
                          expression: "questions.two.q1"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Finance"
                      },
                      domProps: { value: _vm.questions.two.q1 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.two, "q1", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two.q2,
                          expression: "questions.two.q2"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Customer /Market"
                      },
                      domProps: { value: _vm.questions.two.q2 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.two, "q2", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two.q3,
                          expression: "questions.two.q3"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { rows: "3", readonly: "", placeholder: "Budget" },
                      domProps: { value: _vm.questions.two.q3 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions.two, "q3", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three.q1,
                          expression: "questions.three.q1"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Finance"
                      },
                      domProps: { value: _vm.questions.three.q1 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.questions.three,
                            "q1",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three.q2,
                          expression: "questions.three.q2"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        rows: "3",
                        readonly: "",
                        placeholder: "Customer /Market"
                      },
                      domProps: { value: _vm.questions.three.q2 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.questions.three,
                            "q2",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three.q3,
                          expression: "questions.three.q3"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { rows: "3", readonly: "", placeholder: "Budget" },
                      domProps: { value: _vm.questions.three.q3 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.questions.three,
                            "q3",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my marketing mix"
      ? _c("div", [
          _c("div", {}, [
            _c("h5", [_vm._v("MY MARKETING MIX (4P’s)")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "This set of marketing tools will help your business sell products or services to its target customers."
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-5" }, [
              _vm._v(
                "Consult the Marketing Mix Guide for detailed insight on how to adopt the different elements effectively."
              )
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(24),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.questions, function(item, idx) {
                  return _c("tr", { key: idx }, [
                    _c("td", { attrs: { scope: "row" } }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.one,
                            expression: "item.one"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          cols: "30",
                          rows: "3",
                          readonly: "",
                          placeholder: "Customer need"
                        },
                        domProps: { value: item.one },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "one", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.two,
                            expression: "item.two"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          readonly: "",
                          cols: "30",
                          rows: "3",
                          placeholder: "Product"
                        },
                        domProps: { value: item.two },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "two", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.three,
                            expression: "item.three"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          readonly: "",
                          type: "number",
                          placeholder: "Average market price"
                        },
                        domProps: { value: item.three },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "three", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.four,
                            expression: "item.four"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "number",
                          readonly: "",
                          placeholder: "My price"
                        },
                        domProps: { value: item.four },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "four", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.five,
                            expression: "item.five"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { cols: "30", rows: "3", placeholder: "Where" },
                        domProps: { value: item.five },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "five", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: item.six,
                            expression: "item.six"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          readonly: "",
                          cols: "30",
                          rows: "3",
                          placeholder: "How"
                        },
                        domProps: { value: item.six },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(item, "six", $event.target.value)
                          }
                        }
                      })
                    ])
                  ])
                }),
                0
              )
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my business acquisition strategy"
      ? _c("div", [
          _c("div", [
            _c("h5", [_vm._v("MY BUSINESS ACQUISITION STRATEGY")]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v("How many sales would I like to close within a week?")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one,
                    expression: "questions.one"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: ""
                },
                domProps: { value: _vm.questions.one },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions, "one", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "How many active opportunities do I need in order to meet my goal? — Conversion rate"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.two,
                    expression: "questions.two"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: ""
                },
                domProps: { value: _vm.questions.two },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions, "two", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "How many sales pitches will I have to perform each week in order to reach my target amount of active opportunities?"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.three,
                    expression: "questions.three"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: ""
                },
                domProps: { value: _vm.questions.three },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions, "three", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "How many cumulative (In total) Emails, Calls, Insta-dm’s, Insta-posts, Ads, Door-to-door sales do I need to make every week to reach my target sales pitches?"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.four,
                    expression: "questions.four"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: ""
                },
                domProps: { value: _vm.questions.four },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions, "four", $event.target.value)
                  }
                }
              })
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my lead generation overview"
      ? _c("div", [
          _c("div", [
            _c("h5", [_vm._v("MY LEAD GENERATION OVERVIEW")]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v("Where do most of my leads come from?")
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one,
                      expression: "questions.one"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "Instagram promotions"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one)
                      ? _vm._i(_vm.questions.one, "Instagram promotions") > -1
                      : _vm.questions.one
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "Instagram promotions",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions, "one", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions,
                              "one",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions, "one", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Instagram promotions")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one,
                      expression: "questions.one"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "checkbox", readonly: "", value: "Referrals" },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one)
                      ? _vm._i(_vm.questions.one, "Referrals") > -1
                      : _vm.questions.one
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "Referrals",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions, "one", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions,
                              "one",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions, "one", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Referrals")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one,
                      expression: "questions.one"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "checkbox",
                    readonly: "",
                    value: "Other Social Media Platforms"
                  },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one)
                      ? _vm._i(
                          _vm.questions.one,
                          "Other Social Media Platforms"
                        ) > -1
                      : _vm.questions.one
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "Other Social Media Platforms",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions, "one", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions,
                              "one",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions, "one", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Other Social Media Platforms")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-checkbox" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.one,
                      expression: "questions.one"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "checkbox", value: "Walk-in", readonly: "" },
                  domProps: {
                    checked: Array.isArray(_vm.questions.one)
                      ? _vm._i(_vm.questions.one, "Walk-in") > -1
                      : _vm.questions.one
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.questions.one,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = "Walk-in",
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.questions, "one", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.questions,
                              "one",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.questions, "one", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Walk-in")
                ])
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one[0].others,
                    expression: "questions.one[0].others"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "Others"
                },
                domProps: { value: _vm.questions.one[0].others },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.questions.one[0],
                      "others",
                      $event.target.value
                    )
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "How much of my revenue comes from my existing customers?"
                )
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two,
                      expression: "questions.two"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "10%-30%", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.two, "10%-30%") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "two", "10%-30%")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("10%-30%")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two,
                      expression: "questions.two"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "30%-60%", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.two, "30%-60%") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "two", "30%-60%")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("30%-60%")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two,
                      expression: "questions.two"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "60%-80%", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.two, "60%-80%") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "two", "60%-80%")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("60%-80%")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.two,
                      expression: "questions.two"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "80% - 100%", readonly: "" },
                  domProps: {
                    checked: _vm._q(_vm.questions.two, "80% - 100%")
                  },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "two", "80% - 100%")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("80% - 100%")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "How many people are involved in the process of each sale?"
                )
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.three,
                      expression: "questions.three"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "Just me", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.three, "Just me") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "three", "Just me")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Just me")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.three,
                      expression: "questions.three"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "1-3", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.three, "1-3") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "three", "1-3")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("1-3")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.three,
                      expression: "questions.three"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: { type: "radio", value: "5-10", readonly: "" },
                  domProps: { checked: _vm._q(_vm.questions.three, "5-10") },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "three", "5-10")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("5-10")
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "Do you pitch one person from a target account, or multiple people?"
                )
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.four,
                      expression: "questions.four"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "radio",
                    value: "Target Account",
                    readonly: ""
                  },
                  domProps: {
                    checked: _vm._q(_vm.questions.four, "Target Account")
                  },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "four", "Target Account")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Target Account")
                ])
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "custom-control custom-radio" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.questions.four,
                      expression: "questions.four"
                    }
                  ],
                  staticClass: "custom-control-input",
                  attrs: {
                    type: "radio",
                    value: "Multiple People",
                    readonly: ""
                  },
                  domProps: {
                    checked: _vm._q(_vm.questions.four, "Multiple People")
                  },
                  on: {
                    change: function($event) {
                      return _vm.$set(_vm.questions, "four", "Multiple People")
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "custom-control-indicator" }, [
                  _vm._v("Multiple People")
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my sales goals"
      ? _c("div", [
          _c("div", [
            _c("h5", [_vm._v("MY SALES GOALS")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Here we map your sales goals with key activities and resources required to achieve them"
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-5" }, [
              _vm._v(
                "If you have read our guide on Developing your Sales Plan, then you already know exactly how to achieve this."
              )
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(25),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", { attrs: { scope: "row" } }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one,
                          expression: "questions.one"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", cols: "30", rows: "10" },
                      domProps: { value: _vm.questions.one },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "one", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two,
                          expression: "questions.two"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", cols: "30", rows: "10" },
                      domProps: { value: _vm.questions.two },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "two", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three,
                          expression: "questions.three"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", cols: "30", rows: "10" },
                      domProps: { value: _vm.questions.three },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "three", $event.target.value)
                        }
                      }
                    })
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my sales funnel"
      ? _c("div", [
          _c("div", [
            _c("h5", [_vm._v("MY SALES FUNNEL")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "To simplify this, we have made our funnel into three main parts—top, middle and bottom. Briefly describe each part of your sales funnel in the table below, stating the goals and activities to achieve them. Ensure that there is a flow between all 3 parts."
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "mb-5" }, [
              _vm._v(
                "If you have read our guide on Developing your Sales Plan, then you already know exactly how to achieve this."
              )
            ]),
            _vm._v(" "),
            _c("table", { staticClass: "table" }, [
              _vm._m(26),
              _vm._v(" "),
              _c("tbody", [
                _c("tr", [
                  _c("td", { attrs: { scope: "row" } }, [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.one,
                          expression: "questions.one"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { readonly: "", cols: "30", rows: "10" },
                      domProps: { value: _vm.questions.one },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "one", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.two,
                          expression: "questions.two"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { cols: "30", readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.two },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "two", $event.target.value)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.questions.three,
                          expression: "questions.three"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { cols: "30", readonly: "", rows: "10" },
                      domProps: { value: _vm.questions.three },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.questions, "three", $event.target.value)
                        }
                      }
                    })
                  ])
                ])
              ])
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _vm.title == "my customer retention strategy"
      ? _c("div", [
          _c("div", [
            _c("h5", [_vm._v("MY CUSTOMER RETENTION STRATEGY")]),
            _vm._v(" "),
            _vm._m(27),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "The goal here is simple; to attract, monetize and convert customers to brand evangelists."
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v("Two ways that I will attract my ideal customer:")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one.q1,
                    expression: "questions.one.q1"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "1."
                },
                domProps: { value: _vm.questions.one.q1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.one, "q1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.one.q2,
                    expression: "questions.one.q2"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "2."
                },
                domProps: { value: _vm.questions.one.q2 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.one, "q2", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "Two ways that I will continue to meet the expectations of my customer:"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.two.q1,
                    expression: "questions.two.q1"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "1."
                },
                domProps: { value: _vm.questions.two.q1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.two, "q1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.two.q2,
                    expression: "questions.two.q2"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  readonly: "",
                  placeholder: "2."
                },
                domProps: { value: _vm.questions.two.q2 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.two, "q2", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "Two ways that I will make my customer continue to buy from me:"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.three.q1,
                    expression: "questions.three.q1"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "1."
                },
                domProps: { value: _vm.questions.three.q1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.three, "q1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.three.q2,
                    expression: "questions.three.q2"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "2."
                },
                domProps: { value: _vm.questions.three.q2 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.three, "q2", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group mb-5" }, [
              _c("label", { attrs: { for: "" } }, [
                _vm._v(
                  "Two ways that I will make my customers recommend me to others:"
                )
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.four.q1,
                    expression: "questions.four.q1"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  readonly: "",
                  "aria-describedby": "helpId",
                  placeholder: "1."
                },
                domProps: { value: _vm.questions.four.q1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.four, "q1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.questions.four.q2,
                    expression: "questions.four.q2"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "2."
                },
                domProps: { value: _vm.questions.four.q2 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.questions.four, "q2", $event.target.value)
                  }
                }
              })
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.bap
      ? _c("div", { staticClass: "w-100" }, [
          _c(
            "button",
            {
              staticClass: "elevated_btn elevated_btn_sm mx-auto text-main",
              attrs: { type: "button" },
              on: { click: _vm.preview }
            },
            [_vm._v("Close Preview")]
          )
        ])
      : _c("div", { staticClass: "butt" }, [
          _c(
            "button",
            {
              staticClass: "elevated_btn elevated_btn_sm text-main",
              attrs: { type: "button" },
              on: { click: _vm.preview }
            },
            [_vm._v("Re-edit")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass:
                "elevated_btn elevated_btn_sm text-white btn-compliment",
              attrs: { type: "button" },
              on: { click: _vm.submit }
            },
            [_vm._v("Submit")]
          )
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th"),
        _vm._v(" "),
        _c("th", [_vm._v("Name & Job rlie")]),
        _vm._v(" "),
        _c("th", [_vm._v("What traits of theirs do i admire")]),
        _vm._v(" "),
        _c("th", [_vm._v("How can i incorporate these traits into my life")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th"),
        _vm._v(" "),
        _c("th", [_vm._v("In 2years")]),
        _vm._v(" "),
        _c("th", [_vm._v("In 5years")]),
        _vm._v(" "),
        _c("th", [_vm._v("In 10years")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _vm._v("\n            What do we do?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "Clearly outline the main purpose of your organisation, and what they do"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _vm._v("\n            How do we do it?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title: "Mention how you plan on achieving the mission"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _vm._v("\n            Whom do we do it for?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title: "Clearly state the audience of your mission"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _vm._v("\n            What value are we bringing?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title: "Outline the benefits and values of your mission"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n          My BHAV --\n          "),
      _c("em", [_vm._v("“big, hairy, audacious vision”")]),
      _vm._v(" "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "Even if it seems unattainable at first glance that’s perfectly all right. It is what you envision, after all."
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n          My Achievement Timeframe\n          "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "How long is the time period within which you expect to achieve the goal that you have envisioned ? "
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("\n            Is it huge, incredible and bold?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "Even if it seems unattainable at first glance that’s perfectly all right. It is what you envision, after all."
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("\n            Is it specific, clear and concise?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "Will anyone who reads your BHAV be able to grasp it at first look without any need for questions or clarifications?"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("blockquote", [
      _c("em", [_vm._v("“Anything that won’t sell, I don’t want to invent.”")]),
      _vm._v("\n        – Thomas Edison\n      ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("blockquote", [
      _c("em", [
        _vm._v(
          '"Want to know one sure way to fail in small business? Try to sell to everyone!"'
        )
      ]),
      _vm._v("\n          -- BizGuruh\n        ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n            My ideal customers are?\n            "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title: "Here is some suggestion below"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v(
        "\n            My ideal customer is mostly in one or more of the following places:\n            "
      ),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title: "CHere is some suggestion below"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("blockquote", [
      _c("em", [
        _vm._v(
          '"The more you understand about your own company and how you differ from others— the better you are able to compete"'
        )
      ]),
      _vm._v("\n          “-- BizGuruh\n        ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("My Customer")]),
        _vm._v(" "),
        _c("th", [_vm._v("Thier Pain Point/Problem")]),
        _vm._v(" "),
        _c("th", [_vm._v("How my Product/Service Solves it")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n        Briefly define your target market:\n        "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            " Typically, your customers cut across different categories that grows as you get closer to the end user, give yourself a competitive edge by thinking of each of these customers and plan for their needs and motivation."
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n        Briefly describe your customer persona:\n        "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "While your target audience encompasses the elements that show what different groups of people have in common, the customer persona looks for differentiating factors .i.e what makes each person within a certain demographic unique?\n          Simply put, your ideal customer is the one who benefits the most from what you’re selling. \n          "
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "" } }, [
      _vm._v("\n        What is the estimated size of your market?\n        "),
      _c("i", {
        staticClass: "fas fa-question-circle",
        attrs: {
          "aria-hidden": "true",
          "data-toggle": "tooltip",
          "data-placement": "bottom",
          title:
            "This helps to determine if there are enough customers to serve"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("b", [_vm._v("SWOT Analysis")]),
      _vm._v(
        " is the most renowned tool for audit and analysis of the overall strategic position of the business and its environment. Its key purpose is to identify the strategies that will best align an organization’s resources and capabilities to the requirements of the environment in which it operates.\n      "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Strength")]),
        _vm._v(" "),
        _c("th", [_vm._v("Weakness")]),
        _vm._v(" "),
        _c("th", [_vm._v("Opportunities")]),
        _vm._v(" "),
        _c("th", [_vm._v("Threats")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Who are my competitors?")]),
        _vm._v(" "),
        _c("th", [_vm._v("What are the similarities?")]),
        _vm._v(" "),
        _c("th", [_vm._v("What can i learn from them?")]),
        _vm._v(" "),
        _c("th", [_vm._v("How can i differentiate from them?")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("strong", [_vm._v("BIZGURUH TIP:")]),
      _vm._v(
        " To make your marketing goals and budget accurate and effective, keep national and even local trends and events in mind. Adjust your annual and monthly budgets according to these trends.\n      "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("My short term marketing goals")]),
        _vm._v(" "),
        _c("th", [_vm._v("My mid term marketing goals")]),
        _vm._v(" "),
        _c("th", [_vm._v("My long term marketing goals")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Cutomer need")]),
        _vm._v(" "),
        _c("th", [_vm._v("Product")]),
        _vm._v(" "),
        _c("th", [_vm._v("Price")]),
        _vm._v(" "),
        _c("th", [_vm._v("Promotion")]),
        _vm._v(" "),
        _c("th", [_vm._v("Place")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Sales Goals")]),
        _vm._v(" "),
        _c("th", [_vm._v("Key Activities")]),
        _vm._v(" "),
        _c("th", [_vm._v("Key Reources")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("The top of my funnel")]),
        _vm._v(" "),
        _c("th", [_vm._v("The middle of my funnel")]),
        _vm._v(" "),
        _c("th", [_vm._v("The bottom of my funnel")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("blockquote", [
      _c("em", [
        _vm._v(
          "Imagine you’re in a relationship where you don’t feel valued, soon enough you will want out."
        )
      ]),
      _vm._v(" —BizGuruh\n      ")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2e5b7052", module.exports)
  }
}

/***/ })

});