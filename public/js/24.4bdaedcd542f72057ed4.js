webpackJsonp([24],{

/***/ 1065:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1066);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("62b335e5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-64bfbc6b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./homeTopComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-64bfbc6b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./homeTopComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1066:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.vendorMainDis[data-v-64bfbc6b] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n#page-wrapper[data-v-64bfbc6b] {\n  width: 100%;\n  min-height: 100vh;\n  max-height: 100vh;\n  overflow-y: scroll;\n  padding: 15px;\n}\n.chatbox[data-v-64bfbc6b] {\n  position: fixed;\n  bottom: 120px;\n  right: 30px;\n  z-index: 9;\n  background-color: #f7f8fa;\n}\n.chatIcon[data-v-64bfbc6b] {\n  font-size: 14px;\n  margin-top: -4px;\n}\n.mobile[data-v-64bfbc6b] {\n  display: none;\n}\n.chat[data-v-64bfbc6b] {\n  position: relative;\n}\n@media (max-width: 425px) {\n#page-wrapper[data-v-64bfbc6b] {\n    padding: 10px;\n}\n.chatbox[data-v-64bfbc6b] {\n    bottom: 80px;\n    right: 1px;\n}\n.scrollUp[data-v-64bfbc6b] {\n    font-size: 12px;\n    right: 15px;\n}\n.desktop[data-v-64bfbc6b] {\n    display: none;\n}\n.mobile[data-v-64bfbc6b] {\n    display: block;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1067:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_components_chatComponent__ = __webpack_require__(751);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_components_chatComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__user_components_chatComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sideVendorComponent__ = __webpack_require__(973);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sideVendorComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__sideVendorComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stickyHeaderComponent__ = __webpack_require__(978);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stickyHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__stickyHeaderComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      shake: false,
      profileImage: "",
      myNav: false,
      showChat: false,
      showIcon: true,
      showNotify: 0,
      left: 45,
      inboxCount: 0,
      groups: [],
      vendorSub: 1
    };
  },
  components: {
    "app-sidebar": __WEBPACK_IMPORTED_MODULE_1__sideVendorComponent___default.a,
    "app-sticky": __WEBPACK_IMPORTED_MODULE_2__stickyHeaderComponent___default.a,
    chat: __WEBPACK_IMPORTED_MODULE_0__user_components_chatComponent___default.a
  },
  watch: {
    $route: 'closeNav'
  },
  mounted: function mounted() {
    var _this = this;

    this.getVendorSub();
    this.fetchCourses();
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));

      axios.get("/api/getvendor/" + authVendor.storeName, {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          if (response.data.data.length > 0) {
            _this.profile = response.data.data;
            _this.isActive = false;
            response.data.data.forEach(function (value) {
              _this.profileImage = authVendor.image;
            });
          }
        } else {
          _this.$router.push("/vendor/auth");
        }
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    shakeButton: function shakeButton() {
      var _this2 = this;

      this.shake = true;
      setTimeout(function () {
        _this2.shake = false;
      }, 1000);
    },
    getVendorSub: function getVendorSub() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authVendor"));

      if (user !== null) {
        axios.get("/api/vendor-sub/" + user.id).then(function (res) {
          if (res.status == 200) {
            if (res.data.status !== 'failed') {
              _this3.vendorSub = res.data;
            }
          }
        });
      }
    },
    fetchInbox: function fetchInbox() {
      var inbox = localStorage.getItem("inboxCount");
      if (inbox !== null) {
        this.inboxCount = inbox;
      }
    },
    joinGroups: function joinGroups() {
      var _this4 = this;

      this.groups.forEach(function (item) {
        Echo.join("group." + item.id).listen("GroupMessageSent", function (e) {
          _this4.inboxCount++;
          localStorage.setItem("inboxCount", _this4.inboxCount);
          _this4.fetchInbox();
        });
      });
    },
    fetchCourses: function fetchCourses() {
      var _this5 = this;

      var user = JSON.parse(localStorage.getItem("authVendor"));

      axios.get("/api/vendorCourses/" + user.id, {
        headers: {
          Authorization: "Bearer " + user.access_token
        }
      }).then(function (res) {
        _this5.groups = res.data.data;
        _this5.joinGroups();
      });
    },
    chatLeft: function chatLeft() {
      if (this.$route.path == "/chat") {
        this.left = 45;
      } else {
        this.left = 30;
      }
    },
    switchChat: function switchChat() {
      this.showChat = !this.showChat;
      localStorage.removeItem("notify");
      this.showNotify = 0;
    },
    closeChat: function closeChat() {
      this.showChat = false;
    },
    gotoChat: function gotoChat() {
      this.$router.push("/chat");
    },
    showNav: function showNav() {
      this.myNav = !this.myNav;
    },
    closeNav: function closeNav() {
      this.myNav = false;
    }
  }
});

/***/ }),

/***/ 1068:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "main-content" },
    [
      _c("app-sticky", {
        attrs: {
          profileImage: _vm.profileImage,
          vendorSub: _vm.vendorSub,
          shake: _vm.shake,
          myNav: _vm.myNav
        },
        on: { "mobile-nav": _vm.showNav }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "vendorMainDis" },
        [
          _c("app-sidebar", {
            attrs: { showNav: _vm.myNav, vendorSub: _vm.vendorSub },
            on: { shakeButton: _vm.shakeButton }
          }),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { id: "page-wrapper" } },
            [
              _c("router-view", {
                attrs: { inboxCount: _vm.inboxCount, vendorSub: _vm.vendorSub },
                on: {
                  addedProfileImageToHeader: function($event) {
                    _vm.profileImage = $event
                  }
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-64bfbc6b", module.exports)
  }
}

/***/ }),

/***/ 485:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1065)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1067)
/* template */
var __vue_template__ = __webpack_require__(1068)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-64bfbc6b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/homeTopComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-64bfbc6b", Component.options)
  } else {
    hotAPI.reload("data-v-64bfbc6b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 751:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(752)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(754)
/* template */
var __vue_template__ = __webpack_require__(755)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-81c0cb64"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/chatComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-81c0cb64", Component.options)
  } else {
    hotAPI.reload("data-v-81c0cb64", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 752:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(753);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("425ab458", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-81c0cb64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chatComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-81c0cb64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chatComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 753:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-81c0cb64] {\n  height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background-color: #f7f8fa;\n}\n.mini[data-v-81c0cb64] {\n  height: auto;\n  background-color: #f7f8fa;\n  width: 350px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-shadow: rgba(0, 0, 0, 0.1) 2px 4px 15px;\n          box-shadow: rgba(0, 0, 0, 0.1) 2px 4px 15px;\n  border-radius: 5px;\n  /* padding: 15px; */\n}\n.chat-messages[data-v-81c0cb64] {\n  width: 100%;\n}\n.chat-users[data-v-81c0cb64] {\n  width: 30%;\n}\n.isTyping[data-v-81c0cb64] {\n  font-size: 12px;\n}\n", ""]);

// exports


/***/ }),

/***/ 754:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["left"],
  data: function data() {
    return {
      messages: [],
      user: JSON.parse(localStorage.getItem("authUser")),
      users: [],
      activeUser: false,
      typingTimer: false,
      large: false,
      height: 350
    };
  },
  created: function created() {
    var _this = this;

    if (this.$route.query.name == "large") {
      this.large = true;
      this.height = 800;
    }
    this.fetchMessages();

    Echo.join("chat").here(function (user) {
      _this.users = user;
    }).joining(function (user) {
      _this.users.push(user);

      if (user.vendor_user_id != 0) {
        if (_this.$route.path === "/chat") {
          _this.$toasted.info(user.name + " is now available for chat");
        }
      } else {
        if (_this.$route.path === "/chat") {
          _this.$toasted.info(user.name + " joined");
        }
      }
    }).leaving(function (user) {
      _this.users = _this.users.filter(function (u) {
        return u.id != user.id;
      });
      if (_this.$route.path === "/chat") {}
    }).listen("MessageSent", function (e) {
      _this.messages.push({
        message: e.message.message,
        created_at: e.message.created_at,
        user: e.user
      });
    }).listenForWhisper("typing", function (user) {
      _this.activeUser = user;
      if (_this.typingTimer) {
        clearTimeout(_this.typingTimer);
      }
      _this.typingTimer = setTimeout(function () {
        _this.activeUser = false;
      }, 1000);
    });
  },


  methods: {
    switchChat: function switchChat() {
      this.$emit("switch");
    },
    sendTypingEvent: function sendTypingEvent() {
      Echo.join("chat").whisper("typing", this.user);
    },
    fetchMessages: function fetchMessages() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        axios.get("/api/messages", {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this2.messages = response.data;
        });
      }
    },
    addMessage: function addMessage(message) {
      var user = JSON.parse(localStorage.getItem("authUser"));
      if (user !== null) {
        this.messages.push(message);

        axios.post("/api/message", message, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {});
      }
    }
  }
});

/***/ }),

/***/ 755:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _c(
      "div",
      { staticClass: "mini shadow rounded", class: { "w-100": _vm.large } },
      [
        _c("chat-users", {
          staticClass: "d-none",
          attrs: { users: _vm.users }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "chat-messages" },
          [
            _c("chat-messages", {
              attrs: {
                messages: _vm.messages,
                activeUser: _vm.activeUser,
                height: _vm.height,
                left: _vm.left
              },
              on: { switch: _vm.switchChat }
            }),
            _vm._v(" "),
            _c("chat-form", {
              attrs: { user: _vm.user },
              on: {
                messagesent: _vm.addMessage,
                sendTypingEvent: _vm.sendTypingEvent
              }
            })
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-81c0cb64", module.exports)
  }
}

/***/ }),

/***/ 973:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(974)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(976)
/* template */
var __vue_template__ = __webpack_require__(977)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1779c6aa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/sideVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1779c6aa", Component.options)
  } else {
    hotAPI.reload("data-v-1779c6aa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 974:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(975);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("058369d5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 975:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\na[data-v-1779c6aa] {\n  color: #b8c7ce !important;\n}\nspan[data-v-1779c6aa] {\n  font-size: 16px;\n}\n.router-link-exact-active[data-v-1779c6aa]{\nbackground: #1e282c;\nborder-left-color: #3c8dbc;\n}\n.faded[data-v-1779c6aa]{\n  cursor:not-allowed!important;\n  color: rgba(255, 255, 255, 0.4);\n}\n.main-sidebar[data-v-1779c6aa] {\n  \n  background-image: url(\"/images/education.jpg\");\n  background-size: cover;\n  z-index: 0;\n  width: 20%;\n  position: relative;\n  padding-top:0;\n}\n.overlay[data-v-1779c6aa] {\n  background: rgba(29, 53, 73, 0.8);\n  z-index: 1;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.sidebar[data-v-1779c6aa] {\n  padding-top: 30px;\n  z-index: 2;\n  position: relative;\n}\n.main-sidebar-nav[data-v-1779c6aa] {\n  display: block !important;\n  -webkit-transform: translate(-230px, 0);\n  transform: translate(0, 0);\n}\n@media only screen and (max-width: 768px) {\n.main-sidebar[data-v-1779c6aa] {\n    display: none;\n}\n.main-sidebar[data-v-1779c6aa] {\n    width:100%;\n}\nspan[data-v-1779c6aa] {\n    font-size: 16px;\n}\n.sidebar ul li a[data-v-1779c6aa] {\n    padding: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "side-vendor-component",
  props: ["showNav", 'vendorSub'],
  data: function data() {
    return {
      id: "",
      token: "",
      backgroundColor: "",
      fontSize: "",
      color: "",
      sidebar: "",
      webAdmin: false,
      storeAdmin: false,
      authVendor: {},
      username: ""
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      this.username = authVendor.storeName;
      this.authVendor = authVendor;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.sidebarBackgroundColor;
        // this.fontSize = response.data.data.sidebarFontSize;
        // this.color = response.data.data.sidebarColor;
        // this.sidebar = response.data.data.sidebarMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth?type=login");
    }
  },

  methods: {
    shakeButton: function shakeButton() {
      this.$emit('shakeButton');
    },
    logout: function logout() {
      Engagespot.clearUser();
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth?type=login");
    }
  }
});

/***/ }),

/***/ 977:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "aside",
    { staticClass: "main-sidebar", class: { "main-sidebar-nav": _vm.showNav } },
    [
      _c("div", { staticClass: "overlay" }),
      _vm._v(" "),
      _c("section", { staticClass: "sidebar" }, [
        _c(
          "ul",
          { staticClass: "sidebar-menu", attrs: { "data-widget": "tree" } },
          [
            _c("li", { staticClass: "px-2" }, [
              _vm.vendorSub < 3
                ? _c(
                    "button",
                    { staticClass: "elevated_btn elevated_btn_sm text-main" },
                    [_vm._v("Upgrade")]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: "/vendor/dashboard" } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-chart-line pr-3" }),
                      _vm._v(" Dashboard\n          ")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "pull-right-container" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: { name: "viewVendorProduct" } } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-box-open pr-3" }),
                      _vm._v(" Resources\n          ")
                    ])
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "orderVendor" } } },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-bell pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v("Subscribers\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Subscribers\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "messagesVendor" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-inbox pr-3" }),
                          _vm._v(" Inbox\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-inbox pr-3 faded" }),
                        _vm._v(" Inbox\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: "/vendor-form-questions" } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fa fa-wpforms pr-3" }),
                          _vm._v(" Forms\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Forms\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "VendorStatistics" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-chart-bar pr-3" }),
                          _vm._v(" Insights\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded ", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-chart-bar pr-3 faded" }),
                        _vm._v(" Insights\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.authVendor.type === "vendor"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.authVendor.type === "expert"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c(
                  "span",
                  {
                    on: {
                      click: function($event) {
                        return _vm.logout()
                      }
                    }
                  },
                  [
                    _c("i", {
                      staticClass: "fa fa-sign-out pr-3",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" Logout\n          ")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._m(0)
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "p-3" }, [
      _c("span", { attrs: { id: "notification" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1779c6aa", module.exports)
  }
}

/***/ }),

/***/ 978:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(979)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(981)
/* template */
var __vue_template__ = __webpack_require__(982)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-41d38ab3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/stickyHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-41d38ab3", Component.options)
  } else {
    hotAPI.reload("data-v-41d38ab3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 979:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(980);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3f8a0fc8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 980:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.header-section[data-v-41d38ab3] {\n  background: #fff;\n  position: relative;\n  padding: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hamburger[data-v-41d38ab3]{\n  padding:10px;\n}\n.button-blue[data-v-41d38ab3] {\n  font-size: 15px;\n}\n.mobile-nav[data-v-41d38ab3] {\n  display: none;\n}\n.header-right[data-v-41d38ab3] {\n  float: unset;\n  width: auto !important;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.imgDev[data-v-41d38ab3] {\n  width: 25px;\n  height: 25px;\n  border-radius: 50%;\n  margin-top: 0;\n}\n.dropDownVendor[data-v-41d38ab3] {\n  background-color: #ffffff;\n  position: absolute;\n  width: 150px;\n  left: -30px;\n  top: 57px;\n}\n.pointer[data-v-41d38ab3] {\n  font-size: 16px;\n  padding-left: 20px;\n  cursor: pointer;\n  color: hsl(207, 46%, 20%);\n}\n.pointer[data-v-41d38ab3]:hover {\n  color: #333333;\n}\n.nav_item[data-v-41d38ab3] {\n  color: hsl(207, 46%, 20%);\n  font-size: 15px;\n  border-radius: 5px;\n}\n.list[data-v-41d38ab3] {\n  border-bottom: 1px solid #f5f5f5;\n  padding: 5px;\n  position: relative;\n}\n.list[data-v-41d38ab3]::before {\n  position: absolute;\n  content: \"\";\n  top: -6px;\n  right: 20%;\n  width: 15px;\n  height: 15px;\n  background: #fff;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n/* .userAcc {\n  margin: 25px 0 5px;\n} */\nol[data-v-41d38ab3],\nul[data-v-41d38ab3] {\n  list-style: none;\n}\n.accountCaret[data-v-41d38ab3] {\n  margin-left: 0 !important;\n}\n.accAvatar[data-v-41d38ab3] {\n  margin-right: 0;\n}\n.accountProfile[data-v-41d38ab3] {\n  font-size: 16px;\n  padding: 5px 15px;\n  background: #f7f8fa;\n  position: relative;\n}\n.biz[data-v-41d38ab3] {\n  color: #a4c2db !important;\n}\n.guru[data-v-41d38ab3] {\n  color: #333333;\n}\n.fa-bars[data-v-41d38ab3] {\n  color: #a4c2db;\n}\n.businessLogo[data-v-41d38ab3] {\n  font-size: 24px;\n  margin-right: auto;\n}\n.slide-fade-enter-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.3s ease;\n  transition: all 0.3s ease;\n}\n.slide-fade-leave-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.2s ease;\n  transition: all 0.2s ease;\n}\n.slide-fade-enter[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n.slide-fade-leave-to[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n@media only screen and (max-width: 768px) {\n.userAccName[data-v-41d38ab3] {\n    display: none;\n}\n.animated[data-v-41d38ab3] {\n    display: none;\n}\n.dropDownVendor[data-v-41d38ab3] {\n    left: -100px;\n}\n.imgDev[data-v-41d38ab3] {\n    width: 22px;\n    height: 22px;\n    border-radius: 50%;\n    margin-top: 0;\n}\n.businessLogo[data-v-41d38ab3] {\n    font-size: 25px;\n    margin-top: 5px;\n    margin-left: auto;\n    margin-right: auto;\n    margin-bottom: 5px;\n}\n.mobile-nav[data-v-41d38ab3] {\n    display: inline;\n    margin-right: 25px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var user = JSON.parse(localStorage.getItem("authVendor"));
Engagespot.init("cywCppTlinzpmjVHxFcYoiuuKlnefc");
if (user !== null) {
  Engagespot.identifyUser(user.id);
}

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "sticky-header-component",
  props: ["profileImage", "vendorSub", "shake", "myNav"],
  data: function data() {
    return {
      id: "",
      token: "",
      username: "",
      linkTag: false,
      backgroundColor: "",
      fontSize: "",
      color: "",
      topMenu: "",
      showDropDown: false,
      links: [{
        name: "Profile",
        url: "vendorProfile"
      }, {
        name: "Product",
        url: "viewVendorProduct"
      }, {
        name: "Logout",
        url: "logout"
      }]
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.username = authVendor.storeName;
      this.linkTag = true;
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.topMenuBackgroundColor;
        // this.fontSize = response.data.data.topMenuFontSize;
        // this.color = response.data.data.topMenuColor;
        // this.topMenu = response.data.data.topMenuMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    logout: function logout() {
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth");
    },
    openNav: function openNav() {
      this.$emit("mobile-nav");
    }
  }
});

/***/ }),

/***/ 982:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "sticky-header header-section shadow-sm",
      style: { backgroundColor: _vm.backgroundColor }
    },
    [
      _c("div", { staticClass: "mobile-nav" }, [
        _c(
          "span",
          {
            staticClass:
              "d-flex justify-content-start align-items-center mobile-nav",
            on: { click: _vm.openNav }
          },
          [
            _c(
              "button",
              {
                staticClass: "hamburger hamburger--collapse",
                class: { "is-active": _vm.myNav },
                attrs: {
                  tabindex: "0",
                  "aria-label": "Menu",
                  role: "button",
                  "aria-controls": "navigation",
                  type: "button"
                }
              },
              [_vm._m(0)]
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "businessLogo mr-4" },
        [
          _c("router-link", { attrs: { to: "/" } }, [
            _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
            _c("span", { staticClass: "guru" }, [_vm._v("Guruh")])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.$route.meta.vendor
        ? _c(
            "router-link",
            {
              staticClass: "nav_item p-1 border",
              attrs: { to: "/vendor/home" }
            },
            [
              _c("i", {
                staticClass: "fa fa-home",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v("\nMy Home\n   ")
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "header-right ml-auto" }, [
        _vm.vendorSub < 3
          ? _c(
              "button",
              {
                staticClass: "button-blue mr-4 animated",
                class: { shake: _vm.shake }
              },
              [_vm._v("Upgrade")]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("div", { attrs: { id: "notification mr-3" } }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "accountProfile rounded-pill" },
          [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.showDropDown = !_vm.showDropDown
                  }
                }
              },
              [
                _c("span", { staticClass: "userAcc accAvatar" }, [
                  _vm.profileImage !== ""
                    ? _c("span", { staticClass: "pr-2" }, [
                        _c("img", {
                          staticClass: "imgDev",
                          attrs: { src: _vm.profileImage, alt: "avatar" }
                        })
                      ])
                    : _c("i", {
                        staticClass: "fas fa-user-circle-o text-main pr-2"
                      }),
                  _vm._v(" "),
                  _c("span", { staticClass: "userAccName text-main" }, [
                    _vm._v(_vm._s(_vm.username))
                  ])
                ]),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa userAcc accountCaret text-main",
                  class: {
                    "fa-caret-up": _vm.showDropDown,
                    "fa-caret-down": !_vm.showDropDown
                  },
                  attrs: { "aria-hidden": "true" }
                })
              ]
            ),
            _vm._v(" "),
            _c("transition", { attrs: { name: "slide-fade" } }, [
              _vm.showDropDown
                ? _c("div", { staticClass: "dropDownVendor shadow-sm" }, [
                    _c("ul", { staticClass: "menu list pl0 pa0 ma0" }, [
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "viewVendorProduct" } }
                            },
                            [_vm._v("Resources")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: {
                                to: {
                                  name: "vendorProfile",
                                  params: { profile: _vm.username }
                                }
                              }
                            },
                            [_vm._v("Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "vendorConfig" } }
                            },
                            [_vm._v("Update Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("li", { staticClass: "list" }, [
                        _c(
                          "a",
                          {
                            staticClass: "dd-link pointer hover-bg-moon-gray",
                            on: { click: _vm.logout }
                          },
                          [_vm._v("Logout")]
                        )
                      ])
                    ])
                  ])
                : _vm._e()
            ])
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "hamburger-box" }, [
      _c("span", { staticClass: "hamburger-inner" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-41d38ab3", module.exports)
  }
}

/***/ })

});