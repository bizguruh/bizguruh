webpackJsonp([92],{

/***/ 1115:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1116);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("47571e5a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-96c0300a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorGroupChat.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-96c0300a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./vendorGroupChat.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1116:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-96c0300a] {\n  background: white;\n  height: 91vh;\n  padding: 15px;\n}\n.active[data-v-96c0300a] {\n  background: white;\n  border-right: 5px solid #ccc;\n  border-bottom: none !important;\n  border-top: none !important;\n}\n.new_message[data-v-96c0300a] {\n  font-size: 11px;\n  color: rgba(0, 0, 0, 0.6);\n}\n.name_user[data-v-96c0300a] {\n  font-size: 13px;\n}\n.time[data-v-96c0300a] {\n  color: rgba(0, 0, 0, 0.4);\n  font-size: 10px;\n}\n.active[data-v-96c0300a] {\n  background: white;\n  border-right: 5px solid #ccc;\n  border-bottom: none !important;\n  border-top: none !important;\n}\n.form-group[data-v-96c0300a] {\n  position: relative;\n  overflow: hidden;\n}\n.bg-ccc[data-v-96c0300a] {\n  background: #ccc;\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.submit[data-v-96c0300a] {\n  width: 85%;\n  margin: 0 auto;\n}\n.search_icon[data-v-96c0300a] {\n  position: absolute;\n  top: 50%;\n  right: 10px;\n  font-size: 16px;\n  margin-top: -8px;\n  color: rgba(0, 0, 0, 0.5);\n}\n.profile_img[data-v-96c0300a] {\n  width: 30px;\n  height: 30px;\n  border-radius: 50%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  margin-right: 10px;\n}\n.align_image[data-v-96c0300a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.chat_header[data-v-96c0300a] {\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n}\n.message-tab[data-v-96c0300a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 100%;\n  border: 1px solid #f7f8fa;\n}\n.message-body[data-v-96c0300a] {\n    height: 82%;\n  max-height: 82%;\n  overflow-y: scroll;\n  background: #333;\n}\n.message-body div li[data-v-96c0300a] {\n  width: 50%;\n  padding: 10px 30px;\n  font-size: 15px;\n}\n.chat-left[data-v-96c0300a] {\n  background-color: #d1e0ed;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  position: relative;\n   line-height: 1.2;\n}\n.chat-left[data-v-96c0300a]::before {\n  content: \"\";\n  background-color: #d1e0ed;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(50deg);\n          transform: skew(50deg);\n  position: absolute;\n  left: -2px;\n  top: 0px;\n}\n.chat-right[data-v-96c0300a] {\n  position: relative;\n  background: #f7f8fa;\n  padding: 15px 10px;\n  border-radius: 10px;\n  -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n          box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n  clear: both;\n  float: unset;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  width: -webkit-max-content;\n  width: -moz-max-content;\n  width: max-content;\n  margin-left: auto;\n  text-align: left;\n   line-height: 1.2;\n}\n.chat-right[data-v-96c0300a]::before {\n  content: \"\";\n  background: #f7f8fa;\n  height: 14px;\n  width: 20px;\n  -webkit-transform: skew(-50deg);\n          transform: skew(-50deg);\n  position: absolute;\n  right: -2px;\n  top: 0px;\n}\n.form-group[data-v-96c0300a] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\nul[data-v-96c0300a],\nol[data-v-96c0300a],\nli[data-v-96c0300a] {\n  list-style: none;\n}\n.users[data-v-96c0300a] {\n  width: 30%;\n  padding: 15px;\n  background: #f7f8fa;\n  height: 100%;\n  overflow: hidden;\n}\n.message[data-v-96c0300a] {\n  height: 100%;\n  max-height: 100%;\n  width: 100%;\n  position: relative;\n}\n.user_name[data-v-96c0300a] {\n  height: 100%;\n  max-height: 100%;\n  overflow-y: scroll;\n  font-size: 14px;\n}\n.user_name li[data-v-96c0300a] {\n  padding: 10px 15px 10px;\n  border-bottom: 1px solid hsl(220, 23%, 90%);\n  cursor: pointer;\n}\n@media (max-width: 768px) {\n.main-page[data-v-96c0300a] {\n    padding: 0;\n}\n.mobile[data-v-96c0300a] {\n    display: block;\n}\n.chat_header[data-v-96c0300a] {\n    font-size: 14px;\n    padding: 20px 10px !important;\n}\n.message-tab[data-v-96c0300a] {\n    position: relative;\n}\n.message-body div li[data-v-96c0300a] {\n    width: 70%;\n    padding: 10px;\n    font-size: 14px;\n}\n.submit[data-v-96c0300a]{\n    width: 97%;\n}\n.pm[data-v-96c0300a]{\n    font-size: 12px;\n}\n.user_name li[data-v-96c0300a] {\n    padding: 15px 10px;\n    font-size: 12px;\n}\n.users[data-v-96c0300a] {\n    width: 60%;\n    position: absolute;\n    left: 0;\n    top: 0;\n    bottom: 0;\n    z-index: 1;\n    padding: 15px 10px;\n    -webkit-box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n            box-shadow: 0 0.065rem 0.12rem rgba(0, 0, 0, 0.075) !important;\n}\n.profile_img[data-v-96c0300a] {\n    width: 20px;\n    height: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      messages: [],
      users: [],
      course_title: "",
      message: "",
      id: null,
      active: 0,
      search: "",
      activeUser: 0,
      courses: [],
      hide: false,
      new_message: ""
    };
  },
  mounted: function mounted() {
    if (window.innerWidth < 768) {
      this.hide = true;
    } else {
      this.hide = false;
    }
    var user = JSON.parse(localStorage.getItem("authVendor"));
    this.id = user.id;
    this.fetchMessages();
    localStorage.removeItem("inboxCount");
  },

  computed: {
    sortMessages: function sortMessages() {
      var _this = this;

      return this.messages.filter(function (item) {
        return item.course_id == _this.activeUser;
      });
    },
    sortCourses: function sortCourses() {
      var _this2 = this;

      return this.courses.filter(function (item) {
        return item.courses.title.toLowerCase().includes(_this2.search.toLowerCase());
      });
    }
  },
  methods: {
    showUsers: function showUsers() {
      this.hide = !this.hide;
    },
    getMessage: function getMessage(course, index, title) {
      this.active = index;
      this.activeUser = course;
      this.course_title = title;

      this.new_message = "";
      if (window.innerWidth < 768) {
        this.hide = true;
      }
    },
    fetchMessages: function fetchMessages() {
      var _this3 = this;

      var user = JSON.parse(localStorage.getItem("authVendor"));
      if (user !== null) {
        axios.get("/api/vendorCourses/" + this.id, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (res) {
          _this3.courses = res.data.data;
          _this3.activeUser = _this3.courses[0].id;
          _this3.course_title = _this3.courses[0].courses.title;
          res.data.data.forEach(function (item) {
            Echo.join("group." + item.id).listen("GroupMessageSent", function (e) {

              _this3.messages.push({
                message: e.message.message,
                user: e.user,
                course_id: e.message.course_id,
                created_at: e.message.created_at
              });
              _this3.new_message = e.message.course_id;
            }).here(function (user) {
              _this3.users = user;
            });
          });

          axios.get("/api/vendor-group-messages", {
            headers: {
              Authorization: "Bearer " + user.access_token
            }
          }).then(function (response) {
            _this3.messages = response.data;
          });
        });
      }
    },
    sendMessage: function sendMessage() {
      var _this4 = this;

      var user = JSON.parse(localStorage.getItem("authVendor"));

      if (user !== null) {
        var text = {
          message: this.message,
          course_id: this.activeUser,
          user_id: user.id,
          created_at: new Date(),
          user: {
            name: user.storeName,
            vendor_user_id: user.id
          }
        };
        this.messages.push(text);

        var data = {
          message: this.message,
          course_id: this.activeUser
        };

        axios.post("/api/vendor-group-message", data, {
          headers: {
            Authorization: "Bearer " + user.access_token
          }
        }).then(function (response) {
          _this4.message = "";
          if (response.status == 200) {
            localStorage.removeItem("inboxCount");
          }
        }).catch(function (err) {
          _this4.$toasted.error("Message not sent!!");
        });
      }
    }
  }
});

/***/ }),

/***/ 1118:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page" }, [
    _c("div", { staticClass: "message-tab" }, [
      !_vm.hide
        ? _c("div", { staticClass: "users animated slideIn" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.search,
                    expression: "search"
                  }
                ],
                staticClass: "form-control rounded-pill border-0",
                attrs: {
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Search"
                },
                domProps: { value: _vm.search },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.search = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("i", {
                staticClass: "fa fa-search search_icon",
                attrs: { "aria-hidden": "true" }
              })
            ]),
            _vm._v(" "),
            _vm.sortCourses.length
              ? _c("ul", { staticClass: "user_name" }, [
                  _c(
                    "span",
                    _vm._l(_vm.sortCourses, function(course, idx) {
                      return _c(
                        "li",
                        {
                          key: idx,
                          staticClass: "toCaps align_image",
                          class: { active: _vm.active == idx },
                          on: {
                            click: function($event) {
                              return _vm.getMessage(
                                course.id,
                                idx,
                                course.courses.title
                              )
                            }
                          }
                        },
                        [
                          _c(
                            "div",
                            [
                              _vm._v(
                                "\n              " +
                                  _vm._s(course.courses.title.toLowerCase()) +
                                  "\n              "
                              ),
                              _vm._l(_vm.messages, function(message, id) {
                                return message.course_id == course.id &&
                                  _vm.messages.length - 1 == id
                                  ? _c(
                                      "p",
                                      { key: id, staticClass: "new_message" },
                                      [_vm._v(_vm._s(message.message))]
                                    )
                                  : _vm._e()
                              })
                            ],
                            2
                          )
                        ]
                      )
                    }),
                    0
                  )
                ])
              : _c("ul", { staticClass: "user_name" }, [
                  _c("li", [_vm._v("No Course available")])
                ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _c(
        "form",
        {
          staticClass: "message",
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.sendMessage($event)
            }
          }
        },
        [
          _c(
            "div",
            { staticClass: "toCaps p-2 chat_header align_image text-right" },
            [
              _vm.course_title !== ""
                ? _c("p", { staticClass: "toCaps w-75 text-left" }, [
                    _vm._v(
                      _vm._s(_vm.course_title.toLowerCase()) + " Group Chat"
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("i", {
                staticClass: "fa fa-ellipsis-v text-main ml-auto pr-3 mobile",
                attrs: { "aria-hidden": "true" },
                on: { click: _vm.showUsers }
              })
            ]
          ),
          _vm._v(" "),
          _vm.sortMessages.length
            ? _c(
                "ul",
                {
                  directives: [
                    { name: "chat-scroll", rawName: "v-chat-scroll" }
                  ],
                  staticClass: "message-body"
                },
                [
                  _c(
                    "div",
                    { staticClass: "p-2 py-3 pb-4" },
                    _vm._l(_vm.sortMessages, function(message, index) {
                      return _c(
                        "li",
                        {
                          key: index,
                          staticClass: "py-1",
                          class: {
                            "text-right": _vm.id == message.user.vendor_user_id,
                            "ml-auto": _vm.id == message.user.vendor_user_id
                          }
                        },
                        [
                          _c(
                            "div",
                            {
                              class: {
                                "chat-right":
                                  _vm.id == message.user.vendor_user_id,
                                "chat-left":
                                  _vm.id !== message.user.vendor_user_id
                              }
                            },
                            [
                              _c(
                                "p",
                                {
                                  staticClass:
                                    "toCaps text-muted name_user mb-2"
                                },
                                [
                                  _vm._v(
                                    _vm._s(message.user.name.toLowerCase())
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("p", [
                                _vm._v(
                                  "\n                " +
                                    _vm._s(message.message) +
                                    "\n                "
                                ),
                                _c("span", { staticClass: "time" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("moment")(
                                        message.created_at,
                                        "HH:mm"
                                      )
                                    )
                                  )
                                ])
                              ])
                            ]
                          )
                        ]
                      )
                    }),
                    0
                  )
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "bg-ccc p-2" }, [
            _c("div", { staticClass: "form-group submit" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.message,
                    expression: "message"
                  }
                ],
                staticClass: "form-control rounded-pill mr-3",
                attrs: {
                  required: "",
                  type: "text",
                  "aria-describedby": "helpId",
                  placeholder: "Type your message here .."
                },
                domProps: { value: _vm.message },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.message = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(0)
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass:
          "elevated_btn elevated_btn_sm m-0 btn-compliment text-white shadow-none rounded-pill",
        attrs: { type: "submit" }
      },
      [
        _c("i", {
          staticClass: "fas fa-paper-plane text-white",
          attrs: { "aria-hidden": "true" }
        })
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-96c0300a", module.exports)
  }
}

/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1115)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1117)
/* template */
var __vue_template__ = __webpack_require__(1118)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-96c0300a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/vendorGroupChat.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-96c0300a", Component.options)
  } else {
    hotAPI.reload("data-v-96c0300a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});