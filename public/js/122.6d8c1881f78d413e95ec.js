webpackJsonp([122],{

/***/ 1432:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1433);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("71f8e4c4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a31b9652\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privacyComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a31b9652\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./privacyComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1433:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.bread ul[data-v-a31b9652] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: left;\n        -ms-flex-pack: left;\n            justify-content: left;\n    list-style-type: none;\n   margin: 10px 0px 30px;\n    padding:5px 15px;\n    background:#fff;\n}\n.bread ul > li[data-v-a31b9652] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    float: left;\n    height: 10px;\n    width: auto;\n    font-weight: bold;\n    font-size: .8em;\n    cursor: default;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.bread ul > li[data-v-a31b9652]:not(:last-child)::after {\n    content: '/';\n    float: right;\n    font-size: .8em;\n    margin: 0 .5em;\n    cursor: default;\n}\n.linked[data-v-a31b9652] {\n    cursor: pointer;\n    font-size: 1em;\n    font-weight: normal;\n}\n.privacy[data-v-a31b9652] {\n        padding: 150px;\n}\nh1[data-v-a31b9652] {\n        margin:30px 0;\n}\ndiv[data-v-a31b9652] {\n        margin: 10px 0;\n}\nh4[data-v-a31b9652] {\n        margin-top: 100px;\n        margin-bottom: 20px;\n}\nol[data-v-a31b9652] {\n        margin: 20px 0;\n}\n.disc[data-v-a31b9652] {\n        list-style-type: circle;\n        margin-left: 50px;\n}\n.ols[data-v-a31b9652] {\n        list-style-type: decimal;\n        margin-left: 50px;\n}\n.ols li[data-v-a31b9652] {\n        margin: 10px 0;\n}\n.things[data-v-a31b9652] {\n        list-style-type: circle;\n        margin-left: 50px;\n}\n@media(max-width:425px){\n.privacy[data-v-a31b9652]{\n        padding:50px 10px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "privacy-component",
    data: function data() {
        return {
            breadcrumbList: []
        };
    },
    mounted: function mounted() {
        this.updateList();
    },

    watch: {
        '$route': function $route() {
            this.updateList();
        }
    },
    methods: {
        routeTo: function routeTo(pRouteTo) {
            if (this.breadcrumbList[pRouteTo].link) {
                this.$router.push(this.breadcrumbList[pRouteTo].link);
            }
        },
        updateList: function updateList() {
            this.breadcrumbList = this.$route.meta.breadcrumb;
        }
    }
});

/***/ }),

/***/ 1435:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "privacy" }, [
    _c("div", { staticClass: "bread" }, [
      _c(
        "ul",
        _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
          return _c(
            "li",
            {
              key: index,
              class: { linked: !!breadcrumb.link },
              on: {
                click: function($event) {
                  return _vm.routeTo(index)
                }
              }
            },
            [
              _vm._v(
                "\n               " +
                  _vm._s(breadcrumb.name) +
                  "\n             "
              )
            ]
          )
        }),
        0
      )
    ]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Your privacy is important to BizGuruh and always has been. So we've developed a Privacy Policy that covers how we collect, use, disclose, transfer, and store your information. Please take a moment to familiarize yourself with our privacy practices and let us know if you have any questions."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "By visiting BizGuruh, you are accepting the practices described in this Privacy Notice."
      )
    ]),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "To provide the BizGuruh Services, we must process information about you. The type of information that we collect depends on how you use our Services."
      )
    ]),
    _vm._v(" "),
    _vm._m(4),
    _vm._v(" "),
    _vm._m(5),
    _vm._v(" "),
    _vm._m(6),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We use the information we have to deliver our Services, including to personalise features and make suggestions for you (such as groups or events you may be interested in or topics you may want to follow) on and off our Products. To create personalised Services that are unique and relevant to you, we use your connections, preferences, interests and activities based on the data that we collect and learn from you and others (including any data with special protection you choose to provide); how you use and interact with our Products; and the people, places or things that you're connected to and interested in on and off our Products."
      )
    ]),
    _vm._v(" "),
    _vm._m(7),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " We use the information we have to develop, test and improve our Services, including by conducting surveys and research, and testing and troubleshooting new products and features."
      )
    ]),
    _vm._v(" "),
    _vm._m(8),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We use the information that we have to communicate with you about our Services and let you know about our Policies and Terms. We also use your information to respond to you when you contact us."
      )
    ]),
    _vm._v(" "),
    _vm._m(9),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " We use the information that we have (including from research partners who we collaborate with) to conduct and support research and innovation on business education, particularly tailored to the African market and technological advancement for business activities within the region."
      )
    ]),
    _vm._v(" "),
    _vm._m(10),
    _vm._v(" "),
    _c("div", [
      _vm._v("Your information is shared with others in the following ways:")
    ]),
    _vm._v(" "),
    _vm._m(11),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "When, you use BizGuruh to communicate with people or businesses, those people and businesses can see the content you send. Your network can also see actions that you have taken on our Products, including engagement with ads and sponsored content. We also let other accounts see who has viewed BizGuruh Stories."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "Public information can be seen by anyone, on or off our Services, including if they don't have an account. This includes your BizGuruh username, any information you share with a public audience, information in your public profile on BizGuruh, and content you share on   any other public forum, such as BizGuruh Marketplace. You, other people using BizGuruh and we can provide access to or send public information to anyone on or off our Services, in search results or through tools and APIs. Public information can also be seen, accessed, reshared or downloaded through third-party services such as search engines, APIs and offline media such as TV, and by apps, websites and other services that integrate with our Products."
      )
    ]),
    _vm._v(" "),
    _vm._m(12),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " People in your networks can see signals telling them whether you are active on our platform, or when you last used our Services."
      )
    ]),
    _vm._v(" "),
    _vm._m(13),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " When you choose to use third-party apps, websites or other services that use, or are integrated with, our Products, they can receive information about what you post or share. Information collected by these third-party services is subject to their own terms and policies, not this one."
      )
    ]),
    _vm._v(" "),
    _vm._m(14),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We work with third-party partners who help us provide and improve our Services or who use BizGuruh Business Tools to grow their businesses, which makes it possible to operate our companies and provide free services to people around the world. We don't sell any of your information to anyone and we never will. We also impose strict restrictions on how our partners can use and disclose the data we provide. Here are the types of third parties that we share information with:"
      )
    ]),
    _vm._v(" "),
    _vm._m(15),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " We provide aggregated statistics and insights that help people and businesses understand how people are engaging with their posts, listings, Pages, videos and other content on and off the BizGuruh Platform. For example, Page admins and BizGuruh business profiles receive information about the number of people or accounts who viewed, reacted to or commented on their posts, as well as aggregate demographic and other information that helps them understand interactions with their Page or account."
      )
    ]),
    _vm._v(" "),
    _vm._m(16),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We provide advertisers with reports about the kinds of people seeing their ads and how their ads are performing, but we don't share information that personally identifies you (information such as your name or email address that by itself can be used to contact you or identifies who you are) unless you give us permission. For example, we provide general demographic and interest information to advertisers (for example, that an ad was seen by a woman between the ages of 25 and 34 who lives in Ghana and likes software engineering) to help them better understand their audience. We also confirm which BizGuruh ads led you to make a purchase or take an action with an advertiser."
      )
    ]),
    _vm._v(" "),
    _vm._m(17),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We collect device-specific information when you install, access, or use our Services. This includes information such as hardware model, operating system information, browser information, IP address, mobile network information including phone number, and device identifiers. We collect device location information if you use our location features, such as when you choose to share your location with your contacts, view locations nearby or those others have shared with you, and the like, and for diagnostics and troubleshooting purposes such as if you are having trouble with our app’s location features."
      )
    ]),
    _vm._v(" "),
    _vm._m(18),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We also provide information and content to research partners and academics to conduct research that advances scholarship and innovation that supports our business or mission and enhances discovery and innovation on business education, African economies and technological advancement in business activities within African markets."
      )
    ]),
    _vm._v(" "),
    _vm._m(19),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " We share information with law enforcement or in response to legal requests"
      )
    ]),
    _vm._v(" "),
    _vm._m(20),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "BizGuruh takes the security of your personal data very seriously. We make every effort to protect your personal data from misuse, interference, loss, unauthorised access, modification or disclosure."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "Our measures include implementing appropriate access controls, investing in the latest Information Security Capabilities to protect the IT environments we leverage, and ensuring we encrypt, pseudonymise and anonymise personal data wherever possible."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "Access to your personal data is only permitted among our employees and agents on a need-to-know basis and subject to strict contractual confidentiality obligations when processed by third-parties."
      )
    ]),
    _vm._v(" "),
    _vm._m(21),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "Your rights in relation to your personal data, how it is processed."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "Where we process your personal data, you have a number of rights over how the data is processed and can exercise these rights at any point. We have provided an overview of these rights below together with what this entails for you. You can exercise your rights by sending an email or submitting a request through the “Contact Us” form on our websites."
      )
    ]),
    _vm._v(" "),
    _vm._m(22),
    _vm._v(" "),
    _vm._m(23),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We provide you with the ability to access, rectify, port and delete your data."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " We store data until it is no longer necessary to provide our services or until your account is deleted – whichever comes first. This is a case-by-case determination that depends on things such as the nature of the data, why it is collected and processed, and relevant legal or operational retention needs. For example, when you search for something on BizGuruh, you can access and delete that query from within your search history at any time, but the log of that search is deleted after six months. If you submit a copy of your valid photo ID for account verification purposes, we delete that copy 30 days after submission."
      )
    ]),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        " When you delete your account, we delete things that you have posted, and you won't be able to recover this information later. Information that others have shared about you isn't part of your account and won't be deleted. If you don't want to delete your account but want to temporarily stop using the Products, you can deactivate your account instead. To delete your account at any time, please visit the BizGuruh settings."
      )
    ]),
    _vm._v(" "),
    _vm._m(24),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We may collect, use, preserve, and share your information if we have a good-faith belief that it is reasonably necessary to: (a) respond pursuant to applicable law or regulations, to legal process, or to government requests; (b) enforce our Terms and any other applicable terms and policies, including for investigations of potential violations; (c) detect, investigate, prevent, and address fraud and other illegal activity, security, or technical issues; or (d) protect the rights, property, and safety of our users, or others."
      )
    ]),
    _vm._v(" "),
    _vm._m(25),
    _vm._v(" "),
    _c("div", [
      _vm._v(
        "We'll notify you before we make changes to this Policy and give you the opportunity to review the revised Policy before you choose to continue using our Products."
      )
    ]),
    _vm._v(" "),
    _c("h4", [_vm._v("Contact Us")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "If you have questions about our Privacy Policy, please contact us;"
      )
    ]),
    _vm._v(" "),
    _c("p", [_vm._v(" Coeur Consulting Options Limited.")]),
    _vm._v(" "),
    _c("p", [_vm._v("Privacy Policy")]),
    _vm._v(" "),
    _c("p", [_vm._v(" Captain House, 34 Aje Road,")]),
    _vm._v(" "),
    _c("p", [_vm._v("Sabo Yaba, Lagos,")]),
    _vm._v(" "),
    _c("p", [_vm._v("Nigeria.")])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h1", [_c("b", [_vm._v("BIZGURUH PRIVACY POLICY")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("OVERVIEW")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ol", { staticClass: "ols" }, [
      _c("li", [_c("b", [_vm._v("What kinds of information do we collect?")])]),
      _vm._v(" "),
      _c("li", [_c("b", [_vm._v("How do we use this information? ")])]),
      _vm._v(" "),
      _c("li", [_c("b", [_vm._v("How is this information shared?")])]),
      _vm._v(" "),
      _c("li", [_c("b", [_vm._v("How do we protect your personal data?")])]),
      _vm._v(" "),
      _c("li", [_c("b", [_vm._v("What are your rights?")])]),
      _vm._v(" "),
      _c("li", [
        _c("b", [_vm._v("How can I manage or delete information about me?")])
      ]),
      _vm._v(" "),
      _c("li", [_c("b", [_vm._v("Law And Protection")])]),
      _vm._v(" "),
      _c("li", [
        _c("b", [_vm._v("How will we notify you of changes to this Policy?")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("What kinds of information do we collect?")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("Things that you and others do and provide:")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "things" }, [
      _c("li", [
        _c("b", [_vm._v("Information and content you provide.")]),
        _vm._v(
          "\n                We collect the content, communications and other information you provide when you use our Services, including when you sign up for an account, create or share content and message or communicate with others."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("b", [_vm._v("Networks and connections.")]),
        _vm._v(
          "\n                We collect information about the people, pages accounts, hashtags and groups that you are connected to and how you interact with them across our Products, such as people you communicate with the most or groups that you are part of."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("b", [_vm._v("Your usage.")]),
        _vm._v(
          "\n               We collect information about how you use our Services, such as the types of content that you view or engage with, the features you use, the actions you take, the people or accounts you interact with and the time, frequency and duration of your activities. For example, we log when you're using and have last used our Products, and what posts, videos and other content you view on our Products."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _c("b", [
          _vm._v("Information about transactions made on our Products. ")
        ]),
        _vm._v(
          "\n            If you use our business education resources or harness the business development Chatbot or engage in the interactive forum provided on our platform, we collect information about the transaction."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("How do we use this information?")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Product research and development:")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Communicate with you.")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("Research and innovate for social good.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("How is this information shared?")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [
        _vm._v("People and accounts that you share and communicate with:")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [
        _vm._v(
          "Information about your active status or presence on our Platform:"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [
        _vm._v(
          "Apps, websites and third-party integrations on or using our Services."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Sharing with third-party partners")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("Partners who use our analytics services.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Advertisers.")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Device and Connection Information.")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Researchers and academics.")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Law enforcement or legal requests.")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("How do we protect your personal data?")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("What are your rights?")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "disc" }, [
      _c("li", [
        _vm._v(
          "The right to be informed. You have the right to be provided with clear, transparent and easily understandable information about how we use your personal data and your rights. Therefore, we’re providing you with the information in this Notice."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "The right to access and rectification. You have the right to access, correct or update your personal data at any time. We understand the importance of this and should you want to exercise your rights, please contact us."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "The right to data portability. The personal data you have provided us with is portable. This means it can be moved, copied or transmitted electronically under certain circumstances."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " The right to be forgotten. Under certain circumstances, you have right to request that we delete your data. If you wish to delete the personal data we hold about you, please let us know and we will take reasonable steps to respond to your request in accordance with legal requirements. If the personal data we collect is no longer needed for any purposes and we are not required by law to retain it, we will do what we can to delete, destroy or permanently de-identify it."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "The right to object. Under certain circumstances, you have the right to object to certain types of processing, including processing for direct marketing (i.e., receiving emails from us notifying you or being contacted with varying potential opportunities)."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "The right to lodge a complaint with a Supervisory Authority. You have the right to lodge a complaint directly with any local Supervisory Authority about how we process your personal data."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "The right to withdraw consent. If you have given your consent to anything we do with your personal data (i.e., we rely on consent as a legal basis for processing your personal data), you have the right to withdraw your consent at any time (although if you do so, it does not mean that anything we have done with your personal data with your consent up to that point is unlawful). You can withdraw your consent to the processing of your personal data at any time by contacting us with the details provided below."
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Rights related to automated decision-making. You have the right not to be subject to a decision which is based solely on automated processing and which produces legal or other significant effects on you. In particular, you have the right:"
        )
      ]),
      _vm._v(" "),
      _c("ul", [
        _c("li", [_vm._v("to obtain human intervention;")]),
        _vm._v(" "),
        _c("li", [_vm._v("to express your point of view;")]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "to obtain an explanation of the decision reached after an assessment; and"
          )
        ]),
        _vm._v(" "),
        _c("li", [_vm._v("to challenge such a decision.")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("How can I manage or delete information about me?")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_c("b", [_vm._v("Law And Protection")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [
      _c("b", [_vm._v("How will we notify you of changes to this Policy?")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a31b9652", module.exports)
  }
}

/***/ }),

/***/ 562:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1432)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1434)
/* template */
var __vue_template__ = __webpack_require__(1435)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-a31b9652"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/privacyComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a31b9652", Component.options)
  } else {
    hotAPI.reload("data-v-a31b9652", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});