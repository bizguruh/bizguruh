webpackJsonp([135],{

/***/ 1668:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1669);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("615afe82", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58de4716\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./indexFooter.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-58de4716\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./indexFooter.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1669:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-58de4716] {\n  padding: 0px !important;\n}\n.col-xs-6[data-v-58de4716] {\n  width: 50%;\n}\nh2[data-v-58de4716] {\n  font-size: 17px !important;\n}\n.site-footer[data-v-58de4716] {\n  background: #fff;\n  font-size: 16px;\n}\n.footer-widgets h2[data-v-58de4716] {\n  color: #f7f8fa !important;\n}\n.footer_body[data-v-58de4716] {\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n.foot[data-v-58de4716] {\n  padding: 5px;\n  font-size: 12px;\n  line-height: 1.2;\n}\n.reserve[data-v-58de4716] {\n  text-align: center;\n  font-size: 12px;\n  padding: 12px 15px;\n  margin: 0;\n}\n.col-6[data-v-58de4716] {\n  background-color: #fff;\n}\n.socialM[data-v-58de4716] {\n  padding: 10px;\n}\n.social_icons[data-v-58de4716] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: space-evenly;\n      -ms-flex-pack: space-evenly;\n          justify-content: space-evenly;\n  width: 10%;\n  margin: 0 auto;\n  list-style: none;\n}\nul[data-v-58de4716],\nol[data-v-58de4716] {\n  list-style: none;\n}\nul li[data-v-58de4716] {\n  color: rgba(0, 142, 142, 0.84);\n}\na[data-v-58de4716] {\n  color: rgba(0, 0, 0, 0.74) !important;\n}\n.rules[data-v-58de4716] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  font-size: 12px;\n  padding: 12px 15px;\n}\n.copyright[data-v-58de4716] {\n  color: rgba(0, 0, 0, 0.74);\n  font-size: 12px;\n}\n.footer_policy[data-v-58de4716] {\n  -webkit-box-pack: justify;\n      -ms-flex-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-top: 1px solid #ccc;\n  padding: 0 20px;\n  font-size: 12px;\n  margin: 0 auto;\n  width: 82%;\n}\n.guidelines[data-v-58de4716]::after {\n  content: \" |\";\n    color: rgba(0, 0, 0, 0.74);\n  height: 10px;\n}\n.privacy[data-v-58de4716],\n.guidelines[data-v-58de4716],\n.copyright[data-v-58de4716] {\n  padding: 0 5px;\n}\n.terms[data-v-58de4716]::after {\n  content: \" |\";\n   color: rgba(0, 0, 0, 0.74);\n  height: 10px;\n}\n@media (max-width: 1024px) {\n.footer_policy[data-v-58de4716] {\n    width: 100%;\n}\n}\n@media (max-width: 768px) {\n.footer_policy[data-v-58de4716] {\n    padding: 10px;\n}\nh2[data-v-58de4716] {\n    font-size: 16px !important;\n}\n.rules[data-v-58de4716] {\n    font-size: 14px;\n}\n.footer_policy[data-v-58de4716] {\n    width: 100%;\n}\n}\n@media (max-width: 575px) {\n.footer_body[data-v-58de4716] {\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.foot[data-v-58de4716] {\n    padding-left: 80px;\n}\n.rules[data-v-58de4716] {\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n.footer_policy[data-v-58de4716] {\n    text-align: center;\n    width: 100%;\n}\n}\n@media (max-width: 425px) {\n.footer_policy[data-v-58de4716] {\n    padding: 0;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    width: 100%;\n}\n.reserve[data-v-58de4716] {\n    padding: 0;\n}\nh2[data-v-58de4716] {\n    font-size: 14px !important;\n}\n.site-footer[data-v-58de4716] {\n    font-size: 12px;\n}\nli[data-v-58de4716] {\n    margin-bottom: 6px !important;\n}\n.foot[data-v-58de4716] {\n    font-size: 12px;\n    line-height: 1;\n    padding-left: 40px;\n}\n.mt-5[data-v-58de4716] {\n    margin-top: 0px !important;\n}\n.footer_body[data-v-58de4716] {\n    padding: 5px 20px;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.footer_policy[data-v-58de4716] {\n    padding: 5px 0px;\n}\n.reserve[data-v-58de4716] {\n    text-align: center;\n    font-size: 12px;\n}\n.copyright[data-v-58de4716] {\n    padding: 0;\n}\n.rules[data-v-58de4716] {\n    padding: 0;\n    font-size: 12px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n@media (max-width: 375px) {\n.footer_policy[data-v-58de4716] {\n    padding: 0;\n    width: 100%;\n}\n.reserve[data-v-58de4716] {\n    padding: 0;\n}\nh2[data-v-58de4716] {\n    font-size: 14px !important;\n}\n.site-footer[data-v-58de4716] {\n    font-size: 9px;\n}\nli[data-v-58de4716] {\n    margin-bottom: 6px !important;\n}\n.foot[data-v-58de4716] {\n    font-size: 9px;\n    line-height: 1;\n    padding-left: 40px;\n}\n.mt-5[data-v-58de4716] {\n    margin-top: 0px !important;\n}\n.footer_body[data-v-58de4716] {\n    padding: 5px 10px;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n}\n.footer_policy[data-v-58de4716] {\n    padding: 5px 0px;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.reserve[data-v-58de4716] {\n    text-align: center;\n    font-size: 9px;\n}\n.copyright[data-v-58de4716] {\n    padding: 0;\n}\n.rules[data-v-58de4716] {\n    padding: 0;\n    font-size: 9px;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1670:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "index-footer-component",

  data: function data() {
    return {
      year: 0,
      show: true
    };
  },
  mounted: function mounted() {
    this.showFoot();
  },

  computed: {
    getYear: function getYear() {
      var date = new Date();
      return this.year = date.getFullYear();
    }
  },
  watch: {
    $route: "showFoot"
  },
  methods: {
    showFoot: function showFoot() {
      if (this.$route.path === "/auth/register" || this.$route.path === "/vendor/auth" || this.$route.path === "/auth/login" || this.$route.path === "/account-demo" || this.$route.path === "/account" || this.$route.path === "/chat" || this.$route.name === "PrivateChat" || this.$route.name === "GroupChat") {
        this.show = false;
      }
    }
  }
});

/***/ }),

/***/ 1671:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.show
    ? _c("footer", { staticClass: "site-footer" }, [
        _c("div", { staticClass: "row footer_policy" }, [
          _c("div", { staticClass: "copyright" }, [
            _c("p", { staticClass: "reserve" }, [
              _vm._v(
                "© " + _vm._s(_vm.getYear) + " Bizguruh . All rights reserved"
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "rules" }, [
            _c(
              "div",
              { staticClass: "guidelines" },
              [
                _c("router-link", { attrs: { to: "/guidelines" } }, [
                  _vm._v("Bizguruh Guidelines")
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "terms" },
              [
                _c("router-link", { attrs: { to: "/terms" } }, [
                  _vm._v("Terms and Conditions")
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "privacy" },
              [
                _c("router-link", { attrs: { to: "/policies" } }, [
                  _vm._v("Privacy Policy")
                ])
              ],
              1
            )
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex justify-around align-center" }, [
      _c(
        "a",
        {
          attrs: { href: "https://www.facebook.com/BizGuruh-314446332731552/" }
        },
        [
          _c("i", {
            staticClass: "fab fa-facebook fa-1x px-2",
            attrs: { "aria-hidden": "true" }
          })
        ]
      ),
      _vm._v(" "),
      _c("a", { attrs: { href: "https://twitter.com/BizGuruh" } }, [
        _c("i", {
          staticClass: "fab fa-twitter fa-1x px-2",
          attrs: { "aria-hidden": "true" }
        })
      ]),
      _vm._v(" "),
      _c("a", { attrs: { href: "https://www.instagram.com/bizguruh/" } }, [
        _c("i", {
          staticClass: "fab fa-instagram fa-1x px-2",
          attrs: { "aria-hidden": "true" }
        })
      ]),
      _vm._v(" "),
      _c("a", { attrs: { href: "EMAIL: ask@bizguruh.com" } }, [
        _c("i", { staticClass: "fas fa-envelope-open-text fa-1x px-2" })
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-58de4716", module.exports)
  }
}

/***/ }),

/***/ 615:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1668)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1670)
/* template */
var __vue_template__ = __webpack_require__(1671)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-58de4716"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/indexFooter.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-58de4716", Component.options)
  } else {
    hotAPI.reload("data-v-58de4716", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});