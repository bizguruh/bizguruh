webpackJsonp([40],{

/***/ 1388:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1389);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("45d8f0a8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dc96749c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dc96749c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1389:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userAllProductHeaderComponent__ = __webpack_require__(1391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_userAllProductHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_userAllProductHeaderComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userAllProductContentComponent__ = __webpack_require__(1396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_userAllProductContentComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_userAllProductContentComponent__);
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-all-product-component",
    components: {
        'app-header': __WEBPACK_IMPORTED_MODULE_0__components_userAllProductHeaderComponent___default.a,
        'app-content': __WEBPACK_IMPORTED_MODULE_1__components_userAllProductContentComponent___default.a
    }
});

/***/ }),

/***/ 1391:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1392)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1394)
/* template */
var __vue_template__ = __webpack_require__(1395)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-76b86040"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userAllProductHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-76b86040", Component.options)
  } else {
    hotAPI.reload("data-v-76b86040", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1392:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1393);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("744833d8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-76b86040\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-76b86040\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1393:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-76b86040] {\n    padding: 0 !important;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-all-product-header-component"
});

/***/ }),

/***/ 1395:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "courses-page" }, [
      _c("div", { staticClass: "page-header" }, [
        _c("div", { staticClass: "page-header-overlay" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("header", { staticClass: "entry-header" }, [
                  _c("h1", [_vm._v("Courses Onlinjjjje")])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-76b86040", module.exports)
  }
}

/***/ }),

/***/ 1396:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1397)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1399)
/* template */
var __vue_template__ = __webpack_require__(1400)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5bebab64"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userAllProductContentComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5bebab64", Component.options)
  } else {
    hotAPI.reload("data-v-5bebab64", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 1397:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1398);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3e4ad78e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5bebab64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductContentComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5bebab64\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userAllProductContentComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1398:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-5bebab64] {\n    padding: 0 !important;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-all-product-content-component",
    data: function data() {
        return {
            allProducts: [],
            productCategories: [],
            allVendors: [],
            featuredProducts: [],
            subCategories: []
        };
    },
    mounted: function mounted() {
        var _this = this;

        axios.get('/api/products').then(function (response) {
            if (response.status === 200) {
                _this.allProducts = response.data.data;
                _this.getAllCategory();
                _this.getAllVendor();
                _this.getSponsorProduct();
                _this.getPopularCategory();
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        getAllCategory: function getAllCategory() {
            var _this2 = this;

            axios.get('/api/product-category').then(function (response) {
                if (response.status === 200) {
                    _this2.productCategories = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getAllVendor: function getAllVendor() {
            var _this3 = this;

            axios.get('/api/get-all-vendor').then(function (response) {
                if (response.status === 200) {
                    _this3.allVendors = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getSponsorProduct: function getSponsorProduct() {
            var _this4 = this;

            axios.get('/api/sponsor-products').then(function (response) {
                if (response.status === 200) {
                    console.log(response);
                    _this4.featuredProducts = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        getPopularCategory: function getPopularCategory() {
            var _this5 = this;

            axios.get('/api/get-sub-categories').then(function (response) {
                if (response.status === 200) {
                    _this5.subCategories = response.data.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),

/***/ 1400:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-lg-8" }, [
        _c("div", { staticClass: "featured-courses courses-wrap" }, [
          _c(
            "div",
            { staticClass: "row mx-m-25" },
            _vm._l(_vm.allProducts, function(allProduct) {
              return _c("div", { staticClass: "col-12 col-md-6 px-25" }, [
                _c("div", { staticClass: "course-content" }, [
                  _c("figure", { staticClass: "course-thumbnail" }, [
                    _c("a", { attrs: { href: "#" } }, [
                      _c("img", {
                        attrs: {
                          src: allProduct.productImage[0].image,
                          alt: ""
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "course-content-wrap" }, [
                    _c("header", { staticClass: "entry-header" }, [
                      _c("h2", { staticClass: "entry-title" }, [
                        _c("a", { attrs: { href: "#" } }, [
                          _vm._v(_vm._s(allProduct.title))
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "entry-meta flex flex-wrap align-items-center"
                        },
                        [
                          _c("div", { staticClass: "course-author" }, [
                            _c("a", { attrs: { href: "#" } }, [
                              _vm._v(_vm._s(allProduct.category) + " ")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "course-date" }, [
                            _vm._v("July 21, 2018")
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "footer",
                      {
                        staticClass:
                          "entry-footer flex flex-wrap justify-content-between align-items-center"
                      },
                      [
                        _c("div", { staticClass: "course-cost" }, [
                          _vm._v(
                            "\n                                        ₦" +
                              _vm._s(allProduct.productDetail[0].salePrice) +
                              " "
                          ),
                          _c("span", { staticClass: "price-drop" }, [
                            _vm._v(
                              "₦" + _vm._s(allProduct.productDetail[0].price)
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", [_vm._v("ffff")])
                        ]),
                        _vm._v(" "),
                        _vm._m(1, true)
                      ]
                    )
                  ])
                ])
              ])
            }),
            0
          )
        ]),
        _vm._v(" "),
        _vm._m(2)
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-lg-4" }, [
        _c("div", { staticClass: "sidebar" }, [
          _vm._m(3),
          _vm._v(" "),
          _c("div", { staticClass: "cat-links" }, [
            _c("h2", [_vm._v("Categories")]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "p-0 m-0" },
              _vm._l(_vm.productCategories, function(productCategory) {
                return _c("li", [
                  _c("a", { attrs: { href: "#" } }, [
                    _vm._v(_vm._s(productCategory.name))
                  ])
                ])
              }),
              0
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "latest-courses" }, [
            _c("h2", [_vm._v("Latest Courses")]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "p-0 m-0" },
              _vm._l(_vm.featuredProducts, function(featuredProduct) {
                return _c(
                  "li",
                  {
                    staticClass:
                      "flex flex-wrap justify-content-between align-items-center"
                  },
                  [
                    _c("img", {
                      attrs: {
                        src: featuredProduct.productImage[0].image,
                        alt: ""
                      }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "content-wrap" }, [
                      _c("h3", [
                        _c("a", { attrs: { href: "#" } }, [
                          _vm._v(_vm._s(featuredProduct.title))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "course-cost free-cost" }, [
                        _vm._v(
                          "₦" +
                            _vm._s(featuredProduct.productDetail[0].salePrice)
                        )
                      ])
                    ])
                  ]
                )
              }),
              0
            )
          ]),
          _vm._v(" "),
          _vm._m(4),
          _vm._v(" "),
          _c("div", { staticClass: "popular-tags" }, [
            _c("h2", [_vm._v("Popular Tags")]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "flex flex-wrap align-items-center p-0 m-0" },
              _vm._l(_vm.subCategories, function(subCategory) {
                return _c("li", [
                  _c("a", { attrs: { href: "#" } }, [
                    _vm._v(_vm._s(subCategory.name))
                  ])
                ])
              }),
              0
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12" }, [
        _c("div", { staticClass: "breadcrumbs" }, [
          _c(
            "ul",
            { staticClass: "flex flex-wrap align-items-center p-0 m-0" },
            [
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _c("i", { staticClass: "fa fa-home" }),
                  _vm._v(" Home")
                ])
              ]),
              _vm._v(" "),
              _c("li", [_vm._v("Courses")])
            ]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "course-ratings flex justify-content-end align-items-center"
      },
      [
        _c("span", { staticClass: "fa fa-star checked" }),
        _vm._v(" "),
        _c("span", { staticClass: "fa fa-star checked" }),
        _vm._v(" "),
        _c("span", { staticClass: "fa fa-star checked" }),
        _vm._v(" "),
        _c("span", { staticClass: "fa fa-star checked" }),
        _vm._v(" "),
        _c("span", { staticClass: "fa fa-star-o" }),
        _vm._v(" "),
        _c("span", { staticClass: "course-ratings-count" }, [
          _vm._v("(4 votes)")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "pagination flex flex-wrap justify-content-between align-items-center"
      },
      [
        _c(
          "div",
          { staticClass: "col-12 col-lg-4 order-2 order-lg-1 mt-3 mt-lg-0" },
          [
            _c(
              "ul",
              {
                staticClass:
                  "flex flex-wrap align-items-center order-2 order-lg-1 p-0 m-0"
              },
              [
                _c("li", { staticClass: "active" }, [
                  _c("a", { attrs: { href: "#" } }, [_vm._v("1")])
                ]),
                _vm._v(" "),
                _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("2")])]),
                _vm._v(" "),
                _c("li", [_c("a", { attrs: { href: "#" } }, [_vm._v("3")])]),
                _vm._v(" "),
                _c("li", [
                  _c("a", { attrs: { href: "#" } }, [
                    _c("i", { staticClass: "fa fa-angle-right" })
                  ])
                ])
              ]
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "col-12 flex justify-content-start justify-content-lg-end col-lg-8 order-1 order-lg-2"
          },
          [
            _c(
              "div",
              {
                staticClass:
                  "pagination-results flex flex-wrap align-items-center"
              },
              [
                _c("p", { staticClass: "m-0" }, [
                  _vm._v("Showing 1–3 of 12 results")
                ]),
                _vm._v(" "),
                _c("form", [
                  _c("select", [
                    _c("option", [_vm._v("Show: 06")]),
                    _vm._v(" "),
                    _c("option", [_vm._v("Show: 12")]),
                    _vm._v(" "),
                    _c("option", [_vm._v("Show: 18")]),
                    _vm._v(" "),
                    _c("option", [_vm._v("Show: 24")])
                  ])
                ])
              ]
            )
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-widget" }, [
      _c("form", { staticClass: "flex flex-wrap align-items-center" }, [
        _c("input", { attrs: { type: "search", placeholder: "Search..." } }),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "flex justify-content-center align-items-center",
            attrs: { type: "submit" }
          },
          [_c("i", { staticClass: "fa fa-search" })]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "ads" }, [
      _c("img", { attrs: { src: "images/ads.jpg", alt: "" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5bebab64", module.exports)
  }
}

/***/ }),

/***/ 1401:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [_c("app-header"), _vm._v(" "), _c("app-content")], 1)
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-dc96749c", module.exports)
  }
}

/***/ }),

/***/ 552:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1388)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1390)
/* template */
var __vue_template__ = __webpack_require__(1401)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-dc96749c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userAllProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dc96749c", Component.options)
  } else {
    hotAPI.reload("data-v-dc96749c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});