webpackJsonp([83],{

/***/ 1638:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1639);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4828560d", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cb531bf6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subjectMattersComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cb531bf6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subjectMattersComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1639:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main[data-v-cb531bf6] {\n  padding:150px 15px;\n}\na[data-v-cb531bf6] {\n  height: 100%;\n}\n.container[data-v-cb531bf6] {\n  -webkit-box-shadow: 0 0 3px 2px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 0 3px 2px rgba(0, 0, 0, 0.2);\n  border-radius: 4px;\n  padding-top: 40px;\n  min-height: 100vh;\n  background:white;\n}\n.noContent[data-v-cb531bf6]{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: 100vh;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  font-weight: bold;\n  font-size: 18px;\n}\nimg[data-v-cb531bf6] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.bread[data-v-cb531bf6]{\n\tmargin-bottom:40px;\n}\n.header[data-v-cb531bf6] {\n  font-size: 16px;\n  font-weight: normal;\n}\n.sideHeader[data-v-cb531bf6] {\n  font-size: 16px;\n  font-weight: bold;\n}\nul[data-v-cb531bf6]{\n  padding:10px 0;\n}\n.sub[data-v-cb531bf6]{\n\tmargin: 20px 0;\n}\n.videos[data-v-cb531bf6] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n}\n.vidContainer[data-v-cb531bf6] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 30%;\n  height: 350px;\n  margin: 10px;\n  margin-bottom: 40px;\n  background: white;\n  -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);\n  border-radius: 3px;\n}\n.vidImage[data-v-cb531bf6] {\n  width: 100%;\n  height: 50%;\n  position: relative;\n  padding: 10px;\n  font-size: 14px;\n  background:#f7f7f7;\n}\n.seperate[data-v-cb531bf6]{\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n.sideNav[data-v-cb531bf6]{\n  padding: 10px;\n  background: white;\n  border-radius: 4px;\n}\n.navy[data-v-cb531bf6]{\n  display:-webkit-box;\n  display:-ms-flexbox;\n  display:flex;\n}\n.navy[data-v-cb531bf6]:hover{\n color: #88a2b8;\n}\n.vidAbout[data-v-cb531bf6] {\n  position: relative;\n  width: 100%;\n  height: 50%;\n}\n.articles[data-v-cb531bf6] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n}\n.articleContainer[data-v-cb531bf6] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 45%;\n  height: 120px;\n  margin: 10px;\n  background: white;\n\t-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);\n\t        box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.2);\n\tborder-radius: 3px;\n}\n.articleDesc[data-v-cb531bf6] {\n  width: 80%;\n  position: relative;\n}\n.articleImage[data-v-cb531bf6] {\n  width: 20%;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  font-size: 14px;\n  padding: 5px;\n}\n.articleImage img[data-v-cb531bf6] {\n  width: 80px;\n  height: 80px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.articleD[data-v-cb531bf6] {\n  text-transform: capitalize;\n  font-size: 16px;\n  color: rgba(0, 0, 0, 0.54);\n  line-height: 1.2;\n  padding: 10px;\n  max-height: 50px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.checkers[data-v-cb531bf6]{\n  position:relative;\n  margin: auto 10px;\n}\n.title[data-v-cb531bf6] {\n  text-transform: capitalize;\n  font-size: 18px ;\n  color: rgba(0, 0, 0, 0.84);\n  line-height: 1.2;\n  padding: 10px;\n  max-height: 55px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.industry[data-v-cb531bf6] {\n  text-transform: capitalize;\n  font-size: 16px;\n  color: rgba(0, 0, 0, 0.4);\n  line-height: 1.2;\n  padding: 5px 10px;\n}\n.vendor[data-v-cb531bf6] {\n  text-transform: capitalize;\n  font-size: 16px;\n  color: rgba(0, 0, 0, 0.64);\n  line-height: 1.2;\n  padding: 5px 10px;\n}\n.mobilee[data-v-cb531bf6]{\n  display:none;\n}\n@media(max-width:425px){\n.desktop[data-v-cb531bf6]{\n    display: none;\n}\n.mobilee[data-v-cb531bf6]{\n    display:block;\n}\n.main[data-v-cb531bf6]{\n    padding:57px 0;\n}\n.slide[data-v-cb531bf6]{\n    position: -webkit-sticky;\n    position: sticky;\n    top: 0;\n    background: #ccc;\n    padding: 5px 20px;\n    text-align: right;\n    z-index: 5;\n}\n.fa-sliders-h[data-v-cb531bf6]{\n    font-size:16px;\n    color:rgba(0, 0, 0, 0.54);\n}\n.seperate[data-v-cb531bf6]{\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.vidContainer[data-v-cb531bf6]{\n    width: 100%;\n}\n.articleContainer[data-v-cb531bf6]{\n    width: 100%;\n     height:auto;\n     padding:10px;\n}\n.articleD[data-v-cb531bf6]{\n    font-size: 14px;\n    padding: 10px 10px 0;\n     line-height: 1.4;\n     max-height: 48px;\n}\n.title[data-v-cb531bf6]{\n    font-size: 16px;\n    padding: 10px 10px 0;\n}\n.mobilee.sideNav[data-v-cb531bf6]{\n    display:-webkit-box;\n    display:-ms-flexbox;\n    display:flex;\n    width:100%;\n    position: absolute;\n    padding:15px 20px;\n    z-index: 2;\n    top: 35px;\n    height: auto;\n    left:0;\n    -webkit-box-shadow:1px 0 1px 1px #ccc;\n            box-shadow:1px 0 1px 1px #ccc;\n    border-top-left-radius: unset;\n    border-top-right-radius: unset;\n    text-align: left;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1640:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _ref;

    return _ref = {
      id: this.$route.params.id,
      name: this.$route.params.name,
      videoProduct: [],
      researchProduct: [],
      podcastProduct: [],
      articleProduct: [],
      courseProduct: [],
      allProduct: [],
      allVendors: [],
      isActive: true,
      subject_id: this.$route.params.id,
      userSub: null,
      freeTrial: null
    }, _defineProperty(_ref, 'userSub', null), _defineProperty(_ref, 'user', {}), _defineProperty(_ref, 'inLibrary', null), _defineProperty(_ref, 'showUpgrade', false), _defineProperty(_ref, 'allSubjectMatters', []), _defineProperty(_ref, 'subject', ''), _defineProperty(_ref, 'newname', this.$route.params.name), _defineProperty(_ref, 'sideNav', false), _ref;
  },
  created: function created() {
    this.isActive = true;
    this.updateList();
    this.getAllProducts();
    this.checkStatus();
    this.getSubjectMatters();
  },


  watch: {
    $route: function $route() {
      this.updateList();
    },

    'subject_id': 'changeSubject',
    'newname': 'changeName'
  },
  methods: {
    changeSubject: function changeSubject() {

      this.id = this.subject_id;
      this.isActive = false;
    },
    changeName: function changeName() {
      this.name = this.newname;
    },
    getSubjectMatters: function getSubjectMatters() {
      var _this = this;

      axios.get("/api/subjectmatter").then(function (response) {
        _this.allSubjectMatters = response.data.data;
        response.data.data.forEach(function (item) {
          _this.isActive = false;
          if (Number(item.id) === Number(_this.id)) {
            _this.subject = item.name.toLowerCase();
          }
        });
      });
    },
    checkStatus: function checkStatus() {
      var _this2 = this;

      var user = JSON.parse(localStorage.getItem("authUser"));
      this.user = user;
      if (user != null) {
        axios.get("/api/user", { headers: Object(__WEBPACK_IMPORTED_MODULE_0__config__["b" /* getCustomerHeader */])() }).then(function (response) {
          var userDate = response.data.created_at;
          _this2.startDate = new Date(userDate);

          _this2.startDateParsed = Date.parse(new Date(userDate));
          _this2.currentDate = Date.parse(new Date());

          _this2.endDate = Date.parse(new Date(_this2.startDate.setDate(_this2.startDate.getDate(userDate) + 30)));

          if (_this2.currentDate >= _this2.endDate) {
            _this2.freeTrial = false;
          } else {}
        });

        this.token = user.access_token;
        axios.get('/api/user/orders', {
          headers: { Authorization: 'Bearer ' + user.access_token }
        }).then(function (response) {
          response.data.data.forEach(function (element) {
            element.orderDetail.forEach(function (item) {
              if (item.type === "VIDEOS") {
                _this2.checkLibrary.push(item);
              }
            });
          });
        });
        axios.post('/api/user/subscription-plan/' + user.id).then(function (response) {
          _this2.userSub = Number(response.data[0].level);
        });
      }
    },
    loading: function loading() {
      this.isActive = true;
    },
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    },
    getAllProducts: function getAllProducts() {
      var _this3 = this;

      axios.get('/api/products').then(function (response) {
        if (response.status === 200) {
          _this3.allProduct = response.data.data.reverse();
          response.data.data.forEach(function (item) {
            if (item.prodType == "Market Research") {
              item.marketResearch.coverImage = item.coverImage;
              item.marketResearch.subLevel = item.subscriptionLevel;
              item.marketResearch.subjectMatter = item.subjectMatter.id;
              item.marketResearch.industry = item.industry.name;
              item.marketResearch.vendor = item.vendor.storeName;

              _this3.researchProduct.push(item.marketResearch);
            } else if (item.prodType == "Videos") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              item.webinar.subjectMatter = item.subjectMatter.id;
              item.webinar.industry = item.industry.name;
              item.webinar.vendor = item.vendor.storeName;
              _this3.videoProduct.push(item.webinar);
            } else if (item.prodType == "Podcast") {
              item.webinar.coverImage = item.coverImage;
              item.webinar.subLevel = item.subscriptionLevel;
              item.webinar.subjectMatter = item.subjectMatter.id;
              item.webinar.industry = item.industry;
              item.webinar.vendor = item.vendor.storeName;
              _this3.podcastProduct.push(item.webinar);
            } else if (item.prodType == "Articles") {
              item.articles.coverImage = item.coverImage;
              item.articles.subLevel = item.subscriptionLevel;
              item.articles.subjectMatter = item.subjectMatter.id;
              item.articles.industry = item.industry.name;
              item.articles.vendor = item.vendor.storeName;
              _this3.articleProduct.push(item.articles);
            } else if (item.prodType == "Courses") {
              item.courses.coverImage = item.coverImage;
              item.courses.subLevel = item.subscriptionLevel;
              item.courses.subjectMatter = item.subjectMatter.id;
              item.courses.industry = item.industry.name;
              item.courses.vendor = item.vendor.storeName;

              _this3.courseProduct.push(item.courses);
            } else {
              return;
            }
          });
        }
      });
    },
    closeUpgrade: function closeUpgrade() {
      this.showUpgrade = false;
    },
    redirectUpgrade: function redirectUpgrade(level) {
      this.showUpgrade = false;
      var routeData = this.$router.resolve({
        name: "SubscriptionProfile",
        params: {
          level: this.videoProduct[0].subLevel
        }

      });
    },
    openSide: function openSide() {
      this.sideNav = !this.sideNav;
    }
  },
  computed: {
    video: function video() {
      var _this4 = this;

      return this.videoProduct.filter(function (item) {
        if (Number(item.subjectMatter) === Number(_this4.id)) {
          return item;
        } else if (_this4.id === '') {
          return item;
        }
      });
    },
    article: function article() {
      var _this5 = this;

      return this.articleProduct.filter(function (item) {
        if (Number(item.subjectMatter) === Number(_this5.id)) {
          return item;
        } else if (_this5.id === '') {
          return item;
        }
      });
    },
    course: function course() {
      var _this6 = this;

      return this.courseProduct.filter(function (item) {
        if (Number(item.subjectMatter) === Number(_this6.id)) {
          return item;
        } else if (_this6.id === '') {
          return item;
        }
      });
    },
    research: function research() {
      var _this7 = this;

      return this.researchProduct.filter(function (item) {
        if (Number(item.subjectMatter) === Number(_this7.id)) {
          return item;
        } else if (_this7.id === '') {
          return item;
        }
      });
    }
  },
  mounted: function mounted() {}
});

/***/ }),

/***/ 1641:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _vm._e(),
    _vm._v(" "),
    _vm.showUpgrade
      ? _c("div", { staticClass: "upgradeContainer animated fadeIn" }, [
          _c("div", { staticClass: "upgrade" }, [
            _c("p", { staticClass: "upgradeText" }, [
              _vm._v(
                "Oops, looks like you need to upgrade to access this. It’ll only take a sec though. 😁"
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "upgradeButtons" }, [
              _c(
                "div",
                {
                  staticClass: "upgradeButton",
                  on: { click: _vm.redirectUpgrade }
                },
                [_vm._v("Yes please!")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "upgradeButton",
                  on: { click: _vm.closeUpgrade }
                },
                [_vm._v("I'll keep exploring")]
              )
            ])
          ])
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "mobilee slide" }, [
      _c("div", [
        _c(
          "i",
          { staticClass: "fas fa-sliders-h", on: { click: _vm.openSide } },
          [_vm._v(" Filter")]
        )
      ]),
      _vm._v(" "),
      _vm.allSubjectMatters.length > 0 && _vm.sideNav
        ? _c("div", { staticClass: "sideNav mobilee" }, [
            _c("div", [
              _c("h4", [_vm._v("Knowledge Area")]),
              _vm._v(" "),
              _c(
                "ul",
                [
                  _c("li", { staticClass: "navy" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.subject_id,
                          expression: "subject_id"
                        }
                      ],
                      staticClass: "form-check-input checkers",
                      attrs: { type: "radio", name: "", id: "all", value: "" },
                      domProps: { checked: _vm._q(_vm.subject_id, "") },
                      on: {
                        change: function($event) {
                          _vm.subject_id = ""
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "toCaps mainFontColor",
                        attrs: { for: "all" }
                      },
                      [_vm._v("All")]
                    )
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.allSubjectMatters, function(subject, index) {
                    return _c("li", { key: index, staticClass: "navy" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.subject_id,
                            expression: "subject_id"
                          }
                        ],
                        staticClass: "form-check-input checkers",
                        attrs: { type: "radio", name: "", id: subject.name },
                        domProps: {
                          value: subject.id,
                          checked: _vm._q(_vm.subject_id, subject.id)
                        },
                        on: {
                          change: function($event) {
                            _vm.subject_id = subject.id
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "toCaps mainFontColor",
                          attrs: { for: subject.name },
                          on: { click: _vm.loading }
                        },
                        [
                          _vm._v(
                            "\n                      " +
                              _vm._s(subject.name.toLowerCase()) +
                              "\n                      "
                          )
                        ]
                      )
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("div", [
              _c("h4", [_vm._v("Contents")]),
              _vm._v(" "),
              _c("ul", [
                _c("li", { staticClass: "navy" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.newname,
                        expression: "newname"
                      }
                    ],
                    staticClass: "form-check-input checkers",
                    attrs: {
                      type: "radio",
                      name: "newname",
                      id: "video",
                      value: "video"
                    },
                    domProps: { checked: _vm._q(_vm.newname, "video") },
                    on: {
                      change: function($event) {
                        _vm.newname = "video"
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "toCaps mainFontColor",
                      attrs: { for: "video" }
                    },
                    [
                      _vm._v(
                        "\n                     Watch\n                      "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "navy" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.newname,
                        expression: "newname"
                      }
                    ],
                    staticClass: "form-check-input checkers",
                    attrs: {
                      type: "radio",
                      name: " newname",
                      id: "article",
                      value: "article"
                    },
                    domProps: { checked: _vm._q(_vm.newname, "article") },
                    on: {
                      change: function($event) {
                        _vm.newname = "article"
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "toCaps mainFontColor",
                      attrs: { for: "article" }
                    },
                    [
                      _vm._v(
                        "\n                     Read\n                      "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "navy" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.newname,
                        expression: "newname"
                      }
                    ],
                    staticClass: "form-check-input checkers",
                    attrs: {
                      type: "radio",
                      name: "newname",
                      id: "course",
                      value: "course"
                    },
                    domProps: { checked: _vm._q(_vm.newname, "course") },
                    on: {
                      change: function($event) {
                        _vm.newname = "course"
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "toCaps mainFontColor",
                      attrs: { for: "course" }
                    },
                    [
                      _vm._v(
                        "\n                     Study\n                      "
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "navy" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.newname,
                        expression: "newname"
                      }
                    ],
                    staticClass: "form-check-input checkers",
                    attrs: {
                      type: "radio",
                      name: "newname",
                      id: "research",
                      value: "research"
                    },
                    domProps: { checked: _vm._q(_vm.newname, "research") },
                    on: {
                      change: function($event) {
                        _vm.newname = "research"
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "toCaps mainFontColor",
                      attrs: { for: "research" }
                    },
                    [
                      _vm._v(
                        "\n                    Research\n                      "
                      )
                    ]
                  )
                ])
              ])
            ])
          ])
        : _vm._e()
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "seperate" }, [
      _vm.allSubjectMatters.length > 0
        ? _c("div", { staticClass: "sideNav desktop" }, [
            _c("h4", [_vm._v("Knowledge Area")]),
            _vm._v(" "),
            _c(
              "ul",
              [
                _c("li", { staticClass: "navy" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.subject_id,
                        expression: "subject_id"
                      }
                    ],
                    staticClass: "form-check-input checkers",
                    attrs: { type: "radio", name: "", id: "all", value: "" },
                    domProps: { checked: _vm._q(_vm.subject_id, "") },
                    on: {
                      change: function($event) {
                        _vm.subject_id = ""
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "label",
                    {
                      staticClass: "toCaps mainFontColor",
                      attrs: { for: "all" }
                    },
                    [_vm._v("All")]
                  )
                ]),
                _vm._v(" "),
                _vm._l(_vm.allSubjectMatters, function(subject, index) {
                  return _c("li", { key: index, staticClass: "navy" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.subject_id,
                          expression: "subject_id"
                        }
                      ],
                      staticClass: "form-check-input checkers",
                      attrs: { type: "radio", name: "", id: subject.name },
                      domProps: {
                        value: subject.id,
                        checked: _vm._q(_vm.subject_id, subject.id)
                      },
                      on: {
                        change: function($event) {
                          _vm.subject_id = subject.id
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "toCaps mainFontColor",
                        attrs: { for: subject.name },
                        on: { click: _vm.loading }
                      },
                      [
                        _vm._v(
                          "\n                      " +
                            _vm._s(subject.name.toLowerCase()) +
                            "\n                      "
                        )
                      ]
                    )
                  ])
                })
              ],
              2
            ),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("h4", [_vm._v("Contents")]),
            _vm._v(" "),
            _c("ul", [
              _c("li", { staticClass: "navy" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.newname,
                      expression: "newname"
                    }
                  ],
                  staticClass: "form-check-input checkers",
                  attrs: {
                    type: "radio",
                    name: "newname",
                    id: "video",
                    value: "video"
                  },
                  domProps: { checked: _vm._q(_vm.newname, "video") },
                  on: {
                    change: function($event) {
                      _vm.newname = "video"
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "label",
                  {
                    staticClass: "toCaps mainFontColor",
                    attrs: { for: "video" }
                  },
                  [
                    _vm._v(
                      "\n                     Watch\n                      "
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "navy" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.newname,
                      expression: "newname"
                    }
                  ],
                  staticClass: "form-check-input checkers",
                  attrs: {
                    type: "radio",
                    name: " newname",
                    id: "article",
                    value: "article"
                  },
                  domProps: { checked: _vm._q(_vm.newname, "article") },
                  on: {
                    change: function($event) {
                      _vm.newname = "article"
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "label",
                  {
                    staticClass: "toCaps mainFontColor",
                    attrs: { for: "article" }
                  },
                  [
                    _vm._v(
                      "\n                     Read\n                      "
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "navy" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.newname,
                      expression: "newname"
                    }
                  ],
                  staticClass: "form-check-input checkers",
                  attrs: {
                    type: "radio",
                    name: "newname",
                    id: "course",
                    value: "course"
                  },
                  domProps: { checked: _vm._q(_vm.newname, "course") },
                  on: {
                    change: function($event) {
                      _vm.newname = "course"
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "label",
                  {
                    staticClass: "toCaps mainFontColor",
                    attrs: { for: "course" }
                  },
                  [
                    _vm._v(
                      "\n                     Study\n                      "
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "navy" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.newname,
                      expression: "newname"
                    }
                  ],
                  staticClass: "form-check-input checkers",
                  attrs: {
                    type: "radio",
                    name: "newname",
                    id: "research",
                    value: "research"
                  },
                  domProps: { checked: _vm._q(_vm.newname, "research") },
                  on: {
                    change: function($event) {
                      _vm.newname = "research"
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "label",
                  {
                    staticClass: "toCaps mainFontColor",
                    attrs: { for: "research" }
                  },
                  [
                    _vm._v(
                      "\n                    Research\n                      "
                    )
                  ]
                )
              ])
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "bread" }, [
          _c(
            "ul",
            _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
              return _c(
                "li",
                {
                  key: index,
                  class: { linked: !!breadcrumb.link },
                  on: {
                    click: function($event) {
                      return _vm.routeTo(index)
                    }
                  }
                },
                [_vm._v(_vm._s(breadcrumb.name))]
              )
            }),
            0
          )
        ]),
        _vm._v(" "),
        _vm.name === "video"
          ? _c("section", { staticClass: "video" }, [
              _c("div", { staticClass: "sub" }, [
                _c("span", { staticClass: "header mainFontColor" }, [
                  _vm._v("Videos on")
                ]),
                _vm._v(" "),
                _vm.subject_id === ""
                  ? _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v("All Subjects")]
                    )
                  : _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v(_vm._s(_vm.subject))]
                    )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "videos" },
                _vm._l(_vm.video, function(item, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass: "vidContainer animated fadeIn slow delay-3s"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "Video",
                              params: { id: item.product_id }
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "vidImage" }, [
                            _c("img", {
                              attrs: { src: item.coverImage, alt: item.title }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "vidAbout" }, [
                            _c("p", { staticClass: "title" }, [
                              _vm._v(_vm._s(item.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v("video")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v(_vm._s(item.industry.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "vendor" }, [
                              _vm._v(_vm._s(item.vendor.toLowerCase()))
                            ])
                          ])
                        ]
                      )
                    ],
                    1
                  )
                }),
                0
              ),
              _vm._v(" "),
              _vm.video.length === 0
                ? _c("div", { staticClass: "noContent" }, [
                    _vm._v(" No content yet")
                  ])
                : _vm._e()
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.name === "article"
          ? _c("section", { staticClass: "article" }, [
              _c("div", { staticClass: "sub" }, [
                _c("span", { staticClass: "header mainFontColor" }, [
                  _vm._v("Articles on")
                ]),
                _vm._v(" "),
                _vm.subject_id === ""
                  ? _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v("All Subjects")]
                    )
                  : _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v(_vm._s(_vm.subject))]
                    )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "articles" },
                _vm._l(_vm.article, function(item, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass:
                        "articleContainer animated fadeIn slow delay-3s"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "d-flex",
                          attrs: {
                            to: {
                              name: "ArticleSinglePage",
                              params: {
                                id: item.product_id,
                                name: item.title
                                  .replace(/[^\w\s]/gi, "")
                                  .replace(/ /g, "-")
                                  .replace(/\?/g, "-")
                                  .replace(/\!/g, "")
                                  .replace(/\$/g, "")
                              }
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "articleDesc" }, [
                            _c("p", { staticClass: "title" }, [
                              _vm._v(_vm._s(item.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", {
                              staticClass: "articleD",
                              domProps: { innerHTML: _vm._s(item.description) }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "articleImage" }, [
                            _c("img", {
                              attrs: { src: item.coverImage, alt: "item.title" }
                            })
                          ])
                        ]
                      )
                    ],
                    1
                  )
                }),
                0
              ),
              _vm._v(" "),
              _vm.article.length === 0
                ? _c("div", { staticClass: "noContent" }, [
                    _vm._v(" No content yet")
                  ])
                : _vm._e()
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.name === "course"
          ? _c("section", { staticClass: "course" }, [
              _c("div", { staticClass: "sub" }, [
                _c("span", { staticClass: "header mainFontColor" }, [
                  _vm._v("Courses on")
                ]),
                _vm._v(" "),
                _vm.subject_id === ""
                  ? _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v("All Subjects")]
                    )
                  : _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v(_vm._s(_vm.subject))]
                    )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "videos" },
                _vm._l(_vm.course, function(item, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass: "vidContainer animated fadeIn slow delay-3s"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "routerlink",
                          attrs: {
                            to: {
                              name: "CourseFullPage",
                              params: {
                                id: item.product_id,
                                name: item.title.replace(/ /g, "-")
                              }
                            },
                            tag: "a"
                          }
                        },
                        [
                          _c("div", { staticClass: "vidImage" }, [
                            _c("img", {
                              attrs: { src: item.coverImage, alt: item.title }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "vidAbout" }, [
                            _c("p", { staticClass: "title" }, [
                              _vm._v(_vm._s(item.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v("Course")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v(_vm._s(item.industry.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "vendor" }, [
                              _vm._v(_vm._s(item.vendor.toLowerCase()))
                            ])
                          ])
                        ]
                      )
                    ],
                    1
                  )
                }),
                0
              ),
              _vm._v(" "),
              _vm.course.length === 0
                ? _c("div", { staticClass: "noContent" }, [
                    _vm._v(" No content yet")
                  ])
                : _vm._e()
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.name === "research"
          ? _c("section", { staticClass: "research" }, [
              _c("div", { staticClass: "sub" }, [
                _c("span", { staticClass: "header mainFontColor" }, [
                  _vm._v("Market Researches on")
                ]),
                _vm._v(" "),
                _vm.subject_id === ""
                  ? _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v("All Subjects")]
                    )
                  : _c(
                      "span",
                      { staticClass: "sideHeader mainFontColor toCaps" },
                      [_vm._v(_vm._s(_vm.subject))]
                    )
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "videos" },
                _vm._l(_vm.research, function(item, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass: "vidContainer animated fadeIn slow delay-3s"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "ProductPaperDetail",
                              params: {
                                id: item.product_id,
                                name: (item.title
                                  ? item.title
                                  : item.articleTitle
                                )
                                  .replace(/\s+/g, "-")
                                  .toLowerCase(),
                                type: "subscribe"
                              }
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "vidImage" }, [
                            _c("img", {
                              attrs: { src: item.coverImage, alt: item.title }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "vidAbout" }, [
                            _c("p", { staticClass: "title" }, [
                              _vm._v(_vm._s(item.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v("Market Research")
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "industry" }, [
                              _vm._v(_vm._s(item.industry.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("p", { staticClass: "vendor" }, [
                              _vm._v(_vm._s(item.vendor.toLowerCase()))
                            ])
                          ])
                        ]
                      )
                    ],
                    1
                  )
                }),
                0
              ),
              _vm._v(" "),
              _vm.research.length === 0
                ? _c("div", { staticClass: "noContent" }, [
                    _vm._v(" No content yet")
                  ])
                : _vm._e()
            ])
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("div", { staticClass: "loader" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-cb531bf6", module.exports)
  }
}

/***/ }),

/***/ 607:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1638)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1640)
/* template */
var __vue_template__ = __webpack_require__(1641)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-cb531bf6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/subjectMattersComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cb531bf6", Component.options)
  } else {
    hotAPI.reload("data-v-cb531bf6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ })

});