webpackJsonp([172],{

/***/ 1332:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1333);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2fcf4a66", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cbaea6d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminDraftProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-cbaea6d2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminDraftProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1333:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-cbaea6d2] {\n    height: 100vh;\n}\n.main-page[data-v-cbaea6d2] {\n    padding: 40px 20px;\n    max-height: 100vh;\n    height: 100vh;\n    overflow: scroll;\n}\n.rowProduct[data-v-cbaea6d2] {\n    margin-bottom: 50px;\n}\nth[data-v-cbaea6d2] {\n    background-color: #f3f3f3;\n    padding: 40px 10px !important;\n    color: #000000;\n}\ntd[data-v-cbaea6d2] {\n    color: #a4a4a4 !important;\n    padding: 30px 10px !important;\n}\n.dive[data-v-cbaea6d2] {\n    text-align: right;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-draft-product-component",
    data: function data() {
        return {
            products: [],
            overallCheck: false,
            checkIn: false
        };
    },
    mounted: function mounted() {
        var _this = this;

        var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
        if (authAdmin != null) {
            axios.get("/api/admin/get/draft-product", { headers: { "Authorization": "Bearer " + authAdmin.access_token } }).then(function (response) {
                if (response.data.data.length > 0) {
                    var emptyArray = [];

                    response.data.data.forEach(function (item) {
                        switch (item.prodType) {
                            case 'Books':
                                _this.$set(item.books, 'verify', item.verify);
                                _this.$set(item.books, 'uniId', item.id);
                                _this.$set(item.books, 'created_at', item.myDate);
                                emptyArray.push(item.books);
                                break;
                            case 'White Paper':
                                _this.$set(item.whitePaper, 'verify', item.verify);
                                _this.$set(item.whitePaper, 'uniId', item.id);
                                _this.$set(item.whitePaper, 'created_at', item.myDate);
                                emptyArray.push(item.whitePaper);
                                break;
                            case 'Market Reports':
                                _this.$set(item.marketReport, 'verify', item.verify);
                                _this.$set(item.marketReport, 'uniId', item.id);
                                _this.$set(item.marketReport, 'created_at', item.myDate);
                                emptyArray.push(item.marketReport);
                                break;
                            case 'Market Research':
                                _this.$set(item.marketResearch, 'verify', item.verify);
                                _this.$set(item.marketResearch, 'uniId', item.id);
                                _this.$set(item.marketResearch, 'created_at', item.myDate);
                                emptyArray.push(item.marketResearch);
                                break;
                            case 'Webinar':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Podcast':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Videos':
                                _this.$set(item.webinar, 'verify', item.verify);
                                _this.$set(item.webinar, 'uniId', item.id);
                                _this.$set(item.webinar, 'created_at', item.myDate);
                                emptyArray.push(item.webinar);
                                break;
                            case 'Journals':
                                _this.$set(item.journal, 'verify', item.verify);
                                _this.$set(item.journal, 'uniId', item.id);
                                _this.$set(item.journal, 'created_at', item.myDate);
                                emptyArray.push(item.journal);
                                break;
                            case 'Courses':
                                _this.$set(item.courses, 'verify', item.verify);
                                _this.$set(item.courses, 'uniId', item.id);
                                _this.$set(item.courses, 'created_at', item.myDate);
                                emptyArray.push(item.courses);
                                break;
                            default:
                                return false;
                        }
                    });
                    _this.products = emptyArray;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },

    methods: {
        overallCheckBox: function overallCheckBox() {
            var _this2 = this;

            //  this.checkIn = !this.checkIn;
            this.products.forEach(function (item) {
                if (!('itemClick' in item)) {
                    _this2.$set(item, 'itemClick', true);
                } else if (_this2.overallCheck) {
                    _this2.$set(item, 'itemClick', true);
                } else {
                    _this2.$set(item, 'itemClick', false);
                }
            });
        },
        deleteItem: function deleteItem() {
            var _this3 = this;

            var deletedArray = [];
            this.products.forEach(function (item) {
                if (item.itemClick) {
                    console.log(item);
                    console.log('yes');
                    deletedArray.push(item.uniId);
                }
            });
            if (deletedArray.length === 0) {
                this.$toasted.error('You must select at least one product to delete');
            } else {
                console.log(deletedArray);
                axios.post('/api/delete/many-products', JSON.parse(JSON.stringify(deletedArray)), { headers: { "Authorization": "Bearer " + this.token } }).then(function (response) {
                    if (response.status === 200) {
                        console.log(response);
                        for (var i = 0; i < deletedArray.length; i++) {
                            console.log(deletedArray[i]);
                            _this3.products.splice(i, 1);
                        }
                        _this3.$toasted.success('All selected items deleted');
                    } else {
                        _this3.$toasted.error('Unable to delete');
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }
});

/***/ }),

/***/ 1335:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("table", { staticClass: "table" }, [
        _c("thead", [
          _c("tr", [
            _c("th", [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.overallCheck,
                    expression: "overallCheck"
                  }
                ],
                attrs: { type: "checkbox" },
                domProps: {
                  checked: Array.isArray(_vm.overallCheck)
                    ? _vm._i(_vm.overallCheck, null) > -1
                    : _vm.overallCheck
                },
                on: {
                  change: [
                    function($event) {
                      var $$a = _vm.overallCheck,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && (_vm.overallCheck = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.overallCheck = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.overallCheck = $$c
                      }
                    },
                    _vm.overallCheckBox
                  ]
                }
              })
            ]),
            _vm._v(" "),
            _c("th", [_vm._v("Identification")]),
            _vm._v(" "),
            _c("th", [_vm._v("Details")]),
            _vm._v(" "),
            _c("th", [_vm._v("Status")]),
            _vm._v(" "),
            _c("th", [_vm._v("Created On")]),
            _vm._v(" "),
            _c("th")
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.products, function(product, index) {
            return _vm.products.length > 0
              ? _c("tr", [
                  _c("td", [
                    _c("input", {
                      attrs: { type: "checkbox" },
                      domProps: { checked: product.itemClick },
                      on: {
                        change: function($event) {
                          product.itemClick = !product.itemClick
                          _vm.overallCheck = false
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(index + 1))]),
                  _vm._v(" "),
                  product.articleTitle
                    ? _c("td", [_vm._v(_vm._s(product.articleTitle))])
                    : _c("td", [_vm._v(_vm._s(product.title))]),
                  _vm._v(" "),
                  product.verify === 1
                    ? _c("td", [_vm._v("Active")])
                    : _c("td", [_vm._v("Pending")]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(
                      _vm._s(_vm._f("moment")(product.created_at.date, "L"))
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "td",
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "linktoproduct",
                          attrs: {
                            to: {
                              name: "adminProductshowSave",
                              params: { id: product.uniId }
                            }
                          }
                        },
                        [_c("i", { staticClass: "fa fa-arrow-right" })]
                      )
                    ],
                    1
                  )
                ])
              : _c("tr", [_vm._v("No Product")])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-cbaea6d2", module.exports)
  }
}

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1332)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1334)
/* template */
var __vue_template__ = __webpack_require__(1335)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-cbaea6d2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminDraftProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cbaea6d2", Component.options)
  } else {
    hotAPI.reload("data-v-cbaea6d2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});