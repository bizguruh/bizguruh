webpackJsonp([108],{

/***/ 1589:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1590);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("575224a2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-235af56c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userShippingInformationComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-235af56c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userShippingInformationComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1590:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-235af56c]{\n    padding-top: 80px;\n}\n.footerB[data-v-235af56c] {\n        width: 100%;\n}\n.shipTabs[data-v-235af56c] {\n         margin: 30px;\n}\n.shipInner[data-v-235af56c] {\n        border: 1px solid #a3c2dc;\n        padding: 10px;\n        border-radius: 5px\n}\n.addAddress[data-v-235af56c] {\n        color: #ffffff;\n        background-color: #a3c2dc !important;\n}\n@media(max-width: 425px){\n.container[data-v-235af56c]{\n            padding: 60px 0 50px 0;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1591:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-shipping-information-component",
    data: function data() {
        return {
            user: {},
            userDetails: [],
            modal: true,
            fade: true,
            show: false,
            edit: false,
            userDetail: {
                id: '',
                firstName: '',
                lastName: '',
                phoneNo: '',
                address: '',
                state: '',
                city: '',
                country: '',
                user_id: ''
            }
        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user != null) {
            this.user = user;
            this.userDetail.user_id = user.id;

            axios.get('/api/user-details/' + this.user.id, { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this.userDetails = response.data;
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },

    methods: {
        editAddress: function editAddress(id, index) {
            this.edit = true;
            console.log(this.userDetails[index]);
            this.userDetail = this.userDetails[index];
        },
        add: function add() {
            console.log('fjfjfjf');
            this.fade = false;
            this.show = true;
        },
        addAddress: function addAddress() {
            var _this2 = this;

            if (this.edit) {
                axios.put('/api/user-detail/' + this.userDetail.id, JSON.parse(JSON.stringify(this.userDetail)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                    if (response.status === 200) {
                        _this2.$toasted.success('Address successfully updated');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                axios.post('/api/user-detail', JSON.parse(JSON.stringify(this.userDetail)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                    if (response.status === 201) {
                        _this2.$toasted.success('Address successfully added');
                        window.location.reload();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        deleteAddress: function deleteAddress(index, id) {
            var _this3 = this;

            axios.delete('/api/user-detail/' + id, { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.userDetails.splice(index, 1);
                    _this3.$toasted.success('Successfully deleted');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
});

/***/ }),

/***/ 1592:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "m-5" }, [
      _c("h2", [_vm._v("This is the shipping information")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "row" },
        _vm._l(_vm.userDetails, function(userDetail, index) {
          return _vm.userDetails.length > 0
            ? _c("div", { staticClass: "col-md-3 shipTabs" }, [
                _c("div", { staticClass: "shipInner" }, [
                  _c("div", [
                    _c("i", {
                      staticClass: "fa fa-pencil",
                      on: {
                        click: function($event) {
                          return _vm.editAddress(userDetail.id, index)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("div", [
                    _c("i", {
                      staticClass: "fa fa-trash",
                      on: {
                        click: function($event) {
                          return _vm.deleteAddress(index, userDetail.id)
                        }
                      }
                    })
                  ]),
                  _vm._v(" "),
                  userDetail.firstName
                    ? _c("p", [
                        _vm._v("First Name: " + _vm._s(userDetail.firstName))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.lastName
                    ? _c("p", [
                        _vm._v("Last Name: " + _vm._s(userDetail.lastName))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.phoneNo
                    ? _c("p", [
                        _vm._v("Phone No: " + _vm._s(userDetail.phoneNo))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.address
                    ? _c("p", [
                        _vm._v("Address: " + _vm._s(userDetail.address))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.city
                    ? _c("p", [_vm._v("City: " + _vm._s(userDetail.city))])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.state
                    ? _c("p", [_vm._v("State: " + _vm._s(userDetail.state))])
                    : _vm._e(),
                  _vm._v(" "),
                  userDetail.country
                    ? _c("p", [
                        _vm._v("Country: " + _vm._s(userDetail.country))
                      ])
                    : _vm._e()
                ])
              ])
            : _vm._e()
        }),
        0
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-6" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "firstName" } }, [
              _vm._v("First Name")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.firstName,
                  expression: "userDetail.firstName"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "firstName" },
              domProps: { value: _vm.userDetail.firstName },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "firstName", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "lastName" } }, [_vm._v("Last Name")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.lastName,
                  expression: "userDetail.lastName"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "lastName" },
              domProps: { value: _vm.userDetail.lastName },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "lastName", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.phoneNo,
                  expression: "userDetail.phoneNo"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", id: "phoneNo" },
              domProps: { value: _vm.userDetail.phoneNo },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "phoneNo", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "address" } }, [_vm._v("Address")]),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.address,
                  expression: "userDetail.address"
                }
              ],
              staticClass: "form-control",
              attrs: { id: "address" },
              domProps: { value: _vm.userDetail.address },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "address", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "state" } }, [_vm._v("State")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.state,
                  expression: "userDetail.state"
                }
              ],
              staticClass: "form-control",
              attrs: { id: "state" },
              domProps: { value: _vm.userDetail.state },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "state", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-6" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "state" } }, [_vm._v("City")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.city,
                  expression: "userDetail.city"
                }
              ],
              staticClass: "form-control",
              attrs: { id: "city" },
              domProps: { value: _vm.userDetail.city },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "city", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "state" } }, [_vm._v("Country")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.userDetail.country,
                  expression: "userDetail.country"
                }
              ],
              staticClass: "form-control",
              attrs: { id: "country" },
              domProps: { value: _vm.userDetail.country },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.userDetail, "country", $event.target.value)
                }
              }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-d-12 center footerB" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-primary addAddress",
              on: {
                click: function($event) {
                  return _vm.addAddress()
                }
              }
            },
            [_vm._v("Add Address")]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-235af56c", module.exports)
  }
}

/***/ }),

/***/ 595:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1589)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1591)
/* template */
var __vue_template__ = __webpack_require__(1592)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-235af56c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userShippingInformationComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-235af56c", Component.options)
  } else {
    hotAPI.reload("data-v-235af56c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});