webpackJsonp([128],{

/***/ 1609:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1610);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3f9d4f7f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b7a6900c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPartnerProfileComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b7a6900c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userPartnerProfileComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1610:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-b7a6900c] {\n  margin-top: 60px;\n  padding-top: 20px;\n  padding-bottom: 120px;\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  background: white;\n}\nul[data-v-b7a6900c],\nol[data-v-b7a6900c] {\n  list-style: none;\n}\n.title[data-v-b7a6900c] {\n  width: 100%;\n}\na[data-v-b7a6900c] {\n  color: hsl(207, 43%, 20%) !important;\n}\na[data-v-b7a6900c]:hover {\n  color: rgba(0, 0, 0, 0.64) !important;\n  text-decoration: underline !important;\n}\nh2[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.84);\n}\nh4[data-v-b7a6900c] {\n  margin-bottom: 10px;\n  color: rgba(0, 0, 0, 0.6);\n}\n.subject[data-v-b7a6900c] {\n  position: absolute;\n  bottom: 10px;\n  left: 35px;\n  color: hsl(207, 43%, 20%);\n  font-size: 14px;\n  text-transform: capitalize;\n  line-height: 1.2;\n}\n.subject a[data-v-b7a6900c] {\n  color: hsl(207, 43%, 20%) !important;\n}\n.miniAbout[data-v-b7a6900c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 15px 0;\n}\n.joinDate[data-v-b7a6900c] {\n  font-size: 11px;\n  color: rgba(0, 0, 0, 0.54);\n}\n.vendor[data-v-b7a6900c] {\n  font-size: 14px;\n  color: rgba(0, 0, 0, 0.64);\n  padding-bottom: 10px;\n  text-transform: capitalize;\n}\n.imgCover[data-v-b7a6900c] {\n  width: 40px;\n  height: 40px;\n  overflow: hidden;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n.imgCover img[data-v-b7a6900c] {\n  height: 100%;\n  width: 100%;\n}\n.partnerName[data-v-b7a6900c] {\n  text-transform: capitalize;\n}\n.tabs[data-v-b7a6900c] {\n  -webkit-transition: all 0.4s ease;\n  transition: all 0.4s ease;\n  border-bottom: 1px solid #f7f8fa;\n}\n.tab[data-v-b7a6900c] {\n  padding: 2px 30px;\n  color: rgba(0, 0, 0, 0.54);\n  cursor: pointer;\n}\n.tab[data-v-b7a6900c]:hover {\n  color: rgba(0, 0, 0, 0.74);\n}\n.img_text h3[data-v-b7a6900c] {\n  margin-bottom: 2px;\n}\n.activeTab[data-v-b7a6900c] {\n  background: #ffffff;\n  border-bottom: 1px solid #000000;\n  color: rgba(0, 0, 0, 0.84);\n  border-radius: 5px 5px 0 0;\n}\n.spark[data-v-b7a6900c] {\n  float: right;\n}\n.follow[data-v-b7a6900c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.swiper-container[data-v-b7a6900c] {\n  height: auto !important;\n  margin-left: auto;\n  margin-right: auto;\n}\n/* .swiper-slide {\n\theight: 200px;\n} */\nh3[data-v-b7a6900c] {\n  margin-bottom: 20px;\n}\n.popular_box[data-v-b7a6900c] {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  background: #f7f8fa;\n  margin-bottom: 40px;\n  padding: 25px;\n  -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);\n          box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);\n  border-radius: 3px;\n}\n.tab-content p[data-v-b7a6900c] {\n  line-height: 1.2;\n}\n.bio[data-v-b7a6900c] {\n  height: 130px;\n  overflow: hidden;\n}\n.aboutCover[data-v-b7a6900c] {\n  width: 90%;\n}\n.img_text[data-v-b7a6900c] {\n  width: 100%;\n  height: 100%;\n  padding: 20px 10px;\n  font-size: 16px;\n  line-height: 1.6 !important;\n}\n.img_cover[data-v-b7a6900c] {\n  width: 100%;\n  height: 300px;\n}\n.img_cover img[data-v-b7a6900c] {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.vue-tabs.nav-tabs > li > a[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.btn[data-v-b7a6900c] {\n  padding: 0 8px !important;\n  color: hsl(207, 43%, 20%) !important;\n  text-transform: capitalize !important;\n  font-weight: normal;\n  border: 1px solid hsl(207, 43%, 20%);;\n  border-radius: 3px;\n  cursor: pointer;\n  margin: auto 10px;\n  padding: 2px 14px;\n  text-transform: capitalize;\n\n  /* height: 35px; */\n}\n.btn[data-v-b7a6900c]:hover {\n  background: rgba(164, 194, 219, 0.5) !important;\n  color: #fff !important;\n}\n.btn-primary[data-v-b7a6900c] {\n  padding: 10px 20px !important;\n  color: #fff !important;\n  text-transform: capitalize !important;\n  font-weight: normal;\n  text-decoration: none;\n  border-radius: 3px;\n  background: hsl(207, 43%, 20%) !important;\n  cursor: pointer;\n  /* margin: auto ; */\n  padding: 2px 14px;\n  text-transform: capitalize;\n  border: none;\n}\n.btn-primary[data-v-b7a6900c]:hover {\n  background: rgba(164, 194, 219, 0.5) !important;\n  color: #fff !important;\n}\n.ac[data-v-b7a6900c] {\n  display: block;\n  max-width: 868px;\n  width: 100%;\n  margin-right: auto;\n  margin-left: auto;\n  margin-bottom: 30px;\n  padding: 0 15px;\n}\n.followingVendor[data-v-b7a6900c] {\n  color: #ffffff !important;\n  background-color: hsl(207, 43%, 20%) !important;\n  font-weight: normal;\n  border: 1px solid hsl(207, 43%, 20%);\n  border-radius: 3px;\n  cursor: pointer;\n  margin: auto 10px;\n  padding: 2px 4px;\n  text-transform: capitalize;\n  height: 25px;\n}\np[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76);\n}\nsmall[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.54);\n}\nli[data-v-b7a6900c] {\n  font-size: 12px;\n  color: rgba(0, 0, 0, 0.54);\n  padding: 5px;\n}\n.a[data-v-b7a6900c] {\n  display: block;\n  max-width: 868px;\n  width: 100%;\n  min-height: 200px;\n  margin-right: auto;\n  margin-left: auto;\n  margin-bottom: 30px;\n\n  font-family: \"medium-content-sans-serif-font\", \"-apple-system\",\n    \"BlinkMacSystemFont\", \"Segoe UI\", Roboto, Oxygen, Ubuntu, Cantarell,\n    \"Open Sans\", \"Helvetica Neue\", sans-serif !important;\n}\n.b[data-v-b7a6900c] {\n  max-width: 868px;\n  width: 100%;\n  height: auto;\n  /* border: 1px solid black; */\n  margin-right: auto;\n  margin-left: auto;\n}\n.aa[data-v-b7a6900c] {\n  /* border: 1px red dotted; */\n  width: 70%;\n  height: 130px;\n  line-height: 1.6;\n}\n.ac[data-v-b7a6900c] {\n  display: block;\n  max-width: 868px;\n  width: 100%;\n  margin-right: auto;\n  margin-left: auto;\n  margin-bottom: 30px;\n  padding: 0;\n}\n.ab[data-v-b7a6900c] {\n  border-top: 3px #8291c0 dotted;\n  border-bottom: 3px #8291c0 dotted;\n  width: 120px;\n  height: 120px;\n  border-radius: 50%;\n  overflow: hidden;\n  float: right;\n}\n.ab img[data-v-b7a6900c] {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  vertical-align: middle;\n  padding: 5px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.vue-tabs[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\na[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\na.tabs__link[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.title.title_center[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.76) !important;\n}\n.primary_header[data-v-b7a6900c] {\n  width: 100%;\n  margin-bottom: 20px;\n  text-align: left;\n  font-weight: bold;\n}\n.bop[data-v-b7a6900c] {\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.boxx[data-v-b7a6900c] {\n  position: relative;\n  width: 24%;\n  height: 200px;\n  padding: 0 !important;\n  margin-right: 1%;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  -webkit-box-shadow: none !important;\n          box-shadow: none !important;\n  border-radius: 5px;\n}\n.boxx img[data-v-b7a6900c] {\n  width: 100%;\n  height: 100%;\n  border-radius: 5px;\n}\n.shadow[data-v-b7a6900c] {\n  position: absolute;\n  top: 0;\n  background: -webkit-gradient(linear, left top, right top, from(rgb(0, 0, 0)), to(rgb(79, 79, 79)));\n  background: linear-gradient(to right, rgb(0, 0, 0), rgb(79, 79, 79));\n  background: -webkit-gradient(\n    linear,\n    left top, right top,\n    from(rgb(0, 0, 0, 0.5)),\n    to(rgb(79, 79, 79, 0.5))\n  );\n  background: linear-gradient(\n    to right,\n    rgb(0, 0, 0, 0.5),\n    rgb(79, 79, 79, 0.5)\n  );\n  color: #f1f1f1;\n  width: 100%;\n  padding: 60px 0;\n  height: 200px;\n  text-align: center;\n  border-radius: 5px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n}\n.text[data-v-b7a6900c] {\n  font-size: calc(9px + (24 - 9) * ((100vw - 300px) / (1600 - 300)));\n  line-height: 1.6;\n}\n.textContent[data-v-b7a6900c] {\n  color: rgba(0, 0, 0, 0.54);\n  font-weight: normal;\n  height: 25px;\n  overflow: hidden;\n  line-clamp: 1;\n  -webkit-line-clamp: 1;\n  -moz-line-clamp: 1;\n  -ms-line-clamp: 1;\n  -o-line-clamp: 1;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  text-overflow: ellipsis;\n  word-wrap: break-word;\n  white-space: normal;\n}\n.partner_name[data-v-b7a6900c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n@media (max-width: 425px) {\n.container[data-v-b7a6900c] {\n    padding: 20px 15px;\n}\n.btn-primary[data-v-b7a6900c] {\n    font-size: 12px !important;\n    padding: 5px 10px !important;\n}\nh2[data-v-b7a6900c] {\n    font-size: 18px;\n}\nh4[data-v-b7a6900c] {\n    font-size: 16px;\n}\n.primary_header[data-v-b7a6900c] {\n    font-size: 16px;\n}\n.textContent[data-v-b7a6900c] {\n    margin-bottom: 20px;\n}\n.popular_box[data-v-b7a6900c] {\n    padding: 10px;\n}\n.tab[data-v-b7a6900c] {\n    padding: 5px 10px;\n    font-size: 11px;\n}\n.b[data-v-b7a6900c] {\n    padding: 0;\n}\nh2[data-v-b7a6900c] {\n    font-size: 1em;\n}\n.tab-content p[data-v-b7a6900c] {\n    line-height: 1.2;\n    font-size: 0.7em;\n}\n.img_text[data-v-b7a6900c] {\n    width: 100%;\n    height: 100%;\n    padding: 6px 5px;\n    font-size: 16px;\n    line-height: 1.2 !important;\n}\n.img_text h3[data-v-b7a6900c] {\n    font-size: 16px;\n}\n.ab[data-v-b7a6900c] {\n    border-top: 3px hsl(207, 43%, 20%) dotted;\n    border-bottom: 3px hsl(207, 43%, 20%) dotted;\n    width: 80px;\n    height: 80px;\n    border-radius: 50%;\n    overflow: hidden;\n    float: right;\n}\n.nav[data-v-b7a6900c] {\n    font-size: 11px;\n}\n.vue-tabs .nav > li span.title[data-v-b7a6900c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px;\n}\n.vue-tabs .nav li span.title[data-v-b7a6900c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px !important;\n}\n.vue-tabs .nav li span .title[data-v-b7a6900c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    font-size: 11px !important;\n}\n.a[data-v-b7a6900c] {\n    color: rgba(0, 0, 0, 0.76) !important;\n    padding: 0;\n    min-height: 300px;\n}\n.row .a[data-v-b7a6900c] {\n    padding: 0 !important;\n}\n.ab[data-v-b7a6900c] {\n    float: none;\n    width: 120%px;\n    margin-bottom: 10px;\n}\n.aa[data-v-b7a6900c] {\n    clear: right;\n    width: 100%;\n}\n.boxx[data-v-b7a6900c] {\n    width: 24%;\n    height: 120px;\n}\n.shadow[data-v-b7a6900c] {\n    padding: 30px 0;\n    height: 120px;\n}\n.primary_header[data-v-b7a6900c] {\n    font-size: 16px !important;\n}\n.img_cover[data-v-b7a6900c] {\n    width: 100%;\n    height: 120px;\n}\n.textContent[data-v-b7a6900c] {\n    font-size: 14px;\n    line-height: 1.6;\n}\nh3[data-v-b7a6900c]{\n    font-size: 15px;\n}\n.subject[data-v-b7a6900c] {\n    left: 15px;\n    font-size: 12px;\n}\np[data-v-b7a6900c]{\n    font-size: 15px;\n}\n.btn[data-v-b7a6900c] {\n    height: 15px;\n    font-size: 9px;\n    margin: 1px 5px;\n}\n.followingVendor[data-v-b7a6900c] {\n    height: 15px;\n    margin: 1px 5px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1611:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-Partner-Profile-component",
  data: function data() {
    var _ref;

    return _ref = {
      username: this.$route.params.username,
      join: "",
      article: {},
      user: {},
      token: "",
      nonSubscriber: false,
      authenticate: false,
      readCount: 0,
      writterD: {},
      auth: "",
      followNo: 0,
      update: 0,
      vendor: {}
    }, _defineProperty(_ref, "vendor", {}), _defineProperty(_ref, "userFollowing", {}), _defineProperty(_ref, "following", false), _defineProperty(_ref, "followText", "follow"), _defineProperty(_ref, "vendorProducts", []), _defineProperty(_ref, "industries", []), _defineProperty(_ref, "concepts", []), _defineProperty(_ref, "categories", []), _defineProperty(_ref, "articles", false), _defineProperty(_ref, "courses", false), _defineProperty(_ref, "videos", false), _defineProperty(_ref, "podcast", false), _defineProperty(_ref, "research", false), _defineProperty(_ref, "articlesF", false), _defineProperty(_ref, "coursesF", false), _defineProperty(_ref, "videosF", false), _defineProperty(_ref, "podcastF", false), _defineProperty(_ref, "researchF", false), _defineProperty(_ref, "userPreference", []), _defineProperty(_ref, "vendorTopics", []), _defineProperty(_ref, "swiperOption", {
      slidesPerView: 1,
      slidesPerColumn: 2,
      spaceBetween: 20,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },

      observer: true,
      observeParents: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      }
    }), _defineProperty(_ref, "swiperOptions", {
      slidesPerView: 2,
      slidesPerColumn: 2,
      spaceBetween: 20,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },

      observer: true,
      observeParents: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 30
        }
      }
    }), _defineProperty(_ref, "openPref", false), _defineProperty(_ref, "openOthers", true), _defineProperty(_ref, "coursesP", []), _defineProperty(_ref, "articlesP", []), _defineProperty(_ref, "videosP", []), _defineProperty(_ref, "researchP", []), _defineProperty(_ref, "podcastP", []), _defineProperty(_ref, "isActive", true), _ref;
  },


  watch: {
    openPref: "getProducts"
  },

  created: function created() {
    var _this = this;

    axios.get("/api/vendor/" + this.username.toLowerCase()).then(function (response) {
      if (response.status === 200) {
        _this.vendor = response.data.vendorU[0];

        var user = JSON.parse(localStorage.getItem("authUser"));
        var data = {
          id: _this.vendor.id,
          prodType: "Articles",
          readCount: _this.readCount,
          industries: [],
          concepts: [],
          categories: []
        };

        if (user != null) {
          _this.user = user;
          _this.auth = true;
          _this.token = user.access_token;

          axios.get("/api/user-following/" + data.id, {
            headers: { Authorization: "Bearer " + _this.token }
          }).then(function (response) {
            if (response.status === 200) {
              if (response.data === true) {
                _this.following = true;
                _this.followText = "following";
              } else {
                _this.following = false;
                _this.followText = "follow";
              }
            }
          });
          _this.$emit("activity", _this.vendor.id, 1, 0);
        }

        axios.get("/api/getfollowers/" + data.id).then(function (response) {
          if (response.status === 200) {
            _this.followNo = response.data;
          }
        });
        _this.getProducts();
      }
    });

    axios.get("/api/products").then(function (res) {
      res.data.data.forEach(function (element) {
        var vendor_user_id = Number(_this.vendor.id);
        if (element.vendor_user_id === vendor_user_id) {}
      });
    });
  },


  methods: {
    getProducts: function getProducts() {
      var _this2 = this;

      this.coursesP = [];
      this.articlesP = [];
      this.videosP = [];
      this.researchP = [];
      this.podcastP = [];
      axios.get("/api/vendor-products/" + this.vendor.id).then(function (response) {
        if (response.status === 200) {
          _this2.vendorProducts = response.data.data.reverse();

          _this2.vendorProducts.forEach(function (item) {
            if (_this2.openPref) {
              _this2.userPreference.forEach(function (element) {
                if (element.typeId === JSON.parse(item.sub_category_brand_id)[0]) {
                  _this2.vendorTopics.push(item);
                  _this2.vendorTopics.forEach(function (item) {
                    if (item.prodType === "Articles") {
                      _this2.articlesP.push(item);
                      _this2.articlesF = true;
                    }
                    if (item.prodType === "Courses") {
                      _this2.coursesF = true;

                      _this2.coursesP.push(item);
                    }
                    if (item.prodType === "Videos") {
                      _this2.videosP.push(item);
                      _this2.videosF = true;
                    }
                    if (item.prodType === "Podcast") {

                      _this2.podcastF = true;
                      _this2.podcastP.push(item);
                    }
                    if (item.prodType === "Market Research") {
                      _this2.researchF = true;
                      _this2.researchP.push(item);
                    }
                  });
                }
              });
            }
            if (!_this2.openPref) {
              if (item.prodType === "Articles") {
                _this2.articlesP.push(item);
                _this2.articles = true;
              }
              if (item.prodType === "Courses") {
                _this2.coursesP.push(item);
                _this2.courses = true;
              }
              if (item.prodType === "Videos") {
                _this2.videosP.push(item);
                _this2.videos = true;
              }
              if (item.prodType === "Podcast") {

                _this2.podcastP.push(item);
                _this2.podcast = true;
              }
              if (item.prodType === "Market Research") {
                _this2.research = true;
                _this2.researchP.push(item);
              }
            }
          });
          _this2.isActive = false;
        }
      });
    },
    askVendor: function askVendor() {
      this.$router.push({
        name: "BizguruhQuestionBank",
        query: {
          vendor: this.vendor.id
        }
      });
    },
    joinDate: function joinDate(date) {
      var joinDate = new Date(date);
      var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var month = joinDate.getMonth();
      return mlist[month] + " " + joinDate.getFullYear();
    },
    openPrefTab: function openPrefTab() {
      this.openPref = true;
      this.openOthers = false;
    },
    openOtherTab: function openOtherTab() {
      this.openPref = false;
      this.openOthers = true;
    },
    followToggle: function followToggle() {
      this.following ? this.unFollow() : this.follow(this.vendor.id, this.vendor.storeName);
    },
    follow: function follow(params, name) {
      var _this3 = this;

      var data = {
        vendorName: name,
        vendorId: params
      };
      if (this.auth) {
        axios.post("/api/user-following", JSON.parse(JSON.stringify(data)), {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 201) {
            _this3.following = true;
            _this3.followText = "following";
            _this3.$toasted.success("Successfully followed");
            _this3.followNo++;
            _this3.forceRender();
          } else if (response.data === "Already following") {
            _this3.$toasted.error("Already following");
          } else {
            _this3.$toasted.error("Error");
          }
        }).catch(function (error) {
          console.log(error);
        });
      } else {
        this.$toasted.error("You have to log in to follow");
      }
    },
    unFollow: function unFollow() {
      var _this4 = this;

      var vendor = this.vendor.id;
      var unfollow = confirm("unfollow " + this.vendor.storeName.toLowerCase() + "?");
      if (unfollow) {
        axios.delete("/api/user-following/" + vendor, {
          headers: { Authorization: "Bearer " + this.token }
        }).then(function (response) {
          if (response.status === 200) {
            _this4.following = false;
            _this4.followText = "follow";
            _this4.$toasted.success("Successfully Unfollowed");
            _this4.followNo--;
            _this4.forceRender();
          } else {
            _this4.$toasted.error("Already Unfollowed");
          }
        });
      }
    }
  },

  components: {}
});

/***/ }),

/***/ 1612:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.vendorProducts
    ? _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row a" }, [
          _vm.vendor.valid_id !== null
            ? _c("div", { staticClass: "ab" }, [
                _c("img", {
                  attrs: {
                    src: _vm.vendor.valid_id,
                    alt: _vm.vendor.storeName,
                    srcset: ""
                  }
                })
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.vendor.valid_id === null
            ? _c("div", { staticClass: "ab" }, [
                _c("img", {
                  attrs: { src: "/images/profile.png", alt: "", srcset: "" }
                })
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "aa" }, [
            _c("div", { staticClass: "partner_name mb-2" }, [
              _vm.vendor.storeName
                ? _c("h2", { staticClass: "partnerName mr-3 " }, [
                    _vm._v(_vm._s(_vm.vendor.storeName))
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c(
                "button",
                {
                  key: _vm.update,
                  staticClass:
                    "elevated_btn elevated_btn_sm btn-compliment text-white mt-0",
                  attrs: { type: "button" },
                  on: { click: _vm.followToggle }
                },
                [_vm._v(_vm._s(_vm.followText))]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "bio" }, [
              _vm.vendor.bio !== null
                ? _c("p", [_vm._v(_vm._s(_vm.vendor.bio))])
                : _c("p", [_vm._v(_vm._s(_vm.vendor.bio))])
            ]),
            _vm._v(" "),
            _c("div", [
              _c("small", [
                _vm._v(
                  "Member since " +
                    _vm._s(
                      _vm._f("moment")(_vm.vendor.created_at, " MMMM YYYY")
                    )
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", [
              _c("small", [_vm._v(_vm._s(_vm.followNo) + " Follower(s)")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row ac" }, [
          _c(
            "button",
            {
              staticClass:
                "elevated_btn elevated_btn_sm btn-compliment text-white",
              attrs: { type: "button" },
              on: { click: _vm.askVendor }
            },
            [_vm._v("Ask " + _vm._s(_vm.vendor.storeName))]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row b" }, [
          _c("div", { staticClass: "tabs" }, [
            _c(
              "span",
              {
                staticClass: "tab",
                class: { activeTab: _vm.openOthers },
                on: { click: _vm.openOtherTab }
              },
              [_vm._v("Based on Others")]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "tab",
                class: { activeTab: _vm.openPref },
                on: { click: _vm.openPrefTab }
              },
              [_vm._v("Based on Preference")]
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.openOthers,
                  expression: "openOthers"
                }
              ],
              staticClass: "title"
            },
            [
              _vm.vendorProducts.length > 0
                ? _c("h2", { staticClass: "primary_header mt-4" }, [
                    _vm._v("Latest")
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.vendorProducts.length === 0
                ? _c("h2", { staticClass: "primary_header mt-4" }, [
                    _vm._v("No available content yet")
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("div", [
                _c(
                  "div",
                  { staticClass: "mb-4" },
                  [
                    _c(
                      "swiper",
                      {
                        staticClass: "mb-4",
                        attrs: { options: _vm.swiperOption }
                      },
                      [
                        _vm._l(_vm.vendorProducts, function(latest) {
                          return _c("swiper-slide", { key: latest.id }, [
                            latest.prodType === "Courses"
                              ? _c(
                                  "div",
                                  { staticClass: "popular_box" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "subject" },
                                      [
                                        _c(
                                          "router-link",
                                          { attrs: { to: "/courses" } },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.prodType.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v("/\n                    "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Industry",
                                                params: {
                                                  id: latest.industry.id,
                                                  name: latest.industry.name.replace(
                                                    / /g,
                                                    ""
                                                  )
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.industry.name.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "miniAbout" }, [
                                      _c("div", { staticClass: "imgCover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.vendor.valid_id,
                                            alt: _vm.vendor.storeName,
                                            srcset: ""
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "aboutCover" }, [
                                        _c(
                                          "div",
                                          { staticClass: "vendor mr-2" },
                                          [
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.vendor.storeName.toLowerCase()
                                                ) +
                                                " in\n                        "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "SubjectMatters",
                                                    params: {
                                                      id:
                                                        latest.subjectMatter.id,
                                                      name: latest.subjectMatter.name
                                                        .toLowerCase()
                                                        .replace(/ /g, "-")
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.subjectMatter.name.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "joinDate" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                latest.courses.created_at,
                                                "dddd, MMMM Do YYYY"
                                              )
                                            )
                                          )
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "spark" }, [
                                        _c("img", {
                                          attrs: {
                                            src: "/images/spark.png",
                                            alt: "",
                                            height: "30",
                                            width: "35"
                                          }
                                        })
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass: "routerlink",
                                        attrs: {
                                          to: {
                                            name: "CourseFullPage",
                                            params: {
                                              id: latest.courses.product_id,
                                              name: latest.courses.title.replace(
                                                / /g,
                                                "-"
                                              )
                                            }
                                          },
                                          tag: "a"
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "img_cover" },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: latest.coverImage,
                                                alt: latest.courses.title,
                                                srcset: ""
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "img_text" }, [
                                      _c("h3", [
                                        _vm._v(_vm._s(latest.courses.title))
                                      ]),
                                      _vm._v(" "),
                                      _c("p", { staticClass: "textContent" }, [
                                        _vm._v(_vm._s(latest.courses.overview))
                                      ])
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            latest.prodType === "Articles"
                              ? _c(
                                  "div",
                                  { staticClass: "popular_box" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "subject" },
                                      [
                                        _c(
                                          "router-link",
                                          { attrs: { to: "/articles" } },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.prodType.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v("/\n                    "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Industry",
                                                params: {
                                                  id: latest.industry.id,
                                                  name: latest.industry.name.replace(
                                                    / /g,
                                                    ""
                                                  )
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.industry.name.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "miniAbout" }, [
                                      _c("div", { staticClass: "imgCover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.vendor.valid_id,
                                            alt: _vm.vendor.storeName,
                                            srcset: ""
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "aboutCover" }, [
                                        _c(
                                          "div",
                                          { staticClass: "vendor mr-2" },
                                          [
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.vendor.storeName.toLowerCase()
                                                ) +
                                                " in\n                        "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "SubjectMatters",
                                                    params: {
                                                      id:
                                                        latest.subjectMatter.id,
                                                      name: latest.subjectMatter.name
                                                        .toLowerCase()
                                                        .replace(/ /g, "-")
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.subjectMatter.name.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "joinDate" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                latest.articles.created_at,
                                                "dddd, MMMM Do YYYY"
                                              )
                                            )
                                          )
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "spark" }, [
                                        _c("img", {
                                          attrs: {
                                            src: "/images/spark.png",
                                            alt: "",
                                            height: "30",
                                            width: "35"
                                          }
                                        })
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "ArticleSinglePage",
                                            params: {
                                              id: latest.articles.product_id,
                                              name: latest.articles.title
                                                .replace(/[^a-z0-9]/gi, "")
                                                .replace(/\$/g, "")
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "img_cover" },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: latest.coverImage,
                                                alt: latest.articles.title,
                                                srcset: ""
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "img_text" }, [
                                      _c("h3", { staticClass: "toCaps" }, [
                                        _vm._v(
                                          _vm._s(
                                            latest.articles.title.toLowerCase()
                                          )
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("p", {
                                        staticClass: "textContent",
                                        domProps: {
                                          innerHTML: _vm._s(
                                            latest.articles.description
                                          )
                                        }
                                      })
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            latest.prodType === "Videos"
                              ? _c(
                                  "div",
                                  { staticClass: "popular_box" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "subject" },
                                      [
                                        _c(
                                          "router-link",
                                          { attrs: { to: "/videos" } },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.prodType.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v("/\n                    "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Industry",
                                                params: {
                                                  id: latest.industry.id,
                                                  name: latest.industry.name.replace(
                                                    / /g,
                                                    ""
                                                  )
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.industry.name.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "miniAbout" }, [
                                      _c("div", { staticClass: "imgCover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.vendor.valid_id,
                                            alt: _vm.vendor.storeName,
                                            srcset: ""
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "aboutCover" }, [
                                        _c(
                                          "div",
                                          { staticClass: "vendor mr-2" },
                                          [
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.vendor.storeName.toLowerCase()
                                                ) +
                                                " in\n                        "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "SubjectMatters",
                                                    params: {
                                                      id:
                                                        latest.subjectMatter.id,
                                                      name: latest.subjectMatter.name
                                                        .toLowerCase()
                                                        .replace(/ /g, "-")
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.subjectMatter.name.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "joinDate" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                latest.webinar.created_at,
                                                "dddd, MMMM Do YYYY"
                                              )
                                            )
                                          )
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "spark" }, [
                                        _c("img", {
                                          attrs: {
                                            src: "/images/spark.png",
                                            alt: "",
                                            height: "30",
                                            width: "35"
                                          }
                                        })
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "Video",
                                            params: {
                                              id: latest.webinar.product_id
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "img_cover" },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: latest.coverImage,
                                                alt: latest.webinar.title,
                                                srcset: ""
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "img_text" }, [
                                      _c("h3", [
                                        _vm._v(_vm._s(latest.webinar.title))
                                      ]),
                                      _vm._v(" "),
                                      _c("p", { staticClass: "textContent" }, [
                                        _vm._v(
                                          _vm._s(latest.webinar.description)
                                        )
                                      ])
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            latest.prodType === "Podcast"
                              ? _c(
                                  "div",
                                  { staticClass: "popular_box" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "subject" },
                                      [
                                        _c(
                                          "router-link",
                                          { attrs: { to: "/podcast" } },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.prodType.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v("/\n                    "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Industry",
                                                params: {
                                                  id: latest.industry.id,
                                                  name: latest.industry.name.replace(
                                                    / /g,
                                                    ""
                                                  )
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.industry.name.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "miniAbout" }, [
                                      _c("div", { staticClass: "imgCover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.vendor.valid_id,
                                            alt: _vm.vendor.storeName,
                                            srcset: ""
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "aboutCover" }, [
                                        _c(
                                          "div",
                                          { staticClass: "vendor mr-2" },
                                          [
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.vendor.storeName.toLowerCase()
                                                ) +
                                                " in\n                        "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "SubjectMatters",
                                                    params: {
                                                      id:
                                                        latest.subjectMatter.id,
                                                      name: latest.subjectMatter.name
                                                        .toLowerCase()
                                                        .replace(/ /g, "-")
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.subjectMatter.name.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "joinDate" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                latest.webinarVideo.created_at,
                                                "dddd, MMMM Do YYYY"
                                              )
                                            )
                                          )
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "spark" }, [
                                        _c("img", {
                                          attrs: {
                                            src: "/images/spark.png",
                                            alt: "",
                                            height: "30",
                                            width: "35"
                                          }
                                        })
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "SinglePodcastPage",
                                            params: {
                                              id: latest.webinarVideo.product_id
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "img_cover" },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: latest.coverImage,
                                                alt:
                                                  "latest.webinarVideo.title",
                                                srcset: ""
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "img_text" }, [
                                      _c("h3", [
                                        _vm._v(
                                          _vm._s(latest.webinarVideo.title)
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("p", { staticClass: "textContent" }, [
                                        _vm._v(
                                          _vm._s(
                                            latest.webinarVideo.description
                                          )
                                        )
                                      ])
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            latest.prodType === "Market Research"
                              ? _c(
                                  "div",
                                  { staticClass: "popular_box" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "subject" },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "WhitePaperAdmin",
                                                params: {
                                                  name: "market-research"
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.prodType.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v("/\n                    "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Industry",
                                                params: {
                                                  id: latest.industry.id,
                                                  name: latest.industry.name.replace(
                                                    / /g,
                                                    ""
                                                  )
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  latest.industry.name.toLowerCase()
                                                )
                                              )
                                            ])
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "miniAbout" }, [
                                      _c("div", { staticClass: "imgCover" }, [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.vendor.valid_id,
                                            alt: _vm.vendor.storeName,
                                            srcset: ""
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "aboutCover" }, [
                                        _c(
                                          "div",
                                          { staticClass: "vendor mr-2" },
                                          [
                                            _vm._v(
                                              "\n                        " +
                                                _vm._s(
                                                  _vm.vendor.storeName.toLowerCase()
                                                ) +
                                                " in\n                        "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "SubjectMatters",
                                                    params: {
                                                      id:
                                                        latest.subjectMatter.id,
                                                      name: latest.subjectMatter.name
                                                        .toLowerCase()
                                                        .replace(/ /g, "-")
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.subjectMatter.name.toLowerCase()
                                                  )
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "joinDate" }, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("moment")(
                                                latest.marketResearch
                                                  .created_at,
                                                "dddd, MMMM Do YYYY"
                                              )
                                            )
                                          )
                                        ])
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "spark" }, [
                                        _c("img", {
                                          attrs: {
                                            src: "/images/spark.png",
                                            alt: "",
                                            height: "30",
                                            width: "35"
                                          }
                                        })
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        attrs: {
                                          to: {
                                            name: "ProductPaperDetail",
                                            params: {
                                              id:
                                                latest.marketResearch
                                                  .product_id,
                                              name: (latest.marketResearch.title
                                                ? latest.marketResearch.title
                                                : latest.marketResearch
                                                    .articleTitle
                                              )
                                                .replace(/\s+/g, "-")
                                                .toLowerCase(),
                                              type: "subscribe"
                                            }
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "img_cover" },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: latest.coverImage,
                                                alt:
                                                  latest.marketResearch.title,
                                                srcset: ""
                                              }
                                            })
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "img_text" }, [
                                      _c("h3", [
                                        _vm._v(
                                          _vm._s(latest.marketResearch.title)
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("p", { staticClass: "textContent" }, [
                                        _vm._v(
                                          _vm._s(latest.marketResearch.overview)
                                        )
                                      ])
                                    ])
                                  ],
                                  1
                                )
                              : _vm._e()
                          ])
                        }),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "swiper-button-next swiper-button-black",
                          attrs: { slot: "button-next" },
                          slot: "button-next"
                        }),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "swiper-button-prev swiper-button-black",
                          attrs: { slot: "button-prev" },
                          slot: "button-prev"
                        }),
                        _vm._v(" "),
                        _c("div", {
                          staticClass: "swiper-pagination",
                          attrs: { slot: "pagination" },
                          slot: "pagination"
                        })
                      ],
                      2
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm.vendorProducts.length > 0
                ? _c("h2", { staticClass: "mb-4 mt-4" }, [_vm._v("Featured")])
                : _vm._e(),
              _vm._v(" "),
              _vm.articles
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Articles")]),
                        _vm._v(" "),
                        _vm.articlesP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.articlesP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    { key: latest.id },
                                    [
                                      latest.prodType === "Articles"
                                        ? _c(
                                            "div",
                                            { staticClass: "popular_box" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "subject" },
                                                [
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: { to: "/articles" }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.prodType.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  ),
                                                  _vm._v(
                                                    "/\n                    "
                                                  ),
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name: "Industry",
                                                          params: {
                                                            id:
                                                              latest.industry
                                                                .id,
                                                            name: latest.industry.name.replace(
                                                              / /g,
                                                              ""
                                                            )
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.industry.name.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "miniAbout" },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "imgCover" },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            _vm.vendor.valid_id,
                                                          alt:
                                                            _vm.vendor
                                                              .storeName,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "aboutCover"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "vendor mr-2"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                        " +
                                                              _vm._s(
                                                                _vm.vendor.storeName.toLowerCase()
                                                              ) +
                                                              " in\n                        "
                                                          ),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "SubjectMatters",
                                                                  params: {
                                                                    id:
                                                                      latest
                                                                        .subjectMatter
                                                                        .id,
                                                                    name: latest.subjectMatter.name
                                                                      .toLowerCase()
                                                                      .replace(
                                                                        / /g,
                                                                        "-"
                                                                      )
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(
                                                                  latest.subjectMatter.name.toLowerCase()
                                                                )
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "joinDate"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("moment")(
                                                                latest.articles
                                                                  .created_at,
                                                                "dddd, MMMM Do YYYY"
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "ArticleSinglePage",
                                                      params: {
                                                        id:
                                                          latest.articles
                                                            .product_id,
                                                        name: latest.articles.title
                                                          .replace(
                                                            /[^a-z0-9]/gi,
                                                            ""
                                                          )
                                                          .replace(/\$/g, "")
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "img_cover"
                                                    },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            latest.coverImage,
                                                          alt:
                                                            latest.articles
                                                              .title,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "img_text" },
                                                [
                                                  _c(
                                                    "h3",
                                                    { staticClass: "toCaps" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          latest.articles.title.toLowerCase()
                                                        )
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("p", {
                                                    staticClass: "textContent",
                                                    domProps: {
                                                      innerHTML: _vm._s(
                                                        latest.articles
                                                          .description
                                                      )
                                                    }
                                                  })
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.courses
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Courses")]),
                        _vm._v(" "),
                        _vm.coursesP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOptions }
                              },
                              [
                                _vm._l(_vm.coursesP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    { key: latest.id },
                                    [
                                      latest.prodType === "Courses"
                                        ? _c(
                                            "div",
                                            { staticClass: "popular_box" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "subject" },
                                                [
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: { to: "/courses" }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.prodType.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  ),
                                                  _vm._v(
                                                    "/\n                    "
                                                  ),
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name: "Industry",
                                                          params: {
                                                            id:
                                                              latest.industry
                                                                .id,
                                                            name: latest.industry.name.replace(
                                                              / /g,
                                                              ""
                                                            )
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.industry.name.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "miniAbout" },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "imgCover" },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            _vm.vendor.valid_id,
                                                          alt:
                                                            _vm.vendor
                                                              .storeName,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "aboutCover"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "vendor mr-2"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                        " +
                                                              _vm._s(
                                                                _vm.vendor.storeName.toLowerCase()
                                                              ) +
                                                              " in\n                        "
                                                          ),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "SubjectMatters",
                                                                  params: {
                                                                    id:
                                                                      latest
                                                                        .subjectMatter
                                                                        .id,
                                                                    name: latest.subjectMatter.name
                                                                      .toLowerCase()
                                                                      .replace(
                                                                        / /g,
                                                                        "-"
                                                                      )
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(
                                                                  latest.subjectMatter.name.toLowerCase()
                                                                )
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "joinDate"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("moment")(
                                                                latest.courses
                                                                  .created_at,
                                                                "dddd, MMMM Do YYYY"
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  staticClass: "routerlink",
                                                  attrs: {
                                                    to: {
                                                      name: "CourseFullPage",
                                                      params: {
                                                        id:
                                                          latest.courses
                                                            .product_id,
                                                        name: latest.courses.title.replace(
                                                          / /g,
                                                          "-"
                                                        )
                                                      }
                                                    },
                                                    tag: "a"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "img_cover"
                                                    },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            latest.coverImage,
                                                          alt:
                                                            latest.courses
                                                              .title,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "img_text" },
                                                [
                                                  _c("h3", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.courses.title
                                                      )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "p",
                                                    {
                                                      staticClass: "textContent"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          latest.courses
                                                            .overview
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.videos
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Videos")]),
                        _vm._v(" "),
                        _vm.videosP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.videosP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    { key: latest.id },
                                    [
                                      latest.prodType === "Videos"
                                        ? _c(
                                            "div",
                                            { staticClass: "popular_box" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "subject" },
                                                [
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: { to: "/videos" }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.prodType.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  ),
                                                  _vm._v(
                                                    "/\n                    "
                                                  ),
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name: "Industry",
                                                          params: {
                                                            id:
                                                              latest.industry
                                                                .id,
                                                            name: latest.industry.name.replace(
                                                              / /g,
                                                              ""
                                                            )
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.industry.name.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "miniAbout" },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "imgCover" },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            _vm.vendor.valid_id,
                                                          alt:
                                                            _vm.vendor
                                                              .storeName,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "aboutCover"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "vendor mr-2"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                        " +
                                                              _vm._s(
                                                                _vm.vendor.storeName.toLowerCase()
                                                              ) +
                                                              " in\n                        "
                                                          ),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "SubjectMatters",
                                                                  params: {
                                                                    id:
                                                                      latest
                                                                        .subjectMatter
                                                                        .id,
                                                                    name: latest.subjectMatter.name
                                                                      .toLowerCase()
                                                                      .replace(
                                                                        / /g,
                                                                        "-"
                                                                      )
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(
                                                                  latest.subjectMatter.name.toLowerCase()
                                                                )
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "joinDate"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("moment")(
                                                                latest.webinar
                                                                  .created_at,
                                                                "dddd, MMMM Do YYYY"
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Video",
                                                      params: {
                                                        id:
                                                          latest.webinar
                                                            .product_id
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "img_cover"
                                                    },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            latest.coverImage,
                                                          alt:
                                                            latest.webinar
                                                              .title,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "img_text" },
                                                [
                                                  _c("h3", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.webinar.title
                                                      )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "p",
                                                    {
                                                      staticClass: "textContent"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          latest.webinar
                                                            .description
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.podcast
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Podcasts")]),
                        _vm._v(" "),
                        _vm.podcastP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.podcastP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    { key: latest.id },
                                    [
                                      latest.prodType === "Podcast"
                                        ? _c(
                                            "div",
                                            { staticClass: "popular_box" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "subject" },
                                                [
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: { to: "/podcast" }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.prodType.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  ),
                                                  _vm._v(
                                                    "/\n                    "
                                                  ),
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name: "Industry",
                                                          params: {
                                                            id:
                                                              latest.industry
                                                                .id,
                                                            name: latest.industry.name.replace(
                                                              / /g,
                                                              ""
                                                            )
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.industry.name.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "miniAbout" },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "imgCover" },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            _vm.vendor.valid_id,
                                                          alt:
                                                            _vm.vendor
                                                              .storeName,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "aboutCover"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "vendor mr-2"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                        " +
                                                              _vm._s(
                                                                _vm.vendor.storeName.toLowerCase()
                                                              ) +
                                                              " in\n                        "
                                                          ),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "SubjectMatters",
                                                                  params: {
                                                                    id:
                                                                      latest
                                                                        .subjectMatter
                                                                        .id,
                                                                    name: latest.subjectMatter.name
                                                                      .toLowerCase()
                                                                      .replace(
                                                                        / /g,
                                                                        "-"
                                                                      )
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(
                                                                  latest.subjectMatter.name.toLowerCase()
                                                                )
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "joinDate"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("moment")(
                                                                latest
                                                                  .webinarVideo
                                                                  .created_at,
                                                                "dddd, MMMM Do YYYY"
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "SinglePodcastPage",
                                                      params: {
                                                        id:
                                                          latest.webinarVideo
                                                            .product_id
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "img_cover"
                                                    },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            latest.coverImage,
                                                          alt:
                                                            "latest.webinarVideo.title",
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "img_text" },
                                                [
                                                  _c("h3", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.webinarVideo
                                                          .title
                                                      )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "p",
                                                    {
                                                      staticClass: "textContent"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          latest.webinarVideo
                                                            .description
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.research
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Market Research")]),
                        _vm._v(" "),
                        _vm.researchP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.researchP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    { key: latest.id },
                                    [
                                      latest.prodType === "Market Research"
                                        ? _c(
                                            "div",
                                            { staticClass: "popular_box" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "subject" },
                                                [
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name:
                                                            "WhitePaperAdmin",
                                                          params: {
                                                            name:
                                                              "market-research"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.prodType.toLowerCase()
                                                          )
                                                        )
                                                      ]),
                                                      _vm._v(
                                                        " /\n                      "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name: "Industry",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .industry
                                                                    .id,
                                                                name: latest.industry.name.replace(
                                                                  / /g,
                                                                  ""
                                                                )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("span", [
                                                            _vm._v(
                                                              _vm._s(
                                                                latest.industry.name.toLowerCase()
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "miniAbout" },
                                                [
                                                  _c(
                                                    "div",
                                                    { staticClass: "imgCover" },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            _vm.vendor.valid_id,
                                                          alt:
                                                            _vm.vendor
                                                              .storeName,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "aboutCover"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "vendor mr-2"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "\n                        " +
                                                              _vm._s(
                                                                _vm.vendor.storeName.toLowerCase()
                                                              ) +
                                                              " in\n                        "
                                                          ),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "SubjectMatters",
                                                                  params: {
                                                                    id:
                                                                      latest
                                                                        .subjectMatter
                                                                        .id,
                                                                    name: latest.subjectMatter.name
                                                                      .toLowerCase()
                                                                      .replace(
                                                                        / /g,
                                                                        "-"
                                                                      )
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                _vm._s(
                                                                  latest.subjectMatter.name.toLowerCase()
                                                                )
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "joinDate"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("moment")(
                                                                latest
                                                                  .marketResearch
                                                                  .created_at,
                                                                "dddd, MMMM Do YYYY"
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name:
                                                        "ProductPaperDetail",
                                                      params: {
                                                        id:
                                                          latest.marketResearch
                                                            .product_id,
                                                        name: (latest
                                                          .marketResearch.title
                                                          ? latest
                                                              .marketResearch
                                                              .title
                                                          : latest
                                                              .marketResearch
                                                              .articleTitle
                                                        )
                                                          .replace(/\s+/g, "-")
                                                          .toLowerCase(),
                                                        type: "subscribe"
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "img_cover"
                                                    },
                                                    [
                                                      _c("img", {
                                                        attrs: {
                                                          src:
                                                            latest.coverImage,
                                                          alt:
                                                            latest
                                                              .marketResearch
                                                              .title,
                                                          srcset: ""
                                                        }
                                                      })
                                                    ]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "img_text" },
                                                [
                                                  _c("h3", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.marketResearch
                                                          .title
                                                      )
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "p",
                                                    {
                                                      staticClass: "textContent"
                                                    },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          latest.marketResearch
                                                            .overview
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e()
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.openPref,
                  expression: "openPref"
                }
              ],
              staticClass: "title"
            },
            [
              _vm.vendorTopics.length > 0
                ? _c("h2", { staticClass: "primary_header mt-4" }, [
                    _vm._v("Recent")
                  ])
                : _c("h2", { staticClass: "primary_header mt-4" }, [
                    _vm._v("No available content")
                  ]),
              _vm._v(" "),
              _vm.vendorTopics
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c(
                          "swiper",
                          {
                            staticClass: "mb-4",
                            attrs: { options: _vm.swiperOption }
                          },
                          [
                            _vm._l(_vm.vendorTopics, function(latest) {
                              return _c("swiper-slide", { key: latest.id }, [
                                latest.prodType === "Courses"
                                  ? _c(
                                      "div",
                                      { staticClass: "popular_box" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "subject" },
                                          [
                                            _c(
                                              "router-link",
                                              { attrs: { to: "/courses" } },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.prodType.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            ),
                                            _vm._v("/\n                    "),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "Industry",
                                                    params: {
                                                      id: latest.industry.id,
                                                      name: latest.industry.name.replace(
                                                        / /g,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.industry.name.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "miniAbout" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "imgCover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: _vm.vendor.valid_id,
                                                    alt: _vm.vendor.storeName,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "aboutCover" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "vendor mr-2"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                        " +
                                                        _vm._s(
                                                          _vm.vendor.storeName.toLowerCase()
                                                        ) +
                                                        " in\n                        "
                                                    ),
                                                    _c(
                                                      "router-link",
                                                      {
                                                        attrs: {
                                                          to: {
                                                            name:
                                                              "SubjectMatters",
                                                            params: {
                                                              id:
                                                                latest
                                                                  .subjectMatter
                                                                  .id,
                                                              name: latest.subjectMatter.name
                                                                .toLowerCase()
                                                                .replace(
                                                                  / /g,
                                                                  "-"
                                                                )
                                                            }
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.subjectMatter.name.toLowerCase()
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "joinDate" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          latest.courses
                                                            .created_at,
                                                          "dddd, MMMM Do YYYY"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "spark" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: "/images/spark.png",
                                                    alt: "",
                                                    height: "30",
                                                    width: "35"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "routerlink",
                                            attrs: {
                                              to: {
                                                name: "CourseFullPage",
                                                params: {
                                                  id: latest.courses.product_id,
                                                  name: latest.courses.title.replace(
                                                    / /g,
                                                    "-"
                                                  )
                                                }
                                              },
                                              tag: "a"
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "img_cover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: latest.coverImage,
                                                    alt: latest.courses.title,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "img_text" }, [
                                          _c("h3", [
                                            _vm._v(_vm._s(latest.courses.title))
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "p",
                                            { staticClass: "textContent" },
                                            [
                                              _vm._v(
                                                _vm._s(latest.courses.overview)
                                              )
                                            ]
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                latest.prodType === "Articles"
                                  ? _c(
                                      "div",
                                      { staticClass: "popular_box" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "subject" },
                                          [
                                            _c(
                                              "router-link",
                                              { attrs: { to: "/articles" } },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.prodType.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            ),
                                            _vm._v("/\n                    "),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "Industry",
                                                    params: {
                                                      id: latest.industry.id,
                                                      name: latest.industry.name.replace(
                                                        / /g,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.industry.name.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "miniAbout" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "imgCover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: _vm.vendor.valid_id,
                                                    alt: _vm.vendor.storeName,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "aboutCover" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "vendor mr-2"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                        " +
                                                        _vm._s(
                                                          _vm.vendor.storeName.toLowerCase()
                                                        ) +
                                                        " in\n                        "
                                                    ),
                                                    _c(
                                                      "router-link",
                                                      {
                                                        attrs: {
                                                          to: {
                                                            name:
                                                              "SubjectMatters",
                                                            params: {
                                                              id:
                                                                latest
                                                                  .subjectMatter
                                                                  .id,
                                                              name: latest.subjectMatter.name
                                                                .toLowerCase()
                                                                .replace(
                                                                  / /g,
                                                                  "-"
                                                                )
                                                            }
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.subjectMatter.name.toLowerCase()
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "joinDate" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          latest.articles
                                                            .created_at,
                                                          "dddd, MMMM Do YYYY"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "spark" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: "/images/spark.png",
                                                    alt: "",
                                                    height: "30",
                                                    width: "35"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "ArticleSinglePage",
                                                params: {
                                                  id:
                                                    latest.articles.product_id,
                                                  name: latest.articles.title
                                                    .replace(/[^a-z0-9]/gi, "")
                                                    .replace(/\$/g, "")
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "img_cover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: latest.coverImage,
                                                    alt: latest.articles.title,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "img_text" }, [
                                          _c("h3", { staticClass: "toCaps" }, [
                                            _vm._v(
                                              _vm._s(
                                                latest.articles.title.toLowerCase()
                                              )
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("p", {
                                            staticClass: "textContent",
                                            domProps: {
                                              innerHTML: _vm._s(
                                                latest.articles.description
                                              )
                                            }
                                          })
                                        ])
                                      ],
                                      1
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                latest.prodType === "Videos"
                                  ? _c(
                                      "div",
                                      { staticClass: "popular_box" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "subject" },
                                          [
                                            _c(
                                              "router-link",
                                              { attrs: { to: "/videos" } },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.prodType.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            ),
                                            _vm._v("/\n                    "),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "Industry",
                                                    params: {
                                                      id: latest.industry.id,
                                                      name: latest.industry.name.replace(
                                                        / /g,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.industry.name.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "miniAbout" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "imgCover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: _vm.vendor.valid_id,
                                                    alt: _vm.vendor.storeName,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "aboutCover" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "vendor mr-2"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                        " +
                                                        _vm._s(
                                                          _vm.vendor.storeName.toLowerCase()
                                                        ) +
                                                        " in\n                        "
                                                    ),
                                                    _c(
                                                      "router-link",
                                                      {
                                                        attrs: {
                                                          to: {
                                                            name:
                                                              "SubjectMatters",
                                                            params: {
                                                              id:
                                                                latest
                                                                  .subjectMatter
                                                                  .id,
                                                              name: latest.subjectMatter.name
                                                                .toLowerCase()
                                                                .replace(
                                                                  / /g,
                                                                  "-"
                                                                )
                                                            }
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.subjectMatter.name.toLowerCase()
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "joinDate" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          latest.webinar
                                                            .created_at,
                                                          "dddd, MMMM Do YYYY"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "spark" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: "/images/spark.png",
                                                    alt: "",
                                                    height: "30",
                                                    width: "35"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "Video",
                                                params: {
                                                  id: latest.webinar.product_id
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "img_cover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: latest.coverImage,
                                                    alt: latest.webinar.title,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "img_text" }, [
                                          _c("h3", [
                                            _vm._v(_vm._s(latest.webinar.title))
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "p",
                                            { staticClass: "textContent" },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.webinar.description
                                                )
                                              )
                                            ]
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                latest.prodType === "Podcast"
                                  ? _c(
                                      "div",
                                      { staticClass: "popular_box" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "subject" },
                                          [
                                            _c(
                                              "router-link",
                                              { attrs: { to: "/podcast" } },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.prodType.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            ),
                                            _vm._v("/\n                    "),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "Industry",
                                                    params: {
                                                      id: latest.industry.id,
                                                      name: latest.industry.name.replace(
                                                        / /g,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.industry.name.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "miniAbout" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "imgCover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: _vm.vendor.valid_id,
                                                    alt: _vm.vendor.storeName,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "aboutCover" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "vendor mr-2"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                        " +
                                                        _vm._s(
                                                          _vm.vendor.storeName.toLowerCase()
                                                        ) +
                                                        " in\n                        "
                                                    ),
                                                    _c(
                                                      "router-link",
                                                      {
                                                        attrs: {
                                                          to: {
                                                            name:
                                                              "SubjectMatters",
                                                            params: {
                                                              id:
                                                                latest
                                                                  .subjectMatter
                                                                  .id,
                                                              name: latest.subjectMatter.name
                                                                .toLowerCase()
                                                                .replace(
                                                                  / /g,
                                                                  "-"
                                                                )
                                                            }
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.subjectMatter.name.toLowerCase()
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "joinDate" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          latest.webinarVideo
                                                            .created_at,
                                                          "dddd, MMMM Do YYYY"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "spark" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: "/images/spark.png",
                                                    alt: "",
                                                    height: "30",
                                                    width: "35"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "SinglePodcastPage",
                                                params: {
                                                  id:
                                                    latest.webinarVideo
                                                      .product_id
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "img_cover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: latest.coverImage,
                                                    alt:
                                                      "latest.webinarVideo.title",
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "img_text" }, [
                                          _c("h3", [
                                            _vm._v(
                                              _vm._s(latest.webinarVideo.title)
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "p",
                                            { staticClass: "textContent" },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.webinarVideo
                                                    .description
                                                )
                                              )
                                            ]
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                latest.prodType === "Market Research"
                                  ? _c(
                                      "div",
                                      { staticClass: "popular_box" },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "subject" },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "WhitePaperAdmin",
                                                    params: {
                                                      name: "market-research"
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.prodType.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            ),
                                            _vm._v("/\n                    "),
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "Industry",
                                                    params: {
                                                      id: latest.industry.id,
                                                      name: latest.industry.name.replace(
                                                        / /g,
                                                        ""
                                                      )
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.industry.name.toLowerCase()
                                                    )
                                                  )
                                                ])
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "miniAbout" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "imgCover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: _vm.vendor.valid_id,
                                                    alt: _vm.vendor.storeName,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "aboutCover" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "vendor mr-2"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                        " +
                                                        _vm._s(
                                                          _vm.vendor.storeName.toLowerCase()
                                                        ) +
                                                        " in\n                        "
                                                    ),
                                                    _c(
                                                      "router-link",
                                                      {
                                                        attrs: {
                                                          to: {
                                                            name:
                                                              "SubjectMatters",
                                                            params: {
                                                              id:
                                                                latest
                                                                  .subjectMatter
                                                                  .id,
                                                              name: latest.subjectMatter.name
                                                                .toLowerCase()
                                                                .replace(
                                                                  / /g,
                                                                  "-"
                                                                )
                                                            }
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.subjectMatter.name.toLowerCase()
                                                          )
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "joinDate" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm._f("moment")(
                                                          latest.marketResearch
                                                            .created_at,
                                                          "dddd, MMMM Do YYYY"
                                                        )
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "spark" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: "/images/spark.png",
                                                    alt: "",
                                                    height: "30",
                                                    width: "35"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "ProductPaperDetail",
                                                params: {
                                                  id:
                                                    latest.marketResearch
                                                      .product_id,
                                                  name: (latest.marketResearch
                                                    .title
                                                    ? latest.marketResearch
                                                        .title
                                                    : latest.marketResearch
                                                        .articleTitle
                                                  )
                                                    .replace(/\s+/g, "-")
                                                    .toLowerCase(),
                                                  type: "subscribe"
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "img_cover" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: latest.coverImage,
                                                    alt:
                                                      latest.marketResearch
                                                        .title,
                                                    srcset: ""
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "img_text" }, [
                                          _c("h3", [
                                            _vm._v(
                                              _vm._s(
                                                latest.marketResearch.title
                                              )
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "p",
                                            { staticClass: "textContent" },
                                            [
                                              _vm._v(
                                                _vm._s(
                                                  latest.marketResearch.overview
                                                )
                                              )
                                            ]
                                          )
                                        ])
                                      ],
                                      1
                                    )
                                  : _vm._e()
                              ])
                            }),
                            _vm._v(" "),
                            _c("div", {
                              staticClass:
                                "swiper-button-next swiper-button-black",
                              attrs: { slot: "button-next" },
                              slot: "button-next"
                            }),
                            _vm._v(" "),
                            _c("div", {
                              staticClass:
                                "swiper-button-prev swiper-button-black",
                              attrs: { slot: "button-prev" },
                              slot: "button-prev"
                            }),
                            _vm._v(" "),
                            _c("div", {
                              staticClass: "swiper-pagination",
                              attrs: { slot: "pagination" },
                              slot: "pagination"
                            })
                          ],
                          2
                        )
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm.vendorTopics.length > 0
                ? _c("h2", { staticClass: "mb-4 mt-4" }, [_vm._v("Featured")])
                : _vm._e(),
              _vm._v(" "),
              _vm.articlesF
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Articles")]),
                        _vm._v(" "),
                        _vm.articlesP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.articlesP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    {
                                      key: latest.id,
                                      staticClass: "popular_box"
                                    },
                                    [
                                      _c(
                                        "div",
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "subject" },
                                            [
                                              _c(
                                                "router-link",
                                                { attrs: { to: "/articles" } },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.prodType.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v("/\n                    "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Industry",
                                                      params: {
                                                        id: latest.industry.id,
                                                        name: latest.industry.name.replace(
                                                          / /g,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.industry.name.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "miniAbout" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "imgCover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.vendor.valid_id,
                                                      alt: _vm.vendor.storeName,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "aboutCover" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "vendor mr-2"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                        " +
                                                          _vm._s(
                                                            _vm.vendor.storeName.toLowerCase()
                                                          ) +
                                                          " in\n                        "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "SubjectMatters",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .subjectMatter
                                                                    .id,
                                                                name: latest.subjectMatter.name
                                                                  .toLowerCase()
                                                                  .replace(
                                                                    / /g,
                                                                    "-"
                                                                  )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              latest.subjectMatter.name.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "joinDate" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("moment")(
                                                            latest.articles
                                                              .created_at,
                                                            "dddd, MMMM Do YYYY"
                                                          )
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "ArticleSinglePage",
                                                  params: {
                                                    id:
                                                      latest.articles
                                                        .product_id,
                                                    name: latest.articles.title
                                                      .replace(
                                                        /[^a-z0-9]/gi,
                                                        ""
                                                      )
                                                      .replace(/\$/g, "")
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "img_cover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: latest.coverImage,
                                                      alt:
                                                        latest.articles.title,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "img_text" },
                                            [
                                              _c(
                                                "h3",
                                                { staticClass: "toCaps" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.articles.title.toLowerCase()
                                                    )
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c("p", {
                                                staticClass: "textContent",
                                                domProps: {
                                                  innerHTML: _vm._s(
                                                    latest.articles.description
                                                  )
                                                }
                                              })
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.coursesF
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Courses")]),
                        _vm._v(" "),
                        _vm.coursesP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOptions }
                              },
                              [
                                _vm._l(_vm.coursesP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    {
                                      key: latest.id,
                                      staticClass: "popular_box"
                                    },
                                    [
                                      _c(
                                        "div",
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "subject" },
                                            [
                                              _c(
                                                "router-link",
                                                { attrs: { to: "/courses" } },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.prodType.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Industry",
                                                      params: {
                                                        id: latest.industry.id,
                                                        name: latest.industry.name.replace(
                                                          / /g,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.industry.name.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "miniAbout" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "imgCover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.vendor.valid_id,
                                                      alt: _vm.vendor.storeName,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "aboutCover" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "vendor mr-2"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                        " +
                                                          _vm._s(
                                                            _vm.vendor.storeName.toLowerCase()
                                                          ) +
                                                          " in\n                        "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "SubjectMatters",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .subjectMatter
                                                                    .id,
                                                                name: latest.subjectMatter.name
                                                                  .toLowerCase()
                                                                  .replace(
                                                                    / /g,
                                                                    "-"
                                                                  )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              latest.subjectMatter.name.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "joinDate" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("moment")(
                                                            latest.courses
                                                              .created_at,
                                                            "dddd, MMMM Do YYYY"
                                                          )
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              staticClass: "routerlink",
                                              attrs: {
                                                to: {
                                                  name: "CourseFullPage",
                                                  params: {
                                                    id:
                                                      latest.courses.product_id,
                                                    name: latest.courses.title.replace(
                                                      / /g,
                                                      "-"
                                                    )
                                                  }
                                                },
                                                tag: "a"
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "img_cover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: latest.coverImage,
                                                      alt: latest.courses.title,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "img_text" },
                                            [
                                              _c(
                                                "h3",
                                                { staticClass: "toCaps" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.courses.title.toLowerCase()
                                                    )
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "p",
                                                { staticClass: "textContent" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.courses.overview
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.videosF
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Videos")]),
                        _vm._v(" "),
                        _vm.videosP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.videosP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    {
                                      key: latest.id,
                                      staticClass: "popular_box"
                                    },
                                    [
                                      _c(
                                        "div",
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "subject" },
                                            [
                                              _c(
                                                "router-link",
                                                { attrs: { to: "/videos" } },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.prodType.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v("/\n                    "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Industry",
                                                      params: {
                                                        id: latest.industry.id,
                                                        name: latest.industry.name.replace(
                                                          / /g,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.industry.name.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "miniAbout" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "imgCover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.vendor.valid_id,
                                                      alt: _vm.vendor.storeName,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "aboutCover" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "vendor mr-2"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                        " +
                                                          _vm._s(
                                                            _vm.vendor.storeName.toLowerCase()
                                                          ) +
                                                          " in\n                        "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "SubjectMatters",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .subjectMatter
                                                                    .id,
                                                                name: latest.subjectMatter.name
                                                                  .toLowerCase()
                                                                  .replace(
                                                                    / /g,
                                                                    "-"
                                                                  )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              latest.subjectMatter.name.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "joinDate" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("moment")(
                                                            latest.webinar
                                                              .created_at,
                                                            "dddd, MMMM Do YYYY"
                                                          )
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "Video",
                                                  params: {
                                                    id:
                                                      latest.webinar.product_id
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "img_cover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: latest.coverImage,
                                                      alt: latest.webinar.title,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "img_text" },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  _vm._s(latest.webinar.title)
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "p",
                                                { staticClass: "textContent" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.webinar.description
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.podcastF
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Podcasts")]),
                        _vm._v(" "),
                        _vm.podcastP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.podcastP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    {
                                      key: latest.id,
                                      staticClass: "popular_box"
                                    },
                                    [
                                      _c(
                                        "div",
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "subject" },
                                            [
                                              _c(
                                                "router-link",
                                                { attrs: { to: "/podcast" } },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.prodType.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              ),
                                              _vm._v("/\n                    "),
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "Industry",
                                                      params: {
                                                        id: latest.industry.id,
                                                        name: latest.industry.name.replace(
                                                          / /g,
                                                          ""
                                                        )
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.industry.name.toLowerCase()
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "miniAbout" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "imgCover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.vendor.valid_id,
                                                      alt: _vm.vendor.storeName,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "aboutCover" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "vendor mr-2"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                        " +
                                                          _vm._s(
                                                            _vm.vendor.storeName.toLowerCase()
                                                          ) +
                                                          " in\n                        "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "SubjectMatters",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .subjectMatter
                                                                    .id,
                                                                name: latest.subjectMatter.name
                                                                  .toLowerCase()
                                                                  .replace(
                                                                    / /g,
                                                                    "-"
                                                                  )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              latest.subjectMatter.name.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "joinDate" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("moment")(
                                                            latest.webinarVideo
                                                              .created_at,
                                                            "dddd, MMMM Do YYYY"
                                                          )
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "SinglePodcastPage",
                                                  params: {
                                                    id:
                                                      latest.webinarVideo
                                                        .product_id
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "img_cover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: latest.coverImage,
                                                      alt:
                                                        "latest.webinarVideo.title",
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "img_text" },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.webinarVideo.title
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "p",
                                                { staticClass: "textContent" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.webinarVideo
                                                        .description
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.researchF
                ? _c("div", [
                    _c(
                      "div",
                      { staticClass: "mb-4" },
                      [
                        _c("h4", [_vm._v("Market Research")]),
                        _vm._v(" "),
                        _vm.researchP.length > 0
                          ? _c(
                              "swiper",
                              {
                                staticClass: "mb-4",
                                attrs: { options: _vm.swiperOption }
                              },
                              [
                                _vm._l(_vm.researchP, function(latest) {
                                  return _c(
                                    "swiper-slide",
                                    {
                                      key: latest.id,
                                      staticClass: "popular_box"
                                    },
                                    [
                                      _c(
                                        "div",
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "subject" },
                                            [
                                              _c(
                                                "router-link",
                                                {
                                                  attrs: {
                                                    to: {
                                                      name: "WhitePaperAdmin",
                                                      params: {
                                                        name: "market-research"
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        latest.prodType.toLowerCase()
                                                      )
                                                    )
                                                  ]),
                                                  _vm._v(
                                                    " /\n                      "
                                                  ),
                                                  _c(
                                                    "router-link",
                                                    {
                                                      attrs: {
                                                        to: {
                                                          name: "Industry",
                                                          params: {
                                                            id:
                                                              latest.industry
                                                                .id,
                                                            name: latest.industry.name.replace(
                                                              / /g,
                                                              ""
                                                            )
                                                          }
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            latest.industry.name.toLowerCase()
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "miniAbout" },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "imgCover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.vendor.valid_id,
                                                      alt: _vm.vendor.storeName,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "aboutCover" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "vendor mr-2"
                                                    },
                                                    [
                                                      _vm._v(
                                                        "\n                        " +
                                                          _vm._s(
                                                            _vm.vendor.storeName.toLowerCase()
                                                          ) +
                                                          " in\n                        "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          attrs: {
                                                            to: {
                                                              name:
                                                                "SubjectMatters",
                                                              params: {
                                                                id:
                                                                  latest
                                                                    .subjectMatter
                                                                    .id,
                                                                name: latest.subjectMatter.name
                                                                  .toLowerCase()
                                                                  .replace(
                                                                    / /g,
                                                                    "-"
                                                                  )
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              latest.subjectMatter.name.toLowerCase()
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    { staticClass: "joinDate" },
                                                    [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("moment")(
                                                            latest
                                                              .marketResearch
                                                              .created_at,
                                                            "dddd, MMMM Do YYYY"
                                                          )
                                                        )
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "router-link",
                                            {
                                              attrs: {
                                                to: {
                                                  name: "ProductPaperDetail",
                                                  params: {
                                                    id:
                                                      latest.marketResearch
                                                        .product_id,
                                                    name: (latest.marketResearch
                                                      .title
                                                      ? latest.marketResearch
                                                          .title
                                                      : latest.marketResearch
                                                          .articleTitle
                                                    )
                                                      .replace(/\s+/g, "-")
                                                      .toLowerCase(),
                                                    type: "subscribe"
                                                  }
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "img_cover" },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: latest.coverImage,
                                                      alt:
                                                        latest.marketResearch
                                                          .title,
                                                      srcset: ""
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            { staticClass: "img_text" },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  _vm._s(
                                                    latest.marketResearch.title
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "p",
                                                { staticClass: "textContent" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      latest.marketResearch
                                                        .overview
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-next swiper-button-black",
                                  attrs: { slot: "button-next" },
                                  slot: "button-next"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass:
                                    "swiper-button-prev swiper-button-black",
                                  attrs: { slot: "button-prev" },
                                  slot: "button-prev"
                                }),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "swiper-pagination",
                                  attrs: { slot: "pagination" },
                                  slot: "pagination"
                                })
                              ],
                              2
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e()
            ]
          )
        ])
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b7a6900c", module.exports)
  }
}

/***/ }),

/***/ 600:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1609)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1611)
/* template */
var __vue_template__ = __webpack_require__(1612)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b7a6900c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userPartnerProfileComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b7a6900c", Component.options)
  } else {
    hotAPI.reload("data-v-b7a6900c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});