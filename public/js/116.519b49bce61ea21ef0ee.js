webpackJsonp([116],{

/***/ 1511:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1512);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("7a9e8bca", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8196583e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userEventPageComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8196583e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userEventPageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1512:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.selectCheck[data-v-8196583e] {\n    display: none;\n}\n.eventP[data-v-8196583e] {\n    font-weight: bold;\n    margin-top: 5px;\n}\n.qty[data-v-8196583e] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    padding: 20px;\n    border: 1px solid #cecbcb;\n    border-radius: 6px;\n    -webkit-box-shadow: 4px 6px 2px -3px #cecbcb;\n            box-shadow: 4px 6px 2px -3px #cecbcb;\n    border-left: 10px solid #a3c2dc !important;\n}\n.product-dialog[data-v-8196583e] {\n    max-width: 60%;\n}\n.checkoutEvent[data-v-8196583e] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    padding: 20px;\n    margin-top: 200px;\n}\n.location[data-v-8196583e] {\n    margin-top:30px;\n}\n.col8description[data-v-8196583e] {\n    padding: 80px;\n}\n.btn-register[data-v-8196583e] {\n    background-color: #a3c2dc !important;\n    padding: 10px 100px;\n}\n.col12[data-v-8196583e] {\n    text-align: right;\n    border-top: 1px groove #ececec;\n    padding-top: 20px;\n    border-bottom: 1px groove #ececec;\n    padding-bottom: 20px;\n}\n.eventPrice[data-v-8196583e] {\n    margin-top: 30px;\n}\n.eventTitle[data-v-8196583e] {\n    font-weight:bold;\n    font-size: 25px;\n    margin-top: 20px;\n}\n.col4[data-v-8196583e] {\n    font-size: 18px;\n    padding: 50px 0px;\n}\n.col8[data-v-8196583e] {\n    padding: 0px !important;\n}\n.eventDi[data-v-8196583e] {\n    margin-top: -360px;\n    position: relative;\n    width: 83%;\n    margin-left: auto;\n    margin-right: auto;\n    background: #ffffff;\n    border-radius: 5px;\n    border: 1px solid #dedede;\n    -webkit-box-shadow: 2px 0px 4px 1px #dedede;\n            box-shadow: 2px 0px 4px 1px #dedede;\n}\n.eventBanner[data-v-8196583e] {\n    background: #a3c2dc;\n    padding: 200px;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-event-page-component",
    data: function data() {
        return {
            id: this.$route.params.id,
            event: {},
            epiFade: true,
            epiModal: true,
            selectCheck: true,
            quantity: '',
            eventsCount: '',
            startDay: '',
            eventStart: '',
            eventStartEnd: '',
            eventEndEnd: '',
            endDay: '',
            price: '',
            eventEnd: '',
            eventSchedule: [],
            authenticate: false,
            loginCart: [],
            user: {},
            anonymousCart: [],
            realPrice: ''

        };
    },
    mounted: function mounted() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.user = user;
            this.authenticate = true;
        }
        axios.get('/api/event/' + this.id).then(function (response) {
            if (response.status === 200) {
                console.log(response);
                _this.event = response.data.data[0].events;
                _this.eventSchedule = response.data.data[0].eventSchedule;
                _this.event.price = response.data.data[0].price;
                _this.event.uid = response.data.data[0].id;
                _this.event.coverImage = response.data.data[0].coverImage;
                _this.quantity = parseInt(response.data.data[0].events.quantity);
                _this.startDay = _this.event.event_schedule_time[0].startDay;
                _this.eventStart = _this.event.event_schedule_time[0].eventStart;
                _this.eventEnd = _this.event.event_schedule_time[0].eventEnd;
                _this.endDay = _this.event.event_schedule_time[0].endDay;
                _this.eventStartEnd = _this.event.event_schedule_time[0].eventStartEnd;
                _this.eventEndEnd = _this.event.event_schedule_time[0].eventEndEnd;
                _this.price = _this.event.price;
            }
        }).catch(function (error) {
            console.log(error);
        });
    },

    methods: {
        changeEventCount: function changeEventCount(count) {
            //console.log(count);
            this.price = this.realPrice = this.eventsCount * this.event.price;
        },
        openModal: function openModal() {
            this.epiFade = false;
        },
        addToCart: function addToCart(id) {
            if (this.eventsCount === '') {
                this.$toasted.error('You must select at least one quantity');
            } else {
                console.log(id);
                var cart = {
                    productId: id,
                    price: this.realPrice,
                    free: isNaN(this.event.price) ? 'Y' : 'N',
                    quantity: this.quantity,
                    prodType: 'Events',
                    quantities: this.eventsCount
                };

                console.log(cart);
                if (this.authenticate === false) {
                    this.anonymousCart.push(cart);
                    if (JSON.parse(localStorage.getItem('userCart')) === null) {
                        localStorage.setItem('userCart', JSON.stringify(this.anonymousCart));
                        var sessionCart = JSON.parse(localStorage.getItem('userCart'));
                        var cartCount = sessionCart.length;
                        this.$emit('getCartCount', cartCount);
                        this.$notify({
                            group: 'cart',
                            title: this.event.title,
                            text: 'Successfully <i>Added</i> to Cart!'
                        });
                    } else if (JSON.parse(localStorage.getItem('userCart')) !== null) {
                        var _sessionCart = JSON.parse(localStorage.getItem('userCart'));
                        var ss = _sessionCart.length;
                        _sessionCart[ss] = cart;
                        localStorage.setItem('userCart', JSON.stringify(_sessionCart));
                        var a = JSON.parse(localStorage.getItem('userCart'));
                        var aCount = a.length;
                        this.$emit('getCartCount', aCount);
                        this.$notify({
                            group: 'cart',
                            title: this.event.title,
                            text: 'Successfully Added to Cart!'
                        });
                    }
                } else {

                    this.addCart(cart);
                }
            }
        },
        addCart: function addCart(cart) {
            var _this2 = this;

            axios.post('/api/cart', JSON.parse(JSON.stringify(cart)), { headers: { "Authorization": 'Bearer ' + this.user.access_token } }).then(function (response) {
                if (response.status === 201) {
                    _this2.loginCart.push(response.data);
                    var aCount = _this2.loginCart.length;
                    _this2.$emit('getCartCount', aCount);
                    _this2.$notify({
                        group: 'cart',
                        title: _this2.event.title,
                        text: '<p>Successfully <b>Added</b> to Cart!<p><p><a href="/indec/products/cart">Proceed to Checkout</router-link></p>'
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),

/***/ 1514:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "eventBanner" }),
    _vm._v(" "),
    _c("div", { staticClass: "row eventDi" }, [
      _c(
        "div",
        {
          class: { modal: _vm.epiModal, fade: _vm.epiFade },
          attrs: {
            id: "exampleModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalLabel",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog product-dialog",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", [
                    _vm._v("Sales end on " + _vm._s(_vm.event.endDate))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "qty" }, [
                    _c("div", [
                      _c("div", [
                        _c("p", [_vm._v("General Admission")]),
                        _vm._v(" "),
                        _c("span", [
                          _vm._v(
                            _vm._s(
                              _vm.event.price
                                ? "₦" + _vm.event.price + ".00"
                                : "FREE"
                            ) +
                              " | " +
                              _vm._s(_vm.event.quantity) +
                              " Remaining"
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.eventsCount,
                              expression: "eventsCount"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: [
                              function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.eventsCount = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              },
                              function($event) {
                                return _vm.changeEventCount(_vm.eventsCount)
                              }
                            ]
                          }
                        },
                        _vm._l(_vm.quantity, function(events, index) {
                          return _c("option", [_vm._v(_vm._s(events))])
                        }),
                        0
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "checkoutEvent" }, [
                    _vm.eventsCount
                      ? _c("div", { staticClass: "eventP" }, [
                          _vm._v("QTY: " + _vm._s(_vm.eventsCount))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "eventP" }, [
                      _vm._v(
                        _vm._s(_vm.price ? "₦" + _vm.price + ".00" : "FREE")
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-register",
                          on: {
                            click: function($event) {
                              return _vm.addToCart(_vm.event.uid)
                            }
                          }
                        },
                        [_vm._v("Checkout")]
                      )
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-8 col8" }, [
        _c("img", { attrs: { src: _vm.event.coverImage } })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4 col4" }, [
        _c("div", { staticClass: "eventMonth" }, [
          _vm._v(_vm._s(_vm._f("moment")(_vm.event.date, "MMM")))
        ]),
        _vm._v(" "),
        _c("div", [_vm._v(_vm._s(_vm._f("moment")(_vm.event.date, "D")))]),
        _vm._v(" "),
        _c("div", { staticClass: "eventTitle" }, [
          _vm._v(_vm._s(_vm.event.title))
        ]),
        _vm._v(" "),
        _c("small", [_vm._v(_vm._s(_vm.event.convenerName))]),
        _vm._v(" "),
        _c("div", { staticClass: "eventPrice" }, [
          _vm._v(
            _vm._s(_vm.event.price ? "₦" + _vm.event.price + ".00" : "FREE")
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 col12" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-register",
            attrs: { "data-toggle": "modal", "data-target": "#exampleModal" },
            on: {
              click: function($event) {
                return _vm.openModal()
              }
            }
          },
          [_vm._v("Register")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-8 col8description" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("div", {
              domProps: { innerHTML: _vm._s(_vm.event.description) }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 col8description" }, [
            _c("div", [
              _c("div", [_vm._v("Date And Time")]),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _c("div", [
                _vm._v(_vm._s(_vm._f("moment")(_vm.startDay, "MMM Do YYYY")))
              ]),
              _vm._v(" "),
              _c("div", [
                _vm._v(
                  _vm._s(_vm.eventStart) +
                    " PM - " +
                    _vm._s(_vm.eventEnd) +
                    " PM"
                )
              ]),
              _vm._v(" "),
              _vm._m(3),
              _vm._v(" "),
              _c("div", [
                _vm._v(_vm._s(_vm._f("moment")(_vm.endDay, "MMM Do YYYY")))
              ]),
              _vm._v(" "),
              _c("div", [
                _vm._v(
                  _vm._s(_vm.eventStartEnd) +
                    " PM - " +
                    _vm._s(_vm.eventEndEnd) +
                    " PM"
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", [
              _c(
                "select",
                { staticClass: "form-control" },
                [
                  _c("option", [_vm._v("Select Other Date")]),
                  _vm._v(" "),
                  _vm._l(_vm.eventSchedule, function(eventSchedules, index) {
                    return _vm.eventSchedule.length > 1
                      ? _c(
                          "option",
                          {
                            class: {
                              selectCheck: index === 0 ? _vm.selectCheck : ""
                            }
                          },
                          [
                            _vm._v(
                              "\n                                " +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.startDay,
                                    "ddd MMM Do"
                                  )
                                ) +
                                "(" +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.eventStart,
                                    "h:mm:ss a"
                                  )
                                ) +
                                "-" +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.eventEnd,
                                    "h:mm:ss a"
                                  )
                                ) +
                                ")-" +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.endDay,
                                    "ddd MMM Do"
                                  )
                                ) +
                                "(" +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.eventStartEnd,
                                    "h:mm:ss a"
                                  )
                                ) +
                                "-" +
                                _vm._s(
                                  _vm._f("moment")(
                                    eventSchedules.eventEndEnd,
                                    "h:mm:ss a"
                                  )
                                ) +
                                ")\n                            "
                            )
                          ]
                        )
                      : _vm._e()
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "location" }, [
              _c("div", [_vm._v("Location")]),
              _vm._v(" "),
              _c("div", [
                _vm._v(
                  _vm._s(_vm.event.address) +
                    ", " +
                    _vm._s(_vm.event.city) +
                    ", " +
                    _vm._s(_vm.event.state) +
                    ", " +
                    _vm._s(_vm.event.country)
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12 center" }, [
            _vm._v(_vm._s(_vm.event.convenerName))
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-12 center" }, [
            _vm._v(_vm._s(_vm.event.aboutConvener))
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-12 center" }, [
            _vm._v(
              _vm._s(_vm.event.address) +
                ", " +
                _vm._s(_vm.event.city) +
                ", " +
                _vm._s(_vm.event.state) +
                ", " +
                _vm._s(_vm.event.country)
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("b", [_vm._v("Description")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("b", [_vm._v("FROM:")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [_c("b", [_vm._v("TO:")])])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-8196583e", module.exports)
  }
}

/***/ }),

/***/ 578:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1511)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1513)
/* template */
var __vue_template__ = __webpack_require__(1514)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-8196583e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userEventPageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8196583e", Component.options)
  } else {
    hotAPI.reload("data-v-8196583e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});