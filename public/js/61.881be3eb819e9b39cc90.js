webpackJsonp([61],{

/***/ 1352:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1353);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("6b5bb0da", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45c647e8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubscriptionComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45c647e8\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./adminSubscriptionComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1353:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-45c647e8] {\n    height: 100vh;\n}\n.sub_title[data-v-45c647e8] {\n    background: #a3c2dc;\n    color: #ffffff;\n    text-transform: capitalize;\n    font-weight: bold;\n    padding: 10px 5px;\n    border-top-right-radius: 5px;\n    border-top-left-radius: 5px;\n}\n.sub_price[data-v-45c647e8] {\n    padding: 5px 10px;\n    font-weight: bold;\n    background: #ffffff;\n}\n.sub_info[data-v-45c647e8] {\n    background: #ffffff;\n    border-top: 1px solid #000000;\n    padding: 5px 10px;\n}\n.sub_info p[data-v-45c647e8] {\n    margin: 10px;\n}\n.main-page[data-v-45c647e8] {\n    padding: 40px 20px;\n    max-height: 100vh;\n    height: 100vh;\n    overflow: scroll;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 1354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin-subscription-component",
    components: {
        'app-editor': __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]
    },
    data: function data() {
        return {
            token: '',
            subscriptionOne: {
                title: '',
                price: '',
                duration: '',
                moreInfo: ''
            },
            subscriptionTwo: {
                title: '',
                price: '',
                duration: '',
                moreInfo: ''
            },
            subscriptionThree: {
                title: '',
                price: '',
                duration: '',
                moreInfo: ''
            }
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (localStorage.getItem('authAdmin')) {
            var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
            this.token = authAdmin.access_token;
            axios.get('api/subscription', { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    response.data.forEach(function (item) {
                        if (item.level === '1') {
                            _this.subscriptionOne.title = item.title;
                            _this.subscriptionOne.price = item.price;
                            _this.subscriptionOne.duration = item.duration;
                            _this.subscriptionOne.moreInfo = item.moreInfo;
                        } else if (item.level === '2') {
                            _this.subscriptionTwo.title = item.title;
                            _this.subscriptionTwo.price = item.price;
                            _this.subscriptionTwo.duration = item.duration;
                            _this.subscriptionTwo.moreInfo = item.moreInfo;
                        } else if (item.level === '3') {
                            _this.subscriptionThree.title = item.title;
                            _this.subscriptionThree.price = item.price;
                            _this.subscriptionThree.duration = item.duration;
                            _this.subscriptionThree.moreInfo = item.moreInfo;
                        }
                    });
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/admin/auth/manage');
        }
    },

    methods: {
        subscriptionCreate: function subscriptionCreate(params) {
            switch (params) {
                case 'levelOne':
                    this.subscriptionOne.level = '1';
                    this.submitSubscription(this.subscriptionOne);
                    break;
                case 'levelTwo':
                    this.subscriptionTwo.level = '2';
                    this.submitSubscription(this.subscriptionTwo);
                    break;
                case 'levelThree':
                    this.subscriptionThree.level = '3';
                    this.submitSubscription(this.subscriptionThree);
                    break;
                default:
                    return false;
            }
        },
        submitSubscription: function submitSubscription(data) {
            var _this2 = this;

            axios.post('api/subscription', JSON.parse(JSON.stringify(data)), { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200 || 201) {
                    _this2.$toasted.success('Subscription successfully updated');
                } else {
                    _this2.$toasted.error('Subscription cannot be updated');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),

/***/ 1355:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionOne.title,
                  expression: "subscriptionOne.title"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter Subscription Title" },
              domProps: { value: _vm.subscriptionOne.title },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionOne, "title", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionOne.price,
                  expression: "subscriptionOne.price"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Price" },
              domProps: { value: _vm.subscriptionOne.price },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionOne, "price", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionOne.duration,
                  expression: "subscriptionOne.duration"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter duration in days" },
              domProps: { value: _vm.subscriptionOne.duration },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionOne, "duration", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "form-group" },
            [
              _c("app-editor", {
                staticClass: "form-control",
                attrs: {
                  apiKey: "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3"
                },
                model: {
                  value: _vm.subscriptionOne.moreInfo,
                  callback: function($$v) {
                    _vm.$set(_vm.subscriptionOne, "moreInfo", $$v)
                  },
                  expression: "subscriptionOne.moreInfo"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn",
                on: {
                  click: function($event) {
                    return _vm.subscriptionCreate("levelOne")
                  }
                }
              },
              [_vm._v("Subscription")]
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionTwo.title,
                  expression: "subscriptionTwo.title"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter Subscription Title" },
              domProps: { value: _vm.subscriptionTwo.title },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionTwo, "title", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionTwo.price,
                  expression: "subscriptionTwo.price"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Price" },
              domProps: { value: _vm.subscriptionTwo.price },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionTwo, "price", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionTwo.duration,
                  expression: "subscriptionTwo.duration"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter duration in days" },
              domProps: { value: _vm.subscriptionTwo.duration },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionTwo, "duration", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "form-group" },
            [
              _c("app-editor", {
                staticClass: "form-control",
                attrs: {
                  apiKey: "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3"
                },
                model: {
                  value: _vm.subscriptionTwo.moreInfo,
                  callback: function($$v) {
                    _vm.$set(_vm.subscriptionTwo, "moreInfo", $$v)
                  },
                  expression: "subscriptionTwo.moreInfo"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn",
                on: {
                  click: function($event) {
                    return _vm.subscriptionCreate("levelTwo")
                  }
                }
              },
              [_vm._v("Subscription")]
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionThree.title,
                  expression: "subscriptionThree.title"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter Subscription Title" },
              domProps: { value: _vm.subscriptionThree.title },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionThree, "title", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionThree.price,
                  expression: "subscriptionThree.price"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Price" },
              domProps: { value: _vm.subscriptionThree.price },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.subscriptionThree, "price", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.subscriptionThree.duration,
                  expression: "subscriptionThree.duration"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text", placeholder: "Enter duration in days" },
              domProps: { value: _vm.subscriptionThree.duration },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.subscriptionThree,
                    "duration",
                    $event.target.value
                  )
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "form-group" },
            [
              _c("app-editor", {
                staticClass: "form-control",
                attrs: {
                  apiKey: "b2xt6tkzh0bcxra613xpq9319rtgc3nfwqbzh8tn6tckbgv3"
                },
                model: {
                  value: _vm.subscriptionThree.moreInfo,
                  callback: function($$v) {
                    _vm.$set(_vm.subscriptionThree, "moreInfo", $$v)
                  },
                  expression: "subscriptionThree.moreInfo"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c(
              "button",
              {
                staticClass: "btn",
                on: {
                  click: function($event) {
                    return _vm.subscriptionCreate("levelThree")
                  }
                }
              },
              [_vm._v("Subscription")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "sub" }, [
            _vm.subscriptionOne.title
              ? _c("div", { staticClass: "sub_title" }, [
                  _vm._v(_vm._s(_vm.subscriptionOne.title))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionOne.price
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v("₦" + _vm._s(_vm.subscriptionOne.price) + ".00")
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionOne.duration
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v(_vm._s(_vm.subscriptionOne.duration))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionOne.moreInfo
              ? _c("div", {
                  staticClass: "sub_info",
                  domProps: { innerHTML: _vm._s(_vm.subscriptionOne.moreInfo) }
                })
              : _vm._e()
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "sub" }, [
            _vm.subscriptionTwo.title
              ? _c("div", { staticClass: "sub_title" }, [
                  _vm._v(_vm._s(_vm.subscriptionTwo.title))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionTwo.price
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v("₦" + _vm._s(_vm.subscriptionTwo.price) + ".00")
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionTwo.duration
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v(_vm._s(_vm.subscriptionTwo.duration))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionTwo.moreInfo
              ? _c("div", {
                  staticClass: "sub_info",
                  domProps: { innerHTML: _vm._s(_vm.subscriptionTwo.moreInfo) }
                })
              : _vm._e()
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-4" }, [
          _c("div", { staticClass: "sub" }, [
            _vm.subscriptionThree.title
              ? _c("div", { staticClass: "sub_title" }, [
                  _vm._v(_vm._s(_vm.subscriptionThree.title))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionThree.price
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v("₦" + _vm._s(_vm.subscriptionThree.price) + ".00")
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionThree.duration
              ? _c("div", { staticClass: "sub_price" }, [
                  _vm._v(_vm._s(_vm.subscriptionThree.duration))
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.subscriptionThree.moreInfo
              ? _c("div", {
                  staticClass: "sub_info",
                  domProps: {
                    innerHTML: _vm._s(_vm.subscriptionThree.moreInfo)
                  }
                })
              : _vm._e()
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-45c647e8", module.exports)
  }
}

/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1352)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1354)
/* template */
var __vue_template__ = __webpack_require__(1355)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-45c647e8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/adminSubscriptionComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45c647e8", Component.options)
  } else {
    hotAPI.reload("data-v-45c647e8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ })

});