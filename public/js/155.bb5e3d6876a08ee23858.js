webpackJsonp([155],{

/***/ 1340:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1341);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("5342295a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59a4d479\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./showAdminProductComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-59a4d479\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./showAdminProductComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1341:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.stDiv[data-v-59a4d479] {\n    float: right;\n}\n.moreProductDetail[data-v-59a4d479] {\n    text-align: center;\n    padding: 40px;\n}\n.main-page[data-v-59a4d479] {\n    background-color: #ffffff;\n     padding: 30px;\n    width: 70%;\n    margin-top: 28px;\n    margin-bottom: 28px;\n    margin-right: 28px;\n    margin-left: auto;\n}\n.productAuthor[data-v-59a4d479] {\n    float: left;\n    margin-right: 200px;\n}\n.hardCover[data-v-59a4d479], .softCover[data-v-59a4d479] {\n    border: 2px solid #4c4c4c;\n    max-width: 25%;\n    padding: 11px;\n}\n.customTab[data-v-59a4d479] {\n    text-align: center;\n    border: 2px solid #f5f5f5;\n    padding: 20px;\n    text-transform: uppercase;\n    margin: 40px;\n}\n.proReview[data-v-59a4d479] {\n    margin-left: 50px;\n    margin-right: 50px;\n}\n.divC[data-v-59a4d479] {\n    font-size: 40px;\n    margin: 17px 0 50px 0;\n}\n.imgFullSide[data-v-59a4d479] {\n    float:left;\n    height: 50%;\n    width: 70%;\n}\n.videoList[data-v-59a4d479] {\n    padding: 3px 5px;\n    border: 1px solid black;\n    border-radius: 4px;\n}\n", ""]);

// exports


/***/ }),

/***/ 1342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            id: this.$route.params.id,
            products: {},
            isActive: true,
            productDetails: true,
            tableOfContent: false,
            checkBook: false,
            token: '',
            storeName: '',
            categoryName: '',
            excerpts: false,
            aboutAuthor: false,
            linkTag: false,
            price: '',
            title: 'Hard Copy',
            dis: false,
            verifyProducts: 'Verify Product'
        };
    },
    mounted: function mounted() {
        var _this = this;

        var authAdmin = JSON.parse(localStorage.getItem('authAdmin'));
        if (authAdmin != null) {
            this.token = authAdmin.access_token;
            axios.get('/api/product/' + this.id, { headers: { "Authorization": 'Bearer ' + authAdmin.access_token } }).then(function (response) {

                if (response.status === 200) {
                    var emptyObject = {};
                    console.log(response.data.data);
                    switch (response.data.data.prodType) {
                        case 'Books':
                            _this.$set(response.data.data.books, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.books, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.books, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.books, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.books, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.books, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.books, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.books, 'audioPrice', null);
                            _this.$set(response.data.data.books, 'videoPrice', null);
                            _this.$set(response.data.data.books, 'webinarPrice', null);
                            _this.$set(response.data.data.books, 'streamPrice', null);
                            _this.$set(response.data.data.books, 'subscribePrice', null);
                            emptyObject = response.data.data.books;
                            console.log('fff');
                            break;
                        case 'White Paper':
                            _this.$set(response.data.data.whitePaper, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.whitePaper, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.whitePaper, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.whitePaper, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.whitePaper, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.whitePaper, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.whitePaper, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.whitePaper, 'audioPrice', null);
                            _this.$set(response.data.data.whitePaper, 'videoPrice', null);
                            _this.$set(response.data.data.whitePaper, 'webinarPrice', null);
                            _this.$set(response.data.data.whitePaper, 'streamPrice', null);
                            _this.$set(response.data.data.whitePaper, 'subscribePrice', null);
                            emptyObject = response.data.data.whitePaper;
                            break;
                        case 'Market Reports':
                            _this.$set(response.data.data.marketReport, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.marketReport, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.marketReport, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.marketReport, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.marketReport, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.marketReport, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.marketReport, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.marketReport, 'audioPrice', null);
                            _this.$set(response.data.data.marketReport, 'videoPrice', null);
                            _this.$set(response.data.data.marketReport, 'webinarPrice', null);
                            _this.$set(response.data.data.marketReport, 'streamPrice', null);
                            _this.$set(response.data.data.marketReport, 'subscribePrice', null);
                            emptyObject = response.data.data.marketReport;
                            break;
                        case 'Market Research':
                            _this.$set(response.data.data.marketResearch, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.marketResearch, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.marketResearch, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.marketResearch, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.marketResearch, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.marketResearch, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.marketResearch, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.marketResearch, 'audioPrice', null);
                            _this.$set(response.data.data.marketResearch, 'videoPrice', null);
                            _this.$set(response.data.data.marketResearch, 'webinarPrice', null);
                            _this.$set(response.data.data.marketResearch, 'streamPrice', null);
                            _this.$set(response.data.data.marketResearch, 'subscribePrice', null);
                            emptyObject = response.data.data.marketResearch;
                            break;
                        case 'Webinar':
                            _this.$set(response.data.data.webinar, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.webinar, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.webinar, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.webinar, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.webinar, 'readOnlinePrice', null);
                            _this.$set(response.data.data.webinar, 'hardCopyPrice', null);
                            _this.$set(response.data.data.webinar, 'softCopyPrice', null);
                            _this.$set(response.data.data.webinar, 'audioPrice', null);
                            _this.$set(response.data.data.webinar, 'webinarPrice', response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                            _this.$set(response.data.data.webinar, 'videoPrice', null);
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Podcast':
                            _this.$set(response.data.data.webinar, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.webinar, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.webinar, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.webinar, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.webinar, 'readOnlinePrice', response.data.data.readOnlinePrice);
                            _this.$set(response.data.data.webinar, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.webinar, 'softCopyPrice', null);
                            _this.$set(response.data.data.webinar, 'audioPrice', response.data.data.audioPrice ? response.data.data.audioPrice : null);
                            _this.$set(response.data.data.webinar, 'webinarPrice', response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                            _this.$set(response.data.data.webinar, 'streamPrice', response.data.data.streamPrice ? response.data.data.streamPrice : null);
                            _this.$set(response.data.data.webinar, 'subscribePrice', response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
                            _this.$set(response.data.data.webinar, 'videoPrice', response.data.data.videoPrice ? response.data.data.videoPrice : null);
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Videos':
                            _this.$set(response.data.data.webinar, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.webinar, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.webinar, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.webinar, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.webinar, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.webinar, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.webinar, 'softCopyPrice', null);
                            _this.$set(response.data.data.webinar, 'audioPrice', null);
                            _this.$set(response.data.data.webinar, 'webinarPrice', response.data.data.webinarPrice ? response.data.data.webinarPrice : null);
                            _this.$set(response.data.data.webinar, 'streamPrice', response.data.data.streamPrice ? response.data.data.streamPrice : null);
                            _this.$set(response.data.data.webinar, 'subscribePrice', response.data.data.subscribePrice ? response.data.data.subscribePrice : null);
                            _this.$set(response.data.data.webinar, 'videoPrice', response.data.data.videoPrice ? response.data.data.videoPrice : null);
                            emptyObject = response.data.data.webinar;
                            break;
                        case 'Journals':
                            _this.$set(response.data.data.journal, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.journal, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.journal, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.journal, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.journal, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.journal, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.journal, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.journal, 'audioPrice', null);
                            _this.$set(response.data.data.journal, 'videoPrice', null);
                            _this.$set(response.data.data.journal, 'webinarPrice', null);
                            _this.$set(response.data.data.journal, 'streamPrice', null);
                            _this.$set(response.data.data.journal, 'subscribePrice', null);
                            emptyObject = response.data.data.journal;
                            break;
                        case 'Courses':
                            _this.$set(response.data.data.courses, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.courses, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.courses, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.courses, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.courses, 'readOnlinePrice', response.data.data.readOnlinePrice ? response.data.data.readOnlinePrice : null);
                            _this.$set(response.data.data.courses, 'hardCopyPrice', response.data.data.hardCopyPrice ? response.data.data.hardCopyPrice : null);
                            _this.$set(response.data.data.courses, 'softCopyPrice', response.data.data.softCopyPrice ? response.data.data.softCopyPrice : null);
                            _this.$set(response.data.data.courses, 'audioPrice', response.data.data.audioPrice ? response.data.data.audioPrice : null);
                            _this.$set(response.data.data.courses, 'videoPrice', response.data.data.videoPrice ? response.data.data.videoPrice : null);
                            _this.$set(response.data.data.courses, 'webinarPrice', null);
                            _this.$set(response.data.data.courses, 'streamPrice', null);
                            _this.$set(response.data.data.courses, 'subscribePrice', null);
                            emptyObject = response.data.data.courses;
                            break;
                        case 'Events':
                            _this.$set(response.data.data.events, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.events, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.events, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.events, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.events, 'eventPrice', response.data.data.eventPrice);
                            _this.$set(response.data.data.events, 'readOnlinePrice', null);
                            _this.$set(response.data.data.events, 'hardCopyPrice', null);
                            _this.$set(response.data.data.events, 'softCopyPrice', null);
                            _this.$set(response.data.data.events, 'subscribePrice', null);
                            _this.$set(response.data.data.events, 'audioPrice', null);
                            _this.$set(response.data.data.events, 'videoPrice', null);
                            _this.$set(response.data.data.events, 'webinarPrice', null);
                            _this.$set(response.data.data.events, 'streamPrice', null);
                            emptyObject = response.data.data.events;
                            break;
                        case 'Articles':
                            _this.$set(response.data.data.articles, 'vendor', response.data.data.vendor);
                            _this.$set(response.data.data.articles, 'uniId', response.data.data.id);
                            _this.$set(response.data.data.articles, 'coverImage', response.data.data.coverImage);
                            _this.$set(response.data.data.articles, 'verify', response.data.data.verify);
                            _this.$set(response.data.data.articles, 'readOnlinePrice', null);
                            _this.$set(response.data.data.articles, 'hardCopyPrice', null);
                            _this.$set(response.data.data.articles, 'softCopyPrice', null);
                            _this.$set(response.data.data.articles, 'audioPrice', null);
                            _this.$set(response.data.data.articles, 'videoPrice', null);
                            _this.$set(response.data.data.articles, 'webinarPrice', null);
                            _this.$set(response.data.data.articles, 'streamPrice', null);
                            emptyObject = response.data.data.articles;
                            break;
                        default:
                            console.log('ffsss');
                            return false;
                    }
                    console.log(emptyObject);
                    _this.products = emptyObject;
                    // this.products = response.data.data;
                    if (response.data.data.softCopyPrice || response.data.data.hardCopyPrice) {
                        _this.checkBook = true;
                    }
                    _this.isActive = false;
                    _this.linkTag = true;
                    _this.dis = true;
                }
            }).catch(function (error) {
                console.log(error);
            });
        } else {
            this.$router.push('/vendor/auth');
        }
    },

    methods: {
        verifyAdminProduct: function verifyAdminProduct(id) {
            var _this2 = this;

            axios.get('/api/get/verify-bizguruh-product/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    if (response.data.verify) {
                        _this2.$toasted.success('Status successfully change');
                        _this2.verifyProducts = "Unverify Product";
                    }
                } else {
                    _this2.$toasted.success("Successfully unverified");
                    _this2.verifyProducts = "Verify Product";
                }
            });
        },
        swipeImage: function swipeImage(index) {
            console.log(index);
            for (var i = 0; i < this.products.productImage.length; i++) {
                this.imgSrc = this.products.productImage[index].image;
            }
        },
        changeTabsContent: function changeTabsContent(tabArr) {
            switch (tabArr) {
                case 'product detail':
                    this.productDetails = true;
                    this.tableOfContent = false;
                    this.excerpts = false;
                    this.aboutAuthor = false;
                    break;
                case 'About the author':
                    this.productDetails = false;
                    this.tableOfContent = false;
                    this.excerpts = false;
                    this.aboutAuthor = true;
                    break;
                case 'table of content':
                    this.productDetails = false;
                    this.tableOfContent = true;
                    this.excerpts = false;
                    this.aboutAuthor = false;
                    break;
                case 'read an excerpts':
                    this.productDetails = false;
                    this.tableOfContent = false;
                    this.excerpts = true;
                    this.aboutAuthor = false;
                    break;
                default:
                    this.productDetails = false;
                    this.tableOfContent = false;
                    this.excerpts = false;
                    this.aboutAuthor = false;
            }
        },
        deleteProduct: function deleteProduct(id) {
            var _this3 = this;

            console.log(id);
            confirm('Are you sure?');
            axios.delete('/api/product/' + id, { headers: { "Authorization": 'Bearer ' + this.token } }).then(function (response) {
                if (response.status === 200) {
                    _this3.$router.push('/vendor/all');
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

});

/***/ }),

/***/ 1343:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.dis
    ? _c(
        "div",
        { staticClass: "main-page" },
        [
          _c("vue-loading", {
            attrs: {
              active: _vm.isActive,
              spinner: "bar-fade-scale",
              color: "#FF6700"
            }
          }),
          _vm._v(" "),
          _vm.linkTag
            ? _c(
                "router-link",
                {
                  staticClass: "btn btn-primary mb-4",
                  attrs: {
                    tag: "a",
                    to: {
                      name: "adminProductEditcategory",
                      params: { id: _vm.products.uniId }
                    }
                  }
                },
                [_vm._v("Edit")]
              )
            : _vm._e(),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-danger mb-4",
              on: {
                click: function($event) {
                  return _vm.deleteProduct(_vm.products.uniId)
                }
              }
            },
            [_vm._v("Delete")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-primary mb-4",
              on: {
                click: function($event) {
                  return _vm.verifyAdminProduct(_vm.products.uniId)
                }
              }
            },
            [_vm._v(_vm._s(_vm.verifyProducts))]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "stDiv" }, [
            _vm._v(
              "STATUS: " +
                _vm._s(_vm.products.verify === 1 ? "ACTIVE" : "INACTIVE")
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("img", {
                staticClass: "imgFullSide",
                attrs: { src: _vm.products.coverImage }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("h2", [_vm._v(_vm._s(_vm.products.title))]),
              _vm._v(" "),
              _vm.products.host
                ? _c("p", [
                    _vm._v(
                      " " +
                        _vm._s(_vm.products.host ? "Host:" : "") +
                        " " +
                        _vm._s(_vm.products.host)
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(_vm.products.level ? "Level: " : "") +
                    " " +
                    _vm._s(_vm.products.level)
                )
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(
                    _vm.products.certification
                      ? "Certification Avaliability: "
                      : ""
                  ) +
                    " " +
                    _vm._s(_vm.products.certification)
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "productAuthor" }, [
                _vm._v(
                  _vm._s(_vm.products.author ? "Author: " : "") +
                    " " +
                    _vm._s(_vm.products.author) +
                    "  "
                ),
                _vm.products.guest !== null
                  ? _c("span", [
                      _vm._v(
                        _vm._s(_vm.products.guest ? "Guest:" : "") +
                          " " +
                          _vm._s(_vm.products.guest)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" " + _vm._s(_vm.storeName))
              ]),
              _c("span", [_vm._v(_vm._s(_vm.categoryName))]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  _vm._s(_vm.products.publisher ? "Publisher: " : "") +
                    " " +
                    _vm._s(_vm.products.publisher)
                )
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("div", {}),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _vm.products.hardCopyPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "hardCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("hardCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.hardCopyPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Hard Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.softCopyPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.softCopyPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Digital Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.audioPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.audioPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Audio Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.readOnlinePrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("readOnline")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.readOnlinePrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Read Online")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.eventPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("eventPrice")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                     ₦" +
                            _vm._s(_vm.products.eventPrice) +
                            ".00\n                     "
                        ),
                        _c("p", [_vm._v("Event Price")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.videoPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("softCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.videoPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Video Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.webinarPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("webinarCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.webinarPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Webinar Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.streamPrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("streamCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.streamPrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Streaming Copy")])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.subscribePrice !== null
                  ? _c(
                      "div",
                      {
                        staticClass: "softCover col-md-6",
                        on: {
                          click: function($event) {
                            return _vm.togglePrice("subscribeCopy")
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                         ₦" +
                            _vm._s(_vm.products.subscribePrice) +
                            ".00\n                         "
                        ),
                        _c("p", [_vm._v("Subscribe Copy")])
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("hr")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _vm.checkBook
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("product detail")
                      }
                    }
                  },
                  [_vm._v("Product Details")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.aboutAuthor
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("About the author")
                      }
                    }
                  },
                  [_vm._v("About the Author")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.aboutThisAuthor
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("About the author")
                      }
                    }
                  },
                  [_vm._v("About the Author")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.tableOfContent
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("table of content")
                      }
                    }
                  },
                  [_vm._v("Table of Content")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.excerpts
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("read an excerpts")
                      }
                    }
                  },
                  [_vm._v("Read an Excerpts")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.products.description
              ? _c(
                  "div",
                  {
                    staticClass: "col-md-2 customTab",
                    on: {
                      click: function($event) {
                        return _vm.changeTabsContent("overview")
                      }
                    }
                  },
                  [_vm._v("Overview")]
                )
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm.checkBook
            ? _c("div", [
                _vm.products.fileType !== null
                  ? _c("p", [_vm._v("ISBN: " + _vm._s(_vm.products.isbn))])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.publisher !== null
                  ? _c("p", [
                      _vm._v("Publisher: " + _vm._s(_vm.products.publisher))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.publicationDate !== null
                  ? _c("p", [
                      _vm._v(
                        "Publication Date: " +
                          _vm._s(_vm.products.publisherDate)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.products.pageNo !== null
                  ? _c("p", [
                      _vm._v("Pages: " + _vm._s(_vm.products.noOfPages))
                    ])
                  : _vm._e()
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.aboutAuthor
            ? _c("div", [
                _vm._v("\n    " + _vm._s(_vm.products.aboutAuthor) + "\n    ")
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.aboutThisAuthor
            ? _c("div", [
                _vm._v(
                  "\n        " + _vm._s(_vm.products.aboutThisAuthor) + "\n    "
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.tableOfContent
            ? _c("div", [
                _vm._v(
                  "\n        " + _vm._s(_vm.products.tableOfContent) + "\n    "
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.excerpts
            ? _c("div", [
                _vm._v("\n        " + _vm._s(_vm.products.excerpts) + "\n    ")
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm.products.description
            ? _c("div", {
                domProps: { innerHTML: _vm._s(_vm.products.description) }
              })
            : _vm._e(),
          _vm._v(" "),
          _c("hr")
        ],
        1
      )
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("span", { staticClass: "proReview" }, [_vm._v("4 Customer Reviews")]),
      _c("span", { staticClass: "proEdi" }, [_vm._v("Editorial Review")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-59a4d479", module.exports)
  }
}

/***/ }),

/***/ 540:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1340)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1342)
/* template */
var __vue_template__ = __webpack_require__(1343)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-59a4d479"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/showAdminProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-59a4d479", Component.options)
  } else {
    hotAPI.reload("data-v-59a4d479", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});