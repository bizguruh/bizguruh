webpackJsonp([59],{

/***/ 1680:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1681);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("4c05b80a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45d99d5a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticlesForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-45d99d5a\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userArticlesForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1681:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-45d99d5a] {\n  padding-top: 80px;\n  text-align: left;\n}\n.card[data-v-45d99d5a] {\n  width: 100%;\n  background: #fff;\n  text-align: left;\n}\n.card-body[data-v-45d99d5a] {\n  border-top: 1px solid #ccc;\n  text-align: left;\n}\n.card-img-top[data-v-45d99d5a] {\n  margin: 10px auto;\n  width: 100px;\n}\n.btn-primary[data-v-45d99d5a] {\n  color: #fff;\n  background: #a4c2db !important;\n  padding: 5px 30px;\n  border: none !important;\n}\n.btn-primary[data-v-45d99d5a]:hover {\n  background: rgba(164, 194, 219, 0.5) !important;\n  border: none !important;\n}\n.btn-primary[data-v-45d99d5a]:active {\n  border: none !important;\n}\n#post[data-v-45d99d5a]{\n    text-align: center;\n    margin: 15px;\n}\n.form-group[data-v-45d99d5a]{\n    margin: 40px 0;\n}\n", ""]);

// exports


/***/ }),

/***/ 1682:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__ = __webpack_require__(690);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-article-form",
  data: function data() {
    return {
      articles: {
        title: '',
        description: '',
        featured: '',
        tags: '',
        writer: '',
        aboutWriter: '',
        media: '',
        imageName: 'Upload Image',
        imagePlaceholder: '/images/addImage.png',
        prodType: "Articles",
        coverImage: ''
      }

    };
  },

  components: {
    "app-editor": __WEBPACK_IMPORTED_MODULE_0__tinymce_tinymce_vue__["a" /* default */]
  },
  mounted: function mounted() {
    var user = localStorage.getItem('authUser');
    this.$refs;
  },

  methods: {
    getFileName: function getFileName(e, name) {
      var _this = this;

      switch (name) {
        case 'document':
          this.fileName = e.target.files[0].name;
          this.product.document = this.$refs.files.files[0];
          break;
        case 'image':
          var file = e.target.files[0];
          var reader = new FileReader();
          reader.onload = function (event) {
            _this.articles.imagePlaceholder = event.target.result;
            _this.articles.coverImage = event.target.result;
          };
          reader.readAsDataURL(file);

          break;
        default:
          return false;
      }
    },
    upload_handler: function upload_handler(blobInfo, success, failure) {
      var xhr, formData;
      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open("POST", "/api/image-upload");
      xhr.onload = function () {
        var json;

        if (xhr.status != 200) {
          failure("HTTP Error: " + xhr.status);
          return;
        }
        json = JSON.parse(xhr.responseText);

        if (!json || typeof json.location != "string") {
          failure("Invalid JSON: " + xhr.responseText);
          return;
        }
        success(json.location);
      };
      formData = new FormData();
      formData.append("file", blobInfo.blob());
      xhr.send(formData);
    },
    loadFile: function loadFile(cb, mt) {
      if (mt.filetype === "image") {
        var up = this.$refs.upload;

        up.onchange = function () {
          var file = this.files[0];

          var reader = new FileReader();
          reader.onload = function (e) {
            var id = "blobid" + new Date().getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(",")[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            /* call the callback and populate the Title field with the file name */
            cb(blobInfo.blobUri(), { title: file.name });
          };
          reader.readAsDataURL(file);
        };

        up.click();
      }
    },
    createNewProduct: function createNewProduct() {
      var _this2 = this;

      var user = localStorage.getItem('authUser');

      if (user !== null) {

        axios.post('/api/add/article', this.articles).then(function (response) {
          if (response.status === 200) {
            console.log(response);
            if (_this2.redirect === 'S') {
              _this2.$router.push('/admin/bizguruh-products');
              _this2.$toasted.success("Product successfully added");
            }_this2.$toasted.success("Draft product successfully added");
          } else {
            _this2.add = false;
            _this2.Submit = 'Submit';
            _this2.Submit = 'Submit';
          }
        }).catch(function (error) {
          for (var key in error.response.data.errors) {
            _this2.$toasted.error(error.response.data.errors[key][0]);
          }
          _this2.Submit = 'Submit';
        });
      } else {
        this.$toasted.error('Login to create article');
      }
    }
  }
});

/***/ }),

/***/ 1683:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "card" }, [
        _c("h2", { staticClass: "p-4" }, [_vm._v("Create Article")]),
        _vm._v(" "),
        _c("img", {
          staticClass: "card-img-top",
          attrs: {
            src:
              "https://www.freeiconspng.com/uploads/black-pencil-png-black-pencil-vector-8.png",
            width: "100",
            height: "100",
            alt: "pencil image"
          }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "title" } }, [_vm._v("Title")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.articles.title,
                  expression: "articles.title"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                name: "",
                id: "title",
                "aria-describedby": "helpId",
                placeholder: "Title"
              },
              domProps: { value: _vm.articles.title },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.articles, "title", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("input", {
            ref: "upload",
            staticClass: "hidden",
            attrs: { name: "image", type: "file", id: "upload" }
          }),
          _vm._v(" "),
          _c(
            "div",
            [
              _c("app-editor", {
                staticClass: "form-control",
                attrs: {
                  init: {
                    plugins:
                      "advlist autolink lists link image imagetools charmap print preview anchor textcolor insertdatetime media table paste code help wordcount",

                    toolbar:
                      "undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat |image help",
                    image_title: true,

                    height: 300,

                    file_picker_types: "image",
                    automatic_uploads: false,
                    relative_urls: false,
                    convert_urls: false,
                    images_upload_handler: function(
                      blobInfo,
                      success,
                      failure
                    ) {
                      _vm.upload_handler(blobInfo, success, failure)
                    },
                    file_picker_callback: function(callback, value, meta) {
                      _vm.loadFile(callback, value)
                    }
                  }
                },
                model: {
                  value: _vm.articles.description,
                  callback: function($$v) {
                    _vm.$set(_vm.articles, "description", $$v)
                  },
                  expression: "articles.description"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "articleImage" } }, [
              _vm._v("Article Image")
            ]),
            _vm._v(" "),
            _c("input", {
              ref: "articleImage",
              staticClass: "form-control",
              attrs: { type: "file", id: "articleImage" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "" } }, [_vm._v("Tag")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.articles.tags,
                  expression: "articles.tags"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                name: "",
                id: "",
                "aria-describedby": "helpId",
                placeholder: "Upload"
              },
              domProps: { value: _vm.articles.tags },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.articles, "tags", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group d-flex" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.articles.featured,
                  expression: "articles.featured"
                }
              ],
              staticClass: "mr-2",
              attrs: { type: "checkbox", value: "featured", id: "featured" },
              domProps: {
                checked: Array.isArray(_vm.articles.featured)
                  ? _vm._i(_vm.articles.featured, "featured") > -1
                  : _vm.articles.featured
              },
              on: {
                change: function($event) {
                  var $$a = _vm.articles.featured,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "featured",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.articles, "featured", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.articles,
                          "featured",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.articles, "featured", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "featured" } }, [_vm._v("Featured")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "writerName" } }, [
              _vm._v("Writer name")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.articles.writer,
                  expression: "articles.writer"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                name: "",
                id: "writerName",
                "aria-describedby": "helpId"
              },
              domProps: { value: _vm.articles.writer },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.articles, "writer", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "aboutWriter" } }, [
              _vm._v("About Writer")
            ]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.articles.aboutWriter,
                  expression: "articles.aboutWriter"
                }
              ],
              staticClass: "form-control",
              attrs: {
                type: "text",
                name: "",
                id: "aboutWriter",
                "aria-describedby": "helpId"
              },
              domProps: { value: _vm.articles.aboutWriter },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.articles, "aboutWriter", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group" }, [
            _c("label", [
              _c("img", {
                attrs: {
                  src: _vm.articles.imagePlaceholder,
                  width: "100",
                  height: "100"
                }
              }),
              _vm._v(" "),
              _c("input", {
                staticClass: "hide-input",
                attrs: { type: "file", id: "coverImage" },
                on: {
                  change: function($event) {
                    return _vm.getFileName($event, "image")
                  }
                }
              })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "form-group", attrs: { id: "post" } }, [
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.createNewProduct()
                  }
                }
              },
              [
                _c("i", {
                  staticClass: "fa fa-paper-plane",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(" Post\n      ")
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-45d99d5a", module.exports)
  }
}

/***/ }),

/***/ 618:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1680)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1682)
/* template */
var __vue_template__ = __webpack_require__(1683)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-45d99d5a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/userArticlesForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-45d99d5a", Component.options)
  } else {
    hotAPI.reload("data-v-45d99d5a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export bindHandlers */
/* unused harmony export bindModelHandlers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return uuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return isTextarea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return mergePlugins; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var validEvents = [
    'onActivate',
    'onAddUndo',
    'onBeforeAddUndo',
    'onBeforeExecCommand',
    'onBeforeGetContent',
    'onBeforeRenderUI',
    'onBeforeSetContent',
    'onBeforePaste',
    'onBlur',
    'onChange',
    'onClearUndos',
    'onClick',
    'onContextMenu',
    'onCopy',
    'onCut',
    'onDblclick',
    'onDeactivate',
    'onDirty',
    'onDrag',
    'onDragDrop',
    'onDragEnd',
    'onDragGesture',
    'onDragOver',
    'onDrop',
    'onExecCommand',
    'onFocus',
    'onFocusIn',
    'onFocusOut',
    'onGetContent',
    'onHide',
    'onInit',
    'onKeyDown',
    'onKeyPress',
    'onKeyUp',
    'onLoadContent',
    'onMouseDown',
    'onMouseEnter',
    'onMouseLeave',
    'onMouseMove',
    'onMouseOut',
    'onMouseOver',
    'onMouseUp',
    'onNodeChange',
    'onObjectResizeStart',
    'onObjectResized',
    'onObjectSelected',
    'onPaste',
    'onPostProcess',
    'onPostRender',
    'onPreProcess',
    'onProgressState',
    'onRedo',
    'onRemove',
    'onReset',
    'onSaveContent',
    'onSelectionChange',
    'onSetAttrib',
    'onSetContent',
    'onShow',
    'onSubmit',
    'onUndo',
    'onVisualAid'
];
var isValidKey = function (key) { return validEvents.indexOf(key) !== -1; };
var bindHandlers = function (initEvent, listeners, editor) {
    Object.keys(listeners)
        .filter(isValidKey)
        .forEach(function (key) {
        var handler = listeners[key];
        if (typeof handler === 'function') {
            if (key === 'onInit') {
                handler(initEvent, editor);
            }
            else {
                editor.on(key.substring(2), function (e) { return handler(e, editor); });
            }
        }
    });
};
var bindModelHandlers = function (ctx, editor) {
    var modelEvents = ctx.$props.modelEvents ? ctx.$props.modelEvents : null;
    var normalizedEvents = Array.isArray(modelEvents) ? modelEvents.join(' ') : modelEvents;
    var currentContent;
    ctx.$watch('value', function (val, prevVal) {
        if (editor && typeof val === 'string' && val !== currentContent && val !== prevVal) {
            editor.setContent(val);
            currentContent = val;
        }
    });
    editor.on(normalizedEvents ? normalizedEvents : 'change keyup undo redo', function () {
        currentContent = editor.getContent();
        ctx.$emit('input', currentContent);
    });
};
var initEditor = function (initEvent, ctx, editor) {
    var value = ctx.$props.value ? ctx.$props.value : '';
    var initialValue = ctx.$props.initialValue ? ctx.$props.initialValue : '';
    editor.setContent(value || initialValue);
    // checks if the v-model shorthand is used (which sets an v-on:input listener) and then binds either
    // specified the events or defaults to "change keyup" event and emits the editor content on that event
    if (ctx.$listeners.input) {
        bindModelHandlers(ctx, editor);
    }
    bindHandlers(initEvent, ctx.$listeners, editor);
};
var unique = 0;
var uuid = function (prefix) {
    var time = Date.now();
    var random = Math.floor(Math.random() * 1000000000);
    unique++;
    return prefix + '_' + random + unique + String(time);
};
var isTextarea = function (element) {
    return element !== null && element.tagName.toLowerCase() === 'textarea';
};
var normalizePluginArray = function (plugins) {
    if (typeof plugins === 'undefined' || plugins === '') {
        return [];
    }
    return Array.isArray(plugins) ? plugins : plugins.split(' ');
};
var mergePlugins = function (initPlugins, inputPlugins) {
    return normalizePluginArray(initPlugins).concat(normalizePluginArray(inputPlugins));
};


/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Editor__ = __webpack_require__(691);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__components_Editor__["a" /* Editor */]);


/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Editor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__TinyMCE__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Utils__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__ = __webpack_require__(694);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};




var scriptState = __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["a" /* create */]();
var renderInline = function (h, id, tagName) {
    return h(tagName ? tagName : 'div', {
        attrs: { id: id }
    });
};
var renderIframe = function (h, id) {
    return h('textarea', {
        attrs: { id: id },
        style: { visibility: 'hidden' }
    });
};
var initialise = function (ctx) { return function () {
    var finalInit = __assign({}, ctx.$props.init, { readonly: ctx.$props.disabled, selector: "#" + ctx.elementId, plugins: Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["c" /* mergePlugins */])(ctx.$props.init && ctx.$props.init.plugins, ctx.$props.plugins), toolbar: ctx.$props.toolbar || (ctx.$props.init && ctx.$props.init.toolbar), inline: ctx.inlineEditor, setup: function (editor) {
            ctx.editor = editor;
            editor.on('init', function (e) { return Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["a" /* initEditor */])(e, ctx, editor); });
            if (ctx.$props.init && typeof ctx.$props.init.setup === 'function') {
                ctx.$props.init.setup(editor);
            }
        } });
    if (Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["b" /* isTextarea */])(ctx.element)) {
        ctx.element.style.visibility = '';
    }
    Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().init(finalInit);
}; };
var Editor = {
    props: __WEBPACK_IMPORTED_MODULE_3__EditorPropTypes__["a" /* editorProps */],
    created: function () {
        this.elementId = this.$props.id || Object(__WEBPACK_IMPORTED_MODULE_2__Utils__["d" /* uuid */])('tiny-vue');
        this.inlineEditor = (this.$props.init && this.$props.init.inline) || this.$props.inline;
    },
    watch: {
        disabled: function () {
            this.editor.setMode(this.disabled ? 'readonly' : 'design');
        }
    },
    mounted: function () {
        this.element = this.$el;
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            initialise(this)();
        }
        else if (this.element && this.element.ownerDocument) {
            var doc = this.element.ownerDocument;
            var channel = this.$props.cloudChannel ? this.$props.cloudChannel : 'stable';
            var apiKey = this.$props.apiKey ? this.$props.apiKey : '';
            var url = "https://cloud.tinymce.com/" + channel + "/tinymce.min.js?apiKey=" + apiKey;
            __WEBPACK_IMPORTED_MODULE_0__ScriptLoader__["b" /* load */](scriptState, doc, url, initialise(this));
        }
    },
    beforeDestroy: function () {
        if (Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])() !== null) {
            Object(__WEBPACK_IMPORTED_MODULE_1__TinyMCE__["a" /* getTinymce */])().remove(this.editor);
        }
    },
    render: function (h) {
        return this.inlineEditor ? renderInline(h, this.elementId, this.$props.tagName) : renderIframe(h, this.elementId);
    }
};


/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return load; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils__ = __webpack_require__(677);
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

var injectScriptTag = function (scriptId, doc, url, callback) {
    var scriptTag = doc.createElement('script');
    scriptTag.type = 'application/javascript';
    scriptTag.id = scriptId;
    scriptTag.addEventListener('load', callback);
    scriptTag.src = url;
    if (doc.head) {
        doc.head.appendChild(scriptTag);
    }
};
var create = function () {
    return {
        listeners: [],
        scriptId: Object(__WEBPACK_IMPORTED_MODULE_0__Utils__["d" /* uuid */])('tiny-script'),
        scriptLoaded: false
    };
};
var load = function (state, doc, url, callback) {
    if (state.scriptLoaded) {
        callback();
    }
    else {
        state.listeners.push(callback);
        if (!doc.getElementById(state.scriptId)) {
            injectScriptTag(state.scriptId, doc, url, function () {
                state.listeners.forEach(function (fn) { return fn(); });
                state.scriptLoaded = true;
            });
        }
    }
};


/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getTinymce; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var getGlobal = function () { return (typeof window !== 'undefined' ? window : global); };
var getTinymce = function () {
    var global = getGlobal();
    return global && global.tinymce ? global.tinymce : null;
};


/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(32)))

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return editorProps; });
/**
 * Copyright (c) 2018-present, Ephox, Inc.
 *
 * This source code is licensed under the Apache 2 license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
var editorProps = {
    apiKey: String,
    cloudChannel: String,
    id: String,
    init: Object,
    initialValue: String,
    inline: Boolean,
    modelEvents: [String, Array],
    plugins: [String, Array],
    tagName: String,
    toolbar: [String, Array],
    value: String,
    disabled: Boolean
};


/***/ })

});