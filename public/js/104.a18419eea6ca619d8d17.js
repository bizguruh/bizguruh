webpackJsonp([104],{

/***/ 1436:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1437);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("699bbc91", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e80647ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userWishlistComponent.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e80647ca\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./userWishlistComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1437:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.course-Detail[data-v-e80647ca]{\n    padding:80px 60px;\n}\n.courseCover[data-v-e80647ca]{\n    height: 100px;\n    border-bottom: 1px solid #ccc;\npadding: 10px;\ntext-align: center;\n}\n.cover2[data-v-e80647ca]{\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    text-align: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.catImage[data-v-e80647ca]{\n    position: relative;\n    width:15%;\n    padding:10px;\n}\n.catImage img[data-v-e80647ca]{\n    width:60px;\n    height:60px;\n    cursor: pointer;\n}\n.type[data-v-e80647ca]{\n    width: 15%;\n    padding:10px;\n}\n.about[data-v-e80647ca]{\n    width:60%;\n    padding:10px;\n}\n.buttons[data-v-e80647ca]{\n    width:10%;\n    padding:10px;\n}\n.pTitle a[data-v-e80647ca]{\n    color: rgba(0, 0, 0, .84);\n     cursor: pointer;\n}\n.pTitle a[data-v-e80647ca]:hover{\n    color: #a4c2db;\n}\n#mainH[data-v-e80647ca]{\n    text-align:center;\n    margin-bottom:10px;\n}\n@media only screen and (max-width:425px){\n.desktop[data-v-e80647ca]{\n        display: none;\n}\n.type[data-v-e80647ca]{\n        font-size: 12px\n}\n.about[data-v-e80647ca]{\n        font-size: 12px\n}\n.course-Detail[data-v-e80647ca]{\n    padding:70px 10px !important;\n}\n.rowCat[data-v-e80647ca]{\n    text-align: center;\n}\n.catImage[data-v-e80647ca]{\n    padding: 10px 0;\n}\n}\n@media(max-width:425px){\n.desktop[data-v-e80647ca]{\n        display: none;\n}\n.course-Detail[data-v-e80647ca]{\n    padding:70px 10px !important;\n}\n.rowCat[data-v-e80647ca]{\n    text-align: center;\n}\n.catImage[data-v-e80647ca]{\n    padding: 10px 0;\n}\n}\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 1438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "user-wishlist-component",
    data: function data() {
        return {
            user_id: '',
            wishlists: []
        };
    },
    created: function created() {
        var _this = this;

        var user = JSON.parse(localStorage.getItem('authUser'));
        if (user !== null) {
            this.user_id = user.id;

            axios.get('/api/wishlists', { headers: { "Authorization": 'Bearer ' + user.access_token } }).then(function (response) {

                if (response.status === 200) {
                    _this.wishlists = response.data;
                    console.log("Data: created ->  this.wishlists", _this.wishlists);
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    },

    methods: {
        deleteWishlist: function deleteWishlist(id) {
            var _this2 = this;

            var user = JSON.parse(localStorage.getItem('authUser'));
            if (user !== null) {
                var del = confirm("Are you sure ?");
                if (del) {
                    axios.delete('/api/wishlist/' + id, { headers: { "Authorization": 'Bearer ' + user.access_token } }).then(function (response) {
                        if (response.status === 200) {
                            _this2.$toasted.success("Successfully removed");
                            window.location.reload();
                        }
                    });
                }
            }
        }
    }
});

/***/ }),

/***/ 1439:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "course-Detail" }, [
    _c("h4", { attrs: { id: "mainH" } }, [_vm._v("My Wishlist")]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "row rowCat" },
      _vm._l(_vm.wishlists, function(wishlist) {
        return _c(
          "div",
          {
            key: wishlist.id,
            staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12 courseCover"
          },
          _vm._l(wishlist, function(wish) {
            return _c("div", { key: wish.id, staticClass: "cover2" }, [
              _c(
                "div",
                { staticClass: "catImage" },
                [
                  wish.prodType === "Courses"
                    ? _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "CourseFullPage",
                              params: {
                                id: wish.id,
                                name: wish.courses.title.replace(/ /g, "-")
                              }
                            },
                            tag: "a"
                          }
                        },
                        [_c("img", { attrs: { src: wish.coverImage } })]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  wish.prodType === "Videos"
                    ? _c(
                        "router-link",
                        {
                          attrs: {
                            to: {
                              name: "SingleVideoPage",
                              params: { id: wish.id }
                            }
                          }
                        },
                        [_c("img", { attrs: { src: wish.coverImage } })]
                      )
                    : _vm._e()
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "type" }, [
                _c("div", { staticClass: "pTitle" }, [
                  _vm._v(_vm._s(wish.prodType))
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "about" }, [
                wish.prodType === "Courses"
                  ? _c(
                      "div",
                      { staticClass: "pTitle" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "CourseFullPage",
                                params: {
                                  id: wish.id,
                                  name: wish.courses.title.replace(/ /g, "-")
                                }
                              },
                              tag: "a"
                            }
                          },
                          [_vm._v("  " + _vm._s(wish.courses.title))]
                        )
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                wish.prodType === "Videos"
                  ? _c(
                      "div",
                      { staticClass: "pTitle" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "SingleVideoPage",
                                params: { id: wish.id }
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n\n                              " +
                                _vm._s(wish.webinarVideo.title)
                            )
                          ]
                        )
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                wish.prodType === "Podcast"
                  ? _c("div", { staticClass: "pTitle" }, [
                      _vm._v(_vm._s(wish.webinar.title))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                wish.prodType === "Market Research"
                  ? _c("div", { staticClass: "pTitle" }, [
                      _vm._v(_vm._s(wish.marketReasearch.title))
                    ])
                  : _vm._e(),
                _vm._v(" "),
                wish.hardCopyPrice
                  ? _c("div", { staticClass: "pTitle" }, [
                      _vm._v(" ₦ " + _vm._s(wish.hardCopyPrice))
                    ])
                  : _c("div", { staticClass: "pTitle" }, [
                      _vm._v("Subscription")
                    ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "buttons" }, [
                _c(
                  "div",
                  {
                    attrs: { id: "trash" },
                    on: {
                      click: function($event) {
                        return _vm.deleteWishlist(wish.id)
                      }
                    }
                  },
                  [
                    _c("span", { staticClass: "desktop" }, [_vm._v("Remove")]),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "fa fa-trash",
                      attrs: { "aria-hidden": "true" }
                    })
                  ]
                )
              ])
            ])
          }),
          0
        )
      }),
      0
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e80647ca", module.exports)
  }
}

/***/ }),

/***/ 563:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1436)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1438)
/* template */
var __vue_template__ = __webpack_require__(1439)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e80647ca"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/userWishlistComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e80647ca", Component.options)
  } else {
    hotAPI.reload("data-v-e80647ca", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});