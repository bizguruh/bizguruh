webpackJsonp([157],{

/***/ 1364:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1365);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("c7fad652", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-009d0abf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistPageComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-009d0abf\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./checklistPageComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1365:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.content-wrapper[data-v-009d0abf] {\n     height: 100vh;\n}\n.main-page[data-v-009d0abf] {\n     padding: 40px 20px;\n     max-height: 100vh;\n     height: 100vh;\n     overflow: scroll;\n}\nth[data-v-009d0abf], tr[data-v-009d0abf]{\n     text-align: center;\n}\ntd[data-v-009d0abf]{\n     text-transform: capitalize;\n}\n.main[data-v-009d0abf]{\n   width:100%;\n   display: -webkit-box;\n   display: -ms-flexbox;\n   display: flex;\n   -webkit-box-pack: justify;\n       -ms-flex-pack: justify;\n           justify-content: space-between;\n}\n.butt[data-v-009d0abf]{\n     display:-webkit-box;\n     display:-ms-flexbox;\n     display:flex;\n     -ms-flex-pack: distribute;\n         justify-content: space-around;\n}\n.fa-2x[data-v-009d0abf]{\n     font-size: 1.2em;\n}\n.fa-trash[data-v-009d0abf]{\n     color:red;\n}\n.fa-check-square[data-v-009d0abf]{\n     color:green;\n}\n.fa-undo[data-v-009d0abf]{\n     color:black;\n}\n.buttt[data-v-009d0abf]{\n      position: relative;\n      cursor: pointer;\n}\n#verify[data-v-009d0abf]{\n     display: none;\n     position: absolute;\n     top:-30px;\n     left: -15px;\n     width:60px;\n     font-size: 12px;\n     padding: 5px;\n     background:#fafafa;\n     color:black;\n     border-radius: 2px;\n}\n.buttt:hover #verify[data-v-009d0abf]{\n     display: block;\n}\n#edit[data-v-009d0abf]{\n      display: none;\n     position: absolute;\n     top:-30px;\n     left: -15px;\n     width:60px;\n     font-size: 12px;\n     padding: 5px;\n     background:#fafafa;\n     color:black;\n     border-radius: 2px;\n}\n.buttt:hover #edit[data-v-009d0abf]{\n     display: block;\n}\n#del[data-v-009d0abf]{\n     display: none;\n     position: absolute;\n     top:-30px;\n     left: -15px;\n     width:60px;\n     font-size: 12px;\n     padding: 5px;\n     background:#fafafa;\n     color:black;\n     border-radius: 2px;\n}\n.buttt:hover #del[data-v-009d0abf]{\n     display: block;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ 1366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'checklist-page',
    data: function data() {
        return {

            allQuestions: [],
            active: null

        };
    },

    created: function created() {
        this.getQuestions();
    },

    watch: {
        'active': 'getQuestions'
    },
    methods: {
        getQuestions: function getQuestions() {
            var _this = this;

            axios.get('/api/questions').then(function (response) {
                _this.allQuestions = response.data;
            });
        },
        deleteQuestion: function deleteQuestion(id) {
            var _this2 = this;

            var del = confirm('Do you want to delete?');
            if (del) {
                axios.delete('/api/questions/' + id).then(function (response) {
                    if (response.status === 200) {
                        // if (response.data === 'deleted') {
                        _this2.$toasted.success('Question deleted');
                        _this2.$router.go(0);
                        // }
                    }
                });
            }
        },
        verifyQuestion: function verifyQuestion(id) {
            var _this3 = this;

            axios.get('/api/verify-question/' + id).then(function (response) {
                if (response.status === 200) {
                    //   if (response.data === 'verified') {
                    _this3.$toasted.success('Verified');
                    _this3.active = true;

                    // }
                }
            });
        },
        unVerifyQuestion: function unVerifyQuestion(id) {
            var _this4 = this;

            axios.get('/api/unverify-question/' + id).then(function (response) {
                if (response.status === 200) {
                    //    if (response.data === 'unverified') {
                    _this4.$toasted.error('Unverified');
                    _this4.active = false;

                    // }
                }
            });
        }
    }
});

/***/ }),

/***/ 1367:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "main-page" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "main" }, [
          _c("h2", { staticClass: "mb-4" }, [_vm._v("Question List")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "create" },
            [
              _c(
                "router-link",
                { attrs: { tag: "a", to: { name: "ChecklistQuestion" } } },
                [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { type: "button" }
                    },
                    [_vm._v("Create question")]
                  )
                ]
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("table", { staticClass: "table" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm.allQuestions.length > 0
            ? _c(
                "tbody",
                _vm._l(_vm.allQuestions, function(question, index) {
                  return _c("tr", { key: index }, [
                    _c("td", { attrs: { scope: "row" } }, [
                      _vm._v(_vm._s(index + 1))
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(question.title))]),
                    _vm._v(" "),
                    question.verify === 0
                      ? _c("td", [_vm._v("Pending")])
                      : _c("td", [_vm._v("Active")]),
                    _vm._v(" "),
                    _c(
                      "td",
                      { staticClass: "butt" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "buttt",
                            attrs: {
                              tag: "a",
                              to: {
                                name: "editChecklistPage",
                                params: { id: question.id }
                              }
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "fa fa-pencil-square fa-2x ",
                              attrs: { "aria-hidden": "true" }
                            }),
                            _vm._v(" "),
                            _c("span", { attrs: { id: "edit" } }, [
                              _vm._v("Edit")
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "buttt",
                            on: {
                              click: function($event) {
                                return _vm.deleteQuestion(question.id)
                              }
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "fas fa-trash  fa-inverse fa-2x ",
                              attrs: { "aria-hidden": "true" }
                            }),
                            _vm._v(" "),
                            _c("span", { attrs: { id: "del" } }, [
                              _vm._v(" Delete")
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("span", { staticClass: "buttt" }, [
                          question.verify === 0
                            ? _c("i", {
                                staticClass:
                                  "fas fa-check-square fa-inverse fa-2x",
                                attrs: { "aria-hidden": "true" },
                                on: {
                                  click: function($event) {
                                    return _vm.verifyQuestion(question.id)
                                  }
                                }
                              })
                            : _c("i", {
                                staticClass: "fas fa-undo   fa-2x ",
                                on: {
                                  click: function($event) {
                                    return _vm.unVerifyQuestion(question.id)
                                  }
                                }
                              }),
                          _vm._v(" "),
                          _c(
                            "span",
                            { staticClass: "verifys", attrs: { id: "verify" } },
                            [
                              question.verify === 0
                                ? _c("span", [_vm._v(" Verify")])
                                : _c("span", [_vm._v("unverify")])
                            ]
                          )
                        ])
                      ],
                      1
                    )
                  ])
                }),
                0
              )
            : _vm._e()
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S/n")]),
        _vm._v(" "),
        _c("th", [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", [_vm._v("Status")]),
        _vm._v(" "),
        _c("th", [_vm._v("Edit")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-009d0abf", module.exports)
  }
}

/***/ }),

/***/ 546:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1364)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1366)
/* template */
var __vue_template__ = __webpack_require__(1367)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-009d0abf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/admin/components/checklistPageComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-009d0abf", Component.options)
  } else {
    hotAPI.reload("data-v-009d0abf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});