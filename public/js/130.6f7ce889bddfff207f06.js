webpackJsonp([130],{

/***/ 1676:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1677);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("6bf5ff16", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-fb038ef2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sponsorForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-fb038ef2\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sponsorForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1677:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.fa[data-v-fb038ef2] {\n\tpadding: 15px;\n\tfont-size: 18px;\n}\nul[data-v-fb038ef2] {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t-webkit-box-pack: left;\n\t    -ms-flex-pack: left;\n\t        justify-content: left;\n\tlist-style-type: none;\n\tmargin: 20px 0px;\n\tpadding: 5px;\n\tbackground: #ffffff;\n}\nul > li[data-v-fb038ef2] {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\tfloat: left;\n\theight: 10px;\n\twidth: auto;\n\tfont-weight: bold;\n\tfont-size: 0.8em;\n\tcursor: default;\n\t-webkit-box-align: center;\n\t    -ms-flex-align: center;\n\t        align-items: center;\n}\nul > li[data-v-fb038ef2]:not(:last-child)::after {\n\tcontent: \"/\";\n\tfloat: right;\n\tfont-size: 0.8em;\n\tmargin: 0 0.5em;\n\tcursor: default;\n}\n.linked[data-v-fb038ef2] {\n\tcursor: pointer;\n\tfont-size: 1em;\n\tfont-weight: normal;\n}\n.main[data-v-fb038ef2] {\n\twidth: 70%;\n\tmargin: 0 auto;\n\t-webkit-box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n\t        box-shadow: 0 1px 3px 0 rgba(20, 23, 28, 0.15);\n\tpadding: 20px;\n\tpadding-top: 50px;\n\tcolor: #5b84a7;\n    background: #ffffff;\n    height: 50vh;\n}\n.price[data-v-fb038ef2] {\n\tfont-weight: 500;\n    font-size: 18px;\n    margin : 20px 0 10px;\n}\n.benefits[data-v-fb038ef2] {\n\tcolor: rgb(0, 0, 0, 0.74);\n}\n.btn-primary[data-v-fb038ef2] {\n\tbackground: #5b84a7 !important;\n\tcolor: #ffffff;\n\ttext-transform: capitalize;\n\tpadding: 5px 30px;\n}\n.btn-primary[data-v-fb038ef2]:hover {\n\tbackground: rgb(91, 132, 167, 0.5) !important;\n}\n@media(max-width:425px){\n.main[data-v-fb038ef2]{\n\t\tpadding-top: 20px;\n    width:100%;\n    height:90vh;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	name: "sponsor-component",
	data: function data() {
		return {
			breadcrumbList: [],
			authenticate: false,
			token: null,
			userToken: null,
			price: 'default'
		};
	},
	mounted: function mounted() {
		this.updateList();
		var user = JSON.parse(localStorage.getItem("authUser"));
		if (user != null) {
			this.authenticate = true;
			this.token = user.access_token;
		}
	},

	watch: {
		$route: function $route() {
			this.updateList();
		}
	},
	methods: {
		routeTo: function routeTo(pRouteTo) {
			if (this.breadcrumbList[pRouteTo].link) {
				this.$router.push(this.breadcrumbList[pRouteTo].link);
			}
		},
		updateList: function updateList() {
			this.breadcrumbList = this.$route.meta.breadcrumb;
		},
		generateToken: function generateToken(l) {
			var text = "";
			var char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			for (var i = 0; i < l; i++) {
				text += char_list.charAt(Math.floor(Math.random() * char_list.length));
			}

			this.userToken = 'BIZ-' + text;
			return 'BIZ-' + text;
		},
		subscribe: function subscribe() {
			var _this = this;

			if (this.price !== "default") {
				this.generateToken(5);
				if (this.authenticate) {
					var data = {
						type: 4,
						token: this.userToken,
						price: this.price
					};
					console.log("Data: subscribe -> data", data);

					axios.post("/api/usersubscription", JSON.parse(JSON.stringify(data)), { headers: { Authorization: "Bearer " + this.token } }).then(function (response) {
						if (response.status === 200) {
							window.location.href = response.data;
						}
					}).catch(function (error) {

						if (error.response.data.message === "Unauthenticated.") {
							_this.$router.push({
								name: "auth",
								query: { redirect: "user-subscription" }
							});
						}
					});
				} else {
					this.$router.replace({
						name: "auth",
						query: { redirect: "user-subscription" }
					});
				}
			} else {
				this.$toasted.error('Select a plan');
			}
		}
	}
});

/***/ }),

/***/ 1679:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c(
      "ul",
      _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
        return _c(
          "li",
          {
            key: index,
            class: { linked: !!breadcrumb.link },
            on: {
              click: function($event) {
                return _vm.routeTo(index)
              }
            }
          },
          [_vm._v("\n\t\t\t" + _vm._s(breadcrumb.name) + "\n\t\t")]
        )
      }),
      0
    ),
    _vm._v(" "),
    _c("div", { staticClass: "main" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "price" }, [
        _c("div", { staticClass: "form-group" }, [
          _c("label", { staticClass: "price", attrs: { for: "" } }, [
            _vm._v("Pricing")
          ]),
          _vm._v(" "),
          _c(
            "select",
            {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.price,
                  expression: "price"
                }
              ],
              staticClass: "form-control",
              attrs: { name: "", id: "" },
              on: {
                change: function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.price = $event.target.multiple
                    ? $$selectedVal
                    : $$selectedVal[0]
                }
              }
            },
            [
              _c("option", { attrs: { value: "default" } }, [
                _vm._v("Select plan")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "7000" } }, [
                _vm._v("N7000— up to 2 small businesses")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "15000" } }, [
                _vm._v("N15,000— up to 4 small businesses")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "25000" } }, [
                _vm._v("N25,000— up to 7 small businesses")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "50000" } }, [
                _vm._v("N50,000— up to 14 small businesses")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "100000" } }, [
                _vm._v("N100,000— up to 28 small businesses")
              ]),
              _vm._v(" "),
              _c("option", { attrs: { value: "200000" } }, [
                _vm._v("N200,000—  up to 50 small businesses")
              ])
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", {}, [
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            attrs: { type: "button" },
            on: { click: _vm.subscribe }
          },
          [_vm._v("\n\t\t\t\tPay\n\t\t\t")]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "benefits" }, [
      _c("h4", [
        _vm._v(
          "\n\t\t\t\tLike us, are you passionate about raising a generation of\n\t\t\t\tenterprise titans?\n\t\t\t"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n\t\t\t\tDo you want to see more sustainable and reliable businesses\n\t\t\t\tin Africa?\n\t\t\t"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n\t\t\t\tYou can contribute to this cause by becoming a BizGuruh\n\t\t\t\tSponsor, and adopting a small business today.\n\t\t\t"
        )
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-fb038ef2", module.exports)
  }
}

/***/ }),

/***/ 617:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1676)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1678)
/* template */
var __vue_template__ = __webpack_require__(1679)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-fb038ef2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/sponsorForm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fb038ef2", Component.options)
  } else {
    hotAPI.reload("data-v-fb038ef2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});