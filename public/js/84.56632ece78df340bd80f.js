webpackJsonp([84],{

/***/ 1672:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1673);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("2eada619", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35bf51df\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./homeSearchComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35bf51df\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./homeSearchComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1673:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.container[data-v-35bf51df] {\n  padding: 10px;\n  background: #fff;\n}\n.pryHeader[data-v-35bf51df] {\n  text-transform: inherit;\n}\n.bread ul[data-v-35bf51df] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: left;\n      -ms-flex-pack: left;\n          justify-content: left;\n  list-style-type: none;\n  margin: 10px 0px 30px;\n  padding: 5px 15px;\n  background: #fff;\n}\n.bread ul > li[data-v-35bf51df] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  float: left;\n  height: 10px;\n  width: auto;\n  font-weight: bold;\n  font-size: 0.8em;\n  cursor: default;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.bread ul > li[data-v-35bf51df]:not(:last-child)::after {\n  content: \"/\";\n  float: right;\n  font-size: 0.8em;\n  margin: 0 0.5em;\n  cursor: default;\n}\n.linked[data-v-35bf51df] {\n  cursor: pointer;\n  font-size: 1em;\n  font-weight: normal;\n}\n.title[data-v-35bf51df] {\n  font-size: 16px;\n  color: #a4c2db;\n  text-transform: capitalize;\n}\n.title[data-v-35bf51df]:hover {\n  background: #f7f7f7;\n  padding: 3px;\n}\n.overview[data-v-35bf51df] {\n  font-size: 16px;\n  font-weight: normal;\n  color: rgba(0, 0, 0, 0.64);\n  line-height: 1.2;\n  max-height: 50px;\n  display: -webkit-box !important;\n  -webkit-line-clamp: 2;\n  -moz-line-clamp: 2;\n  -ms-line-clamp: 2;\n  -o-line-clamp: 2;\n  line-clamp: 2;\n  -webkit-box-orient: vertical;\n  -ms-box-orient: vertical;\n  -o-box-orient: vertical;\n  box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: normal;\n}\n.cover[data-v-35bf51df] {\n  padding: 15px 80px;\n}\n.cover img[data-v-35bf51df] {\n  width: 100%;\n  height: 100%;\n}\n@media (max-width: 425px) {\n.cover[data-v-35bf51df] {\n    padding: 15px 50px;\n}\n}\n@media (max-width: 425px) {\n.container[data-v-35bf51df] {\n    padding: 3px;\n}\n.main[data-v-35bf51df] {\n    padding: 5px;\n    border-bottom: 1px solid #eee;\n}\n.title[data-v-35bf51df] {\n    padding: 0;\n    font-size: 14px;\n}\n.overview[data-v-35bf51df]{\n      font-size: 14px;\n      max-height: 34px;\n}\n.cover[data-v-35bf51df] {\n    padding: 20px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config__ = __webpack_require__(669);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  search: "home-search-component",
  data: function data() {
    return {
      id: 0,
      search: this.$route.params.name,
      breadcrumbList: [],
      videoProduct: [],
      researchProduct: [],
      podcastProduct: [],
      articleProduct: [],
      courseProduct: []
    };
  },
  mounted: function mounted() {
    var _this = this;

    this.updateList();

    axios.get("/api/products").then(function (response) {
      response.data.data.forEach(function (item) {
        if (item.prodType == "Market Research") {
          _this.researchProduct.push(item.marketResearch);
        } else if (item.prodType == "Videos") {
          //  item.webinar.push(item.coverImage)
          item.webinar.image = item.coverImage;
          _this.videoProduct.push(item.webinar);
          console.log("Data: mounted ->   this.videoProduct", _this.videoProduct);
        } else if (item.prodType == "Podcast") {
          _this.podcastProduct.push(item.webinar);
        } else if (item.prodType == "Articles") {
          _this.articleProduct.push(item.articles);
        } else if (item.prodType == "Courses") {
          _this.courseProduct.push(item.courses);
        } else {
          return;
        }
      });
    });
  },

  watch: {
    $route: function $route() {
      this.updateList();
    }
  },
  computed: {
    researches: function researches() {
      var _this2 = this;

      return this.researchProduct.filter(function (element) {
        return element.title.toLowerCase().includes(_this2.search.toLowerCase());
      });
    },
    videos: function videos() {
      var _this3 = this;

      return this.videoProduct.filter(function (element) {
        return element.title.toLowerCase().includes(_this3.search.toLowerCase());
      });
    },
    podcasts: function podcasts() {
      var _this4 = this;

      return this.podcastProduct.filter(function (element) {
        return element.title.toLowerCase().includes(_this4.search.toLowerCase());
      });
    },
    articles: function articles() {
      var _this5 = this;

      return this.articleProduct.filter(function (element) {
        return element.title.toLowerCase().includes(_this5.search.toLowerCase());
      });
    },
    courses: function courses() {
      var _this6 = this;

      return this.courseProduct.filter(function (element) {
        return element.title.toLowerCase().includes(_this6.search.toLowerCase());
      });
    }
  },
  methods: {
    routeTo: function routeTo(pRouteTo) {
      if (this.breadcrumbList[pRouteTo].link) {
        this.$router.push(this.breadcrumbList[pRouteTo].link);
      }
    },
    updateList: function updateList() {
      this.breadcrumbList = this.$route.meta.breadcrumb;
    }
  }
});

/***/ }),

/***/ 1675:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "bread" }, [
      _c(
        "ul",
        _vm._l(_vm.breadcrumbList, function(breadcrumb, index) {
          return _c(
            "li",
            {
              key: index,
              class: { linked: !!breadcrumb.link },
              on: {
                click: function($event) {
                  return _vm.routeTo(index)
                }
              }
            },
            [_vm._v(_vm._s(breadcrumb.search))]
          )
        }),
        0
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.search,
              expression: "search"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            "aria-describedby": "helpId",
            placeholder: "Search",
            disabled: ""
          },
          domProps: { value: _vm.search },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.search = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("h4", { staticClass: "mainHeader" }, [_vm._v('"Search Result(s)"')]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _vm.articles.length === 0 &&
      _vm.videos.length === 0 &&
      _vm.podcasts.length === 0 &&
      _vm.courses.length === 0 &&
      _vm.researches.length === 0
        ? _c("h4", [_vm._v("No result(s) found")])
        : _vm._e(),
      _vm._v(" "),
      _vm.articles.length > 0
        ? _c(
            "div",
            {},
            _vm._l(_vm.articles, function(article, index) {
              return _c(
                "div",
                {
                  key: index,
                  staticClass: "col-lg-12 col-md-12 col-xs-12 main"
                },
                [
                  _c("h4", { staticClass: "pryHeader" }, [
                    _vm._v("Article result(s)")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-3 col-xs-3 cover " }, [
                    _c("img", {
                      attrs: {
                        src: article.image,
                        alt: "article image",
                        srcset: ""
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-md-8" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "title",
                          attrs: {
                            to: {
                              name: "ArticleSinglePage",
                              params: {
                                id: article.product_id,
                                name: article.title.replace(/ /g, "-")
                              }
                            }
                          }
                        },
                        [_vm._v(_vm._s(article.title.toLowerCase()))]
                      ),
                      _vm._v(" "),
                      _c("div", {
                        staticClass: "overview",
                        domProps: { innerHTML: _vm._s(article.description) }
                      })
                    ],
                    1
                  )
                ]
              )
            }),
            0
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.researches.length > 0
        ? _c(
            "div",
            [
              _c("h4", { staticClass: "pryHeader" }, [
                _vm._v("Market Research result(s)")
              ]),
              _vm._v(" "),
              _vm._l(_vm.researches, function(research, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass: "col-lg-12 col-md-12 col-xs-12 main"
                  },
                  [
                    _c("div", { staticClass: "col-md-3 col-xs-3 cover" }, [
                      _c("img", {
                        attrs: {
                          src: research.image,
                          alt: research.title,
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-md-8" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "ProductPaperDetail",
                                params: {
                                  id: research.product_id,
                                  name: research.title
                                    .replace(/\s+/g, "-")
                                    .toLowerCase(),
                                  type: "subscribe"
                                }
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v(_vm._s(research.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "overview" }, [
                              _vm._v(_vm._s(research.overview))
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.videos.length > 0
        ? _c(
            "div",
            [
              _c("h4", { staticClass: "pryHeader" }, [
                _vm._v("Video result(s)")
              ]),
              _vm._v(" "),
              _vm._l(_vm.videos, function(video, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass: "col-lg-12 col-md-12 col-xs-12 main"
                  },
                  [
                    _c("div", { staticClass: "col-md-3 col-xs-3 cover" }, [
                      _c("img", {
                        attrs: {
                          src: video.image,
                          alt: video.title,
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-md-8" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "Video",
                                params: { id: video.product_id }
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v(_vm._s(video.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "overview" }, [
                              _vm._v(_vm._s(video.description))
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.podcasts.length > 0
        ? _c(
            "div",
            [
              _c("h4", { staticClass: "pryHeader" }, [
                _vm._v("Podcast result(s)")
              ]),
              _vm._v(" "),
              _vm._l(_vm.podcasts, function(podcast, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass: "col-lg-12 col-md-12 col-xs-12 main"
                  },
                  [
                    _c("div", { staticClass: "col-md-3 col-xs-3 cover" }, [
                      _c("img", {
                        attrs: {
                          src: podcast.image,
                          alt: podcast.title,
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-md-8" },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: {
                              to: {
                                name: "SingleVideoPage",
                                params: { id: podcast.product_id }
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v(_vm._s(podcast.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "overview" }, [
                              _vm._v(_vm._s(podcast.description))
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              })
            ],
            2
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.courses.length > 0
        ? _c(
            "div",
            [
              _c("h4", { staticClass: "pryHeader" }, [
                _vm._v("Course result(s)")
              ]),
              _vm._v(" "),
              _vm._l(_vm.courses, function(course, index) {
                return _c(
                  "div",
                  {
                    key: index,
                    staticClass: "col-lg-12 col-md-12 col-xs-12 main"
                  },
                  [
                    _c("div", { staticClass: "col-md-3 col-xs-3 cover" }, [
                      _c("img", {
                        attrs: {
                          src: course.image,
                          alt: course.title,
                          srcset: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-md-8" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "title",
                            attrs: {
                              to: {
                                name: "CourseFullPage",
                                params: {
                                  id: course.product_id,
                                  name: course.title.replace(/ /g, "-")
                                }
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "title" }, [
                              _vm._v(_vm._s(course.title.toLowerCase()))
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "overview" }, [
                              _vm._v(_vm._s(course.overview))
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]
                )
              })
            ],
            2
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35bf51df", module.exports)
  }
}

/***/ }),

/***/ 616:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1672)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1674)
/* template */
var __vue_template__ = __webpack_require__(1675)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35bf51df"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/user/components/homeSearchComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35bf51df", Component.options)
  } else {
    hotAPI.reload("data-v-35bf51df", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getOpsHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAdminHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCustomerHeader; });
/* unused harmony export getIlcHeader */
var getHeader = function getHeader() {
    var vendorToken = JSON.parse(localStorage.getItem('authVendor'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + vendorToken.access_token
    };
    return headers;
};

var getOpsHeader = function getOpsHeader() {
    var opsToken = JSON.parse(localStorage.getItem('authOps'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + opsToken.access_token
    };
    return headers;
};

var getAdminHeader = function getAdminHeader() {
    var adminToken = JSON.parse(localStorage.getItem('authAdmin'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + adminToken.access_token
    };
    return headers;
};

var getCustomerHeader = function getCustomerHeader() {
    var customerToken = JSON.parse(localStorage.getItem('authUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};
var getIlcHeader = function getIlcHeader() {
    var customerToken = JSON.parse(localStorage.getItem('ilcUser'));
    var headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + customerToken.access_token
    };
    return headers;
};

/***/ })

});