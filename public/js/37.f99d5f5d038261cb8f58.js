webpackJsonp([37],{

/***/ 1103:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(1104);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("14e5bf00", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-619efc2b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./orderVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-619efc2b\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./orderVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 1104:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.main-page[data-v-619efc2b] {\n  position: relative;\n  height: 100vh;\n  overflow: scroll;\n  padding: 50px;\n}\n.title[data-v-619efc2b] {\n  font-weight: bold;\n  font-family: \"Josefin   Sans\";\n}\n.time[data-v-619efc2b] {\n  font-size: 12px;\n  float: right;\n}\n.fa-plus[data-v-619efc2b],\n.fa-minus[data-v-619efc2b] {\n  font-size: 0.6em;\n}\ntable[data-v-619efc2b] {\n  font-size: 15px;\n}\n.single-notify[data-v-619efc2b] {\n  border: 1px solid #f7f8fa;\n}\n.tab[data-v-619efc2b] {\n  background: #f7f8fa;\n  padding: 5px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  border-radius: 4px;\n}\n.tab-body[data-v-619efc2b] {\n  padding: 10px 15px;\n}\n.tab-body div p[data-v-619efc2b] {\n  border-bottom: 1px solid #f7f8fa;\n  border-top: 1px solid #f7f8fa;\n  padding: 10px 0;\n  margin: 40px 10px;\n  color: rgb(0, 0, 0, 0.64);\n  position: relative;\n}\n.tab-body div p[data-v-619efc2b]::before {\n  content: \"\";\n  position: absolute;\n  width: 30px;\n  height: 30px;\n  border-right: 2px solid #f7f8fa;\n  top: -30px;\n  left: 10%;\n}\n@media (max-width: 768px) {\ntable[data-v-619efc2b] {\n    font-size: 14px;\n}\nth[data-v-619efc2b] {\n    background-color: #f3f3f3;\n    padding: 5px 8px !important;\n    color: #000000;\n}\ntd[data-v-619efc2b] {\n    color: #a4a4a4 !important;\n    padding: 5px 8px !important;\n}\n.btn[data-v-619efc2b] {\n    text-transform: capitalize !important;\n    padding: 6px 12px;\n    font-size: 13px;\n}\n}\n@media (max-width: 425px) {\n.main-page[data-v-619efc2b] {\n    padding: 15px;\n}\ntable[data-v-619efc2b] {\n    font-size: 14px;\n}\nth[data-v-619efc2b] {\n    background-color: #f3f3f3;\n    padding: 5px 8px !important;\n    color: #000000;\n}\ntd[data-v-619efc2b] {\n    color: #a4a4a4 !important;\n    padding: 5px 8px !important;\n}\n.btn[data-v-619efc2b] {\n    text-transform: capitalize !important;\n    padding: 6px 12px;\n    font-size: 13px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 1105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sideVendorComponent__ = __webpack_require__(973);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__sideVendorComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__sideVendorComponent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent__ = __webpack_require__(978);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__stickyHeaderComponent__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['vendorSub'],
  name: "order-vendor-component",
  data: function data() {
    return {
      orders: [],
      isActive: true,
      activeCourse: "",
      users: [],
      indexCheck: null,
      index2Check: null,
      nameSet: [],
      messages: [],
      name: "",
      courses: [],
      time: []
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      if (vm.$props.vendorSub > 2) {
        next();
      } else {
        next('/vendor/dashboard');
      }
    });
  },
  mounted: function mounted() {
    var _this = this;

    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      var id = authVendor.id;
      axios.get("/api/order-detail/vendor/" + id, {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          _this.orders = response.data;
          _this.isActive = false;
        }
      }).catch(function (error) {
        if (error.response.status === 401) {
          _this.$toasted.error("Unknown error occur");
        }
      });
    } else {
      this.$router.push("/vendor/auth");
    }
    this.getNotifications();
    this.getCourseNotification();
  },

  methods: {
    showStory: function showStory(value, name) {
      this.name = "";
      this.courses = [];
      this.time = [];
      this.indexCheck = value;
      this.name = name;
      this.getCourseNotification(name);
    },
    showStory2: function showStory2(value, name) {
      this.index2Check = value;
      this.activeCourse = name;
    },
    closeStory: function closeStory() {
      this.indexCheck = null;
      this.index2Check = null;
      this.name = "";
      this.courses = [];
      this.time = [];
    },
    closeStory2: function closeStory2() {
      this.index2Check = null;
    },
    viewOrder: function viewOrder() {
      console.log("tthdhdhfh");
    },
    getNotifications: function getNotifications() {
      var _this2 = this;

      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      var id = authVendor.id;
      axios.get("/api/vendor-notifications", {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          var names = [];
          response.data.forEach(function (item) {
            _this2.messages = response.data;
            names.push(item.name);
          });
          var newSet = new Set(names);
          _this2.nameSet = Array.from(newSet);
          _this2.isActive = false;
        }
      }).catch(function (error) {
        if (error.response.status === 401) {
          _this2.$toasted.error("Unknown error occur");
        }
      });
    },
    getUsers: function getUsers() {
      var _this3 = this;

      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      var id = authVendor.id;
      axios.get("/api/vendor-subscribers", {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          _this3.users = response.data;
          _this3.isActive = false;
        }
      }).catch(function (error) {
        _this3.$toasted.error("Unknown error occur");
      });
    },
    getCourseNotification: function getCourseNotification(name) {
      var _this4 = this;

      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      var id = authVendor.id;
      axios.get("/api/course-notifications", {
        headers: { Authorization: "Bearer " + authVendor.access_token }
      }).then(function (response) {
        if (response.status === 200) {
          _this4.users = response.data;
          var videos = [];

          var forWatched = [];
          if (_this4.notifications.length) {
            response.data.forEach(function (item) {
              var data = {};

              if (name == item.name) {
                data.name = item.name;
                data.productId = item.productId;
                data.title = item.productTitle;
                data.total_content = item.total_content;

                videos.push(data);

                forWatched.push(item.productId);
                _this4.time.push(item.time);
              }
            });

            var watchedcount = forWatched.reduce(function (b, c) {
              return (b[b.findIndex(function (d) {
                return d.el === c;
              })] || b[b.push({ el: c, count: 0 }) - 1]).count++, b;
            }, []);

            var jsonObject = videos.map(JSON.stringify);
            var uniqueSet = new Set(jsonObject);
            var perCourse = Array.from(uniqueSet).map(JSON.parse);

            perCourse.forEach(function (item, idx) {
              _this4.calcPercent(watchedcount[idx].count, item.total_content, item.title, item.name, _this4.time[idx]);
            });
            _this4.sortNotification();
          }

          _this4.isActive = false;
        }
      }).catch(function (error) {
        console.log("getCourseNotification -> error", error);
        _this4.$toasted.error("Unknown error occur");
      });
    },
    calcPercent: function calcPercent(videos, courses, title, name, time) {
      var value = {};
      value.percent = Math.round(videos / courses * 100);
      value.title = title;
      value.name = name;
      value.time = time;

      this.notifications.push(value);
    },
    sortNotification: function sortNotification() {
      var arr = [];
      this.notifications.forEach(function (item) {
        arr.push(item.title);
      });
      var jsonObject = arr.map(JSON.stringify);
      var uniqueSet = new Set(jsonObject);
      this.courses = Array.from(uniqueSet).map(JSON.parse);
    }
  },
  computed: {
    notifications: function notifications() {
      var _this5 = this;

      return this.messages.filter(function (index) {
        return index.name == _this5.name;
      });
    },
    sortNotify: function sortNotify() {
      var _this6 = this;

      return this.notifications.filter(function (index) {
        return index.title == _this6.activeCourse;
      });
    }
  }
});

/***/ }),

/***/ 1106:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "main-page bg-white rounded" }, [
    _vm.isActive
      ? _c("div", { staticClass: "loaderShadow" }, [_vm._m(0)])
      : _c("div", [
          _c("h4", { staticClass: "mb-3" }, [_vm._v("Subscriber Activities")]),
          _vm._v(" "),
          _vm.nameSet.length > 0
            ? _c(
                "div",
                { staticClass: "notifications" },
                _vm._l(_vm.nameSet, function(name, index) {
                  return _c(
                    "div",
                    { key: index, staticClass: "single-notify" },
                    [
                      _c("div", { staticClass: "tab shadow-sm" }, [
                        index !== _vm.indexCheck
                          ? _c("i", {
                              staticClass: "fa fa-plus mr-3 p-1",
                              attrs: { "aria-hidden": "true" },
                              on: {
                                click: function($event) {
                                  return _vm.showStory(index, name)
                                }
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        index == _vm.indexCheck
                          ? _c("i", {
                              staticClass: "fa fa-minus mr-3 p-1",
                              attrs: { "aria-hidden": "true" },
                              on: {
                                click: function($event) {
                                  return _vm.closeStory()
                                }
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        _c("span", { staticClass: "text" }, [
                          _vm._v(_vm._s(name))
                        ])
                      ]),
                      _vm._v(" "),
                      index == _vm.indexCheck
                        ? _c("div", { staticClass: "tab-body" }, [
                            _c(
                              "div",
                              { staticClass: "p-3" },
                              _vm._l(_vm.courses, function(course, idx) {
                                return _c(
                                  "div",
                                  { key: idx, staticClass: "my-2" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "tab shadow-sm" },
                                      [
                                        idx !== _vm.index2Check
                                          ? _c("i", {
                                              staticClass:
                                                "fa fa-plus mr-3 p-1",
                                              attrs: { "aria-hidden": "true" },
                                              on: {
                                                click: function($event) {
                                                  return _vm.showStory2(
                                                    idx,
                                                    course
                                                  )
                                                }
                                              }
                                            })
                                          : _vm._e(),
                                        _vm._v(" "),
                                        idx == _vm.index2Check
                                          ? _c("i", {
                                              staticClass:
                                                "fa fa-minus mr-3 p-1",
                                              attrs: { "aria-hidden": "true" },
                                              on: {
                                                click: function($event) {
                                                  return _vm.closeStory2()
                                                }
                                              }
                                            })
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _c("span", { staticClass: "text" }, [
                                          _vm._v(_vm._s(course))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    idx == _vm.index2Check
                                      ? _c(
                                          "div",
                                          { staticClass: "m-3" },
                                          _vm._l(_vm.sortNotify, function(
                                            notification,
                                            index
                                          ) {
                                            return _c("div", { key: index }, [
                                              _c(
                                                "div",
                                                { staticClass: "mx-5 my-3" },
                                                [
                                                  notification.message ==
                                                  "subscribe"
                                                    ? _c("p", [
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              notification.name
                                                            )
                                                          )
                                                        ]),
                                                        _vm._v(
                                                          " subscribed to\n                      "
                                                        ),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "title"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                notification.title
                                                              )
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "time"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm._f(
                                                                  "moment"
                                                                )(
                                                                  notification.time,
                                                                  "L"
                                                                )
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  notification.message ==
                                                  "purchase"
                                                    ? _c("p", [
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              notification.name
                                                            )
                                                          )
                                                        ]),
                                                        _vm._v(
                                                          " purchased\n                      "
                                                        ),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "title"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                notification.title
                                                              )
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "time"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm._f(
                                                                  "moment"
                                                                )(
                                                                  notification.time,
                                                                  "L"
                                                                )
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  notification.percent > 0
                                                    ? _c("p", [
                                                        _vm._v(
                                                          "\n                      " +
                                                            _vm._s(
                                                              notification.name
                                                            ) +
                                                            " started\n                      "
                                                        ),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "title"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                notification.title
                                                              )
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "time ml-auto"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm._f(
                                                                  "moment"
                                                                )(
                                                                  notification.time,
                                                                  "L"
                                                                )
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  notification.percent < 100
                                                    ? _c("p", [
                                                        _vm._v(
                                                          "\n                      " +
                                                            _vm._s(
                                                              notification.name
                                                            ) +
                                                            " is " +
                                                            _vm._s(
                                                              notification.percent
                                                            ) +
                                                            "% to completion of\n                      "
                                                        ),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "title"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                notification.title
                                                              )
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "time ml-auto"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm._f(
                                                                  "moment"
                                                                )(
                                                                  notification.time,
                                                                  "L"
                                                                )
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  notification.percent == 100
                                                    ? _c("p", [
                                                        _vm._v(
                                                          "\n                      " +
                                                            _vm._s(
                                                              notification.name
                                                            ) +
                                                            " completed\n                      "
                                                        ),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass: "title"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                notification.title
                                                              )
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "time ml-auto"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm._f(
                                                                  "moment"
                                                                )(
                                                                  notification.time,
                                                                  "L"
                                                                )
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ])
                                                    : _vm._e()
                                                ]
                                              )
                                            ])
                                          }),
                                          0
                                        )
                                      : _vm._e()
                                  ]
                                )
                              }),
                              0
                            ),
                            _vm._v(" "),
                            _c("hr")
                          ])
                        : _vm._e()
                    ]
                  )
                }),
                0
              )
            : _vm._e()
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "loadContainer" }, [
      _c("img", {
        staticClass: "icon",
        attrs: { src: "/images/logo.png", alt: "bizguruh loader" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "loader" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-619efc2b", module.exports)
  }
}

/***/ }),

/***/ 493:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(1103)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(1105)
/* template */
var __vue_template__ = __webpack_require__(1106)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-619efc2b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/orderVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-619efc2b", Component.options)
  } else {
    hotAPI.reload("data-v-619efc2b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 973:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(974)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(976)
/* template */
var __vue_template__ = __webpack_require__(977)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1779c6aa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/sideVendorComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1779c6aa", Component.options)
  } else {
    hotAPI.reload("data-v-1779c6aa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 974:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(975);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("058369d5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1779c6aa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sideVendorComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 975:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\na[data-v-1779c6aa] {\n  color: #b8c7ce !important;\n}\nspan[data-v-1779c6aa] {\n  font-size: 16px;\n}\n.router-link-exact-active[data-v-1779c6aa]{\nbackground: #1e282c;\nborder-left-color: #3c8dbc;\n}\n.faded[data-v-1779c6aa]{\n  cursor:not-allowed!important;\n  color: rgba(255, 255, 255, 0.4);\n}\n.main-sidebar[data-v-1779c6aa] {\n  \n  background-image: url(\"/images/education.jpg\");\n  background-size: cover;\n  z-index: 0;\n  width: 20%;\n  position: relative;\n  padding-top:0;\n}\n.overlay[data-v-1779c6aa] {\n  background: rgba(29, 53, 73, 0.8);\n  z-index: 1;\n  width: 100%;\n  height: 100%;\n  position: absolute;\n}\n.sidebar[data-v-1779c6aa] {\n  padding-top: 30px;\n  z-index: 2;\n  position: relative;\n}\n.main-sidebar-nav[data-v-1779c6aa] {\n  display: block !important;\n  -webkit-transform: translate(-230px, 0);\n  transform: translate(0, 0);\n}\n@media only screen and (max-width: 768px) {\n.main-sidebar[data-v-1779c6aa] {\n    display: none;\n}\n.main-sidebar[data-v-1779c6aa] {\n    width:100%;\n}\nspan[data-v-1779c6aa] {\n    font-size: 16px;\n}\n.sidebar ul li a[data-v-1779c6aa] {\n    padding: 15px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "side-vendor-component",
  props: ["showNav", 'vendorSub'],
  data: function data() {
    return {
      id: "",
      token: "",
      backgroundColor: "",
      fontSize: "",
      color: "",
      sidebar: "",
      webAdmin: false,
      storeAdmin: false,
      authVendor: {},
      username: ""
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      this.username = authVendor.storeName;
      this.authVendor = authVendor;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.sidebarBackgroundColor;
        // this.fontSize = response.data.data.sidebarFontSize;
        // this.color = response.data.data.sidebarColor;
        // this.sidebar = response.data.data.sidebarMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth?type=login");
    }
  },

  methods: {
    shakeButton: function shakeButton() {
      this.$emit('shakeButton');
    },
    logout: function logout() {
      Engagespot.clearUser();
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth?type=login");
    }
  }
});

/***/ }),

/***/ 977:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "aside",
    { staticClass: "main-sidebar", class: { "main-sidebar-nav": _vm.showNav } },
    [
      _c("div", { staticClass: "overlay" }),
      _vm._v(" "),
      _c("section", { staticClass: "sidebar" }, [
        _c(
          "ul",
          { staticClass: "sidebar-menu", attrs: { "data-widget": "tree" } },
          [
            _c("li", { staticClass: "px-2" }, [
              _vm.vendorSub < 3
                ? _c(
                    "button",
                    { staticClass: "elevated_btn elevated_btn_sm text-main" },
                    [_vm._v("Upgrade")]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: "/vendor/dashboard" } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-chart-line pr-3" }),
                      _vm._v(" Dashboard\n          ")
                    ]),
                    _vm._v(" "),
                    _c("span", { staticClass: "pull-right-container" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              [
                _c(
                  "router-link",
                  { attrs: { tag: "a", to: { name: "viewVendorProduct" } } },
                  [
                    _c("span", [
                      _c("i", { staticClass: "fas fa-box-open pr-3" }),
                      _vm._v(" Resources\n          ")
                    ])
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "orderVendor" } } },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-bell pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v("Subscribers\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Subscribers\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "messagesVendor" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-inbox pr-3" }),
                          _vm._v(" Inbox\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-inbox pr-3 faded" }),
                        _vm._v(" Inbox\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 1
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: "/vendor-form-questions" } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fa fa-wpforms pr-3" }),
                          _vm._v(" Forms\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded", on: { click: _vm.shakeButton } },
                      [
                        _c("i", {
                          staticClass: "fas fa-bell pr-3 faded",
                          attrs: { "aria-hidden": "true" }
                        }),
                        _vm._v("Forms\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.vendorSub > 2
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      { attrs: { tag: "a", to: { name: "VendorStatistics" } } },
                      [
                        _c("span", [
                          _c("i", { staticClass: "fas fa-chart-bar pr-3" }),
                          _vm._v(" Insights\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _c("li", [
                  _c("a", [
                    _c(
                      "span",
                      { staticClass: "faded ", on: { click: _vm.shakeButton } },
                      [
                        _c("i", { staticClass: "fas fa-chart-bar pr-3 faded" }),
                        _vm._v(" Insights\n          ")
                      ]
                    )
                  ])
                ]),
            _vm._v(" "),
            _vm.authVendor.type === "vendor"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.authVendor.type === "expert"
              ? _c(
                  "li",
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "vendorProfile",
                            params: { profile: _vm.username }
                          }
                        }
                      },
                      [
                        _c("span", [
                          _c("i", {
                            staticClass: "fas fa-user pr-3",
                            attrs: { "aria-hidden": "true" }
                          }),
                          _vm._v(" Profile\n          ")
                        ])
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#" } }, [
                _c(
                  "span",
                  {
                    on: {
                      click: function($event) {
                        return _vm.logout()
                      }
                    }
                  },
                  [
                    _c("i", {
                      staticClass: "fa fa-sign-out pr-3",
                      attrs: { "aria-hidden": "true" }
                    }),
                    _vm._v(" Logout\n          ")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._m(0)
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "p-3" }, [
      _c("span", { attrs: { id: "notification" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1779c6aa", module.exports)
  }
}

/***/ }),

/***/ 978:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(979)
}
var normalizeComponent = __webpack_require__(80)
/* script */
var __vue_script__ = __webpack_require__(981)
/* template */
var __vue_template__ = __webpack_require__(982)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-41d38ab3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/vendor/components/stickyHeaderComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-41d38ab3", Component.options)
  } else {
    hotAPI.reload("data-v-41d38ab3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 979:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(980);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(81)("3f8a0fc8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-41d38ab3\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./stickyHeaderComponent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 980:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(57)(false);
// imports


// module
exports.push([module.i, "\n.header-section[data-v-41d38ab3] {\n  background: #fff;\n  position: relative;\n  padding: 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.hamburger[data-v-41d38ab3]{\n  padding:10px;\n}\n.button-blue[data-v-41d38ab3] {\n  font-size: 15px;\n}\n.mobile-nav[data-v-41d38ab3] {\n  display: none;\n}\n.header-right[data-v-41d38ab3] {\n  float: unset;\n  width: auto !important;\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.imgDev[data-v-41d38ab3] {\n  width: 25px;\n  height: 25px;\n  border-radius: 50%;\n  margin-top: 0;\n}\n.dropDownVendor[data-v-41d38ab3] {\n  background-color: #ffffff;\n  position: absolute;\n  width: 150px;\n  left: -30px;\n  top: 57px;\n}\n.pointer[data-v-41d38ab3] {\n  font-size: 16px;\n  padding-left: 20px;\n  cursor: pointer;\n  color: hsl(207, 46%, 20%);\n}\n.pointer[data-v-41d38ab3]:hover {\n  color: #333333;\n}\n.nav_item[data-v-41d38ab3] {\n  color: hsl(207, 46%, 20%);\n  font-size: 15px;\n  border-radius: 5px;\n}\n.list[data-v-41d38ab3] {\n  border-bottom: 1px solid #f5f5f5;\n  padding: 5px;\n  position: relative;\n}\n.list[data-v-41d38ab3]::before {\n  position: absolute;\n  content: \"\";\n  top: -6px;\n  right: 20%;\n  width: 15px;\n  height: 15px;\n  background: #fff;\n  -webkit-transform: rotate(45deg);\n          transform: rotate(45deg);\n}\n/* .userAcc {\n  margin: 25px 0 5px;\n} */\nol[data-v-41d38ab3],\nul[data-v-41d38ab3] {\n  list-style: none;\n}\n.accountCaret[data-v-41d38ab3] {\n  margin-left: 0 !important;\n}\n.accAvatar[data-v-41d38ab3] {\n  margin-right: 0;\n}\n.accountProfile[data-v-41d38ab3] {\n  font-size: 16px;\n  padding: 5px 15px;\n  background: #f7f8fa;\n  position: relative;\n}\n.biz[data-v-41d38ab3] {\n  color: #a4c2db !important;\n}\n.guru[data-v-41d38ab3] {\n  color: #333333;\n}\n.fa-bars[data-v-41d38ab3] {\n  color: #a4c2db;\n}\n.businessLogo[data-v-41d38ab3] {\n  font-size: 24px;\n  margin-right: auto;\n}\n.slide-fade-enter-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.3s ease;\n  transition: all 0.3s ease;\n}\n.slide-fade-leave-active[data-v-41d38ab3] {\n  -webkit-transition: all 0.2s ease;\n  transition: all 0.2s ease;\n}\n.slide-fade-enter[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n.slide-fade-leave-to[data-v-41d38ab3] {\n  -webkit-transform: translateY(-50px);\n          transform: translateY(-50px);\n  opacity: 0;\n}\n@media only screen and (max-width: 768px) {\n.userAccName[data-v-41d38ab3] {\n    display: none;\n}\n.animated[data-v-41d38ab3] {\n    display: none;\n}\n.dropDownVendor[data-v-41d38ab3] {\n    left: -100px;\n}\n.imgDev[data-v-41d38ab3] {\n    width: 22px;\n    height: 22px;\n    border-radius: 50%;\n    margin-top: 0;\n}\n.businessLogo[data-v-41d38ab3] {\n    font-size: 25px;\n    margin-top: 5px;\n    margin-left: auto;\n    margin-right: auto;\n    margin-bottom: 5px;\n}\n.mobile-nav[data-v-41d38ab3] {\n    display: inline;\n    margin-right: 25px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var user = JSON.parse(localStorage.getItem("authVendor"));
Engagespot.init("cywCppTlinzpmjVHxFcYoiuuKlnefc");
if (user !== null) {
  Engagespot.identifyUser(user.id);
}

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "sticky-header-component",
  props: ["profileImage", "vendorSub", "shake", "myNav"],
  data: function data() {
    return {
      id: "",
      token: "",
      username: "",
      linkTag: false,
      backgroundColor: "",
      fontSize: "",
      color: "",
      topMenu: "",
      showDropDown: false,
      links: [{
        name: "Profile",
        url: "vendorProfile"
      }, {
        name: "Product",
        url: "viewVendorProduct"
      }, {
        name: "Logout",
        url: "logout"
      }]
    };
  },
  mounted: function mounted() {
    if (localStorage.getItem("authVendor")) {
      var authVendor = JSON.parse(localStorage.getItem("authVendor"));
      this.username = authVendor.storeName;
      this.linkTag = true;
      this.token = authVendor.access_token;
      this.id = authVendor.id;
      axios.get("/api/get/vendor/" + this.id, {
        headers: { Authorization: "Bearer " + this.token }
      }).then(function (response) {
        // this.backgroundColor = response.data.data.topMenuBackgroundColor;
        // this.fontSize = response.data.data.topMenuFontSize;
        // this.color = response.data.data.topMenuColor;
        // this.topMenu = response.data.data.topMenuMargin;
      }).catch(function (error) {
        console.log(error);
      });
    } else {
      this.$router.push("/vendor/auth");
    }
  },

  methods: {
    logout: function logout() {
      localStorage.removeItem("authVendor");
      this.$router.push("/vendor/auth");
    },
    openNav: function openNav() {
      this.$emit("mobile-nav");
    }
  }
});

/***/ }),

/***/ 982:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "sticky-header header-section shadow-sm",
      style: { backgroundColor: _vm.backgroundColor }
    },
    [
      _c("div", { staticClass: "mobile-nav" }, [
        _c(
          "span",
          {
            staticClass:
              "d-flex justify-content-start align-items-center mobile-nav",
            on: { click: _vm.openNav }
          },
          [
            _c(
              "button",
              {
                staticClass: "hamburger hamburger--collapse",
                class: { "is-active": _vm.myNav },
                attrs: {
                  tabindex: "0",
                  "aria-label": "Menu",
                  role: "button",
                  "aria-controls": "navigation",
                  type: "button"
                }
              },
              [_vm._m(0)]
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "businessLogo mr-4" },
        [
          _c("router-link", { attrs: { to: "/" } }, [
            _c("span", { staticClass: "biz" }, [_vm._v("Biz")]),
            _c("span", { staticClass: "guru" }, [_vm._v("Guruh")])
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.$route.meta.vendor
        ? _c(
            "router-link",
            {
              staticClass: "nav_item p-1 border",
              attrs: { to: "/vendor/home" }
            },
            [
              _c("i", {
                staticClass: "fa fa-home",
                attrs: { "aria-hidden": "true" }
              }),
              _vm._v("\nMy Home\n   ")
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "header-right ml-auto" }, [
        _vm.vendorSub < 3
          ? _c(
              "button",
              {
                staticClass: "button-blue mr-4 animated",
                class: { shake: _vm.shake }
              },
              [_vm._v("Upgrade")]
            )
          : _vm._e(),
        _vm._v(" "),
        _c("div", { attrs: { id: "notification mr-3" } }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "accountProfile rounded-pill" },
          [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.showDropDown = !_vm.showDropDown
                  }
                }
              },
              [
                _c("span", { staticClass: "userAcc accAvatar" }, [
                  _vm.profileImage !== ""
                    ? _c("span", { staticClass: "pr-2" }, [
                        _c("img", {
                          staticClass: "imgDev",
                          attrs: { src: _vm.profileImage, alt: "avatar" }
                        })
                      ])
                    : _c("i", {
                        staticClass: "fas fa-user-circle-o text-main pr-2"
                      }),
                  _vm._v(" "),
                  _c("span", { staticClass: "userAccName text-main" }, [
                    _vm._v(_vm._s(_vm.username))
                  ])
                ]),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa userAcc accountCaret text-main",
                  class: {
                    "fa-caret-up": _vm.showDropDown,
                    "fa-caret-down": !_vm.showDropDown
                  },
                  attrs: { "aria-hidden": "true" }
                })
              ]
            ),
            _vm._v(" "),
            _c("transition", { attrs: { name: "slide-fade" } }, [
              _vm.showDropDown
                ? _c("div", { staticClass: "dropDownVendor shadow-sm" }, [
                    _c("ul", { staticClass: "menu list pl0 pa0 ma0" }, [
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "viewVendorProduct" } }
                            },
                            [_vm._v("Resources")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: {
                                to: {
                                  name: "vendorProfile",
                                  params: { profile: _vm.username }
                                }
                              }
                            },
                            [_vm._v("Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "li",
                        { staticClass: "list" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "dd-link pointer hover-bg-moon-gray",
                              attrs: { to: { name: "vendorConfig" } }
                            },
                            [_vm._v("Update Profile")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("li", { staticClass: "list" }, [
                        _c(
                          "a",
                          {
                            staticClass: "dd-link pointer hover-bg-moon-gray",
                            on: { click: _vm.logout }
                          },
                          [_vm._v("Logout")]
                        )
                      ])
                    ])
                  ])
                : _vm._e()
            ])
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "hamburger-box" }, [
      _c("span", { staticClass: "hamburger-inner" })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-41d38ab3", module.exports)
  }
}

/***/ })

});