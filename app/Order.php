<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function orderDetail(){
        return $this->hasMany('App\OrderDetail', 'order_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
