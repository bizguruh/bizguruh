<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $table = 'product_categories';

    protected $fillable = ['name', 'description'];

    public function product(){
        return $this->hasMany('App\Product', 'category_id', 'id');
    }

    public function subCategory(){
    return $this->hasMany('App\ProductSubCategory', 'sub_category_id');
    }
}
