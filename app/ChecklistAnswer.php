<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistAnswer extends Model
{
    protected $table = 'checklist_answer';

    protected $fillable = ['user_id', 'title','name','response','question_id','answers'];
}
