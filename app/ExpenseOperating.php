<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseOperating extends Model
{
    protected $table = 'operating_expense';
    protected $fillable = [
        'user_id',
        'month',
        'rent',
        'salaries',
        'light',
        'data',
        'repairs',
        'bills',
        'marketing',
        'logistics',
        'office',
        'levies',
        'others'
    ];
}
