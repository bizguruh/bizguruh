<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function orderDetailVideo()
    {
        return $this->hasMany('App\OrderDetailVideo', 'order_id', 'id');
    }
}
