<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountDetails extends Model
{
    //
    protected $table = 'account_details';
    protected $fillable = ['user_id', 'contact_id', 'contact_person_id'];
}
