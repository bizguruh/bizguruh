<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';

    protected $fillable = [
      'vendor_id',
    'locale',
    'count',
    'user_id'
    ];
}
