<?php

namespace App;

use Carbon\Carbon;
use App\AccountDetails;
use Illuminate\Database\Eloquent\Model;

class Accounting extends Model
{
    protected $table = 'accounting';

    public function refreshToken()
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $access = $client->request('POST', 'https://accounts.zoho.com/oauth/v2/token?refresh_token=' . $oauth->refresh_token . '&client_id=' . $oauth->client_id . '&client_secret=' . $oauth->client_secret . '&redirect_uri=' . 'https://bizguruh.com/oauth&grant_type=refresh_token', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',

            ]
        ]);

        $oauth->access_token = json_decode($access->getBody())->access_token;
        $oauth->save();
        return 'changed';
    }


    // contact
    public function createContact($contact_name,  $first_name, $last_name, $email, $user_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();

        $data = json_encode([
            "contact_name" => $contact_name,
            "company_name" => 'bizguruh',
            "contact_type" => "customer",
            "contact_persons" => [[

                "first_name" => $first_name,
                "last_name" => $last_name,
                "email" => $email
            ]]


        ]);



        $api = $client->request('post', 'https://books.zoho.com/api/v3/contacts?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        $account_details = AccountDetails::create([
            'user_id' => $user_id,
            'contact_id' => json_decode($api->getBody())->contact->contact_id,
            'contact_person_id' => json_decode($api->getBody())->contact->contact_persons[0]->contact_person_id
        ]);
        return $api->getBody();
    }



    public function updateContact($contact_id, $contact_name)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();

        $data = json_encode([
            "contact_name" => $contact_name,
        ]);


        $api = $client->request('put', 'https://books.zoho.com/api/v3/contacts/' . $contact_id . '?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }
    public function getContact($contact_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('get', 'https://books.zoho.com/api/v3/contacts/' . $contact_id . '?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }



    // create item  

    public function createItem($name, $rate, $description)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();

        $data = json_encode([
            "name" => $name,
            "rate" => $rate,
            "description" => $description,
            "product_type"=> "goods",
            

        ]);


        $api = $client->request('post', 'https://books.zoho.com/api/v3/items?organization_id=698673313&send=true&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);




       return $item_id= json_decode($api->getBody())->item->item_id;
     
    }


    // INVOICE 

    public function createInvoice($customer_id, $contact_person_id,$name,$rate,$description)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();
        $item_id = $this->createItem($name,$rate,$description);


        $data = json_encode([
            "customer_id" => $customer_id,
            "contact_persons" => [$contact_person_id],
            "date" => Carbon::now()->format('Y-m-d'),
            "reference_number" => "REF-001",

            "line_items" => [

                [
                    "item_id" => $item_id,

                ]

            ]

        ]);


        $api = $client->request('post', 'https://books.zoho.com/api/v3/invoices?organization_id=698673313&send=true&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }

    public function getInvoice()
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();
        


        $api = $client->request('post', 'https://books.zoho.com/api/v3/invoices?organization_id=698673313&send=true', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }

    public function listInvoices($name)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('GET', 'https://books.zoho.com/api/v3/invoices?organization_id=698673313&customer_name=' . $name, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }

    public function retrieveInvoice($invoice_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('GET', 'https://books.zoho.com/api/v3/invoices/' . $invoice_id . '/?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }
    public function deleteInvoice($invoice_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('delete', 'https://books.zoho.com/api/v3/invoices/' . $invoice_id . '/?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }


    // pORTAL  
    public function enablePortal()
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();

        $data = json_encode([
            "contact_persons" => [[

                "contact_person_id" => 2059437000000082056
            ]]


        ]);

        $contact_id = 2059437000000082054;
        $api = $client->request('post', 'https://books.zoho.com/api/v3/contacts/' . $contact_id . '/portal/enable?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }



    // pAYMENT 
    public function createPayment($customer_id, $amount, $payment_mode, $invoice_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();
        

        $data = json_encode([
            "customer_id" => $customer_id,
            "payment_mode" => $payment_mode,
            "amount" => $amount,
            "date" => Carbon::now()->format('Y-m-d'),
            "reference_number" => "INV-384",
            "description" => "Payment has been added to INV-384",

            "invoices" => [

                [
                    "invoice_id" => $invoice_id,
                    "amount_applied" => $amount

                ]

            ]

        ]);


        $api = $client->request('post', 'https://books.zoho.com/api/v3/customerpayments?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }


    public function listPayments($name)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('GET', 'https://books.zoho.com/api/v3/customerpayments?organization_id=698673313&customer_name=' . $name, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }


    public function retrievePayments($payment_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('GET', 'https://books.zoho.com/api/v3/customerpayments/' . $payment_id . '/?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }
    public function deletePayments($payment_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();


        $api = $client->request('delete', 'https://books.zoho.com/api/v3/customerpayments/' . $payment_id . '/?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }



    // STATEMENT  
    public function sendStatement($contact_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();

        $data = json_encode([
            "send_from_org_email_id" => true,
            "to_mail_ids" => [
                "succy2010@gmail.com"
            ],

            "subject" => "Statement of transactions with Zillium Inc",
            "body" => "Dear Customer,     <br/>We have attached with this email a list of all your transactions with us for the period 01 Sep 2013 to 30 Sep 2013. You can write to us or call us if you need any assistance or clarifications.     <br/>Thanks for your business.<br/>Regards<br/>Zillium Inc"
        ]);

        $api = $client->request('post', 'https://books.zoho.com/api/v3/contacts/' . $contact_id . '/statements/email?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);

        return $api->getBody();
    }


    // SALES ORDER 
    public function createSalesOrder($customer_id, $contact_person_id, $name,$rate,$description)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();
       $item_id = $this->createItem($name,$rate,$description);

        $data = json_encode([
            "customer_id" => $customer_id,
            "contact_persons" => [$contact_person_id],
            "date" => Carbon::now()->format('Y-m-d'),
            "reference_number" => "REF-001",

            "line_items" => [

                [
                    "item_id" => $item_id,

                ]

            ]

        ]);


        $api = $client->request('post', 'https://books.zoho.com/api/v3/salesorders?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }
    public function listSalesOrder($name)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();




        $api = $client->request('get', 'https://books.zoho.com/api/v3/salesorders?organization_id=698673313&&customer_name=' . $name, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }

    public function retrieveSalesOrder($sales_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();




        $api = $client->request('get', 'https://books.zoho.com/api/v3/salesorders/' . $sales_id . '?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }
    public function deleteSalesOrder($sales_id)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();




        $api = $client->request('delete', 'https://books.zoho.com/api/v3/salesorders/' . $sales_id . '?organization_id=698673313', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }


    // Expenses 

    public function createExpenses($name,$rate,$description)
    {
        $oauth = Accounting::first();
        $client = new \GuzzleHttp\Client();
        $item_id = $this->createItem($name,$rate,$description);

        $data = json_encode([
            "account_id" => 2059437000000082054,
            "date" => Carbon::now()->format('Y-m-d'),
            "amount" => $rate,
            "reference_number" => "null",
            "description" => $description,

            "line_items" => [

                [
                    "item_id" => $item_id,

                ]

            ]

        ]);


        $api = $client->request('post', 'https://books.zoho.com/api/v3/expenses?organization_id=698673313&JSONString=' . $data, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Zoho-oauthtoken ' . $oauth->access_token,


            ],


        ]);





        return $api->getBody();
    }
}
