<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufacturerProductMaterials extends Model
{
    protected $table = 'manufacturer_product_materials';
    protected $fillable = [
        'user_id',
        'cost_of_production',
  
        
    ];
}
