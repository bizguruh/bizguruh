<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BapTemplate extends Model
{

    protected $table = 'bap_templates';
    protected $fillable =['responses','month','week','template'];

    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
