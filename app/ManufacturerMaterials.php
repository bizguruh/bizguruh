<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufacturerMaterials extends Model
{
     protected $table = 'manufacturer_materials';
    protected $fillable = [

        'user_id',
        'material_name',
        'material_no',
        'quantity_per_unit',
        'in_stock',
        'cost_price'
    ];
}
