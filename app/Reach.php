<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reach extends Model
{
    protected $table = 'reach';
    protected $fillable = [
        'user_id',
        'vendor_id',
        'product_id'
    ];
}
