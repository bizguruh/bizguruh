<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $table = 'statement';
    protected $fillable = [
        'user_id',
        'revenue',
        'cogs',
        'gross_profit',
        'opex',
        'pbt',
        'net_profit',
        'month',
    ];
}
