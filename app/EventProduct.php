<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventProduct extends Model
{
    //
    protected $table = 'event_products';

    protected $fillable = ['title', 'description', 'date', 'address', 'city', 'time', 'endDate', 'endTime',
        'state', 'country', 'quantity', 'convenerName', 'aboutConvener', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function eventScheduleTime()
    {
     return $this->hasMany('App\EventScheduleTime', 'product_id');
   }
}
