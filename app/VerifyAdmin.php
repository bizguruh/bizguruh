<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyAdmin extends Model
{
    //
    protected $guarded = [];

    public function adminUser(){
        return $this->belongsTo('App\AdminUser', 'admin_user_id');
    }
}
