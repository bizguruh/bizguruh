<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';
    
    protected $fillable = [     
      'product_id',
      'vendor_id',
      'user_id',
      'like' ,
      'unlike'
    ];
}
