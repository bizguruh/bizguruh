<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    //
    protected $table = 'product_sub_categories';

    protected $fillable = ['name', 'description', 'sub_category_id'];

    public function category(){
        return $this->belongsTo('App/ProductCategory');
    }

    public function subCategoryBrand(){
        return $this->hasMany('App\ProductSubCategoryBrand', 'sub_brand_category_id');
    }
}
