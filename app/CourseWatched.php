<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseWatched extends Model
{

    protected $table = 'courses_watched';
    protected $fillable = [  'user_id', 'course_id', 'vendor_id', 'video_id', 'total_content','type','order_id','time'];
}
