<?php

namespace App\Console\Commands;

use App\Notify;
use Carbon\Carbon;
use Illuminate\Console\Command;

class HelloWorld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'say:hello';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' Sending notify to where needed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notify = new Notify;
        $notify->send();
        $this->line('Running my job at notify vendor ' . Carbon::now('Africa/Lagos'));
        $this->line('Ending my job at notify vendor ' . Carbon::now('Africa/Lagos'));
    }
}
