<?php

namespace App\Console\Commands;

use App\Notify;
use Carbon\Carbon;
use Illuminate\Console\Command;


class AccountReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder to record your daily accounting activities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $account = new Notify;
        $account->accountReminder();
        $this->line('Running my job at account reminder ' . Carbon::now('Africa/Lagos'));
        $this->line('Ending my job at account reminder ' . Carbon::now('Africa/Lagos'));
    }
}
