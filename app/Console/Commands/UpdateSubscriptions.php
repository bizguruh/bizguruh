<?php

namespace App\Console\Commands;

use auth;
use Carbon\Carbon;
use App\UserSubscriptionPlan;
use Illuminate\Console\Command;

class UpdateSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updatesusers subscription status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscription = UserSubscriptionPlan::where('verify', 1)->get();
        $current = Carbon::now();
     
        foreach ($subscription as $sub) {
           if($sub->level > 0){
            $end = Carbon::parse($sub->endDate);
            $diff = $current->greaterThan($end);
            if ($diff) {
                $sub->status = 'expired';
                $sub->save();
              
            }else{
                $sub->status = 'active';
                $sub->save();
            }
           }else{
            $sub->status = 'active';
            $sub->save();
           }
        }
        
       
    }
}
