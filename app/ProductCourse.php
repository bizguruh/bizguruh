<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCourse extends Model
{
    //
    protected $table = 'products_courses';

    protected $fillable = ['title', 'overview', 'duration', 'courseModules', 'courseObjectives', 'whoThisCourseFor', 'keyBenefits',
        'whenYouComplete', 'faq', 'online', 'offline', 'level', 'certification',
        'aboutAuthor', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }


    public function productCourseModule()
    {
        return $this->hasMany('App\ProductCourseModule', 'products_courses_id', 'id');
    }

    public function courseToWhatLearn()
    {
        return $this->hasMany('App\CourseWhatYouWillLearn', 'product_course_id', 'id');
    }

}
