<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseWhatYouWillLearn extends Model
{
    //
    protected $table = 'course_what_you_will_learns';

    protected $fillable = ['whatYouWillLearn', 'product_course_id'];

    public function productCourses()
    {
        return $this->belongsTo('App\ProductCourse');
    }
}
