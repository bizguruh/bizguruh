<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMarketReport extends Model
{
    //
    protected $table = 'product_market_report';

    protected $fillable = ['title', 'contributors', 'aboutThisReport', 'documentName', 'overview',
        'tableOfContents', 'excerpt', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
