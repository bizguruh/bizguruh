<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SignupMail extends Mailable
{
    use Queueable, SerializesModels;
     public $newUser;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newUser)
    {
         $this->newUser = $newUser;
        //
    }

/**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.signupMail')->subject('Welcome to Bizguruh');
    }
}
