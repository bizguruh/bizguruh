<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyVendorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $vendorUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vendorUser)
    {
        //
        $this->vendorUser = $vendorUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.verifyVendor');
    }
}
