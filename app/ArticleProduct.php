<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleProduct extends Model
{
    //
    protected $table = 'article_products';

    protected $fillable = ['title', 'description', 'tags', 'media', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
