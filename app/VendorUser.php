<?php

namespace App;

use App\GroupChat;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class VendorUser extends Authenticatable
{
    //
    use HasApiTokens, Notifiable;

    protected $table = 'vendor_users';


    protected $fillable = [
        'storeName','username', 'email', 'password', 'type', 'bio', 'subjectMatter', 'topic', 'valid_id'];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function product()
    {
        return $this->hasMany('App\Product');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyVendor');
    }

    public function vendorDetail()
    {
        return $this->hasOne('App\VendorDetail');
    }

    public function vendorSettings()
    {
        return $this->hasOne('App\VendorSettings');
    }

    public function vendorShipping()
    {
        return $this->hasOne('App\VendorShipping');
    }
    public function message()
    {
        return $this->hasMany(MessagePrivate::class);
    }
    public function vendorGroupMessage()
    {
        return $this->hasMany(GroupChat::class);
    }
}
