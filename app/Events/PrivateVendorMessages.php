<?php

namespace App\Events;

use App\VendorUser;
use App\MessagePrivate;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PrivateVendorMessages implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
 
  
    public $vendor;
    public $message;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VendorUser $vendor, MessagePrivate $message)
    {
    
        $this->vendor = $vendor;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat.'.$this->vendor->id).$this->message->user_id;
    }
}
