<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interaction extends Model
{
    protected $table = 'interactions';
    protected $fillable = [
        'vendor_id',
        'likes',
        'shares',
        'views'
    ];
}
