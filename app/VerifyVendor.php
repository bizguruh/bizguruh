<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyVendor extends Model
{
    //
    protected $guarded = [];

    public function vendorUser()
    {
        return $this->belongsTo('App\VendorUser', 'vendor_user_id');
    }
}
