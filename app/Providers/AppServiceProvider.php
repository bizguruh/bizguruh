<?php

namespace App\Providers;

use JD\Cloudder\Facades\Cloudder;
use Illuminate\Support\Facades\App;
// use App\Services\SocialUserResolver;
use Illuminate\Support\Facades\URL;
use App\Resolvers\SocialUserResolver;
use Illuminate\Support\Facades\Schema;
// use Hivokas\LaravelPassportSocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\UrlGenerator;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];
    public function boot(UrlGenerator $url)
    {
        Schema::defaultStringLength(191);
        if (App::environment('production')) {
            $this->app['request']->server->set('HTTPS','on');
        }
        if (env('REDIRECT_HTTPS')) {
            $this->app['request']->server->set('HTTPS', true);
        }
      
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    }
}
