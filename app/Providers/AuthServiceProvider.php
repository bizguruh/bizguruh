<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Passport::routes();

        Gate::define('super-admin', function($admin){
            return ($admin->role == 0);
        });

        Gate::define('store-admin', function($user){
           return ($user->role == 1 || $user->role == 0 );
        });

        Gate::define('web-admin', function($user){
            return ($user->role == 2 || $user->role == 0 );
        });

        Gate::define('vendor-user', function($vendor){
            return $vendor->type == 'vendor';
        });

        Gate::define(   'expert-user', function($expert){
           return $expert->type == 'expert';
        });
    }
}
