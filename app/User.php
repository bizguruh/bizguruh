<?php

namespace App;

use App\Bap;
use App\GroupChat;
use App\BapTemplate;
use App\SocialAccounts;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastName', 'email', 'phoneNo', 'password', 'type','business_name',
        'bio', 'subjectMatter', 'topic','specialToken','vendor_user_id','business_type','age','gender','first_name','location','phone_code','logo','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userDetail()
    {
        return $this->hasMany('App\UserDetail');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id', 'id');
    }

    public function userSubsriptionPlan()
    {
        return $this->hasMany('App\UserSubscriptionPlan', 'user_id', 'id');
    }
    public function subscriptionTemp()
    {
        return $this->hasMany('App\SubscriptionTemp', 'user_id', 'id');
    }


    public function userFollowing()
    {
        return $this->hasMany('App\UserFollowing', 'user_id', 'id');
    }

    public function linkedSocialAccounts()
    {
        return $this->hasMany(SocialAccounts::class);
    }

    public function findForPassport($username)
    {
        if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $this->where('phoneNo', $username)->first();
        }
      
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $this->where('email', $username)->first();
        }
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function privateMessages()
    {
        return $this->hasMany(MessagePrivate::class);
    }

    public function groupMessages()
    {
        return $this->hasMany(GroupChat::class);
    }

    public function baps()
    {
        return $this->hasMany(Bap::class);
    }

    public function bapTemplates()
    {
        return $this->hasMany(BapTemplate::class);
    }
    public function bapDrafts()
    {
        return $this->hasMany(BapDraft::class);
    }
}
