<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCount extends Model
{
    //
    protected $table = 'article_counts';
    protected $fillable =  ['user_id','articleId', 'articleCount'];


}
