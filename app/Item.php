<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    protected $fillable = [
        'user_id',
        'item_name',
        'item_desc',
        'rate',
        'in_stock'
    ];
}
