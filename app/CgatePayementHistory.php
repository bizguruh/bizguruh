<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CgatePayementHistory extends Model
{
   // "passBackReference": "12341820220", "traceId": "12341820220",
    //             "paymentReference": "03848982793092", "customerRef": "12345", "responseCode":"00", "merchantId":"93493MIU93832", "mobileNumber":"0808***7273",
    //             "amount":1000, "transactionDate":"2020-07-01T18:20:16.465107+01:00", "shortCode":"894",
    // "currency":"NGN",
    // "channel":"USSD",
    // "hash": "093483ierl8w7s0s0-skj2j3k3wsj383mdlw838",

    protected $table = 'cgate_payment_history';
    protected $fillable = [ "passBackReference","traceId","paymentReference","customerRef", "responseCode","merchantId","mobileNumber", "amount","transactionDate","shortCode","currency","channel","hash"];
}
