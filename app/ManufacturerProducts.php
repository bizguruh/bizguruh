<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufacturerProducts extends Model
{
    protected $table = 'manufacturer_products';
    protected $fillable = [

        'user_id',
        'product_name',
        'product_no',
        'product_desc',
        'quantity_per_batch',
        'in_stock',
        'selling_price'
    ];
}
