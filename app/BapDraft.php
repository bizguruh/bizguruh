<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BapDraft extends Model
{
    protected $table = 'bap_draft';
    protected $fillable =['responses','month','week','template'];

    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
