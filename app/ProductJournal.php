<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductJournal extends Model
{
    //
    protected $table = 'products_journal_article';

    protected $fillable = ['title', 'abstract', 'author', 'volume', 'issue',
        'month/year', 'pages', 'publicationDate', 'publisher',  'isbn', 'sponsor',  'tableOfContents', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
