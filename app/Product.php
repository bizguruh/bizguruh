<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = ['title', 'prodType', 'type', 'quantity', 'overview', 'publisher', 'furtherDescription', 'readOnline',

        'copyType', 'description', 'author', 'videoPrice', 'softCopyPrice', 'hardCopyPrice', 'coverImage', 'fileType', 'excerpts', 'readOnlinePrice', 'pageNo',

        'tableOfContent', 'aboutAuthor', 'audioCopy','audioPrice', 'hardCopy', 'softCopy', 'category_id'];

    protected $hidden = ['document'];



    public function productChapter()
    {
        return $this->hasOne('App\ProductChapter');
    }

    public function marketReport()
    {
        return $this->hasOne('App\ProductMarketReport');
    }

    public function marketResearch()
    {
        return $this->hasOne('App\ProductMarketResearch');
    }


    public function productBook()
    {
        return $this->hasOne('App\ProductBook');
    }

    public function productEvent()
    {
        return $this->hasOne('App\EventProduct');
    }

    public function productArticle()
    {
        return $this->hasOne('App\ArticleProduct');
    }

    public function faqs(){
        return $this->hasMany('App\FAQ', 'productId', 'id');
    }

    public function productWhitePaper()
    {
        return $this->hasOne('App\ProductWhitePaper');
    }

    public function productJournal()
    {
        return $this->hasOne('App\ProductJournal');
    }

    public function productCourse()
    {
        return $this->hasOne('App\ProductCourse');
    }

   /* public function productVideo()
    {
        return $this->hasMany('App\ProductVideoDetail', 'productId', 'id');
    }*/

    public function productImage()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function vendorUser()
    {
        return $this->belongsTo('App\VendorUser');
    }

   /* public function productCategory()
    {
        return $this->belongsTo('App\ProductCategory');
    }*/

    public function productCategories()
    {
        return $this->belongsTo('App\ProductCategory');
    }

    public function productSubjectMatter()
    {
        return $this->belongsTo('App\ProductSubCategory');
    }

    public function cart(){
        return $this->belongsTo('App\Cart', 'productId');
    }


}
