<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorForm extends Model
{
    protected  $table  = 'vendor_forms';
    protected $fillable = ['vendor_id','group','form_title','template','product_id'];
}
