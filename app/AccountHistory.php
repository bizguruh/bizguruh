<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountHistory extends Model
{
    //
    protected $table = 'account_history';
    protected $fillable = ['user_id','total_income','total_expenses','total_balance','month'];
}
