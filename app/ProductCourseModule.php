<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCourseModule extends Model
{
    //
    protected $table = 'product_course_modules';

    protected $fillable = ['title', 'description', 'file', 'fileName',
        'author', 'duration', 'products_courses_id'];

    public function productCourses()
    {
        return $this->belongsTo('App\ProductCourse');
    }

}
