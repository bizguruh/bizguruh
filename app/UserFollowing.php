<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFollowing extends Model
{
    protected $table = 'user_followings';
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
