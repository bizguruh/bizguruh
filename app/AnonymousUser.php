<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnonymousUser extends Model
{
    //
    protected $table = 'anonymous_user';

    protected $fillable = ['anonymousUser', 'anonymousCount', 'anonymousProductId', 'duration', 'durationCount'];
}
