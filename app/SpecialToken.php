<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialToken extends Model
{
    //
    protected $table = 'special_token';
   protected $fillable = [ 'user_id','token','verify','price','mail','users','start_date', 'end_date'];
}
