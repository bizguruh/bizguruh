<?php

namespace App\Http\Controllers;

use App\Product;
use Carbon\Carbon;
use App\ArticleCount;
use App\AnonymousUser;
use App\UserFollowing;
use App\ArticleProduct;
use App\UserPreference;
use App\ProductCategory;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use App\ProductSubCategoryBrand;
use App\Http\Resources\Product as ProductResource;

class ArticleProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getArticleCount($id)
    {
        return $count = ArticleCount::where('user_id', $id)->first();
    }
    public function clearCount()
    {
    }


    public function getArticleFromNetwork()
    {
        $id = auth('api')->user()->id;

        $user = UserFollowing::where('user_id', $id)->get();
        foreach ($user as $users) {
            $product = Product::where('prodType', 'Articles')->where('vendor_user_id', $users->vendorId)->where('draftValue', '=', 'N')->where('verify', 1)->get();
            return ProductResource::collection($product);
        }
    }

    public function getAllTopicForArticles()
    {
        $topic = [];
        $products = Product::where('prodType', 'Articles')->where('verify', 1)->select('sub_category_brand_id')->get();

        foreach ($products as $product) {
            array_push($topic, json_decode($product->sub_category_brand_id));
        }

        //return $topic;

        $topicArray = [];

        foreach ($topic as $topics) {
            foreach ($topics as $top) {
                array_push($topicArray, ProductSubCategoryBrand::where('id', $top)->first());
            }
        }

        return array_unique($topicArray);
        // $body = json_decode($product, true);
        // return $body;
    }

    public function getAllArticle(Request $request)
    {
        switch ($request->type) {
            case 'all':
                $product = Product::where('prodType', 'Articles')->where('verify', 1)->get();
                return ProductResource::collection($product);
            default:
                return [];
        }
    }

    public function getArticleBasedOnPopularity()
    {
        return  $products = Product::where('prodType', 'Articles')->where('verify', 1)->with('productArticle')->orderBy('title', 'desc')->paginate(5);
    }


    public function getArticleBasedOnUserPreferences()
    {
        $userId =   auth('api')->user()->id;
        $articlePreference = [];
        $userp = UserPreference::where('userId', $userId)->get();

        $products = Product::where('prodType', 'Articles')->where('verify', 1)->paginate(6);

        foreach ($userp as $userpre) {
            foreach ($products as $product) {
                if ($userpre->typeId === $product->sub_category_id) {
                    $productSub = ProductSubCategory::where('id', $product->sub_category_id)->first();
                    $product->setAttribute('subjectMatter', $productSub);
                    $product->setAttribute('articlesDisplay', $product->productArticle);
                    array_push($articlePreference, $product);
                }
            }
        }


        return $articlePreference;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public $readCount = 0;


    public function getSingleFreeArticle(Request $request)
    {
        $product = Product::where('id', $request->id)->where('verify', 1)->first();
        $art = Product::where('id', $request->id)->first()->productArticle;
        $deart = Product::where('id', $request->id)->first()->productArticle;
        $detail = Product::where('id', $request->id)->first()->vendorUser;


        $art->makeHidden('description');
        $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);

        $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
        $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
        $art->setAttribute('content', substr(strip_tags($deart->description), 0, 500));
        $art->setAttribute('readability', 'none');
        return $art;

        // if ((int) $product->subscriptionLevel > 0) {

        //     $art = Product::where('id', $request->id)->first()->productArticle;
        //     $deart = Product::where('id', $request->id)->first()->productArticle;
        //     $detail = Product::where('id', $request->id)->first()->vendorUser;


        //     $art->makeHidden('description');
        //     $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);

        //     $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
        //     $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
        //     $art->setAttribute('content', substr(strip_tags($deart->description), 0, 500));
        //     $art->setAttribute('readability', 'none');
        //     return $art;
        // } elseif ((int) $product->subscriptionLevel === 0) {

        //     $requestToken = $request->header('X-CSRF-TOKEN');
        //     $anonymous = AnonymousUser::where('anonymousUser', $requestToken)->first();
        //     if (!is_null($anonymous)) {


        //         if ($anonymous->anonymousCount >= 2) {
        //             $art = Product::where('id', $request->id)->first()->productArticle;
        //             $now = Carbon::now('Africa/Lagos');

        //             $endNow = $art->updated_at->diffInDays($now);

        //             $endNow = (int) $endNow;

        //             $deart = Product::where('id', $request->id)->first()->productArticle;
        //             $detail = Product::where('id', $request->id)->first()->vendorUser;

        //             $art->makeHidden('description');
        //             $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);

        //             $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
        //             $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
        //             $art->setAttribute('content', substr(strip_tags($deart->description), 0, 500));
        //             $art->setAttribute('readability', 'none');
        //             return $art;
        //         } else {
        //             $anonymous->anonymousUser = $requestToken;
        //             $anonymous->anonymousCount = $anonymous->anonymousCount + 1;
        //             $anonymous->save();

        //             $art =  Product::where('id', $request->id)->first()->productArticle;
        //             $detail = Product::where('id', $request->id)->first()->vendorUser;

        //             $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
        //             $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
        //             $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
        //             $art->setAttribute('content', $art->description);
        //             $art->setAttribute('readability', 'yes');

        //             return $art;
        //         }
        //     } else {
        //         $anonymousUser = new AnonymousUser();

        //         $anonymousUser->anonymousUser = $requestToken;
        //         $anonymousUser->anonymousCount = 1;
        //         $anonymousUser->save();

        //         $art =  Product::where('id', $request->id)->first()->productArticle;
        //         $detail = Product::where('id', $request->id)->first()->vendorUser;

        //         $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
        //         $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
        //         $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
        //         $art->setAttribute('content', $art->description);
        //         $art->setAttribute('readability', 'yes');

        //         return $art;
        //     }
        // }


        // $product = Product::where('id', $id)->where('verify', 1)->get();
        // return ProductResource::collection($product);
    }

    public function getSingleArticle(Request $request)
    {
        $userId =   auth('api')->user()->id;
        $now = Carbon::now('Africa/Lagos');
        $articeCheckArray = [];

        $userPlan = UserSubscriptionPlan::where('user_id', $userId)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->first();
      
        $product = Product::where('id', $request->id)->first();


        $endDate =  Carbon::parse($userPlan->endDate);
        $checkDate = $endDate->diffInDays($now);
        $checkDate = (int) $checkDate;

        # code...

      
        if (($userPlan->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType)) {
            $art =  Product::where('id', $request->id)->first()->productArticle;
            $detail = Product::where('id', $request->id)->first()->vendorUser;

            $productId = Product::where('id', $request->id)->first();
            $product->title = $product->title + 1;
            $product->save();
            // $productId = Product::where('id', $request->id)->first()->productArticle->id;
            /* $artProduct = ArticleProduct::where('id', $productId)->first();
             $artProduct->articleCount = $artProduct->articleCount + 1;
             $artProduct->save();*/
            // return $detail->type;
            $count = ArticleCount::where('user_id', $userId)->get();

            if ($userPlan->level == "0" || $userPlan->level == "2") {
                if (count($count) === 0) {
                    # code...
                    ArticleCount::create([
                        'user_id' => $userId,
                        'articleId' => $request->id,
                        'articleCount' => 0,

                    ]);

                    $art = Product::where('id', $request->id)->first()->productArticle;
                    $detail = Product::where('id', $request->id)->first()->vendorUser;


                    $deart = Product::where('id', $request->id)->first()->productArticle;
                    $art->setAttribute('content', $art->description);
                    $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
                    $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
                    $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
                    $art->setAttribute('readability', 'yes');


                    return $art;
                } else {
                    if (count($count) < 4) {
                        $counting = ArticleCount::where('user_id', $userId)->where('articleId', $request->id)->first();
                        if (is_null($counting)) {
                            ArticleCount::create([
                                'user_id' => $userId,
                                'articleId' => $request->id,
                                'articleCount' => 0,
        
                            ]);
                        }

                     
                                                  
                        
                        $art = Product::where('id', $request->id)->first()->productArticle;
                        $detail = Product::where('id', $request->id)->first()->vendorUser;


                        $deart = Product::where('id', $request->id)->first()->productArticle;
                        $art->setAttribute('content', $art->description);
                        $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
                        $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
                        $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
                        $art->setAttribute('readability', 'yes');


                        return $art;
                    } else {
                        $art = Product::where('id', $request->id)->first()->productArticle;
                        $detail = Product::where('id', $request->id)->first()->vendorUser;


                        $deart = Product::where('id', $request->id)->first()->productArticle;
                        $art->makeHidden('description');
                        $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
                        $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
                        $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
                        $art->setAttribute('content', substr(strip_tags($deart->description), 0, 500));
                        $art->setAttribute('readability', 'none');
                        return $art;
                    }
                }
            } else {
                $art = Product::where('id', $request->id)->first()->productArticle;
                $detail = Product::where('id', $request->id)->first()->vendorUser;


                $deart = Product::where('id', $request->id)->first()->productArticle;
                $art->setAttribute('content', $art->description);
                $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
                $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
                $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
                $art->setAttribute('readability', 'yes');


                return $art;
            }

            $art = Product::where('id', $request->id)->first()->productArticle;
            $detail = Product::where('id', $request->id)->first()->vendorUser;


            $deart = Product::where('id', $request->id)->first()->productArticle;
            $art->setAttribute('content', $art->description);
            $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
            $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
            $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
            $art->setAttribute('readability', 'yes');




            return $art;
        } else {
            $art = Product::where('id', $request->id)->first()->productArticle;
            $detail = Product::where('id', $request->id)->first()->vendorUser;


            $deart = Product::where('id', $request->id)->first()->productArticle;
            $art->makeHidden('description');
            $art->setAttribute('coverImage', Product::where('id', $request->id)->first()->coverImage);
            $art->setAttribute('writerDetail', $detail->type === 'expert' ? $detail : null);
            $art->setAttribute('subjectMatter', ProductSubCategory::where('id', Product::where('id', $request->id)->first()->sub_category_id)->first()->name);
            $art->setAttribute('content', substr(strip_tags($deart->description), 0, 500));
            $art->setAttribute('readability', 'none');
            return $art;
        }
        
        return response()->json([
            'data' => 'error'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArticleProduct  $articleProduct
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleProduct $articleProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArticleProduct  $articleProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleProduct $articleProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArticleProduct  $articleProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleProduct $articleProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArticleProduct  $articleProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleProduct $articleProduct)
    {
        //
    }
}
