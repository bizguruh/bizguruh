<?php

namespace App\Http\Controllers;


use Image;
use App\User;
use App\Insight;
use App\Industry;
use Carbon\Carbon;
use App\VendorUser;
use App\AccountUser;
use App\VendorDetail;
use App\VerifyVendor;
use App\VendorSettings;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use App\Mail\VerifyVendorMail;
use App\Mail\AccountingMail;
use App\ProductSubCategoryBrand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\VendorRegisterRequest;
use App\Http\Resources\VendorDetail as VendorDetailResource;

class VendorUserController extends Controller
{

    public function getVendorSubjectMatter()
    {
        return ProductSubCategory::all();
    }

    public function getVendorInsight()
    {
        return Insight::all();
    }

    public function getVendorTopic()
    {
        return ProductSubCategoryBrand::all();
    }

    public function getVendorIndustries()
    {
        return Industry::all();
    }

    



    protected function create(VendorRegisterRequest $request)
    {
        // return $request;




        $vendorUser = VendorUser::create([
            'bio' => $request['bio'],
            'type' => $request['userSwitch'],
            'subjectMatter' => $request['subjmatter'],
            'topic' => $request['mytopic'],
            'storeName' => $request['storeName'],
            'username' => strtolower($request['username']),
            'occupation' => $request['occupation'],
            'address' => $request['address'],
            'email' => $request['email'],
            'valid_id' => $request['image'],
            'password' => Hash::make($request['password']),
        ]);


        $verifyVendor = VerifyVendor::create([
            'vendor_user_id' => $vendorUser->id,
            'token' => str_random(40)
        ]);

        $vendorDetail = VendorDetail::create([
            'vendor_id' =>  $vendorUser->id,
        ]);

        $vendorSettings = new VendorSettings();
        $vendorSettings->vendor_id = $vendorUser->id;
        $vendorSettings->save();

        $user =  User::create([
            'name' => $request['storeName'],
            'lastName' => 'null',
            'email' => $request['email'],
            'phoneNo' => '8799',
            'password' => Hash::make($request['password']),
            'count' => '1',
            'specialToken' => 'null',
            'vendor_user_id' => $vendorUser->id
        ]);

        $user = User::where('email', $request->email)->first();
        $userSubscriptionPlan = new UserSubscriptionPlan();
        $userSubscriptionPlan->price = 0;
        $userSubscriptionPlan->startDate = Carbon::now('Africa/Lagos');
        $userSubscriptionPlan->duration = null;
        $userSubscriptionPlan->level = 1;
        $userSubscriptionPlan->name = "contributor";
        $userSubscriptionPlan->endDate = Carbon::now('Africa/Lagos')->addYear();
        $userSubscriptionPlan->user_id = $user->id;
        $userSubscriptionPlan->verify = 1;
        $userSubscriptionPlan->type = 'expert';
        $userSubscriptionPlan->save();

        // AccountUser::create([
        //     'user_id' => $user->id,
        //     'name' => $request['storeName'],
        //     'email' => $request['email'],
        //     'password' => Hash::make('admin'),
        //     'role' => 'admin',
        //     'verify' => 1
           
        // ]);



        Mail::to($vendorUser->email)->send(new VerifyVendorMail($vendorUser));
        // Mail::to($vendorUser->email)->send(new AccountingMail($vendorUser));



        return response()->json([
            'success' => true,
            'message' => 'Registration Successful',
            'data' => $vendorUser
        ]);
    }

    public function allowUser(Request $request){
        $vendor = VendorUser::where('email',$request->email)->first();
      
        return $vendor;
    }

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::guard('vendor_user')->user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised Access'], 401);
        }
    }
    public function checkVendor($data)
    {
        $email = VendorUser::where('email', $data)->where('verified', 1)->first();
        $storeName = VendorUser::where('storeName', $data)->where('verified', 1)->first();

        if ($email !== null) {
            return 'email_error';
        } else if ($storeName !== null) {
            return 'storeName_error';
        } else {
            return 'good';
        }
    }


    public function verifyVendor($token)
    {

        $verifyVendor = VerifyVendor::where('token', $token)->first();
        if (isset($verifyVendor)) {
            $vendor = $verifyVendor->vendorUser;
            if (!$vendor->verified) {
                $verifyVendor->vendorUser->verified = 1;
                $verifyVendor->vendorUser->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/vendor/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/auth/login')->with('status', $status);
    }





    public function updateVendorProfile(Request $request)
    {
        $vendorProfile = VendorDetail::where('vendor_id', $request->vendor_id)->first();

        $vendorProfile->firstName = $request->firstName;
        $vendorProfile->lastName = $request->lastName;
        $vendorProfile->address = $request->address;
        $vendorProfile->storeName = $request->storeName;
        $vendorProfile->city = $request->city;
        $vendorProfile->state = $request->state;
        $vendorProfile->country = $request->country;
        $vendorProfile->bio = $request->bio;
        $vendorProfile->bank_name = $request->bank_name;
        $vendorProfile->account_no = $request->account_no;

        $profileImage = $request->image;

        if (strpos($profileImage, 'data:image/') !== false) {
            $profileImageName = "profile-" . time() . ".png";

            $mainImagePath = public_path('images/vendor' . '/' . $profileImageName);


            Image::make(file_get_contents($profileImage))->save($mainImagePath);

            $oldfilename = $vendorProfile->image;

            $vendorProfile->image = $profileImageName;

            Storage::delete($oldfilename);
        } else { }



        $vendorProfile->save();

        return back();
    }

    public function getVendorProfileDetail($storeName)
    {
     
        $vendor = VendorDetail::where('storeName', $storeName)->get();

        return VendorDetailResource::collection($vendor);
    }

    public function postVendorSettings(Request $request)
    {
        $vendorSettings = VendorSettings::where('vendor_id', '=', $request->id)->first();

        $vendorSettings->storeName = $request->storeName;
        $vendorSettings->sidebarBackgroundColor =  $request->sidebarBackgroundColor;
        $vendorSettings->sidebarColor = $request->sidebarColor;
        $vendorSettings->sidebarFontSize = $request->sidebarFontSize;
        $vendorSettings->sidebarMargin = $request->sidebarMargin;
        $vendorSettings->topMenuBackgroundColor = $request->topMenuBackgroundColor;
        $vendorSettings->topMenuColor = $request->topMenuColor;
        $vendorSettings->topMenuFontSize = $request->topMenuFontSize;
        $vendorSettings->topMenuMargin = $request->topMenuMargin;
        $vendorSettings->overallBackgroundColor = $request->overallBackgroundColor;
        $vendorSettings->overallColor = $request->overallColor;
        $vendorSettings->overFontSize = $request->overFontSize;
        $vendorSettings->save();

        return response()->json([
            "success" => 200,
            "data" => $vendorSettings
        ]);
    }

    public function getVendorSettings($settings)
    {
        $vendorSettings = VendorSettings::where('vendor_id', '=', $settings)->first();
        return response()->json([
            'status' => '200',
            'data' => $vendorSettings
        ]);
    }

    public function getAllVendor()
    {
        return VendorUser::where('verified',1)->get();
    }
}
