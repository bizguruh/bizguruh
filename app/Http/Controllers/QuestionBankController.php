<?php

namespace App\Http\Controllers;

use App\QuestionBank;
use Illuminate\Http\Request;
use App\Http\Resources\QuestionBank as QuestionBankResource;


class QuestionBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return QuestionBank::all();
    }

    public function getQuestionBySubjectMatterId($id){


        $question =  QuestionBank::where('subjectMatter', $id)->paginate(10);
        return QuestionBankResource::collection($question);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, array(
            'question' => 'required',
            'answer' => 'required',
            'subjectMatter' => 'required',
            'subjectMatterName' => 'required',
        ));

        $questionBank = new QuestionBank();
        $questionBank->question = $request->question;
        $questionBank->answer = $request->answer;
        $questionBank->subjectMatter = $request->subjectMatter;
        $questionBank->subjectMatterName = $request->subjectMatterName;
        $questionBank->topicName = $request->topicName;
        $questionBank->topic = $request->topic;
        $questionBank->save();
        return $questionBank;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionBank $questionBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function edit( $questionBank)
    {
        //
        return QuestionBank::where('id', $questionBank)->first();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionBank $questionBank)
    {
        //
        $this->validate($request, array(
            'question' => 'required',
            'answer' => 'required',
            'subjectMatter' => 'required',
            'subjectMatterName' => 'required',
        ));

        $questionBank = QuestionBank::where('id', $request->id)->first();
        $questionBank->question = $request->question;
        $questionBank->answer = $request->answer;
        $questionBank->subjectMatter = $request->subjectMatter;
        $questionBank->subjectMatterName = $request->subjectMatterName;
        $questionBank->topicName = $request->topicName;
        $questionBank->topic = $request->topic;
        $questionBank->save();
        return $questionBank;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function destroy($questionBank)
    {
        //

        $questionBank = QuestionBank::where('id', $questionBank)->first();
        $questionBank->delete();
        return $questionBank;
    }
}
