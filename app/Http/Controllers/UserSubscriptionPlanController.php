<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Product;
use App\Industry;
use Carbon\Carbon;
use App\OrderDetail;
use App\Notification;
use App\SpecialToken;
use App\Subscription;
use App\ProductCategory;
use App\OrderDetailVideo;
use App\SubscriptionTemp;
use App\ProductSubCategory;
use App\VendorSubscription;
use App\AccountSubscription;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use Illuminate\Support\Facades\DB;

class UserSubscriptionPlanController extends Controller
{
    public $subscriptionData = ['hhhh'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getSubscriptionPlanForUser()
    {
        $userId =   auth('api')->user()->id;
   
        $userPlans = UserSubscriptionPlan::where('user_id', $userId)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();

        return $userPlans;
    }

    public function subscriptionPlanForUser($id)
    {
      
        $userPlan = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();
      
        return $userPlan;
    }


    public function watchEpisodeMedia(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;
        $user = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->first();
        $product = Product::where('id', $request->id)->where('verify', 1)->first();
       
        if (!is_null($user)) {
            if (($user->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType)) {
                return    $productArray = $product->productChapter->productVideoDetail->where('id', $request->vid)->first();
            } else {
                return response()->json([
                        'status' =>'failed'
                    ]);
            }
        } else {
            return response()->json([
                'status' =>'failed'
            ]);
        }
    }


    public function addVideoToLibrary(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;
        $user = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->first();
      
        $product = Product::where('id', $request->id)->where('verify', 1)->first();

        if (!is_null($user)) {
            $endDate = Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int)$checkDate;

         
                if (($user->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType)) {
                    $checkSave = false;
                    $ord = Order::where('user_id', $id)->get();
                    $ordDetail = OrderDetail::where('productId', $product->id)->where('itemPurchase', '=', 'itemPurchase')->first();
                    $orders = DB::table('orders')
                        ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
                        ->select('user_id', 'productId')
                        ->where('productId', $product->id)
                        ->where('user_id', $id)
                        ->where('itemPurchase', '=', 'itemPurchase')
                        ->get();
                    
                    if (count($orders) === 0) {
                        $checkSave = true;
                    }
        
        
                    if ($checkSave) {
                        $order = new Order();
                        $order->deliveryId = null;
                        $order->paidAmount = $user->price;
                        $order->user_id = $id;
                        $order->verify = 1;
                        $order->orderNum = 'Biz-' . rand(1, 1000000);
                        $order->referenceNo = 'Subscription';
                        $order->totalAmount = $user->price;
                        $order->paymentType = 'Paystack';
                        $order->save();
        
                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;
                        $orderDetail->productTitle = $product->productChapter->title;
                        // $orderDetail->episodeTitle = $request->name;
                        $orderDetail->itemPurchase = 'itemPurchase';
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->amount = $user->price;
                        if ($request->prodType === 'Videos') {
                            $orderDetail->prodType = 'Video Insight Subscription';
                        } elseif ($request->prodType === 'Podcast') {
                            $orderDetail->prodType = 'Podcast Insight Subscription';
                        }
                        $orderDetail->category = $request->getType;
                        $orderDetail->save();
        
                        $productArrays = $product->productChapter->productVideoDetail;
                        foreach ($productArrays as $productArray) {
                            $ordv = new OrderDetailVideo();
                            $ordv->title = $productArray->title;
                            $ordv->description = $productArray->description;
                            $ordv->videoFile = $productArray->videoFile;
                            $ordv->orderNum = 'Biz-' . rand(1, 1000000);
                            $ordv->episodeGuest = $productArray->guest;
                            $ordv->episodedescription = $productArray->aboutGuest;
                            $ordv->myUpdatedDate = $productArray->date;
                            $ordv->mediaPublicId = $productArray->videoPublicId;
                            $ordv->videoName = $productArray->videoName;
                            $ordv->order_id = $orderDetail->id;
                            $ordv->save();
                        }
                        return $orderDetail;
                    } else {
                        return 'Order Already Added';
                    }
                } else {
                    return response()->json(['status' => 99, 'data' => 'error']);
                }
            
        }
    }

    public function addVideoEpisodeToLibrary(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;
        $user = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();
        $product = Product::where('id', $request->id)->where('verify', 1)->first();

        if (!is_null($user)) {
            $endDate = Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int)$checkDate;

           
           
                if (($user->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType)) {
                    $checkSave = false;
                    $ord = Order::where('user_id', $id)->get();
                    $ordDetail = OrderDetail::where('itemPurchase', $request->vid)->first();
                    $orders = DB::table('orders')
                ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
                ->select('user_id', 'itemPurchase')
                ->where('user_id', $id)
                ->where('itemPurchase', '=', $request->vid)
                ->get();


                    if (count($orders) === 0) {
                        $checkSave = true;
                    } else {
                        return 'you have added this product already';
                    }



                    if ($checkSave) {
                        $order = new Order();
                        $order->deliveryId = null;
                        $order->paidAmount = $user->price;
                        $order->user_id = $id;
                        $order->verify = 1;
                        $order->orderNum = 'Biz-' . rand(1, 1000000);
                        $order->referenceNo = 'Subscription';
                        $order->totalAmount = $user->price;
                        $order->paymentType = 'Paystack';
                        $order->save();

                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;
                        $orderDetail->itemPurchase = $request->vid;
                        $orderDetail->productTitle = $product->productChapter->title;
                        $orderDetail->episodeTitle = $request->name;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->amount = $user->price;
                        $orderDetail->fileType = 'video';
                        if ($request->prodType === 'Videos') {
                            $orderDetail->prodType = 'Video Episode Insight Subscription';
                        } elseif ($request->prodType === 'Podcast') {
                            $orderDetail->prodType = 'Podcast Episode Insight Subscription';
                        }
                        $orderDetail->category = $request->getType;
                        $orderDetail->save();


                        $productArray = $product->productChapter->productVideoDetail->where('id', $request->vid)->first();

                        $ordv = new OrderDetailVideo();
                        $ordv->title = $productArray->title;
                        $ordv->description = $productArray->description;
                        $ordv->videoFile = $productArray->videoFile;
                        $ordv->orderNum = 'Biz-' . rand(1, 1000000);
                        $ordv->mediaPublicId = $productArray->videoPublicId;
                        $ordv->videoName = $productArray->videoName;
                        $ordv->order_id = $orderDetail->id;
                        $ordv->save();

                        return $orderDetail;
                    }
                } else {
                    return response()->json(['status' => 99, 'data' => 'error']);
                }
            
        }
    }


    public function streamMedia(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;

        $userPlan = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();
        $product = Product::where('id', $request->id)->where('verify', 1)->first();

        if (isset($id) && $product->subscriptionLevel === '0') {
            if ($request->type === 'AV' || $request->type === 'AP') {
                return $product->productChapter->productVideoDetail;
            } elseif ($request->type === 'EV' || $request->type === 'EP') {
                return    $productArray = $product->productChapter->productVideoDetail->where('id', $request->vid)->first();
            }
        }

        foreach ($userPlan as $user) {
            $endDate = Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int)$checkDate;


            if ($user->level == 2) {
                if (($product->subscriptionLevel == 0) && ($product->prodType === $request->prodType) && ($user->status == 'active')) {
                    if ($request->type === 'AV' || $request->type === 'AP') {
                        return $product->productChapter->productVideoDetail;
                    } elseif ($request->type === 'EV' || $request->type === 'EP') {
                        return    $productArray = $product->productChapter->productVideoDetail->where('id', $request->vid)->first();
                    }
                }
            } else {
                if (($user->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType) && ($user->status == 'active')) {
                    if ($request->type === 'AV' || $request->type === 'AP') {
                        return $product->productChapter->productVideoDetail;
                    } elseif ($request->type === 'EV' || $request->type === 'EP') {
                        return    $productArray = $product->productChapter->productVideoDetail->where('id', $request->vid)->first();
                    }
                }
            }
        }
    }


    public function sendItemToLibrary(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;
        $userPlan = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();



        $product = Product::where('id', $request->id)->where('verify', 1)->first();


        if (isset($id) && $product->subscriptionLevel === '0') {
            $checkSave = false;
            $ord = Order::where('user_id', $id)->get();
            $ordDetail = OrderDetail::where('productId', $product->id)->first();
            $orders = DB::table('orders')
            ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
            ->select('user_id', 'productId')
            ->where('productId', $product->id)
            ->where('user_id', $id)
            ->get();

            if (count($orders) === 0) {
                $checkSave = true;
            }

            if ($checkSave) {
                $order = new Order();
                $order->deliveryId = null;
                $order->paidAmount = '0';
                $order->user_id = $id;
                $order->verify = 1;
                $order->orderNum = 'Biz-' . rand(1, 1000000);
                $order->referenceNo = 'Subscription';
                $order->totalAmount = '0';
                $order->paymentType = 'Paystack';
                $order->save();
                $orderDetail = new OrderDetail();
                $orderDetail->productId = $product->id;
                $orderDetail->order_id = $order->id;






                if ($request->prodType === 'Books') {
                    $orderDetail->productTitle = $product->productBook->title;
                    $orderDetail->prodType = 'Insight Subscription Book';
                } elseif ($request->prodType === 'Journals') {
                    $orderDetail->prodType = 'Insight Subscription Journal';
                    $orderDetail->productTitle = $product->productJournal->articleTitle;
                } elseif ($request->prodType === 'White-Paper') {
                    $orderDetail->productTitle = $product->productWhitePaper->title;
                    $orderDetail->prodType = 'Insight Subscription White Paper';
                } elseif ($request->prodType === 'Market-Research') {
                    $orderDetail->productTitle = $product->marketResearch->title;
                    $orderDetail->prodType = 'Insight Subscription Market Research';
                } elseif ($request->prodType === 'Market-Report') {
                    $orderDetail->productTitle = $product->marketReport->title;
                    $orderDetail->prodType = 'Insight Subscription Market Report';
                }

                $orderDetail->amount = '0';
                $orderDetail->itemPurchase = $product->document;
                $orderDetail->coverImage = $product->coverImage;
                $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                $orderDetail->category = $request->getType;
                $orderDetail->save();

                return $orderDetail;
            } else {
                return 'Order Already Added';
            }
        } else {
        }

   
        foreach ($userPlan as $user) {
            $endDate =  Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int) $checkDate;

     
          
                if (($user->level >= $product->subscriptionLevel ) && ($product->prodType === $request->prodType) ) {
                    $checkSave = false;

                    $ord = Order::where('user_id', $id)->get();
                    $ordDetail = OrderDetail::where('productId', $product->id)->first();

                    if (count($ord) > 0) {
                        foreach ($ord as $ods) {
                            if (isset($ods->user_id) && isset($ordDetail)) {
                                $checkSave = false;
                            } else {
                                $checkSave = true;
                            }
                        }
                    } else {
                        $checkSave = true;
                    }

                    if ($checkSave) {
                        $order = new Order();
                        $order->deliveryId = null;
                        $order->paidAmount = $user->price;
                        $order->user_id = $id;
                        $order->verify = 1;
                        $order->orderNum = 'Biz-' . rand(1, 1000000);
                        $order->referenceNo = 'Subscription';
                        $order->totalAmount = $user->price;
                        $order->paymentType = 'Paystack';
                        $order->save();

                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;

                        if ($request->prodType === 'Books') {
                            $orderDetail->productTitle = $product->productBook->title;
                            $orderDetail->prodType = 'Insight Subscription Book';
                        } elseif ($request->prodType === 'Journals') {
                            $orderDetail->prodType = 'Insight Subscription Journal';
                            $orderDetail->productTitle = $product->productJournal->articleTitle;
                        } elseif ($request->prodType === 'White Paper') {
                            $orderDetail->productTitle = $product->productWhitePaper->title;
                            $orderDetail->prodType = 'Insight Subscription White Paper';
                        } elseif ($request->prodType === 'Market Research') {
                            $orderDetail->productTitle = $product->marketResearch->title;
                            $orderDetail->prodType = 'Insight Subscription Market Research';
                        } elseif ($request->prodType === 'Market Report') {
                            $orderDetail->productTitle = $product->marketReport->title;
                            $orderDetail->prodType = 'Insight Subscription Market Report';
                        }


                        $orderDetail->amount = $user->price;
                        $orderDetail->itemPurchase = $product->document;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->category = $request->getType;
                        $orderDetail->save();

                        return $orderDetail;
                    } else {
                        return 'Order Already Added';
                    }

                    $checkSave = false;
                } else {
                    return response()->json(['status' => 99, 'data' => 'error']);
                }
            
        }
    }

    public function getItemToDownload(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');

        $id = auth('api')->user()->id;
        $userPlan = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->get();



        $product = Product::where('id', $request->id)->where('verify', 1)->first();

        if (isset($id) && $product->subscriptionLevel === '0') {
            return $product->document;
        }

        foreach ($userPlan as $user) {
            $endDate =  Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int) $checkDate;

            $requestProductType = '';


            if (substr_count($request->prodType, '-')) {
                $position = strpos($request->prodType, '-');
                $requestProductType .= substr_replace($request->prodType, ' ', $position, 1);
            } else {
                $requestProductType .= $request->prodType;
            }



            if (($user->level >= $product->subscriptionLevel) && (strtolower($product->prodType) === strtolower($requestProductType)) && ($user->status == 'active') && $user->level >=  $product->subscriptionLevel) {
                return $product->document;
            } else {
                return response()->json(['status' => 99, 'data' => 'error']);
            }
        }
    }


    public function sendCourseToLibrary(Request $request)
    {
        $now =  Carbon::now('Africa/Lagos');
        $id = auth('api')->user()->id;

        $user = UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->first();

        $product = Product::where('id', $request->id)->where('verify', 1)->first();

     
        if (!is_null($user)) {
            $endDate = Carbon::parse($user->endDate);
            $checkDate = $endDate->diffInDays($now);
            $checkDate = (int)$checkDate;

            if (($user->level >= $product->subscriptionLevel) && ($product->prodType === $request->prodType) ) {
                $checkSave = false;
                $ord = Order::where('user_id', $id)->get();
                $ordDetail = OrderDetail::where('productId', $product->id)->first();

               
                $orders = DB::table('orders')
                ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
                ->select('user_id', 'productId')
                ->where('productId', $product->id)
                ->where('user_id', $id)
                ->get();

                if (count($orders) === 0) {
                    $checkSave = true;
                }




                if ($checkSave) {
                    $order = new Order();
                    $order->deliveryId = null;
                    $order->paidAmount = $user->price;
                    $order->user_id = $id;
                    $order->verify = 1;
                    $order->orderNum = 'Biz-' . rand(1, 1000000);
                    $order->referenceNo = 'Subscription';
                    $order->totalAmount = $user->price;
                    $order->paymentType = 'Paystack';
                    $order->save();



                    $productArrays = $product->productCourse->productCourseModule;
                 
                  
                    $file = false;
                    $audiofile = false;
                    $videofile = false;

                    foreach ($productArrays as $prodarr) {
                        if ($prodarr->file !== "") {
                            $file = true;
                        }

                        if ($prodarr->audioFile !== "") {
                            $audiofile = true;
                        }


                        if ($prodarr->videoFile !== "") {
                            $videofile = true;
                        }
                    }

                    if ($file) {
                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;
                        $orderDetail->productTitle = $product->productCourse->title;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->amount = $user->price;
                        $orderDetail->prodType = 'Courses Insight Subscription Book';
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->category = $request->getType;
                        $orderDetail->fileType= 'pdf';
                        $orderDetail->save();


                        for ($i = 0; $i < count($productArrays); $i++) {
                            $ordv = new OrderDetailVideo();
                            $ordv->title = $productArrays[$i]->title;
                            $ordv->description = $productArrays[$i]->description;
                            $ordv->videoFile = $productArrays[$i]->file;
                            $ordv->orderNum = 'Biz-' . rand(1, 1000000);
                            $ordv->mediaPublicId = $productArrays[$i]->filePublicId;
                            $ordv->videoName = $productArrays[$i]->fileName;
                            $ordv->order_id = $orderDetail->id;
                            $ordv->save();
                        }
                    }

                    if ($audiofile) {
                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;
                        $orderDetail->productTitle = $product->productCourse->title;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->amount = $user->price;
                        $orderDetail->prodType = 'Courses Insight Subscription Audio';
                        $orderDetail->category = $request->getType;
                        $orderDetail->fileType= 'audio';
                        $orderDetail->save();


                        for ($i = 0; $i < count($productArrays); $i++) {
                            $ordv = new OrderDetailVideo();
                            $ordv->title = $productArrays[$i]->title;
                            $ordv->description = $productArrays[$i]->description;
                            $ordv->videoFile = $productArrays[$i]->audioFile;
                            $ordv->orderNum = 'Biz-' . rand(1, 1000000);
                            $ordv->mediaPublicId = $productArrays[$i]->audioPublicId;
                            $ordv->videoName = $productArrays[$i]->fileAudioName;
                            $ordv->order_id = $orderDetail->id;
                            $ordv->save();
                        }
                    }


                    if ($videofile) {
                        $orderDetail = new OrderDetail();
                        $orderDetail->productId = $product->id;
                        $orderDetail->order_id = $order->id;
                        $orderDetail->productTitle = $product->productCourse->title;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->amount = $user->price;
                        $orderDetail->prodType = 'Courses Insight Subscription Video';
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->category = $request->getType;
                        $orderDetail->fileType= 'video';
                        $orderDetail->save();
                        for ($i = 0; $i < count($productArrays); $i++) {
                            $ordv = new OrderDetailVideo();
                            $ordv->title = $productArrays[$i]->title;
                            $ordv->description = $productArrays[$i]->description;
                            $ordv->videoFile = $productArrays[$i]->videoFile;
                            $ordv->orderNum = 'Biz-' . rand(1, 1000000);
                            $ordv->mediaPublicId = $productArrays[$i]->videoPublicId;
                            $ordv->videoName = $productArrays[$i]->fileVideoName;
                            $ordv->order_id = $orderDetail->id;
                            $ordv->save();
                        }
                    }
                    $check =  Notification::where('user', $id)->where('product', $orderDetail->productId)->where('vendor', $product->vendor_user_id)->where('message', 'subscribe')->first();
     
                    if (is_null($check)) {
                        Notification::create([
                         'user' =>$id,
                         'vendor' =>  $product->vendor_user_id,
                         'product' => $orderDetail->productId,
                         'message' => 'subscribe',
                         'time'=> Carbon::now()
                     ]);
                    }
                    
                    return $order;
                } else {
                    return 'Order Already Added';
                }
            } else {
                return response()->json(['status' => 99, 'data' => 'error']);
            }
        }
    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = auth('api')->user()->id;
    
        //   if ($request->type == 4) {
        //     $special = SpecialToken::where('user_id', $id)->first();
        //     if($request->price == 7000){
        //         $users = 2;
        //     }else if($request->price === '15000'){
        //          $users = 4;
        //     }else if($request->price === '25000'){

        //          $users = 7;
        //     }else if($request->price === '50000'){
        //          $users = 14;
        //     }else if($request->price === '100000'){
        //           $users = 28;
        //     }else{
        //         $users = 50;
        //     }
 
        //     if (isset($special)) {
          
        //         $special->end_date =  Carbon::parse($special->end_date)->addYear();
        //         $special->save();
            
        //     }else{
          
        //         SpecialToken::create([
        //             'user_id'=> $id,
        //             'token' => $request->token,
        //              'verify' => 0,
        //              'price'=> $request->price,
        //              'mail'=> 1,
        //              'users'=>$users,
        //             'start_date'  => Carbon::now('Africa/Lagos'),
        //             'end_date' => Carbon::now('Africa/Lagos')->addYear()
        //          ]);
    
        //     }
       
        
        //   }
   
     
        if ($id !== null) {
            if ($request->subType == 'entrepreneur') {
                $subscribe = Subscription::where('level', $request->level)->where('duration', $request->duration)->first();
            } elseif ($request->subType == 'accounting') {
                $subscribe = AccountSubscription::where('level', $request->level)->where('duration', $request->duration)->first();
            } elseif ($request->subType == 'expert') {
                $subscribe = VendorSubscription::where('level', $request->level)->where('duration', $request->duration)->first();
            }
          

          
            $email = auth('api')->user()->email;
            if ($request->type == 7) {
                $price = $request->price;
            } else {
                $price = $subscribe->price;
            }
            $level = $subscribe->level;
            $name = $subscribe->title;
           
           
            $startDate  = Carbon::now('Africa/Lagos');
            $st = Carbon::now('Africa/Lagos');
           
            if ($request->duration == 'monthly') {
                $endDate = $st->addMonth();
                $duration = $request->duration;
            }
            if ($request->duration == 'yearly') {
                $endDate = $st->addYear();
                $duration = $request->duration;
            }
          
       
            $verify = 0;
            $curl = curl_init();
            $amount = $price * 100;

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                    'amount'=>$amount,
                    'email'=>$email,
                ]),
                // sk_live_dcaa7733714af8493e63e72c92915af7c609b478
                // sk_test_860118830d804ca16d1920b8baf7309dbad4fb70
               
                CURLOPT_HTTPHEADER => [
                    "authorization: Bearer sk_test_860118830d804ca16d1920b8baf7309dbad4fb70",
                    "header :Access-Control-Allow-Origin: *",
                    "content-type: application/json",
                    "cache-control: no-cache"
                ],
            ));

            $response = curl_exec($curl);

            $err = curl_error($curl);

            if ($err) {
                die('Curl returned error: ' . $err);
            }

            $tranx = json_decode($response, true);

            if (!$tranx['status']) {
                // there was an error from the API
                print_r('API returned error: ' . $tranx['message']);
            }

    

            $userSubscriptionPlan = SubscriptionTemp::where('user_id', $id)->where('type', $request->subType)->first();
           
            if (isset($userSubscriptionPlan)) {
                $userSubscriptionPlan->price = $amount;
                $userSubscriptionPlan->startDate = $startDate;
                $userSubscriptionPlan->duration = $duration;
                $userSubscriptionPlan->level = $level;
                $userSubscriptionPlan->name = $name;
                $userSubscriptionPlan->endDate = $endDate;
                $userSubscriptionPlan->user_id = $id;
                $userSubscriptionPlan->verify = $verify;
                $userSubscriptionPlan->type = $request->subType;
                $userSubscriptionPlan->status = 'pending';
                $userSubscriptionPlan->save();
            } else {
                $userSubscriptionPlan = new SubscriptionTemp();
                $userSubscriptionPlan->price = $amount;
                $userSubscriptionPlan->startDate = $startDate;
                $userSubscriptionPlan->duration = $duration;
                $userSubscriptionPlan->level = $level;
                $userSubscriptionPlan->name = $name;
                $userSubscriptionPlan->endDate = $endDate;
                $userSubscriptionPlan->user_id = $id;
                $userSubscriptionPlan->type = $request->subType;
                $userSubscriptionPlan->verify = $verify;
                $userSubscriptionPlan->status = 'pending';
                $userSubscriptionPlan->save();
            }

            


            return $tranx['data']['authorization_url'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function show(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    public function updateSubscriptionStatus()
    {
    }
    public function getUserSub($id){
       
        $userPlan =  UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'entrepreneur')->value('level');
       if(!is_null( $userPlan )){
        return $userPlan ;
       }else{
        return response()->json([
            'status'=>'failed'
        ]);
       }
    }
    public function getAccountSub($id){
       
        $userPlan =  UserSubscriptionPlan::where('user_id', $id)->where('verify', 1)->where('status', 'active')->where('type', 'accounting')->value('level');
       if(!is_null( $userPlan )){
        return $userPlan ;
       }else{
        return response()->json([
            'status'=>'failed'
        ]);
       }
    }

    public function getVendorSub($id){
       $user = User::where('vendor_user_id',$id)->value('id');
   
         $userPlan = UserSubscriptionPlan::where('user_id', $user)->where('verify', 1)->where('status', 'active')->where('type', 'expert')->value('level');
         if(!is_null( $userPlan )){
            return $userPlan ;
           }else{
            return response()->json([
                'status'=>'failed'
            ]);
           }
     }
}
