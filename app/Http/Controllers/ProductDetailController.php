<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ProductDetail;
use Illuminate\Http\Request;
use App\Http\Resources\ProductDetail as ProductDetailResource;
use App\Http\Resources\ProductChapters as ProductChapterResource;

class ProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productDetail = ProductDetail::all();

        return ProductDetailResource::collection($productDetail);
    }

    public function getAllChapterForPost($id){
        $productChapter = ProductDetail::where('product_id', $id)->get();

        return ProductChapterResource::collection($productChapter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $productChapter = new ProductDetail();
        $productChapter->title = $request->input('title');
        $productChapter->description = $request->input('description');
        $productChapter->prodType = $request->input('prodType');
        $productChapter->product_id = $request->input('product_id');
        $productChapter->save();

        return  $productChapter;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductDetail  $productDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ProductDetail $productDetail)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductDetail  $productDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productDetail = ProductDetail::findOrFail($id);

        return $productDetail;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductDetail  $productDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        //
        $productDetail = ProductDetail::findOrFail($id);
        $productDetail->update([
           $productDetail->title = $request->title,
            $productDetail->description = $request->description,
            $productDetail->prodType = $request->prodType,
            $productDetail->product_id = $request->product_id
        ]);

        return $productDetail;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductDetail  $productDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productDetail = ProductDetail::findOrFail($id);
        $productDetail->delete();

        return $productDetail;
    }
}
