<?php

namespace App\Http\Controllers;

use App\Ip;
use Illuminate\Http\Request;

class IpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $getIps = Ip::where('email',$request->email)->first();
             
        if ($getIps === null) {
            Ip::create([
                'email'=> $request->email,
                'ip_address'=>  $request->login_token 
            ]);
            return response()->json(['status'=>'login']);
        }else{

            $getIps->ip_address =  $request->login_token ;
            $getIps->save();
            return response()->json(['status'=>'logout']);
        }
        
       
    }


    public function addUserIp(Request $request)
    {
       
        $getIps = Ip::where('email',$request->email)->first();
               $clientIP = \Request::getClientIp(true);
        if ($getIps === null) {
            Ip::create([
                'email'=> $request->email,
                'ip_address'=> $clientIP 
            ]);
            return response()->json(['status'=>'login']);
        }else{

            $getIps->ip_address =  $clientIP ;
            $getIps->save();
            return response()->json(['status'=>'logout']);
        }
        
       
    }
    public function getUserIp(Request $request){
       
        $clientIP = \Request::getClientIp(true);
        return $clientIP;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientIP = \Request::getClientIp(true);
        $getLoggedUser = Ip::where('email',$id)->where('ip_address', $clientIP)->first();
        if ($getLoggedUser === null) {
            return response()->json(['status'=>'logout']);
        }
        else{
            
            return response()->json(['status'=>'login']);
        }
    }

    public function checkLogin($id, $token)
    {
       
        $getLoggedUser = Ip::where('email',$id)->where('ip_address', $token)->first();
        if ($getLoggedUser === null) {
            return response()->json(['status'=>'logout']);
        }
        else{
            
            return response()->json(['status'=>'login']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $loggedUser =  Ip::where('email', $id)->first();
      
        $loggedUser->delete();
        return response()->json(['status'=> 'deleted']);
    }
}
