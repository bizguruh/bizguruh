<?php

namespace App\Http\Controllers;

use App\User;
use App\Expenses;
use Carbon\Carbon;
use App\TotalBalance;
use App\AccountHistory;
use App\Mail\SignupMail;
use Illuminate\Http\Request;
use Engagespot\EngagespotPush;
use Illuminate\Support\Facades\Mail;

class ExpenseController extends Controller
{
    //
    public function addTotals(Request $request)
    {

        $user_id =  auth('account_api')->user()->user_id;

        $budget =  TotalBalance::where('user_id', $user_id)->first();
        if (is_null($budget)) {
            TotalBalance::create([
                'user_id' =>  $user_id,
                'reminder' => $request->reminder,
                'total_income' => $request->total_income,
                'total_expenses' => $request->total_expenses,
                'total_balance' => $request->total_balance,
                'budget' => $request->budget,
            ]);
        } else {
            $budget->budget = $request->budget;
            $budget->total_balance =  $request->total_balance;
            $budget->save();
        }
    }
    public function addReminder(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $budget =  TotalBalance::where('user_id', $user_id)->first();
        $budget->reminder = $request->reminder;
        $budget->save();
    }

    public function addExpense(Request $request)
    {

      
        $user_id =  auth('account_api')->user()->user_id;
        $allIncomes = AccountHistory::where('user_id', $user_id)->where('month',$request->month)->value('total_income');
        $allExpense = AccountHistory::where('user_id', $user_id)->where('month',$request->month)->value('total_expenses');
        $oldValue = AccountHistory::where('user_id', $user_id)->where('month',$request->month)->first();
        Expenses::create([
            'user_id' =>  $user_id,
            'budget_id' => $request->budget_id,
            'transaction' => $request->transaction,
            'amount' => $request->amount,
            'category' => $request->category,
            'payment_type' => $request->payment_type,
            'date' => $request->date,
            'note' => $request->note,
            'month' => $request->month,
        ]);
       if(!is_null($oldValue)){
        if($request->transaction == 'income'){
            $newIncome = \intval($allIncomes) + \intval($request->amount);
            $newExpense = $allExpense;
           }
           if($request->transaction == 'expense'){
               $newExpense = \intval($allExpense) + \intval($request->amount);
               $newIncome = $allIncomes;
           }
           $oldValue->total_income = $newIncome;
           $oldValue->total_expenses = $newExpense;
           $oldValue->total_balance = intval($newIncome) - \intval($newExpense);
           $oldValue->save();
       }else{
        if($request->transaction == 'income'){
            AccountHistory::create([
                'user_id' =>  $user_id,
                'month' => $request->month,
                'total_income' => $request->amount,
                'total_expenses' => 0,
                'total_balance' => intval($request->amount) - 0,
    
            ]);
           }
           if($request->transaction == 'expense'){
            AccountHistory::create([
                'user_id' =>  $user_id,
                'month' => $request->month,
                'total_income' => 0,
                'total_expenses' =>$request->amount,
                'total_balance' => 0 - intval($request->amount),
    
            ]);
           }
      
       }
    }

    public function getTotals(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $total = TotalBalance::where('user_id', $user_id)->first();
        return $total;
    }
    public function getExpense(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        return Expenses::where('user_id', $user_id)->get();
    }

    public function getExpensePerMonth($month)
    {
        $user_id =  auth('account_api')->user()->user_id;
        
        return Expenses::where('user_id', $user_id)->where('month',$month)->get();
    }

    public function sendNotification()
    {
        $user_id =  auth('account_api')->user()->user_id;
        $total = TotalBalance::where('user_id', $user_id)->first();
        // $currentTime = Carbon::now();
        // $reminder = Carbon::parse($total->reminder);
        // $currentHour = $currentTime->hour + 1;
        // $currentMinute = $currentTime->minute;
        // $reminderMinute = $reminder->minute;
        // $reminderHour = $reminder->hour;




        // if (!is_null($total)) {

            // EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
            // $data = [
            //     "campaignName" =>  "Account Reminder",
            //     "title" => "Account Reminder",
            //     "message" => "Reminder to enter your daily transactions",
            //     "link" => "https://bizguruh.com/account",
            //     "icon" => "https://bizguruh.com/images/logo.png"
            // ];

            // EngagespotPush::setMessage($data);
            // EngagespotPush::addIdentifiers(array($total->user_id));

            // EngagespotPush::send();
        // }
    }


    public function monthlySave(Request $request)
    {
      
        // $user_id =  auth('account_api')->user()->user_id;
        // $userHistory =  AccountHistory::where('user_id', $user_id)->where('month', $request->month)->first();
        // if (is_null($userHistory)) {
        //     AccountHistory::create([
        //         'user_id' =>  $user_id,
        //         'month' => $request->month,
        //         'total_income' => $request->total_income,
        //         'total_expenses' => $request->total_expenses,
        //         'total_balance' => $request->total_balance,
    
        //     ]);

        //     return response()->json([ 'status'=> 'created']);
        // }else{
        //     $userHistory->total_income = $request->total_income;
        //     $userHistory->total_expenses = $request->total_expenses;
        //     $userHistory->total_balance = $request->total_balance;
        //     $userHistory->save();
        //     return response()->json([ 'status'=> 'updated']);
        // }
      
       
    }
    public function getMonthlySave($month)
    {    
        $user_id =  auth('account_api')->user()->user_id;
       return AccountHistory::where('user_id', $user_id)->where('month', $month)->first();
        
      
       
    }
    public function allMonthlySave()
    {    
        $user_id =  auth('account_api')->user()->user_id;
       return AccountHistory::where('user_id', $user_id)->get();
        
      
       
    }
    public function delBook($id)
    {

        $expense =  Expenses::find($id);

        $expense->delete();
        return 'deleted';
    }

    public function testCron()
    {
        $user = User::where('id',152)->first();
        Mail::to('succy2010@gmail.com')->send(new SignupMail($user));
       
    }
}
