<?php

namespace App\Http\Controllers;

use App\Age;
use App\Like;
use App\Reach;
use App\Gender;
use App\Notify;
use App\Activity;
use App\Location;
use Carbon\Carbon;
use App\TopContent;
use App\Interaction;
use App\TopIndustry;
use App\ContentUpload;
use App\ChecklistVendor;
use Illuminate\Http\Request;

class VendorStatisticController extends Controller
{
    public function addActivity(Request $request)
    {
        $activity = Activity::where('vendor_id', $request->vendor_id)->where('created_at', '>', Carbon::now()->startOfWeek())
        ->where('created_at', '<', Carbon::now()->endOfWeek())->first();
        $now = Carbon::now();
       

        if (is_null($activity)) {
           
            Activity::create([
               'vendor_id'=> $request->vendor_id,
               'profile_visits'=>  intval($request->profile_visits),
               'content_views'=> intval($request->content_views)
           ]);
        } else {
            // $current = Carbon::parse($activity->created_at);
            // if ($now->weekOfYear === $current->weekOfYear) {
                $activity->profile_visits = intval($activity->profile_visits) + intval($request->profile_visits) ;
                $activity->content_views = intval($activity->content_views) + intval($request->content_views) ;
                $activity->save();
            // }else{
            //     Activity::create([
            //         'vendor_id'=> $request->vendor_id,
            //         'profile_visits'=>  intval($request->profile_visits),
            //         'content_views'=> intval($request->content_views)
            //     ]);
            // }
           
        }
    }

    public function getActivity($id)
    {
       
        $activity = Activity::where('vendor_id', $id)->get();
        return $activity;
    }
    public function addGender(Request $request)
    {
        $gender = Gender::where('vendor_id', $request->vendor_id)->where('user_id', $request->user_id)->first();
      
        if (is_null($gender)) {
            Gender::create([
                'user_id' => $request->user_id,
               'vendor_id'=> $request->vendor_id,
               'female'=> $request->female,
               'male'=> $request->male
           ]);
        }
        //  else {
        //     $gender->female = intval($gender->female) + intval($request->female) ;
        //     $gender->male = intval($gender->male) + intval($request->male) ;
        //     $gender->save();
        // }
    }
    public function getGender($id)
    {
        $gender = Gender::where('vendor_id', $id)->get();
        return $gender;
    }
    public function addLocation(Request $request)
    {
        $location = Location::where('vendor_id', $request->vendor_id)->where('locale', $request->locale)->where('user_id', $request->user_id)->first();
      
        if (is_null($location)) {
            Location::create([
                'user_id' => $request->user_id,
               'vendor_id'=> $request->vendor_id,
               'locale'=> $request->locale,
               'count'=> $request->count
           ]);
        }
        //  else {
        //     $location->count = intval($location->count) + intval($request->count) ;
        //     $location->save();
        // }
    }
    public function getLocation($id)
    {
        $location = Location::where('vendor_id', $id)->get();
        return $location;
    }
    public function addLikes(Request $request)
    {
        $likes = Like::where('vendor_id', $request->vendor_id)->where('product_id', $request->product_id)->where('user_id', $request->user_id)->first();
        if (is_null($likes)) {
            Like::create([
            'user_id'=> $request->user_id,
            'product_id'=> $request->product_id,
            'vendor_id'=> $request->vendor_id,
            'like'=> $request->like,
            'unlike'=> $request->unlike
           ]);
        } else {
            $likes->like = $request->like;
            $likes->unlike = $request->unlike;
            $likes->save();
        }
    }
    public function getLikes($user, $product)
    {
        $likes = Like::where('user_id', $user)->where('product_id', $product)->first();
        return $likes;
    }
    public function getAllLikes($product)
    {
        $likes = Like::where('product_id', $product)->where('like', 1)->get();
        return $likes;
    }
    public function getAllUnLikes($product)
    {
        $likes = Like::where('product_id', $product)->where('unlike', 1)->get();
        return $likes;
    }
    public function getVendorLikes($vendor)
    {
        $likes = Like::where('vendor_id', $vendor)->get();
        return $likes;
    }
    public function addAge(Request $request)
    {
        
        $ages = Age::where('vendor_id', $request->vendor_id)->where('user_id', $request->user_id)->first();
        if (is_null($ages)) {
          
            if ($request->age>0 && $request->age < 18) {
                
                Age::create([
                    'user_id' => $request->user_id,
                  'vendor_id' => $request->vendor_id,
                
                  'first' => $request->count
              ]);
            }
            if ($request->age>17 && $request->age < 26) {
                Age::create([
                    'user_id' => $request->user_id,
               'vendor_id' => $request->vendor_id,
             
               'second' => $request->count
           ]);
            }
            if ($request->age>25 && $request->age < 41) {
                Age::create([
                    'user_id' => $request->user_id,
                   'vendor_id' => $request->vendor_id,
             
               'third' => $request->count
           ]);
            }
            if ($request->age>40 && $request->age < 61) {
                Age::create([
                    'user_id' => $request->user_id,
               'vendor_id' => $request->vendor_id,
             
               'fourth' => $request->count
           ]);
            }
            if ($request->age> 60) {
                Age::create([
                'user_id' => $request->user_id,
               'vendor_id' => $request->vendor_id,            
               'fifth' => $request->count
           ]);
            }
        }
        //  else {
        //     if ($request->age>0 && $request->age < 18) {
        //         $ages->first = $request->count +  $ages->first;
        //         $ages->save();
        //     }
        //     if ($request->age>17 && $request->age < 26) {
        //         $ages->second = $request->count +  $ages->second;
        //         $ages->save();
        //     }
        //     if ($request->age>25 && $request->age < 41) {
        //         $ages->third = $request->count +  $ages->third;
        //         $ages->save();
        //     }
        //     if ($request->age>40 && $request->age < 61) {
        //         $ages->fourth = $request->count +  $ages->fourth;
        //         $ages->save();
        //     }
        //     if ($request->age> 60) {
        //         $ages->fifth = $request->count +  $ages->fifth;
        //         $ages->save();
        //     }
        // }
    }

    public function getAge($id)
    {
        return $ages = Age::where('vendor_id', $id)->get();
    }
    public function removeLike($id)
    {
    }
    public function addTopContent(Request $request)
    {
        $content = TopContent::where('vendor_id', $request->vendor_id)->where('content_id', intval($request->content_id))->first();
        if (is_null($content)) {
            TopContent::create([
                    'vendor_id'=> $request->vendor_id,
                    'content_id'=>  intval($request->content_id),
                    'count'=> intval($request->count)
                ]);
        } else {
            $content->count = intval($content->count) + intval($request->count);
            $content->save();
        }
    }
    public function getTopContent($id)
    {
        return $content = TopContent::where('vendor_id', $id)->get();
    }
    public function addTopIndustry(Request $request)
    {
        $industry = TopIndustry::where('vendor_id', $request->vendor_id)->where('industry', $request->industry)->first();
        if (is_null($industry)) {
            TopIndustry::create([
                    'vendor_id'=> $request->vendor_id,
                    'industry'=>  $request->industry,
                    'count'=> intval($request->count)
                ]);
        } else {
            $industry->count = intval($industry->count) + intval($request->count);
            $industry->save();
        }
    }
    public function getTopIndustry($id)
    {
        return $industry = TopIndustry::where('vendor_id', $id)->get();
    }
    public function addTopPreference(Request $request)
    {
    }
    public function getTopPreference($id)
    {
    }

    public function uploadDay(Request $request)
    {
        $vendor = ContentUpload::where('vendor_id', $request->vendor_id)->first();
        if (is_null($vendor)) {
            ContentUpload::create([
            'vendor_id'=>$request->vendor_id,
            'day'=>$request->day
             ]);
        } else {
            $vendor->day = $request->day;
            $vendor->save();
        }
    }
    public function getUploadDay($id)
    {
        $day = ContentUpload::where('vendor_id', $id)->first();
        return $day;
    }
    public function checklistVendor(Request $request)
    {
     
        ChecklistVendor::create([
           'vendor_id'=>$request->vendor_id,
           'response'=> json_encode($request->response)
            ]);
    }
    public function getChecklistVendor($id)
    {
        $list = ChecklistVendor::where('vendor_id', $id)->first();
        return $list;
    }

    public function addInteraction(Request $request){
        $vendor = Interaction::where('vendor_id', $request->vendor_id)->first();
        if (is_null($vendor)) {
            Interaction::create([
                'vendor_id' => $request->vendor_id,
                'likes'=> $request->likes,
                'views' => $request->views,
                'shares' => $request->shares
            ]);

        }else{
            $vendor->likes = $request->likes +  $vendor->likes;
            $vendor->shares = $request->shares +  $vendor->shares;
            $vendor->views = $request->views + $vendor->views ;
            $vendor->save();
        }
    }
    public function getInteractions($id)
    {
        $list = Interaction::where('vendor_id', $id)->first();
        return $list;
    }
    public function addReach(Request $request){
        $reach = Reach::where('user_id', $request->user_id)->where('vendor_id', $request->vendor_id)->where('product_id',$request->product_id)->first();
        if(is_null($reach)){
            Reach::create([
                'user_id'=> $request->user_id,
                'vendor_id'=> $request->vendor_id,
                'product_id'=> $request->product_id
            ]);
            return response()->json([
                'message'=>'created'
            ]);
        }
    }
    public function getReach($id){
        $reach = Reach::where('vendor_id', $id)->get();
        return $reach;
    }

 

}
