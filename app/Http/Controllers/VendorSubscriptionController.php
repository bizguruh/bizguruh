<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Subscription;
use App\SubscriptionTemp;
use App\VendorSubscription;
use App\AccountSubscription;
use Illuminate\Http\Request;

class VendorSubscriptionController extends Controller
{
    public function store(Request $request)
    {
        $id = auth('vendor')->user()->id;
        $user_id = User::where('vendor_user_id', $id)->value('id');
       
        //   if ($request->type == 4) {
        //     $special = SpecialToken::where('user_id', $id)->first();
        //     if($request->price == 7000){
        //         $users = 2;
        //     }else if($request->price === '15000'){
        //          $users = 4;
        //     }else if($request->price === '25000'){

        //          $users = 7;
        //     }else if($request->price === '50000'){
        //          $users = 14;
        //     }else if($request->price === '100000'){
        //           $users = 28;
        //     }else{
        //         $users = 50;
        //     }
 
        //     if (isset($special)) {
          
        //         $special->end_date =  Carbon::parse($special->end_date)->addYear();
        //         $special->save();
            
        //     }else{
          
        //         SpecialToken::create([
        //             'user_id'=> $id,
        //             'token' => $request->token,
        //              'verify' => 0,
        //              'price'=> $request->price,
        //              'mail'=> 1,
        //              'users'=>$users,
        //             'start_date'  => Carbon::now('Africa/Lagos'),
        //             'end_date' => Carbon::now('Africa/Lagos')->addYear()
        //          ]);
    
        //     }
       
        
        //   }
   
     
        if ($id !== null) {
       
            $subscribe = VendorSubscription::where('level', $request->level)->where('duration', $request->duration)->first();
         
        
            $email = auth('vendor')->user()->email;
            if ($request->type == 7) {
                $price = $request->price;
            } else {
                $price = $subscribe->price;
            }
            $level = $subscribe->level;
            $name = $subscribe->title;
           
           
            $startDate  = Carbon::now('Africa/Lagos');
            $st = Carbon::now('Africa/Lagos');
           
            if ( $request->duration == 'monthly') {
                $endDate = $st->addMonth();
                $duration = $request->duration;
            }
            if ( $request->duration == 'yearly') {
                $endDate = $st->addYear();
                $duration = $request->duration;
            }
          

            $verify = 0;
            $curl = curl_init();
            $amount = $price * 100;

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                    'amount'=>$amount,
                    'email'=>$email,
                ]),
                // sk_live_dcaa7733714af8493e63e72c92915af7c609b478
                // sk_test_860118830d804ca16d1920b8baf7309dbad4fb70
               
                CURLOPT_HTTPHEADER => [
                    "authorization: Bearer sk_live_dcaa7733714af8493e63e72c92915af7c609b478",
                    "header :Access-Control-Allow-Origin: *",
                    "content-type: application/json",
                    "cache-control: no-cache"
                ],
            ));

            $response = curl_exec($curl);

            $err = curl_error($curl);

            if ($err) {
                die('Curl returned error: ' . $err);
            }

            $tranx = json_decode($response, true);

            if (!$tranx['status']) {
                // there was an error from the API
                print_r('API returned error: ' . $tranx['message']);
            }

    

            $userSubscriptionPlan = SubscriptionTemp::where('user_id', $id)->where('type',$request->subType)->first();
          
            if (isset($userSubscriptionPlan)) {
                $userSubscriptionPlan->price = $amount;
                $userSubscriptionPlan->startDate = $startDate;
                $userSubscriptionPlan->duration = $duration;
                $userSubscriptionPlan->level = $level;
                $userSubscriptionPlan->name = $name;
                $userSubscriptionPlan->endDate = $endDate;
                $userSubscriptionPlan->user_id = $user_id;
                $userSubscriptionPlan->verify = $verify;
                $userSubscriptionPlan->type = $request->subType;
                $userSubscriptionPlan->status = 'pending';
                $userSubscriptionPlan->save();
            } else {
                $userSubscriptionPlan = new SubscriptionTemp();
                $userSubscriptionPlan->price = $amount;
                $userSubscriptionPlan->startDate = $startDate;
                $userSubscriptionPlan->duration = $duration;
                $userSubscriptionPlan->level = $level;
                $userSubscriptionPlan->name = $name;
                $userSubscriptionPlan->endDate = $endDate;
                $userSubscriptionPlan->user_id = $user_id;
                $userSubscriptionPlan->type = $request->subType;
                $userSubscriptionPlan->verify = $verify;
                $userSubscriptionPlan->status = 'pending';
                $userSubscriptionPlan->save();
            }

            


            return $tranx['data']['authorization_url'];
        }
    }

}
