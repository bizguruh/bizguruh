<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use App\ProductSubCategory;
use Illuminate\Http\Request;
use App\Http\Resources\ProductCategory as ProductCategoryResource;
use App\Http\Resources\CategorySubCategorySubBrandCategory as CategoryProduct;

class ProductCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       // $category = ProductCategory::where('adminCategory', '!==, 'AO')->get();
        $category = ProductCategory::all();

        //return ProductCategoryResource::collection($category);
        return $category;
    }

    public function getVendorCategories(){
        $category = ProductCategory::where('adminCategory', '!=', 'AO')->get();
        return $category;
    }

    public function bizguruhCategory()
    {
        return ProductCategory::all();
    }
    public function bizguruhMatter(){
        $subCategory = ProductSubCategory::all();
        return $subCategory;
    }



    public function getAllCategorywithSubCategory(){
        $category = ProductCategory::all();

       // $category = ProductSubCategory::all();
        return CategoryProduct::collection($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|string',
        'description' => 'required|string',
    ]);
        $category = new ProductCategory();
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->adminCategory = $request->input('adminCategory');
        $category->save();

        return $category;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($productCategory)
    {
        //
         return ProductCategory::where('id',$productCategory)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        //
        $this->validate($request, [
            'name' => 'required|string|unique:product_categories,id',
            'description' => 'required|string',
        ]);
        $productCategory = ProductCategory::where('id', $request->id)->first();
            $productCategory->name = $request->name;
            $productCategory->description = $request->description;
            $productCategory->adminCategory = $request->adminCategory;
            $productCategory->save();



        return $productCategory;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($productCategory)
    {
        //
        $productCategory = ProductCategory::where('id', $productCategory)->first();
        $productCategory->delete();
        return $productCategory;
    }

    public function getSubCategoryById($catid){
       return ProductCategory::where('id', '=', $catid)->first();
    }

}
