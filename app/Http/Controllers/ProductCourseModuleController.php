<?php

namespace App\Http\Controllers;

use App\ProductCourseModule;
use Illuminate\Http\Request;

class ProductCourseModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCourseModule  $productCourseModule
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCourseModule $productCourseModule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCourseModule  $productCourseModule
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCourseModule $productCourseModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCourseModule  $productCourseModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCourseModule $productCourseModule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCourseModule  $productCourseModule
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCourseModule $productCourseModule)
    {
        //
    }
}
