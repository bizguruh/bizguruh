<?php

namespace App\Http\Controllers;

use App\EventScheduleTime;
use Illuminate\Http\Request;

class EventScheduleTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventScheduleTime  $eventScheduleTime
     * @return \Illuminate\Http\Response
     */
    public function show(EventScheduleTime $eventScheduleTime)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventScheduleTime  $eventScheduleTime
     * @return \Illuminate\Http\Response
     */
    public function edit(EventScheduleTime $eventScheduleTime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventScheduleTime  $eventScheduleTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventScheduleTime $eventScheduleTime)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventScheduleTime  $eventScheduleTime
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventScheduleTime $eventScheduleTime)
    {
        //
    }
}
