<?php

namespace App\Http\Controllers;

use App\EventProduct;
use Illuminate\Http\Request;

class EventProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventProduct  $eventProduct
     * @return \Illuminate\Http\Response
     */
    public function show(EventProduct $eventProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventProduct  $eventProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(EventProduct $eventProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventProduct  $eventProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventProduct $eventProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventProduct  $eventProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventProduct $eventProduct)
    {
        //
    }
}
