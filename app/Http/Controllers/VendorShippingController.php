<?php

namespace App\Http\Controllers;

use App\VendorShipping;
use Illuminate\Http\Request;

class VendorShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getShippingDetailForVendor(){
        $id = auth('vendor_user')->user()->id;
        return VendorShipping::where('vendor_id', $id)->first();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id = auth('vendor_user')->user()->id;
        $this->validate($request, [
            'location' => 'required'
        ]);




        $vendorShipping = VendorShipping::where('vendor_id', '=', $id)->first();
        if(!isset($vendorShipping)){
           $vendorShip = VendorShipping::create([
               'location' => $request['location'],
               'addressOne' => $request['addressOne'],
               'addressTwo' => $request['addressTwo'],
               'state' => $request['state'],
               'country' => $request['country'],
               'localShippingRates' => $request['localShippingRates'],
               'localShippingTime' => $request['localShippingTime'],
               'intlShippingRates' => $request['intlShippingRates'],
               'intlShippingTime' => $request['intlShippingTime'],
               'deliveryInformation' => $request['deliveryInformation'],
               'returnPolicy' => $request['returnPolicy'],
               'extraInformation' => $request['extraInformation'],
               'vendor_id' => $id

           ]);

           return $vendorShip;


        }else{
            $vendorShipping = VendorShipping::where('vendor_id', '=', $id)->first();
            $vendorShipping->location = $request->input('location');
            $vendorShipping->addressOne = $request->input('addressOne');
            $vendorShipping->addressTwo = $request->input('addressTwo');
            $vendorShipping->state = $request->input('state');
            $vendorShipping->country = $request->input('country');
            $vendorShipping->localShippingRates = $request->input('localShippingRates');
            $vendorShipping->localShippingTime = $request->input('localShippingTime');
            $vendorShipping->intlShippingRates = $request->input('intlShippingRates');
            $vendorShipping->intlShippingTime = $request->input('intlShippingTime');
            $vendorShipping->deliveryInformation = $request->input('deliveryInformation');
            $vendorShipping->returnPolicy = $request->input('returnPolicy');
            $vendorShipping->extraInformation = $request->input('extraInformation');
            $vendorShipping->vendor_id = $id;
            $vendorShipping->save();

            return $vendorShipping;
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VendorShipping  $vendorShipping
     * @return \Illuminate\Http\Response
     */
    public function show(VendorShipping $vendorShipping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VendorShipping  $vendorShipping
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorShipping $vendorShipping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VendorShipping  $vendorShipping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorShipping $vendorShipping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VendorShipping  $vendorShipping
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorShipping $vendorShipping)
    {
        //
    }
}
