<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\UserPreference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SignupMail;

class UserPreferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function contact(Request $request){
        Mail::to('bizguruh@gmail.com')->send(new ContactMail($request));
    }




    public function getPreferenceForuser($id){
        // $id = auth('api')->user()->id;
        $userP =  UserPreference::where('userId', $id)->get();
        return \App\Http\Resources\UserPreference::collection($userP);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id = auth('api')->user()->id;
        $resp =  json_decode($request->getContent(), true);

        $userPreferences = UserPreference::where('userId', $id)->get();


    if(count($userPreferences) > 0){
       foreach($userPreferences as $userPreference){
           $userPreference->delete();
        }
        foreach ($resp as $res){
            $userP = new UserPreference();
            $userP->name = $res['name'];
            $userP->type = $res['type'];
            $userP->typeId = $res['typeId'];
            $userP->userId = $id;
            $userP->save();
        }
    }else{
        foreach ($resp as $res){
            $userP = new UserPreference();
            $userP->name = $res['name'];
            $userP->type = $res['type'];
            $userP->typeId = $res['typeId'];
            $userP->userId = $id;
            $userP->save();
        }

        return $resp;
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserPreference  $userPreference
     * @return \Illuminate\Http\Response
     */
    public function show(UserPreference $userPreference)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPreference  $userPreference
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPreference $userPreference)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPreference  $userPreference
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPreference $userPreference)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPreference  $userPreference
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPreference $userPreference)
    {
        //
    }
}
