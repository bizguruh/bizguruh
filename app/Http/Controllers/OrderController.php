<?php

namespace App\Http\Controllers;

use App\Cart;
use App\User;
use App\Order;
use App\Product;
use App\Industry;
use Carbon\Carbon;
use App\AccountUser;
use App\OrderDetail;
use App\Mail\BapMail;
use App\Notification;
use App\SpecialToken;
use App\Mail\TrialEnd;
use App\Mail\OrderMail;
use App\ProductCategory;
use App\Mail\SponsorMail;
use App\Mail\TrialEnding;
use App\OrderDetailVideo;
use App\SubscriptionTemp;
use App\ProductSubCategory;
use App\ProductVideoDetail;
use App\Mail\AccountingMail;
use App\ProductCourseModule;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use App\Mail\SubscriptionMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\BapController;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\Order as OrderResource;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $order = Order::all();

        return OrderResource::collection($order);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function clearOrder()
    {
        // $id = auth('api')->user()->id;
        // $user = User::where('id', $id)->first();

        // $email = User::where('id', $id)->value('email');
        // $endDate = Carbon::parse($user->created_at)->addDay(30);
        // $soon = Carbon::parse($user->created_at)->addDay(23);
        // $today = Carbon::now();

        // $userSub = UserSubscriptionPlan::where('user_id', $id)->first();
        // $order = Order::where('user_id', $id)->where('verify', 1)->get();

        // if ($userSub->level === "0") {
        //     if ($today >= $endDate) {
        //         if (count($order) !== 0) {
        //             $order->each->delete();
        //             Mail::to($email)->send(new TrialEnd($user));
        //             return "deleted";
        //         } else {
        //             return "its null";
        //         }
        //     } elseif ($today === $soon) {
        //         Mail::to($email)->send(new TrialEnding($user));
        //     } else {
        //         return 'early';
        //     }

        //     return ;
        // }
    }


    public function getUserOrderById()
    {
        $id = auth('api')->user()->id;
        $user = User::where('id', $id)->first();

       
        $order = Order::where('user_id', $id)->where('verify', 1)->get();

       
        return OrderResource::collection($order);
    }

    public function checkUserOrderById($product)
    {
        $id = auth('api')->user()->id;
        $order = DB::table('orders')
            ->leftJoin('order_details', 'orders.id', '=', 'order_details.order_id')
            ->select('user_id', 'productId')
            ->where('productId', $product)
            ->where('user_id', $id)
            ->where('verify', 1)
            ->first();


        if (isset($order)) {
            return 'true';
        } else {
            return 'false';
        }
    }
  
    public function verifyTransaction($reference, $type,$id)
    {
   
        if ($type=='accounting') {
           
       }else if($type=='entrepreneur'){
           
       }else if($type=='expert'){
           $id = User::where('vendor_user_id', $id)->value('id');

       }
     
        DB::transaction(function () use ($reference,$type,$id) {
            
            $order = Order::where('referenceNo', $reference)->first();
    
         
            if (isset($order)) {
                $user = User::where('id', $id)->first();
    
                $newU = json_decode(json_encode($user), true);
                $newO = json_decode(json_encode($order), true);
                $orderDetail = OrderDetail::where('order_id', $order->id)->get();
                $newOd = json_decode(json_encode($orderDetail), true);
    
                $mailOrder = array_merge($newO, $newOd, $newU);
    
              
                $order->verify = 1;
                $order->save();
                Mail::to($user->email)->send(new OrderMail($mailOrder));
                return $newOd;
                // return new OrderMail( $mailOrder );
            }
    
            $curl = curl_init();
    
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "authorization: Bearer sk_test_860118830d804ca16d1920b8baf7309dbad4fb70",
                    "cache-control: no-cache"
                ],
            ));
    
            $response = curl_exec($curl);
    
            $err = curl_error($curl);
    
            if ($err) {
                // there was an error contacting the Paystack API
                die('Curl returned error: ' . $err);
            }
    
            $tranx = json_decode($response);
    
    
            if (!$tranx->status) {
                // there was an error from the API
                die('API returned error: ' . $tranx->message);
            }
    
            if ('success' == $tranx->data->status) {
              
                $carts = Cart::where('vendor', $id)->get();
                foreach ($carts as $cart) {
                    $cart->delete();
                }
    
    
              
                $order = Order::where('referenceNo', $reference)->first();
    
                if (isset($order)) {
                    $user = User::where('id', $id)->first();
                    // $order = Order::where('user_id', $id)->latest()->first();
                    $newU = json_decode(json_encode($user), true);
                    $newO = json_decode(json_encode($order), true);
                    $orderDetail = OrderDetail::where('order_id', $order->id)->latest()->first();
                    $newOd = json_decode(json_encode($orderDetail), true);
    
                    $mailOrder = array_merge($newO, $newOd, $newU);
    
                    if ($order->verify === 0) {
                        Mail::to($user->email)->send(new OrderMail($mailOrder));
                    }
                    $order->verify = 1;
                    $order->save();
                    $vendor = Product::where('id', $orderDetail->productId)->value('vendor_user_id');
                    $check =  Notification::where('user', $user_id)->where('product', $orderDetail->productId)->where('vendor', $vendor)->where('message', 'purchase')->first();
         
                    if (is_null($check)) {
                        Notification::create([
                         'user' =>$user_id,
                         'vendor' => $vendor,
                         'product' => $orderDetail->productId,
                         'message' => 'purchase',
                         'time'=> Carbon::now()
                     ]);
                    }
                    return $mailOrder;
                } else {
                    $subscription = UserSubscriptionPlan::where('user_id', $id)->where('type', $type)->first();
                    $subscription_temp = SubscriptionTemp::where('user_id', $id)->where('type', $type)->first();
                    $special = SpecialToken::where('user_id', $id)->first();
                    $user = User::where('id', $id)->first();
                    if (isset($special)) {
                        $special->verify = 1;
                        $special->save();
                        $subscription->verify = 1;
                        $subscription->status = 'active';
                        $subscription->save();
                        if ($special->mail === 1) {
                            Mail::to($user->email)->send(new SponsorMail($special));
                            $special->mail = 0;
                            $special->save();
                        }
                    } else {
                        if (isset($subscription_temp)) {
                            $user = User::find($id);
                            // $newU = json_decode(json_encode($user), true);
                            // $newO = json_decode(json_encode($subscription), true);
                            // $mailOrder = array_merge($newO, $newU);
                            // if ($subscription_temp->verify === 0) {
                            //     Mail::to($user->email)->send(new SubscriptionMail($mailOrder));
                            // }
    
                            if (isset($subscription)) {
                                $subscription->price = $subscription_temp->price;
                                $subscription->startDate = $subscription_temp->startDate;
                                $subscription->duration = $subscription_temp->duration;
                                $subscription->level = $subscription_temp->level ;
                                $subscription->name = $subscription_temp->name;
                                $subscription->endDate = $subscription_temp->endDate ;
                                $subscription->user_id =     $subscription_temp->user_id;
                                $subscription->type = $type;
                                $subscription->verify = 1;
                                $subscription->status = 'active';
                                $subscription->save();
                            } else {
                                $subscription = new UserSubscriptionPlan();
                                $subscription->price = $subscription_temp->price;
                                $subscription->startDate = $subscription_temp->startDate;
                                $subscription->duration = $subscription_temp->duration;
                                $subscription->level = $subscription_temp->level ;
                                $subscription->name = $subscription_temp->name;
                                $subscription->type = $type;
                                $subscription->endDate = $subscription_temp->endDate ;
                                $subscription->user_id =     $subscription_temp->user_id;
                                $subscription->verify = 1;
                                $subscription->status = 'active';
                                $subscription->save();
    
                                if ($type == 'entrepreneur' && $subscription_temp->level >= 3) {
                                    $createBap = new BapController();
                                    $createBap->start();
                                    Mail::to($user->email)->send(new BapMail());
                                }
                            }
    
                         
                            if ($type == 'accounting') {
                                AccountUser::create([
                                    'user_id' => $user->id,
                                    'name' => $user->name,
                                    'email' => $user->email,
                                    'phone_no' => $user->phone_no,
                                    'password' => Hash::make('admin'),
                                    'role' => 'admin',
                                    'verify' => 1
                                   
                                ]);
                                Mail::to($user->email)->send(new AccountingMail($user));
                            }
    
                            $subscription_temp->delete();
                        }
                    }
                }
                // transaction was successful...
            }
        });

        return ['status' => '200', 'message' => 'Verification successful'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $userId =   auth('api')->user()->id;


        if ($userId !== null) {
            $this->validate($request, [
                'deliveryId' => 'required',
                'paidAmount' => 'required',
                'totalAmount' => 'required',
                'paymentType' => 'required'
            ]);

            $curl = curl_init();

            $email = $request->email;
            $amount = $request->totalAmount * 100;

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                    'amount' => $amount,
                    'email' => $email,
                ]),
                // sk_test_860118830d804ca16d1920b8baf7309dbad4fb70
                CURLOPT_HTTPHEADER => [
                    "authorization: Bearer sk_test_860118830d804ca16d1920b8baf7309dbad4fb70",
                    "header :Access-Control-Allow-Origin: *",
                    "content-type: application/json",
                    "cache-control: no-cache"
                ],
            ));

            $response = curl_exec($curl);

            // return $response;
            $err = curl_error($curl);

            if ($err) {
                die('Curl returned error: ' . $err);
            }

            $tranx = json_decode($response, true);

            if (!$tranx['status']) {
                // there was an error from the API
                print_r('API returned error: ' . $tranx['message']);
            }


            $order = new Order();
            $order->deliveryId = $request->deliveryId;
            $order->paidAmount = $request->paidAmount;
            $order->user_id = $userId;
            $order->verify = 0;
            $order->orderNum = 'Biz-' . rand(1, 1000000);
            $order->referenceNo = $tranx['data']['reference'];
            // $order->referenceNo =5;
            $order->totalAmount = $request->totalAmount;
            $order->paymentType = $request->paymentType;

            $order->save();



            foreach ($request->item as $items) {
                $orderDetail = new OrderDetail();

                if ($items['productType'] === 'HardCopy') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->hardCopyPrice) {
                        $orderDetail->amount = $product->hardCopyPrice;
                        if (!isset($product->quantity)) {
                            $orderDetail->productTitle = $items['title'];
                            $orderDetail->episodeTitle = $items['title'];
                            $orderDetail->coverImage = $product->coverImage;
                            $orderDetail->quantity = 1;
                            $orderDetail->vendorId =  $items['vendor_user_id'];
                            $orderDetail->prodType = $items['productType'];
                            $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                            $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                            $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                            $orderDetail->order_id = $order->id;
                            $orderDetail->productId =  $items['id'];
                            $orderDetail->save();
                            if (isset($items['product_chapter'])) {
                                $productWeb = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                                foreach ($productWeb as $web) {
                                    $od = new OrderDetailVideo();
                                    $od->title = $web->title;
                                    $od->description = $web->description;
                                    $od->videoFile = $web->videoFile;
                                    $od->videoName = $web->videoName;
                                    $od->orderNum = $order->orderNum;
                                    $od->order_id = $orderDetail->id;
                                    $orderDetail->orderDetailVideo()->save($od);
                                }
                            } elseif (isset($items['product_course'])) {
                                $productModules = ProductCourseModule::where('products_courses_id', $items['product_course']['id'])->get();
                                foreach ($productModules as $productModule) {
                                    $od = new OrderDetailVideo();
                                    $od->title = $productModule->title;
                                    $od->description = $productModule->description;
                                    $od->videoFile = 'hard';
                                    $od->videoName = 'Hard Copy';
                                    $od->orderNum = $order->orderNum;
                                    $od->order_id = $orderDetail->id;
                                    $orderDetail->orderDetailVideo()->save($od);
                                }
                            }
                        } else {
                            $product->quantity = $product->quantity - 1;
                            $orderDetail->productTitle = $items['title'];
                            $orderDetail->episodeTitle = $items['title'];
                            $orderDetail->coverImage = $product->coverImage;
                            $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                            $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                            $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                            $orderDetail->quantity = 1;
                            $orderDetail->vendorId =  $items['vendor_user_id'];
                            $orderDetail->prodType = $items['productType'];
                            $orderDetail->order_id = $order->id;
                            $orderDetail->productId =  $items['id'];
                            $orderDetail->save();
                        }
                    }
                }


                if ($items['productType'] === 'SoftCopy') {
                    $product = Product::where('id', $items['id'])->first();




                    if ($items['amount'] === $product->softCopyPrice) {
                        $orderDetail->amount = $product->softCopyPrice;
                        $orderDetail->itemPurchase = $product->document;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        if (isset($items['product_course'])) {
                            $orderDetail->category = 'Courses';
                        }
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();

                        if (isset($items['product_course'])) {
                            $productModules = ProductCourseModule::where('products_courses_id', $items['product_course']['id'])->get();
                            foreach ($productModules as $productModule) {
                                $od = new OrderDetailVideo();
                                $od->title = $productModule->title;
                                $od->description = $productModule->description;
                                $od->videoFile = $productModule->file;
                                $od->videoName = $productModule->fileName;
                                $od->mediaPublicId = $productModule->filePublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }

                if ($items['productType'] === 'VideoCopy') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->videoPrice) {
                        $orderDetail->amount = $product->videoPrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->itemPurchase = 'Webinar';
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();

                        if (isset($items['product_chapter'])) {
                            $productWebinar = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                            foreach ($productWebinar as $web) {
                                $od = new OrderDetailVideo();
                                $od->title = $web->title;
                                $od->description = $web->description;
                                $od->videoFile = $web->videoFile;
                                $od->videoName = $web->videoName;
                                $od->mediaPublicId = $web->videoPublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        } elseif (isset($items['product_course'])) {
                            $productModules = ProductCourseModule::where('products_courses_id', $items['product_course']['id'])->get();
                            foreach ($productModules as $productModule) {
                                $od = new OrderDetailVideo();
                                $od->title = $productModule->title;
                                $od->description = $productModule->description;
                                $od->videoFile = $productModule->videoFile;
                                $od->videoName = $productModule->fileVideoName;
                                $od->mediaPublicId = $productModule->videoPublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }

                if ($items['productType'] === 'VideoEpisode') {
                    $product = Product::where('id', $items['id'])->first();
                    $productVideo = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();



                    foreach ($productVideo as $pv) {
                        if ($items['amount'] === $pv->price) {
                            $orderDetail->amount = $pv->price;
                            $orderDetail->coverImage = $product->coverImage;
                            $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                            $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                            $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                            $orderDetail->productTitle =  $items['title'];
                            $orderDetail->episodeTitle =  $pv->title;
                            $orderDetail->prodType = $items['productType'];
                            $orderDetail->vendorId =  $items['vendor_user_id'];
                            $orderDetail->itemPurchase = $pv->videoPublicId;
                            $orderDetail->productId =  $items['id'];
                            $orderDetail->vid = $pv->id;
                            $orderDetail->order_id = $order->id;
                            $orderDetail->save();
                        }
                    }
                }

                if ($items['productType'] === 'Subscription') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->subscribePrice) {
                        $orderDetail->amount = $product->subscribePrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();
                        $productWebinar = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                        foreach ($productWebinar as $web) {
                            $od = new OrderDetailVideo();
                            $od->title = $web->title;
                            $od->description = $web->description;
                            $od->videoFile = $web->videoFile;
                            $od->videoName = $web->videoName;
                            $od->mediaPublicId = $web->videoPublicId;
                            $od->orderNum = $order->orderNum;
                            $od->order_id = $orderDetail->id;
                            $orderDetail->orderDetailVideo()->save($od);
                        }
                    }
                }

                if ($items['productType'] === 'Streaming') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->streamOnlinePrice) {
                        $orderDetail->amount = $product->streamOnlinePrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();
                        $productWebinar = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                        foreach ($productWebinar as $web) {
                            $od = new OrderDetailVideo();
                            $od->title = $web->title;
                            $od->description = $web->description;
                            $od->videoFile = $web->videoFile;
                            $od->videoName = $web->videoName;
                            $od->mediaPublicId = $web->videoPublicId;
                            $od->orderNum = $order->orderNum;
                            $od->order_id = $orderDetail->id;
                            $orderDetail->orderDetailVideo()->save($od);
                        }
                    }
                }

                if (($items['productType'] === 'AudioCopy') && ($items['prodType'] === 'Books')) {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->audioPrice) {
                        $orderDetail->amount = $product->audioPrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->itemPurchase = $product->productBook->audioFile;
                        $orderDetail->category = $product->productBook->audioPublicId;
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();
                    }
                }

                if (($items['productType'] === 'AudioCopy') && ($items['prodType'] === 'Podcast')) {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->audioPrice) {
                        $orderDetail->amount = $product->audioPrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->category = 'PodcastAudio';
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();
                        if (isset($items['product_chapter'])) {
                            $productWebinar = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                            foreach ($productWebinar as $web) {
                                $od = new OrderDetailVideo();
                                $od->title = $web->title;
                                $od->description = $web->description;
                                $od->videoFile = $web->videoFile;
                                $od->videoName = $web->videoName;
                                $od->mediaPublicId = $web->videoPublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }


                if (($items['productType'] === 'AudioCopy') && ($items['prodType'] !== 'Books')) {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->audioPrice) {
                        $orderDetail->amount = $product->audioPrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();
                        if (isset($items['product_chapter'])) {
                            $productWebinar = ProductVideoDetail::where('productId', $items['product_chapter']['id'])->get();
                            foreach ($productWebinar as $web) {
                                $od = new OrderDetailVideo();
                                $od->title = $web->title;
                                $od->description = $web->description;
                                $od->videoFile = $web->videoFile;
                                $od->videoName = $web->videoName;
                                $od->mediaPublicId = $web->videoPublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }


                if ($items['productType'] === 'ReadOnline') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] = $product->readOnlinePrice) {
                        $orderDetail->amount = $product->readOnlinePrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->itemPurchase = $product->document;
                        if (isset($items['product_course'])) {
                            $orderDetail->category = 'CoursesReadOnline';
                        }
                        $orderDetail->vendorId =  $items['vendor_user_id'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();


                        if (isset($items['product_course'])) {
                            $productModules = ProductCourseModule::where('products_courses_id', $items['product_course']['id'])->get();
                            foreach ($productModules as $productModule) {
                                $od = new OrderDetailVideo();
                                $od->title = $productModule->title;
                                $od->description = $productModule->description;
                                $od->videoFile = $productModule->file;
                                $od->videoName = $productModule->fileName;
                                $od->mediaPublicId = $productModule->filePublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }

                if ($items['productType'] === 'CourseCopy') {
                    $product = Product::where('id', $items['id'])->first();
                    if ($items['amount'] === $product->hardCopyPrice) {
                        $orderDetail->amount = $product->hardCopyPrice;
                        $orderDetail->coverImage = $product->coverImage;
                        $orderDetail->type = ProductCategory::where('id', $product->category_id)->first()->name;
                        $orderDetail->subjectMatter = ProductSubCategory::where('id', $product->sub_category_id)->first()->name;
                        $orderDetail->industry = Industry::where('id', $product->industry_id)->value('name');
                        $orderDetail->productTitle = $items['title'];
                        $orderDetail->episodeTitle = $items['title'];
                        $orderDetail->category = 'Course';
                        $orderDetail->prodType = $items['productType'];
                        $orderDetail->productId =  $items['id'];
                        $orderDetail->order_id = $order->id;
                        $orderDetail->save();

                        if (isset($items['product_course'])) {
                            $productModules = ProductCourseModule::where('products_courses_id', $items['product_course']['id'])->get();
                            foreach ($productModules as $productModule) {
                                $od = new OrderDetailVideo();
                                $od->title = $productModule->title;
                                $od->description = $productModule->description;
                                $od->videoFile = $productModule->videoFile;
                                $od->videoName = $productModule->fileVideoName;
                                $od->mediaPublicId = $productModule->videoPublicId;
                                $od->audioFile = $productModule->audioFile;
                                $od->audioPublicId = $productModule->audioPublicId;
                                $od->audioName = $productModule->fileAudioName;
                                $od->file = $productModule->file;
                                $od->fileName = $productModule->fileName;
                                $od->filePublicId = $productModule->filePublicId;
                                $od->orderNum = $order->orderNum;
                                $od->order_id = $orderDetail->id;
                                $orderDetail->orderDetailVideo()->save($od);
                            }
                        }
                    }
                }
            }


            return $tranx['data']['authorization_url'];
        }
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function sendOrder($id)
    {
        $userEmail = User::where('id', $id)->value('email');

        $order = Order::where('user_id', $id)->latest()->first();
        $newO = json_decode(json_encode($order), true);
        $orderDetail = OrderDetail::where('order_id', $order->id)->latest()->first();
        $newOd = json_decode(json_encode($orderDetail), true);

        $mailOrder = array_merge($newO, $newOd);

        // Mail::to($userEmail)->send(new OrderMail( $mailOrder));
        // dd( $mailOrder);

        return $mailOrder;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
