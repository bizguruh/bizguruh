<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Wishlist;
use App\User;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id = auth('api')->user()->id;
        return Wishlist::where('userId', $id)->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getWishlistByUserId(){

        $id = auth('api')->user()->id;
        $wishlists = Wishlist::where('userId', $id)->get();

       if (isset($wishlists)) {

           $wishlist = [];
           foreach ($wishlists as $wish) {
               $product = Product::where('id', $wish->productId)->get();

              array_push($wishlist,  ProductResource::collection($product));

           }
           return $wishlist;
       }

    }

    public function checkWishlistByUserId($product){

        $id = auth('api')->user()->id;
        $wishlists = Wishlist::where('userId', $id)->where('productId', $product)->first();
       if (isset($wishlists)) {
        return 'true';

       }else{
           return 'false';
       }



    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
           'productId' => 'required',
           'userId' => 'required',
           'vendorId' => 'required'
        ]);



        $wishlist = new Wishlist();
        $wishlist->productId = $request->productId;
        $wishlist->userId = $request->userId;
        $wishlist->vendorId = $request->vendorId;
        $wishlist->save();

        return $wishlist;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show(Wishlist $wishlist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Wishlist $wishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wishlist $wishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $wish = Wishlist::where('productId', $id)->first();
        $wish->delete();
        return response(200);
    }
}
