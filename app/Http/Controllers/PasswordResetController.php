<?php

namespace App\Http\Controllers;

use App\User;
use App\VendorUser;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PasswordResetController extends Controller
{
    //

    public function sendLink($email)
    {
       $user = VendorUser::where('email', $email)->value('id');
       if (is_null($user)) {
       
        $user = User::where('email', $email)->value('id');
        Mail::to($email)->send(new ResetPassword($user));
       
       }else{
       
        Mail::to($email)->send(new ResetPassword($user));
       
       }
   
    }
    public function changePassword(Request $request){


            $vendor =    $user = VendorUser::where('id', $request->id)->first();
            if (is_null($vendor)) {
                $user = User::where('id', $request->id)->first();
                if ($user->password ===  Hash::make($request->newPassword)) {
                   
                    return 'error';
                }else{
                   $user->password = Hash::make($request->newPassword);
                   $user->save();
                   return response()->json([
                       'message'=>'success'
                   ]) ;
                }
            }else{
               
                if ($vendor->password ===  Hash::make($request->newPassword)) {
                   
                    return 'error';
                }else{
                   $vendor->password = Hash::make($request->newPassword);
                   $vendor->save();
                  
                   return response()->json([
                    'message'=> 'vendor password changed'
                ]) ;
                }
            }
            
            
       
    }
}
