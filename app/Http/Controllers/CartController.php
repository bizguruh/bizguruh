<?php

namespace App\Http\Controllers;

use App\Cart;
//use App\Http\Resources\Product;
use App\User;
use App\Http\Resources\Product as ProductResource;
use App\Product;
use App\ProductVideoDetail;
use App\UserDetail;
use App\VendorShipping;
use Illuminate\Http\Request;
use App\Http\Resources\CartResource as CartResources;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */





    public function index()
    {
        //
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function checkCart($id, $product)
    {
        $cart = Cart::where('vendor', $id)->where('productId', $product)->first();
        if (isset($cart)) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function postToCart(Request $request)
    {
        $cartArray = [];
        $totalAmount = [];
        $shippingPriceAmount = [];
        $userDetail = UserDetail::where('id', $request->addressId)->first();
        if ($request->sessionCheck === 'C') {
            $carts = Cart::where('vendor', $request->customerId)->get();

            if (sizeof($carts) > 0) {
                foreach ($carts as $cart) {
                    $products = Product::where('id', $cart->productId)->select(
                        'id',
                        'prodType',
                        'quantity',
                        'hardCopy',
                        'softCopy',
                        'audioCopy',
                        'videoCopy',
                        'coverImage',
                        'vendor_user_id',
                        'videoPrice',
                        'subscribePrice',
                        'eventPrice',
                        'streamOnlinePrice',
                        'webinarPrice',
                        'softCopyPrice',
                        'hardCopyPrice',
                        'audioPrice',
                        'readOnlinePrice',
                        'verify',
                        'readOnline',
                        'verify',
                        'sponsor',
                        'category_id',
                        'sub_category_id',
                        'sub_category_brand_id'
                    )->get();
                    foreach ($products as $product) {
                        $product->setAttribute('uid', $cart->id);
                        switch ($product->prodType) {
                           case 'Market Reports':
                               $product->setAttribute('title', $product->marketReport->title);
                               break;
                           case 'Books':
                               $product->setAttribute('title', $product->productBook->title);
                               break;
                           case 'White Paper':
                               $product->setAttribute('title', $product->productWhitePaper->title);
                               break;
                           case 'Market Research':
                               $product->setAttribute('title', $product->marketResearch->title);
                               break;
                           case 'Journals':
                               $product->setAttribute('title', $product->productJournal->journalTitle);
                               break;
                           case 'Events':
                               $product->setAttribute('title', $product->productEvent->title);
                               break;
                           case 'Webinar':
                               $product->setAttribute('title', $product->productChapter->title);
                               $prV = ProductVideoDetail::where('productId', $product->productChapter->id)->get();
                               $product->setAttribute('data', $prV);
                               break;
                           case 'Courses':

                               $product->setAttribute('title', $product->productCourse->title);
                               break;
                           case 'Podcast':
                               $product->setAttribute('title', $product->productChapter->title);
                               break;
                           case 'Videos':
                               $product->setAttribute('title', $product->productChapter->title);
                               break;
                           default:
                               return false;
                       }



                        if ($cart->salePrice) {
                            $vendorShip = VendorShipping::where('vendor_id', $product->vendor_user_id)->first();
                            if (isset($userDetail->country)) {
                                if (trim(strtolower($userDetail->country)) === 'nigeria') {
                                    $product->setAttribute('shippingPrice', $vendorShip->localShippingRates);
                                    array_push($shippingPriceAmount, $vendorShip->localShippingRates);
                                } elseif (trim(strtolower($userDetail->country)) !== 'nigeria') {
                                    $product->setAttribute('shippingPrice', $vendorShip ? $vendorShip->intlShippingRates : '');
                                    array_push($shippingPriceAmount, $vendorShip ? $vendorShip->intlShippingRates : '');
                                }
                            }


                            $product->setAttribute('amount', $product->hardCopyPrice);
                            $product->setAttribute('productType', 'HardCopy');
                            array_push($totalAmount, $product->hardCopyPrice);
                        } elseif ($cart->prodType === 'All-Format') {
                            $product->setAttribute('amount', $product->hardCopyPrice);
                            $product->setAttribute('productType', 'CourseCopy');
                            array_push($totalAmount, $product->hardCopyPrice);
                        } elseif ($cart->prodType === 'AudioCopy') {
                            $product->setAttribute('amount', $product->audioPrice);
                            $product->setAttribute('productType', 'AudioCopy');
                            array_push($totalAmount, $product->audioPrice);
                        } elseif ($cart->prodType === 'Events') {
                            $product->setAttribute('amount', ($product->eventPrice) * ($cart->quantity));
                            $product->setAttribute('productType', 'eventCopy');
                            $product->setAttribute('myQty', $cart->quantity);
                            array_push($totalAmount, ($product->eventPrice) * ($cart->quantity));
                        } elseif ($cart->prodType === 'VideoCopy') {
                            $product->setAttribute('amount', $product->videoPrice);
                            $product->setAttribute('productType', 'VideoCopy');
                            array_push($totalAmount, $product->videoPrice);
                        } elseif ($cart->prodType === 'Video') {
                            $product->setAttribute('amount', $product->videoPrice);
                            $product->setAttribute('productType', 'VideoCopy');
                            array_push($totalAmount, $product->videoPrice);
                        } elseif ($cart->prodType === 'Audio') {
                            $product->setAttribute('amount', $product->audioPrice);
                            $product->setAttribute('productType', 'AudioCopy');
                            array_push($totalAmount, $product->audioPrice);
                        } elseif ($cart->prodType === 'ReadOnline') {
                            $product->setAttribute('amount', $product->readOnlinePrice);
                            $product->setAttribute('productType', 'ReadOnline');
                            array_push($totalAmount, $product->readOnlinePrice);
                        } elseif ($cart->prodType === 'VideoEpisode') {
                            $productVideo = ProductVideoDetail::where('id', $cart->vid)->first();
                            $product->setAttribute('amount', $productVideo->price);
                            $product->setAttribute('productType', 'VideoEpisode');
                            array_push($totalAmount, $productVideo->price);
                        } elseif ($cart->prodType === 'Subscription') {
                            $product->setAttribute('amount', $product->subscribePrice);
                            $product->setAttribute('productType', 'Subscription');
                            array_push($totalAmount, $product->subscribePrice);
                        } elseif ($cart->prodType === 'Streaming') {
                            $product->setAttribute('amount', $product->streamOnlinePrice);
                            $product->setAttribute('productType', 'Streaming');
                            array_push($totalAmount, $product->streamOnlinePrice);
                        } else {
                            $product->setAttribute('amount', $product->softCopyPrice);
                            $product->setAttribute('productType', 'SoftCopy');
                            array_push($totalAmount, $product->softCopyPrice);
                        }
                        array_push($cartArray, $product);
                    }
                }
            }

            $count = 0;


            foreach ($request->itemId as $item) {
                $product = Product::where('id', $item['id'])->first();


                switch ($product->prodType) {
                    case 'Market Reports':
                        $product->setAttribute('title', $product->marketReport->title);
                        break;
                    case 'Books':
                        $product->setAttribute('title', $product->productBook->title);
                        break;
                    case 'White Paper':
                        $product->setAttribute('title', $product->productWhitePaper->title);
                        break;
                    case 'Market Research':
                        $product->setAttribute('title', $product->marketResearch->title);
                        break;
                    case 'Journals':
                        $product->setAttribute('title', $product->productJournal->journalTitle);
                        break;
                    case 'Events':
                        $product->setAttribute('title', $product->productEvent->title);
                        break;
                    case 'Webinar':
                        $product->setAttribute('title', $product->productChapter->title);
                        $product->setAttribute('datax', $product->productChapter->title);
                        break;
                    case 'Courses':
                        $product->setAttribute('title', $product->productCourse->title);
                        break;
                    case 'Podcast':
                        $product->setAttribute('title', $product->productChapter->title);
                        break;
                    case 'Videos':
                        $product->setAttribute('title', $product->productChapter->title);
                        break;
                    default:
                        return false;
                }


                $product->setAttribute('count', $count++);
                if ($item['prodType'] === 'Events') {
                    // return $item['quantities'];
                    $product->setAttribute('amount', ($product->eventPrice) * ($item['quantities']));
                    $product->setAttribute('myQty', $item['quantities']);
                    $product->setAttribute('productType', 'eventCopy');
                    array_push($totalAmount, ($product->eventPrice) * ($item['quantities']));
                } elseif ($item['quantity']) {
                    $vendorShip = VendorShipping::where('vendor_id', $product->vendor_user_id)->first();


                    $product->setAttribute('amount', $product->hardCopyPrice);
                    $product->setAttribute('productType', 'HardCopy');

                    if (isset($userDetail->country)) {
                        if (trim(strtolower($userDetail->country)) === 'nigeria') {
                            $product->setAttribute('shippingPrice', $vendorShip->localShippingRates);
                            // array_push($totalAmount, $vendorShip->localShippingRates);
                            array_push($shippingPriceAmount, $vendorShip->localShippingRates);
                        } else {
                            $product->setAttribute('shippingPrice', $vendorShip->intlShippingRates);
                            // array_push($totalAmount, $vendorShip->intlShippingRates);
                            array_push($shippingPriceAmount, $vendorShip->intlShippingRates);
                        }
                    }

                    array_push($totalAmount, $product->hardCopyPrice);
                } elseif ($item['prodType'] === 'AudioCopy') {
                    $product->setAttribute('amount', $product->audioPrice);
                    $product->setAttribute('productType', 'AudioCopy');
                    array_push($totalAmount, $product->audioPrice);
                } elseif ($item['prodType'] === 'VideoCopy') {
                    $product->setAttribute('amount', $product->videoPrice);
                    $product->setAttribute('productType', 'VideoCopy');
                    array_push($totalAmount, $product->videoPrice);
                } elseif ($item['prodType'] === 'Video') {
                    $product->setAttribute('amount', $product->videoPrice);
                    $product->setAttribute('productType', 'VideoCopy');
                    array_push($totalAmount, $product->videoPrice);
                } elseif ($item['prodType'] === 'ReadOnline') {
                    $product->setAttribute('amount', $product->readOnlinePrice);
                    $product->setAttribute('productType', 'ReadOnline');
                    array_push($totalAmount, $product->readOnlinePrice);
                } elseif ($item['prodType'] === 'Audio') {
                    $product->setAttribute('amount', $product->audioPrice);
                    $product->setAttribute('productType', 'AudioCopy');
                    array_push($totalAmount, $product->audioPrice);
                } elseif ($item['prodType'] === 'Subscription') {
                    $product->setAttribute('amount', $product->subscribePrice);
                    $product->setAttribute('productType', 'Subscription');
                    array_push($totalAmount, $product->subscribePrice);
                } elseif ($item['prodType'] === 'VideoEpisode') {
                    $productVideo = ProductVideoDetail::where('id', $item['mediaId'])->first();
                    $product->setAttribute('amount', $productVideo->price);
                    $product->setAttribute('productType', 'VideoEpisode');
                    $product->setAttribute('vid', $productVideo->id);
                    array_push($totalAmount, $productVideo->price);
                } elseif ($item['prodType'] === 'Streaming') {
                    $product->setAttribute('amount', $product->streamOnlinePrice);
                    $product->setAttribute('productType', 'Streaming');
                    array_push($totalAmount, $product->streamOnlinePrice);
                } else {
                    $product->setAttribute('amount', $product->softCopyPrice);
                    $product->setAttribute('productType', 'SoftCopy');
                    array_push($totalAmount, $product->softCopyPrice);
                }
                array_push($cartArray, $product);
            }
        } elseif ($request->sessionCheck === 'NC') {
            $count = '0';


            foreach ($request->itemId as $item) {
                $product = Product::where('id', $item['id'])->first();


                $products = new ProductResource($product);
                // return $product;


                switch ($product->prodType) {
                    case 'Market Reports':
                      $product->setAttribute('title', $product->marketReport->title);
                    break;
                    case 'Books':
                        $product->setAttribute('title', $product->productBook->title);
                     break;
                    case 'White Paper':
                        $product->setAttribute('title', $product->productWhitePaper->title);
                        break;
                    case 'Market Research':
                        $product->setAttribute('title', $product->marketResearch->title);
                        break;
                    case 'Journals':
                        $product->setAttribute('title', $product->productJournal->journalTitle);
                        break;
                    case 'Webinar':
                       // return $product;
                        $product->setAttribute('title', $product->productChapter->title);
                        break;
                    case 'Courses':
                        $product->setAttribute('title', $product->productCourse->title);
                        break;
                    case 'Podcast':
                        $product->setAttribute('title', $product->productChapter->title);
                        break;
                    case 'Events':
                        $product->setAttribute('title', $product->productEvent->title);
                        break;
                    case 'Videos':
                        $product->setAttribute('title', $product->productChapter->title);
                        break;
                    default:
                        return false;
                }

                $product->setAttribute('count', $count++);
                if ($item['prodType'] === 'Events') {
                    $product->setAttribute('amount', ($product->eventPrice) * ($item['quantities']));
                    $product->setAttribute('myQty', $item['quantities']);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, ($product->eventPrice) * ($item['quantities']));
                } elseif ($item['quantity']) {
                    $product->setAttribute('amount', $product->hardCopyPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->hardCopyPrice);
                } elseif ($item['prodType'] === 'AudioCopy') {
                    $product->setAttribute('amount', $product->audioPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->audioPrice);
                } elseif ($item['prodType'] === 'VideoCopy') {
                    $product->setAttribute('amount', $product->videoPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->videoPrice);
                } elseif ($item['prodType'] === 'Video') {
                    $product->setAttribute('amount', $product->videoPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->videoPrice);
                } elseif ($item['prodType'] === 'ReadOnline') {
                    $product->setAttribute('amount', $product->readOnlinePrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->readOnlinePrice);
                } elseif ($item['prodType'] === 'Audio') {
                    $product->setAttribute('amount', $product->audioPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->audioPrice);
                } elseif ($item['prodType'] === 'Subscription') {
                    $product->setAttribute('amount', $product->subscribePrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->subscribePrice);
                } elseif ($item['prodType'] === 'Streaming') {
                    $product->setAttribute('amount', $product->streamOnlinePrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->streamOnlinePrice);
                } elseif ($item['prodType'] === 'VideoEpisode') {
                    $productVideo = ProductVideoDetail::where('id', $item['mediaId'])->first();
                    $product->setAttribute('amount', $productVideo->price);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $productVideo->price);
                } else {
                    $product->setAttribute('amount', $product->softCopyPrice);
                    $product->setAttribute('inSession', 1);
                    array_push($totalAmount, $product->softCopyPrice);
                }






                array_push($cartArray, $product);
            }
        }

        $totalAmount = array_sum($totalAmount);
        $shippingPriceAmount = array_sum($shippingPriceAmount);

        return response()->json([
            'item' => $cartArray,
            'shippingPrice' => $shippingPriceAmount,
            'subTotal' => $totalAmount,
            'totalAmount' => $totalAmount + $shippingPriceAmount
        ]);
    }




    public function getCartNo($cart)
    {
        $cart = Cart::where('vendor', '=', $cart)->get();
        $cartCount = count($cart);
        return $cartCount;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
     
    
        $check = Cart::where('productId',$request->input('productId'))->where('vendor',auth('api')->user()->id)->first();

       if(is_null($check)){
        $cart = new Cart();
        $cart->productId = $request->input('productId');
        $cart->prodType = $request->input('prodType');
        $cart->vendor = auth('api')->user()->id;
        $cart->price = $request->input('price');
        $cart->quantity = $request->input('quantities');
        $cart->salePrice = $request->input('quantity') === 1 ? 1 : 0;
        $cart->vid = $request->input('mediaId');
        $cart->episodeVid = $request->input('vidEpisode');
        $cart->save();

        return $cart;
       }
       else{
           return response()->json([
               'status'=>200
           ]);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }

    public function deleteItem($id)
    {
        $cart = Cart::where('id', $id)->first();
        $cart->delete();

        return response()->json('Deleted Successfully');
    }
}
