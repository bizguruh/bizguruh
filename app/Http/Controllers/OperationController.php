<?php

namespace App\Http\Controllers;

use App\Operation;
use App\Product;
use App\VendorDetail;
use App\VendorUser;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;

class OperationController extends Controller
{
    public function getAllProducts()
    {
        $allProduct =  Product::all();
        return ProductResource::collection($allProduct);
    }

    public function showProduct(Product $product)
    {
        $product = Product::findOrFail($product->id);

        return new ProductResource($product);
    }

    public function verifyProduct($productid)
    {
        $product = Product::findOrFail($productid);
        if ($product->verify) {
            $product->verify = 0;
            $product->save();
            return $product;
        } else {
            $product->verify = 1;
            $product->save();
            return $product;
        }
    }

    public function featuredProduct($productid)
    {
        $product = Product::findOrFail($productid);
        if ($product->sponsor == 'Y') {
            $product->sponsor = 'N';
            $product->save();
            return $product;
        } elseif ($product->sponsor == 'N') {
            $product->sponsor = 'Y';
            $product->save();
            return $product;
        }
    }

    public function getAllVerifiedProduct()
    {
        return Product::where('verify', 1)->get();
    }

    public function getAllVendor()
    {
        return VendorUser::all();
    }

    public function getvendorDetail($detail)
    {
        $vendorDetail = VendorDetail::where('id', $detail)->get();
        $vendor = VendorUser::where('id', $detail)->get();
        $vendorU = VendorUser::where('username', $detail)->get();


        return response()->json([ 'status' => 0,
         'vendor' => $vendor,
          'vendorDetail' =>$vendorDetail,'vendorU' => $vendorU]);
    }

    public function disableEnableVendor($status)
    {
        $vendor = VendorUser::where('id', $status)->first();
        if ($vendor->verified) {
            $vendor->verified = 0;
            $vendor->save();
            return response()->json(['status' => 67, 'message' => 'disabled']);
        } else {
            $vendor->verified = 1;
            $vendor->save();
            return response()->json(['status' => 00, 'message' => 'enabled']);
        }
    }
}
