<?php

namespace App\Http\Controllers;

use App\FormTemplate;
use Illuminate\Http\Request;

class FormTemplateController extends Controller
{
    public function save(Request $request){
      
     return  FormTemplate::create([
         'group'=> 'First',
          'form_title'=> $request->title,
          'template'=>json_encode($request->template)
       ]);
    }

    public function update(Request $request){
      
    
        $user = auth('api')->user()->id;
        $form = FormTemplate::where('id', $request->id)->first();
          $form->group= $request->group;
          $form->form_title= $request->title;
          $form->template = json_encode($request->template);
          $form->save();
          return 'Updated';
   
    }
  public function deleteTemplate($id){
      $form = FormTemplate::find($id);
      $form->delete();
      return response()->json([
          'status'=>'deleted'
      ]);

  }
  public function deleteAllTemplate(Request $request)
  {
    
      foreach ($request->ids as  $value) {
          $form = FormTemplate::find($value);
          $form->delete();
      }
      return response()->json([
        'status'=>'deleted'
    ]);

  }
    public function getAllTemplates(){
        return FormTemplate::get();
    }

    public function getSingleTemplate($id){
        return FormTemplate::where('id',$id)->first();
    }
}
