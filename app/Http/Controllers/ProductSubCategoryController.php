<?php

namespace App\Http\Controllers;

use App\ProductSubCategory;
use Illuminate\Http\Request;
use App\Http\Resources\ProductSubCategory as ProductSubCategoryResource;

class ProductSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subCategory = ProductSubCategory::all();
        return ProductSubCategoryResource::collection($subCategory);
    }

    public function subjectMatter()
    {
        $productsubcategory =  ProductSubCategory::paginate(6);
        return $productsubcategory;

        //return ProductSubCategoryResource::collection($subCategory);
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getSubCategoryById($id){
        return ProductSubCategory::where('sub_category_id', '=', $id)->get();
    }


    public function getVendorSubCategoryById($id){
       return ProductSubCategory::where('sub_category_id', '=', 5)->where('adminCategory', '!=', 'AO')->get();
        // return  $productCategory;
       // return ProductSubCategoryResource::collection($productCategory);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
/*            'sub_category_id' => 'required'*/

        ]);

        $subcategory = new ProductSubCategory();
        $subcategory->name = $request->input('name');
        $subcategory->description = $request->input('description');
        $subcategory->adminCategory = $request->input('adminCategory');
        $subcategory->sub_category_id = $request->input('sub_category_id');
        $subcategory->save();
        return $subcategory;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductSubCategory  $productSubCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSubCategory $productSubCategory)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductSubCategory  $productSubCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($productSubCategory)
    {
        //
        return ProductSubCategory::where('id', $productSubCategory)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductSubCategory  $productSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSubCategory $productSubCategory)
    {
        //
        $this->validate($request, [
/*            'sub_category_id' => 'required',*/
            'name' => 'required|string|unique:product_sub_categories,id',
            'description' => 'required|string',
        ]);

        $productSubCategory = ProductSubCategory::where('id', $request->id)->first();
        $productSubCategory->name = $request->name;
        $productSubCategory->description = $request->description;
        $productSubCategory->adminCategory = $request->adminCategory;
        $productSubCategory->save();

        return $productSubCategory;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductSubCategory  $productSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($productSubCategory)
    {
        //
        $productSubCategory = ProductSubCategory::where('id', $productSubCategory)->first();
        $productSubCategory->delete();

        return $productSubCategory;
    }

    public function destroyMany(){
        $body =  json_decode(request()->getContent(), true);

        for($i = 0; $i < count($body); $i++){
            $subjectMatter = ProductSubCategory::where('id', $body[$i])->first();
            $subjectMatter->delete();
            return response()->json(['message', 'All item deleted']);


        }

    }
}
