<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Accounting;
use App\AccountUser;
use App\SpecialToken;
use App\Http\Requests;
use App\Mail\SignupMail;
use App\Mail\AccountingMail;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use Illuminate\Mail\Markdown;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CustomerRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phoneNo' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
            // 'password_confirmation' => 'required|string|min:6|same:password'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'lastName' => $data['lastName'],
            'email' => $data['email'],
            'phoneNo' => $data['phoneNo'],
            'count' => '1',
            'vendor_user_id'=> 0,
            'password' => Hash::make($data['password']),
            'specialToken' => $data['specialToken'],
            'address'=> $date['address']
        ]);
    }

    protected function register(CustomerRequest $request)
    {
        $account = new Accounting;

        DB::transaction(function () use ($request) {
            $user =  User::create([
                'name' => $request['name'],
                'lastName' => $request['name'],
                'email' => $request['email'],
                'phoneNo' => $request['phone_no'],
                'password' => Hash::make($request['password']),
                'count' => 1,
                'specialToken' => $request['specialToken'],
                'vendor_user_id'=> 0,
                'business_name'=>$request->business_name,
                'business_type'=>$request->business_type,
                'address' => $request->address
            ]);
           
    
          
            $user = User::where('email', $request->email)->first();
            $check = SpecialToken::where('token', $user->specialToken)->first();
            if ($request->phone_no == '8799') {
                Mail::to($request->email)->send(new SignupMail($user));
                
            }
            
            if ($check === null) {
                $userSubscriptionPlan = new UserSubscriptionPlan();
                $userSubscriptionPlan->price = 0;
                $userSubscriptionPlan->startDate = Carbon::now('Africa/Lagos');
                $userSubscriptionPlan->duration = null;
                $userSubscriptionPlan->level = 1;
                $userSubscriptionPlan->name = "community";
                $userSubscriptionPlan->endDate = Carbon::now('Africa/Lagos')->addYear();
                $userSubscriptionPlan->user_id = $user->id;
                $userSubscriptionPlan->status = 'active';
                $userSubscriptionPlan->type = 'entrepreneur';
                $userSubscriptionPlan->verify = 1;
                $userSubscriptionPlan->save();
    
                return response()->json([
               'success' => true,
               'message' => 'Registration Successful',
    
           ]);
            } else {
                $userSubscriptionPlan = new UserSubscriptionPlan();
                $userSubscriptionPlan->price = 0;
                $userSubscriptionPlan->startDate = $check->start_date;
                $userSubscriptionPlan->duration = null;
                $userSubscriptionPlan->level = 4;
                $userSubscriptionPlan->name = "Sponsor";
                $userSubscriptionPlan->endDate = $check->end_date;
                $userSubscriptionPlan->user_id = $user->id;
                $userSubscriptionPlan->verify = 1;
                $userSubscriptionPlan->save();
    
    
                return response()->json([
               'success' => true,
               'message' => 'token registration Successful',
              
           ]);
            }
    
            return response()->json([
                'success' => true,
                'message' => 'Registration Successful',
                'data' => $user
            ]);
        });
       
    }
    public function mail()
    {
        $markdown = new Markdown(view(), config('mail.markdown'));
        return $markdown->render('email.bapMail');
    }


    public function getUsers($email)
    {
        $users = User::where('email', $email)->first();
        return $users;
    }


    public function delCount($email)
    {
        $users = User::where('email', $email)->update(['count' => 0]);
    }
    public function addCount($email)
    {
        $users = User::where('email', $email)->update(['count' => 1]);

        return response()->json([
        'success' => true,
        'message' => 'Registration Successful',
        'data' => $users
    ]);
    }
    public function checkToken($token)
    {
        $check = SpecialToken::where('token', $token)->where('verify', 1)->first();
        $count = User::where('specialToken', $token)->get();
          
           

        if ($check !== null) {
            if (count($count)< $check->users) {
                return response()->json([
                    'message' => 'valid',
                    'data' => $check->token
                ]);
            } else {
                return response()->json([
                    'message' => 'limit'
    
                ]);
            }
        } else {
            return response()->json([
                'message' => 'invalid'

            ]);
        }
    }
    
    public function updateProfile(Request $request)
    {
        $user = $request->id;
      
        $profile = User::where('id', $user)->first();
        $profile->age = $request->age;
        $profile->name = $request->first_name;
        $profile->first_name = $request->first_name;
        $profile->lastName = $request->first_name;
        $profile->gender = $request->gender;
        $profile->location = $request->location;
        $profile->bio = $request->bio;
        $profile->business_type = $request->business_type;
        $profile->business_name = $request->business_name;
        $profile->phone_code = $request->phone_code;
        $profile->phoneNo = $request->phone_no;
        $profile->logo = $request->logo;
        $profile->address = $request->address;
        $profile->type = $request->industry;
        
        $profile->save();

        return response()->json([
            'message'=>'Success'
        ]);
    }
    
    public function retrieveUser($email)
    {
        return  User::where('email', $email)->first();
    }
}
