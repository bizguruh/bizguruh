<?php

namespace App\Http\Controllers;

use App\Mail\QuestionBank;
use App\User;
use App\UserDetail;
use App\VendorUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;


class UserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function sendUserQuestion(Request $request){
       
       $vendor = VendorUser::where('storeName',$request->vendor)->first();

        Mail::to('bizguruh@gmail.com')->bcc($vendor->email)->send(new QuestionBank($request));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function changeUserPassword(Request $request)
    {
        $this->validate($request, array(
             'oldPassword' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6|same:password'

        ));

        $id = auth('api')->user()->id;

        $user = User::where('id', $id)->first();

        if( Hash::check($request->oldPassword, $user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
            return 'success';
        }else{
            return Response::json([
                'status' => 'error',
                'msg'    => 'Error',
                'errors' => 'Password does not match existing password',
            ], 422);
        }

        //$user->password = Hash::make($request->password);
      //  $user->save();


    }

    public function getUserDetailFromProfile(){
        return auth('api')->user();


    }

    public function getUserDetailById($user){
        return UserDetail::where('user_id', $user)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user = new UserDetail();
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->phoneNo = $request->phoneNo;
        $user->address = $request->address;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->user_id = $request->user_id;
        $user->save();

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function show(UserDetail $userDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDetail $userDetail)
    {
        //
        $userDetail = UserDetail::findOrFail($userDetail->id);

        return $userDetail;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDetail $userDetail)
    {
        //
        $userDetail->update([
            $userDetail->firstName = $request->firstName,
            $userDetail->lastName = $request->lastName,
            $userDetail->phoneNo = $request->phoneNo,
            $userDetail->address = $request->address,
            $userDetail->state = $request->state,
            $userDetail->city = $request->city,
            $userDetail->country = $request->country,
            $userDetail->user_id = $request->user_id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetail $userDetail)
    {
        //
        $detail = UserDetail::findOrFail($userDetail->id);
        $detail->delete();

        return $detail;
    }
}
