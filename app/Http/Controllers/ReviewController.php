<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\Http\Resources\ReviewResource;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reviews =  Review::all();

        return ReviewResource::collection($reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getReviewsForProduct(Request $request){

        $review = Review::where('productId', $request->id)->where('verify', 1)->get();

        return ReviewResource::collection($review);
    }

    public function userSaveReview(Request $request){
        $this->validate($request, [
            'productId' => 'required',
//            'title' => 'required',
            'description' => 'required',
            'rating' => 'required'
        ]);

        $productId = $request->productId;

        $userId = auth('api')->user()->id;

        $orders = Order::where('user_id', $userId)->where('verify', 1)->get();

        $reviewDetail  = [];

        //return $order->orderDetail;

        foreach($orders as $order){

         //   return count($order->orderDetail->where('productId', $productId)->all());

            if(count($order->orderDetail->where('productId', $productId)->all()) > 0){
                array_push($reviewDetail,  'added');
            }
        }

        $review = Review::where('userId', $userId)->where('productId', $productId)->get();


        if(count($reviewDetail) > 0 && (count($reviewDetail) > count($review))){

            $reviews = new Review();
            $reviews->title = 'title';
            $reviews->description = $request->description;
            $reviews->productId = $request->productId;
            $reviews->userId = $userId;
            $reviews->rating = $request->rating;
            $reviews->save();
            return $reviews;

        }else{
            return response()->json([ 'message' => 'You Cannot review this product until you purchase it']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
        $review->delete();
        return response()->json('Deleted Successfully');
    }
}
