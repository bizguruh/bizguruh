<?php

namespace App\Http\Controllers;

use App\User;
use App\Expenses;
use Carbon\Carbon;
use App\AccountUser;
use App\CgatePayment;
use Illuminate\Http\Request;
use App\CgatePayementHistory;
use App\UserSubscriptionPlan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AccountUssdController extends Controller
{
    public function UniqueRandomNumbersWithinRange($min, $max, $quantity)
    {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

   
    public function startUp()
    {
        return response()->json([
            'data'=> "0. Register,\n1. Subscribe,\n2. Daily income,\n3. Daily expense,\n4.Check account",
            'endofsession'=>1
        ]);
    }
    public function getUser(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();

        return response()->json([
            'data'=>'Welcome '.$user->name,
            'endofsession'=>1
        ]);
    }
    public function register($request)
    {
        $numbers = mt_rand(100, 1000);
      

        
        $resp =  DB::transaction(function () use ($request) {
            $user = User::create([
                'name' => $request->name,
                'lastName' => $request->name,
                'email'=>'example@email.com',
                'phoneNo' => $request->msiisdn,
                'count' => '1',
                'vendor_user_id'=> 0,
                'password' => Hash::make('admin'),
                'specialToken' => null,
                'address'=> null,
               
            ]);
    
            AccountUser::create([
                'user_id'=>$user->id,
                'name'=>$request->name,
                'business_name'=>$request->business_name,
                'email'=>'example@email.com',
                'role'=>'admin',
                'password'=> Hash::make('admin'),
                'verify'=>false,
                'phone_no'=>$request->msiisdn,
                'customer_ref'=>$numbers
            ]);
            return response()->json([
                'data'=>'Registration successful',
                'endofsession'=>1
            ]);
        });
        return $resp;
    }

    public function addincome(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();


            
        Expenses::create([
            'user_id' =>  $user->user_id,
            'transaction' => 'income',
            'amount' => $request->amount,
            'category' => 'others',
            'payment_type' => 'bank',
            'date' => Carbon::now('Africa/lagos'),
            'note' => 'ussd',
            'month' =>Carbon::now('Africa/lagos')->month,
        ]);
        return response()->json([
            'data'=>'Transaction successful',
            'endofsession'=>1
        ]);
    }
    public function addexpense(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();
        Expenses::create([
            'user_id' =>  $user->user_id,
            'transaction' => 'expense',
            'amount' => $request->amount,
            'category' => 'others',
            'payment_type' => 'bank',
            'date' => Carbon::now('Africa/lagos'),
            'note' => 'ussd',
            'month' =>Carbon::now('Africa/lagos')->month,
        ]);
        return response()->json([
            'data'=>'Transaction successful',
            'endofsession'=>1
        ]);
    }

    public function checkincome(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();
        $incomes = Expenses::where('user_id', $user->user_id)->where('transaction', 'income')->get();
        $message;

        if ($request->period=='annual') {
            $data = array_filter($incomes->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year;
            });
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);

            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });

        
            $message = number_format($resp);
        }
        if ($request->period=='monthly') {
            $data = array_filter($incomes->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month));
            });


           
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);
  
            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });
  
          
            $message = number_format($resp);
        }
        if ($request->period=='daily') {
            $data = array_filter($incomes->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month)) && Carbon::parse($v['date'])->day  == intval($request->day);
            });


           
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);
  
            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });
  
          
            $message = number_format($resp);
        }
        return response()->json([
            'data'=>$message.' naira',
            'endofsession'=>1
        ]);
    }

    public function checkexpense(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();
        $expenses = Expenses::where('user_id', $user->user_id)->where('transaction', 'expense')->get();
        $message;

        if ($request->period=='annual') {
            $data = array_filter($expenses->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year;
            });
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);

            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });

       
            $message = number_format($resp);
        }
        if ($request->period=='monthly') {
            $data = array_filter($incomes->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month));
            });


          
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);
 
            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });
 
         
            $message = number_format($resp);
        }
        if ($request->period=='daily') {
            $data = array_filter($incomes->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month)) && Carbon::parse($v['date'])->day  == intval($request->day);
            });


          
            $amount = array_map(function ($v) {
                return $v['amount'];
            }, $data);
 
            $resp = array_reduce($amount, function ($a, $b) {
                return intval($a) + intval($b);
            });
 
         
            $message = number_format($resp);
        }
        return response()->json([
           'data'=>$message.' naira',
           'endofsession'=>1
       ]);
    }
    public function checkbalance(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();
        $transactions = Expenses::where('user_id', $user->user_id)->get();
        $message;

        if ($request->period=='annual') {
            $data = array_filter($transactions->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year;
            });
            $expense = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'expense';
            });
            $income = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'income';
            });


            $expense_amount = array_map(function ($v) {
                return $v['amount'];
            }, $expense);

            $income_amount = array_map(function ($v) {
                return $v['amount'];
            }, $income);

            $resp_expense = array_reduce($expense_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);
            $resp_income = array_reduce($income_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);

            $balance = intval($resp_income) - intval($resp_expense);

       
            $message = number_format($balance);
        }
        if ($request->period=='monthly') {
            $data = array_filter($transactions->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month));
            });


            $expense = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'expense';
            });
            $income = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'income';
            });
    
    
            $expense_amount = array_map(function ($v) {
                return $v['amount'];
            }, $expense);
    
            $income_amount = array_map(function ($v) {
                return $v['amount'];
            }, $income);
    
            $resp_expense = array_reduce($expense_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);
            $resp_income = array_reduce($income_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);
    
            $balance = intval($resp_income) - intval($resp_expense);
    
           
            $message = number_format($balance);
        }
        if ($request->period=='daily') {
            $data = array_filter($transactions->toArray(), function ($v) use ($request) {
                return Carbon::parse($v['date'])->year === $request->year && Carbon::parse($v['date'])->month  == date('m', strtotime($request->month)) && Carbon::parse($v['date'])->day  == intval($request->day);
            });

            $expense = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'expense';
            });
            $income = array_filter($data, function ($v) use ($request) {
                return $v['transaction'] === 'income';
            });
    
    
            $expense_amount = array_map(function ($v) {
                return $v['amount'];
            }, $expense);
    
            $income_amount = array_map(function ($v) {
                return $v['amount'];
            }, $income);
    
            $resp_expense = array_reduce($expense_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);
            $resp_income = array_reduce($income_amount, function ($a, $b) {
                return intval($a) + intval($b);
            }, 0);
    
            $balance = intval($resp_income) - intval($resp_expense);
    
           
            $message = number_format($balance);
        }
        return response()->json([
           'data'=>$message.' naira',
           'endofsession'=>1
       ]);
    }

    public function ussdsubscribe(Request $request)
    {
        $user = AccountUser::where('phone_no', $request->msiisdn)->first();
        if (!is_null($user)) {
            $checkgate = CgatePayment::where('user_id', $user->user_id)->first();
            $number = mt_rand(1000, 10000);
            if (is_null($checkgate)) {
                CgatePayment::create([
                'ref'=>$number,
                'user_id' =>  $user->user_id,
                'amount' =>3000,
            
            ]);
            } else {
                $checkgate->ref = $number;
                $checkgate->save();
            }
       
       
            return response()->json([
            'data'=>'To subscribe to our accounting plan, Dial *222# and follow the prompt, Your reference code is '.$number,
            'endofsession'=>2
        ]);
        } else {
            return response()->json([
            'data'=>'You have to be registered first',
            'endofsession'=>2
        ]);
        }
    }

    public function handleAccounting(Request $request)
    {
        switch ($request->input) {
             case '*6025*20#':
                return $this->startUp();
                 break;
                 case '0':
                    return $this->register($request);
                     break;
                     case '1':
                        return $this->ussdsubscribe($request);
                         break;
                         case '2':
                            return response()->json([
                                'data'=>'Enter amount',
                                'endofsession'=>1
                            ]);
                            // return $this->addincome($request);
                             break;
                             case '3':
                                return response()->json([
                                    'data'=>'Enter amount',
                                    'endofsession'=>1
                                ]);
                                // return $this->addexpense($request);
                                 break;
                                 case '*4':
                                    return $this->checkbalance();
                                     break;
             
             default:
                 # code...
                 break;
         }
    }


    // CGATE Payment
    // BillerCode: 459
    // MID:
    
    // KEY: 07ff5916-cb29-4e3a-a774-3e87996c2c07
    public function getdetails(Request $request)
    {
      
           
        try {
        $customer_ref = substr($request->customerRef, 4);
           
        $mid = $request->merchantId;
      
        $details =  CgatePayment::where('ref', $request->customerRef)->first();
          

        if (!is_null($details)) {
            $user =  AccountUser::where('user_id', $details->user_id)->first();
            
            $traceId = mt_rand(1000, 10000000);
            // $traceId = 572131;
           
             $checkTrace = CgatePayementHistory::where('traceId',$traceId)->first();
            while (!is_null($checkTrace)) {
               
                $traceId = mt_rand(1000, 10000000);
                $checkTrace = CgatePayementHistory::where('traceId',$traceId)->first();
              
             
            }
            return response()->json([
                    "traceId"=> strval($traceId),
                    "customerName"=> $user->name,
                    "amount"=> $details->amount,
                    "displayMessage"=> $user->name." Monthly Payment",
                    "responseCode"=> "00",
                  
                   
            ]);
        } else {
            return response()->json([
                
                "ResponseCode"=> "01",
                "ResponseMessage"=> "Failed"
           ]);
        }
            } catch (\Throwable $th) {
            return response()->json([
            "ResponseCode"=> "01",
            "ResponseMessage"=> "Failed"
       ]);
        }
    }

    //             "passBackReference": "12341820220", "traceId": "12341820220",
    //             "paymentReference": "03848982793092", "customerRef": "12345", "responseCode":"00", "merchantId":"93493MIU93832", "mobileNumber":"0808***7273",
    //             "amount":1000, "transactionDate":"2020-07-01T18:20:16.465107+01:00", "shortCode":"894",
    // "currency":"NGN",
    // "channel":"USSD",
    // "hash": "093483ierl8w7s0s0-skj2j3k3wsj383mdlw838",

    public function subscription(Request $request)
    {
        try {
            $customer_ref = $request->customerRef;
            $details =  CgatePayment::where('ref', $customer_ref)->first();
            $user =  AccountUser::where('user_id', $details->user_id)->first();
            
            CgatePayementHistory::create([
                "passBackReference" =>$request->passBackReference,
                "traceId"  =>$request->traceId,
                "paymentReference"  =>$request->paymentReference,
                "customerRef"  =>$request->customerRef,
                 "responseCode"  =>$request->responseCode,
                 "merchantId"  =>$request->merchantId,
                 "mobileNumber"  =>$request->mobileNumber, 
                 "amount"  =>$request->amount,
                 "transactionDate"  =>$request->transactionDate,
                 "shortCode"  =>$request->shortCode,
                 "currency"  =>$request->currency,
                 "channel"  =>$request->Channel,
                 "hash" =>$request->hash
            ]);

            if (!is_null($user)) {
                $checkSub = UserSubscriptionPlan::where('user_id', 120)->first();
                if (is_null($checkSub)) {
                  
                    $subscription = new UserSubscriptionPlan;
                    $subscription->price = 2500;
                    $subscription->startDate = Carbon::now('Africa/lagos');
                    $subscription->duration = 'yearly';
                    $subscription->level = 1;
                    $subscription->name = 'Started kit';
                    $subscription->endDate = Carbon::now('Africa/lagos')->addYear();
                    $subscription->user_id =  $details->user_id;
                    $subscription->type = 'accounting';
                    $subscription->verify = 1;
                    $subscription->status = 'active';
                    $subscription->save();
                    $subscription;
                } else {
                  
                    $checkSub->price = 2500;
                    $checkSub->startDate = Carbon::now('Africa/lagos');
                    $checkSub->duration = 'yearly';
                    $checkSub->level = 1;
                    $checkSub->name = 'Started kit';
                    $checkSub->endDate = Carbon::now('Africa/lagos')->addYear();
                    $checkSub->type = 'accounting';
                    $checkSub->verify = 1;
                    $checkSub->status = 'active';
                    $checkSub->save();
                }
              

                // $details->delete();
                              
                return response()->json([
                                     "ResponseCode"=> "00",
                                     "ResponseMessage"=> "Successful"
                                ]);
            } else {
                return response()->json([
                    "ResponseCode"=> "01",
                    "ResponseMessage"=> "User not found"
               ]);
            }
        } catch (\Throwable $th) {
            // return $th;
            return response()->json([
                                        "ResponseCode"=> "01",
                                        "ResponseMessage"=> "Failed"
                                   ]);
        }
    }
}
