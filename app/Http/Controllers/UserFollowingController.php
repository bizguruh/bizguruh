<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\UserFollowing;
use Illuminate\Http\Request;

class UserFollowingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserFollowing $userFollowing)
    {
        $following = UserFollowing::get();
        return $following;
    }


    public function getFollowers($vendor){
     
     
        $userF = UserFollowing::where('vendorId', $vendor)->get();
       
        return count($userF);

    }
    public function getFollowersWeekly($vendor){
     
    //    $follow = [];
        $userF = UserFollowing::where('vendorId', $vendor)->where('created_at', '>', Carbon::now()->startOfWeek())
        ->where('created_at', '<', Carbon::now()->endOfWeek())->get();
       
        // $now = Carbon::now();
        // foreach ($userF as $item) {
           
        //     if ($now->weekOfYear === Carbon::parse($item->created_at)->weekOfYear) {
        //         array_push($follow,$item);
        //     }
        // }
         return count($userF);
       

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //
        $id = auth('api')->user()->id;

        $this->validate($request, array(
            'vendorName' => 'required',
            'vendorId' => 'required',
        ));

        $userF = UserFollowing::where('user_id', $id)->where('vendorId', $request->vendorId)->get();
        if(count($userF) > 0){
              foreach ($userF as $user){
                  if($user->vendorId === $request->vendorId){
                      return 'Already following';
                  }
              }
        }else {
            $userFollowing = new UserFollowing();
            $userFollowing->vendorName = $request->vendorName;
            $userFollowing->vendorId = $request->vendorId;
            $userFollowing->user_id = $id;
            $userFollowing->save();
            return $userFollowing;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserFollowing  $userFollowing
     * @return \Illuminate\Http\Response
     */
    public function show($vendor)
    {
         $id = auth('api')->user()->id;
        
        $userF = UserFollowing::where('user_id', $id)->where('vendorId', $vendor)->get();

   
                  if(count($userF) === 1){
                      return 'true';
                  }else{
                      return 'false';
                  }
              


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserFollowing  $userFollowing
     * @return \Illuminate\Http\Response
     */
    public function edit(UserFollowing $userFollowing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserFollowing  $userFollowing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserFollowing $userFollowing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserFollowing  $userFollowing
     * @return \Illuminate\Http\Response
     */
    public function destroy( $vendor)
    {
        $user =   auth('api')->user()->id;
        $userfollow = UserFollowing::where('user_id', $user)->where('vendorId' , $vendor)->first();
        $userfollow->delete();
       return response()->json(['message', 'Vendor Un-followed']);
    }
}
