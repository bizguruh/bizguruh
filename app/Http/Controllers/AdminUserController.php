<?php

namespace App\Http\Controllers;

use App\AdminUser;
use App\Http\Requests\OpsRegisterRequest;
use App\Mail\VerifyOpsMail;
use App\OpsUser;
use App\Product;
use App\ProductCategory;
use App\ProductSubCategory;
use App\ProductSubCategoryBrand;
use App\Review;
use App\User;
use App\VendorUser;
use App\VerifyAdmin;
use App\VerifyOps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\VendorUser as VendorUserResource;

class AdminUserController extends Controller
{




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminUser $adminUser)
    {
        //

    }

    public function getUserForAdmin(){
        return User::all();
    }

    public function getSubjectMatterForAdminDisplay(){
        return ProductSubCategory::paginate(10);
    }

    public function getTopicForAdminDisplay(){
        return ProductSubCategoryBrand::paginate(10);
    }

   public function getVendorForProduct(){
        return VendorUser::where('type', 'vendor')->where('verified', 1)->get();
   }

   public function getExpertsForProduct()
   {
       return VendorUser::where('type', 'expert')->where('verified', 1)->get();
   }


    public function getAllAdminUser(){
        return AdminUser::all();
    }

    public function getCategory(){

        $category =  ProductCategory::where('adminCategory', '!=', 'UO')->get();
        return $category;
    }


    public function getSubCategoryById($id){

        return ProductSubCategory::where('sub_category_id', '=', $id)->where('adminCategory', '!=', 'UO')->get();

    }

    public function getSubTopicFromSubCategory($id){

        $productSubCategoryBrand = ProductSubCategoryBrand::where('sub_brand_category_id', $id)->where('adminCategory', '!=', 'UO')->get();

        return $productSubCategoryBrand;
    }


    public function createAdminLevel(Request $request){
        $adminUser = AdminUser::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => (int) $request['role']
        ]);

        $verifyAdmin = VerifyAdmin::create([
         'admin_user_id' => $adminUser->id,
            'token' => str_random(40)
        ]);

        Mail::to($adminUser->email)->send(new \App\Mail\AdminUser($adminUser));

        return response()->json([
            'success' => true,
            'message' => 'Registration Successful',
            'data' => $adminUser
        ]);
    }


    public function createOpsUser(OpsRegisterRequest $request){
        $opsUser = OpsUser::create([
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);


        $verifyOps = VerifyOps::create([
            'ops_user_id' => $opsUser->id,
            'token' => str_random(40)
        ]);

        Mail::to($opsUser->email)->send(new VerifyOpsMail($opsUser));

        return response()->json([
            'success' => true,
            'message' => 'Registration Successful',
            'data' => $opsUser
        ]);
    }


    public function verifyOps($token)
    {
        $verifyOps = VerifyOps::where('token', $token)->first();
        if (isset($verifyOps)) {
            $ops = $verifyOps->opsUser;
            if (!$ops->verified) {
                $verifyOps->opsUser->verified = 1;
                $verifyOps->opsUser->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/')->with('status', $status);
    }

    public function verifyAdmin($token){
        $verifyAdmin = VerifyAdmin::where('token', $token)->first();
        if(isset($verifyAdmin)){
            $admin = $verifyAdmin->adminUser;
            if(!$admin->verified) {
                $verifyAdmin->adminUser->verified = 1;
                $verifyAdmin->adminUser->save();
                $status = "Your e-mail is verified. You can now login. ";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/')->with('status', $status);
    }


    public function getAllVendor(){
        $allVendor = VendorUser::all();
        return $allVendor;
    }


    public function getAllProductsForAdmin(){
/*        $allAdminProduct = Product::where('draftValue', 'N')->get();*/
        $allAdminProduct = Product::all();
        return ProductResource::collection($allAdminProduct);
    }


    public function getAllUniverifyProduct(){
        $allAdmin = Product::where('verify', 0)->where('draftValue', 'N')->get();
         return ProductResource::collection($allAdmin);
    }



    public function showProductForAdmin(Product $product){
        $product = Product::findOrFail($product->id);

        return new ProductResource($product);
    }

    public function verifyProductForAdmin($productid){
        $product = Product::findOrFail($productid);
        if($product->verify){
            $product->verify = 0;
            $product->save();
            return $product;

        }else{
            $product->verify = 1;
            $product->save();
            return $product;
        }
    }

    public function verifybizguruhProduct($id){
        $product = Product::findOrFail($id);
        if($product->verify && $product->draftValue === 'N'){
            $product->verify = 0;
            $product->save();
            return $product;
        }else{
            $product->verify = 1;
            $product->save();
            return $product;
        }

    }


    public function verifyReview($review){
        $review = Review::findOrFail($review);
        if($review->verify){
            $review->verify = 0;
            $review->save();
            return $review;
        }else{
            $review->verify = 1;
            $review->save();
            return $review;
        }
    }


    public function featuredProductForAdmin($productid){
        $product = Product::findOrFail($productid);
        if($product->sponsor == 'Y'){
            $product->sponsor = 'N';
            $product->save();
            return $product;
        }elseif ($product->sponsor == 'N'){
            $product->sponsor = 'Y';
            $product->save();
            return $product;
        }
    }

    public function getAllVendorUser(){
        $vendorUser = VendorUser::all();
        return  VendorUserResource::collection($vendorUser);
    }

    public function getVendorDetail($id){
        $vendorDetail = VendorUser::where('id', $id)->get();
        return VendorUserResource::collection($vendorDetail);
    }

    public function getAllOpsUser(){
        $opsUser = OpsUser::all();
        return $opsUser;
    }

    public function enableOrDisableVendorForAdmin($ops){

        $ops = OpsUser::where('id', '=',$ops)->first();
        if($ops['verified']){
            $ops['verified'] = 0;
            $ops->save();
            return $ops;
        }else{
            $ops['verified'] = 1;
            $ops->save();
            return $ops;
        }
     }

     public function deleteProduct(Product $product){
        $product->delete();

         return response()->json('Deleted Successfully');

     }

    public function destroyMany(){
        $body =  json_decode(request()->getContent(), true);

        for($i = 0; $i < count($body); $i++){
            $product = Product::where('id', $body[$i])->first();
            $product->delete();
        }
        return response()->json(['message', 'All item deleted']);

    }



}
