<?php

namespace App\Http\Controllers;

use App\ChecklistAnswer;
use App\Question;
use Illuminate\Http\Request;
use App\User;

use function GuzzleHttp\json_encode;

class ChecklistAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
       
       return ChecklistAnswer::where('user_id', $id)->get();
    }
    public function getHistory($id)
    {
       
       return ChecklistAnswer::where('user_id', $id)->get();
    }

    public function getSingleHistory(Request $request )
    {
      
       
       return ChecklistAnswer::where('question_id', $request->question_id)->where('user_id', $request->user_id)->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $check = ChecklistAnswer::where('question_id', $request->question_id)->where('user_id', $request->user_id)->first();
  
       if(is_null($check)){
        ChecklistAnswer::create([
            'user_id'=> $request->user_id,
            'title'=> $request->title,
            'name'=> $request->name,
            'question_id'=> $request->question_id,
            'answers'=> json_encode($request->answers),
            'response' => json_encode($request->response)
        ]);
        return  response()->json(['status' => 'created']);

       }else{
           $check->answers = json_encode($request->answers);
           $check->save();
           return  response()->json(['status' => 'updated']);

       }
       
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = auth('api')->user()->id;
       return ChecklistAnswer::where('user_id', $user_id)->where('id', $id)->get();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
