<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getSubscriptionForUser(){
        return Subscription::all();
    }

    public function index()
    {
        //
        $subcription = Subscription::all();
        return $subcription;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
           'title' => 'required',
           'price' => 'required',
           'moreInfo' => 'required',
           'level' => 'required'
        ]);

        $subscriptionCheck = Subscription::where('level', $request->level)->first();
        if(isset($subscriptionCheck)){
            $subscriptionCheck->title = $request->input('title');
            $subscriptionCheck->price = $request->input('price');
            $subscriptionCheck->duration = $request->input('duration');
            $subscriptionCheck->moreInfo = $request->input('moreInfo');
            $subscriptionCheck->level = $request->input('level');
            $subscriptionCheck->save();
            return $subscriptionCheck;
        }else {
            $subscription = new Subscription();
            $subscription->title = $request->input('title');
            $subscription->price = $request->input('price');
            $subscription->duration = $request->input('duration');
            $subscription->moreInfo = $request->input('moreInfo');
            $subscription->level = $request->input('level');
            $subscription->save();
            return $subscription;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }
}
