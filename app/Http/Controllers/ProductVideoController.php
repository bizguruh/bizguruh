<?php

namespace App\Http\Controllers;

use App\ProductVideoDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use JD\Cloudder\Facades\Cloudder;

class ProductVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getEpisodeByProductChapter($id){
        $productVideo = ProductVideoDetail::where('productId', $id)->get();

        return $productVideo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $productVideo = new ProductVideoDetail();
        $productVideo->title = $request->input('title');
        $productVideo->description = $request->input('description');
        $productVideo->mediaType = $request->input('mediaType');
         $productVideo->videoFile = $request->input('videoFile');
         $productVideo->videoName = $request->input('videoName');
        /*if($request->hasFile('videoFile')) {
            $media = $request->file('videoFile');
            $mediaName = 'Media-'.time();
            Cloudder::uploadVideo($media, $mediaName, array('timeout' =>120000));
            $productVideo->videoFile = $mediaName;
            $productVideo->videoName = $request->videoName;
        }*/
        $productVideo->productId = $request->input('productId');
        $productVideo->save();
        return $productVideo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productVideo = ProductVideoDetail::findOrFail($id);

        return $productVideo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $productVideo = ProductVideoDetail::findOrFail($id);

        $productVideo->title = $request->input('title');
        $productVideo->description = $request->input('description');
        $productVideo->mediaType = $request->input('mediaType');
        $productVideo->videoFile = $request->input('videoFile');
        $productVideo->videoName = $request->input('videoName');
        $productVideo->productId = $request->input('productId');




        /* if($request->hasFile('videoFile')){
             $media = $request->file('videoFile');
             $oldFileName = $productVideo->videoFile;
             Cloudder::delete($oldFileName);
             $mediaName = 'Media-'.time();
             Cloudder::uploadVideo($media, $mediaName, array('timeout' =>120000));
             $productVideo->videoFile = $mediaName;
             $productVideo->videoName = $request->videoName;
         }*/

        $productVideo->save();

        return $productVideo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productVideo = ProductVideoDetail::findOrFail($id);

        $oldFileName = $productVideo->videoFile;
       Cloudder::destroy($oldFileName);
        $productVideo->delete();
        return $productVideo;
    }
}
