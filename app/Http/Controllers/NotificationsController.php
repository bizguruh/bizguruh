<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use Carbon\Carbon;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Product as ProductResource;

class NotificationsController extends Controller
{
    public function send(Request $request)
    {
        $now = Carbon::now()->dayOfWeek;

        $apikey = '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt';
        $send_data=array(
                'campaign_name'=>'Content Upload',
                'notification'=> [
                    'title'=>'Content upload reminder',
                    'message'=>'This is to remind you to send in your content today',
                    'icon'=>'https://engagespot.co/webpushicon.png',
                    'url'=>'http://localhost:8000/vendor/dashboard'
                ],
                'send_to'=>'identifiers',
                'identifiers'=>[174]
        );

        $payload=json_encode($send_data);
        $submit_url='https://api.engagespot.co/2/campaigns';
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $submit_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "api-key:".$apikey,
            'content-type: application/json'

        ]);
      
        $result=curl_exec($ch);
        curl_close($ch);
        $mcdata=json_decode($result);
       

        if (!empty($mcdata->error)) {
            return "Mailchimp Error: ".$mcdata->error;
        }
        return $result;
    }
    public function sendNotification(Request $request)
    {
        $user_id =  auth('api')->user()->id;
        $vendor = Product::where('id', $request->product)->value('vendor_user_id');
        $check =  Notification::where('user', $user_id)->where('product', $request->product)->where('vendor', $vendor)->where('message', $request->type)->first();
        if (is_null($check)) {
            return   Notification::create([
            'user' =>$user_id,
            'vendor' => $vendor,
            'product' => $request->product,
            'message' => $request->type,
            'time'=> Carbon::now()
        ]);
        } else {
            return 'present';
        }
    }
    public function getNotifications()
    {
        $vendor_id =  auth('vendor')->user()->id;
        $notify = DB::table('notifications')
        ->join('users', 'notifications.user','=', 'users.id')
        ->join('products', 'notifications.product','=', 'products.id')
        ->join('products_courses', 'notifications.product','=', 'products_courses.product_id')
        ->where('vendor', $vendor_id)
        // ->where('message', 'subscribe')
        // ->orWhere('message', 'purchase')
        ->get();
        return $notify;
    }
    public function getUsers()
    {
      
      
    }
}
