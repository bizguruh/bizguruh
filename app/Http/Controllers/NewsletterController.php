<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Auth;
use Config;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{

    // public $mailchimp;
    // public $listId = '800d2be573';


    // public function __construct(\Mailchimp $mailchimp)
    // {
    //     $this->mailchimp = $mailchimp;
    // }


 

    // public function subscribes(Request $request)
    // {

     
    // 	$this->validate($request, [
	//     	'email' => 'required|email',
    //     ]);


    //     try {

        	
    //         $this->mailchimp
    //         ->lists
    //         ->subscribe(
    //             $this->listId,
    //             ['email' => $request->input('email')]
    //         );


    //         return redirect()->back()->with('success','Email Subscribed successfully');


    //     } catch (\Mailchimp_List_AlreadySubscribed $e) {
    //         return redirect()->back()->with('error','Email is Already Subscribed');
    //     } catch (\Mailchimp_Error $e) {
    //         return redirect()->back()->with('error','Error from MailChimp');
    //     }
    // }


    public function sendCampaign(Request $request)
    {
 
    	$this->validate($request, [
	    	'subject' => 'required',
	    	// 'to_email' => 'required',
	    	// 'from_email' => 'required',
	    	'message' => 'required',
        ]);


        try {


	        $options = [
	        'list_id'   => $this->listId,
	        'subject' => $request->input('subject'),
	        'from_name' => $request->input('from_name'),
	        'from_email' => 'bizguruh@bizguruh.com',
	        'to_email' => 'bizguruh@gmail.com'
	        ];


	        $content = [
	        'html' => $request->input('message'),
	        'text' => strip_tags($request->input('message'))
	        ];


	        $campaign = $this->mailchimp->campaigns->create('regular', $options, $content);
	        $this->mailchimp->campaigns->send($campaign['id']);


        	return redirect()->back()->with('success','send campaign successfully');

        	
        } catch (Exception $e) {
        	return redirect()->back()->with('error','Error from MailChimp');
        }
    }

    public function subscribe(Request $request)
    {

        $newsletter = new Newsletter();

        return  $newsletter->subscribe($request);
    }
   
   
   
    public function newList(Request $request){
        $apikey ='b59706b51236cbcd5ed95fcb34952ad5-us4';

        $data = array( // the information for your new list--not all is required
            "name" => $request->name,
            "contact" => array (
                "company" => $request->company,
                "address1" => "",
                "address2" => "",
                "city" => "",
                "state" => "",
                "zip" => "",
                "country" => 'Nigeria',
                "phone" => ''
            ),
            "permission_reminder" => "You signed up for this mail from our website",
            "use_archive_bar" => true,
            "campaign_defaults" => array(
                "from_name" => "",
                "from_email" => "",
                "subject" => "",
                "language" => 'english'
            ),
            "notify_on_subscribe" => "",
            "notify_on_unsubscribe" => "",
            "email_type_option" => true,
            "visibility" => 'pub'
        );

        $payload=json_encode($data);
        $submit_url="https://us4.api.mailchimp.com/3.0/lists/";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $submit_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_USERPWD, "user:".$apikey);
        $result=curl_exec($ch);
        curl_close($ch);
        $mcdata=json_decode($result);
       

        if (!empty($mcdata->error)) {
            return "Mailchimp Error: ".$mcdata->error;
        }
        return $result;
    }
    
}
