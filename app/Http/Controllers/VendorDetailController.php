<?php

namespace App\Http\Controllers;

use App\VendorDetail;
use Illuminate\Http\Request;

class VendorDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       /* $vendorDetail = new VendorDetail();
        $vendorDetail->firstName = $request->firstName;
        $vendorDetail->lastName = $request->lastName;
        $vendorDetail->address = $request->address;
        $vendorDetail->city = $request->city;
        $vendorDetail->state = $request->state;
        $vendorDetail->country = $request->country;
        $vendorDetail->image = 1;
        $vendorDetail->bio = $request->bio;
        $vendorDetail->vendor_id = $request->vendor_id;
        $vendorDetail->save();*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
