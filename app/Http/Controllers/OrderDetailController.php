<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\OrderDetailVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getOrderProduct($getId, $vid, $type)
    {


        $id = auth('api')->user()->id;
        $orders = Order::where('user_id', $id)->get();
        $arr = [];

        if (is_numeric($vid)) {
            $Ord = OrderDetail::where('vid', $vid)->first();
            $Ord->setAttribute('mediaPublicId', $Ord->itemPurchase);
            array_push($arr, $Ord);
            return $arr;
        } else if ($type === 'VideoCopy') {
            $videoCopy = [];
            foreach ($orders as $order) {


                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();

                        foreach ($productDetail as $productDetails) {
                            $productDetails->setAttribute('type', 'videocopy');
                            array_push($videoCopy, $productDetails);
                        }

                        return $videoCopy;
                    }
                }
            }
        } else if ($type === 'podcast-audio') {
            $podcastAudio = [];
            foreach ($orders as $order) {
                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
                        foreach ($productDetail as $productDetails) {
                            $productDetails->setAttribute('type', 'podcastAudio');
                            array_push($podcastAudio, $productDetails);
                        }
                    }
                }
            }
            return $podcastAudio;
        } else if ($type === 'book-audio') {
            $bookAudio = [];
            $Ord = OrderDetail::where('id', $getId)->first();
            $Ord->setAttribute('mediaPublicId', $Ord->category);
            array_push($bookAudio, $Ord);
            return $bookAudio;
        } else if ($type === 'readonline' || $type === 'download') {
            $readOnline = [];
            $Ord = OrderDetail::where('id', $getId)->first();
            $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
            array_push($readOnline, $Ord, $productDetail);

            $order = DB::table('order_details')
                ->leftJoin('order_detail_videos', 'order_details.id', '=', 'order_detail_videos.order_id')
                ->select('*')
                ->where('order_detail_videos.order_id', $getId)
                ->get();



            return $order;
        } else if ($vid === 'courses') {
            $courses = [];
            foreach ($orders as $order) {
                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
                        foreach ($productDetail as $productDetails) {
                            $productDetails->setAttribute('type', 'courseDigital');
                            array_push($courses, $productDetails);
                        }
                        return $courses;
                    }
                }
            }
        } else if ($vid === 'CoursesReadOnline') {
            $readOnlineCourses = [];

            foreach ($orders as $order) {
                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
                        foreach ($productDetail as $productDetails) {
                            $productDetails->setAttribute('type', 'courseReadOnline');
                            array_push($readOnlineCourses, $productDetails);
                        }
                        return $readOnlineCourses;
                    }
                }
            }
        } else if ($type === 'Video-Insight-Subscription') {
            $videoCopy = [];

            foreach ($orders as $order) {
                // return $order->orderDetail;


                foreach ($order->orderDetail as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
                        // return $productDetail;
                        foreach ($productDetail as $productDetails) {
                            $productDetails->setAttribute('type', 'videocopy');
                            array_push($videoCopy, $productDetails);
                        }

                        return $videoCopy;
                    }
                }
            }
        } else if ($type === 'Insight-Subscription-Market-Research') {

            foreach ($orders as $order) {

                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetail::where('id', $getId)->first();
                        return $productDetail;
                    }
                }
            }
        } else {
            foreach ($orders as $order) {

                foreach ($order['orderDetail'] as $ord) {
                    if ($ord->id == $getId) {
                        $productDetail = OrderDetailVideo::where('order_id', $getId)->get();
                        // array_push($arr, $ord );
                        return $productDetail;
                    }
                }
            }
        }




        return $arr;
    }



    public function getOrderForVendor($id)
    {

        $vid =  auth('vendor')->user()->id;
        return OrderDetail::where('vendorId', $vid)->get();
    }

    public function getOrderDetailForVendor($id)
    {
        $vendorOrder = [];
        $orderDetails =  OrderDetail::where('vendorId', $id)->get();
        foreach ($orderDetails as $orderDetail) {
            $products = Product::where('id', $orderDetail->productId)->get();
            foreach ($products as $product) {
                array_push($vendorOrder, $product);
            }
        }
        return $vendorOrder;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderDetail $orderDetail)
    {
        //
    }
}
