<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Resources\Product as ProductResource;
use App\Industry;
use Illuminate\Http\Request;

class IndustryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Industry::paginate(20);
    }
    public function getProductByIndustry($id)
    {
        //
        $product = Product::where('industry_id','=', $id)->get();

        return $product;

    }

    public function getAllIndustry(){
        return Industry::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required'
        ]);

        $industry = new Industry();
        $industry->name = $request->input('name');
        $industry->description = $request->input('description');
        $industry->adminCategory = $request->input('adminCategory');
        $industry->save();
        return $industry;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function show(Industry $industry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function edit($industry)
    {
        //
        return Industry::where('id', $industry)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Industry $industry)
    {
        //
        $this->validate($request, [
            'name' => 'required'
        ]);

        $industry = Industry::where('id', $request->id)->first();
        $industry->name = $request->input('name');
        $industry->description = $request->input('description');
        $industry->adminCategory = $request->input('adminCategory');
        $industry->save();
        return $industry;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Industry  $industry
     * @return \Illuminate\Http\Response
     */
    public function destroy($industry)
    {
        //
        $industry = Industry::where('id', $industry)->first();
        $industry->delete();

        return $industry;

    }

    public function destroyMany(){
        $body =  json_decode(request()->getContent(), true);

        for($i = 0; $i < count($body); $i++){
            $industry = Industry::where('id',$body[$i])->first();
            $industry->delete();
        }
        return response()->json(['message', 'All item deleted']);


    }
}
