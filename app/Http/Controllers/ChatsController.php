<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use App\GroupChat;
use App\VendorUser;
use App\MessagePrivate;
use App\Events\MessageSent;
use App\Events\OnlineUsers;
use Illuminate\Http\Request;
use App\UserSubscriptionPlan;
use Engagespot\EngagespotPush;
use App\Events\GroupMessageSent;
use Illuminate\Support\Facades\DB;
use App\Events\PrivateVendorMessages;

class ChatsController extends Controller
{
   

// public function __construct()
    // {
    //   $this->middleware('auth');
    // }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        $user_id =  auth('api')->user()->user_id;
 
        return Message::with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */

 

    public function sendMessage(Request $request)
    {
        $user =  auth('api')->user();

        $message = $user->messages()->create([
      'message' => $request->input('message')
    ]);
    
        broadcast(new MessageSent($user, $message))->toOthers();
        
  
        return ['status' => 'Message Sent!'];
    }
    public function newMessage()
    {
        $users = UserSubscriptionPlan::where('verify', 1)->where('level', 3)->get();
        $new_arr=[];
        foreach ($users as $user) {
            array_push($new_arr, $user->user_id);
        }
 
        EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
        $data = [
                      "campaignName" =>  "New Message",
                      "title" => 'New Message  ',
                      "message" =>  "New Message",
                      "link" => "https://bizguruh.com/chat",
                      "icon" => "https://bizguruh.com/images/logo.png"
                  ];

        EngagespotPush::setMessage($data);
        EngagespotPush::addIdentifiers($new_arr);

        EngagespotPush::send();
    }




    public function fetchVendorMessages()
    {
        $user = auth('vendor')->user();
      
        return MessagePrivate::with('user')->get();
    }


    public function sendVendorMessage(Request $request)
    {
        $vendor =  auth('vendor')->user();
      
        $message = $vendor->message()->create([
            'message' => $request->input('message'),
            'status'=> 'sent',
            'user_id'=> $request->input('user'),
            'sender_id'=> $vendor->id,
            'receiver_id'=> $request->input('user')
          ]);
        broadcast(new PrivateVendorMessages($vendor, $message))->toOthers();
        return ['status' => 'Message Sent!'];
    }




    public function fetchUserMessages($id)
    {
        $user = auth('api')->user();
        $vendor = VendorUser::find($id);
      
        return MessagePrivate::where('user_id', $user->id)->where('vendor_user_id', $id)->get();
    }
    public function sendUserMessage(Request $request)
    {
        $user =  auth('api')->user();
        $vendor = VendorUser::find($request->input('vendor'));
       
        $message = new MessagePrivate;
        $message->vendor_user_id= $request->input('vendor');
        $message->message = $request->input('message');
        $message->status = 'sent';
        $message->user_id = $user->id;
        $message->sender_id = $user->id;
        $message->receiver_id = $request->input('vendor');
        $message->save();


        broadcast(new PrivateVendorMessages($vendor, $message))->toOthers();
        return ['status' => 'Message Sent!'];
    }

    public function fetchGroupMessages($id)
    {
        $user = auth('api')->user();
        return GroupChat::where('course_id', $id)->with('user')->get();
    }
    public function sendGroupMessage(Request $request)
    {
        $user =  auth('api')->user();
        $message = $user->groupMessages()->create([
            'message' => $request->input('message'),
            'course_id'=> $request->input('course_id'),
          
          ]);
        broadcast(new GroupMessageSent($user, $message))->toOthers();
        return ['status' => 'Message Sent!'];
    }

    public function fetchVendorGroupMessages()
    {
        return GroupChat::with('user')->get();
    }
    public function sendVendorGroupMessage(Request $request)
    {
        $user =  auth('vendor')->user();
        $user_id = User::where('vendor_user_id',$user->id)->value('id');
        $vendor = User::find($user_id);
        $message = new GroupChat;
      
        $message->message = $request->input('message');
        $message->course_id = $request->input('course_id');
        $message->user_id = $user_id;
        $message->save();
          
        broadcast(new GroupMessageSent($vendor, $message))->toOthers();
        return ['status' => 'Message Sent!'];
    }
}
