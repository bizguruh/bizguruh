<?php

namespace App\Http\Controllers;

use App\CourseWhatYouWillLearn;
use Illuminate\Http\Request;

class CourseWhatYouWillLearnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CourseWhatYouWillLearn  $courseWhatYouWillLearn
     * @return \Illuminate\Http\Response
     */
    public function show(CourseWhatYouWillLearn $courseWhatYouWillLearn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseWhatYouWillLearn  $courseWhatYouWillLearn
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseWhatYouWillLearn $courseWhatYouWillLearn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CourseWhatYouWillLearn  $courseWhatYouWillLearn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseWhatYouWillLearn $courseWhatYouWillLearn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CourseWhatYouWillLearn  $courseWhatYouWillLearn
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseWhatYouWillLearn $courseWhatYouWillLearn)
    {
        //
    }
}
