<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use App\Invoice;
use App\Customer;
use App\Expenses;
use App\Payments;
use App\Inventory;
use App\Statement;
use App\Accounting;
use App\CreditNote;
use App\SalesOrder;
use App\AccountUser;
use App\AccountDetails;
use App\ExpenseOperating;
use App\Mail\AccountInvite;
use App\VendorSubscription;
use App\AccountSubscription;
use Illuminate\Http\Request;
use App\ManufacturerProducts;
use Illuminate\Mail\Markdown;
use App\ManufacturerMaterials;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\ManufacturerProductMaterials;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function randomNumber($length)
    {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        return $result;
    }

    public $user_id;
    public function getUserId()
    {
        $user_id =  auth('account_api')->user()->user_id;
        $this->user_id = AccountDetails::where('user_id', $user_id)->first();
        return $this->user_id;
    }

    
    public function index(Request $request)
    {


        // $account = new Accounting;

        // return $account->refreshToken();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    //  create items
    public function createItem(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $item = new Inventory;
        $item->user_id = $user_id;
        $item->product_name = $request->product_name;
        $item->product_desc = $request->product_desc;
        $item->product_no = $this->randomNumber(3);
        $item->product_value = ($request->product_value);
        $item->unit_cost = $request->unit_cost;
        $item->package_cost = $request->package_cost;
        $item->in_stock = $request->in_stock;
        $item->selling_price = $request->selling_price;
        $item->save();

        return response()->json(
            [
                'id' => $item->id,
                'rate' => $item->unit_cost
            ]
        );
    }

    public function listItems()
    {
        $user_id =  auth('account_api')->user()->user_id;
        return Inventory::where('user_id', $user_id)->get();
    }

    public function getItem($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        return Inventory::where('id', $id)->where('user_id', $user_id)->first();
    }

    public function updateItem(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $item = Inventory::where('id', $request->id)->where('user_id', $user_id)->first();
      
        $item->product_name = $request->product_name;
        $item->product_desc = $request->product_desc;
        $item->product_value = ($request->product_value);
        $item->unit_cost = $request->unit_cost;
        $item->package_cost = $request->package_cost;
        $item->in_stock = $request->in_stock;
        $item->selling_price = $request->selling_price;
        $item->save();
    }


    public function deleteItem($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $item = Inventory::find($id);
        $item->delete();
        return response()->json(
            [
                'message' => 'Item deleted'

            ]
        );
    }
    //  invoice starts


    public function createInvoice(Request $request)
    {
        // $account = new Accounting;
        // return $account->createInvoice($this->getUserId()->contact_id,  $this->getUserId()->contact_person_id);

        $user_id =  auth('account_api')->user()->user_id;

        Invoice::create([
            'user_id' => $user_id,
            'invoice_no' => 'inv' . $this->randomNumber(6),
            'order_no' => $request->order_no,
            'quantity' => $request->quantity,
            'customer_name' => $request->customer_name,
            'status' => $request->status,
            'due_date' => 7,
            'amount' => $request->amount,
            'balance_due' => $request->balance_due,
            'item_id' => 0,
            'invoiceArray'=>json_encode($request->invoiceArray)
        ]);
    }
    public function listInvoices()
    {
        // $account = new Accounting;
        $user_id =  auth('account_api')->user()->user_id;
        // $name = User::where('id', $user_id)->value('name');
        // return $account->listInvoices($name);
        return Invoice::where('user_id', $user_id)->get();
    }
    public function showInvoice($invoice_id)
    {
        $user_id =  auth('account_api')->user()->user_id;

        $invoice = Invoice::where('user_id', $user_id)->where('id', $invoice_id)->first();
        $order = SalesOrder::where('user_id', $user_id)->where('order_no', $invoice->order_no)->first();
       
        $item = Inventory::where('user_id', $user_id)->where('id', $order->item_id)->first();
        $invoice->items = json_decode($order->sales_array);
        return  $invoice;
    }
    public function getPaymentInvoice($invoice_no)
    {
        $user_id =  auth('account_api')->user()->user_id;

        $invoice = Invoice::where('user_id', $user_id)->where('invoice_no', $invoice_no)->first();
        // $item = Item::where('user_id', $user_id)->where('id',  $invoice->item_id)->first();
        // $invoice->items =  $item;
        return  $invoice;
    }

    public function getInvoiceSales($order_id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $sales = SalesOrder::where('user_id', $user_id)->where('order_no', $order_id)->first();
        if (!is_null($sales)) {
            $item = Inventory::where('user_id', $user_id)->where('id', $sales->item_id)->first();
            $sales->items =  $item;
            return $sales;
        } else {
            return response()->json(['message' => 'not found']);
        }
    }
    public function deleteInvoice($invoice_id)
    {

        // $account = new Accounting;
        // return $account->deleteInvoice($invoice_id);
        $user_id =  auth('account_api')->user()->user_id;
        $invoice = Invoice::find($invoice_id);
        $invoice->delete();
        return response()->json(['message' => 'Invoice was deleted']);
    }

    // Payments starts


    public function createPayment(Request $request)
    {
        // $account = new Accounting;
        //return $account->createPayment($customer_id, $amount, $payment_mode, $invoice_id);
        $user_id =  auth('account_api')->user()->user_id;
        $pay =  Payments::where('user_id', $user_id)->where('invoice_no', $request->invoice_no)->first();


        if (is_null($pay)) {
            Payments::create([
            'user_id' => $user_id,
            'payment_no' => 'pmt' . $this->randomNumber(6),
            'type' => $request->type,
            'order_no' => $request->order_no,
            'customer_name' => $request->customer_name,
            'invoice_no' => $request->invoice_no,
            'mode' => $request->mode,
            'amount' => $request->amount
        ]);
        } else {
            $pay->customer_name = $request->customer_name;
            $pay->mode = $request->mode;
            $pay->amount = $request->amount;
            $pay->type = $request->type;
            $pay->save();
        }

       
        $invoice = Invoice::where('user_id', $user_id)->where('invoice_no', $request->invoice_no)->first();
        $order = SalesOrder::where('user_id', $user_id)->where('order_no', $request->order_no)->first();

        $invoice->status = 'paid';
        $invoice->balance_due = 0;
        $invoice->save();
        $order->status ='paid';
        $order->save();
      
        return response()->json(['message' => 'payment was created']);
    }
    public function listPayments()
    {
        $user_id =  auth('account_api')->user()->user_id;

        return Payments::where('user_id', $user_id)->get();
    }
    public function showPayment($payment_id)
    {
        $user_id =  auth('account_api')->user()->user_id;

        $payment = Payments::where('user_id', $user_id)->where('id', $payment_id)->first();

        $invoice = Invoice::where('user_id', $user_id)->where('invoice_no', $payment->invoice_no)->first();
        $order = SalesOrder::where('user_id', $user_id)->where('order_no', $invoice->order_no)->first();
        $item = Inventory::where('user_id', $user_id)->where('id', $order->item_id)->first();
        $payment->items =  $item;
        $payment->invoices = $invoice;
        return  $payment;
    }
    public function deletePayment($payment_id)
    {
        $payment = Payments::find($payment_id);
        $payment->delete();
        return response()->json(['message' => 'payment was deleted']);
    }


    //    Sales order starts

    public function createSalesOrder(Request $request)
    {
 
           
        // $account = new Accounting;
        // return $account->createSalesOrder($this->getUserId()->contact_id,  $this->getUserId()->contact_person_id,$request->name,$request->rate,$request->description);
        $user_id =  auth('account_api')->user()->user_id;
        $orderNo = SalesOrder::where('user_id', $user_id)->where('order_no', $request->salesOrdNo)->first();
        $invoiceNo = Invoice::where('user_id', $user_id)->where('order_no', $request->salesOrdNo)->first();
        if (is_null($orderNo)) {
            SalesOrder::create([
            'user_id' => $user_id,
            'order_no' => 'ord' . $this->randomNumber(6),
            'customer_name' => $request->customer_name,
            'status' => $request->status,
            'quantity' => $request->quantity,
            'rate' =>  $request->rate,
            'amount' => $request->amount,
            'item_id' => 0,
            'sales_array' => json_encode($request->salesArray),
        ]);
            return response()->json(['message' => 'sales order was created']);
        } else {
            if (!is_null($invoiceNo) && $orderNo->status == 'paid') {
                $invoiceNo->amount = $request->amount;
                $invoiceNo->balance_due = 0;
                $invoiceNo->due_date = 0;
                $invoiceNo->quantity = $request->quantity;
                $invoiceNo->save();
            }

            $orderNo->customer_name = $request->customer_name;
            $orderNo->status = $request->status;
            $orderNo->quantity = $request->quantity;
            $orderNo->rate = $request->rate;
            $orderNo->amount = $request->amount;
            $orderNo->item_id = 0;
            $orderNo->sales_array = json_encode($request->salesArray);
            $orderNo->save();
            return response()->json(['message' => 'sales order was updated']);
        }
    }
    public function listSalesOrders()
    {
        // $account = new Accounting;
        $user_id =  auth('account_api')->user()->user_id;
        //     $name = User::where('id', $user_id)->value('name');
        //     return $account->listSalesOrder($name);
        return SalesOrder::where('user_id', $user_id)->get();
    }
    public function showSalesOrder($order_id)
    {
        // $account = new Accounting;
        // return $account->retrieveSalesOrder($order_id);
        $user_id =  auth('account_api')->user()->user_id;
        $sales = SalesOrder::where('user_id', $user_id)->where('id', $order_id)->first();
        // dd($sales);
        // $item = Inventory::where('user_id', $user_id)->where('id', $sales->item_id)->first();
        // $sales->items =  $item;
        return $sales;
    }
    public function deleteSalesOrder($order_id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $sales = SalesOrder::find($order_id);
        $sales->delete();
        return response()->json(['message' => 'sales order was deleted']);
    }


    // statement starts
    //  return $account->sendStatement('2059437000000082001');
    public function addStatement(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        Statement::create([
            'user_id' => $user_id,
            'revenue' => $request->revenue,
            'cogs'  => $request->cogs,
            'gross_profit' => $request->gross_profit,
            'opex'  => $request->opex,
            'pbt'  => $request->pbt,
            'net_profit'  => $request->net_profit,
            'month'  => $request->month,
        ]);
        return response()->json(
            [
                'message' => 'Statement added'

            ]
        );
    }

    public function listStatements()
    {
        $user_id =  auth('account_api')->user()->user_id;
        $statements = Statement::where('user_id', $user_id)->get();
        return $statements;
    }
    public function getStatement($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $statement = Statement::where('user_id', $user_id)->where('id', $id)->first();
        return $statement;
    }



    public function createCustomer(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $customer = new Customer;
        $customer->user_id = $user_id;
        $customer->customer_name = $request->customer_name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->balance_due = 0;
        $customer->save();

        return response()->json(
            [
                'customer_name' => $customer->customer_name,

            ]
        );
    }
    public function listCustomers()
    {
        $user_id =  auth('account_api')->user()->user_id;
        return  Customer::where('user_id', $user_id)->get();
    }

    public function getCustomer($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        return Customer::where('user_id', $user_id)->where('id', $id)->first();
    }
    public function updateCustomer(Request $request, $id)
    {
        $customer =  Customer::where('id', $id)->first();
        $customer->customer_name = $request->customer_name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->save();
    }
    public function deleteCustomer($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $cust = Customer::find($id);
        $cust->delete();

        return response()->json(
            [
                'message' => 'Customer deleted'

            ]
        );
    }

    public function createCredit(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;

        CreditNote::create([
                    'user_id' => $user_id,
                    'credit_no' => 'crd' . $this->randomNumber(6),
                    'customer_name' => $request->customer_name,
                    'status' => $request->status,
                    'quantity' => $request->quantity,
                    'rate' => $request->rate,
                    'item_id' => $request->item_id,
                    'customer_note' => $request->customer_note,
                    'terms_conditions' => $request->terms_conditions,
                    'balance_due' => $request->balance_due,
                ]);
        return response()->json(['message' => 'credit note was created']);
    }
    public function listCredits()
    {
        $user_id =  auth('account_api')->user()->user_id;
        return CreditNote::where('user_id', $user_id)->get();
    }
    public function getCredit($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $credit = CreditNote::where('user_id', $user_id)->where('id', $id)->first();
        dd($credit);
        $item=  Inventory::where('user_id', $user_id)->where('id', $credit->item_id)->first();
        $credit->item = $item;
        return $credit;
    }
    public function getManufacturerCredit($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $credit = CreditNote::where('user_id', $user_id)->where('id', $id)->first();
        $item=  ManufacturerProducts::where('user_id', $user_id)->where('id', $credit->item_id)->first();
        $credit->item = $item;
        return $credit;
    }
    public function deleteCredit($id)
    {
        CreditNote::find($id)->delete();
        return response()->json(['message' => 'credit note was deleted']);
    }
    public function addTotalExpenses(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $expense = ExpenseOperating::where('user_id', $user_id)->where('month', $request->month)->first();
        if (is_null($expense)) {
            ExpenseOperating::create([
                'user_id'=>$user_id,
                'rent' => $request->rent,
                'salaries' => $request->salaries,
                'light' => $request->light,
                'bills' => $request->bills,
                'data' => $request->data,
                'repairs' => $request->repairs,
                'marketing' => $request->marketing,
                'logistics' => $request->logistics,
                'office' => $request->office,
                'levies' => $request->levies,
                'others' => $request->others,
                'month'=>$request->month
            ]);
        } else {
            $expense->rent = intval($request->rent) + intval($expense->rent);
            $expense->salaries = intval($request->salaries) + intval($expense->salaries);
            $expense->light = intval($request->light) + intval($expense->light);
            $expense->data = intval($request->data) + intval($expense->data);
            $expense->bills = intval($request->bills) + intval($expense->bills);
            $expense->repairs = intval($request->repairs) + intval($expense->repairs);
            $expense->marketing = intval($request->marketing) + intval($expense->marketing);
            $expense->logistics = intval($request->logistics) + intval($expense->logistics);
            $expense->office = intval($request->office) + intval($expense->office);
            $expense->levies = intval($request->levies) + intval($expense->levies);
            $expense->others = intval($request->others) + intval($expense->others);

            $expense->save();
        }
    }

    public function getTotExp($month)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $expense = ExpenseOperating::where('user_id', $user_id)->where('month', $month)->first();
        return $expense;
    }

    public function addManufacturerMaterial(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        ManufacturerMaterials::create([
            'user_id'=> $user_id,
            'material_no' => $this->randomNumber(3),
            'material_name' => $request->material_name,
            'quantity_per_unit' => $request->quantity,
            'in_stock'=> $request->in_stock,
            'cost_price'=> $request->cost_price

        ]);
    }
    public function listManufacturerMaterials()
    {
        $user_id =  auth('account_api')->user()->user_id;
        return ManufacturerMaterials::where('user_id', $user_id)->get();
    }
    public function listManufacturerProducts()
    {
        $user_id =  auth('account_api')->user()->user_id;
        return ManufacturerProducts::where('user_id', $user_id)->get();
    }
    public function getManufacturerProducts($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        return ManufacturerProducts::where('user_id', $user_id)->where('product_name', $id)->first();
    }
    public function getManufacturerProduct($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        return ManufacturerProducts::where('user_id', $user_id)->where('id', $id)->first();
    }
    public function addManufacturerProduct(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        ManufacturerProducts::create([
            'user_id'=> $user_id,
            'product_no' => $this->randomNumber(3),
            'product_name' => strtolower($request->product_name),
            'quantity_per_batch' => $request->quantity_per_batch,
            'in_stock'=> $request->in_stock,
            'selling_price'=> $request->selling_price,
            'product_desc'=> $request->product_desc
        ]);
    }
    public function addProductMaterial(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $find= ManufacturerProductMaterials::where('user_id', $user_id)->first();
        $arr = $request->cop;
      
        if (is_null($find)) {
            ManufacturerProductMaterials::create([
            'user_id'=> $user_id,
            'cost_of_production'=>json_encode($request->cop),
          
        ]);
        } else {
            $oldValue = json_decode($find->cost_of_production);
            array_push($oldValue, $arr[0]);
            $find->cost_of_production = json_encode($oldValue);
            $find->save();
        }
    }
    public function listProductMaterial(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
      
        $result = ManufacturerProductMaterials::where('user_id', $user_id)->first();
        if (is_null($result)) {
            return response()->json(
                [
                'message' => 'empty'

            ]
            );
        } else {
            return $result;
        }
    }
    public function getManufacturerMaterial($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
      
        return ManufacturerMaterials::where('user_id', $user_id)->where('id', $id)->first();
    }
    public function deleteManufacturerProduct($id)
    {
        $find =  ManufacturerProducts::find($id);
        $find->delete();
        return response()->json(
            [
                  'message' => 'Item deleted'
  
              ]
        );
    }
    public function deleteManufacturerMaterial($id)
    {
        $find =  ManufacturerMaterials::find($id);
        $find->delete();
        return response()->json(
            [
                'message' => 'Item deleted'

            ]
        );
    }

    public function updateProduct($id, Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
     
        $item = ManufacturerProducts::where('user_id', $user_id)->where('id', $id)->first();

        $item->product_name = strtolower($request->product_name);
        $item->quantity_per_batch =$request->quantity_per_batch;
        $item->in_stock= $request->in_stock;
        $item->selling_price= $request->selling_price;
        $item->product_desc = $request->product_desc;
        $item->save();
    }
    public function updateMaterial($id, Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $item = ManufacturerMaterials::where('user_id', $user_id)->where('id', $id)->first();

        $item->material_name = strtolower($request->material_name);
        $item->quantity_per_unit = $request->quantity;
        $item->in_stock= $request->in_stock;
        $item->cost_price= $request->cost_price;
        $item->save();
    }

    public function addUser(Request $request)
    {
        $user_id =  auth('account_api')->user()->user_id;
        AccountUser::create([
                'user_id' => $user_id,
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role,
                'verify' => 0
               
            ]);
        $user = AccountUser::where('email', $request->email)->first();
        $user->password = $request->password;
        return new AccountInvite($user);
        Mail::to($request->email)->send(new AccountInvite($user));
        return response()->json([
                'message'=> 'User Created'
            ]);
    }
    public function updateUser(Request $request, $id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $user= AccountUser::where('user_id', $user_id)->where('id', $id)->first();
        if (!is_null($user)) {
            $user->name = $request->name;
            $user->email = strtolower($request->email);
            $user->password = Hash::make($request->password);
            $user->role = $request->role;
            $user->save();
            return response()->json([
                'message'=> 'User Updated'
            ]);
        }
    }
    public function verifyUser(Request $request, $id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $user= AccountUser::where('user_id', $user_id)->where('id', $id)->first();
        if (!is_null($user)) {
            $user->verify = $request->verify;
        
            $user->save();
            return response()->json([
                'message'=> 'User verified'
            ]);
        }
    }
    public function deleteUser($id)
    {
        $user = AccountUser::find($id);
        $user->delete();
        return response()->json([
            'message'=> 'User Deleted'
        ]);
    }
    public function getUsers()
    {
        $user_id =  auth('account_api')->user()->user_id;
        $user= AccountUser::where('user_id', $user_id)->get();
        return $user;
    }
    public function getUser($id)
    {
        $user_id =  auth('account_api')->user()->user_id;
        $user= AccountUser::where('user_id', $user_id)->where('id', $id)->first();
        
        return $user;
    }
    public function accountLogin()
    {
        $user_id =  auth('account_api')->user()->user_id;
        $id =  auth('account_api')->user()->id;
        $business_name = User::where('id', $user_id)->value('business_name');
        $business_type= User::where('id', $user_id)->value('business_type');
        $logo= User::where('id', $user_id)->value('logo');
        
        $users= AccountUser::where('id', $id)->first();
        $users->business_name = $business_name;
        $users->business_type = $business_type;
        $users->logo = $logo;
        return $users;
    }
    public function accountSignup(Request $request)
    {
      
        
        DB::transaction(function () use ($request) {
            if (is_null($request['phone_no'])) {
                $phone = '1234';
            } else {
                $phone = $request['phone_no'];
            }
            $user =  User::create([
                'name' => $request['name'],
                'lastName' => $request['name'],
                'email' => $request['email'],
               
                'phoneNo' => $phone,
                'password' => Hash::make($request['password']),
                'count' => 1,
                'specialToken' => $request['specialToken'],
                'vendor_user_id'=> 0,
                'business_name'=>$request->business_name,
                'business_type'=>$request->business_type,
                'address' => 'null',
                'logo' => $request->logo
            ]);

              $account = AccountUser::create([
                        'user_id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'password' => Hash::make($request->password),
                        'role' => 'admin',
                        'verify' => 0
               
            ]);
            return  $account ;
        });
       
    }

    public function addSub(){
       
        
      return  VendorSubscription::create([
                'title'=>'community leader',
                'price' => 100000,
                'level'=> 3,
                'info'=> 'community leader',
                'duration'=>'yearly',
        ]);
    }
}
