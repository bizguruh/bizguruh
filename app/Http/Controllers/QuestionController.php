<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
     return Question::all();
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request)
    {
       

    }
    public function verifiedQuestions( Request $request)
    {
        return Question::where('verify' , 1)->get();

    }
    public function verify( $id)
    {
        
        $question = Question::where('id' , $id)->first();
        $question->verify = 1;
        $question->save();
        return 'verified';

    }
    public function unverify( $id)
    {
        $question =  Question::where('id' , $id)->first();
        $question->verify = 0;
        $question->save();
       return 'unverified';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        Question::create([
            'title'=> $request->title,
            'name'=> $request->name,
            'catalog'=> $request->catalog,
            'premise'=> $request->premise,
            'questions' => json_encode($request->models) ,
            'verify' => 0

        ]);

        return 'saved';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Question::where('id' , $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Question::where('id' , $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            
   
        $question =  Question::where('id' , $id)->first();
        $question->title = $request->title ;
        $question->name = $request->name  ;
        $question -> $request->catalog;
        $question->verify = 0;
         $question->questions = json_encode($request->models)  ;
         $question->save();
         return 'updated';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question =  Question::find($id);
   
        $question->delete();
        return 'deleted';
    }
}
