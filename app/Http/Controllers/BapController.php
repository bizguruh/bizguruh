<?php

namespace App\Http\Controllers;

use App\Bap;
use App\BapDraft;
use Carbon\Carbon;
use App\BapTemplate;
use App\FormTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BapController extends Controller
{
    public function start()
    {
        $now = Carbon::now();
        $user =  auth('api')->user();
      
        $user->baps()->create([
           'current_month'=> 1,
           'current_week' => 1,
           'folder_status' => 'pending',
           'survey_taken' => 'no',
           'survey_scores' => 0,
           'survey_history' => '',
           'status' => 'active'
        ]);
        return ['status' => 'created!'];
    }


    public function getActiveRecord()
    {
        $user =  auth('api')->user();
        return Bap::with('user')->where('status','active')->where('user_id',$user->id)->first();
    }

    public function getAllRecord()
    {
        return Bap::with('user')->get();
    }

    public function checkProgress()
    {
    
        $records = Bap::with('user')->where('status', 'active')->get();
        $now = Carbon::now();

        foreach ($records as $record) {
           
                $check = Carbon::parse($record->created_at)->addDays(30);
           
                $diff = $now->greaterThan($check);
            
                if ($diff) {
                    Bap::create([
                        'user_id' => $record->user_id,
                         'current_month'=> $record->current_month + 1,
                         'current_week' => 1,
                         'folder_status' => 'pending',
                         'survey_taken' => 'no',
                         'survey_scores' => 0,
                         'survey_history' => '',
                         'status' => 'active'
                      ]);
             
                    $record->status = 'finished';
                    $record->save();
              
                  
                }
           
        }
        return ['status' => 'created!'];
    }

    public function sendFolder($id)
    {
        $record = Bap::with('user')->where('folder_status', 'pending')->where('status', 'active')->where('user_id', $id)->first();
        if (!is_null($record)) {
               echo 'sent';
        }
    }
    public function checkFolderSend()
    {
        $records = Bap::with('user')->where('folder_status', 'pending')->where('status', 'active')->get();

        foreach($records as $record){
            if (!is_null($record)) {
                $this->sendFolder($record->user_id);
                $record->folder_status = 'sent';
                $record->save();
        }
        }
     
    }

    public function checkWeeklyProgress()
    {
    
        $records = Bap::where('status', 'active')->get();
        $now = Carbon::now();

        foreach ($records as $record) {
           
                $check = Carbon::parse($record->updated_at)->addDays(7);
           
                $diff = $now->greaterThan($check);
                
                if ($diff) {
                   
                    $record->current_week = intval($record->current_week) + 1;
                    $record->save();
              
                  
                }
           
        }
        return ['status' => 'created!'];
    }

    public function saveResponses(Request $request){
      
        $user =  auth('api')->user();
    
        $user->bapTemplates()->create([
             'responses'=>  \json_encode($request->responses),
             'month'=> $request->month,
             'week' =>$request->week,
             'template' => $request->template
        ]);

        return ['status'=> 'saved'];
    }

    public function saveToDraft(Request $request){
      
        $user =  auth('api')->user();
        $temp = BapDraft::where('user_id',$user->id)->where('template',$request->template)->first();
        if(is_null($temp)){
            $user->bapDrafts()->create([
                'responses'=>  \json_encode($request->responses),
                'month'=> $request->month,
                'week' =>$request->week,
                'template' => strtolower($request->template)
           ]);
   
           return ['status'=> 'saved'];
        }else{
           $temp->responses = \json_encode($request->responses);
           $temp->save();
        }
    
       
    }
    public function getDraft(Request $request){
        $user =  auth('api')->user();
        return BapDraft::with('user')->where('user_id',$user->id)->where('template',strtolower($request->template))->first();
    }
    public function getTemplates(){
        $user =  auth('api')->user();
        return BapTemplate::with('user')->where('user_id',$user->id)->get();
    }
    public function getUserTemplates(){
        $user =  auth('api')->user();
        return BapTemplate::with('user')->where('user_id',$user->id)->get();
    }
    public function getTemplate($name){
        $user =  auth('api')->user();
        $template = BapTemplate::with('user')->where('template', $name)->where('user_id',$user->id)->first();
        if(is_null($template)){
            return ['status'=> 'absent'];  
        }else{
            return ['status'=> 'active']; 
        }
    }
    public function getTemplateById($id){
        return BapTemplate::with('user')->where('id', $id)->first();
    }


    public function getForms($name){

     
        return FormTemplate::where('group',$name)->get();
    }
    
}
