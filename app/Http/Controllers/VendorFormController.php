<?php

namespace App\Http\Controllers;

use App\CourseForm;
use App\VendorForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendorFormController extends Controller
{
    public function save(Request $request)
    {
        DB::transaction(function () use ($request) {
            $vendor = auth('vendor')->user()->id;

            $form =  VendorForm::create([
                'vendor_id'=>$vendor,
                'group'=> $request->group,
                 'form_title'=> $request->title,
                 'template'=> json_encode($request->template)
              ]);
            if (!is_null($request->product_id)) {
                $vendorCourse = CourseForm::create([
                'vendor_id'=> $vendor,
                'form_id'=> $form->id,
                'product_id'=>$request->product_id
            ]);
            }
        });
    }


    public function update(Request $request)
    {
        $vendor = auth('vendor')->user()->id;
        $form = VendorForm::where('id', $request->id)->first();
        $form->vendor_id=$vendor;
        $form->group= $request->group;
        $form->form_title= $request->title;
        $form->template = json_encode($request->template);
        $form->save();
        return 'Updated';
    }
    public function deleteForm($id)
    {
        $form = VendorForm::find($id);
        $form->delete();
        return 'Deleted';
    }
    public function deleteAllForm(Request $request)
    {
      
        foreach ($request->ids as  $value) {
            $form = VendorForm::find($value);
            $form->delete();
        }
        return response()->json([
          'status'=>'deleted'
      ]);
  
    }
    public function getAllTemplates()
    {
        $vendor = auth('vendor')->user()->id;
        return VendorForm::where('vendor_id', $vendor)->get();
    }

    public function getSingleTemplate($id)
    {
        $vendor = auth('vendor')->user()->id;
        return VendorForm::where('id', $id)->where('vendor_id', $vendor)->first();
    }
    public function getCourseForm($id)
    {
        $courses = CourseForm::where('product_id', $id)->get();
        $forms = [];
        foreach ($courses as $course) {
            $form =  VendorForm::where('id', $course->form_id)->where('vendor_id', $course->vendor_id)->first();
            \array_push($forms, $form);
        }
        return $forms;
    }
    public function getForm($id){
      return  VendorForm::where('id', $id)->first();
    }
}
