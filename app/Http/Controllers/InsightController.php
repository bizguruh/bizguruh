<?php

namespace App\Http\Controllers;

use App\Insight;
use Illuminate\Http\Request;

class InsightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Insight::paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
           'name' => 'required'
        ]);

        $insight = new Insight();
        $insight->name = $request->input('name');
        $insight->description = $request->input('description');
        $insight->adminCategory = $request->input('adminCategory');
        $insight->save();
        return $insight;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Insight  $insight
     * @return \Illuminate\Http\Response
     */
    public function show(Insight $insight)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Insight  $insight
     * @return \Illuminate\Http\Response
     */
    public function edit($insight)
    {
        //
        return Insight::where('id', $insight)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Insight  $insight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Insight $insight)
    {
        //

        $this->validate($request, [
            'name' => 'required'
        ]);

        $insight = Insight::where('id', $request->id)->first();
        $insight->name = $request->input('name');
        $insight->description = $request->input('description');
        $insight->adminCategory = $request->input('adminCategory');
        $insight->save();
        return $insight;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Insight  $insight
     * @return \Illuminate\Http\Response
     */
    public function destroy($insight)
    {
        //
        $insight = Insight::where('id', $insight)->first();
        $insight->delete();

        return $insight;
    }

    public function destroyMany(){
        $body =  json_decode(request()->getContent(), true);

        for($i= 0; $i < count($body); $i++){
            $insight = Insight::where('id', $body[$i])->first();
            $insight->delete();
        }

        return response()->json(['message', 'All item deleted']);

    }
}

