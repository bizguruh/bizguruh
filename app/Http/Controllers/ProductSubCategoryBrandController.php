<?php

namespace App\Http\Controllers;

use App\ProductSubCategory;
use App\ProductSubCategoryBrand;
use Illuminate\Http\Request;
use App\Http\Resources\ProductSubCategory as ProductSubCategoryResource;


class ProductSubCategoryBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function topic(){
        return ProductSubCategoryBrand::all();
    }

    public function bizguruhTopic(){
        $productTopic =  ProductSubCategoryBrand::all();
        return $productTopic;
    }


    public function filterSubCategoryTopic(){
        return ProductSubCategoryBrand::all();
    }

    public function getSubCategoryBrandById($subcat){
        return ProductSubCategoryBrand::where('sub_brand_category_id', '=', $subcat)->get();
    }

    public function getSubCategoryById($id){

        $productCategory =  ProductSubCategory::where('id', '=', $id)->get();
       // return  $productCategory;
        return ProductSubCategoryResource::collection($productCategory);
    }






    public function getSubCatByBrand($id){
        $productSubCategoryBrand = ProductSubCategoryBrand::where('sub_brand_category_id', $id)->get();

        return $productSubCategoryBrand;
    }


    public function getSubTopicForVendor($id){
        $productSubCategoryBrand = ProductSubCategoryBrand::where('sub_brand_category_id', $id)->where('adminCategory', '!=', 'AO')->get();

        return $productSubCategoryBrand;
    }

    public function getSpecificCategoryBrand($id){
     return ProductSubCategoryBrand::where('id', '=', $id)->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required|string',
/*            'sub_brand_category_id' => 'required'*/

        ]);

        $subCategoryBrand = new ProductSubCategoryBrand();
        $subCategoryBrand->name = $request->input('name');
        $subCategoryBrand->description = $request->input('description');
        $subCategoryBrand->adminCategory = $request->input('adminCategory');
        $subCategoryBrand->sub_brand_category_id = $request->input('sub_brand_category_id');
        $subCategoryBrand->save();
        return $subCategoryBrand;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        return ProductSubCategoryBrand::where('id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
/*            'sub_brand_category_id' => 'required',*/
            'name' => 'required|string|unique:sub_categories_brand,id',
            'description' => 'required|string'
        ]);


        $productSubCategoryBrand = ProductSubCategoryBrand::where('id', $id)->first();
        $productSubCategoryBrand->name = $request->name;
        $productSubCategoryBrand->description = $request->description;
        $productSubCategoryBrand->adminCategory = $request->adminCategory;
        $productSubCategoryBrand->save();

        return $productSubCategoryBrand;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productSubCategory = ProductSubCategoryBrand::where('id', $id)->first();
        $productSubCategory->delete();

        return $productSubCategory;
    }

    public function destroyMany(){
        $body =  json_decode(request()->getContent(), true);

        for($i = 0; $i < count($body); $i++){
            $concept = ProductSubCategoryBrand::where('id', $body[$i])->first();
            $concept->delete();
        }

        return response()->json(['message', 'All item deleted']);

    }
}
