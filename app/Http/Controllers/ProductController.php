<?php

namespace App\Http\Controllers;

use Image;
use App\Product;
use App\VendorUser;
use App\ProductBook;
use App\EventProduct;
use App\Subscription;
use App\Http\Requests;
use App\ProductCourse;
use App\ArticleProduct;
use App\ProductChapter;
use App\ProductJournal;
use App\ProductCategory;
use App\EventScheduleTime;
use App\ProductWhitePaper;
use App\ProductSubCategory;
use App\ProductVideoDetail;
use App\ProductCourseModule;
use App\ProductMarketReport;
use Illuminate\Http\Request;
use App\ProductMarketResearch;
use Engagespot\EngagespotPush;
use App\CourseWhatYouWillLearn;
use JD\Cloudder\Facades\Cloudder;
use Symfony\Component\Console\Input\Input;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductEdit as ProductEditResource;

class ProductController extends Controller
{
    public $outer = [];
    public $filterArray = [];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function aVendor($uid)
    {
        $vendorId = Product::where('id', $uid)->value('vendor_user_id');
        $vendor = VendorUser::where('id', $vendorId)->first();
        return $vendor;
    }

    public function imageUpload(Request $request)
    {
        $file = $request->file('file');


        $path = secure_url('images/') . '/' . $file->getClientOriginalName();
        $imgpath = $file->move(public_path('images/'), $file->getClientOriginalName());
        $fileNameToStore = $path;

        return json_encode(['location' => $fileNameToStore]);
    }

    public function getHome()
    {
        return view('welcome');
    }

    public function getQueryProduct($params)
    {
        $this->displayBotContent();
        foreach ($this->outer as $prod) {
            //  return $params;
            if ($prod['categoryId'] == $params) {
                array_push($this->filterArray, $prod);
            }
        }
        return $this->filterArray;
    }

    public function searchProduct($searchTerm)
    {
        $allProduct = [];
        $products = Product::all();
        foreach ($products as $product) {
            if ($product->productBook) {
                if (stristr($product->productBook->title, $searchTerm)) {
                    $product->productBook->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->productBook->setAttribute('softCopy', $product->softCopyPrice);
                    $product->productBook->setAttribute('audioCopy', $product->audioPrice);
                    $product->productBook->setAttribute('readOnline', $product->readOnlinePrice);

                    array_push($allProduct, $product->productBook);
                }
            }

            if ($product->productCourse) {
                if (stristr($product->productCourse->title, $searchTerm)) {
                    $product->productCourse->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->productCourse->setAttribute('softCopy', $product->softCopyPrice);
                    $product->productCourse->setAttribute('audioCopy', $product->audioPrice);
                    $product->productCourse->setAttribute('readOnline', $product->readOnlinePrice);
                    array_push($allProduct, $product->productCourse);
                }
            }

            if ($product->productJournal) {
                if (stristr($product->productJournal->title, $searchTerm)) {
                    $product->productJournal->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->productJournal->setAttribute('softCopy', $product->softCopyPrice);
                    $product->productJournal->setAttribute('audioCopy', $product->audioPrice);
                    $product->productJournal->setAttribute('readOnline', $product->readOnlinePrice);
                    array_push($allProduct, $product->productJournal);
                }
            }

            if ($product->marketResearch) {
                if (stristr($product->marketResearch->title, $searchTerm)) {
                    $product->marketResearch->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->marketResearch->setAttribute('softCopy', $product->softCopyPrice);
                    $product->marketResearch->setAttribute('audioCopy', $product->audioPrice);
                    $product->marketResearch->setAttribute('readOnline', $product->readOnlinePrice);
                    array_push($allProduct, $product->marketResearch);
                }
            }

            if ($product->productChapter) {
                if (stristr($product->productChapter->title, $searchTerm)) {
                    $product->productChapter->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->productChapter->setAttribute('softCopy', $product->softCopyPrice);
                    $product->productChapter->setAttribute('audioCopy', $product->audioPrice);
                    $product->productChapter->setAttribute('readOnline', $product->readOnlinePrice);
                    array_push($allProduct, $product->productChapter);
                }
            }

            if ($product->marketReport) {
                if (stristr($product->marketReport->title, $searchTerm)) {
                    $product->marketReport->setAttribute('hardCopy', $product->hardCopyPrice);
                    $product->marketReport->setAttribute('softCopy', $product->softCopyPrice);
                    $product->marketReport->setAttribute('audioCopy', $product->audioPrice);
                    $product->marketReport->setAttribute('readOnline', $product->readOnlinePrice);
                    array_push($allProduct, $product->marketReport);
                }
            }
        }

        return $allProduct;
    }

    public function getItem($id, $searchTeam)
    {
        $productForBot = [];

        $products = Product::where('category_id', $id)->get();


        foreach ($products as $product) {
            if ($product->productCategory === 'VP' || 'BP' || 'EP') {
                switch ($product->prodType) {
                    case 'Books':
                        if (stristr($product->productBook->title, $searchTeam)) {
                            $product->productBook->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productBook->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productBook->setAttribute('audioCopy', $product->audioPrice);
                            $product->productBook->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productBook);
                        }
                        break;
                    case 'Podcast':
                        if (stristr($product->productChapter->title, $searchTeam)) {
                            $product->productChapter->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productChapter->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productChapter->setAttribute('audioCopy', $product->audioPrice);
                            $product->productChapter->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productChapter);
                        }
                        break;
                    case 'Video':
                        if (stristr($product->productChapter->title, $searchTeam)) {
                            $product->productChapter->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productChapter->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productChapter->setAttribute('audioCopy', $product->audioPrice);
                            $product->productChapter->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productChapter);
                        }
                        break;
                    case 'White Paper':
                        if (stristr($product->productWhitePaper->title, $searchTeam)) {
                            $product->productWhitePaper->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productWhitePaper->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productWhitePaper->setAttribute('audioCopy', $product->audioPrice);
                            $product->productWhitePaper->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productWhitePaper);
                        }
                        break;
                    case 'Journals':
                        if (stristr($product->productJournal->title, $searchTeam)) {
                            $product->productJournal->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productJournal->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productJournal->setAttribute('audioCopy', $product->audioPrice);
                            $product->productJournal->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productJournal);
                        }
                        break;
                    case 'Market Research':
                        if (stristr($product->marketResearch->title, $searchTeam)) {
                            $product->marketResearch->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->marketResearch->setAttribute('softCopy', $product->softCopyPrice);
                            $product->marketResearch->setAttribute('audioCopy', $product->audioPrice);
                            $product->marketResearch->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->marketResearch);
                        }
                        break;
                    case 'Courses':
                        if (stristr($product->productCourse->title, $searchTeam)) {
                            $product->productCourse->setAttribute('hardCopy', $product->hardCopyPrice);
                            $product->productCourse->setAttribute('softCopy', $product->softCopyPrice);
                            $product->productCourse->setAttribute('audioCopy', $product->audioPrice);
                            $product->productCourse->setAttribute('readOnline', $product->readOnlinePrice);
                            array_push($productForBot, $product->productCourse);
                        }
                        break;

                    default:
                        // return 'false';
                }
            }
        }

        return $productForBot;




        // return $id;
    }

    public function getSubjectMatterProduct(Request $request)
    {
        if (isset($request->categoryId) && isset($request->subjectMatterId)) {
            $this->displayBotContent();
            foreach ($this->outer as $prod) {
                //  return $params;
                if (($prod['categoryId'] == $request->categoryId) && ($prod['subjectMatterId'] == $request->subjectMatterId)) {
                    array_push($this->filterArray, $prod);
                }
            }
            return $this->filterArray;
        } elseif (isset($request->categoryId)) {
            $this->displayBotContent();
            foreach ($this->outer as $prod) {
                //  return $params;
                if ($prod['categoryId'] == $request->categoryId) {
                    array_push($this->filterArray, $prod);
                }
            }
            return $this->filterArray;
        }
    }

    public function getBotCategory()
    {
        $productCat =  ProductCategory::all();
        foreach ($productCat as $prod) {
            $prod = array(
                "id" => $prod->id,
                "category" => $prod->name
            );
            $prodSubCat = ProductSubCategory::all();
            $test = [];
            foreach ($prodSubCat as $subCat) {
                if ($prod['id'] == $subCat['sub_category_id']) {
                    $subArray = array(
                        "id" => $subCat->id,
                        "subjectMatter" => $subCat->name,
                    );
                    array_push($test, $subArray);
                    $prod['subjectMatter'] = $test;
                }
            }
            array_push($this->filterArray, $prod);
        }


        return $this->filterArray;
    }

    public function displayBotContent()
    {
        // $outer = [];
        $products = Product::whereIn('prodType', ['Market Reports', 'Courses', 'White Paper', 'Journals', 'Market Research', 'Videos', 'Books', 'Podcast', 'Market Reports'])->get();
        //return $products;
        foreach ($products as  $product) {
            $prod = array(
                "id" => $product->id,
                "subjectMatter" => ProductSubCategory::where('id', $product->sub_category_id)->first()->name,
                "productCategory" => ProductCategory::where('id', $product->category_id)->first()->name,
                "categoryId" => $product->category_id,
                "subjectMatterId" => $product->sub_category_id,
                "author" => VendorUser::where('id', $product->vendor_user_id)->first()->storeName,
            );

            if ($product->prodType === 'Podcast' || $product->prodType === 'Videos') {
                $prod['productName'] = $product->productChapter->title;
            } elseif ($product->prodType === 'Market Research') {
                $prod['productName'] = $product->marketResearch->title;
            } elseif ($product->prodType === 'Market Reports') {
                $prod['productName'] = $product->marketReport->title;
            } elseif ($product->prodType === 'Books') {
                $prod['productName'] = $product->productBook->title;
            } elseif ($product->prodType === 'White Paper') {
                $prod['productName'] = $product->productWhitePaper->title;
            } elseif ($product->prodType === 'Journals') {
                $prod['productName'] = $product->productJournal->title;
            } elseif ($product->prodType === 'Courses') {
                $prod['productName'] = $product->productCourse->title;
            }


            $arrayT = [];
            switch ($product->productCategory) {
                case 'ES':
                    if ($product->softCopyPrice !== null) {
                        $a = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->softCopyPrice
                        );
                        array_push($arrayT, $a);
                    }

                    if ($product->hardCopyPrice !== null) {
                        $b = array(
                            "specification" => "HARD COPY",
                            "price" => $product->hardCopyPrice
                        );
                        array_push($arrayT, $b);
                    }


                    if ($product->readOnlinePrice !== null) {
                        $b = array(
                            "specification" => "READ ONLINE",
                            "price" => $product->readOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->videoPrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->videoPrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->subscribePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->subscribePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->streamOnlinePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->streamOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }
                    break;
                case 'VP':
                    if ($product->softCopyPrice !== null) {
                        $a = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->softCopyPrice
                        );
                        array_push($arrayT, $a);
                    }

                    if ($product->hardCopyPrice !== null) {
                        $b = array(
                            "specification" => "HARD COPY",
                            "price" => $product->hardCopyPrice
                        );
                        array_push($arrayT, $b);
                    }


                    if ($product->readOnlinePrice !== null) {
                        $b = array(
                            "specification" => "READ ONLINE",
                            "price" => $product->readOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->videoPrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->videoPrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->subscribePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->subscribePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->streamOnlinePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->streamOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }
                    break;
                case 'EP':

                    if ($product->softCopyPrice !== null) {
                        $a = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->softCopyPrice
                        );
                        array_push($arrayT, $a);
                    }

                    if ($product->hardCopyPrice !== null) {
                        $b = array(
                            "specification" => "HARD COPY",
                            "price" => $product->hardCopyPrice
                        );
                        array_push($arrayT, $b);
                    }


                    if ($product->readOnlinePrice !== null) {
                        $b = array(
                            "specification" => "READ ONLINE",
                            "price" => $product->readOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->videoPrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->videoPrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->subscribePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->subscribePrice
                        );
                        array_push($arrayT, $b);
                    }

                    if ($product->streamOnlinePrice !== null) {
                        $b = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->streamOnlinePrice
                        );
                        array_push($arrayT, $b);
                    }
                    break;
                case 'BP':
                    if ($product->subscriptionLevel > 0) {
                        $sub =  Subscription::where('level', $product->subscriptionLevel)->first()->price;

                        $a = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $sub
                        );
                        array_push($arrayT, $a);
                    }

                    if ($product->subscriptionLevel < 1) {
                        $a = array(
                            "specification" => "DIGITAL COPY",
                            "price" => "0"
                        );
                        array_push($arrayT, $a);
                    }
                    break;

                default:
                    array_push($arrayT, []);
            }
            $prod['productSpecifications'] = $arrayT;


            /*if($product->productCategory === 'BP'){
                    switch ($product->subscriptionLevel){
                        case  0:
                       $productSpecifications = array(
                            "specification" => "DIGITAL COPY",
                            "price" => 0
                        );
                       array_push($productSpecificationArray, $productSpecifications);

                        $prod['productSpecifications'] = $productSpecificationArray;
                            break;

                    }
                }*/


            /*  $arrayTest = [];
                if($product->productCategory === 'ES'){
                    if($product->softCopyPrice !== null){
                        $productSpecification = array(
                            "specification" => "DIGITAL COPY",
                            "price" => $product->softCopyPrice
                        );
                        array_push($arrayTest, $productSpecification);

                    }

                    if($product->hardCopyPrice !== null){
                        $productSpecification = array(
                            "specification" => "HARD COPY",
                            "price" => $product->hardCopyPrice
                        );
                        array_push($arrayTest, $productSpecification);

                    }

                    if($product->readOnlinePrice !== null){
                        $productSpecification = array(
                            "specification" => "READ ONLINE",
                            "price" => $product->readOnlinePrice
                        );
                        array_push($arrayTest, $productSpecification);

                    }
                    array_push($productSpecificationArray, $arrayTest);
                    $prod['productSpecifications'] = $productSpecificationArray;

                }*/



            array_push($this->outer, $prod);
        }

        return $this->outer;
    }


    public function index()
    {
        //
        $product = Product::paginate(20);

        return ProductResource::collection($product);
    }
    public function getVendorCourses($id)
    {
        //
        $product = Product::where('prodType', '=', 'courses')->where('vendor_user_id', $id)->where('verify', 1)->get();

        return ProductResource::collection($product);
    }

    public function getProductByVendor($id)
    {
        //
        $product = Product::where('vendor_user_id', '=', $id)->where('verify', 1)->orderBy('created_at', 'desc')->get();

        return ProductResource::collection($product);
    }


    public function getProductByIndustry($id)
    {
        //
        $product = Product::where('industry_id', '=', $id)->where('verify', 1)->get();

        return ProductResource::collection($product);
    }
    public function getProductByTopic($id)
    {
        //
        $product = Product::where('sub_category_brand_id', '=', $id)->get();

        return ProductResource::collection($product);
    }

    public function getAllVerifyEvent()
    {
        $product = Product::where('prodType', 'Events')->where('verify', 1)->get();
        return ProductResource::collection($product);
    }

    public function getSingleEvent($id)
    {
        $product = Product::where('id', $id)->where('verify', 1)->get();
        return ProductResource::collection($product);
    }



    public function getAllVerifyarticle()
    {
        $product = Product::where('prodType', 'Articles')->where('verify', 1)->orderBy('id', 'DESC')->get();
        return ProductResource::collection($product);
    }


    public function getAllProductType($prodType)
    {
        $product =  Product::where('prodType', $prodType)->where('verify', '=', 1)->where('draftValue', '=', 'N')->where('productCategory', '=', 'VP')->get();
        return ProductResource::collection($product);
    }

    public function getSpecificProductType($prodType)
    {
        $product =  Product::where('prodType', $prodType)->where('verify', '=', 1)->where('draftValue', '=', 'N')->get();
        return ProductResource::collection($product);
    }


    public function getAllProductTypeForBizguruh($prodType)
    {
        //   return $prodType;


        $product = Product::where('prodType', '=', $prodType)->where('verify', '=', 1)->where('draftValue', '=', 'N')->get();
        return ProductResource::collection($product);
    }


    public function getSomeProductType()
    {
        $product = Product::whereIn('prodType', ['Books', 'Articles', 'Videos', 'Podcast', 'Market Research'])->where('verify', '=', 1)->where('draftValue', '=', 'N')->paginate(10);

        return ProductResource::collection($product);
    }

    public function getRelatedProductType($prodType, $id)
    {
        $product = Product::where('prodType', $prodType)->where('id', '!=', $id)->where('verify', '=', 1)->where('draftValue', '=', 'N')->get();
        return ProductResource::collection($product);
    }

    public function bestSellingProducts()
    {
        $product = Product::paginate(2);

        return ProductResource::collection($product);
    }


    public function getAllVerifiedProduct()
    {
        $product =  Product::where('verify', '=', 1)->where('draftValue', '=', 'N')->get();
        return ProductResource::collection($product);
    }

    public function getAllDraftNotVerifiedProduct()
    {
        $id = auth('vendor')->user()->id;

        $product = Product::where('vendor_user_id', $id)->where('verify', 0)->where('draftValue', 'Y')->get();
        return ProductResource::collection($product);
    }


    public function getAllDraftProductForAdmin()
    {
        $id = auth('admin_user')->user()->id;

        $product = Product::where('vendor_user_id', $id)->where('verify', 0)->where('productCategory', 'BP')->where('draftValue', 'Y')->get();
        return ProductResource::collection($product);
    }

    public function getAllRelatedProduct($id)
    {
        $product = Product::where('sub_category_id', '=', $id)->simplePaginate(3);
        return ProductResource::collection($product);
    }

    public function showProductDetail($id)
    {
        $product = Product::where('id', '=', $id)->where('verify', 1)->get();
        return ProductResource::collection($product);
    }

    public function showCourseDetail($id)
    {
        $product = ProductCourse::where('product_id', $id)->value('id');
        $course = ProductCourseModule::where('products_courses_id', $product)->get();

        return $course;
    }

    public function getVideoDetail($id)
    {
        $productVideos = ProductVideoDetail::where('productId', $id)->select('id', 'title', 'overview', 'guest', 'aboutGuest', 'price')->get();
        return $productVideos;
    }

    public function getFrontCategory($id)
    {
        $product = Product::where('category_id', '=', $id)->where('verify', 1)->get();
        return ProductResource::collection($product);
    }

    public function getFrontSubCategory($id)
    {
        $product = Product::where('sub_category_id', '=', $id)->where('verify', 1)->get();
        return ProductResource::collection($product);
    }

    public function getFrontSubCategoryBrand($id)
    {
        $product = Product::where('sub_category_brand_id', '=', $id)->where('verify', 1)->get();
        return ProductResource::collection($product);
    }

    public function getAllSponsorProduct()
    {
        $product = Product::where('verify', '=', 1)->where('sponsor', '=', 'Y')->get();
        return ProductResource::collection($product);
    }

    public function getVendorProduct($request)
    {
        $product = Product::where('vendor_user_id', $request)->where('draftValue', 'N')->latest()->get();

        return ProductResource::collection($product);
    }

    public function getAdminProduct()
    {
        $product = Product::where('draftValue', 'N')->latest()->paginate(10);

        return ProductResource::collection($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function addShippingDetail(Request $request)
    {
        $this->validate($request, [
            'location' => 'required'
        ]);


        $product = Product::where('id', '=', $request->id)->first();
        $product->location = $request->location;
        $product->addressLineOne = $request->addressLineOne;
        $product->addressLineTwo = $request->addressLineTwo;
        $product->state = $request->state;
        $product->localShippingRates = $request->localShippingRates;
        $product->localShippingTime = $request->localShippingTime;
        $product->intShippingRates = $request->intShippingRates;
        $product->intShippingTime = $request->intShippingTime;
        $product->deliveryInformation = $request->deliveryInformation;
        $product->returnPolicy = $request->returnPolicy;
        $product->extraComment = $request->extraComment;
        $product->save();

        return $product;
    }

    public function continueAddProduct(Request $request)
    {
        //return $request;
        //$body = $request->getContent('data');


        $body =  json_decode(request()->getContent(), true);

        $product = Product::where('id', $body['id'])->first();
        $product->productCategory = $body['productCategory'];
        $adminId =  $product->vendor_user_id;
        $product->vendor_user_id = $body['vendorUserId'] ? (int) $body['vendorUserId'] : $adminId;
        if (isset($body['subscriptionLevel'])) {
            $product->subscriptionLevel = $body['subscriptionLevel'];
        }

        switch ($body['prodType']) {
            case 'Events':
                $product->prodType = $body['prodType'];
                $product->draftValue = $body['draftType'];
                $product->eventPrice = $body['eventPrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $ep = new EventProduct();
                $ep->title = $body['events']['title'];
                $ep->description = $body['events']['description'];
                $ep->date = $body['events']['date'];
                $ep->time = $body['events']['time'];
                $ep->endDate = $body['events']['endDate'];
                $ep->endTime = $body['events']['endTime'];
                $ep->address = $body['events']['address'];
                $ep->city = $body['events']['city'];
                $ep->state = $body['events']['state'];
                $ep->country = $body['events']['country'];
                $ep->quantity = $body['events']['quantity'];
                $ep->convenerName = $body['events']['convenerName'];
                $ep->aboutConvener = $body['events']['aboutConvener'];

                if (!isset($product->productEvent)) {
                    $product->productEvent()->save($ep);
                    foreach ($body['events']['eventSchedule'] as $we) {
                        $ev = new EventScheduleTime();
                        $ev->startDay = $we['startDay'];
                        $ev->eventStart = $we['eventStart'];
                        $ev->endDay = $we['endDay'];
                        $ev->eventStartEnd = $we['eventStartEnd'];
                        $ev->eventEndEnd = $we['eventEndEnd'];
                        $ev->eventEnd = $we['eventEnd'];
                        $ev->moreInformation = $we['moreInformation'];
                        $ev->product_id = $ep->id;
                        $ep->eventScheduleTime()->save($ev);
                    }
                    $product->save();
                } else {
                    return response()->json('Data already saved');
                }
                break;
            case 'Articles':
                $product->prodType = $body['prodType'];
                $product->draftValue = $body['draftType'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $ap = new ArticleProduct();
                $ap->title = $body['articles']['title'];
                $ap->description = $body['articles']['description'];
                $ap->tags = $body['articles']['tags'];
                $ap->media = $body['articles']['media'];
                $ap->writer = $body['articles']['writer'];
                $ap->aboutWriter = $body['articles']['aboutWriter'];
                $ap->featured = $body['articles']['featured'];

                if (!isset($product->productArticle)) {
                    $product->productArticle()->save($ap);
                    $product->save();
                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New Article",
                        "title" => 'New Article Alert ',
                        "message" => $body['articles']['title'],
                        "link" => "https://bizguruh.com/articles",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved');
                }

                break;
            case 'Market Reports':
                $product->prodType = $body['prodType'];

                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $mr = new ProductMarketReport();
                $mr->title = $body['marketReport']['title'];
                $mr->aboutThisReport = $body['marketReport']['aboutThisReport'];
                $mr->overview = $body['marketReport']['overview'];
                $mr->tableOfContent = $body['marketReport']['tableOfContent'];
                $mr->contributor = $body['marketReport']['contributor'];
                $mr->author = $body['marketReport']['author'];
                $mr->excerpt = $body['marketReport']['excerpts'];
                $mr->excerptName = $body['marketReport']['excerptsName'];
                $mr->excerptFile = $body['marketReport']['excerptsFile'];
                $mr->excerptPublicId = $body['marketReport']['excerptsPublicId'];
                $mr->sponsorName = $body['marketReport']['sponsorName'];
                $mr->aboutSponsor = $body['marketReport']['aboutSponsor'];

                if (!isset($product->marketReport)) {
                    $product->marketReport()->save($mr);
                    $product->save();
                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New  Research",
                        "title" => "New Research Alert",
                        "message" => $body['marketReport']['title'],
                        "link" => "https://bizguruh.com/paper/market-research",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved');
                }
                break;
            case 'Market Research':
                $product->prodType = $body['prodType'];

                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }

                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $mre = new ProductMarketResearch();
                $mre->title = $body['marketResearch']['title'];
                $mre->aboutThisReport = $body['marketResearch']['aboutThisReport'];
                $mre->overview = $body['marketResearch']['overview'];
                $mre->tableOfContent = $body['marketResearch']['tableOfContent'];

                if(is_null($body['marketResearch']['contributor'])){
                    $mre->contributor='null';
                }else{
                    $mre->contributor = $body['marketResearch']['contributor'];
                }
               
                $mre->author = $body['marketResearch']['author'];
                $mre->excerpt = $body['marketResearch']['excerpts'];
                $mre->excerptName = $body['marketResearch']['excerptsName'];
                $mre->excerptFile = $body['marketResearch']['excerptsFile'];
                $mre->excerptPublicId = $body['marketResearch']['excerptsPublicId'];
                $mre->sponsorName = $body['marketResearch']['sponsorName'];
                $mre->aboutSponsor = $body['marketResearch']['aboutSponsor'];

                if (!isset($product->marketResearch)) {
                    $product->marketResearch()->save($mre);
                    $product->save();

                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New Research ",
                        "title" => "New Research Alert",
                        "message" => $body['marketResearch']['title'],
                        "link" => "https://bizguruh.com/paper/market-research",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved');
                }

                break;
            case 'Books':


                $product->prodType = $body['prodType'];
                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }

                if ($body['audioPrice'] !== '') {
                    $product->audioCopy = 1;
                }


                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->audioPrice = $body['audioPrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $book = new ProductBook();
                $book->title = $body['books']['title'];
                $book->author = $body['books']['author'];
                $book->publisher = $body['books']['publisher'];
                $book->tableOfContent = $body['books']['tableOfContent'];
                $book->publicationDate = $body['books']['publicationDate'];
                $book->noOfPages = $body['books']['noOfPages'];
                $book->edition = $body['books']['edition'];
                $book->isbn = $body['books']['isbn'];
                $book->sponsorName = $body['books']['sponsorName'];
                $book->aboutSponsor = $body['books']['aboutSponsor'];
                $book->edition = $body['books']['edition'];
                $book->aboutThisAuthor = $body['books']['aboutThisAuthor'];
                $book->excerptName = $body['books']['excerptName'];
                $book->excerpt = $body['books']['excerpt'];
                $book->excerptFile = $body['books']['excerptFile'];
                $book->excerptPublicId = $body['books']['excerptPublicid'];
                $book->audioFile = $body['books']['audioFile'];
                $book->audioName = $body['books']['audioName'];
                $book->audioPublicId = $body['books']['audioPublicId'];

                if (!isset($product->productBook)) {
                    $product->productBook()->save($book);
                    $product->save();
                } else {
                    return response()->json('Data already saved');
                }

                break;
            case 'White Paper':
                $product->prodType = $body['prodType'];

                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }

                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wp = new ProductWhitePaper();
                $wp->title = $body['whitePaper']['title'];
                $wp->author = $body['whitePaper']['author'];
                $wp->contributors = $body['whitePaper']['contributor'];
                $wp->problemStatement = $body['whitePaper']['problemStatement'];
                $wp->executiveSummary = $body['whitePaper']['executiveSummary'];
                $wp->excerpt = $body['whitePaper']['excerpts'];
                $wp->excerptName = $body['whitePaper']['excerptsName'];
                $wp->sponsorName = $body['whitePaper']['sponsorName'];
                $wp->aboutSponsor = $body['whitePaper']['aboutSponsor'];
                $wp->excerptFile = $body['whitePaper']['excerptsFile'];
                $wp->excerptPublicId = $body['whitePaper']['excerptsPublicId'];

                if (!isset($product->productWhitePaper)) {
                    $product->productWhitePaper()->save($wp);
                    $product->save();
                } else {
                    return response()->json('Data already saved');
                }
                break;
            case 'Journals':
                $product->prodType = $body['prodType'];


                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $pj = new ProductJournal();
                $pj->articleTitle = $body['journal']['articleTitle'];
                $pj->journalTitle = $body['journal']['journalTitle'];
                $pj->abstract = $body['journal']['abstract'];
                $pj->author = $body['journal']['author'];
                $pj->volume = $body['journal']['volume'];
                $pj->issue = $body['journal']['issue'];
                $pj->monthYear = $body['journal']['monthYear'];
                $pj->pages = $body['journal']['pageNo'];
                $pj->publicationDate = $body['journal']['publicationDate'];
                $pj->issn = $body['journal']['isbn'];
                $pj->tableOfContent = $body['journal']['tableOfContent'];
                $pj->publisher = $body['journal']['publisher'];
                $pj->excerptFile = 5;
                $pj->sponsorName = $body['journal']['sponsorName'];
                $pj->aboutSponsor = $body['journal']['aboutSponsor'];

                if (!isset($product->productJournal)) {
                    $product->productJournal()->save($pj);
                    $product->save();
                } else {
                    return response()->json('Data already saved');
                }
                break;
            case 'Webinar':
                $product->prodType = $body['prodType'];

                if ($body['webinarPrice'] !== '') {
                    $product->videoCopy = 1;
                }

                $product->webinarPrice = $body['webinarPrice'];
                $product->draftValue = $body['draftType'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wb = new ProductChapter();
                $wb->title = $body['webinarVideo']['title'];
                $wb->description = $body['webinarVideo']['description'];
                $wb->host = $body['webinarVideo']['host'];
                $wb->aboutHost = $body['webinarVideo']['aboutHost'];
                $wb->description = $body['webinarVideo']['overview'];
                $wb->startDate = $body['webinarVideo']['date'];
                $wb->excerpts = $body['webinarVideo']['excerpt'];
                $wb->sponsorName = $body['webinarVideo']['sponsorName'];
                $wb->aboutSponsor = $body['webinarVideo']['aboutSponsor'];
                $wb->excerptsName = $body['webinarVideo']['excerptName'];
                $wb->excerptsPublicId = $body['webinarVideo']['excerptPublicId'];
                $wb->description = $body['webinarVideo']['overview'];
                $wb->prodType = 'Webinar';


                if (!isset($product->productChapter)) {
                    $product->productChapter()->save($wb);
                    $product->save();
                } else {
                    return response()->json('Data already saved, Edit to make changes');
                }
                break;
            case 'Videos':
                $product->prodType = $body['prodType'];

                if ($body['videoPrice'] !== '') {
                    $product->videoCopy = 1;
                }

                $product->videoPrice = $body['videoPrice'];
                $product->draftValue = $body['draftType'];
                $product->subscribePrice = $body['subscribePrice'];
                $product->streamOnlinePrice = $body['streamOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wb = new ProductChapter();
                $wb->title = $body['videos']['title'];
                $wb->host = $body['videos']['host'];
                $wb->aboutHost = $body['videos']['aboutHost'];
                $wb->description = $body['videos']['overview'];
                $wb->sponsorName = $body['videos']['sponsorName'];
                $wb->aboutSponsor = $body['videos']['aboutSponsor'];
                $wb->excerpts = $body['videos']['excerpts'];
                $wb->excerptsName = $body['videos']['excerptsName'];
                $wb->excerptsPublicId = $body['videos']['excerptsPublicId'];
                $wb->prodType = 'Videos';


                if (!isset($product->productChapter)) {
                    $product->productChapter()->save($wb);
                    $product->save();

                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New Video Alert",
                        "title" => "New Video Alert",
                        "message" => $body['videos']['title'],
                        "link" => "https://bizguruh.com/videos",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved, Edit to make changes');
                }

                foreach ($body['videos']['episode'] as $webinar) {
                    $wbe = new ProductVideoDetail();
                    $wbe->title = $webinar['title'];
                    $wbe->overview = $webinar['description'];
                    $wbe->videoFile = $webinar['videoFile'];
                    $wbe->guest = $webinar['guest'];
                    $wbe->aboutGuest = $webinar['aboutGuest'];
                    $wbe->price = $webinar['price'];
                    $wbe->date = $webinar['date'];
                    $wbe->videoPublicId = $webinar['videoPublicId'];
                    $wbe->videoName = $webinar['videoName'];
                    $wbe->mediaType = $webinar['mediaType'];


                    $wb->productVideoDetail()->save($wbe);
                }

                break;
            case 'Podcast':

                $product->prodType = $body['prodType'];

                if ($body['audioPrice'] != '') {
                    $product->audioCopy = 1;
                }

                $product->audioPrice = $body['audioPrice'];
                $product->draftValue = $body['draftType'];
                $product->subscribePrice = $body['subscribePrice'];
                $product->streamOnlinePrice = $body['streamOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wb = new ProductChapter();
                $wb->title = $body['podcast']['title'];
                $wb->host = $body['podcast']['host'];
                $wb->aboutHost = $body['podcast']['aboutHost'];
                $wb->sponsorName = $body['podcast']['sponsorName'];
                $wb->aboutSponsor = $body['podcast']['aboutSponsor'];
                $wb->excerpts = $body['podcast']['excerpt'];
                $wb->excerptsName = $body['podcast']['excerptName'];
                $wb->excerptsPublicId = $body['podcast']['excerptPublicId'];
                $wb->description = $body['podcast']['overview'];
                $wb->prodType = 'Podcast';


                if (!isset($product->productChapter)) {
                    $product->productChapter()->save($wb);
                    $product->save();

                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New Podcast Alert",
                        "title" => "New Podcast Alert",
                        "message" => $body['podcast']['title'],
                        "link" => "https://bizguruh.com/podcast",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved, Edit to make changes');
                }

                foreach ($body['podcast']['episode'] as $webinar) {
                    $wbe = new ProductVideoDetail();
                    $wbe->title = $webinar['title'];
                    $wbe->overview = $webinar['description'];
                    $wbe->videoFile = $webinar['videoFile'];
                    $wbe->guest = $webinar['guest'];
                    $wbe->aboutGuest = $webinar['aboutGuest'];
                    $wbe->price = $webinar['price'];
                    $wbe->date = $webinar['date'];
                    $wbe->videoPublicId = $webinar['videoPublicId'];
                    $wbe->videoName = $webinar['videoName'];
                    $wbe->mediaType = $webinar['mediaType'];


                    $wb->productVideoDetail()->save($wbe);
                }

                break;
            case 'Courses':
                 
                $product->prodType = $body['prodType'];

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }


                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['readOnlinePrice'] !== '') {
                    $product->readOnline = 1;
                }

                if ($body['audioPrice'] !== '') {
                    $product->audioCopy = 1;
                }

                if ($body['videoPrice'] !== '') {
                    $product->videoCopy = 1;
                }
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->audioPrice = $body['audioPrice'];
                $product->videoPrice = $body['videoPrice'];
                $product->department_id =$body['course']['department_id'];
                $product->faculty_id =$body['course']['faculty_id'];
                $product->course_level = $body['course']['course_level'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $co =  new ProductCourse();
                $co->title =  $body['course']['title'];
                $co->overview =  $body['course']['overview'];
                $co->duration =  $body['course']['duration'];
                $co->durationPeriod =  $body['course']['durationPeriod'];
                 $co->courseModules =  json_encode($body['course']['courseModules']);
                 $co->courseObjectives =  $body['course']['courseObjectives'];
                 $co->whoThisCourseFor =  json_encode($body['course']['whoIsthisCourseFor']);
                 $co->keyBenefits =  $body['course']['keyBenefit'];
                $co->whenYouComplete =  $body['course']['whenYouComplete'];
                $co->fullOnline =  $body['course']['fullOnline'];
                $co->mixedClass =  $body['course']['mixedClass'];
                $co->level =  $body['course']['level'];
                $co->certification =  $body['course']['certification'];
                $co->excerpt = $body['course']['excerpts'];
                $co->excerptName = $body['course']['excerptsName'];
                $co->excerptFile = $body['course']['excerptsFile'];
                $co->excerptPublicId = $body['course']['excerptsPublicId'];
                $co->sponsorName = $body['course']['sponsorName'];
                $co->aboutSponsor = $body['course']['aboutSponsor'];

                
                       
                if (!isset($product->productCourse)) {
                    $product->productCourse()->save($co);
                    $product->save();

                    EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
                    $data = [
                        "campaignName" =>  "New Course Alert",
                        "title" => "New Course Alert",
                        "message" => $body['course']['title'],
                        "link" => "https://bizguruh.com/courses",
                        "icon" => "https://bizguruh.com/images/logo.png"
                    ];

                    EngagespotPush::setMessage($data);
                  

                    // EngagespotPush::send();
                } else {
                    return response()->json('Data already saved, Edit to make changes');
                }

                foreach ($body['course']['whatYouWillLearn'] as $learn) {
                    $cl = new CourseWhatYouWillLearn();
                    $cl->whatYouWillLearn = $learn['whatYouWillLearn'];
                    $co->courseToWhatLearn()->save($cl);
                }

                foreach ($body['course']['modules'] as $module) {
                    $pcm = new ProductCourseModule();
                    $pcm->title = $module['title'];
                    $pcm->description = $module['description'];
                    $pcm->fileName = $module['fileName'];
                    $pcm->filePublicId = $module['filePublicId'];
                    $pcm->audioFile = $module['audioFile'];
                    $pcm->fileAudioName = $module['fileAudioName'];
                    $pcm->audioPublicId = $module['audioPublicId'];
                    
                    if(is_null($module['file'])){
                        $pcm->file = 0;
                    }else{
                        $pcm->file = $module['file'];
                    }
                    if(is_null($module['vidTime'])){
                        $pcm->vidTime = 0;
                    }else{
                        $pcm->vidTime = $module['vidTime'];
                    }
                    if(is_null($module['audTime'])){
                        $pcm->audTime = 0;
                    }else{
                        $pcm->audTime = $module['audTime'];
                    }
                   
                    $pcm->videoPublicId = $module['videoPublicId'];
                    $pcm->fileVideoName = $module['fileVideoName'];
                    $pcm->author = $module['author'];
                    $pcm->aboutAuthor = $module['aboutAuthor'];
                    $pcm->duration = $module['duration'];
                    $pcm->durationPeriod = $module['durationPeriod'];

                    $co->productCourseModule()->save($pcm);
                }


                break;
            default:
                return false;
        }



        //  $body =  json_decode(request()->getContent(), true);

        return new ProductResource($product);
    }



    public function addArticle(Request $request)
    {
        $body =  json_decode(request()->getContent(), true);

        $product = Product::where('id', $body['id'])->first();
        $product->productCategory = $body['productCategory'];
        $adminId =  $product->vendor_user_id;
        $product->vendor_user_id = $body['vendorUserId'] ? (int) $body['vendorUserId'] : $adminId;
        if (isset($body['subscriptionLevel'])) {
            $product->subscriptionLevel = $body['subscriptionLevel'];
        }


        $product->prodType = $body['prodType'];
        $product->draftValue = $body['draftType'];
        $mainImageName = "coverImage" . time();
        Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
        $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
        $ap = new ArticleProduct();
        $ap->title = $body['articles']['title'];
        $ap->description = $body['articles']['description'];
        $ap->tags = $body['articles']['tags'];
        $ap->media = $body['articles']['media'];
        $ap->writer = $body['articles']['writer'];
        $ap->aboutWriter = $body['articles']['aboutWriter'];
        $ap->featured = $body['articles']['featured'];

        if (!isset($product->productArticle)) {
            $product->productArticle()->save($ap);
            $product->save();

            //if new article added

            EngagespotPush::initialize('cywCppTlinzpmjVHxFcYoiuuKlnefc', '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt');
            $data = [
                "campaignName" =>  "New  Article",
                "title" =>  "New Article Alert",
                "message" => $body['articles']['title'],
                "link" => "https://bizguruh.com/articles",
                "icon" => "https://bizguruh.com/images/logo.png"
            ];

            EngagespotPush::setMessage($data);
          

            // EngagespotPush::send();
        } else {
            return response()->json('Data already saved');
        }





        return new ProductResource($product);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'category_id' => 'required',
        ]);
     
        $product = new Product();
        $product->category_id = $request->category_id;
        $product->sub_category_id = $request->sub_category_id;
        $product->sub_category_brand_id =json_encode( $request->sub_category_brand_id);
        $product->insight_id = $request->insight_id;
        $product->industry_id = $request->industry_id;
        $product->vendor_user_id = $request->vendor_user_id;
        $product->productCategory = $request->productCategory;
        $product->save();

        return new ProductResource($product);
    }

    public function show($id)
    {
        //
      
        $product = Product::findOrFail($id);

        return new ProductResource($product);
    }

    public function edit(Product $product, $id)
    {
        //
        //return $product->id;
        $product = Product::findOrFail($id);

        return new ProductEditResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request  $request, Product $product)
    {
        //

        $product->update([
            $product->category_id = $request->category_id,
            $product->sub_category_id = $request->sub_category_id,
            $product->sub_category_brand_id = $request->sub_category_brand_id,
            $product->vendor_user_id = $request->vendor_user_id
        ]);

        return new ProductResource($product);
    }

    public function continueEditProduct(Request $request)
    {
        $body =  json_decode(request()->getContent(), true);


        $product = Product::where('id', $body['id'])->first();
        $product->productCategory = $body['productCategory'];
        if (isset($body['subscriptionLevel'])) {
            $product->subscriptionLevel = $body['subscriptionLevel'];
        }

        $product->verify = 0;


        switch ($body['prodType']) {
            case 'Events':



                $product->prodType = $body['prodType'];
                $product->draftValue = $body['draftType'];
                $product->eventPrice = $body['eventPrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $ep = EventProduct::where('product_id', $body['id'])->first();
                $ep->title = $body['events']['title'];
                $ep->description = $body['events']['description'];
                $ep->date = $body['events']['date'];
                $ep->time = $body['events']['time'];
                $ep->endDate = $body['events']['endDate'];
                $ep->endTime = $body['events']['endTime'];
                $ep->address = $body['events']['address'];
                $ep->city = $body['events']['city'];
                $ep->state = $body['events']['state'];
                $ep->country = $body['events']['country'];
                $ep->quantity = $body['events']['quantity'];
                $ep->convenerName = $body['events']['convenerName'];
                $ep->aboutConvener = $body['events']['aboutConvener'];


                $product->productEvent()->save($ep);
                $product->save();

                $ev =  EventScheduleTime::where('product_id', $ep->id)->get();




                if (count($body['events']['eventSchedule']) === count($ev)) {
                    for ($i = 0; $i < count($ev); $i++) {
                        $ev[$i]->startDay = ($body['events']['eventSchedule'])[$i]['startDay'];
                        $ev[$i]->eventStart = ($body['events']['eventSchedule'])[$i]['eventStart'];
                        $ev[$i]->endDay = ($body['events']['eventSchedule'])[$i]['endDay'];
                        $ev[$i]->eventStartEnd = ($body['events']['eventSchedule'])[$i]['eventStartEnd'];
                        $ev[$i]->eventEndEnd = ($body['events']['eventSchedule'])[$i]['eventEndEnd'];
                        $ev[$i]->eventEnd = ($body['events']['eventSchedule'])[$i]['eventEnd'];
                        $ev[$i]->moreInformation = ($body['events']['eventSchedule'])[$i]['moreInformation'];
                        $ev[$i]->save();
                    }
                } elseif (count($body['events']['eventSchedule']) > count($ev)) {
                    for ($i = 0; $i < count($body['events']['eventSchedule']); $i++) {
                        if ($i < count($ev)) {
                            $ev[$i]->startDay = ($body['events']['eventSchedule'])[$i]['startDay'];
                            $ev[$i]->eventStart = ($body['events']['eventSchedule'])[$i]['eventStart'];
                            $ev[$i]->endDay = ($body['events']['eventSchedule'])[$i]['endDay'];
                            $ev[$i]->eventStartEnd = ($body['events']['eventSchedule'])[$i]['eventStartEnd'];
                            $ev[$i]->eventEndEnd = ($body['events']['eventSchedule'])[$i]['eventEndEnd'];
                            $ev[$i]->eventEnd = ($body['events']['eventSchedule'])[$i]['eventEnd'];
                            $ev[$i]->moreInformation = ($body['events']['eventSchedule'])[$i]['moreInformation'];
                            $ev[$i]->save();
                        } else {
                            $evd = new EventScheduleTime();
                            $ev->startDay = ($body['events']['eventSchedule'])[$i]['startDay'];
                            $ev->eventStart = ($body['events']['eventSchedule'])[$i]['eventStart'];
                            $ev->endDay = ($body['events']['eventSchedule'])[$i]['endDay'];
                            $ev->eventStartEnd = ($body['events']['eventSchedule'])[$i]['eventStartEnd'];
                            $ev->eventEndEnd = ($body['events']['eventSchedule'])[$i]['eventEndEnd'];
                            $ev->eventEnd = ($body['events']['eventSchedule'])[$i]['eventEnd'];
                            $ev->moreInformation = ($body['events']['eventSchedule'])[$i]['moreInformation'];
                            $ev->product_id = $ep->id;
                            $ev->save();
                        }
                    }
                } elseif (count($body['events']['eventSchedule']) < count($ev)) {
                    for ($i = 0; $i < count($ev); $i++) {
                        if ($i < count($body['events']['eventSchedule'])) {
                            $ev[$i]->startDay = ($body['events']['eventSchedule'])[$i]['startDay'];
                            $ev[$i]->eventStart = ($body['events']['eventSchedule'])[$i]['eventStart'];
                            $ev[$i]->endDay = ($body['events']['eventSchedule'])[$i]['endDay'];
                            $ev[$i]->eventStartEnd = ($body['events']['eventSchedule'])[$i]['eventStartEnd'];
                            $ev[$i]->eventEndEnd = ($body['events']['eventSchedule'])[$i]['eventEndEnd'];
                            $ev[$i]->eventEnd = ($body['events']['eventSchedule'])[$i]['eventEnd'];
                            $ev[$i]->moreInformation = ($body['events']['eventSchedule'])[$i]['moreInformation'];
                            $ev[$i]->save();
                        } else {
                            $ev[$i]->delete();
                        }
                    }
                }



                break;
            case 'Articles':
                $product->prodType = $body['prodType'];
                $product->draftValue = $body['draftType'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $ap = ArticleProduct::where('product_id', $body['id'])->first();
                $ap->title = $body['articles']['title'];
                $ap->description = $body['articles']['description'];
                $ap->writer = $body['articles']['writer'];
                $ap->aboutWriter = $body['articles']['aboutWriter'];
                $ap->featured = $body['articles']['featured'];
                $ap->tags = $body['articles']['tags'];
                $ap->media = $body['articles']['media'];
                $product->productArticle()->save($ap);
                $product->save();
                break;
            case 'Market Reports':
                $product->prodType = $body['prodType'];
                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->editFlag = $body['editFlag'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                //$mr = new ProductMarketReport();

                $mr = ProductMarketReport::where('product_id', $body['id'])->first();
                $mr->title = $body['marketReport']['title'];
                $mr->aboutThisReport = $body['marketReport']['aboutThisReport'];
                $mr->overview = $body['marketReport']['overview'];
                $mr->tableOfContent = $body['marketReport']['tableOfContent'];
                $mr->contributor = $body['marketReport']['contributor'];
                $mr->author = $body['marketReport']['author'];
                $mr->excerpt = $body['marketReport']['excerpt'];
                $mr->excerptName = $body['marketReport']['excerptName'];
                $mr->excerptFile = $body['marketReport']['excerptFile'];
                $mr->excerptPublicId = $body['marketReport']['excerptPublicId'];
                $mr->sponsorName = $body['marketReport']['sponsorName'];
                $mr->aboutSponsor = $body['marketReport']['aboutSponsor'];

                $product->marketReport()->save($mr);
                $product->save();
                break;
            case 'Market Research':
                $product->prodType = $body['prodType'];
                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }

                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                // $mre = new ProductMarketResearch();
                $mre = ProductMarketResearch::where('product_id', $body['id'])->first();

                $mre->title = $body['marketResearch']['title'];
                $mre->aboutThisReport = $body['marketResearch']['aboutThisReport'];
                $mre->overview = $body['marketResearch']['overview'];
                $mre->tableOfContent = $body['marketResearch']['tableOfContent'];
                $mre->contributor = $body['marketResearch']['contributor'];
                $mre->author = $body['marketResearch']['author'];
                $mre->excerpt = $body['marketResearch']['excerpt'];
                $mre->excerptName = $body['marketResearch']['excerptName'];
                $mre->excerptFile = $body['marketResearch']['excerptFile'];
                $mre->excerptPublicId = $body['marketResearch']['excerptPublicId'];
                $mre->sponsorName = $body['marketResearch']['sponsorName'];
                $mre->aboutSponsor = $body['marketResearch']['aboutSponsor'];


                $product->marketResearch()->save($mre);
                $product->save();

                break;
            case 'Books':

                $product->prodType = $body['prodType'];
                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }

                if ($body['audioPrice'] !== '') {
                    $product->audioCopy = 1;
                }

                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->editFlag = $body['editFlag'];
                $product->draftValue = $body['draftType'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->audioPrice = $body['audioPrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $book = ProductBook::where('product_id', $body['id'])->first();
                $book->title = $body['books']['title'];
                $book->author = $body['books']['author'];
                $book->publisher = $body['books']['publisher'];
                $book->tableOfContent = $body['books']['tableOfContent'];
                $book->publicationDate = $body['books']['publicationDate'];
                $book->noOfPages = $body['books']['noOfPages'];
                $book->edition = $body['books']['edition'];
                $book->isbn = $body['books']['isbn'];
                $book->sponsorName = $body['books']['sponsorName'];
                $book->aboutSponsor = $body['books']['aboutSponsor'];
                $book->edition = $body['books']['edition'];
                $book->aboutThisAuthor = $body['books']['aboutThisAuthor'];
                $book->excerpt = $body['books']['excerpt'];
                $book->excerptFile = $body['books']['excerptFile'];
                $book->excerptName = $body['books']['excerptName'];
                $book->excerptPublicId = $body['books']['excerptPublicId'];
                $book->audioFile = $body['books']['audioFile'];
                $book->audioName = $body['books']['audioName'];
                $book->audioPublicId = $body['books']['audioPublicId'];
                $product->productBook()->save($book);
                $product->save();
                break;
            case 'White Paper':
                $product->prodType = $body['prodType'];
                if (isset($body['hardCopyPrice'])) {
                    $product->hardCopy = 1;
                }

                if (isset($body['softCopyPrice'])) {
                    $product->softCopy = 1;
                }

                if (isset($body['readonline'])) {
                    $product->readonline = 1;
                }



                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wp = ProductWhitePaper::where('product_id', $body['id'])->first();
                $wp->title = $body['whitePaper']['title'];
                $wp->author = $body['whitePaper']['author'];
                $wp->contributors = $body['whitePaper']['contributors'];
                $wp->problemStatement = $body['whitePaper']['problemStatement'];
                $wp->executiveSummary = $body['whitePaper']['executiveSummary'];
                $wp->excerpt = $body['whitePaper']['excerpt'];
                $wp->excerptName = $body['whitePaper']['excerptName'];
                $wp->sponsorName = $body['whitePaper']['sponsorName'];
                $wp->aboutSponsor = $body['whitePaper']['aboutSponsor'];
                $wp->excerptFile = $body['whitePaper']['excerptFile'];
                $wp->excerptPublicId = $body['whitePaper']['excerptPublicId'];

                $product->productWhitePaper()->save($wp);
                $product->save();
                break;
            case 'Journals':
                $product->prodType = $body['prodType'];

                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }

                if ($body['readonline'] !== '') {
                    $product->readonline = 1;
                }
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $product->document = $body['document'];
                $product->documentName = $body['documentName'];
                $product->documentPublicId = $body['documentPublicId'];
                $product->quantity = $body['quantity'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                // $pj = new ProductJournal();
                $pj = ProductJournal::where('product_id', $body['id'])->first();
                $pj->articleTitle = $body['journal']['articleTitle'];
                $pj->journalTitle = $body['journal']['journalTitle'];
                $pj->abstract = $body['journal']['abstract'];
                $pj->author = $body['journal']['author'];
                $pj->volume = $body['journal']['volume'];
                $pj->issue = $body['journal']['issue'];
                $pj->monthYear = $body['journal']['monthYear'];
                $pj->pages = $body['journal']['pages'];
                $pj->publicationDate = $body['journal']['publicationDate'];
                $pj->issn = $body['journal']['issn'];
                $pj->tableOfContent = $body['journal']['tableOfContent'];
                $pj->publisher = $body['journal']['publisher'];
                $pj->excerptFile = 5;
                $pj->sponsorName = $body['journal']['sponsorName'];
                $pj->aboutSponsor = $body['journal']['aboutSponsor'];

                $product->productJournal()->save($pj);
                $product->save();

                break;
            case 'Webinar':
                $product->prodType = $body['prodType'];

                if ($body['webinarPrice'] !== '') {
                    $product->videoCopy = 1;
                }
                $product->webinarPrice = $body['webinarPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                $wb = new ProductChapter();
                $wb->title = $body['webinarVideo']['title'];
                $wb->description = $body['webinarVideo']['description'];
                $wb->host = $body['webinarVideo']['host'];
                $wb->aboutHost = $body['webinarVideo']['aboutHost'];
                $wb->description = $body['webinarVideo']['overview'];
                $wb->startDate = $body['webinarVideo']['date'];
                $wb->excerpts = $body['webinarVideo']['excerpt'];
                $wb->sponsorName = $body['webinarVideo']['sponsorName'];
                $wb->aboutSponsor = $body['webinarVideo']['aboutSponsor'];
                $wb->excerptsName = $body['webinarVideo']['excerptName'];
                $wb->excerptsPublicId = $body['webinarVideo']['excerptPublicId'];
                $wb->description = $body['webinarVideo']['overview'];
                $wb->prodType = 'Webinar';


                if (!isset($product->productChapter)) {
                    $product->productChapter()->save($wb);
                    $product->save();
                } else {
                    return response()->json('Data already saved, Edit to make changes');
                }

                /*foreach($body['webinar']['episode'] as $webinar){
                    $wbe = new ProductVideoDetail();
                    $wbe->title = $webinar['title'];
                    $wbe->description = $webinar['description'];
                    $wbe->videoFile = $webinar['videoFile'];
                    $wbe->videoName = $webinar['videoName'];
                    $wbe->mediaType = $webinar['mediaType'];


                    $wb->productVideoDetail()->save($wbe);

               }*/
                break;
            case 'Videos':

                $product->prodType = $body['prodType'];


                if ($body['videoPrice'] !== '') {
                    $product->videoCopy = 1;
                }

                $product->videoPrice = $body['videoPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $product->subscribePrice = $body['subscribePrice'];
                $product->streamOnlinePrice = $body['streamOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                // $wb = new ProductChapter();
                $wb = ProductChapter::where('product_id', $body['id'])->first();
                $wb->title = $body['videos']['title'];
                $wb->host = $body['videos']['host'];
                $wb->aboutHost = $body['videos']['aboutHost'];
                $wb->description = $body['videos']['overview'];
                $wb->sponsorName = $body['videos']['sponsorName'];
                $wb->aboutSponsor = $body['videos']['aboutSponsor'];
                $wb->excerpts = $body['videos']['excerpts'];
                $wb->excerptsName = $body['videos']['excerptsName'];
                $wb->excerptsPublicId = $body['videos']['excerptsPublicId'];
                $wb->prodType = 'Videos';

                $product->productChapter()->save($wb);
                $product->save();


                $wbs = ProductVideoDetail::where('productId', $wb->id)->get();
                /*foreach($body['videos']['episode'] as $webinar){
                    foreach($wbs as $wbe){

                        $wbe->title = $webinar['title'];
                        $wbe->overview = $webinar['overview'];
                        $wbe->videoFile = $webinar['videoFile'];
                        $wbe->guest = $webinar['guest'];
                        $wbe->aboutGuest = $webinar['aboutGuest'];
                        $wbe->date = $webinar['date'];
                        $wbe->videoPublicId = $webinar['videoPublicId'];
                        $wbe->videoName = $webinar['videoName'];
                        $wbe->mediaType = $webinar['mediaType'];
                        $wbe->productId = $wb->id;
                        $wbe->save();
                      //  $wbe->save();
                      //  $wb->productVideoDetail()->save($wbe);


                    }

                }*/
                if (count($body['videos']['episode']) === count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        $wbs[$i]->title = ($body['videos']['episode'])[$i]['title'];
                        $wbs[$i]->overview = ($body['videos']['episode'])[$i]['overview'];
                        $wbs[$i]->videoFile = ($body['videos']['episode'])[$i]['videoFile'];
                        $wbs[$i]->guest = ($body['videos']['episode'])[$i]['guest'];
                        $wbs[$i]->aboutGuest = ($body['videos']['episode'])[$i]['aboutGuest'];
                        $wbs[$i]->date = ($body['videos']['episode'])[$i]['date'];
                        $wbs[$i]->price = ($body['videos']['episode'])[$i]['price'];
                        $wbs[$i]->videoPublicId = ($body['videos']['episode'])[$i]['videoPublicId'];
                        $wbs[$i]->videoName = ($body['videos']['episode'])[$i]['videoName'];
                        $wbs[$i]->mediaType = ($body['videos']['episode'])[$i]['mediaType'];
                        $wbs[$i]->productId =  $wb->id;
                        $wbs[$i]->save();
                    }
                } elseif (count($body['videos']['episode']) > count($wbs)) {
                    for ($i = 0; $i < count($body['videos']['episode']); $i++) {
                        if ($i < count($wbs)) {
                            $wbs[$i]->title = ($body['videos']['episode'])[$i]['title'];
                            $wbs[$i]->overview = ($body['videos']['episode'])[$i]['overview'];
                            $wbs[$i]->videoFile = ($body['videos']['episode'])[$i]['videoFile'];
                            $wbs[$i]->guest = ($body['videos']['episode'])[$i]['guest'];
                            $wbs[$i]->aboutGuest = ($body['videos']['episode'])[$i]['aboutGuest'];
                            $wbs[$i]->date = ($body['videos']['episode'])[$i]['date'];
                            $wbs[$i]->price = ($body['videos']['episode'])[$i]['price'];
                            $wbs[$i]->videoPublicId = ($body['videos']['episode'])[$i]['videoPublicId'];
                            $wbs[$i]->videoName = ($body['videos']['episode'])[$i]['videoName'];
                            $wbs[$i]->mediaType = ($body['videos']['episode'])[$i]['mediaType'];
                            $wbs[$i]->productId = $wb->id;
                            $wbs[$i]->save();
                        } else {
                            $wds = new ProductVideoDetail();
                            $wds->title = ($body['videos']['episode'])[$i]['title'];
                            $wds->overview = ($body['videos']['episode'])[$i]['overview'];
                            $wds->videoFile = ($body['videos']['episode'])[$i]['videoFile'];
                            $wds->guest = ($body['videos']['episode'])[$i]['guest'];
                            $wds->aboutGuest = ($body['videos']['episode'])[$i]['aboutGuest'];
                            $wds->date = ($body['videos']['episode'])[$i]['date'];
                            $wbs[$i]->price = ($body['videos']['episode'])[$i]['price'];
                            $wds->videoPublicId = ($body['videos']['episode'])[$i]['videoPublicId'];
                            $wds->videoName = ($body['videos']['episode'])[$i]['videoName'];
                            $wds->mediaType = ($body['videos']['episode'])[$i]['mediaType'];
                            $wds->productId = $wb->id;
                            $wds->save();
                        }
                    }
                } elseif (count($body['videos']['episode']) < count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        if ($i < count($body['videos']['episode'])) {
                            $wbs[$i]->title = ($body['videos']['episode'])[$i]['title'];
                            $wbs[$i]->overview = ($body['videos']['episode'])[$i]['overview'];
                            $wbs[$i]->videoFile = ($body['videos']['episode'])[$i]['videoFile'];
                            $wbs[$i]->guest = ($body['videos']['episode'])[$i]['guest'];
                            $wbs[$i]->aboutGuest = ($body['videos']['episode'])[$i]['aboutGuest'];
                            $wbs[$i]->date = ($body['videos']['episode'])[$i]['date'];
                            $wbs[$i]->price = ($body['videos']['episode'])[$i]['price'];
                            $wbs[$i]->videoPublicId = ($body['videos']['episode'])[$i]['videoPublicId'];
                            $wbs[$i]->videoName = ($body['videos']['episode'])[$i]['videoName'];
                            $wbs[$i]->mediaType = ($body['videos']['episode'])[$i]['mediaType'];
                            $wbs[$i]->productId = $wb->id;
                            $wbs[$i]->save();
                        } else {
                            $wbs[$i]->delete();
                        }
                    }
                }

                break;
            case 'Podcast':

                $product->prodType = $body['prodType'];

                if ($body['audioPrice'] != '') {
                    $product->audioCopy = 1;
                }

                $product->audioPrice = $body['audioPrice'];
                $product->draftValue = $body['draftType'];
                $product->editFlag = $body['editFlag'];
                $product->subscribePrice = $body['subscribePrice'];
                $product->streamOnlinePrice = $body['streamOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';
                // $wb = new ProductChapter();
                $wb = ProductChapter::where('product_id', $body['id'])->first();
                $wb->title = $body['podcast']['title'];
                $wb->host = $body['podcast']['host'];
                $wb->aboutHost = $body['podcast']['aboutHost'];
                $wb->sponsorName = $body['podcast']['sponsorName'];
                $wb->aboutSponsor = $body['podcast']['aboutSponsor'];
                $wb->excerpts = $body['podcast']['excerpt'];
                $wb->excerptsName = $body['podcast']['excerptName'];
                $wb->excerptsPublicId = $body['podcast']['excerptPublicId'];
                $wb->description = $body['podcast']['overview'];
                $wb->prodType = 'Podcast';


                $product->productChapter()->save($wb);
                $product->save();
                $wbs = ProductVideoDetail::where('productId', $wb->id)->get();


                /* foreach($body['podcast']['episode'] as $webinar){
                   // $wbe = new ProductVideoDetail();
                    foreach($wbs as $wbe) {
                        $wbe->title = $webinar['title'];
                        $wbe->overview = $webinar['overview'];
                        $wbe->videoFile = $webinar['videoFile'];
                        $wbe->guest = $webinar['guest'];
                        $wbe->aboutGuest = $webinar['aboutGuest'];
                        $wbe->date = $webinar['date'];
                        $wbe->videoPublicId = $webinar['videoPublicId'];
                        $wbe->videoName = $webinar['videoName'];
                        $wbe->mediaType = $webinar['mediaType'];
                        $wbe->productId = $wb->id;
                        $wbe->save();
                    }

                }*/

                if (count($body['podcast']['episode']) === count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        $wbs[$i]->title = ($body['podcast']['episode'])[$i]['title'];
                        $wbs[$i]->overview = ($body['podcast']['episode'])[$i]['overview'];
                        $wbs[$i]->videoFile = ($body['podcast']['episode'])[$i]['videoFile'];
                        $wbs[$i]->guest = ($body['podcast']['episode'])[$i]['guest'];
                        $wbs[$i]->aboutGuest = ($body['podcast']['episode'])[$i]['aboutGuest'];
                        $wbs[$i]->date = ($body['podcast']['episode'])[$i]['date'];
                        $wbs[$i]->videoPublicId = ($body['podcast']['episode'])[$i]['videoPublicId'];
                        $wbs[$i]->videoName = ($body['podcast']['episode'])[$i]['videoName'];
                        $wbs[$i]->mediaType = ($body['podcast']['episode'])[$i]['mediaType'];
                        $wbs[$i]->productId =  $wb->id;
                        $wbs[$i]->save();
                    }
                } elseif (count($body['podcast']['episode']) > count($wbs)) {
                    for ($i = 0; $i < count($body['podcast']['episode']); $i++) {
                        if ($i < count($wbs)) {
                            $wbs[$i]->title = ($body['podcast']['episode'])[$i]['title'];
                            $wbs[$i]->overview = ($body['podcast']['episode'])[$i]['overview'];
                            $wbs[$i]->videoFile = ($body['podcast']['episode'])[$i]['videoFile'];
                            $wbs[$i]->guest = ($body['podcast']['episode'])[$i]['guest'];
                            $wbs[$i]->aboutGuest = ($body['podcast']['episode'])[$i]['aboutGuest'];
                            $wbs[$i]->date = ($body['podcast']['episode'])[$i]['date'];
                            $wbs[$i]->videoPublicId = ($body['podcast']['episode'])[$i]['videoPublicId'];
                            $wbs[$i]->videoName = ($body['podcast']['episode'])[$i]['videoName'];
                            $wbs[$i]->mediaType = ($body['podcast']['episode'])[$i]['mediaType'];
                            $wbs[$i]->productId = $wb->id;
                            $wbs[$i]->save();
                        } else {
                            $wds = new ProductVideoDetail();
                            $wds->title = ($body['podcast']['episode'])[$i]['title'];
                            $wds->overview = ($body['podcast']['episode'])[$i]['overview'];
                            $wds->videoFile = ($body['podcast']['episode'])[$i]['videoFile'];
                            $wds->guest = ($body['podcast']['episode'])[$i]['guest'];
                            $wds->aboutGuest = ($body['podcast']['episode'])[$i]['aboutGuest'];
                            $wds->date = ($body['podcast']['episode'])[$i]['date'];
                            $wds->videoPublicId = ($body['podcast']['episode'])[$i]['videoPublicId'];
                            $wds->videoName = ($body['podcast']['episode'])[$i]['videoName'];
                            $wds->mediaType = ($body['podcast']['episode'])[$i]['mediaType'];
                            $wds->productId = $wb->id;
                            $wds->save();
                        }
                    }
                } elseif (count($body['podcast']['episode']) < count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        if ($i < count($body['podcast']['episode'])) {
                            $wbs[$i]->title = ($body['podcast']['episode'])[$i]['title'];
                            $wbs[$i]->overview = ($body['podcast']['episode'])[$i]['overview'];
                            $wbs[$i]->videoFile = ($body['podcast']['episode'])[$i]['videoFile'];
                            $wbs[$i]->guest = ($body['podcast']['episode'])[$i]['guest'];
                            $wbs[$i]->aboutGuest = ($body['podcast']['episode'])[$i]['aboutGuest'];
                            $wbs[$i]->date = ($body['podcast']['episode'])[$i]['date'];
                            $wbs[$i]->videoPublicId = ($body['podcast']['episode'])[$i]['videoPublicId'];
                            $wbs[$i]->videoName = ($body['podcast']['episode'])[$i]['videoName'];
                            $wbs[$i]->mediaType = ($body['podcast']['episode'])[$i]['mediaType'];
                            $wbs[$i]->productId = $wb->id;
                            $wbs[$i]->save();
                        } else {
                            $wbs[$i]->delete();
                        }
                    }
                }




                break;
            case 'Courses':
                $product->prodType = $body['prodType'];

                if ($body['softCopyPrice'] !== '') {
                    $product->softCopy = 1;
                }


                if ($body['hardCopyPrice'] !== '') {
                    $product->hardCopy = 1;
                }

                if ($body['readOnlinePrice'] !== '') {
                    $product->readOnline = 1;
                }

                if ($body['audioPrice'] !== '') {
                    $product->audioCopy = 1;
                }

                if ($body['videoPrice'] !== '') {
                    $product->videoCopy = 1;
                }
                $product->softCopyPrice = $body['softCopyPrice'];
                $product->draftValue = $body['draftType'];
                $product->hardCopyPrice = $body['hardCopyPrice'];
                $product->audioPrice = $body['audioPrice'];
                $product->videoPrice = $body['videoPrice'];
                $product->department_id =$body['course']['department_id'];
                $product->faculty_id =$body['course']['faculty_id'];
                $product->course_level = $body['course']['course_level'];
                $product->readOnlinePrice = $body['readOnlinePrice'];
                $mainImageName = "coverImage" . time();
                Cloudder::upload($body['coverImage'], $mainImageName, array("quality" => 60));
                $product->coverImage = 'https://res.cloudinary.com/bizguruh-com/image/upload/v1589205937/' . $mainImageName . '.jpg';


                
                $co =  ProductCourse::where('id', $body['course']['id'])->first();
                $co->title =  $body['course']['title'];
                $co->overview =  $body['course']['overview'];
                $co->duration =  $body['course']['duration'];
                $co->durationPeriod =  $body['course']['durationPeriod'];
                $co->courseModules =  $body['course']['courseModules'];
                $co->courseObjectives =  $body['course']['courseObjectives'];
                $co->whoThisCourseFor =  $body['course']['whoIsthisCourseFor'];
                $co->keyBenefits =  $body['course']['keyBenefit'];
                $co->whenYouComplete =  $body['course']['whenYouComplete'];
               
                $co->fullOnline =  $body['course']['fullOnline'];
                $co->mixedClass =  $body['course']['mixedClass'];
                $co->level =  $body['course']['level'];
                $co->certification =  $body['course']['certification'];
                $co->excerpt = $body['course']['excerpt'];
                $co->excerptName = $body['course']['excerptName'];
                $co->excerptFile = $body['course']['excerptFile'];
                $co->excerptPublicId = $body['course']['excerptPublicId'];
                $co->sponsorName = $body['course']['sponsorName'];

                // return $body['course'];
                $co->aboutSponsor = $body['course']['aboutSponsor'];
                $product->productCourse()->save($co);
                $product->save();
               

                $cl = CourseWhatYouWillLearn::where('product_course_id', $co->id)->get();
             
               if(!is_null($cl)){
                  
                if (count($body['course']['whatYouWillLearn']) === count($cl)) {
                    for ($i = 0; $i < count($cl); $i++) {
                        $cl[$i]->whatYouWillLearn = ($body['course']['whatYouWillLearn'])[$i]['whatYouWillLearn'];
                        $cl[$i]->save();
                    }
                } elseif (count($body['course']['whatYouWillLearn']) > count($cl)) {
                    for ($i = 0; $i < count($body['course']['whatYouWillLearn']); $i++) {
                        if ($i < count($cl)) {
                            $cl[$i]->whatYouWillLearn = ($body['course']['whatYouWillLearn'])[$i]['whatYouWillLearn'];
                            $cl[$i]->save();
                        } else {
                        }
                    }
                }
               }


                $wbs = ProductCourseModule::where('products_courses_id', $co->id)->get();


                if (count($body['course']['modules']) === count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        $wbs[$i]->title = ($body['course']['modules'])[$i]['title'];
                        $wbs[$i]->description = ($body['course']['modules'])[$i]['description'];
                        $wbs[$i]->fileName = ($body['course']['modules'])[$i]['fileName'];
                        $wbs[$i]->filePublicId = ($body['course']['modules'])[$i]['filePublicId'];
                        $wbs[$i]->audioFile = ($body['course']['modules'])[$i]['audioFile'];
                        $wbs[$i]->fileAudioName = ($body['course']['modules'])[$i]['fileAudioName'];
                        $wbs[$i]->audioPublicId = ($body['course']['modules'])[$i]['audioPublicId'];
                        $wbs[$i]->file = ($body['course']['modules'])[$i]['file'];
                        $wbs[$i]->videoFile = ($body['course']['modules'])[$i]['videoFile'];
                        $wbs[$i]->videoPublicId = ($body['course']['modules'])[$i]['videoPublicId'];
                        $wbs[$i]->fileVideoName = ($body['course']['modules'])[$i]['fileVideoName'];
                        $wbs[$i]->author = ($body['course']['modules'])[$i]['author'];
                        $wbs[$i]->aboutAuthor = ($body['course']['modules'])[$i]['aboutAuthor'];
                        $wbs[$i]->duration = ($body['course']['modules'])[$i]['duration'];
                        $wbs[$i]->durationPeriod = ($body['course']['modules'])[$i]['durationPeriod'];
                        $wbs[$i]->products_courses_id = $co->id;
                        $wbs[$i]->save();
                    }
                } elseif (count($body['course']['modules']) > count($wbs)) {
                    for ($i = 0; $i < count($body['course']['modules']); $i++) {
                        if ($i < count($wbs)) {
                            $wbs[$i]->title = ($body['course']['modules'])[$i]['title'];
                            $wbs[$i]->description = ($body['course']['modules'])[$i]['description'];
                            $wbs[$i]->fileName = ($body['course']['modules'])[$i]['fileName'];
                            $wbs[$i]->filePublicId = ($body['course']['modules'])[$i]['filePublicId'];
                            $wbs[$i]->audioFile = ($body['course']['modules'])[$i]['audioFile'];
                            $wbs[$i]->fileAudioName = ($body['course']['modules'])[$i]['fileAudioName'];
                            $wbs[$i]->audioPublicId = ($body['course']['modules'])[$i]['audioPublicId'];
                            $wbs[$i]->file = ($body['course']['modules'])[$i]['file'];
                            $wbs[$i]->videoFile = ($body['course']['modules'])[$i]['videoFile'];
                            $wbs[$i]->videoPublicId = ($body['course']['modules'])[$i]['videoPublicId'];
                            $wbs[$i]->fileVideoName = ($body['course']['modules'])[$i]['fileVideoName'];
                            $wbs[$i]->author = ($body['course']['modules'])[$i]['author'];
                            $wbs[$i]->aboutAuthor = ($body['course']['modules'])[$i]['aboutAuthor'];
                            $wbs[$i]->duration = ($body['course']['modules'])[$i]['duration'];
                            $wbs[$i]->durationPeriod = ($body['course']['modules'])[$i]['durationPeriod'];
                            $wbs[$i]->products_courses_id = $co->id;
                            $wbs[$i]->save();
                        } else {
                            $wds = new ProductCourseModule();
                            $wds->title = ($body['course']['modules'])[$i]['title'];
                            $wds->description = ($body['course']['modules'])[$i]['description'];
                            $wds->fileName = ($body['course']['modules'])[$i]['fileName'];
                            $wds->filePublicId = ($body['course']['modules'])[$i]['filePublicId'];
                            $wds->audioFile = ($body['course']['modules'])[$i]['audioFile'];
                            $wds->fileAudioName = ($body['course']['modules'])[$i]['fileAudioName'];
                            $wds->audioPublicId = ($body['course']['modules'])[$i]['audioPublicId'];
                            $wds->file = ($body['course']['modules'])[$i]['file'];
                            $wds->videoFile = ($body['course']['modules'])[$i]['videoFile'];
                            $wds->videoPublicId = ($body['course']['modules'])[$i]['videoPublicId'];
                            $wds->fileVideoName = ($body['course']['modules'])[$i]['fileVideoName'];
                            $wds->author = ($body['course']['modules'])[$i]['author'];
                            $wds->aboutAuthor = ($body['course']['modules'])[$i]['aboutAuthor'];
                            $wds->duration = ($body['course']['modules'])[$i]['duration'];
                            $wds->durationPeriod = ($body['course']['modules'])[$i]['durationPeriod'];
                            $wds->products_courses_id = $co->id;
                            $wds->save();
                        }
                    }
                } elseif (count($body['course']['modules']) < count($wbs)) {
                    for ($i = 0; $i < count($wbs); $i++) {
                        if ($i < count($body['course']['modules'])) {
                            $wbs[$i]->title = ($body['course']['modules'])[$i]['title'];
                            $wbs[$i]->description = ($body['course']['modules'])[$i]['description'];
                            $wbs[$i]->fileName = ($body['course']['modules'])[$i]['fileName'];
                            $wbs[$i]->filePublicId = ($body['course']['modules'])[$i]['filePublicId'];
                            $wbs[$i]->audioFile = ($body['course']['modules'])[$i]['audioFile'];
                            $wbs[$i]->fileAudioName = ($body['course']['modules'])[$i]['fileAudioName'];
                            $wbs[$i]->audioPublicId = ($body['course']['modules'])[$i]['audioPublicId'];
                            $wbs[$i]->file = ($body['course']['modules'])[$i]['file'];
                            $wbs[$i]->videoFile = ($body['course']['modules'])[$i]['videoFile'];
                            $wbs[$i]->videoPublicId = ($body['course']['modules'])[$i]['videoPublicId'];
                            $wbs[$i]->fileVideoName = ($body['course']['modules'])[$i]['fileVideoName'];
                            $wbs[$i]->author = ($body['course']['modules'])[$i]['author'];
                            $wbs[$i]->aboutAuthor = ($body['course']['modules'])[$i]['aboutAuthor'];
                            $wbs[$i]->duration = ($body['course']['modules'])[$i]['duration'];
                            $wbs[$i]->durationPeriod = ($body['course']['modules'])[$i]['durationPeriod'];
                            $wbs[$i]->products_courses_id = $co->id;
                            $wbs[$i]->save();
                        } else {
                            $wbs[$i]->delete();
                        }
                    }
                }


                break;
            default:
                return false;
        }


        // $body =  json_decode(request()->getContent(), true);





        return new ProductResource($product);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, $id)
    {
        //
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json('Deleted Successfully');
    }

    public function destroyMany()
    {
        $body =  json_decode(request()->getContent(), true);
        $id =  auth('vendor')->user()->id;

        for ($i = 0; $i < count($body); $i++) {
            $product = Product::where('id', $body[$i])->where('vendor_user_id', $id)->first();
            $product->delete();
        }
        return response()->json(['message', 'All item deleted']);
    }
}
