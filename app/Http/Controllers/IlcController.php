<?php

namespace App\Http\Controllers;

use\App\IlcUser;
use\App\User;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\UserSubscriptionPlan;
use Illuminate\Support\Carbon;
use App\Http\Resources\Product as ProductResource;

class IlcController extends Controller
{
    public function register(Request $request)
    {
        $check = IlcUser::where('email', $request['email'])->first();
        if (is_null($check)) {
            $valid  =  request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:ilc_users'],
                // 'password' => ['required', 'string', 'min:6'],
                
            ]);
      
            $user =  User::create([
                'name' => $request['name'],
                'lastName' => $request['name'],
                'email' => $request['email'],
                'phoneNo' => $request['phone'],
                'password' => Hash::make('bizguruh'),
                'count' => 1,
                'vendor_user_id'=> 0,
            ]);
           
            $userSubscriptionPlan = new UserSubscriptionPlan();
            $userSubscriptionPlan->price = 0;
            $userSubscriptionPlan->startDate = Carbon::now('Africa/Lagos');
            $userSubscriptionPlan->duration = null;
            $userSubscriptionPlan->level = 3;
            $userSubscriptionPlan->name = "Free";
            $userSubscriptionPlan->endDate = Carbon::now('Africa/Lagos')->addYear();
            $userSubscriptionPlan->user_id = $user->id;
            $userSubscriptionPlan->status = 'active';
            $userSubscriptionPlan->verify = 1;
            $userSubscriptionPlan->save();

            return IlcUser::create([
                 'user_id' => $user->id,
                'name' => $request['name'],
                'email' => $request['email'],
                'mat_no' => $request['mat_no'],
                'password' => Hash::make('bizguruh'),
                'phone' => $request['phone'],
                'gender' => $request['gender'],
                'level_of_edu' => $request['level_of_edu'],
                'school' => $request['school'],
                'faculty' => $request['faculty'],
                'department' => $request['department'],
                'course_level'=> $request['course_level']
            ]);
        } else {
            return response()->json(['status'=> 201]);
        }
    }

    public function getUser()
    {
    }
    public function getUserCourse($id)
    {
        $user= IlcUser::where('user_id',$id)->first();
   
        $product = Product::where('prodType', '=', 'Courses')->where('department_id', $user->department)->where('course_level',$user->course_level)->where('verify', '=', 1)->where('draftValue', '=', 'N')->get();
        return ProductResource::collection($product);
    }
}
