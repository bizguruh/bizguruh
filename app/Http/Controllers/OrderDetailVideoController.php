<?php

namespace App\Http\Controllers;

use App\OrderDetailVideo;
use Illuminate\Http\Request;

class OrderDetailVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderDetailVideo  $orderDetailVideo
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetailVideo $orderDetailVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderDetailVideo  $orderDetailVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetailVideo $orderDetailVideo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderDetailVideo  $orderDetailVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetailVideo $orderDetailVideo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderDetailVideo  $orderDetailVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderDetailVideo $orderDetailVideo)
    {
        //
    }
}
