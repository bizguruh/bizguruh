<?php

namespace App\Http\Controllers;

use App\Product;
use Carbon\Carbon;
use App\OrderDetail;
use App\CourseWatched;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseWatchedController extends Controller
{
    public function save(Request $request)
    {
     
        $user = auth('api')->user()->id;
        $product = OrderDetail::where('id',$request->course)->value('productId');
        $vendor = Product::where('id', $product)->value('vendor_user_id');    
        $watched = CourseWatched::where('user_id', $user)->where('course_id', $product)->where('vendor_id', $vendor)->where('video_id', $request->video)->first();
      
        if (is_null($watched)) {
            return  CourseWatched::create([
                'user_id'=>$user,
                'course_id' => $product,
                'vendor_id' => $vendor,
                'video_id'=>$request->video,
                'total_content'=> $request->total,
                'type'=>$request->type,
                'order_id'=> $request->course,
                'time'=> Carbon::now()
             ]);
        } else {
            return null;
        }
    }
    public function getCourseNotifications()
    {
        $vendor_id =  auth('vendor')->user()->id;
        $notify = DB::table('courses_watched')
         ->join('users', 'courses_watched.user_id','=', 'users.id')
         ->join('order_details', 'courses_watched.order_id','=', 'order_details.id')
        // ->join('order_detail_videos', 'order_details.order_id','=', 'order_detail_videos.id')
        ->where('vendor_id', $vendor_id)
        // ->where('message', 'subscribe')
        // ->orWhere('message', 'purchase')
        ->get();
        return $notify;
    }
}
