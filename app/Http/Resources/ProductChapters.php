<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductChapters extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
           'id' => $this->id,
           'title' => $this->title,
           'host' => $this->host,
           'description' => $this->description,
           'product_id' => $this->product_id,
           //'episodes' => ProductVideo::collection($this->productVideoDetail)

        ];
    }
}
