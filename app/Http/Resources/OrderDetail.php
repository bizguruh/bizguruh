<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
          'productId' => $this->productId,
          'vendorId' => $this->vendorId,
            'amount'=> $this->amount,
            'prodType' => $this->prodType,
            'quantity'=> $this->quantity,
            'itemPurchase' => $this->itemPurchase,
            'coverImage' => $this->coverImage,
            'type' => $this->type,
            'topic'=> $this->topic,
            'subjectMatter' => $this->subjectMatter,
            'title'=> $this->productTitle,
            'subTitle' => $this->episodeTitle,
            'vid' => $this->vid,
            'fileType'=>$this->fileType,
            'category' => $this->category,
            'created' => $this->created_at,
            'updated' => $this->updated_at
        ];
    }
}
