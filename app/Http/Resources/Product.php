<?php

namespace App\Http\Resources;

use App\ArticleProduct;
use App\CourseWhatYouWillLearn;
use App\EventProduct;
use App\ProductBook;
use App\ProductChapter;
use App\ProductCourse;
use App\ProductCourseModule;
use App\ProductJournal;
use App\ProductMarketReport;
use App\ProductMarketResearch;
use App\ProductSubCategoryBrand;
use App\ProductWhitePaper;
use App\Review;
use App\VendorShipping;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'prodType' => $this->prodType,
            'edition' => $this->description,
            'series' => $this->furtherDescription,
            'audioCopy' => $this->audioCopy,
            'softCopyPrice'=> $this->softCopyPrice,
            'hardCopyPrice'=> $this->hardCopyPrice,
            'webinarPrice' => $this->webinarPrice,
            'subscribePrice' => $this->subscribePrice,
            'streamPrice' => $this->streamOnlinePrice,
            'videoPrice'=> $this->videoPrice,
            'hardCopy' => $this->hardCopy,
            'draftType' => $this->draftValue,
            'documentName' => $this->copyType,
            'author' => $this->author,
            'pageNo' => $this->pageNo,
            'editFlag' => $this->editFlag,
            'productType' => $this->type,
            'industry' => \App\Industry::where('id', $this->industry_id)->first(),
            'type'=> \App\ProductCategory::where('id', $this->category_id)->first(),
            'subjectMatter' => \App\ProductSubCategory::where('id', $this->sub_category_id)->first(),
            'price' => $this->eventPrice,
            'prodCategoryType' => $this->productCategory,
            'eventPrice' => $this->eventPrice,
            'audioPrice'=> $this->audioPrice,
            'readOnlinePrice'=> $this->readOnlinePrice,
            'readOnline' => $this->readOnline,
            'tableOfContent'=> $this->tableOfContent,
            'aboutAuthor'=> $this->aboutAuthor,
            'location'=>$this->location,
            'addressLineOne'=>$this->addressLineOne,
            'addressLineTwo'=>$this->addressLineTwo,
            'state'=>$this->state,
            'myDate' => $this->created_at,
            'excerpts'=> $this->excerpts,
            'fileType' => $this->fileType,
            'publicationDate' => $this->overview,
            'subscriptionLevel'=> $this->subscriptionLevel,
            'publisher' => $this->publisher,
            'softCopy' => $this->softCopy,
            'sponsor' => $this->sponsor,
            'verify' => $this->verify,
            'category_id' => $this->category_id,
            'course_level' => $this->course_level,
            'department_id' => $this->department_id,
            'school_id' => $this->school_id,
            'faculty_id' => $this->faculty_id,
            'sub_category_id' => $this->sub_category_id,
            'vendor_user_id' => $this->vendor_user_id,
            'vendorName' => '',
            'sub_category_brand_id' => $this->sub_category_brand_id,
            'quantity' => $this->quantity,
            'coverImage' => $this->coverImage,
            'rating' => Review::where('productId', $this->id)->first() ? Review::where('productId', $this->id)->first()->rating : 0,
            'ratingCount' => Review::where('productId', $this->id)->count(),
            'broughtByStu' => \App\OrderDetail::where('productId', $this->id)->count(),
            'faq' => FaqResource::collection($this->faqs),
            'books' => ProductBook::where('product_id', $this->id)->first(),
            'articles' => ArticleProduct::where('product_id', $this->id)->first(),
            'events' => $this->productEvent,
            'eventSchedule' => $this->productEvent ? $this->productEvent->eventScheduleTime : '',
            'whitePaper' => ProductWhitePaper::where('product_id', $this->id)->first(),
            'marketReport' => ProductMarketReport::where('product_id', $this->id)->first(),
            'marketResearch' => ProductMarketResearch::where('product_id', $this->id)->first(),
            'webinar' => ProductChapter::where('product_id', $this->id)->first(),
            'webinarVideo' => ProductChapter::where('product_id', $this->id)->first(),
            'journal' => ProductJournal::where('product_id', $this->id)->first(),
            'courses' => ProductCourse::where('product_id', $this->id)->first(),
            'coursesModule' => ProductCourseModule::where('products_courses_id', ProductCourse::where('product_id', $this->id)->first() ? ProductCourse::where('product_id', $this->id)->first()->id : '')->select('title', 'description')->get(),
            'whatYouWillLearn' => CourseWhatYouWillLearn::where('product_course_id', ProductCourse::where('product_id', $this->id)->first() ? ProductCourse::where('product_id', $this->id)->first()->id : '')->get(),
            'category' => \App\ProductCategory::where('id', $this->category_id)->first(),
            'subcategory' => \App\ProductSubCategory::where('id', $this->sub_category_id)->first(),
            'topic' => ProductSubCategoryBrand::where('id', $this->sub_category_brand_id)->first(),
            'vendor' => \App\VendorUser::where('id', $this->vendor_user_id)->first(),
            'shippingDetail' => VendorShipping::where('vendor_id', $this->vendor_user_id)->first()
        ];
    }
}
