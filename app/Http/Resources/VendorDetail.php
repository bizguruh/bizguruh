<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
           'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'storeName' => $this->storeName,
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
             'image' => '/images/vendor'.'/'.$this->image,
            'bio' => $this->bio,

        ];
    }
}
