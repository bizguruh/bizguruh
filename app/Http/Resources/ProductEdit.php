<?php

namespace App\Http\Resources;

use App\ArticleProduct;
use App\EventProduct;
use Illuminate\Http\Resources\Json\JsonResource;
use App\ProductSubCategoryBrand;
use App\ProductBook;
use App\ProductChapter;
use App\ProductCourse;
use App\ProductJournal;
use App\ProductMarketReport;
use App\ProductMarketResearch;
use App\ProductWhitePaper;


class ProductEdit extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);

        return [
            'id' => $this->id,
            /*            'title' => $this->title,*/
            'prodType' => $this->prodType,
            'edition' => $this->description,
            'series' => $this->furtherDescription,
            'softCopyPrice'=> $this->softCopyPrice,
            'audioCopy' => $this->audioCopy,
            'hardCopyPrice'=> $this->hardCopyPrice,
            'videoPrice'=> $this->videoPrice,
            'hardCopy' => $this->hardCopy,
            'subscribePrice' => $this->subscribePrice,
            'streamOnlinePrice' => $this->streamOnlinePrice,
            'document'=> $this->document,
            'documentName' => $this->documentName,
            'documentPublicId' => $this->documentPublicId,
            'author' => $this->author,
            'pageNo' => $this->pageNo,
            'webinarPrice' => $this->webinarPrice,
            'productType' => $this->type,
            'audioPrice'=> $this->audioPrice,
            'readOnlinePrice'=> $this->readOnlinePrice,
            'readOnline' => $this->readOnline,
            'tableOfContent'=> $this->tableOfContent,
            'aboutAuthor'=> $this->aboutAuthor,
            'location'=>$this->location,
            'addressLineOne'=>$this->addressLineOne,
            'addressLineTwo'=>$this->addressLineTwo,
            'state'=>$this->state,
            'excerpts'=> $this->excerpts,
            'fileType' => $this->fileType,
            'publicationDate' => $this->overview,
            'course_level' => $this->course_level,
            'department_id' => $this->department_id,
            'school_id' => $this->school_id,
            'faculty_id' => $this->faculty_id,
            'publisher' => $this->publisher,
            'videoCopy' => $this->videoCopy,
            'softCopy' => $this->softCopy,
            'subscriptionLevel' => $this->subscriptionLevel,
            'sponsor' => $this->sponsor,
            'eventPrice'=> $this->eventPrice,
            'prodCategoryType' => $this->productCategory,
            'verify' => $this->verify,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'vendor_user_id' => $this->vendor_user_id,
            'sub_category_brand_id' => $this->sub_category_brand_id,
            'quantity' => $this->quantity,
            'coverImage' => $this->coverImage,
            'books' => ProductBook::where('product_id', $this->id)->first(),
            'articles' => ArticleProduct::where('product_id', $this->id)->first(),
/*            'events' => EventProduct::where('product_id', $this->id)->first(),*/
            'events' => $this->productEvent,
            'eventSchedule' => $this->productEvent ? $this->productEvent->eventScheduleTime : '',
            'whitePaper' => ProductWhitePaper::where('product_id', $this->id)->first(),
            'marketReport' => ProductMarketReport::where('product_id', $this->id)->first(),
            'marketResearch' => ProductMarketResearch::where('product_id', $this->id)->first(),
            'webinar' =>$this->productChapter,
            'courseModule' => $this->productCourse ? $this->productCourse->productCourseModule : '',
            'courses' => $this->productCourse,
            'whatYouWillLearn' => $this->productCourse ? $this->productCourse->courseToWhatLearn : '',
            'webinarVideo' => $this->productChapter ? $this->productChapter->productVideoDetail : '',
            'journal' => ProductJournal::where('product_id', $this->id)->first(),
/*            'courses' => ProductCourse::where('product_id', $this->id)->first(),*/
            'category' => \App\ProductCategory::where('id', $this->category_id)->first(),
            'subcategory' => \App\ProductSubCategory::where('id', $this->sub_category_id)->first(),
            'topic' => ProductSubCategoryBrand::where('id', $this->sub_category_brand_id)->first(),
            'vendor' => \App\VendorUser::where('id', $this->vendor_user_id)->first()
        ];
    }
}
