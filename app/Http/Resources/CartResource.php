<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'productId' => $this->productId,
            'userId' => $this->vendor,
            'price' => $this->price,
            'salePrice' => $this->salePrice,
            'product' => Product::collection($this->products)
        ];
    }
}
