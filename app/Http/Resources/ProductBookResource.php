<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'contributor' => $this->contributor,
            'aboutThisReport' => $this->aboutThisReport,
            'author' => $this->author,
            'overview' => $this->overview,
            'tableOfContent' => $this->tableOfContent,
            'excerpt' => $this->excerpt,
            'excerptName' => $this->excerptName,
            'product_id' => $this->product_id
        ];
    }
}
