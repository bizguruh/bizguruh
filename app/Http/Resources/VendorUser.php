<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'storeName' => $this->storeName,
            'email' => $this->email,
            'verify' => $this->verified,
            'detail' => \App\VendorDetail::where('vendor_id', $this->id)->first()
        ];
    }
}
