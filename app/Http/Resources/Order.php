<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      //  return parent::toArray($request);
        return [
          'deliveryId'=>$this->deliveryId,
            'paymentType'=> $this->paymentType,
            'paidAmount'=> $this->paidAmount,
            'orderNum' => $this->orderNum,
            'referenceNo' => $this->referenceNo,
            'verify' => $this->verify,
            'totalAmount'=>$this->totalAmount,
            'created' => $this->created_at,
            'updated' => $this->updated_at,
            'orderDetail' => OrderDetail::collection($this->orderDetail)
        ];
    }
}
