<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    protected $table ='education_level';
    protected $fillable = ['education_level','abbrevation'];
}
