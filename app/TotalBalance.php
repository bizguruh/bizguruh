<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalBalance extends Model
{
    //
    protected $table = 'total_balance';
    protected $fillable = ['user_id','total_income','total_expenses','total_balance','reminder', 'budget'];
}
