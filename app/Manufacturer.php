<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    //

    protected $table = 'manufacturer';
    protected $fillable =[
        'user_id',
        'product',
        'material',
        'product_material'
    ];
}
