<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSubscription extends Model
{
    protected $table ='account_subscriptions';
    protected $fillable = ['title','price','info','level','duration'];
}
