<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTemp extends Model
{

    protected $table = 'temp_subscription';
    public function user(){
        return $this->belongsTo('App\User');
    }

}
