<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleTime extends Model
{
        protected $table = 'event_schedule_times';

        protected $fillable = ['startDay', 'eventStart', 'eventEnd', 'moreInformation', 'product_id'];


    public function eventProduct()
    {
        return $this->belongsTo('App\EventProduct');
    }
}
