<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    //

    protected $fillable = ['firstName', 'lastName', 'phoneNo', 'address', 'state', 'city', 'country'];

    public function user(){
        return $this->belongsTo('App\User');
    }

}
