<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    protected $fillable = [
        'user_id',
        'invoice_no',
        'order_no',
        'customer_name',
        'status',
        'due_date',
        'quantity',
        'amount',
        'balance_due',
        'item_id',
        'invoiceArray'
    ];
}
