<?php

namespace App;


use config;
use Illuminate\Database\Eloquent\Model;


class Newsletter extends Model
{

    public function getCampaigns(){

    }
    public function sendCampaigns(){
        
    }
    public function createList(){
        
    }
    public function subscribe($lead)
    {

        $apikey = config('services.mailchimp.apikey');
       
       
        $list_id = config('services.mailchimp.list_id');
        
        $merge_vars=array(
    // 'OPTIN_IP'=>$ip, // Use their IP (if avail)
    'OPTIN-TIME'=>"now", // Must be something readable by strtotime...
    'FNAME'=>ucwords(strtolower(trim($lead['first_name']))),
    'LNAME'=>ucwords(strtolower(trim($lead['last_name']))),
    // 'COMPANY'=>ucwords(strtolower(trim($lead['company']))),
    // 'ORGTYPE'=>ucwords(strtolower(trim($lead['company_type']))),
    // 'PLANNING'=>strtolower(trim(empty($lead['planning_stage'])?"Unknown":$lead['planning_stage'])),
    );

        $send_data=array(
    'email'=>array('email'=>$lead['email']),
    'apikey'=>$apikey, // Your Key
    'id'=>$list_id, // Your proper List ID
    'merge_vars'=>$merge_vars,
    'double_optin'=>false,
    'update_existing'=>true,
    'replace_interests'=>false,
    'send_welcome'=>false,
    'email_type'=>"html",
);

        $payload=json_encode($send_data);
        $submit_url="https://us4.api.mailchimp.com/2.0/lists/subscribe.json";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $submit_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $result=curl_exec($ch);
        curl_close($ch);
        $mcdata=json_decode($result);
       

        if (!empty($mcdata->error)) {
            return "Mailchimp Error: ".$mcdata->error;
        }
        return $result;
    }
}
