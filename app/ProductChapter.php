<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductChapter extends Model
{
    //

    protected $table = 'product_chapters';

    protected $fillable = ['title', 'prodType', 'guest', 'host','description', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function productVideoDetail()
    {
        return $this->hasMany('App\ProductVideoDetail', 'productId', 'id');
    }
}
