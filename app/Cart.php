<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = 'carts';

    protected $fillable = ['productId', 'vendor', 'price', 'salePrice'];

    public function products(){
        return $this->hasMany('App\Product');
    }



}
