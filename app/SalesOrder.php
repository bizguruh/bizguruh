<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    protected $table = 'sales_order';

    protected $fillable = [
          'user_id',
        'order_no',
        'customer_name',
        'status',
        'quantity',
        'rate',
        'amount',
        'item_id',
        'sales_array'
    ];
}
