<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    //
    protected $table = 'f_a_qs';

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
