<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    //
    protected $table = 'expenses';
    protected $fillable = [
        'id',
        'user_id',
        'budget_id',
        'transaction',
        'amount',
        'category',
        'payment_type',
        'date',
        'note',
        'month'
    ];


}
