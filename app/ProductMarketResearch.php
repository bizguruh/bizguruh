<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMarketResearch extends Model
{
    //
    protected $table = 'product_market_research';

    protected $fillable = ['title', 'contributors', 'aboutThisReport', 'documentName', 'overview',
        'tableOfContents', 'excerpt', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
