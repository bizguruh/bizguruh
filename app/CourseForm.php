<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseForm extends Model
{
    protected $table = 'course_forms';
    protected $fillable = ['form_id','product_id','vendor_id'];
}
