<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table ='customers';
    protected $fillable = [
        'user_id',
        'customer_name',
        'email',
        'phone',
        'balance_due'
    ];
}
