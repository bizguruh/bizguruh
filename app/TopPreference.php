<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopPreference extends Model
{
    protected $table = 'top_preferences';

    protected $fillable = [
       'vendor_id',
       'preference',
       'count'
    ];
}
