<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'gender';
    protected $fillable = [
        'user_id',
        'vendor_id',
        'male',
        'female'
    ];
}
