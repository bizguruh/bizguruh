<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CgatePayment extends Model
{
    protected $table= 'cgate_payment';
    protected $fillable = ['ref','user_id','amount'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
