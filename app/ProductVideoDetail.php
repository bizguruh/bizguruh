<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVideoDetail extends Model
{
    //
    protected $table = 'product_video_details';

    protected $fillable = ['title', 'description', 'mediaType', 'videoName', 'videoFile'];

    protected $hidden = ['productId'];

    public function productDetail()
    {
        return $this->belongsTo('App\ProductChapter');
    }

   /* public function products()
    {
        return $this->belongsTo('App\Product');
    }*/


}
