<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessagePrivate extends Model
{
    protected $table =  'messages_private';
    protected $fillable = ['message','user_id','sender_id','receiver_id','status'];

    /**
 * A message belong to a user
 *
 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 */
public function vendor()
{
  return $this->belongsTo(VendorUser::class);
}
public function user()
{
  return $this->belongsTo(User::class);
}
}
