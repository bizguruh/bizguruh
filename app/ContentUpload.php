<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentUpload extends Model
{
    protected $table = 'content_upload';

    protected $fillable = [
        'vendor_id',
        'day'
    ];
}
