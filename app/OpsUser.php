<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class OpsUser extends Authenticatable
{
    //
    use HasApiTokens, Notifiable;

    protected $table = 'ops_users';

    protected $fillable = ['email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function verifyOps(){
        return $this->hasOne('App\VerifyOps');
}
}
