<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = "inventory";
    protected $fillable = [
        'user_id',
        'product_no',
        'product_name',
        'product_desc',
        'unit_cost',
        'package_cost',
        'in_stock',
        'product_value',
        'selling_price'
    ];
}
