<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ip extends Model
{

    protected $table = 'ip';
    //
    protected $fillable = ['email','ip_address'];
}
