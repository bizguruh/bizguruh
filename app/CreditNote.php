<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{
    protected $table = 'credit_notes';
    protected $fillable = [
        'user_id',
        'credit_no',
        'customer_name',
        'item_id',
        'rate',
        'quantity',
        'balance_due',
        'status',
        'customer_note',
        'terms_conditions'
    ];
}
