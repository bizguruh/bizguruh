<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWhitePaper extends Model
{
    //
    protected $table = 'products_white_paper';

    protected $fillable = ['title', 'author', 'contributors', 'problemStatement', 'executiveSummary',
        'excerpt', 'excerptName', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
