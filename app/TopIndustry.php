<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopIndustry extends Model
{
    protected $table = 'top_industries';

    protected $fillable = [
       'vendor_id',
       'industry',
       'count'
    ];
}
