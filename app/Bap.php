<?php

namespace App;

use App\Bap;
use App\User;
use Carbon\Carbon;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Bap extends Model
{
    protected $fillable = [
      'user_id',
      'current_month',
    'current_week' ,
    'folder_status',
    'survey_taken',
    'survey_scores',
    'survey_history',
    'status' ];
   
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function checkProgress(){
    
      $record = Bap::with('user')->where('status', 'active')->first();
      $now = Carbon::now();
      if(!is_null($record)){
        $check = Carbon::parse($record->created_at)->addDays(30);
     
        $diff = $now->greaterThan($check);
      
        if ($diff) {
      
         $users =  auth('api')->user();
    
         Bap::create([
           'user_id' => $record->user_id,
            'current_month'=> $record->current_month + 1,
            'current_week' => 1,
            'folder_status' => 'pending',
            'survey_taken' => 'no',
            'survey_scores' => 0,
            'survey_history' => '',
            'status' => 'active'
         ]);

         $record->status = 'finished';
         $record->save();
        
         return ['status' => 'created!'];
        }
      }
    }

    public function sendFolder(){
      $record = Bap::with('user')->where('folder_status','pending')->where('status', 'active')->first();
      if(!is_null($record)){
        
      }
    }
}
