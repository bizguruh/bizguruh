<?php

namespace App;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class IlcUser extends Authenticatable
{
    use HasApiTokens, Notifiable;


    protected $table = 'ilc_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
       'user_id', 'name', 'email', 'password','mat_no','phone','gender','level_of_edu', 'school','faculty', 'department','course_level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
  

  
}
