<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopContent extends Model
{
    protected $table = 'top_contents';

    protected $fillable = [
       'vendor_id',
       'content_id',
       'count'
    ];
}
