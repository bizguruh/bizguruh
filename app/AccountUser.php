<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class AccountUser extends  Authenticatable
{
    use HasApiTokens, Notifiable;
    //
    protected $table ="account_users";
    protected $fillable = [
        'name',
        'user_id',
        'role',
        'email',
        'phone_no',
        'password',
        'verify',
        'business_name',
        'customer_ref'
    ];
    public function findForPassport($username)
    {
       
        if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $this->where('phone_no', $username)->first();
          }
      
          if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return $this->where('email', $username)->first();
          }
      
      
    }

}
