<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistVendor extends Model
{

    protected $table = 'checklist_vendor';
    protected $fillable = [
        'vendor_id',
        'response'
    ];
}
