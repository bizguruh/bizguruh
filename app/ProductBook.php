<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBook extends Model
{
    //
    protected $table = 'product_books';

    protected $fillable = ['title', 'author', 'contributors', 'aboutThisReport', 'documentName', 'overview',
        'tableOfContents', 'excerpt', 'product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
