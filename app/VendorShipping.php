<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorShipping extends Model
{
    //
    protected $fillable = ['location', 'addressOne', 'addressTwo', 'state', 'country', 'localShippingRates', 'localShippingTime', 'intlShippingRates',
        'intlShippingTime', 'deliveryInformation', 'returnPolicy', 'extraInformation', 'vendor_id'];

    public function vendorUser(){
        return $this->belongsTo('App\VendorUser', 'vendor_id');
    }
}
