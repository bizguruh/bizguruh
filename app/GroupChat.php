<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupChat extends Model
{
    //
    protected $table =  'group_chat';
    protected $fillable = ['course_id','message'];


    public function user()
{
  return $this->belongsTo(User::class);
}
}
