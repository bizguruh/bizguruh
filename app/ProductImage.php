<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //

    protected $fillable = ['main_image', 'back_image', 'left_image', 'right_image', 'front_image', 'top_image'];

    public function product(){
        return $this->belongsTo('App\Product');
    }

}
