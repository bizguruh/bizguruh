<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age extends Model
{
    protected $table = 'ages';
     protected $fillable = [
      'user_id',
        'vendor_id',
        'first',
        'second',
        'third',
        'fourth',
        'fifth'
     ];
}
