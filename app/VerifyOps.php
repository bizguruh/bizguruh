<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyOps extends Model
{
    //
    protected $guarded = [];

    public function opsUser(){
        return $this->belongsTo('App\OpsUser', 'ops_user_id');
    }
}
