<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class AdminUser extends Authenticatable
{
    //
    use HasApiTokens, Notifiable;

    protected $table = 'admin_users';

    protected $fillable = ['email', 'password', 'role'];

    protected $hidden = ['password', 'remember_token'];

    public function verifyAdmin(){
        return $this->hasOne('App\VerifyAdmin');
    }



}
