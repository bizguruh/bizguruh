<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'user_id',
        'payment_no',
        'type',
        'order_no',
        'customer_name',
        'invoice_no',
        'mode',
        'amount'
    ];
}
