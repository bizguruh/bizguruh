<?php

namespace App\Services;

use App\User;
use Carbon\Carbon;
use App\AccountUser;
use App\SocialAccounts;
use App\UserSubscriptionPlan;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Two\User as ProviderUser;

class SocialAccountsService
{
    /**
     * Find or create user instance by provider user instance and provider name.
     * 
     * @param ProviderUser $providerUser
     * @param string $provider
     * 
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {
        $linkedSocialAccount = SocialAccounts::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();

        if ($linkedSocialAccount) {
            return $linkedSocialAccount->user;
        } else {
            $user = null;

            if ($email = $providerUser->getEmail()) {
                $user = User::where('email', $email)->first();
            }

            if (! $user) {
                $user = User::create([
                    'name' => $providerUser->getName(),
                    'first_name' => $providerUser->getName(),
                    'lastName' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'phoneNo' => 8799,
                    'password' =>null,
                    'count' => 1,
                    'specialToken' => null,
                    'vendor_user_id'=> 0,
                    'business_name'=>null,
                    'business_type'=>null,
                    'address' => null,
                    'logo'=> $providerUser->getAvatar()
                ]);
                $userReg = User::where('email', $providerUser->getEmail())->first();
                AccountUser::create([
                    'user_id' => $userReg->id,
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'phone_no' => $userReg->phone_no,
                    'password' => Hash::make('admin'),
                    'role' => 'admin',
                    'verify' => 1
                   
                ]);
                $userSubscriptionPlan = new UserSubscriptionPlan();
                $userSubscriptionPlan->price = 0;
                $userSubscriptionPlan->startDate = Carbon::now('Africa/Lagos');
                $userSubscriptionPlan->duration = null;
                $userSubscriptionPlan->level = 0;
                $userSubscriptionPlan->name = "Free";
                $userSubscriptionPlan->endDate = Carbon::now('Africa/Lagos')->addYear();
                $userSubscriptionPlan->user_id = $userReg->id;
                $userSubscriptionPlan->verify = 1;
                $userSubscriptionPlan->save();
        
            }

            $user->linkedSocialAccounts()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);

            return $user;
        }
    }
}