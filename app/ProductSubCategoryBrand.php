<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategoryBrand extends Model
{
    //
    protected $table = 'sub_categories_brand';


    protected $fillable = ['name', 'description', 'sub_brand_category_id'];

    public function subCategory(){
        return $this->belongsTo('App/ProductSubCategory');
    }
}
