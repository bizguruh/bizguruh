<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorDetail extends Model
{
    //
    protected $fillable = ['firstName', 'lastName', 'address', 'city', 'state', 'country',
             'image', 'bio', 'vendor_id'];

    public function vendorUser(){
        return $this->belongsTo('App\VendorUser', 'vendor_id');
    }
}
