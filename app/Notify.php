<?php

namespace App;

use App\TotalBalance;
use App\ContentUpload;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
  
    public function send()
    {
        $apikey = '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt';
        $vendor = ContentUpload::get();
        $now = Carbon::now('Africa/Lagos')->dayOfWeek;

        foreach ($vendor as $key) {
            $day = Carbon::parse($key->day)->dayOfWeek;
            if ($now === $day) {
                
               
                $send_data=array(
                        'campaign_name'=>'Content Upload',
                        'notification'=> [
                            'title'=>'Content upload reminder',
                            'message'=>'This is to remind you to send in your content today',
                            'icon'=>"https://bizguruh.com/images/logo.png",
                            'url'=>'http://bizguruh.com/vendor/dashboard'
                        ],
                        'send_to'=>'identifiers',
                        'identifiers'=>[$key->user_id]
                );
        
                $payload=json_encode($send_data);
                $submit_url='https://api.engagespot.co/2/campaigns';
                $ch=curl_init();
                curl_setopt($ch, CURLOPT_URL, $submit_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    "api-key:".$apikey,
                    'content-type: application/json'
        
                ]);
              
                $result=curl_exec($ch);
                curl_close($ch);
                $mcdata=json_decode($result);
               
        
                if (!empty($mcdata->error)) {
                    return "Mailchimp Error: ".$mcdata->error;
                }
               
            }
        }
    }

    public function accountReminder(){
        $apikey = '6fjqpZF8ZNnwfWXuQmd7tntMYYz8wt';
        $user = TotalBalance::get();
        $now = strtotime(Carbon::now('Africa/Lagos')->format('H:i'));     
        echo Carbon::now('Africa/Lagos')->addHour()->format('H:i');
        foreach ($user as $key) {
            $time = strtotime(Carbon::parse($key->reminder)->format('H:i'));
            if (!is_null($key->reminder) ) {
               if ($now === $time) {
              
                $send_data=array(
                        'campaign_name'=>'Transactions Reminder',
                        'notification'=> [
                            'title'=>'Daily transactions reminder',
                            'message'=>'This is a reminder to record your daily transactions for today',
                            'icon'=>"https://bizguruh.com/images/logo.png",
                            'url'=>'http://bizguruh.com/account'
                        ],
                        'send_to'=>'identifiers',
                        'identifiers'=>[$key->user_id]
                );
        
                $payload=json_encode($send_data);
                $submit_url='https://api.engagespot.co/2/campaigns';
                $ch=curl_init();
                curl_setopt($ch, CURLOPT_URL, $submit_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    "api-key:".$apikey,
                    'content-type: application/json'
        
                ]);
              
                $result=curl_exec($ch);
                curl_close($ch);
                $mcdata=json_decode($result);
               
        
                if (!empty($mcdata->error)) {
                    return "Mailchimp Error: ".$mcdata->error;
                }
               
            }
            }
          

           
           
        }
    }
}
