<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSettings extends Model
{
    //
    protected $table = 'vendors_settings';

    protected $fillable = ['storeName', 'sidebarBackgroundColor', 'sidebarColor', 'sidebarFontSize', 'sidebarMargin',
    'topMenuBackgroundColor', 'topMenuColor', 'topMenuFontSize', 'topMenuMargin', 'overallBackgroundColor',
        'overallColor', 'overFontSize'];

    public function vendorUser(){
        return $this->belongsTo('App\VendorUser', 'vendor_id');
    }
}
