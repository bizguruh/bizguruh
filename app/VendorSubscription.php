<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorSubscription extends Model
{
    protected $table ='vendor_subscriptions';
    protected $fillable = ['title','price','info','level','duration'];
}
