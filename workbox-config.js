module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{js,php,css,html,ico,svg,jpg,png,jpeg}"
  ],
  "swDest": "public/sw.js",
  // "maximumFileSizeToCacheInBytes": 10000000,
};