let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// require('laravel-mix-bundle-analyzer');
// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

// if (!mix.inProduction()) {
//     mix.bundleAnalyzer();
// }

if (mix.inProduction()) {
   mix.version();
}



mix.js('resources/assets/js/app.js'
        , 'public/js/app.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles([
      'public/css/app.css',
      'public/css/layout.css',
      'public/css/hamburger.css',
      'public/css/style.css',
      'public/css/bootstrap.css',
      'public/css/framework.css',
      'public/css/graph.css',
      'public/css/custom.css',
      'public/css/export.css',
      'public/css/monthly.css',
      'public/css/SidebarNav.min.css',
      'public/css/styles.css',
      'public/css/ops.css',
      'public/css/opstemplate.css',
      'public/css/userStyles.css',
    ], 'public/css/app.css')
    .extract(['vue','vue-video-player','jquery','lodash','vue-toasted','bootstrap-vue','vuejs-dialog','vue-element-loading','vue-affix','vue-notification','vue-scrollto','vue-graph', 'vue-moment','bootstrap','swiper'
    
]) .webpackConfig({
   resolve: {
       alias: {
           "@": path.resolve("resources/assets") // just to use relative path properly
       }
   }
})
   .options({processCssUrls: false
});

// mix.copyDirectory('resources/assets/images', 'public/images');
mix.config.webpackConfig.output = {
   chunkFilename: 'js/[name].[chunkhash].js',
   publicPath: '/',
   
};

 
   

